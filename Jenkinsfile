@Library('octopus-utils') _

def branch = "${BRANCH_NAME}".replaceAll('-', '_').replaceAll('/', '_')
def version = "${BRANCH_NAME}".replaceAll('_', '-').replaceAll('/', '-')
def workspace = "/opt/bitnami/apps/jenkins/jenkins_home/workspace/octopus_wealth_" + branch
def virtualenv = "~/.virtualenvs/octopus_wealth_${branch}"

// Create MySQL port
Random random = new Random()
int randomDbPort = random.nextInt(29999 + 1 - 20000) + 20000

pipeline {
  options {
    buildDiscarder(logRotator(numToKeepStr: '10', artifactNumToKeepStr: '10'))
    disableConcurrentBuilds()
  }

  agent {
    label {
      label ""
      customWorkspace workspace
    }
  }

  stages {
    stage('checkout') {
      steps {
        checkout scm
        ciSkip action: 'check'
        sh('git remote set-url origin git@github.com:octopus-investments/octopus-wealth-fact')
        sh('git fetch --tags')
      }
    }

    stage('Install Prerequisites') {
      steps {
        echo 'Installing required software and setup'
        sh """
          . ~/.bash_profile
          pyenv local 3.6.5
          virtualenv ${virtualenv} --python=python3.6
          . ${virtualenv}/bin/activate
          pip install -U -r requirements.txt -r env_requirements.txt
          npm i --prefix frontend frontend
          pre-commit install
        """
      }
    }

    stage('style') {
      steps {
        sh """
          . ~/.bash_profile
          pyenv local 3.6.5
          . ${virtualenv}/bin/activate
          pre-commit run --all-files
        """
      }
    }

    stage('deploy') {
      steps {
        sh '''
          cd frontend
          npm run build
          docker system prune -f
        '''
        script {
          if (branch == 'master') {
            kubernetesDeploy project: 'octopus-wealth-fact', version: version, environment: 'prod', configname: 'octopus-wealth-fact', clustername: 'wealth', zone: 'europe-west1-b', mysqluser: 'octopus', domain: 'octopuswealth.com', certName: 'octopuswealth.com', memcached: true, envvars: 'WEALTH_ENV=prod', port: 8080, customDomains: 'octopuswealth.com'
            sh """
              export SLACK_CHANNEL=scrum-wealth
              export PROJECT="Octopus Wealth"
              export ENVIRONMENT=Production
              /home/tomcat/bin/release
            """
          } else {
            kubernetesDeploy project: 'octopus-wealth-fact', version: version, environment: 'dev', configname: 'octopus-wealth-fact', clustername: 'wealth', zone: 'europe-west1-b', mysqluser: 'octopus', domain: 'octopuswealth.com', certName: 'octopuswealth.com', memcached: true, envvars: 'WEALTH_ENV=dev', port: 8080, customDomains: 'dev.octopuswealth.com'
          }
        }
      }
    }
  }

  post {
    success {
      slackSend color: "#00FF00", channel: "#audit-wealth", message: "Wealth build Success: Job ${env.JOB_NAME} [${env.BUILD_NUMBER}] (${env.BUILD_URL}) by ${env.CHANGE_AUTHOR}"
    }

    failure {
      slackSend color: "#FF0000", channel: "#audit-wealth", message: "Wealth build failure: Job ${env.JOB_NAME} [${env.BUILD_NUMBER}] (${env.BUILD_URL}) by ${env.CHANGE_AUTHOR}"
    }

    unstable {
      slackSend color: "#FFFF00", channel: "#audit-wealth", message: "Wealth build unstable: Job ${env.JOB_NAME} [${env.BUILD_NUMBER}] (${env.BUILD_URL}) by ${env.CHANGE_AUTHOR}"
    }

    always {
      deleteDir()
    }
  }
}
