from pymemcache.client import base as memcache
from tornado.options import options

app_engine = False


class cache:
    def __init__(self):
        cache.__dict__["impl"] = memcache.Client(options.cache_server_list, debug=0)


def get_cache():
    # if not cache.__dict__.get('impl', None):
    #     cache()
    # return cache.impl
    return FakeCache()


class FakeCache:
    @classmethod
    def get(cls, key):
        return False

    @classmethod
    def set(cls, key, value):
        return True
