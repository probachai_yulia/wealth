// eslint-disable-next-line
import 'regenerator-runtime/runtime';
import React from 'react';
import ReactDOM from 'react-dom';
import App from './app';

const container = document.getElementById('root');
ReactDOM.render(<App />, container);
