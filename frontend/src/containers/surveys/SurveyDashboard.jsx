import React, { Component } from 'react';
import { Link, withRouter } from 'react-router-dom';
import PropTypes from 'prop-types';
import get from 'lodash/get';
import { change } from 'redux-form';
import connect from 'react-redux/es/connect/connect';
import { bindActionCreators } from 'redux';
import moment from 'moment';
import { getMePlan } from '../../actions/UserActions';
import { SECTION_FACTFIND } from '../../helpers/constants';

class SurveyDashboard extends Component {
    constructor(props) {
        super(props);

        this.state = {
            sectionFf: SECTION_FACTFIND,
        };

        const {
            handleGetMePlan,
        } = this.props;

        handleGetMePlan();

        this.workingProgress = this.workingProgress.bind(this);
    }

    workingProgress(task, type, id) {
        const {
            assets, plan, users, cashflows, debts,
        } = this.props;
        let action = 'disable';
        let link = '/survey/dashboard';
        let result = {
            action: 'todo',
            button: 'Start',
            link,
        };

        switch (task) {
            case SECTION_FACTFIND[0].section: /* FAMILY */
                if (typeof users.main !== 'undefined') {
                    action = 'todo';
                    link = '/survey/family/know-you';

                    if (users.main.profile.dob !== null) {
                        action = 'working';
                        link = '/survey/family/about-partner';
                    }
                    if ((plan.family_has_partner && users.partner !== null) || plan.family_has_partner === false) {
                        action = 'working';
                        link = '/survey/family/about-children';
                    }
                    if ((plan.family_has_children && users.child.length > 0) || plan.family_has_children === false) {
                        action = 'working';
                        link = '/survey/family/about-dependents';
                    }
                    if ((plan.family_has_dependents && users.dependent.length > 0) || plan.family_has_dependents === false) {
                        action = 'working';
                        link = '/survey/family/work/main';
                    }
                    if (users.main.jobs.length > 0) {
                        action = 'working';
                        link = '/survey/family/retirement';
                    }
                    if (users.main.retirement_age !== null || users.main.is_never_want_to_retire !== null) {
                        action = 'done';
                        // eslint-disable-next-line prefer-destructuring
                        link = SECTION_FACTFIND[0].link;
                    }
                }
                break;
            case SECTION_FACTFIND[1].section: { /* FINANCE */
                action = 'todo';
                // eslint-disable-next-line prefer-destructuring
                link = SECTION_FACTFIND[1].link;
                if (assets.length > 0) {
                    const cashOther = assets.filter(item => item.asset_type === 'cash_other');
                    const cash = assets.filter(item => item.asset_type === 'cash_current_account' || item.asset_type === 'cash_term_deposit' || item.asset_type === 'cash_savings' || item.asset_type === 'cash');
                    const pensions = assets.filter(item => item.asset_type === 'benefit' || item.asset_type === 'contribution' || item.asset_type === 'annuity' || item.asset_type === 'pension');
                    const property = assets.filter(item => item.asset_type.indexOf('property') !== -1);
                    const isa = assets.filter(item => item.asset_type.indexOf('isa') !== -1);
                    if (cashOther.length > 0 && property.length > 0 && cash.length > 0 && isa.length > 0 && pensions.length > 0) {
                        action = 'done';
                        // eslint-disable-next-line prefer-destructuring
                        link = SECTION_FACTFIND[1].link;
                    } else if (cashOther.length > 0 || property.length > 0 || cash.length > 0 || isa.length > 0 || pensions.length > 0) {
                        action = 'done';
                        // eslint-disable-next-line prefer-destructuring
                        link = SECTION_FACTFIND[1].link;
                    }
                }
                break;
            }
            case SECTION_FACTFIND[2].section: /* INCOME */
                action = 'todo';
                // eslint-disable-next-line prefer-destructuring
                link = SECTION_FACTFIND[2].link;
                if (cashflows.length > 0) {
                    const cashIncome = cashflows.filter((item) => {
                        if (item.user_cashflow_type !== null && typeof item.user_cashflow_type !== 'undefined') {
                            if (item.user_cashflow_type.indexOf('work') !== -1) {
                                return item;
                            }
                        }
                        return null;
                    });
                    if (cashIncome.length > 0) {
                        action = 'working';
                        link = '/survey/income/additional-income';
                    }
                    if (plan.plan_has_other_income !== null) {
                        action = 'working';
                        link = '/survey/income/income-changes';
                    }
                    if (users.main.is_not_sure_other_income !== null) {
                        action = 'done';
                        ({
                            link,
                        } = SECTION_FACTFIND[2]);
                    }
                }
                break;
            case SECTION_FACTFIND[3].section: /* CHARITY & GIFTING */
                action = 'todo';
                ({
                    link,
                } = SECTION_FACTFIND[3]);
                if ((plan.plan_has_gifting !== null && typeof plan.plan_has_gifting !== 'undefined') || (plan.plan_has_charity !== null && typeof plan.plan_has_charity !== 'undefined')) {
                    action = 'done';
                    ({
                        link,
                    } = SECTION_FACTFIND[3]);
                }
                break;
            case SECTION_FACTFIND[4].section: /* DEBTS */
                action = 'todo';
                ({
                    link,
                } = SECTION_FACTFIND[4]);
                if (debts.length > 0) {
                    const debtsMotgage = debts.filter(item => item.plan_debt_type.indexOf('mortgage') !== -1);
                    const debtsLoan = debts.filter(item => item.plan_debt_type.indexOf('loans') !== -1);
                    const debtsCreditCard = debts.filter(item => item.plan_debt_type.indexOf('credit_card') !== -1);
                    if (debtsMotgage.length > 0 && debtsLoan.length > 0 && debtsCreditCard.length > 0) {
                        action = 'done';
                    } else if (debtsMotgage.length > 0 || debtsLoan.length > 0 || debtsCreditCard.length > 0) {
                        action = 'working';
                    }
                }
                break;
            case SECTION_FACTFIND[5].section: /* EXPENSES */
                action = 'todo';
                ({
                    link,
                } = SECTION_FACTFIND[5]);
                if (cashflows.length > 0) {
                    const underageChild = users.child.filter(item => moment()
                        .diff(item.profile.dob, 'years') < 16);
                    const ageChild = users.child.filter(item => moment()
                        .diff(item.profile.dob, 'years') < 18);
                    const nannyCashflow = cashflows.find(item => item.user_cashflow_type === 'nanny');
                    const childcareCashflow = cashflows.find(item => item.user_cashflow_type === 'childcare');
                    const educChasflow = cashflows.find(item => item.user_cashflow_type === 'education');

                    const totalExpense = cashflows.filter(item => item.user_cashflow_type === 'expenses_total');
                    if (underageChild.length > 0 && (typeof nannyCashflow === 'undefined' || typeof childcareCashflow === 'undefined')) {
                        action = 'working';
                        link = '/survey/expenses/add-children-underage-expenses';
                    } else if (ageChild.length > 0 && (typeof educChasflow === 'undefined' || typeof childcareCashflow === 'undefined')) {
                        action = 'working';
                        link = '/survey/expenses/about-children-expenses';
                    } else {
                        action = 'working';
                        link = '/survey/expenses/add-expenses';
                    } if (totalExpense.length > 0) {
                        action = 'done';
                        link = '/survey/expenses/about-children-expenses';
                    }
                }
                break;
            case SECTION_FACTFIND[6].section: /* PROTECTION */
                action = 'todo';
                ({
                    link,
                } = SECTION_FACTFIND[6]);
                if (assets.length > 0) {
                    if (plan.has_insurance !== null) {
                        action = 'done';
                    }
                }
                break;
            case SECTION_FACTFIND[7].section: /* RISK TOLERANCE */
                action = 'done';
                ({
                    link,
                } = SECTION_FACTFIND[7]);
                if (assets.length > 0) {
                    ({
                        link,
                    } = SECTION_FACTFIND[7]);
                }
                break;
            case SECTION_FACTFIND[8].section: /* PROVIDERS */
                action = 'todo';
                ({
                    link,
                } = SECTION_FACTFIND[8]);
                if (typeof users.main !== 'undefined') {
                    if (users.main.profile.national_insurance_nbr !== null && users.partner !== null) {
                        if (users.partner.profile.national_insurance_nbr !== null) {
                            action = 'done';
                        } else {
                            action = 'working';
                        }
                    } else if (users.main.profile.national_insurance_nbr !== null && users.partner === null) {
                        action = 'done';
                    }
                }
                break;
            default:
                break;
        }

        const {
            sectionFf,
        } = this.state;


        if (action === 'done') {
            result = {
                action: 'done',
                button: 'Edit',
                link,
            };
        } else if (action === 'working') {
            result = {
                action: 'todo',
                button: 'Continue',
                link,
            };
        } else {
            result = {
                action: 'todo',
                button: 'Start',
                link,
            };
        }
        if (sectionFf[id].action !== action) {
            const ff = sectionFf;
            ff[id].action = action;
            this.setState({ sectionFf: ff });
        }

        if (id > 0) {
            if (sectionFf[id - 1].action !== 'done') {
                result = {
                    action: 'disable',
                    button: 'Continue',
                    link,
                };
            }
        }
        return result[type];
    }

    render() {
        const {
            sectionFf,
        } = this.state;
        return (
            <div className="container dis-f fd-c ">
                <div className="span-8 d-c">
                    <h2 className="fw-b mt-xlarge mb-xsmall">
                        Building your LifeLine
                    </h2>
                    <p className="mb-xsmall">
The following information will help us build the first version of your
                        LifeLine. The more accurate your responses. The more accurate your plan. You can come back and
                        complete the questionnaire at any time.
                    </p>
                </div>
                <div className="span-8 d-c">
                    {
                        sectionFf.map(item => (
                            <div
                                key={item.id}
                                className={`links-block mt-1rem ${this.workingProgress(item.section, 'action', item.id)} list-style-${item.id + 1}`}
                            >
                                <p>
                                    {item.title}
                                </p>
                                <p className="fz-lhs fz-sml">
                                    {item.description}
                                </p>
                                {this.workingProgress(item.section, 'action', item.id) !== 'disable'
                                && (
                                    <Link to={this.workingProgress(item.section, 'link', item.id)}>
                                        {this.workingProgress(item.section, 'button', item.id)}
                                    </Link>
                                )
                                }
                            </div>
                        ))
                    }

                </div>
            </div>
        );
    }
}

SurveyDashboard.propTypes = {
    assets: PropTypes.arrayOf(PropTypes.object),
    debts: PropTypes.arrayOf(PropTypes.object),
    cashflows: PropTypes.arrayOf(PropTypes.object),
    users: PropTypes.object,
    plan: PropTypes.object,
    handleGetMePlan: PropTypes.func.isRequired,
};

SurveyDashboard.defaultProps = {
    assets: [],
    debts: [],
    cashflows: [],
    users: {},
    plan: {},
};

const mapStateToProps = state => ({
    assets: get(state, 'plan.assets', []),
    debts: get(state, 'plan.debts', []),
    plan: get(state, 'plan', {}),
    users: get(state, 'plan.users', {}),
    cashflows: get(state, 'plan.cashflows', []),
    maritalStatus: get(state, 'plan.marital_status', null),
    dob: get(state, 'plan.users.main.profile.dob', null),
    birthCountryCD: get(state, 'plan.users.main.profile.birth_country_cd', null),
});

const mapDispatchToProps = dispatch => ({
    changeFieldValue: (field, value) => {
        dispatch(change('addInvestments', field, value));
    },
    handleGetMePlan: bindActionCreators(getMePlan, dispatch),
});

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(SurveyDashboard));
