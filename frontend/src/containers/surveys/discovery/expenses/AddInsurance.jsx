import React, { Component, Fragment } from 'react';
import { change, Field } from 'redux-form';
import get from 'lodash/get';
import isEmpty from 'lodash/isEmpty';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { withRouter } from 'react-router-dom';

import { BackNextButtons } from '../../../../components/BackNextButtons';
import FulfilledLine from '../../../../components/FulfilledLine';

import { booleanRequired } from '../../../../utils/validation.helper';
import YesNo from '../../../../components/YesNo';
import FreeAdditionalBox from '../../../../components/FreeAdditionalBox';
import SurveyHoc from '../../../hoc/SurveyHoc';
import { normalizeBoolean } from '../../../../utils/helper';
import { patchPlan } from '../../../../actions/PlanActions';
import ModalInformationIcon from '../../../../components/ModalInformationIcon';

/**
 * Render the content for user idle. Dumb component
 *
 */
class AddInsurance extends Component {
    static formName = 'addInsurance';

    static surveyData = {
        formName: AddInsurance.formName,
        nextStage: 'nextStage',
        prevStage: 'prevStage',
        data: [
            {
                fields: [
                    'has_insurance',
                    'insurance_additional',
                ],
                isUpdate: 'update',
                selector: 'planId',
                // additionalFields: [],
                methodUpdate: patchPlan,
            },
        ],
    };

    constructor(props) {
        super(props);

        this.renderExistInsurance = this.renderExistInsurance.bind(this);

        const {
            handleGetMePlan,
        } = this.props;

        handleGetMePlan();
    }

    renderExistInsurance() {
        const { formValue, isSubmitted } = this.props;
        if (get(formValue, 'values.has_insurance', null) === true) {
            return (
                <Fragment>
                    <Field
                        component={FreeAdditionalBox}
                        label="Can you tell us a little more about the insurance you or your family have?"
                        extraClasses="wealth-textarea mb-5rem"
                        placeholder="Explain brielfy..."
                        name="insurance_additional"
                        id="1"
                        cols={30}
                        rows={10}
                        isShowErrors={isSubmitted}
                    />
                </Fragment>
            );
        }
        return null;
    }

    render() {
        const {
            formValue,
            isSubmitted,
            handleSubmit,
            handleBack,
            partnerName,
        } = this.props;
        const isAnySyncErrors = isEmpty(get(formValue, 'syncErrors', {}));
        const textPartner = partnerName ? `and ${partnerName}` : '';
        return (
            <Fragment>
                <FulfilledLine loadingPercent={40} lineColor="#6E0A7B" />
                <div className="container dis-f mt-5rem">
                    <div className="col span-8">
                        <h2 className="fw-b mt-xlarge mb-mlarge">
                            Your insurance policies
                        </h2>
                        <div className="dis-f input-wrap span-8">
                            <Field
                                component={YesNo}
                                name="has_insurance"
                                componentClass="span-8"
                                label={`Do you ${textPartner} have any insurance policies in place? e.g. life cover or critical illness`}
                                yesChecked={get(formValue, 'values.has_insurance', null) === true}
                                noChecked={get(formValue, 'values.has_insurance', null) === false}
                                isShowErrors={isSubmitted}
                                normalize={normalizeBoolean}
                                validate={[
                                    booleanRequired,
                                ]}
                            />
                        </div>
                        {this.renderExistInsurance()}
                    </div>
                    <div className="col span-3 col-last explain_text mt-4rem">
                        <ModalInformationIcon
                            title="Why we’re asking this"
                            text="It's essential to have the right insurance protection in place to safeguard your long-term financial wellbeing and limit the damage unexpected events can cause."
                        />
                    </div>
                </div>
                <BackNextButtons
                    isActiveBack={false}
                    onClickNext={handleSubmit}
                    isActiveNext={!isSubmitted || (isAnySyncErrors && isSubmitted)}
                    onClickBack={handleBack}
                    extraClass="span-8"
                    buttonClass="btn-purple"
                />
            </Fragment>
        );
    }
}

AddInsurance.propTypes = {
    formValue: PropTypes.object,
    handleSubmit: PropTypes.func.isRequired,
    isSubmitted: PropTypes.bool.isRequired,
    handleBack: PropTypes.func.isRequired,
    handleGetMePlan: PropTypes.func.isRequired,
    partnerName: PropTypes.string,
};

AddInsurance.defaultProps = {
    formValue: {},
    partnerName: '',
};

const mapStateToProps = state => ({
    initialValues: {
        update: true,
        planId: get(state, 'plan.plan_uuid', null),
        has_insurance: get(state, 'plan.has_insurance', null),
        insurance_additional: get(state, 'plan.insurance_additional', null),
        nextStage: '/survey/risk-tolerance/add-risk-tolerance',
        prevStage: '/survey/dashboard',
    },
    usersOptions: get(state, 'plan.users', {}),
    partner: get(state, 'plan.users.partner', {}),
    assets: get(state, 'plan.assets', []),
    partnerName: get(state, 'plan.users.partner.profile.first_name', ''),
});

const mapDispatchToProps = dispatch => ({
    changeFieldValue: (field, value) => {
        dispatch(change(AddInsurance.formName, field, value));
    },
});

const AddInsuranceWithHoc = SurveyHoc(AddInsurance, AddInsurance.surveyData);

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(AddInsuranceWithHoc));
