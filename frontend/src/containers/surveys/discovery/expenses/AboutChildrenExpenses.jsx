import React, { Component, Fragment } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { withRouter } from 'react-router-dom';
import get from 'lodash/get';
import { change } from 'redux-form';
import { bindActionCreators } from 'redux';
import { BackNextButtons } from '../../../../components/BackNextButtons';
import FulfilledLine from '../../../../components/FulfilledLine';
import FinanceAboutTable from '../../../../components/FinanceAboutTable';
import { getMePlan } from '../../../../actions/UserActions';
import { getPersonAge } from '../../../../utils/helper';

/**
 * Render the content for user idle. Dumb component
 *
 */
class AboutChildrenExpenses extends Component {
    static formName = 'aboutChildrenExpenses';

    constructor(props) {
        super(props);

        this.getListSavings = this.getListSavings.bind(this);
        this.handleBack = this.handleBack.bind(this);
        this.handleNext = this.handleNext.bind(this);

        const {
            handleGetMePlan,
        } = this.props;
        handleGetMePlan();
    }

    getListSavings = () => {
        const { users, childs, cashflows } = this.props;
        let arrayList = [];
        Object.keys(users)
            .map((userItem) => {
                if (userItem === 'child') {
                    arrayList = [];
                    users[userItem].map((subitem) => {
                        const editSection = childs.find(item => (item.profile_uuid === subitem.profile_uuid));
                        const id = childs.indexOf(editSection);
                        let educChasflow = [];
                        let childcareChasflow = [];
                        if (typeof editSection !== 'undefined') {
                            educChasflow = cashflows.find((item) => {
                                if (item.profile_uuid === subitem.profile_uuid && item.user_cashflow_type === 'education') {
                                    return true;
                                }
                                return false;
                            });
                            childcareChasflow = cashflows.find((item) => {
                                if (item.profile_uuid === subitem.profile_uuid && item.user_cashflow_type === 'childcare') {
                                    return true;
                                }
                                return false;
                            });
                        }
                        let addNew = false;
                        let textButton = 'Edit';
                        let eduFrequency;
                        let childFrequency;
                        if (typeof educChasflow !== 'undefined') {
                            eduFrequency = educChasflow.frequency === 'per_month' ? 'p.m.' : 'p.a.';
                        }
                        if (typeof childcareChasflow !== 'undefined') {
                            childFrequency = childcareChasflow.frequency === 'per_month' ? 'p.m.' : 'per term';
                            childFrequency = childcareChasflow.frequency === 'per_year' ? 'p.a.' : childFrequency;
                        }
                        let educationCost = typeof educChasflow !== 'undefined' ? `£ ${educChasflow.amount.toLocaleString('en')} ${eduFrequency}` : null;
                        let childcareCost = typeof childcareChasflow !== 'undefined' ? `£ ${childcareChasflow.amount.toLocaleString('en')} ${childFrequency}` : null;
                        if (educationCost === null && childcareCost === null) {
                            addNew = true;
                            textButton = '+ Add childcare costs';
                        } else {
                            educationCost = educationCost !== null ? educationCost : 0;
                            childcareCost = childcareCost !== null ? childcareCost : 0;
                        }
                        let hideAccordion = true;
                        if (childs[id].cashflow_changes) {
                            hideAccordion = false;
                        }
                        arrayList.push({
                            edit_link: `/survey/expenses/add-children-expenses/${subitem.profile_uuid}`,
                            childcare_expense: childcareCost,
                            education_expense: educationCost,
                            extra_information: childs[id].cashflow_changes,
                            addNew,
                            hideAccordion,
                            buttonText: textButton,
                            icon: 'user.svg',
                            subtitle: getPersonAge(subitem.profile.dob),
                            title: `${subitem.profile.first_name} ${subitem.profile.last_name}`,
                        });
                        return null;
                    });
                }
                return null;
            });
        return arrayList;
    };

    handleBack() {
        const { history } = this.props;
        history.push('/survey/dashboard');
    }

    handleNext() {
        const { history } = this.props;
        history.push('/survey/expenses/add-expenses');
    }

    render() {
        return (
            <Fragment>
                <FulfilledLine loadingPercent={40} lineColor="#6E0A7B" />
                <div className="container dis-f mt-5rem">
                    <div className="col span-8">
                        <h2 className="fw-b mt-xlarge mb-mlarge">
                            Looking after your children
                        </h2>
                        <h5 className="mb-2rem">
                            How much do you spend on your kids?
                        </h5>

                        <FinanceAboutTable data={this.getListSavings()} section="cash" />

                    </div>
                    <div className="col span-3 col-last explain_text mt-4rem" />
                </div>
                <BackNextButtons
                    isActiveBack={false}
                    onClickNext={this.handleNext}
                    onClickBack={this.handleBack}
                    extraClass="span-8"
                    buttonClass="btn-purple"
                />
            </Fragment>
        );
    }
}

AboutChildrenExpenses.propTypes = {
    history: PropTypes.object.isRequired,
    users: PropTypes.object,
    handleGetMePlan: PropTypes.func,
    cashflows: PropTypes.arrayOf(PropTypes.object),
    childs: PropTypes.arrayOf(PropTypes.object),
};

AboutChildrenExpenses.defaultProps = {
    handleGetMePlan: () => {},
    users: {},
    cashflows: [],
    childs: [],
};

const mapStateToProps = state => ({
    initialValues: {},
    users: get(state, 'plan.users', {}),
    childs: get(state, 'plan.users.child', []),
    debts: get(state, 'plan.debts', []),
    cashflows: get(state, 'plan.cashflows', []),
    addresses: get(state, 'plan.addresses', []),
});

const mapDispatchToProps = dispatch => ({
    changeFieldValue: (field, value) => {
        dispatch(change(AboutChildrenExpenses.formName, field, value));
    },
    handleGetMePlan: bindActionCreators(getMePlan, dispatch),
});

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(AboutChildrenExpenses));
