import React, { Component, Fragment } from 'react';
import { change, Field } from 'redux-form';
import get from 'lodash/get';
import isEmpty from 'lodash/isEmpty';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { withRouter } from 'react-router-dom';
import moment from 'moment';

import { BackNextButtons } from '../../../../components/BackNextButtons';
import FulfilledLine from '../../../../components/FulfilledLine';

import { InputPound } from '../../../../components/InputPound';
import {
    booleanRequired,
    isNumber, positiveNumber, required, requiredMoney,
} from '../../../../utils/validation.helper';
import YesNo from '../../../../components/YesNo';
import InputCustom from '../../../../components/InputCustom';
import SurveyHoc from '../../../hoc/SurveyHoc';
import { normalizeBoolean } from '../../../../utils/helper';
import { patchPlanCashflow } from '../../../../actions/PlanActions';
import Select from '../../../../components/Select';
import { PERIODS } from '../../../../helpers/constants';

/**
 * Render the content for user idle. Dumb component
 *
 */
class AddChildrenUnderageExpenses extends Component {
    static formName = 'addExpensesChildrenUnderAge';

    static surveyData = {
        formName: AddChildrenUnderageExpenses.formName,
        nextStage: 'nextStage',
        prevStage: 'prevStage',
        data: [
            {
                fields: [
                    'cashflows_plan',
                    'family_has_childcare',
                    'family_has_nanny',
                ],
                selector: 'profile_id',
                isUpdate: 'update',
                // additionalFields: [],
                methodUpdate: patchPlanCashflow,
            },
        ],
    };

    constructor(props) {
        super(props);

        this.renderHasNanny = this.renderHasNanny.bind(this);

        const {
            handleGetMePlan,
        } = this.props;

        handleGetMePlan();
    }

    componentWillUpdate() {
        const {
            formValue, changeFieldValue, cashflows, usersOptions, history,
        } = this.props;
        if (typeof usersOptions.child !== 'undefined') {
            let underage = false;
            usersOptions.child.map((item) => {
                const age = (moment()
                    .diff(item.profile.dob, 'years'));
                if (age < 16) {
                    underage = true;
                }
                return null;
            });
            if (underage === false) {
                history.push(AddChildrenUnderageExpenses.surveyData.nextStage);
            }
        }
        const planUserCashflow = [];
        if (get(formValue, 'values.family_has_nanny', null) === true) {
            const cashflowNanny = cashflows.find(item => item.user_cashflow_type === 'nanny');
            planUserCashflow.push({
                user_cashflow_type: 'nanny',
                amount: get(formValue, 'values.family_has_nanny_amount', 0),
                value_type: 'F',
                duration: get(formValue, 'values.family_has_nanny_year_remain', 0),
                frequency: get(formValue, 'values.family_has_nanny_frequency', 'per_month'),
                plan_cashflow_id: typeof cashflowNanny !== 'undefined' ? cashflowNanny.plan_cashflow_uuid : null,
            });
        }
        if (get(formValue, 'values.family_has_nanny', null) === true || get(formValue, 'values.family_has_childcare', null) === true) {
            changeFieldValue('cashflows_plan', planUserCashflow);
        }
    }

    renderHasNanny() {
        const { formValue, isSubmitted } = this.props;
        if (get(formValue, 'values.family_has_nanny', null) === true) {
            return (
                <Fragment>
                    <label className="input-label" htmlFor="family_has_nanny_amount">How much do you spend on your nanny/childminder?</label>
                    <div className="dis-f">
                        <Field
                            id="family_has_nanny_amount"
                            name="family_has_nanny_amount"
                            type="text"
                            component={InputPound}
                            isShowErrors={isSubmitted}
                            extraClasses="col span-4"
                            validate={[
                                requiredMoney,
                                isNumber,
                                positiveNumber,
                            ]}
                        />
                        <Field
                            name="family_has_nanny_frequency"
                            type="text"
                            extraClasses="col span-2"
                            hideLabel
                            component={Select}
                            options={PERIODS}
                            label=""
                        />
                    </div>
                    <div className="dis-f">
                        <Field
                            name="family_has_nanny_year_remain"
                            type="text"
                            component={InputCustom}
                            isShowErrors={isSubmitted}
                            extraClasses="col span-4"
                            hintMessage="Enter 0 if less than a year"
                            label="For how many years do you expect to employ a nanny/childminder?"
                            validate={[
                                required,
                                isNumber,
                                positiveNumber,
                            ]}
                        />
                    </div>
                </Fragment>
            );
        }
        return null;
    }

    render() {
        const {
            formValue,
            isSubmitted,
            handleSubmit,
            handleBack,
        } = this.props;
        const isAnySyncErrors = isEmpty(get(formValue, 'syncErrors', {}));

        return (
            <Fragment>
                <FulfilledLine loadingPercent={40} lineColor="#6E0A7B" />
                <div className="container dis-f mt-5rem mb-5rem">
                    <div className="col span-8">
                        <h2 className="fw-b mt-xlarge mb-mlarge">
                            Looking after your children
                        </h2>

                        <div className="dis-f input-wrap span-8">
                            <Field
                                component={YesNo}
                                name="family_has_nanny"
                                label="Do you employ a nanny/childminder?"
                                yesChecked={get(formValue, 'values.family_has_nanny', null) === true}
                                noChecked={get(formValue, 'values.family_has_nanny', null) === false}
                                isShowErrors={isSubmitted}
                                normalize={normalizeBoolean}
                                validate={[
                                    booleanRequired,
                                ]}
                            />
                        </div>
                        {this.renderHasNanny()}
                    </div>
                    <div className="col span-3 col-last explain_text mt-4rem" />
                </div>
                <BackNextButtons
                    isActiveBack={false}
                    onClickNext={handleSubmit}
                    isActiveNext={!isSubmitted || (isAnySyncErrors && isSubmitted)}
                    onClickBack={handleBack}
                    extraClass="span-8"
                    buttonClass="btn-purple"
                />
            </Fragment>
        );
    }
}

AddChildrenUnderageExpenses.propTypes = {
    changeFieldValue: PropTypes.func.isRequired,
    formValue: PropTypes.object,
    handleSubmit: PropTypes.func.isRequired,
    isSubmitted: PropTypes.bool.isRequired,
    handleBack: PropTypes.func.isRequired,
    usersOptions: PropTypes.object,
    history: PropTypes.object.isRequired,
    handleGetMePlan: PropTypes.func.isRequired,
    cashflows: PropTypes.arrayOf(PropTypes.object),
};

AddChildrenUnderageExpenses.defaultProps = {
    formValue: {},
    usersOptions: {},
    cashflows: [],
};

const mapStateToProps = (state) => {
    const nannyCashflow = get(state, 'plan.cashflows', [])
        .find((item) => {
            if (item.user_cashflow_type === 'nanny' && item.plan_user_cashflow.profile_uuid === get(state, 'plan.users', []).main.profile_uuid) {
                return true;
            }
            return false;
        });
    const childcareCashflow = get(state, 'plan.cashflows', [])
        .find((item) => {
            if (item.user_cashflow_type === 'childcare' && item.plan_user_cashflow.profile_uuid === get(state, 'plan.users', []).main.profile_uuid) {
                return true;
            }
            return false;
        });
    let familyHasNannyAmount;
    let familyHasNannyFrequency;
    let familyHasNannyYearRemain;
    let familyHasChildcareAmount;
    let familyHasChildcareFrequency;
    let familyHasChildcareYearRemain;
    if (typeof nannyCashflow !== 'undefined') {
        familyHasNannyAmount = nannyCashflow.amount;
        familyHasNannyFrequency = nannyCashflow.frequency;
        familyHasNannyYearRemain = nannyCashflow.duration;
    }
    if (typeof childcareCashflow !== 'undefined') {
        familyHasChildcareAmount = childcareCashflow.amount;
        familyHasChildcareFrequency = childcareCashflow.frequency;
        familyHasChildcareYearRemain = childcareCashflow.duration;
    }
    return ({
        initialValues: {
            update: true,
            profile_id: get(state, 'plan.users.main.profile_uuid', ''),
            family_has_nanny: get(state, 'plan.family_has_nanny', null),
            family_has_nanny_amount: familyHasNannyAmount,
            family_has_nanny_frequency: familyHasNannyFrequency || 'per_month',
            family_has_nanny_year_remain: familyHasNannyYearRemain,
            family_has_childcare: get(state, 'plan.family_has_childcare', null),
            family_has_childcare_amount: familyHasChildcareAmount,
            family_has_childcare_frequency: familyHasChildcareFrequency || 'per_month',
            family_has_childcare_year_remain: familyHasChildcareYearRemain,
            nextStage: '/survey/expenses/about-children-expenses',
            prevStage: '/survey/dashboard',
        },
        cashflows: get(state, 'plan.cashflows', []),
        usersOptions: get(state, 'plan.users', {}),
        assets: get(state, 'plan.assets', []),
    });
};

const mapDispatchToProps = dispatch => ({
    changeFieldValue: (field, value) => {
        dispatch(change(AddChildrenUnderageExpenses.formName, field, value));
    },
});

const AddChildrenUnderageExpensesWithHoc = SurveyHoc(AddChildrenUnderageExpenses, AddChildrenUnderageExpenses.surveyData);

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(AddChildrenUnderageExpensesWithHoc));
