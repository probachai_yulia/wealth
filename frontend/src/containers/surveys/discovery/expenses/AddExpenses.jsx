import React, { Component, Fragment } from 'react';
import { change, Field } from 'redux-form';
import get from 'lodash/get';
import isEmpty from 'lodash/isEmpty';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { withRouter } from 'react-router-dom';

import { BackNextButtons } from '../../../../components/BackNextButtons';
import FulfilledLine from '../../../../components/FulfilledLine';

import { InputPound } from '../../../../components/InputPound';
import {
    booleanRequired,
    isNumber, positiveNumber, required, requiredMoney,
} from '../../../../utils/validation.helper';
import YesNo from '../../../../components/YesNo';
import InputCustom from '../../../../components/InputCustom';
import FreeAdditionalBox from '../../../../components/FreeAdditionalBox';
import SurveyHoc from '../../../hoc/SurveyHoc';
import { normalizeBoolean } from '../../../../utils/helper';
import { updateUserInPlanCashflow } from '../../../../actions/UserActions';

/**
 * Render the content for user idle. Dumb component
 *
 */
class AddExpenses extends Component {
    static formName = 'addExpenses';

    static surveyData = {
        formName: AddExpenses.formName,
        nextStage: 'nextStage',
        prevStage: 'prevStage',
        data: [
            {
                fields: [
                    'cashflows_plan',
                    'cashflow_changes',
                ],
                selector: 'profile_id',
                isUpdate: 'update',
                // additionalFields: [],
                methodUpdate: updateUserInPlanCashflow,
            },
        ],
    };

    constructor(props) {
        super(props);

        this.renderChangeNotably = this.renderChangeNotably.bind(this);

        const {
            handleGetMePlan,
        } = this.props;

        handleGetMePlan();
    }

    componentWillUpdate() {
        const { formValue, changeFieldValue } = this.props;
        const planCashflow = [{
            user_cashflow_type: 'expenses_total',
            amount: get(formValue, 'values.monthly_spend_amount', 0),
            value_type: 'F',
            duration: get(formValue, 'values.has_monthly_spend', null) === true ? get(formValue, 'values.monthly_spend_duration', 0) : 0,
            frequency: 'per_month',
            plan_cashflow_id: get(formValue, 'values.childcare_id', null),
        }];
        changeFieldValue('cashflows_plan', planCashflow);
    }

    renderChangeNotably() {
        const { formValue, isSubmitted } = this.props;
        if (get(formValue, 'values.has_monthly_spend', null) === true) {
            return (
                <Fragment>
                    <Field
                        component={InputCustom}
                        name="monthly_spend_duration"
                        label="What age will you be when this happens?"
                        placeholder="Your age"
                        extraClasses="span-6"
                        isShowErrors={isSubmitted}
                        validate={[
                            required,
                            isNumber,
                        ]}
                    />
                    <Field
                        component={FreeAdditionalBox}
                        label="Please describe"
                        placeholder="Explain brielfy..."
                        extraClasses="wealth-textarea mb-5rem"
                        name="cashflow_changes"
                        id="1"
                        cols={30}
                        rows={10}
                        isShowErrors={isSubmitted}
                        validate={[
                            required,
                        ]}
                    />
                </Fragment>
            );
        }
        return null;
    }

    render() {
        const {
            formValue,
            isSubmitted,
            handleSubmit,
            handleBack,
            partnerName,
        } = this.props;
        const isAnySyncErrors = isEmpty(get(formValue, 'syncErrors', {}));
        const textPartner = partnerName ? `and ${partnerName}` : '';
        return (
            <Fragment>
                <FulfilledLine loadingPercent={40} lineColor="#6E0A7B" />
                <div className="container dis-f mt-5rem mb-5rem">
                    <div className="col span-8">
                        <h2 className="fw-b mt-xlarge mb-mlarge">
                            Expenses
                        </h2>

                        <div className="dis-f input-wrap span-8">
                            <Field
                                label={`In approximate terms, how much do you ${textPartner} spend in total each month?`}
                                component={InputPound}
                                name="monthly_spend_amount"
                                isShowErrors={isSubmitted}
                                extraClasses="span-6"
                                validate={[
                                    requiredMoney,
                                    isNumber,
                                    positiveNumber,
                                ]}
                            />
                        </div>
                        <div className="hr span-8" />
                        <Field
                            component={YesNo}
                            name="has_monthly_spend"
                            label={`Do you ${textPartner} expect your expenditure to change notably in the next 5 years?`}
                            yesChecked={get(formValue, 'values.has_monthly_spend', null) === true}
                            noChecked={get(formValue, 'values.has_monthly_spend', null) === false}
                            componentClass="span-8"
                            isShowErrors={isSubmitted}
                            normalize={normalizeBoolean}
                            validate={[
                                booleanRequired,
                            ]}
                        />
                        {this.renderChangeNotably()}
                    </div>
                    <div className="col span-3 col-last explain_text mt-4rem" />
                </div>
                <BackNextButtons
                    isActiveBack={false}
                    onClickNext={handleSubmit}
                    isActiveNext={!isSubmitted || (isAnySyncErrors && isSubmitted)}
                    onClickBack={handleBack}
                    extraClass="span-8"
                    buttonClass="btn-purple"
                />
            </Fragment>
        );
    }
}

AddExpenses.propTypes = {
    changeFieldValue: PropTypes.func.isRequired,
    formValue: PropTypes.object,
    handleSubmit: PropTypes.func.isRequired,
    isSubmitted: PropTypes.bool.isRequired,
    handleBack: PropTypes.func.isRequired,
    handleGetMePlan: PropTypes.func.isRequired,
    partnerName: PropTypes.string,
};

AddExpenses.defaultProps = {
    formValue: {},
    partnerName: '',
};

const mapStateToProps = (state) => {
    const cashflows = get(state, 'plan.cashflows', []);
    const expenseCashflow = cashflows.find((item) => {
        if (item.user_cashflow_type === 'expenses_total' && item.plan_user_cashflow.profile_uuid === get(state, 'plan.users.main.profile_uuid', '')) {
            return true;
        }
        return false;
    });
    const id = cashflows.indexOf(expenseCashflow);
    const nestHasMothlySpend = get(state, `plan.cashflows[${id}].duration`, null) > 0 ? true : null;
    return ({
        initialValues: {
            update: true,
            monthly_spend_duration: get(state, `plan.cashflows[${id}].duration`, null),
            monthly_spend_amount: get(state, `plan.cashflows[${id}].amount`, null),
            has_monthly_spend: get(state, `plan.cashflows[${id}].duration`, null) === 0 ? false : nestHasMothlySpend,
            childcare_id: get(state, `plan.cashflows[${id}].plan_cashflow_uuid`, null),
            cashflow_changes: get(state, 'plan.users.main.cashflow_changes', []),
            profile_id: get(state, 'plan.users.main.profile_uuid', ''),
            nextStage: '/survey/expenses/add-insurance',
            prevStage: '/survey/dashboard',
        },
        usersOptions: get(state, 'plan.users', {}),
        assets: get(state, 'plan.assets', []),
        partnerName: get(state, 'plan.users.partner.profile.first_name', ''),
    });
};

const mapDispatchToProps = dispatch => ({
    changeFieldValue: (field, value) => {
        dispatch(change(AddExpenses.formName, field, value));
    },
});

const AddExpensesWithHoc = SurveyHoc(AddExpenses, AddExpenses.surveyData);

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(AddExpensesWithHoc));
