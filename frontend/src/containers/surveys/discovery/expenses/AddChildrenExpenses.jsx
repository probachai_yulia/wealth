import React, { Component, Fragment } from 'react';
import { change, Field } from 'redux-form';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import get from 'lodash/get';
import PropTypes from 'prop-types';
import isEmpty from 'lodash/isEmpty';
import moment from 'moment';
import { isNumber, positiveNumber, requiredMoney } from '../../../../utils/validation.helper';
import { PERIODS, PERIODS_TERM } from '../../../../helpers/constants';
import Select from '../../../../components/Select';
import { CancelSaveHeader } from '../../../../components/CancelSaveHeader';
import { InputPound } from '../../../../components/InputPound';
import AnythingElse from '../../../../components/AnythingElse';
import SurveyHoc from '../../../hoc/SurveyHoc';
import { BackNextButtons } from '../../../../components/BackNextButtons';
import { updateUserInPlanCashflow } from '../../../../actions/UserActions';

/**
 * Render the content for user idle. Dumb component
 *
 */
class AddChildrenExpenses extends Component {
    static formName = 'addExpensesChildren';

    static surveyData = {
        formName: AddChildrenExpenses.formName,
        nextStage: 'nextStage',
        nextStageNoChild: '/survey/expenses/add-expenses',
        prevStage: 'prevStage',
        data: [
            {
                fields: [
                    'cashflows_plan',
                    'cashflow_changes',
                ],
                selector: 'profile_id',
                isUpdate: 'update',
                // additionalFields: [],
                methodUpdate: updateUserInPlanCashflow,
            },
        ],
    };

    constructor(props) {
        super(props);

        this.renderChildcare = this.renderChildcare.bind(this);
        this.renderEducation = this.renderEducation.bind(this);

        const {
            handleGetMePlan,
        } = this.props;

        handleGetMePlan();
    }

    componentWillUpdate() {
        const {
            formValue, changeFieldValue, age, usersOptions, history,
        } = this.props;
        const planCashflow = [];
        if (age < 18) {
            planCashflow.push({
                user_cashflow_type: 'childcare',
                amount: get(formValue, 'values.childcare_amount', 0),
                value_type: 'F',
                frequency: get(formValue, 'values.childcare_frequency', 'per_month'),
                plan_cashflow_id: get(formValue, 'values.childcare_id', null),
            });
        }
        if (age > 4 && age < 18) {
            planCashflow.push({
                user_cashflow_type: 'education',
                amount: get(formValue, 'values.education_amount', 0),
                value_type: 'F',
                frequency: get(formValue, 'values.education_frequency', 'per_month'),
                plan_cashflow_id: get(formValue, 'values.education_id', null),
            });
        }
        changeFieldValue('cashflows_plan', planCashflow);
        if (typeof usersOptions.child !== 'undefined') {
            let underage = false;
            usersOptions.child.map((item) => {
                const curAge = (moment().diff(item.profile.dob, 'years'));
                if (curAge < 18) {
                    underage = true;
                }
                return null;
            });
            if (underage === false) {
                history.push(AddChildrenExpenses.surveyData.nextStageNoChild);
            }
        }
    }

    renderChildcare() {
        const {
            isSubmitted,
            age,
            firstName,
        } = this.props;
        if (age < 16) {
            return (
                <Fragment>
                    <label className="input-label">
                        {`Excluding nanny costs, how much do you spend on childcare for ${firstName}?`}
                    </label>
                    <div className="dis-f col">
                        <Field
                            name="childcare_amount"
                            type="text"
                            component={InputPound}
                            extraClasses="col span-4"
                            isShowErrors={isSubmitted}
                            validate={[
                                requiredMoney,
                                isNumber,
                                positiveNumber,
                            ]}
                        />
                        <Field
                            name="childcare_frequency"
                            type="text"
                            extraClasses="col span-2"
                            hideLabel
                            component={Select}
                            options={PERIODS}
                            label=""
                        />
                    </div>
                </Fragment>
            );
        }
        return null;
    }

    renderEducation() {
        const {
            isSubmitted,
            age,
            firstName,
        } = this.props;
        if (age > 4 && age < 18) {
            return (
                <Fragment>
                    <label className="input-label">
                        {`How much do you spend on ${firstName}&apos;s education?`}
                    </label>
                    <div className="dis-f">
                        <Field
                            name="education_amount"
                            type="text"
                            is
                            component={InputPound}
                            extraClasses="col span-4"
                            isShowErrors={isSubmitted}
                            validate={[
                                requiredMoney,
                                isNumber,
                                positiveNumber,
                            ]}
                        />
                        <Field
                            name="education_frequency"
                            type="text"
                            extraClasses="col span-2"
                            component={Select}
                            hideLabel
                            options={PERIODS_TERM}
                        />
                    </div>
                </Fragment>
            );
        }
        return null;
    }

    render() {
        const {
            isSubmitted,
            handleSubmit,
            handleBack,
            formValue,
        } = this.props;
        const isAnySyncErrors = isEmpty(get(formValue, 'syncErrors', {}));
        return (
            <Fragment>
                <CancelSaveHeader title="Add childcare costs" />
                <div className="mt-fix-action-header container dis-f fd-c mt-xlarge">
                    <form className="know-you-form col span-8 dis-f fd-c mt-xlarge">
                        {this.renderChildcare()}
                        {this.renderEducation()}
                        <AnythingElse
                            name="cashflow_changes"
                            placeholder="E.g. upcoming costs like gap years or supporting particular hobbies"
                        />
                    </form>
                </div>
                <BackNextButtons
                    isActiveBack={false}
                    onClickNext={handleSubmit}
                    isActiveNext={!isSubmitted || (isAnySyncErrors && isSubmitted)}
                    onClickBack={handleBack}
                    buttonClass="btn-purple"
                    nextText="Save"
                />
            </Fragment>
        );
    }
}

AddChildrenExpenses.propTypes = {
    age: PropTypes.number,
    firstName: PropTypes.string,
    changeFieldValue: PropTypes.func.isRequired,
    history: PropTypes.object.isRequired,
    formValue: PropTypes.object,
    handleSubmit: PropTypes.func.isRequired,
    isSubmitted: PropTypes.bool.isRequired,
    handleBack: PropTypes.func.isRequired,
    usersOptions: PropTypes.object,
    handleGetMePlan: PropTypes.func.isRequired,
};

AddChildrenExpenses.defaultProps = {
    formValue: {},
    usersOptions: {},
    age: 0,
    firstName: '',
};

const mapStateToProps = (state, ownProps) => {
    const editSection = get(state, 'plan.users.child', [])
        .find(item => (item.profile_uuid === ownProps.match.params.child_uuid));
    const id = get(state, 'plan.users.child', [])
        .indexOf(editSection);
    const age = moment().diff(get(state, `plan.users.child[${id}].profile.dob`, 0), 'years');
    let educChasflow = [];
    let childcareChasflow = [];
    if (typeof editSection !== 'undefined') {
        childcareChasflow = get(state, 'plan.cashflows', [])
            .find((item) => {
                if (item.profile_uuid === ownProps.match.params.child_uuid && item.user_cashflow_type === 'childcare') {
                    return true;
                }
                return false;
            });
        educChasflow = get(state, 'plan.cashflows', [])
            .find((item) => {
                if (item.profile_uuid === ownProps.match.params.child_uuid && item.user_cashflow_type === 'education') {
                    return true;
                }
                return false;
            });
    }
    return ({
        initialValues: {
            update: true,
            profile_id: get(state, `plan.users.child[${id}].profile_uuid`, ''),
            cashflow_changes: get(state, `plan.users.child[${id}].cashflow_changes`, null),
            childcare_amount: typeof childcareChasflow !== 'undefined' ? childcareChasflow.amount : null,
            childcare_frequency: typeof childcareChasflow !== 'undefined' ? childcareChasflow.frequency : 'per_month',
            childcare_id: typeof childcareChasflow !== 'undefined' ? childcareChasflow.plan_cashflow_uuid : null,
            education_amount: typeof educChasflow !== 'undefined' ? educChasflow.amount : null,
            education_frequency: typeof educChasflow !== 'undefined' ? educChasflow.frequency : 'per_month',
            education_id: typeof educChasflow !== 'undefined' ? educChasflow.plan_cashflow_uuid : null,
            nextStage: '/survey/expenses/about-children-expenses',
            nextStageNoChild: '/survey/expenses/add-expenses',
        },
        firstName: `${get(state, `plan.users.child[${id}].profile.first_name`, '')}`,
        assets: get(state, 'plan.assets', []),
        age,
    });
};

const mapDispatchToProps = dispatch => ({
    changeFieldValue: (field, value) => {
        dispatch(change(AddChildrenExpenses.formName, field, value));
    },
});

const AddChildrenExpensesWithHoc = SurveyHoc(AddChildrenExpenses, AddChildrenExpenses.surveyData);

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(AddChildrenExpensesWithHoc));
