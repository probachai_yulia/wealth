/* eslint-disable max-len */
import React, { Component, Fragment } from 'react';
import { change, Field } from 'redux-form';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import get from 'lodash/get';
import isEmpty from 'lodash/isEmpty';
import PropTypes from 'prop-types';
import { BackNextButtons } from '../../../../components/BackNextButtons';
import FulfilledLine from '../../../../components/FulfilledLine';
import YesNo from '../../../../components/YesNo';
import MultipleMoneyWhomBlock from '../../../../components/MultipleMoneyWhomBlock';
import SurveyHoc from '../../../hoc/SurveyHoc';
import { patchPlan, addPlanGiving } from '../../../../actions/PlanActions';
import { normalizeBoolean } from '../../../../utils/helper';
import ModalInformationIcon from '../../../../components/ModalInformationIcon';

/**
 * Render the content for user idle. Dumb component
 *
 */
class AddCharity extends Component {
    static formName = 'addCharity';

    static surveyData = {
        formName: AddCharity.formName,
        nextStage: 'nextStage',
        prevStage: 'prevStage',
        data: [
            {
                fields: [
                    'plan_has_charity',
                    'plan_has_gifting',
                ],
                selector: 'planId',
                isUpdate: 'updatePlan',
                methodCreate: patchPlan,
                methodUpdate: patchPlan,
            },
            {
                fields: [
                    'charity_givings_plan',
                    'isUpdate',
                ],
                isUpdate: 'isUpdate',
                selector: 'planId',
                methodCreate: addPlanGiving,
                methodUpdate: addPlanGiving,
            },
        ],
    };

    constructor(props) {
        super(props);

        this.state = {
            errorMessage: false,
        };

        this.renderTextPartnerCharity = this.renderTextPartnerCharity.bind(this);
        this.renderTextRegularGifting = this.renderTextRegularGifting.bind(this);
        this.renderBlockPartnerCharity = this.renderBlockPartnerCharity.bind(this);
        this.renderBlockRegularGifting = this.renderBlockRegularGifting.bind(this);
        this.preHandleSubmit = this.preHandleSubmit.bind(this);

        const {
            handleGetMePlan,
        } = this.props;

        handleGetMePlan();
    }

    componentWillUpdate() {
        const { formValue, changeFieldValue } = this.props;
        const charity = get(formValue, 'values.plan_has_charity', null);
        const gifting = get(formValue, 'values.plan_has_gifting', null);
        const arrayCharity = [];
        if (charity === true) {
            const charityContribution = get(formValue, 'values.charity_spend', []);
            charityContribution.map((item) => {
                arrayCharity.push({
                    amount: item.amount,
                    frequency: item.frequency,
                    value_type: item.value_type,
                    other_income_type: item.whom,
                    giving_type: 'charity',
                    plan_cashflow_id: item.plan_cashflow_id,
                    profile_uuid: item.profile_uuid,
                });
                return null;
            });
        }
        if (gifting === true) {
            const charityContribution = get(formValue, 'values.gifting_spend', []);
            charityContribution.map((item) => {
                arrayCharity.push({
                    amount: item.amount,
                    frequency: item.frequency,
                    value_type: item.value_type,
                    other_income_type: item.whom,
                    giving_type: 'gifting',
                    plan_cashflow_id: item.plan_cashflow_id,
                    profile_uuid: item.profile_uuid,
                });
                return null;
            });
        }
        changeFieldValue('charity_givings_plan', arrayCharity);
    }

    preHandleSubmit(e) {
        e.preventDefault();
        const {
            handleSubmit, formValue,
        } = this.props;
        const charity = get(formValue, 'values.plan_has_charity', null);
        const gifting = get(formValue, 'values.plan_has_gifting', null);
        if (charity !== null && gifting !== null) {
            handleSubmit();
        } else {
            this.setState({
                errorMessage: true,
            });
        }
    }

    renderTextPartnerCharity() {
        const { formValue } = this.props;
        const charity = get(formValue, 'values.plan_has_charity', null);
        if (charity === true) {
            return (
                <ModalInformationIcon
                    title="The gift that keeps on giving"
                    text="If you give to charity using gift aid, your favourite charity will receive a 20% top-up from HMRC. You may also be able to claim back 25% depending on your tax rate."
                />
            );
        }
        return null;
    }

    renderTextRegularGifting = () => {
        const { formValue } = this.props;
        const gifting = get(formValue, 'values.plan_has_gifting', null);
        if (gifting === true) {
            return (
                <ModalInformationIcon
                    title="Giving to your loved ones"
                    text="Inheritance tax is not so much a problem for you as it might be for your heirs. We can help you look at smart strategies that can help you avoid immediate and delayed tax charges, and ensure your affairs are left the way you want them to be."
                />
            );
        }
        return null;
    };

    renderBlockPartnerCharity = () => {
        const { formValue, isSubmitted } = this.props;
        const charity = get(formValue, 'values.plan_has_charity', null);
        if (charity === true) {
            return (
                <MultipleMoneyWhomBlock
                    labelPound="How much?"
                    labelWhom="Who do you give to?"
                    name="charity_spend"
                    hideLabel
                    poundFieldName="childcareSpend"
                    selectFieldName="childcareSpendPeriod"
                    extraClasses="mt-2rem"
                    isShowErrors={isSubmitted}
                />
            );
        }
        return null;
    };

    renderBlockRegularGifting = () => {
        const { formValue, isSubmitted } = this.props;
        const gifting = get(formValue, 'values.plan_has_gifting', null);
        if (gifting === true) {
            return (
                <MultipleMoneyWhomBlock
                    labelPound="How much?"
                    name="gifting_spend"
                    labelWhom="To whom?"
                    hideLabel
                    poundFieldName="childcareSpend"
                    selectFieldName="childcareSpendPeriod"
                    extraClasses="mt-2rem"
                    isShowErrors={isSubmitted}
                />
            );
        }
        return null;
    };

    render() {
        const {
            formValue,
            isSubmitted,
            handleBack,
            partnerName,
        } = this.props;
        const {
            errorMessage,
        } = this.state;
        const {
            preHandleSubmit,
        } = this;
        const isAnySyncErrors = isEmpty(get(formValue, 'syncErrors', {}));
        const textPartner = partnerName ? `and ${partnerName}` : '';
        return (
            <Fragment>
                <FulfilledLine loadingPercent={40} lineColor="#6E0A7B" />
                <div className="container dis-f mt-5rem mb-5rem">
                    <div className="col span-8">
                        <h2 className="fw-b mt-xlarge mb-mlarge">
                            Charity and gifting
                        </h2>
                        <h5 className="mb-2rem">
                            Charitable Giving
                        </h5>

                        <Field
                            component={YesNo}
                            // extraClasses="span-3"
                            // containerClass='span-6'
                            name="plan_has_charity"
                            label={`Do you ${textPartner} give to charity?`}
                            componentClass="span-8"
                            yesChecked={get(formValue, 'values.plan_has_charity', null) === true}
                            noChecked={get(formValue, 'values.plan_has_charity', null) === false}
                            isShowErrors={isSubmitted}
                            normalize={normalizeBoolean}
                        />
                        {this.renderBlockPartnerCharity()}
                        <div className="hr span-8" />

                        <h5 className="mb-2rem">
                            Gifting to friends and family
                        </h5>

                        <Field
                            component={YesNo}
                            // extraClasses="span-3"
                            // containerClass='span-6'
                            name="plan_has_gifting"
                            componentClass="span-8"
                            label={`Do you ${textPartner} make any other regular gifting?`}
                            yesChecked={get(formValue, 'values.plan_has_gifting', null) === true}
                            noChecked={get(formValue, 'values.plan_has_gifting', null) === false}
                            isShowErrors={isSubmitted}
                            normalize={normalizeBoolean}
                        />
                        {this.renderBlockRegularGifting()}
                        {
                            errorMessage && (
                                <div className="span-8">
                                    <div className="error-message dis-f">
                                        <img src="/static/img/error-icon.svg" alt="error-icon" />
                                        <p className="message mstart-0-5rem fz-sml">One field is required</p>
                                    </div>
                                </div>
                            )
                        }
                    </div>
                    <div className="col span-3 col-last explain_text mt-4rem">
                        {this.renderTextPartnerCharity()}
                        {this.renderTextRegularGifting()}
                    </div>
                </div>
                <BackNextButtons
                    isActiveBack={false}
                    onClickNext={preHandleSubmit}
                    isActiveNext={!isSubmitted || (isAnySyncErrors && isSubmitted)}
                    onClickBack={handleBack}
                    buttonClass="btn-purple"
                    extraClass="span-8"
                />
            </Fragment>
        );
    }
}

AddCharity.propTypes = {
    changeFieldValue: PropTypes.func.isRequired,
    formValue: PropTypes.object,
    handleSubmit: PropTypes.func.isRequired,
    isSubmitted: PropTypes.bool.isRequired,
    handleBack: PropTypes.func.isRequired,
    handleGetMePlan: PropTypes.func.isRequired,
    partnerName: PropTypes.string,
};

AddCharity.defaultProps = {
    formValue: {},
    partnerName: '',
};

const mapStateToProps = (state) => {
    const givings = get(state, 'plan.givings', []);
    const cashflow = get(state, 'plan.cashflows', []);
    const gifting = givings.filter(item => item.giving_type === 'gifting');
    const charity = givings.filter(item => item.giving_type === 'charity');
    let giftingAmount = []; let
        charityAmount = [];
    if (gifting.length === 0) {
        giftingAmount = [{
            amount: null,
            frequency: 'per_month',
            whom: '',
            plan_cashflow_id: null,
            plan_giving_id: null,
            profile_uuid: null,
            value_type: 'F',
        }];
    }
    gifting.map((item) => {
        const cashflowGifting = cashflow.find(subitem => subitem.plan_cashflow_uuid === item.plan_cashflow_uuid);
        if (typeof cashflowGifting !== 'undefined') {
            giftingAmount.push({
                amount: cashflowGifting.amount,
                frequency: cashflowGifting.frequency,
                whom: cashflowGifting.other_income_type,
                plan_cashflow_id: cashflowGifting.plan_cashflow_uuid,
                plan_giving_id: cashflowGifting.plan_giving_uuid,
                profile_uuid: cashflowGifting.profile_uuid,
                value_type: cashflowGifting.value_type,
            });
        }
        return null;
    });
    if (charity.length === 0) {
        charityAmount = [{
            amount: null,
            frequency: 'per_month',
            whom: '',
            plan_cashflow_id: null,
            plan_giving_id: null,
            profile_uuid: null,
            value_type: 'F',
        }];
    }
    charity.map((item) => {
        const cashflowCharity = cashflow.find(subitem => subitem.plan_cashflow_uuid === item.plan_cashflow_uuid);
        if (typeof cashflowCharity !== 'undefined') {
            charityAmount.push({
                amount: cashflowCharity.amount,
                frequency: cashflowCharity.frequency,
                whom: cashflowCharity.other_income_type,
                plan_cashflow_id: cashflowCharity.plan_cashflow_uuid,
                plan_giving_id: cashflowCharity.plan_giving_uuid,
                profile_uuid: cashflowCharity.profile_uuid,
                value_type: cashflowCharity.value_type,
            });
        }
        return null;
    });
    return ({
        initialValues: {
            isUpdate: false,
            plan_has_charity: get(state, 'plan.plan_has_charity', null),
            plan_has_gifting: get(state, 'plan.plan_has_gifting', null),
            planId: get(state, 'plan.plan_uuid', null),
            gifting_spend: giftingAmount,
            charity_spend: charityAmount,
            updatePlan: true,
            nextStage: '/survey/dashboard',
            prevStage: '/survey/dashboard',
        },
        partnerName: get(state, 'plan.users.partner.profile.first_name', ''),
        usersOptions: get(state, 'plan.users', {}),
        assets: get(state, 'plan.assets', []),
        givings: get(state, 'plan.givings', []),
        cashflow: get(state, 'plan.cashflow', []),
    });
};

const mapDispatchToProps = dispatch => ({
    changeFieldValue: (field, value) => {
        dispatch(change('addCharity', field, value));
    },
});

const AddCharityWithHoc = SurveyHoc(AddCharity, AddCharity.surveyData);

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(AddCharityWithHoc));
