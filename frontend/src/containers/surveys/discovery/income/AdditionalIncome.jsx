import React, { Component, Fragment } from 'react';
import { change, Field } from 'redux-form';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import PropTypes from 'prop-types';
import get from 'lodash/get';
import isEmpty from 'lodash/isEmpty';

import { bindActionCreators } from 'redux';
import { BackNextButtons } from '../../../../components/BackNextButtons';
import FulfilledLine from '../../../../components/FulfilledLine';
import AddDashedButton from '../../../../components/AddDashedButton';
import YesNo from '../../../../components/YesNo';
import { booleanRequired } from '../../../../utils/validation.helper';
import BoxUserText from '../../../../components/BoxUserText';
import SurveyHoc from '../../../hoc/SurveyHoc';
import { normalizeBoolean } from '../../../../utils/helper';
import { patchPlanCashflow } from '../../../../actions/PlanActions';
import { requestsReset } from '../../../../actions/CommonActions';


/**
 * Render the content for user idle. Dumb component
 *
 */
class AdditionalIncome extends Component {
    static formName = 'additionalIncome';

    static surveyData = {
        formName: AdditionalIncome.formName,
        nextStage: 'nextStage',
        prevStage: 'prevStage',
        data: [
            {
                fields: [
                    'plan_has_other_income',
                ],
                additionalFields: {
                    isLocalRequest: true,
                },
                isUpdate: 'update',
                methodCreate: patchPlanCashflow,
                methodUpdate: patchPlanCashflow,
            },
        ],
    };

    constructor(props) {
        super(props);

        this.renderAdditionalIncome = this.renderAdditionalIncome.bind(this);
        this.renderBoxAssets = this.renderBoxAssets.bind(this);
        this.handleRedirect = this.handleRedirect.bind(this);

        const {
            handleGetMePlan,
        } = this.props;

        handleGetMePlan();
    }

    componentDidUpdate() {
        const {
            isLocalRequestsDone, handleRequestsReset, history,
        } = this.props;
        if (isLocalRequestsDone === 1) {
            handleRequestsReset();
            history.push('/survey/income/add-additional-income');
        }
    }

    handleRedirect() {
        const {
            handleUpdatePlan,
            formValue,
        } = this.props;
        const data = {
            plan_has_other_income: get(formValue, 'values.plan_has_other_income', null),
            isLocalRequest: true,
        };
        handleUpdatePlan(data);
    }

    renderAdditionalIncome() {
        const { formValue } = this.props;
        const isHaveAdditionalIncome = get(formValue, 'values.plan_has_other_income', null) === true;
        if (isHaveAdditionalIncome) {
            return (
                <AddDashedButton
                    name="Additional income"
                    addName="+ Add additional income"
                    handleRedirect={this.handleRedirect}
                />
            );
        }
        return null;
    }

    renderBoxAssets() {
        const {
            assets, cashflows, usersOptions, addressesList, assetsList, taxBenefits,
        } = this.props;
        const assetList = [];
        assetsList.map((item) => {
            Object.keys(item).map((subitem) => {
                item[subitem].map((subsitem) => {
                    assetList.push(subsitem);
                    return null;
                });
                return null;
            });
            return null;
        });
        taxBenefits.map((item) => {
            assetList.push(item);
            return null;
        });
        return cashflows.map((cashItem, i) => {
            let currentAsset;
            let currentCashFlow;
            let owners = [];
            let selectName;
            let iconItem;
            let user;
            let extraInformation;
            let imgUser;
            let currentAssetName = { name: '' };
            assets.map((assetItem) => {
                const filteredCashflow = assetItem.cashflows.find(subitem => subitem.plan_cashflow_uuid === cashItem.plan_cashflow_uuid);
                if (typeof filteredCashflow !== 'undefined') {
                    currentAsset = assetItem;
                    currentCashFlow = filteredCashflow;
                }
                return null;
            });
            if (typeof currentAsset !== 'undefined') {
                ({
                    owners,
                } = currentAsset);
                extraInformation = currentCashFlow.additional;
                currentAssetName = assetList.find(subitem => subitem.code === currentAsset.asset_type || subitem.code === currentAsset.tax_benefit);
                if (currentAsset.asset_type === 'benefit' || currentAsset.asset_type === 'contribution' || currentAsset.asset_type === 'annuity') { // PENSIONS
                    selectName = `${currentAsset.name} - ${currentAsset.provider_name}`;
                    iconItem = 'pensions.svg';
                } else if (currentAsset.asset_type.indexOf('isa') !== -1) { // ISAs
                    selectName = `ISA - ${currentAsset.provider_name} ${typeof currentAssetName !== 'undefined' ? ` - ${currentAssetName.name}` : ''}`;
                    iconItem = 'ISAs.svg';
                } else if (currentAsset.asset_type.indexOf('cash') !== -1 && currentAsset.asset_type !== 'cash_other') { // CASH
                    selectName = `Cash - ${currentAsset.provider_name} ${typeof currentAssetName !== 'undefined' ? ` - ${currentAssetName.name}` : ''}`;
                    iconItem = 'cash_savings.svg';
                } else if (currentAsset.asset_type.indexOf('property') !== -1) { // PROPERTY
                    const cureAddress = addressesList.find(subitem => currentAsset.address_uuid === subitem.address_uuid);
                    selectName = `Property - ${cureAddress.address_1}`;
                    iconItem = 'properties.svg';
                } else {
                    selectName = `Investment - ${currentAsset.provider_name}`;
                    iconItem = 'investments.svg';
                }
            } else {
                owners[0] = cashItem.profile_uuid;
                selectName = cashItem.other_income_type;
                iconItem = 'money.svg';
            }
            user = { profile: { first_name: 'no owner' } };
            imgUser = 'user.svg';
            Object.keys(usersOptions).map((userItem) => {
                if ((userItem === 'main' || userItem === 'partner') && usersOptions[userItem] !== null) {
                    if (owners.length > 1) {
                        user = { profile: { first_name: 'Joint' } };
                        imgUser = 'joint.svg';
                    } else if (usersOptions[userItem].profile_uuid === owners[0]) {
                        user = usersOptions[userItem];
                        imgUser = 'user.svg';
                    }
                }
                return null;
            });
            const frequency = cashItem.frequency === 'per_month' ? 'p.m.' : 'p.a.';
            const amount = `£${cashItem.amount.toLocaleString('en')} ${frequency}`;
            return (
                <BoxUserText
                    key={i.toString()}
                    title={amount}
                    subtitle={selectName}
                    icon={iconItem}
                    clientName={user.profile.first_name}
                    clientImage={imgUser}
                    extraInformation={extraInformation}
                    linkTo={`/survey/income/add-additional-income/${cashItem.plan_cashflow_uuid}`}
                    btnText="Edit"
                />
            );
        });
    }

    render() {
        const {
            formValue,
            isSubmitted,
            handleSubmit,
            handleBack,
            firstName,
        } = this.props;
        const isAnySyncErrors = isEmpty(get(formValue, 'syncErrors', {}));
        const text = firstName ? `do you or ${firstName}` : 'do you';
        return (
            <Fragment>
                <FulfilledLine loadingPercent={40} lineColor="#6e0a7b" />
                <div className="container dis-f fd-c">
                    <h2 className="fw-b mt-xlarge mb-medium">
                        Do you have other sources of income?
                    </h2>
                    <h5 className=" fw-b mb-medium">
                        Tell us about any income sources other than salaries, for example rental income, annuities or dividends.
                    </h5>
                    <form className="know-you-form col span-8 dis-f fd-c">
                        <Field
                            component={YesNo}
                            label={`Besides your salary, ${text} receive any additional income?`}
                            name="plan_has_other_income"
                            componentClass="span-8"
                            yesChecked={get(formValue, 'values.plan_has_other_income', null) === true}
                            noChecked={get(formValue, 'values.plan_has_other_income', null) === false}
                            isShowErrors={isSubmitted}
                            normalize={normalizeBoolean}
                            validate={[
                                booleanRequired,
                            ]}
                        />
                        {this.renderBoxAssets()}
                        {this.renderAdditionalIncome()}
                    </form>
                </div>
                <BackNextButtons
                    isActiveBack={false}
                    backText="Cancel"
                    nextText="Save"
                    onClickNext={handleSubmit}
                    isActiveNext={!isSubmitted || (isAnySyncErrors && isSubmitted)}
                    onClickBack={handleBack}
                    buttonClass="btn-purple"
                    extraClass="span-8"
                />
            </Fragment>
        );
    }
}

AdditionalIncome.propTypes = {
    history: PropTypes.object.isRequired,
    formValue: PropTypes.object,
    handleSubmit: PropTypes.func.isRequired,
    isSubmitted: PropTypes.bool.isRequired,
    handleBack: PropTypes.func.isRequired,
    usersOptions: PropTypes.object,
    assets: PropTypes.arrayOf(PropTypes.object),
    addressesList: PropTypes.arrayOf(PropTypes.object),
    handleGetMePlan: PropTypes.func.isRequired,
    handleRequestsReset: PropTypes.func.isRequired,
    handleUpdatePlan: PropTypes.func.isRequired,
    cashflows: PropTypes.arrayOf(PropTypes.object),
    firstName: PropTypes.string,
    isLocalRequestsDone: PropTypes.number.isRequired,
    taxBenefits: PropTypes.arrayOf(PropTypes.object),
    assetsList: PropTypes.arrayOf(PropTypes.object),
};

AdditionalIncome.defaultProps = {
    formValue: {},
    usersOptions: {},
    addressesList: [],
    assets: [],
    cashflows: [],
    firstName: '',
    taxBenefits: [],
    assetsList: [],
};

const mapStateToProps = (state) => {
    const cashflows = get(state, 'plan.cashflows', []).filter(item => item.user_cashflow_type === 'assets' || item.user_cashflow_type === 'other') || [];
    return ({
        initialValues: {
            nextStage: '/survey/income/income-changes',
            prevStage: '/survey/dashboard',
            plan_has_other_income: get(state, 'plan.plan_has_other_income', null),
            update: true,
        },
        usersOptions: get(state, 'plan.users', {}),
        assets: get(state, 'plan.assets', []),
        addressesList: get(state, 'plan.addresses', []),
        cashflows,
        firstName: get(state, 'plan.users.partner.profile.first_name', null),
        isLocalRequestsDone: get(state, 'common.isLocalRequestsDone', 0),
        taxBenefits: get(state, 'options.tax_benefits', []),
        assetsList: get(state, 'options.asset_types', []),
    });
};

const mapDispatchToProps = dispatch => ({
    changeFieldValue: (field, value) => {
        dispatch(change(AdditionalIncome.formName, field, value));
    },
    handleUpdatePlan: bindActionCreators(patchPlanCashflow, dispatch),
    handleRequestsReset: bindActionCreators(requestsReset, dispatch),
});

const AdditionalIncomeWithHoc = SurveyHoc(AdditionalIncome, AdditionalIncome.surveyData);

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(AdditionalIncomeWithHoc));
