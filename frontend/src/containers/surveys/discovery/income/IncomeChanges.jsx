import React, { Component, Fragment } from 'react';
import { change, Field } from 'redux-form';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import PropTypes from 'prop-types';
import get from 'lodash/get';
import isEmpty from 'lodash/isEmpty';

import { BackNextButtons } from '../../../../components/BackNextButtons';
import FulfilledLine from '../../../../components/FulfilledLine';
import YesNoNotSure from '../../../../components/YesNoNotSure';
import FreeAdditionalBox from '../../../../components/FreeAdditionalBox';
import { required } from '../../../../utils/validation.helper';
import SurveyHoc from '../../../hoc/SurveyHoc';
import ModalInformationIcon from '../../../../components/ModalInformationIcon';
import { updateIncomeChanges } from '../../../../actions/PlanActions';

/**
 * Render the content for user idle. Dumb component
 *
 */
class IncomeChanges extends Component {
    static formName = 'incomeChanges';

    static surveyData = {
        formName: IncomeChanges.formName,
        nextStage: 'nextStage',
        prevStage: 'prevStage',
        data: [
            {
                fields: [
                    'other_income_user',
                ],
                additionalCreateFields: [
                    {
                        plan_user_type: 'main',
                        is_active: true,
                    },
                ],
                additionalUpdateFields: [
                    {
                        is_active: true,
                    },
                ],
                isUpdate: 'updateUser',
                selector: 'userId',
                methodUpdate: updateIncomeChanges,
            },
        ],
    };

    constructor(props) {
        super(props);

        this.renderPersonalIncomeChanges = this.renderPersonalIncomeChanges.bind(this);
        this.renderPartnerIncomeChanges = this.renderPartnerIncomeChanges.bind(this);

        const {
            handleGetMePlan,
        } = this.props;

        handleGetMePlan();
    }

    componentWillUpdate() {
        const {
            formValue, changeFieldValue, partnerFirstName, usersOptions,
        } = this.props;
        const isPersonalExpectIncomeChanges = get(formValue, 'values.personal-income-changes-yn', '0');
        const isPartnerExpectIncomeChanges = get(formValue, 'values.partner-income-changes-yn', '0');
        const arrayUser = {
            main: {},
            partner: {},
        };
        if (typeof usersOptions.main !== 'undefined') {
            if (isPersonalExpectIncomeChanges === '3') {
                arrayUser.main.is_not_sure_other_income = true;
            } else {
                arrayUser.main.is_not_sure_other_income = false;
            }
            arrayUser.main.other_income = get(formValue, 'values.personal-income-changes', '');
            arrayUser.main.user_id = usersOptions.main.profile_uuid;
        }
        if (partnerFirstName !== null) {
            if (isPartnerExpectIncomeChanges === '3') {
                arrayUser.partner.is_not_sure_other_income = true;
            } else {
                arrayUser.partner.is_not_sure_other_income = false;
            }
            arrayUser.partner.other_income = get(formValue, 'values.partner-income-changes', '');
            arrayUser.partner.user_id = usersOptions.partner.profile_uuid;
        }
        changeFieldValue('other_income_user', arrayUser);
    }

    renderPersonalIncomeChanges() {
        const { formValue, isSubmitted } = this.props;
        const isPersonalExpectIncomeChanges = get(formValue, 'values.personal-income-changes-yn', '0');
        if (isPersonalExpectIncomeChanges === '1') {
            return (
                <Field
                    component={FreeAdditionalBox}
                    label="Describe changes"
                    name="personal-income-changes"
                    extraClasses="mb-small"
                    id="1"
                    cols={30}
                    rows={10}
                    isShowErrors={isSubmitted}
                />
            );
        }
        return null;
    }

    renderPartnerIncomeChanges() {
        const { formValue, isSubmitted } = this.props;
        const isPersonalExpectIncomeChanges = get(formValue, 'values.partner-income-changes-yn', '0');
        if (isPersonalExpectIncomeChanges === '1') {
            return (
                <Field
                    component={FreeAdditionalBox}
                    label="Describe changes"
                    name="partner-income-changes"
                    extraClasses="mb-small"
                    id="2"
                    cols={30}
                    rows={10}
                    isShowErrors={isSubmitted}
                />
            );
        }
        return null;
    }

    render() {
        const {
            formValue,
            isSubmitted,
            handleSubmit,
            handleBack,
            partnerFirstName,
        } = this.props;
        const isAnySyncErrors = isEmpty(get(formValue, 'syncErrors', {}));

        return (
            <Fragment>
                <FulfilledLine loadingPercent={40} lineColor="#6e0a7b" />
                <div className="container dis-f mt-5rem mb-5rem">
                    <div className="col span-8">
                        <h2 className="fw-b mt-xlarge mb-medium">
                            Income changes
                        </h2>
                        <p className="mb-large">
                            It&apos;s important for us to know if your income is likely to change over the medium term,
                            whether as a result of salary adjustments, selling investments, windfalls or inheritances.
                        </p>
                        <Field
                            component={YesNoNotSure}
                            label="Do you expect any changes to your income in the next 5 years?"
                            name="personal-income-changes-yn"
                            extraClasses="span-2"
                            yesChecked={get(formValue, 'values.personal-income-changes-yn', '0') === '1'}
                            noChecked={get(formValue, 'values.personal-income-changes-yn', '0') === '2'}
                            notSureChecked={get(formValue, 'values.personal-income-changes-yn', '0') === '3'}
                            isShowErrors={isSubmitted}
                            validate={[
                                required,
                            ]}
                        />
                        {this.renderPersonalIncomeChanges()}
                        {
                            partnerFirstName !== null
                            && (
                                <Fragment>
                                    <div className="hr span-8" />
                                    <Field
                                        component={YesNoNotSure}
                                        label={`Does ${partnerFirstName} expect any changes to their income in the next 5 years?`}
                                        name="partner-income-changes-yn"
                                        extraClasses="span-2"
                                        yesChecked={get(formValue, 'values.partner-income-changes-yn', '0') === '1'}
                                        noChecked={get(formValue, 'values.partner-income-changes-yn', '0') === '2'}
                                        notSureChecked={get(formValue, 'values.partner-income-changes-yn', '0') === '3'}
                                        isShowErrors={isSubmitted}
                                        validate={[
                                            required,
                                        ]}
                                    />
                                    {this.renderPartnerIncomeChanges()}
                                </Fragment>
                            )
                        }
                    </div>
                    <div className="col span-3 col-last explain_text mt-4rem">
                        <ModalInformationIcon
                            title="Income taxing"
                            text="93% of people who come to us have not structured the most effective way to take income from a tax perspective."
                        />
                    </div>

                </div>
                <BackNextButtons
                    isActiveBack={false}
                    onClickNext={handleSubmit}
                    isActiveNext={!isSubmitted || (isAnySyncErrors && isSubmitted)}
                    onClickBack={handleBack}
                    buttonClass="btn-purple"
                    extraClass="span-8"
                />
            </Fragment>
        );
    }
}

IncomeChanges.propTypes = {
    changeFieldValue: PropTypes.func.isRequired,
    formValue: PropTypes.object,
    handleSubmit: PropTypes.func.isRequired,
    isSubmitted: PropTypes.bool.isRequired,
    handleBack: PropTypes.func.isRequired,
    usersOptions: PropTypes.object,
    handleGetMePlan: PropTypes.func.isRequired,
    partnerFirstName: PropTypes.string,
};

IncomeChanges.defaultProps = {
    formValue: {},
    usersOptions: {},
    partnerFirstName: null,
};

const mapStateToProps = state => ({
    initialValues: {
        nextStage: '/survey/dashboard',
        prevStage: '/survey/dashboard',
        updateUser: true,
    },
    usersOptions: get(state, 'plan.users', {}),
    partnerFirstName: get(state, 'plan.users.partner.profile.first_name', null),
    assets: get(state, 'plan.assets', []),

});

const mapDispatchToProps = dispatch => ({
    changeFieldValue: (field, value) => {
        dispatch(change(IncomeChanges.formName, field, value));
    },
});

const IncomeChangesWithHoc = SurveyHoc(IncomeChanges, IncomeChanges.surveyData);

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(IncomeChangesWithHoc));
