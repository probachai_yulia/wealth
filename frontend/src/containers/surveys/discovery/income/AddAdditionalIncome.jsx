import React, { Component, Fragment } from 'react';
import { change, Field } from 'redux-form';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import PropTypes from 'prop-types';

import isEmpty from 'lodash/isEmpty';
import get from 'lodash/get';
import { required } from '../../../../utils/validation.helper';
import { CancelSaveHeader } from '../../../../components/CancelSaveHeader';
import Select from '../../../../components/Select';
import MoneyBlock from '../../../../components/MoneyBlock';
import SurveyHoc from '../../../hoc/SurveyHoc';
import { BackNextButtons } from '../../../../components/BackNextButtons';
import { addPlanCashflow, updatePlanCashflow } from '../../../../actions/PlanActions';
import InputCustom from '../../../../components/InputCustom';
import FreeAdditionalBox from '../../../../components/FreeAdditionalBox';

/**
 * Render the content for user idle. Dumb component
 *
 */
class AddAdditionalIncome extends Component {
    static formName = 'addAdditionalIncome';

    static surveyData = {
        formName: AddAdditionalIncome.formName,
        nextStage: 'nextStage',
        prevStage: 'prevStage',
        data: [
            {
                fields: [
                    'cashflows_plan',
                    'plan_giving_data',
                    'plan_giving_id,',
                ],
                isUpdate: 'update',
                selector: 'cashflows_id',
                methodCreate: addPlanCashflow,
                methodUpdate: updatePlanCashflow,
            },
        ],
    };

    constructor(props) {
        super(props);

        this.renderOtherIncome = this.renderOtherIncome.bind(this);
        this.optionAssets = this.optionAssets.bind(this);
        this.optionOwnerAccount = this.optionOwnerAccount.bind(this);
        this.optionOtherAssset = this.optionOtherAssset.bind(this);

        const {
            handleGetMePlan,
        } = this.props;

        handleGetMePlan();
    }

    componentWillUpdate() {
        const {
            formValue, changeFieldValue,
        } = this.props;
        const selectedAsset = get(formValue, 'values.asset_type', '');

        const planCashflow = [];
        if (selectedAsset === 'other') {
            planCashflow.push({
                user_cashflow_type: 'other',
                amount: get(formValue, 'values.asset_amount', 0),
                value_type: 'F',
                frequency: get(formValue, 'values.asset_frequency', 0),
                plan_cashflow_id: get(formValue, 'values.cashflows_id', null),
                other_income_type: get(formValue, 'values.asset_other', ''),
                profile_uuid: get(formValue, 'values.owner', ''),
                additional: get(formValue, 'values.additional', ''),
            });
        } else {
            planCashflow.push({
                user_cashflow_type: 'assets',
                asset_uuid: get(formValue, 'values.asset_type', null),
                amount: get(formValue, 'values.asset_amount', 0),
                value_type: 'F',
                frequency: get(formValue, 'values.asset_frequency', 0),
                plan_cashflow_id: get(formValue, 'values.cashflows_id', null),
                additional: get(formValue, 'values.additional', ''),
            });
        }
        if (get(formValue, 'values.update', null) === true) {
            changeFieldValue('plan_giving_data', planCashflow[0]);
            changeFieldValue('plan_giving_id', get(formValue, 'values.cashflows_id', null));
        }
        changeFieldValue('cashflows_plan', planCashflow);
    }


    optionAssets = () => {
        const {
            assets, assetsList, addressesList, taxBenefits,
        } = this.props;
        const ASSETS = [];
        const assetList = [];
        assetsList.map((item) => {
            Object.keys(item).map((subitem) => {
                item[subitem].map((subsitem) => {
                    assetList.push(subsitem);
                    return null;
                });
                return null;
            });
            return null;
        });
        taxBenefits.map((item) => {
            assetList.push(item);
            return null;
        });
        assets.map((item, i) => {
            const currentAsset = assetList.find(subitem => subitem.code === item.asset_type || subitem.code === item.tax_benefit);
            let selectName = item.provider_name;
            if (typeof currentAsset !== 'undefined') {
                if (item.asset_type === 'benefit' || item.asset_type === 'contribution' || item.asset_type === 'annuity') { // PENSIONS
                    selectName = `${currentAsset.name} - ${item.provider_name}`;
                } else if (item.asset_type.indexOf('isa') !== -1) { // ISAs
                    selectName = `${item.provider_name} - ${currentAsset.name}`;
                } else if (item.asset_type.indexOf('cash') !== -1 && item.asset_type !== 'cash_other') { // CASH
                    selectName = `${item.provider_name} - ${currentAsset.name}`;
                } else if (item.asset_type.indexOf('property') !== -1) { // PROPERTY
                    const cureAddress = addressesList.find(subitem => item.address_uuid === subitem.address_uuid);
                    selectName = `prop ${cureAddress.address_1}`;
                } else {
                    selectName = `inve ${item.provider_name}`;
                }
            }
            ASSETS.push(
                {
                    id: i,
                    name: selectName,
                    code: item.plan_asset_uuid,
                },
            );
            return null;
        });
        ASSETS.push(
            {
                id: ASSETS.length,
                name: 'Other (please type)',
                code: 'other',
            },
        );
        return ASSETS;
    };

    optionOwnerAccount() {
        const ownersList = [];
        let id = 0;
        const {
            usersOptions,
        } = this.props;
        Object.keys(usersOptions).map((item) => {
            if (usersOptions[item] !== null) {
                if (item === 'main' || item === 'partner') {
                    ownersList.push({
                        id,
                        code: usersOptions[item].profile_uuid,
                        name: `${item}`,
                    });
                    id += 1;
                }
                if (item === 'child') {
                    usersOptions[item].map((subitem, i) => {
                        ownersList.push({
                            id,
                            code: subitem.profile.profile_id,
                            name: `${item} ${i + 1}`,
                            dob: subitem.profile.dob,
                            role: item,
                        });
                        id += 1;
                        return null;
                    });
                }
            }
            return null;
        });

        return ownersList;
    }

    optionOtherAssset() {
        const { isSubmitted, formValue } = this.props;
        const selectedAsset = get(formValue, 'values.asset_type', '');
        if (selectedAsset === 'other') {
            return (
                <Field
                    component={InputCustom}
                    name="asset_other"
                    extraClasses="span-4 col"
                    label=""
                    placeholder="Descbribe additional income briefly..."
                    isShowErrors={isSubmitted}
                    validate={[
                        required,
                    ]}
                />
            );
        }
        return null;
    }

    renderOtherIncome() {
        const { isSubmitted, formValue } = this.props;
        const selectedAsset = get(formValue, 'values.asset_type', '');
        if (selectedAsset !== 'other') {
            return (
                <div className="dis-f span-8">
                    <MoneyBlock
                        label="Estimated amount received"
                        poundFieldName="asset_amount"
                        selectFieldName="asset_frequency"
                        isShowErrors={isSubmitted}
                    />
                </div>
            );
        }
        return (
            <Fragment>
                <div className="span-8">
                    <MoneyBlock
                        label="Estimated amount received"
                        poundFieldName="asset_amount"
                        selectFieldName="asset_frequency"
                        isShowErrors={isSubmitted}
                    />
                </div>
                <div className="form-inline dis-f span-8">
                    <Field
                        component={Select}
                        name="owner"
                        extraClasses="span-4"
                        label="Who receives this income?"
                        placeholder="Select owner"
                        options={this.optionOwnerAccount()}
                        isShowErrors={isSubmitted}
                        validate={[
                            required,
                        ]}
                    />
                </div>
            </Fragment>

        );
    }

    render() {
        const {
            handleSubmit, isSubmitted, formValue, handleBack,
        } = this.props;
        const isAnySyncErrors = isEmpty(get(formValue, 'syncErrors', {}));
        const selectedAsset = get(formValue, 'values.asset_type', '');
        const activeSubmit = !isSubmitted || (isAnySyncErrors && isSubmitted);
        return (
            <Fragment>
                <CancelSaveHeader
                    title="Add additional income"
                />
                <div className="container dis-f fd-c mt-5rem mb-2rem">
                    <form className="know-you-form col span-8 dis-f fd-c mt-xlarge">
                        <div className="col-span-8">
                            <div className="dis-f span-8">
                                <Field
                                    component={Select}
                                    name="asset_type"
                                    extraClasses={`${selectedAsset === 'other' && 'col'} span-4`}
                                    label="Additional income from which asset?"
                                    placeholder="Select asset"
                                    options={this.optionAssets()}
                                    isShowErrors={isSubmitted}
                                    validate={[
                                        required,
                                    ]}
                                />
                                {this.optionOtherAssset()}
                            </div>
                            <div className="hr_nm span-8 mb-medium" />
                            {this.renderOtherIncome()}
                            <Field
                                component={FreeAdditionalBox}
                                label="Describe income (optional)"
                                extraClasses="wealth-textarea"
                                name="additional"
                                cols={30}
                                rows={10}
                                isShowErrors={isSubmitted}
                                placeholder=""
                                subExtraClass="no-padding"
                            />
                        </div>
                    </form>
                </div>
                <BackNextButtons
                    isActiveBack={false}
                    onClickNext={handleSubmit}
                    isActiveNext={activeSubmit}
                    onClickBack={handleBack}
                    buttonClass="btn-purple"
                    nextText="Save"
                />
            </Fragment>
        );
    }
}

AddAdditionalIncome.propTypes = {
    changeFieldValue: PropTypes.func.isRequired,
    formValue: PropTypes.object,
    handleSubmit: PropTypes.func.isRequired,
    isSubmitted: PropTypes.bool.isRequired,
    handleBack: PropTypes.func.isRequired,
    usersOptions: PropTypes.object,
    assets: PropTypes.arrayOf(PropTypes.object),
    handleGetMePlan: PropTypes.func.isRequired,
    addressesList: PropTypes.arrayOf(PropTypes.object),
    taxBenefits: PropTypes.arrayOf(PropTypes.object),
    assetsList: PropTypes.arrayOf(PropTypes.object),
};

AddAdditionalIncome.defaultProps = {
    formValue: {},
    usersOptions: {},
    assets: [],
    addressesList: [],
    taxBenefits: [],
    assetsList: [],
};

const mapStateToProps = (state, ownProps) => {
    const cashflows = get(state, 'plan.cashflows', []);
    const assets = get(state, 'plan.assets', []);
    const cashflowsId = cashflows.indexOf(cashflows.find(item => item.plan_cashflow_uuid === ownProps.match.params.cashflow_uuid));
    let assetId;
    if (get(state, `plan.cashflows[${cashflowsId}].user_cashflow_type`, null) !== null && get(state, `plan.cashflows[${cashflowsId}].user_cashflow_type`, null) !== 'assets') {
        assetId = 'other';
    } else {
        assets.map((item) => {
            const filteredCashflow = item.cashflows.find(subitem => subitem.plan_cashflow_uuid === ownProps.match.params.cashflow_uuid);
            if (typeof filteredCashflow !== 'undefined') {
                assetId = item.plan_asset_uuid;
            }
            return null;
        });
    }
    return ({
        initialValues: {
            additional: get(state, `plan.cashflows[${cashflowsId}].additional`, null),
            asset_other: get(state, `plan.cashflows[${cashflowsId}].other_income_type`, ''),
            nextStage: '/survey/income/additional-income',
            prevStage: '/survey/income/additional-income',
            cashflows_id: ownProps.match.params.cashflow_uuid,
            asset_type: assetId,
            asset_amount: get(state, `plan.cashflows[${cashflowsId}].amount`, null),
            asset_frequency: get(state, `plan.cashflows[${cashflowsId}].frequency`, 'per_month'),
            update: get(state, `plan.cashflows[${cashflowsId}].plan_cashflow_uuid`, null) !== null,
            owner: get(state, `plan.cashflows[${cashflowsId}].profile_uuid`, null),
        },
        usersOptions: get(state, 'plan.users', {}),
        assets: get(state, 'plan.assets', []),
        taxBenefits: get(state, 'options.tax_benefits', []),
        assetsList: get(state, 'options.asset_types', []),
        addressesList: get(state, 'plan.addresses', []),
        firstName: get(state, 'plan.users.partner.profile.first_name', null),
        lastName: get(state, 'plan.users.partner.profile.last_name', null),
    });
};

const mapDispatchToProps = dispatch => ({
    changeFieldValue: (field, value) => {
        dispatch(change(AddAdditionalIncome.formName, field, value));
    },
});

const AddAdditionalIncomeWithHoc = SurveyHoc(AddAdditionalIncome, AddAdditionalIncome.surveyData);

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(AddAdditionalIncomeWithHoc));
