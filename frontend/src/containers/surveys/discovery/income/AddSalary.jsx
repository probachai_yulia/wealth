import React, { Component, Fragment } from 'react';
import { change, Field } from 'redux-form';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import get from 'lodash/get';
import PropTypes from 'prop-types';

import isEmpty from 'lodash/isEmpty';
import { booleanRequired, required } from '../../../../utils/validation.helper';
import { CancelSaveHeader } from '../../../../components/CancelSaveHeader';
import { InputPound } from '../../../../components/InputPound';
import DisabledSelectedSelect from '../../../../components/DisabledSelectedSelect';
import YesNo from '../../../../components/YesNo';
import InputCustom from '../../../../components/InputCustom';
import AnythingElse from '../../../../components/AnythingElse';
import SurveyHoc from '../../../hoc/SurveyHoc';
import { BackNextButtons } from '../../../../components/BackNextButtons';
import { normalizeBoolean } from '../../../../utils/helper';
import { addCashflowToJob, updatePlanJob } from '../../../../actions/PlanActions';

/**
 * Render the content for user idle. Dumb component
 *
 */
class AddSalary extends Component {
    static formName = 'addSalary';

    static surveyData = {
        formName: AddSalary.formName,
        nextStage: 'nextStage',
        prevStage: 'prevStage',
        data: [
            {
                fields: [
                    'cashflows_plan',
                    'cashflow_changes',
                ],
                // additionalFields: [],
                isUpdate: 'update',
                selector: 'job_id',
                methodUpdate: addCashflowToJob,
            },
            {
                fields: [
                    'company',
                    'employment_type',
                    'has_remuneration',
                    'salary_additional',
                ],
                // additionalFields: [],
                isUpdate: 'update',
                selector: 'job_id',
                methodUpdate: updatePlanJob,
            },
        ],
    };

    constructor(props) {
        super(props);

        this.renderAdditionalReceive = this.renderAdditionalReceive.bind(this);
        this.renderAdditionalSalaryBlock = this.renderAdditionalSalaryBlock.bind(this);

        const {
            handleGetMePlan,
        } = this.props;

        handleGetMePlan();
    }

    componentDidUpdate() {
        const {
            formValue, changeFieldValue,
        } = this.props;
        const planCashflow = [];
        planCashflow.push({
            user_cashflow_type: 'work_salary',
            amount: get(formValue, 'values.work_salary_amount', 0),
            value_type: 'F',
            frequency: 'per_year',
            plan_cashflow_id: get(formValue, 'values.work_salary_id', null),
        });
        if (get(formValue, 'values.work_bonus_amount', null) !== null) {
            planCashflow.push({
                user_cashflow_type: 'work_bonus',
                amount: get(formValue, 'values.work_bonus_amount', 0),
                value_type: 'F',
                frequency: 'per_year',
                plan_cashflow_id: get(formValue, 'values.work_bonus_id', null),
            });
        }
        if (get(formValue, 'values.work_bonus_amount', null) !== null) {
            planCashflow.push({
                user_cashflow_type: 'work_bonus',
                amount: get(formValue, 'values.work_bonus_amount', 0),
                value_type: 'F',
                frequency: 'per_year',
                plan_cashflow_id: get(formValue, 'values.work_bonus_id', null),
            });
        }
        if (get(formValue, 'values.work_dividends_amount', null) !== null) {
            planCashflow.push({
                user_cashflow_type: 'work_dividends',
                amount: get(formValue, 'values.work_dividends_amount', 0),
                value_type: 'F',
                frequency: 'per_year',
                plan_cashflow_id: get(formValue, 'values.work_dividends_id', null),
            });
        }
        if (get(formValue, 'values.work_other_amount', null) !== null) {
            planCashflow.push({
                additional: get(formValue, 'values.work_other_additional', ''),
                user_cashflow_type: 'work_other',
                amount: get(formValue, 'values.work_other_amount', 0),
                value_type: 'F',
                frequency: 'per_year',
                plan_cashflow_id: get(formValue, 'values.work_dividends_id', null),
            });
        }
        changeFieldValue('cashflows_plan', planCashflow);
    }

    renderAdditionalSalaryBlock() {
        const { formValue, isSubmitted } = this.props;
        const isSelfEmployer = get(formValue, 'values.employment_type', null) === 'self_employed';
        // check if person is self-employed
        if (isSelfEmployer) {
            return (
                <Fragment>
                    <div className="dis-f span-8">
                        <div className="input-wrap col span-4">
                            <Field
                                component={InputPound}
                                extraClasses="span-4"
                                label="Your average annual dividends"
                                hintMessage="Enter 0 if none"
                                name="work_dividends_amount"
                                isShowErrors={isSubmitted}
                                validate={[
                                    required,
                                ]}
                            />
                        </div>
                    </div>
                    <AnythingElse open={get(formValue, 'values.salary_additional', null) !== null} name="salary_additional" placeholder="E.g. value and percentage ownership of your business" />
                </Fragment>
            );
        }
        return (
            <Fragment>
                <Field
                    component={YesNo}
                    label="Do you receive anything else as part of your remuneration?"
                    name="has_remuneration"
                    componentClass="span-8"
                    yesChecked={get(formValue, 'values.has_remuneration', null) === true}
                    noChecked={get(formValue, 'values.has_remuneration', null) === false}
                    isShowErrors={isSubmitted}
                    normalize={normalizeBoolean}
                    validate={[
                        booleanRequired,
                    ]}
                />
                {this.renderAdditionalReceive()}
                <AnythingElse open={get(formValue, 'values.salary_additional', null) !== null} name="salary_additional" placeholder="E.g. other benefits such as life insurance or childcare you may receive" />
            </Fragment>
        );
    }

    renderAdditionalReceive() {
        const { formValue, isSubmitted } = this.props;
        const isAdditionalReceive = get(formValue, 'values.has_remuneration', null) === true;
        if (isAdditionalReceive) {
            return (
                <div className="dis-f span-8">
                    <Field
                        component={InputPound}
                        extraClasses="span-4"
                        label="How much per year?"
                        name="work_other_amount"
                        isShowErrors={isSubmitted}
                        validate={[
                            required,
                        ]}
                    />
                    <Field
                        component={InputCustom}
                        extraClasses="span-4"
                        label="Describe"
                        placeholder="Please describe briefly..."
                        name="work_other_additional"
                        isShowErrors={isSubmitted}
                        validate={[
                            required,
                        ]}
                    />
                </div>
            );
        }
        return null;
    }

    render() {
        const {
            handleSubmit, isSubmitted, formValue, handleBack,
        } = this.props;
        const isAnySyncErrors = isEmpty(get(formValue, 'syncErrors', {}));
        return (
            <Fragment>
                <CancelSaveHeader
                    title="Add salary income"
                />
                <div className="container dis-f fd-c mt-xlarge">
                    <form className="know-you-form col span-8 dis-f fd-c mt-xlarge">
                        <div className="col-span-8">
                            <div className="dis-f span-8">
                                <DisabledSelectedSelect
                                    extraClasses="span-4"
                                    label="Role"
                                    name="role-in-company"
                                    selectedValue={get(formValue, 'values.role', '[role] at [Company]')}
                                    disabled
                                    placeholder={get(formValue, 'values.role', '[role] at [Company]')}
                                />
                            </div>
                            <div className="hr span-8" />
                            <div className="dis-f span-8">
                                <div className="input-wrap col span-4">
                                    <Field
                                        component={InputPound}
                                        extraClasses="span-4"
                                        label="What’s your annual salary?"
                                        name="work_salary_amount"
                                        // placeholder="First name"
                                        isShowErrors={isSubmitted}
                                        validate={[
                                            required,
                                        ]}
                                    />
                                </div>
                                <div className="input-wrap col-last span-4">
                                    <Field
                                        component={InputPound}
                                        extraClasses="span-4"
                                        label="And your average annual bonus?"
                                        name="work_bonus_amount"
                                        hintMessage="Enter 0 if none"
                                        isShowErrors={isSubmitted}
                                        validate={[
                                            required,
                                        ]}
                                    />
                                </div>
                            </div>
                            {this.renderAdditionalSalaryBlock()}
                        </div>
                    </form>
                </div>
                <BackNextButtons
                    isActiveBack={false}
                    onClickNext={handleSubmit}
                    isActiveNext={!isSubmitted || (isAnySyncErrors && isSubmitted)}
                    onClickBack={handleBack}
                    buttonClass="btn-purple"
                    nextText="Save"
                />
            </Fragment>
        );
    }
}

AddSalary.propTypes = {
    changeFieldValue: PropTypes.func.isRequired,
    formValue: PropTypes.object,
    handleSubmit: PropTypes.func.isRequired,
    isSubmitted: PropTypes.bool.isRequired,
    handleBack: PropTypes.func.isRequired,
    handleGetMePlan: PropTypes.func.isRequired,
};

AddSalary.defaultProps = {
    formValue: {},
};

const mapStateToProps = (state, ownProps) => {
    let employmentName; let currentJob; let company; let employmentType;
    let workSalary; let workSalaryAmount; let workSalaryId;
    let workBonus; let workBonusAmount; let workBonusId;
    let workDividends; let workDividendsAmount; let workDividendsId;
    let workOther; let workOtherAmount; let workOtherAdditional;
    const partnerJobs = get(state, 'plan.users.partner.jobs', []).find(item => item.plan_user_job_uuid === ownProps.match.params.jobs_uuid);
    const clientJobs = get(state, 'plan.users.main.jobs', []).find(item => item.plan_user_job_uuid === ownProps.match.params.jobs_uuid);
    if (typeof partnerJobs !== 'undefined') {
        currentJob = partnerJobs;
    }
    if (typeof clientJobs !== 'undefined') {
        currentJob = clientJobs;
    }
    if (typeof currentJob !== 'undefined') {
        const employName = get(state, 'options.employment_types', []).find(item => item.code === currentJob.employment_type);
        employmentName = typeof employName !== 'undefined' ? employName.name : '';
        ({
            company,
        } = currentJob);
        employmentType = employName.code;
        workSalary = currentJob.cashflows.find(item => item.user_cashflow_type === 'work_salary');
        workSalaryAmount = typeof workSalary !== 'undefined' ? workSalary.amount : null;
        workSalaryId = typeof workSalary !== 'undefined' ? workSalary.plan_cashflow_uuid : null;
        workBonus = currentJob.cashflows.find(item => item.user_cashflow_type === 'work_bonus');
        workBonusAmount = typeof workBonus !== 'undefined' ? workBonus.amount : null;
        workOtherAdditional = typeof workBonus !== 'undefined' ? workBonus.additional : null;
        workBonusId = typeof workBonus !== 'undefined' ? workBonus.plan_cashflow_uuid : null;
        workDividends = currentJob.cashflows.find(item => item.user_cashflow_type === 'work_dividends');
        workDividendsAmount = typeof workDividends !== 'undefined' ? workDividends.amount : null;
        workDividendsId = typeof workDividends !== 'undefined' ? workDividends.plan_cashflow_uuid : null;
        workOther = currentJob.cashflows.find(item => item.user_cashflow_type === 'work_other');
        workOtherAmount = typeof workOther !== 'undefined' ? workOther.amount : null;
    }
    return ({
        initialValues: {
            nextStage: '/survey/income/salary-income',
            prevStage: '/survey/dashboard',
            role: `${employmentName} at ${company}`,
            employment_type: employmentType,
            company,
            job_id: ownProps.match.params.jobs_uuid,
            update: true,
            has_remuneration: currentJob ? currentJob.has_remuneration : null,
            work_salary_amount: workSalaryAmount,
            work_salary_id: workSalaryId,
            work_bonus_amount: workBonusAmount,
            work_bonus_id: workBonusId,
            work_dividends_amount: workDividendsAmount,
            work_dividends_id: workDividendsId,
            salary_additional: currentJob ? currentJob.salary_additional : null,
            work_other_amount: workOtherAmount,
            work_other_additional: workOtherAdditional,
        },
        usersOptions: get(state, 'plan.users', {}),
        assets: get(state, 'plan.assets', []),
        employmentTypes: get(state, 'options.employment_types', []),
    });
};

const mapDispatchToProps = dispatch => ({
    changeFieldValue: (field, value) => {
        dispatch(change(AddSalary.formName, field, value));
    },
});

const AddSalaryWithHoc = SurveyHoc(AddSalary, AddSalary.surveyData);

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(AddSalaryWithHoc));
