import React, { Component, Fragment } from 'react';
import { change } from 'redux-form';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import PropTypes from 'prop-types';
import get from 'lodash/get';
import { bindActionCreators } from 'redux';

import { BackNextButtons } from '../../../../components/BackNextButtons';
import FulfilledLine from '../../../../components/FulfilledLine';
import FinanceAboutTable from '../../../../components/FinanceAboutTable';
import { getMePlan } from '../../../../actions/UserActions';

/**
 * Render the content for user idle. Dumb component
 *
 */
class SalaryIncome extends Component {
    constructor(props) {
        super(props);

        this.handleBack = this.handleBack.bind(this);
        this.handleNext = this.handleNext.bind(this);
        this.getPersonalListSavings = this.getPersonalListSavings.bind(this);
        this.getPartnerListSavings = this.getPartnerListSavings.bind(this);

        const {
            handleGetMePlan,
        } = this.props;

        handleGetMePlan();
    }

    componentWillUpdate() {
        const { client, partner, history } = this.props;
        let redirect = false;
        if (client !== null && partner !== null) {
            if (partner.jobs.length === 0 && client.jobs.length === 0) {
                redirect = true;
            }
        }
        if (client !== null && partner === null) {
            if (client.jobs.length === 0) {
                redirect = true;
            }
        }
        if (redirect) {
            history.push('/survey/income/additional-income');
        }
    }

    getPersonalListSavings = () => {
        const { client, clientJob, employmentTypes } = this.props;
        const salaryArray = [];
        clientJob.map((item) => {
            const employName = employmentTypes.find(subitem => subitem.code === item.employment_type);
            const employmentName = typeof employName !== 'undefined' ? employName.name : '';
            const workSalary = item.cashflows.find(subitem => subitem.user_cashflow_type === 'work_salary');
            const workSalaryAmount = typeof workSalary !== 'undefined' ? `£${workSalary.amount.toLocaleString('en')}` : null;
            const workBonus = item.cashflows.find(subitem => subitem.user_cashflow_type === 'work_bonus');
            const workBonusAmount = typeof workBonus !== 'undefined' ? `£${workBonus.amount.toLocaleString('en')}` : null;
            const workDividends = item.cashflows.find(subitem => subitem.user_cashflow_type === 'work_dividends');
            const workDividendsAmount = typeof workDividends !== 'undefined' ? `£${workDividends.amount.toLocaleString('en')}` : null;
            let addNew = true;
            if (workSalary || workBonus || workDividends) {
                addNew = false;
            }
            salaryArray.push({
                clientName: client.profile.first_name,
                icon: 'work.svg',
                client_image: 'user.svg',
                annual_salary: [`${workSalaryAmount}`],
                annual_dividends: [`${workDividendsAmount}`],
                addNew,
                buttonText: '+ Add salary income',
                average_annual_bonus: [`${workBonusAmount}`],
                edit_link: `/survey/income/add-salary/${item.plan_user_job_uuid}`,
                subtitle: employmentName,
                title: item.company,
                extra_information: item.salary_additional,
            });
            return null;
        });
        return salaryArray;
    };

    getPartnerListSavings = () => {
        const { partner, partnerJob, employmentTypes } = this.props;
        const salaryArray = [];
        partnerJob.map((item) => {
            const employName = employmentTypes.find(subitem => subitem.code === item.employment_type);
            const employmentName = typeof employName !== 'undefined' ? employName.name : '';
            const workSalary = item.cashflows.find(subitem => subitem.user_cashflow_type === 'work_salary');
            const workSalaryAmount = typeof workSalary !== 'undefined' ? workSalary.amount : null;
            const workBonus = item.cashflows.find(subitem => subitem.user_cashflow_type === 'work_bonus');
            const workBonusAmount = typeof workBonus !== 'undefined' ? workBonus.amount : null;
            const workDividends = item.cashflows.find(subitem => subitem.user_cashflow_type === 'work_dividends');
            const workDividendsAmount = typeof workDividends !== 'undefined' ? workDividends.amount : null;
            const workOther = item.cashflows.find(subitem => subitem.user_cashflow_type === 'work_other');
            const workOtherAmount = typeof workOther !== 'undefined' ? workOther.amount : null;
            let addNew = true;
            if (workSalary || workBonus || workDividends) {
                addNew = false;
            }
            salaryArray.push({
                clientName: partner.profile.first_name,
                icon: 'work.svg',
                client_image: 'user.svg',
                annual_salary: [`${workSalaryAmount}`],
                annual_dividends: [`${workDividendsAmount}`],
                other_work: [`${workOtherAmount}`],
                addNew,
                buttonText: '+ Add salary income',
                average_annual_bonus: [`${workBonusAmount}`],
                edit_link: `/survey/income/add-salary/${item.plan_user_job_uuid}`,
                subtitle: employmentName,
                title: item.company,
                extra_information: item.salary_additional,
            });
            return null;
        });
        return salaryArray;
    };

    handleBack() {
        const { history } = this.props;
        history.push('../dashboard');
    }

    handleNext() {
        const { history } = this.props;
        history.push('/survey/income/additional-income');
    }

    render() {
        return (
            <Fragment>
                <FulfilledLine loadingPercent={40} lineColor="#6e0a7b" />
                <div className="container dis-f fd-c mb-5rem">
                    <h2 className="fw-b mt-xlarge mb-mlarge">
                        What you earn
                    </h2>
                    <FinanceAboutTable data={this.getPersonalListSavings()} />
                    <div className="hr span-8" />
                    <FinanceAboutTable data={this.getPartnerListSavings()} />
                </div>
                <BackNextButtons
                    isActiveBack={false}
                    onClickNext={this.handleNext}
                    onClickBack={this.handleBack}
                    buttonClass="btn-purple"
                    extraClass="span-8"
                />
            </Fragment>
        );
    }
}

SalaryIncome.propTypes = {
    history: PropTypes.object.isRequired,
    handleGetMePlan: PropTypes.func.isRequired,
    client: PropTypes.object,
    partner: PropTypes.object,
    clientJob: PropTypes.arrayOf(PropTypes.object),
    partnerJob: PropTypes.arrayOf(PropTypes.object),
    employmentTypes: PropTypes.arrayOf(PropTypes.object),
};

SalaryIncome.defaultProps = {
    client: null,
    partner: null,
    clientJob: [],
    partnerJob: [],
    employmentTypes: [],
};

const mapStateToProps = state => ({
    arrayData: get(state, 'plan.assets', []),
    users: get(state, 'plan.users', {}),
    addresses: get(state, 'plan.addresses', []),
    client: get(state, 'plan.users.main', null),
    partner: get(state, 'plan.users.partner', null),
    clientJob: get(state, 'plan.users.main.jobs', []),
    partnerJob: get(state, 'plan.users.partner.jobs', []),
    employmentTypes: get(state, 'options.employment_types', []),
});

const mapDispatchToProps = dispatch => ({
    changeFieldValue: (field, value) => {
        dispatch(change('addCashSavingsAccount', field, value));
    },
    handleGetMePlan: bindActionCreators(getMePlan, dispatch),
});

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(SalaryIncome));
