import React, { Component, Fragment } from 'react';
import { Field } from 'redux-form';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import PropTypes from 'prop-types';
import get from 'lodash/get';
import isEmpty from 'lodash/isEmpty';
import { BackNextButtons } from '../../../../components/BackNextButtons';
import FulfilledLine from '../../../../components/FulfilledLine';
import {
    booleanRequired, requiredAtLeastOne,
} from '../../../../utils/validation.helper';
import SurveyHoc from '../../../hoc/SurveyHoc';
import {
    patchPlan,
} from '../../../../actions/PlanActions';
import YesNo from '../../../../components/YesNo';
import {
    normalizeBoolean,
} from '../../../../utils/helper';
import ModalInformationIcon from '../../../../components/ModalInformationIcon';
import CheckboxesBlockArray from '../../../../components/CheckboxesBlockArray';
import MultipleInputBlock from '../../../../components/MultipleInputBlock';
import { BENEFICIARIES } from '../../../../helpers/constants';

/**
 * Render the content for user idle. Dumb component
 * @return {JSX.Element}
 */

class LegacyPlans extends Component {
    static formName = 'legacyPlans';

    static surveyData = {
        formName: LegacyPlans.formName,
        nextStage: 'nextStage',
        prevStage: 'prevStage',
        data: [
            {
                fields: [],
                isUpdate: 'updatePlan',
                selector: 'planId',
                methodUpdate: patchPlan,
            },
        ],
    };

    constructor(props) {
        super(props);

        this.renderValidWill = this.renderValidWill.bind(this);

        const {
            handleGetMePlan,
        } = this.props;

        handleGetMePlan();
    }

    renderValidWill() {
        const { formValue, isSubmitted } = this.props;

        const isValidWill = get(formValue, 'values.valid_will', null);

        if (isValidWill) {
            return (
                <Fragment>
                    <CheckboxesBlockArray
                        formValue={formValue}
                        checkboxes={BENEFICIARIES}
                        isSubmitted={isSubmitted}
                        name="beneficiaries"
                        fieldName="beneficiaries"
                        extraClasses="pb-0"
                        validate={[
                            requiredAtLeastOne,
                        ]}
                    />
                    <MultipleInputBlock
                        name="name"
                        fieldsName="name"
                        isShowErrors={isSubmitted}
                        extraClasses="mt-1rem pb-0"
                        label="+ Add other"
                        buttonLabel="Remove Beneficiary"
                        placeholder="Type beneficiary first and last name"
                    />
                </Fragment>
            );
        }

        return null;
    }

    render() {
        const {
            formValue,
            handleBack,
            isSubmitted,
            handleSubmit,
        } = this.props;
        const isAnySyncErrors = isEmpty(get(formValue, 'syncErrors', {}));

        return (
            <Fragment>
                <FulfilledLine loadingPercent={10} lineColor="#1ED66C" />
                <section className="container dis-f">
                    <div className="col span-8">
                        <h2 className="fw-b mt-xlarge mb-mlarge">
                            Tell us about your legacy plans
                        </h2>
                        <form className="details-form col span-8 dis-f fd-c">
                            <Field
                                component={YesNo}
                                componentClass="span-8"
                                name="valid_will"
                                // eslint-disable-next-line max-len
                                label="Do you have a valid will in place?"
                                yesChecked={get(formValue, 'values.valid_will', null) === true}
                                normalize={normalizeBoolean}
                                noChecked={get(formValue, 'values.valid_will', null) === false}
                                validate={[
                                    booleanRequired,
                                ]}
                            />
                            {this.renderValidWill()}
                        </form>
                    </div>
                    <div className="col span-3 col-last explain_text mt-4rem p-r">
                        <ModalInformationIcon
                            title="Why we’re asking this"
                            // eslint-disable-next-line max-len
                            text="By understanding your family circumstances in full, we can build a plan that helps create the best possible future for all of you."
                        />
                    </div>
                </section>
                <BackNextButtons
                    isActiveBack={false}
                    onClickNext={handleSubmit}
                    isActiveNext={!isSubmitted || (isAnySyncErrors && isSubmitted)}
                    onClickBack={handleBack}
                />
            </Fragment>
        );
    }
}

LegacyPlans.propTypes = {
    handleBack: PropTypes.func.isRequired,
    handleSubmit: PropTypes.func.isRequired,
    handleGetMePlan: PropTypes.func.isRequired,
    isSubmitted: PropTypes.bool.isRequired,
    formValue: PropTypes.object,
};

LegacyPlans.defaultProps = {
    formValue: {},
};

const mapStateToProps = () => ({
    initialValues: {},
});

const LegacyPlansWithHoc = SurveyHoc(LegacyPlans, LegacyPlans.surveyData);

export default withRouter(connect(mapStateToProps)(LegacyPlansWithHoc));
