import React, { Component, Fragment } from 'react';
import { Field } from 'redux-form';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import PropTypes from 'prop-types';
import get from 'lodash/get';
import isEmpty from 'lodash/isEmpty';
import { BackNextButtons } from '../../../../components/BackNextButtons';
import FulfilledLine from '../../../../components/FulfilledLine';
import {
    booleanRequired,
} from '../../../../utils/validation.helper';
import SurveyHoc from '../../../hoc/SurveyHoc';
import {
    patchPlan,
} from '../../../../actions/PlanActions';
import YesNo from '../../../../components/YesNo';
import {
    normalizeBoolean,
} from '../../../../utils/helper';
import AnythingElse from '../../../../components/AnythingElse';

/**
 * Render the content for user idle. Dumb component
 * @return {JSX.Element}
 */

class LegacyDetails extends Component {
    static formName = 'legacyDetails';

    static surveyData = {
        formName: LegacyDetails.formName,
        nextStage: 'nextStage',
        prevStage: 'prevStage',
        data: [
            {
                fields: [],
                isUpdate: 'updatePlan',
                selector: 'planId',
                methodUpdate: patchPlan,
            },
        ],
    };

    constructor(props) {
        super(props);

        this.renderAnyInheritancesPast = this.renderAnyInheritancesPast.bind(this);
        this.renderAnyInheritancesFuture = this.renderAnyInheritancesFuture.bind(this);
        this.renderAnyGifts = this.renderAnyGifts.bind(this);

        const {
            handleGetMePlan,
        } = this.props;

        handleGetMePlan();
    }

    renderAnyInheritancesPast() {
        const { formValue } = this.props;

        const isPastInheritances = get(formValue, 'values.any_inheritances_past', null);

        if (isPastInheritances) {
            return (
                <AnythingElse
                    open
                    openText="Please describe"
                    placeholder="Please describe the situation for you [and Client2] briefly..."
                    name="any_inheritances_past_details"
                />
            );
        }

        return null;
    }

    renderAnyInheritancesFuture() {
        const { formValue } = this.props;

        const isFutureInheritances = get(formValue, 'values.any_inheritances_future', null);

        if (isFutureInheritances) {
            return (
                <AnythingElse
                    open
                    openText="Please describe"
                    placeholder="[Some explanation about excepting to receive inheritances...]"
                    name="any_inheritances_future_details"
                />
            );
        }

        return null;
    }

    renderAnyGifts() {
        const { formValue } = this.props;

        const isAnyGifts = get(formValue, 'values.any_gifts', null);

        if (isAnyGifts) {
            return (
                <AnythingElse
                    open
                    openText="Please describe"
                    placeholder="[Some explanation about significant gifts...]"
                    name="any_gifts_details"
                />
            );
        }

        return null;
    }

    render() {
        const {
            formValue,
            handleBack,
            isSubmitted,
            handleSubmit,
        } = this.props;
        const isAnySyncErrors = isEmpty(get(formValue, 'syncErrors', {}));

        return (
            <Fragment>
                <FulfilledLine loadingPercent={10} lineColor="#1ED66C" />
                <section className="container dis-f">
                    <div className="col span-8">
                        <h2 className="fw-b mt-xlarge mb-mlarge">
                            Tell us about other legacy details
                        </h2>
                        <form className="details-form col span-8 dis-f fd-c">
                            <Field
                                component={YesNo}
                                componentClass="span-8"
                                name="any_inheritances_past"
                                // eslint-disable-next-line max-len
                                label="Have you [or Client 2] received any inheritances over the last 10 years?"
                                yesChecked={get(formValue, 'values.any_inheritances_past', null) === true}
                                normalize={normalizeBoolean}
                                noChecked={get(formValue, 'values.any_inheritances_past', null) === false}
                                validate={[
                                    booleanRequired,
                                ]}
                            />
                            {this.renderAnyInheritancesPast()}
                            <div className="hr" />
                            <Field
                                component={YesNo}
                                componentClass="span-8"
                                name="any_inheritances_future"
                                // eslint-disable-next-line max-len
                                label="Do you [or Client2] expect to receive any inheritances or windfalls over the next 10 years?"
                                yesChecked={get(formValue, 'values.any_inheritances_future', null) === true}
                                normalize={normalizeBoolean}
                                noChecked={get(formValue, 'values.any_inheritances_future', null) === false}
                                validate={[
                                    booleanRequired,
                                ]}
                            />
                            {this.renderAnyInheritancesFuture()}
                            <div className="hr" />
                            <Field
                                component={YesNo}
                                componentClass="span-8"
                                name="any_gifts"
                                // eslint-disable-next-line max-len
                                label="Have you [or Client2] made any significant gifts in the last 7 years to family or friends?"
                                yesChecked={get(formValue, 'values.any_gifts', null) === true}
                                normalize={normalizeBoolean}
                                noChecked={get(formValue, 'values.any_gifts', null) === false}
                                validate={[
                                    booleanRequired,
                                ]}
                            />
                            {this.renderAnyGifts()}
                        </form>
                    </div>
                </section>
                <BackNextButtons
                    isActiveBack={false}
                    onClickNext={handleSubmit}
                    isActiveNext={!isSubmitted || (isAnySyncErrors && isSubmitted)}
                    onClickBack={handleBack}
                />
            </Fragment>
        );
    }
}

LegacyDetails.propTypes = {
    handleBack: PropTypes.func.isRequired,
    handleSubmit: PropTypes.func.isRequired,
    handleGetMePlan: PropTypes.func.isRequired,
    isSubmitted: PropTypes.bool.isRequired,
    formValue: PropTypes.object,
};

LegacyDetails.defaultProps = {
    formValue: {},
};

const mapStateToProps = () => ({
    initialValues: {},
});

const LegacyDetailsWithHoc = SurveyHoc(LegacyDetails, LegacyDetails.surveyData);

export default withRouter(connect(mapStateToProps)(LegacyDetailsWithHoc));
