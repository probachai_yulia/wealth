import React, { Component, Fragment } from 'react';
import { Field } from 'redux-form';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import PropTypes from 'prop-types';
import get from 'lodash/get';
import isEmpty from 'lodash/isEmpty';
import { BackNextButtons } from '../../../../components/BackNextButtons';
import FulfilledLine from '../../../../components/FulfilledLine';
import {
    isEmail,
    isPhoneRequired,
    required,
} from '../../../../utils/validation.helper';
import SurveyHoc from '../../../hoc/SurveyHoc';
import {
    patchPlan,
} from '../../../../actions/PlanActions';
import InputCustom from '../../../../components/InputCustom';
import { TitleHeader } from '../../../../components/TitleHeader';
import {
    LoadableAddress,
} from '../../../../components/Loadable';

/**
 * Render the content for user idle. Dumb component
 * @return {JSX.Element}
 */

class AddPowerOfAttorney extends Component {
    static formName = 'addPowerOfAttorney';

    static surveyData = {
        formName: AddPowerOfAttorney.formName,
        nextStage: 'nextStage',
        prevStage: 'prevStage',
        data: [
            {
                fields: [],
                isUpdate: 'updatePlan',
                selector: 'planId',
                methodUpdate: patchPlan,
            },
        ],
    };

    constructor(props) {
        super(props);

        const {
            handleGetMePlan,
        } = this.props;

        handleGetMePlan();
    }

    render() {
        const {
            formValue,
            handleBack,
            isSubmitted,
            handleSubmit,
            countriesOptions,
        } = this.props;
        const isAnySyncErrors = isEmpty(get(formValue, 'syncErrors', {}));

        return (
            <Fragment>
                <FulfilledLine loadingPercent={10} lineColor="#1ED66C" />
                <TitleHeader title="Add power of attorney" />
                <section className="container dis-f">
                    <div className="col span-8 pt-xlarge">
                        <form className="details-form col span-8 dis-f fd-c">
                            <div className="dis-f span-8">
                                <div className="input-wrap col span-4">
                                    <Field
                                        component={InputCustom}
                                        label="First Name"
                                        type="text"
                                        name="first_name"
                                        placeholder="First name"
                                        isShowErrors={isSubmitted}
                                        validate={[
                                            required,
                                        ]}
                                    />
                                </div>
                                <div className="input-wrap col-last span-4">
                                    <Field
                                        component={InputCustom}
                                        label="Last Name"
                                        name="last_name"
                                        type="text"
                                        placeholder="Last name"
                                        isShowErrors={isSubmitted}
                                        validate={[
                                            required,
                                        ]}
                                    />
                                </div>
                            </div>
                            <div className="dis-f span-8 mt-1rem">
                                <Field
                                    name="phone"
                                    component={InputCustom}
                                    extraClasses="col span-4"
                                    label="Phone"
                                    type="number"
                                    placeholder="Phone"
                                    isShowErrors={isSubmitted}
                                    validate={[
                                        required,
                                        isPhoneRequired,
                                    ]}
                                />
                                <Field
                                    name="email"
                                    component={InputCustom}
                                    readOnly={false}
                                    extraClasses="col-last span-4"
                                    label="Email"
                                    type="text"
                                    placeholder="Email"
                                    isShowErrors={isSubmitted}
                                    validate={[
                                        required,
                                        isEmail,
                                    ]}
                                />
                            </div>
                            <LoadableAddress
                                isShowErrors={isSubmitted}
                                formName={AddPowerOfAttorney.formName}
                                postcode={get(formValue, 'values.postcode', null)}
                                selectAddress={get(formValue, 'values.select-address', null)}
                                label="What is your attorney's address?"
                                countriesOptions={countriesOptions}
                                isNotRequired
                            />
                        </form>
                    </div>
                </section>
                <BackNextButtons
                    isActiveBack={false}
                    onClickNext={handleSubmit}
                    isActiveNext={!isSubmitted || (isAnySyncErrors && isSubmitted)}
                    onClickBack={handleBack}
                />
            </Fragment>
        );
    }
}

AddPowerOfAttorney.propTypes = {
    handleBack: PropTypes.func.isRequired,
    handleSubmit: PropTypes.func.isRequired,
    handleGetMePlan: PropTypes.func.isRequired,
    isSubmitted: PropTypes.bool.isRequired,
    formValue: PropTypes.object,
    countriesOptions: PropTypes.arrayOf(PropTypes.object),
};

AddPowerOfAttorney.defaultProps = {
    formValue: {},
    countriesOptions: [],
};

const mapStateToProps = state => ({
    initialValues: {},
    countriesOptions: get(state, 'options.countries', []),
});

const AddPowerOfAttorneyWithHoc = SurveyHoc(AddPowerOfAttorney, AddPowerOfAttorney.surveyData);

export default withRouter(connect(mapStateToProps)(AddPowerOfAttorneyWithHoc));
