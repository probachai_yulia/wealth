import React, { Component, Fragment } from 'react';
import { Field } from 'redux-form';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import PropTypes from 'prop-types';
import get from 'lodash/get';
import isEmpty from 'lodash/isEmpty';
import { BackNextButtons } from '../../../../components/BackNextButtons';
import FulfilledLine from '../../../../components/FulfilledLine';
import { AddDashedButtonLink } from '../../../../components/AddDashedButtonLink';
import {
    booleanRequired,
} from '../../../../utils/validation.helper';
import SurveyHoc from '../../../hoc/SurveyHoc';
import {
    patchPlan,
} from '../../../../actions/PlanActions';
import YesNo from '../../../../components/YesNo';
import {
    normalizeBoolean,
} from '../../../../utils/helper';

/**
 * Render the content for user idle. Dumb component
 * @return {JSX.Element}
 */

class PowerOfAttorney extends Component {
    static formName = 'powerOfAttorney';

    static surveyData = {
        formName: PowerOfAttorney.formName,
        nextStage: 'nextStage',
        prevStage: 'prevStage',
        data: [
            {
                fields: [],
                isUpdate: 'updatePlan',
                selector: 'planId',
                methodUpdate: patchPlan,
            },
        ],
    };

    constructor(props) {
        super(props);

        this.renderUserAttorney = this.renderUserAttorney.bind(this);
        this.renderPartnerAttorney = this.renderPartnerAttorney.bind(this);

        const {
            handleGetMePlan,
        } = this.props;

        handleGetMePlan();
    }

    renderUserAttorney() {
        const { formValue } = this.props;

        const hasAttorney = get(formValue, 'values.has_power_of_attorney', null);

        if (hasAttorney) {
            return (
                <Fragment>
                    <h5 className="mb-medium">
                        Your power of attorney
                    </h5>
                    <AddDashedButtonLink
                        link="./add-power-of-attorney"
                        addName="+ Add Power of Attorney"
                    />
                </Fragment>
            );
        }

        return null;
    }

    renderPartnerAttorney() {
        const { formValue } = this.props;

        const hasAttorney = get(formValue, 'values.partner_has_power_of_attorney', null);

        if (hasAttorney) {
            return (
                <Fragment>
                    <h5 className="mb-medium">
                        [Client 2]&apos;s power of attorney
                    </h5>
                    <AddDashedButtonLink
                        link="./add-power-of-attorney"
                        addName="+ Add Power of Attorney"
                    />
                </Fragment>
            );
        }

        return null;
    }

    render() {
        const {
            formValue,
            handleBack,
            isSubmitted,
            handleSubmit,
        } = this.props;
        const isAnySyncErrors = isEmpty(get(formValue, 'syncErrors', {}));

        return (
            <Fragment>
                <FulfilledLine loadingPercent={10} lineColor="#1ED66C" />
                <section className="container dis-f">
                    <div className="col span-8">
                        <h2 className="fw-b mt-xlarge mb-mlarge">
                            Power of Attorney
                        </h2>
                        <form className="details-form col span-8 dis-f fd-c">
                            <Field
                                component={YesNo}
                                componentClass="span-8"
                                name="has_power_of_attorney"
                                label="Do you have a Power of Attorney in place?"
                                yesChecked={get(formValue, 'values.has_power_of_attorney', null) === true}
                                normalize={normalizeBoolean}
                                noChecked={get(formValue, 'values.has_power_of_attorney', null) === false}
                                validate={[
                                    booleanRequired,
                                ]}
                            />
                            {this.renderUserAttorney()}
                            <div className="hr mt-large" />
                            <Field
                                component={YesNo}
                                componentClass="span-8"
                                name="partner_has_power_of_attorney"
                                label="Does [Client 2] have the same Power of Attorney as you?"
                                yesChecked={get(formValue, 'values.partner_has_power_of_attorney', null) === true}
                                normalize={normalizeBoolean}
                                noChecked={get(formValue, 'values.partner_has_power_of_attorney', null) === false}
                                validate={[
                                    booleanRequired,
                                ]}
                            />
                            {this.renderPartnerAttorney()}
                        </form>
                    </div>
                </section>
                <BackNextButtons
                    isActiveBack={false}
                    onClickNext={handleSubmit}
                    isActiveNext={!isSubmitted || (isAnySyncErrors && isSubmitted)}
                    onClickBack={handleBack}
                />
            </Fragment>
        );
    }
}

PowerOfAttorney.propTypes = {
    handleBack: PropTypes.func.isRequired,
    handleSubmit: PropTypes.func.isRequired,
    handleGetMePlan: PropTypes.func.isRequired,
    isSubmitted: PropTypes.bool.isRequired,
    formValue: PropTypes.object,
};

PowerOfAttorney.defaultProps = {
    formValue: {},
};

const mapStateToProps = () => ({
    initialValues: {},
});

const PowerOfAttorneyWithHoc = SurveyHoc(PowerOfAttorney, PowerOfAttorney.surveyData);

export default withRouter(connect(mapStateToProps)(PowerOfAttorneyWithHoc));
