import React, { Component, Fragment } from 'react';
import { change, reduxForm } from 'redux-form';
import get from 'lodash/get';
import isEmpty from 'lodash/isEmpty';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { withRouter } from 'react-router-dom';

import { bindActionCreators } from 'redux';
import { BackNextButtons } from '../../../../components/BackNextButtons';
import FulfilledLine from '../../../../components/FulfilledLine';
import TableExtraInformation from '../../../../components/TableExtraInformation';
import { getMePlan } from '../../../../actions/UserActions';
import { requestsReset } from '../../../../actions/CommonActions';

// Pensions ISA Investments
class AddProductProviders extends Component {
    constructor(props) {
        super(props);

        this.state = {
            isSubmitted: false,
        };

        this.handleSubmit = this.handleSubmit.bind(this);
        this.clientAssets = this.clientAssets.bind(this);
        this.partnerAssets = this.partnerAssets.bind(this);
        this.activeNextButton = this.activeNextButton.bind(this);
        const {
            handleGetMePlan,
        } = this.props;

        handleGetMePlan();
    }

    componentDidUpdate() {
        const { isRequestsDone, handleGetMePlan, handleRequestsReset } = this.props;
        if (isRequestsDone !== 0) {
            handleRequestsReset();
            handleGetMePlan();
        }
    }

    handleSubmit = (event) => {
        const { formValue, history } = this.props;
        this.setState({ isSubmitted: true });
        if (!isEmpty(get(formValue, 'syncErrors', {})) || !this.activeNextButton()) {
            event.preventDefault();
        } else {
            history.push('/survey/submit-form');
        }
    };

    clientAssets = () => {
        const {
            assets, clientID, optionAssetType, optionBenefit,
        } = this.props;
        const arrayList = [];
        const assetList = [];
        optionAssetType.map((item) => {
            Object.keys(item).map((subitem) => {
                item[subitem].map((subsitem) => {
                    assetList.push(subsitem);
                    return null;
                });
                return null;
            });
            return null;
        });
        optionBenefit.map((item) => {
            assetList.push(item);
            return null;
        });
        assets.map((item) => {
            if (
                (
                    item.asset_type === 'cash_other' ||
                    item.asset_type.indexOf('isa') !== -1 ||
                    item.asset_type === 'benefit' ||
                    item.asset_type === 'contribution' ||
                    item.asset_type === 'annuity' ||
                    item.asset_type === 'pension'
                ) &&
                clientID === item.owners[0]
            ) {
                let icon;
                // let subtitle;
                let currentAssetName = assetList.find(subitem => subitem.code === item.asset_type || subitem.code === item.tax_benefit);
                if (item.asset_type === 'cash_other') {
                    icon = 'investments.svg';
                    currentAssetName = { name: item.asset_type_other };
                }
                if (item.asset_type.indexOf('isa') !== -1) {
                    icon = 'ISAs.svg';
                }
                if (item.asset_type === 'benefit' || item.asset_type === 'contribution' || item.asset_type === 'annuity' || item.asset_type === 'pension') {
                    icon = 'pensions.svg';
                }
                arrayList.push({
                    balance: `£ ${item.current_value.toLocaleString('en')}`,
                    policy_number: item.policy_number,
                    icon,
                    subtitle: typeof currentAssetName !== 'undefined' ? currentAssetName.name : '',
                    title: item.provider_name,
                    plan_asset_uuid: item.plan_asset_uuid,
                    assetType: item.asset_type,
                });
            }
            return null;
        });
        return arrayList;
    };

    partnerAssets = () => {
        const {
            assets, partnerID, optionAssetType, optionBenefit,
        } = this.props;
        const arrayList = [];
        const assetList = [];
        optionAssetType.map((item) => {
            Object.keys(item).map((subitem) => {
                item[subitem].map((subsitem) => {
                    assetList.push(subsitem);
                    return null;
                });
                return null;
            });
            return null;
        });
        optionBenefit.map((item) => {
            assetList.push(item);
            return null;
        });
        assets.map((item) => {
            if (
                (
                    item.asset_type === 'cash_other' ||
                    item.asset_type.indexOf('isa') !== -1 ||
                    item.asset_type === 'benefit' ||
                    item.asset_type === 'contribution' ||
                    item.asset_type === 'annuity' ||
                    item.asset_type === 'pension'
                ) &&
                partnerID === item.owners[0]
            ) {
                let icon;
                const currentAssetName = assetList.find(subitem => subitem.code === item.asset_type || subitem.code === item.tax_benefit);
                if (item.asset_type === 'cash_other') {
                    icon = 'investments.svg';
                }
                if (item.asset_type.indexOf('isa') !== -1) {
                    icon = 'ISAs.svg';
                }
                if (item.asset_type === 'benefit' || item.asset_type === 'contribution' || item.asset_type === 'annuity' || item.asset_type === 'pension') {
                    icon = 'pensions.svg';
                }
                arrayList.push({
                    balance: `£ ${item.current_value.toLocaleString('en')}`,
                    policy_number: item.policy_number,
                    icon,
                    subtitle: typeof currentAssetName !== 'undefined' ? currentAssetName.name : '',
                    title: item.provider_name,
                    plan_asset_uuid: item.plan_asset_uuid,
                    assetType: item.asset_type,
                });
            }
            return null;
        });
        return arrayList;
    };

    activeNextButton() {
        const { isSubmitted } = this.state;
        const {
            formValue,
            partnerFirstName,
            clientNIN,
            partnerNIN,
        } = this.props;
        const isAnySyncErrors = isEmpty(get(formValue, 'syncErrors', {}));
        if ((partnerFirstName && partnerNIN) || (!partnerFirstName && clientNIN)) {
            if (!isSubmitted || (isAnySyncErrors && isSubmitted)) {
                return true;
            }
        }
        return false;
    }

    render() {
        const {
            clientFirstName,
            partnerFirstName,
            clientID,
            partnerID,
            clientNIN,
            partnerNIN,
            formValue,
        } = this.props;
        return (
            <Fragment>
                <FulfilledLine loadingPercent={90} lineColor="#6E0A7B" />
                <div className="container dis-f mt-5rem mb-10rem">
                    <div className="col span-8">
                        <h2 className="fw-b mt-xlarge mb-medium">
                            One last thing...
                        </h2>
                        <p className="mb-small">
                            It would be very useful if you could provide us with some additional information about your existing investments. If you decide to work with us,
                            we&apos;ll need this information so we can get in touch with the providers.
                        </p>
                        <p>
                            It will accelerate the process to fill in the gaps now, but if you prefer just bring along your policy numbers to our meeting and we can capture the information then.
                        </p>

                        <h6 className="mt-small mb-small">
                            {clientFirstName}
’s missing information
                        </h6>
                        <TableExtraInformation
                            data={this.clientAssets()}
                            formName="addProductProviders"
                            profileUUID={clientID}
                            nin={clientNIN}
                        />
                        {
                            partnerFirstName
                                && (
                                    <Fragment>
                                        <h6 className="mt-small mb-small">
                                            {partnerFirstName}
’s missing information
                                        </h6>
                                        <TableExtraInformation
                                            data={this.partnerAssets()}
                                            formName="addProductProviders"
                                            formValue={formValue}
                                            profileUUID={partnerID}
                                            nin={partnerNIN}
                                        />
                                    </Fragment>
                                )
                        }
                    </div>
                    <div className="col span-3 col-last explain_text mt-large" />
                </div>
                <BackNextButtons
                    isActiveBack={false}
                    onClickNext={this.handleSubmit}
                    isActiveNext={this.activeNextButton()}
                    nextText={this.activeNextButton() ? 'Next' : 'Skip'}
                    buttonClass="btn-border-purple"
                    extraClass="span-8"
                />
            </Fragment>
        );
    }
}

AddProductProviders.propTypes = {
    formValue: PropTypes.object,
    history: PropTypes.object.isRequired,
    assets: PropTypes.arrayOf(PropTypes.object),
    handleGetMePlan: PropTypes.func,
    clientID: PropTypes.string,
    partnerID: PropTypes.string,
    clientFirstName: PropTypes.string,
    partnerFirstName: PropTypes.string,
    isRequestsDone: PropTypes.number,
    handleRequestsReset: PropTypes.func.isRequired,
    clientNIN: PropTypes.string,
    partnerNIN: PropTypes.string,
    optionAssetType: PropTypes.arrayOf(PropTypes.object),
    optionBenefit: PropTypes.arrayOf(PropTypes.object),
};

AddProductProviders.defaultProps = {
    handleGetMePlan: () => {},
    formValue: {},
    assets: [],
    isRequestsDone: 0,
    partnerNIN: null,
    clientNIN: null,
    clientID: null,
    partnerID: null,
    clientFirstName: null,
    partnerFirstName: null,
    optionAssetType: [],
    optionBenefit: [],
};

const initializeForm = reduxForm({
    form: 'addProductProviders',
})(AddProductProviders);

const mapStateToProps = state => ({
    formValue: get(state, 'form.addProductProviders', {}),
    isRequestsDone: get(state, 'common.isRequestsDone', 0),
    assets: get(state, 'plan.assets', []),
    clientID: get(state, 'plan.users.main.profile_uuid', ''),
    clientFirstName: get(state, 'plan.users.main.profile.first_name', ''),
    partnerID: get(state, 'plan.users.partner.profile_uuid', ''),
    partnerFirstName: get(state, 'plan.users.partner.profile.first_name', ''),
    clientNIN: get(state, 'plan.users.main.profile.national_insurance_nbr', null),
    partnerNIN: get(state, 'plan.users.partner.profile.national_insurance_nbr', null),
    optionAssetType: get(state, 'options.asset_types', []),
    optionBenefit: get(state, 'options.tax_benefits', []),
});

const mapDispatchToProps = dispatch => ({
    changeFieldValue: (field, value) => {
        dispatch(change(AddProductProviders.formName, field, value));
    },
    handleGetMePlan: bindActionCreators(getMePlan, dispatch),
    handleRequestsReset: bindActionCreators(requestsReset, dispatch),
});

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(initializeForm));
