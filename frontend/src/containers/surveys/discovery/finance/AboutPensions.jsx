/* eslint-disable max-len */
import React, { Component, Fragment } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { withRouter } from 'react-router-dom';

import get from 'lodash/get';
import { change } from 'redux-form';
import { bindActionCreators } from 'redux';
import { BackNextButtons } from '../../../../components/BackNextButtons';
import FulfilledLine from '../../../../components/FulfilledLine';

import FinanceAboutTable from '../../../../components/FinanceAboutTable';
import { AddDashedButtonLink } from '../../../../components/AddDashedButtonLink';
import ModalInformationIcon from '../../../../components/ModalInformationIcon';
import { getMePlan } from '../../../../actions/UserActions';

/**
 * Render the content for user idle. Dumb component
 *
 */
class AboutPensions extends Component {
    constructor(props) {
        super(props);

        this.handleBack = this.handleBack.bind(this);
        this.handleNext = this.handleNext.bind(this);
        this.getListSavings = this.getListSavings.bind(this);

        const {
            handleGetMePlan,
        } = this.props;

        handleGetMePlan();
    }

    getListSavings = () => {
        const { arrayData, users, radioButton } = this.props;
        const arrayList = [];
        arrayData.map((item) => {
            const regularContributionEmployer = [];
            const regularContributionPersonal = [];
            const regularContributionIncome = [];
            let taxBenefit = '';
            let user = {
                profile: { first_name: '' },
            };
            let imgUser = 'user.svg';
            Object.keys(users).map((userItem) => {
                // console.log(users[userItem], item.owners);
                if ((userItem === 'main' || userItem === 'partner') && users[userItem] !== null) {
                    if (item.owners.length > 1) {
                        user = { profile: { first_name: 'Joint' } };
                        imgUser = 'joint.svg';
                    } else if (users[userItem].profile_uuid === item.owners[0]) {
                        user = users[userItem];
                    }
                }
                if (typeof users[userItem][0] !== 'undefined') {
                    users[userItem].map((subitem) => {
                        if (subitem.profile_uuid === item.owners[0]) {
                            user = subitem;
                        }
                        return null;
                    });
                }
                return null;
            });

            item.regular_contributions.map((value) => {
                let frequency;
                switch (value.regular_contribution_type) {
                    case 'personal_contribution':
                        frequency = value.value_type === 'F' ? '£' : '%';
                        if (frequency === '£') {
                            regularContributionPersonal.push(`£ ${value.amount.toLocaleString('en')}`);
                        } else {
                            regularContributionPersonal.push(`${value.amount.toLocaleString('en')}% of base salary`);
                        }
                        break;
                    case 'employer_contribution':
                        frequency = value.value_type === 'F' ? '£' : '%';
                        if (frequency === '£') {
                            regularContributionEmployer.push(`£ ${value.amount.toLocaleString('en')}`);
                        } else {
                            regularContributionEmployer.push(`${value.amount.toLocaleString('en')}% of base salary`);
                        }
                        break;
                    case 'receive_income':
                        frequency = value.frequency === 'per_month' ? 'p.m.' : 'p.a.';
                        regularContributionIncome.push(`£ ${value.amount.toLocaleString('en')} ${frequency}`);
                        break;
                    default:
                        return null;
                }
                return null;
            });

            radioButton.map((taxBen) => {
                if (taxBen.code === item.asset_type) {
                    taxBenefit = taxBen.name;
                }
                return null;
            });
            if (item.asset_type === 'benefit' || item.asset_type === 'contribution' || item.asset_type === 'annuity' || item.asset_type === 'pension') {
                arrayList.push({
                    balance: `£ ${item.current_value.toLocaleString('en')}`,
                    clientName: user.profile.first_name,
                    client_image: imgUser,
                    extra_information: item.additional,
                    personal_contribution: regularContributionPersonal,
                    employer_contribution: regularContributionEmployer,
                    income_received: regularContributionIncome,
                    edit_link: `/survey/finance/add-pensions/${item.plan_asset_uuid}`,
                    icon: 'pensions.svg',
                    subtitle: item.asset_type === 'pension' ? 'Unknown pension type' : taxBenefit,
                    title: item.provider_name,
                });
            }
            return null;
        });
        return arrayList;
    };

    handleBack() {
        const { history } = this.props;
        history.push('assets');
    }

    handleNext() {
        const { history } = this.props;
        history.push('assets');
    }

    render() {
        return (
            <Fragment>
                <FulfilledLine loadingPercent={40} lineColor="#6E0A7B" />
                <div className="container dis-f mt-5rem mb-5rem">
                    <div className="col span-8">
                        <h2 className="fw-b mt-xlarge mb-mlarge">
                            Tell us about your pensions
                        </h2>
                        <h5 className="mb-2rem">
                            Pensions
                        </h5>

                        <FinanceAboutTable data={this.getListSavings()} section="pensions" />

                        <AddDashedButtonLink
                            link="/survey/finance/add-pensions"
                            className="mt-3rem btn_large border_dashed link_purple"
                            addName="+ Add a pension"
                        />

                    </div>
                    <div className="col span-3 col-last explain_text mt-4rem">
                        <ModalInformationIcon
                            title="DB or not DB?"
                            text="Low interest rates have meant that the transfer of a defined benefit pension to a defined contribution pension has often attracted high transfer values (sometimes 30 to 40 times annual DB income). However, its important to consider your personal situation alongside the offered transfer value as for some individuals it may be more beneficial to receive the guaranteed income offered by a DB pension."
                        />
                        <ModalInformationIcon
                            title="DC pension tax perks"
                            text="Did you know pensions are excluded from your estate for inheritance tax purposes? We can help you understand the most effective way to draw down in retirement under current rules.<br /><br />Up to 25% of your DC pension is tax free with the remainder of your pot taxed as income (liable to income tax)."
                        />
                    </div>
                </div>
                <BackNextButtons
                    isActiveBack={false}
                    onClickNext={this.handleNext}
                    onClickBack={this.handleBack}
                    extraClass="span-8"
                    buttonClass="btn-purple"
                />
            </Fragment>
        );
    }
}

AboutPensions.propTypes = {
    arrayData: PropTypes.arrayOf(PropTypes.object),
    history: PropTypes.object.isRequired,
    users: PropTypes.object,
    handleGetMePlan: PropTypes.func.isRequired,
    radioButton: PropTypes.arrayOf(PropTypes.object),
};

AboutPensions.defaultProps = {
    arrayData: [],
    users: {},
    radioButton: [],
};

const mapStateToProps = (state) => {
    let radioButton;
    get(state, 'options.asset_types', []).map((item) => {
        if (typeof item.pension !== 'undefined') {
            radioButton = item.pension;
        }
        return null;
    });
    return ({
        arrayData: get(state, 'plan.assets', []),
        users: get(state, 'plan.users', {}),
        radioButton: radioButton || [],
    });
};

const mapDispatchToProps = dispatch => ({
    changeFieldValue: (field, value) => {
        dispatch(change('addInvestments', field, value));
    },
    handleGetMePlan: bindActionCreators(getMePlan, dispatch),
});

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(AboutPensions));
