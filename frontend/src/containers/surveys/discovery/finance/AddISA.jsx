import React, { Component, Fragment } from 'react';
import { change, Field } from 'redux-form';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import get from 'lodash/get';
import PropTypes from 'prop-types';
import isEmpty from 'lodash/isEmpty';
import {
    isNumber,
    maxValue20k,
    positiveNumber,
    required,
    requiredMoney,
    requiredYesNo,
} from '../../../../utils/validation.helper';
import InputCustom from '../../../../components/InputCustom';
import Select from '../../../../components/Select';
import YesNoNotSure from '../../../../components/YesNoNotSure';
import { InputPound } from '../../../../components/InputPound';
import RadioBlocks from '../../../../components/RadioBlocks';
import AnythingElse from '../../../../components/AnythingElse';
import Radio from '../../../../components/Radio';
import SurveyHoc from '../../../hoc/SurveyHoc';
import { addPlanAsset, updatePlanAsset } from '../../../../actions/PlanActions';
import { BackNextButtons } from '../../../../components/BackNextButtons';
import UploadAssetsStatement from '../../../../components/UploadAssetsStatement';
import { TitleHeader } from '../../../../components/TitleHeader';

/**
 * Render the content for user idle. Dumb component
 *
 */
class AddISA extends Component {
    static formName = 'addISA';

    static surveyData = {
        formName: AddISA.formName,
        nextStage: '/survey/finance/about-isa',
        prevStage: 'prevStage',
        data: [
            {
                fields: [
                    'owners',
                    'asset_type',
                    'provider_name',
                    'current_value',
                    'is_not_sure_regular_contribution',
                    'is_not_sure_asset_type',
                    'is_not_sure',
                    'regular_contribution',
                    'additional',
                    'plan_asset_uuid',
                    'update',
                    'plan_document_uuids',
                ],
                // additionalFields: [],
                isUpdate: 'update',
                selector: 'planUUID',
                methodCreate: addPlanAsset,
                methodUpdate: updatePlanAsset,
            },
        ],
    };

    constructor(props) {
        super(props);

        this.renderTypeRadioBtn = this.renderTypeRadioBtn.bind(this);
        this.renderConditionalBlock = this.renderConditionalBlock.bind(this);
        this.renderSaveMoneyBlock = this.renderSaveMoneyBlock.bind(this);

        const {
            handleGetMePlan,
        } = this.props;

        handleGetMePlan();
    }

    componentDidUpdate() {
        const { formValue, changeFieldValue, usersOptions } = this.props;
        if (get(formValue, 'values.owner', '') === 'joint') {
            changeFieldValue('owners', [usersOptions.main.profile_uuid, usersOptions.partner.profile_uuid]);
            changeFieldValue('is_tenants_in_common', false);
        } else if (get(formValue, 'values.owner', '') === 'tennants') {
            changeFieldValue('is_tenants_in_common', true);
            changeFieldValue('owners', '');
        } else {
            changeFieldValue('owners', [get(formValue, 'values.owner', '')]);
            changeFieldValue('is_tenants_in_common', false);
        }
        switch (get(formValue, 'values.is_not_sure_regular_contribution_info', '')) {
            case '1':
            case '2':
                changeFieldValue('is_not_sure_regular_contribution', 'false');
                break;
            case '3':
                changeFieldValue('is_not_sure_regular_contribution', 'true');
                break;
            default:
                changeFieldValue('is_not_sure_regular_contribution', 'true');
        }
        if (get(formValue, 'values.asset_type_info', '') === 'Not sure') {
            changeFieldValue('asset_type', 'isa');
            changeFieldValue('is_not_sure_asset_type', true);
        } else {
            changeFieldValue('asset_type', get(formValue, 'values.asset_type_info', ''));
            changeFieldValue('is_not_sure_asset_type', false);
        }
        changeFieldValue('regular_contribution', [{
            amount: get(formValue, 'values.regular_contribution_info', 0), frequency: 'per_year', value_type: 'F', regular_contribution_type: 'personal_contribution',
        }]);
        if (get(formValue, 'values.files', []).length !== get(formValue, 'values.plan_document_uuids', []).length) {
            get(formValue, 'values.files', []).map(() => {
                changeFieldValue('uploadingFiles', true);
                return null;
            });
            setTimeout(() => {
                const fileList = [];
                get(formValue, 'values.files', []).map((item, i) => {
                    fileList[i] = item.planDocumentUuids;
                    if (get(formValue, 'values.files', []).length - 1 === i) {
                        changeFieldValue('uploadingFiles', false);
                    }
                    return null;
                });
                changeFieldValue('plan_document_uuids', fileList);
            }, 2000);
        }
    }

    optionowner = () => {
        const ownersList = []; let id = 0;
        const {
            usersOptions,
        } = this.props;
        Object.keys(usersOptions).map((item) => {
            if (usersOptions[item] !== null) {
                if (item === 'main' || item === 'partner') {
                    ownersList.push({
                        id,
                        code: usersOptions[item].profile_uuid,
                        name: usersOptions[item].profile.first_name,
                        dob: usersOptions[item].profile.dob,
                        role: item,
                    });
                    id += 1;
                }
                if (item === 'child') {
                    usersOptions[item].map((subitem) => {
                        ownersList.push({
                            id,
                            code: subitem.profile_uuid,
                            name: subitem.profile.first_name,
                            dob: subitem.profile.dob,
                            role: item,
                        });
                        id += 1;
                        return null;
                    });
                }
            }
            return null;
        });

        return ownersList;
    };

    blockFinance() {
        const { formValue, radioButton } = this.props;
        let financeIsa = [];
        const selUser = this.optionowner().find(item => item.code === get(formValue, 'values.owner', ''));
        if (typeof selUser !== 'undefined') {
            if (selUser.dob !== null) {
                const birthYear = new Date(selUser.dob);
                const ageDifMs = Date.now() - birthYear.getTime();
                const ageDate = new Date(ageDifMs); // miliseconds from epoch
                const age = Math.abs(ageDate.getUTCFullYear() - 1970);
                radioButton.map((item) => {
                    if (item.code !== 'isa_junior' && age >= 18) {
                        financeIsa.push({
                            id: item.id,
                            name: item.name,
                            description: item.description,
                            code: item.code,
                        });
                    } else if (item.code === 'isa_junior' && age < 18) {
                        financeIsa.push({
                            id: item.id,
                            name: item.name,
                            description: item.description,
                            code: item.code,
                        });
                    }
                    return null;
                });
            } else {
                financeIsa = radioButton;
            }
        } else {
            financeIsa = radioButton;
        }
        return financeIsa;
    }

    renderTypeRadioBtn() {
        const { formValue, isSubmitted } = this.props;
        const selectedValue = get(formValue, 'values.asset_type_info', null);
        const valueNotSure = 'Not sure';
        return (
            <Fragment>
                <div className="dis-f span-8 no-padding">
                    <Field
                        component={RadioBlocks}
                        blocks={this.blockFinance()}
                        label="What type of ISA is this?"
                        name="asset_type_info"
                        fieldsName="asset_type_info"
                        extraClasses="pb-0"
                        selectedValue={selectedValue}
                        isShowErrors={isSubmitted}
                        validate={[
                            requiredYesNo,
                        ]}
                    />
                </div>
                <Field
                    component={Radio}
                    extraClasses={`span-4 ${selectedValue === valueNotSure ? '' : 'mb-2rem'}`}
                    name="asset_type_info"
                    fieldValue={valueNotSure}
                    label="I’m not sure"
                    checked={selectedValue === valueNotSure}
                    isShowErrors={isSubmitted}
                    validate={[
                        requiredYesNo,
                    ]}
                />
                {selectedValue === valueNotSure
                && (
                    <p className="mb-2rem greyText">
                        No problem, we can work out the finer details later.
                    </p>
                )
                }
            </Fragment>
        );
    }

    renderConditionalBlock() {
        const { formValue, isSubmitted } = this.props;
        const selectedRadio = get(formValue, 'values.asset_type_info', null);
        if (selectedRadio) {
            return (
                <Fragment>
                    <div className="input-wrap col span-8">
                        <Field
                            component={InputCustom}
                            name="provider_name"
                            label="Provider name"
                            placeholder="Provider name"
                            extraClasses="col span-4 inline"
                            isShowErrors={isSubmitted}
                            validate={[
                                required,
                            ]}
                        />
                        <Field
                            label="Current value"
                            component={InputPound}
                            name="current_value"
                            isShowErrors={isSubmitted}
                            extraClasses="col span-4 col-last"
                            validate={[
                                requiredMoney,
                                isNumber,
                                positiveNumber,
                            ]}
                        />
                    </div>
                    <Field
                        component={YesNoNotSure}
                        extraClasses="span-2"
                        name="is_not_sure_regular_contribution_info"
                        label="Will you save money this tax year in this ISA?"
                        yesChecked={get(formValue, 'values.is_not_sure_regular_contribution_info', '0') === '1'}
                        noChecked={get(formValue, 'values.is_not_sure_regular_contribution_info', '0') === '2'}
                        notSureChecked={get(formValue, 'values.is_not_sure_regular_contribution_info', '0') === '3'}
                        isShowErrors={isSubmitted}
                        validate={[
                            required,
                        ]}
                    />
                    {this.renderSaveMoneyBlock()}
                    <AnythingElse open={get(formValue, 'values.additional', null) !== null} name="additional" />
                </Fragment>
            );
        }
        return null;
    }

    renderSaveMoneyBlock() {
        const { formValue, isSubmitted } = this.props;
        if (get(formValue, 'values.is_not_sure_regular_contribution_info', '0') === '1') {
            return (
                <Field
                    label="How much?"
                    component={InputPound}
                    name="regular_contribution_info"
                    isShowErrors={isSubmitted}
                    extraClasses="col span-4 col-last mb-2rem"
                    hintMessage="Keep in mind, £20,000 is the limit for all products execpt Junior ISA which is £4,260."
                    validate={[
                        requiredMoney,
                        isNumber,
                        maxValue20k,
                        positiveNumber,
                    ]}
                />
            );
        }
        return null;
    }

    render() {
        const {
            handleSubmit, isSubmitted, formValue, handleBack,
        } = this.props;
        const isAnySyncErrors = isEmpty(get(formValue, 'syncErrors', {}));
        let activeSubmit = !isSubmitted || (isAnySyncErrors && isSubmitted);
        activeSubmit = get(formValue, 'values.uploadingFiles', false) === true ? false : activeSubmit;
        return (
            <Fragment>
                <TitleHeader
                    title="Add investment"
                />
                <div className="container dis-f fd-c pt-large">
                    <form className="know-you-form col span-8 dis-f fd-c">
                        <div className="dis-f col-last span-8">
                            <Field
                                component={Select}
                                extraClasses="span-4"
                                name="owner"
                                label="Who is the owner of this ISA?"
                                placeholder="Owner"
                                options={this.optionowner()}
                                isShowErrors={isSubmitted}
                            />
                        </div>
                        {this.renderTypeRadioBtn()}
                        {this.renderConditionalBlock()}
                        <Field
                            component={UploadAssetsStatement}
                            disabled={false}
                            name="files"
                            isShowErrors={isSubmitted}
                        />
                    </form>
                </div>
                <BackNextButtons
                    isActiveBack={false}
                    onClickNext={handleSubmit}
                    isActiveNext={activeSubmit}
                    onClickBack={handleBack}
                    buttonClass="btn-purple"
                    nextText="Save"
                />
            </Fragment>
        );
    }
}

AddISA.propTypes = {
    changeFieldValue: PropTypes.func.isRequired,
    formValue: PropTypes.object,
    handleSubmit: PropTypes.func.isRequired,
    isSubmitted: PropTypes.bool.isRequired,
    handleBack: PropTypes.func.isRequired,
    usersOptions: PropTypes.object,
    handleGetMePlan: PropTypes.func.isRequired,
    radioButton: PropTypes.arrayOf(PropTypes.object),
};

AddISA.defaultProps = {
    formValue: {},
    usersOptions: {},
    radioButton: [],
};

const mapStateToProps = (state, ownProps) => {
    const assets = get(state, 'plan.assets', []);
    let id;
    let isNotSureRegularContribution;
    let regularContrinution;
    let assetType;
    assets.map((item, i) => {
        if (item.plan_asset_uuid === ownProps.match.params.plan_uuid) {
            id = i;
            if (get(state, `plan.assets[${id}].is_not_sure_regular_contribution`, '') === true) {
                isNotSureRegularContribution = '3';
            } else if (get(state, `plan.assets[${id}].regular_contributions`, []).amount > 0) {
                isNotSureRegularContribution = '1';
                regularContrinution = get(state, `plan.assets[${id}].regular_contributions`, '')[0].amount;
            } else {
                isNotSureRegularContribution = '2';
            }
            if (get(state, `plan.assets[${id}].is_not_sure_asset_type`, '') === true) {
                assetType = 'Not sure';
            } else {
                assetType = get(state, `plan.assets[${id}].asset_type`, '');
            }
        }
        return null;
    });
    let radioButton;
    get(state, 'options.asset_types', []).map((item) => {
        if (typeof item.isa !== 'undefined') {
            radioButton = item.isa;
        }
        return null;
    });
    return ({
        initialValues: {
            planUUID: get(state, `plan.assets[${id}].plan_asset_uuid`, null),
            update: get(state, `plan.assets[${id}].plan_asset_uuid`, null) !== null,
            asset_type_info: assetType,
            is_not_sure: 'false',
            additional: get(state, `plan.assets[${id}].additional`, null),
            is_not_sure_regular_contribution_info: isNotSureRegularContribution,
            regular_contribution: get(state, `plan.assets[${id}].regular_contributions`, []),
            regular_contribution_info: regularContrinution,
            owners: get(state, `plan.assets[${id}].owners`, []),
            owner: get(state, `plan.assets[${id}].owners[0]`, ''),
            current_value: get(state, `plan.assets[${id}].current_value`, '').toString(),
            provider_name: get(state, `plan.assets[${id}].provider_name`, ''),
            plan_asset_uuid: get(state, `plan.assets[${id}].plan_asset_uuid`, '').toString(),
            nextStage: '/survey/finance/about-isa',
            prevStage: '/survey/finance/assets',
            uploadingFiles: false,
        },
        enableReinitialize: true,
        usersOptions: get(state, 'plan.users', {}),
        assets: get(state, 'plan.assets', []),
        radioButton: radioButton || [],
    });
};

const mapDispatchToProps = dispatch => ({
    changeFieldValue: (field, value) => {
        dispatch(change(AddISA.formName, field, value));
    },
});

const AddISAWithHoc = SurveyHoc(AddISA, AddISA.surveyData);

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(AddISAWithHoc));
