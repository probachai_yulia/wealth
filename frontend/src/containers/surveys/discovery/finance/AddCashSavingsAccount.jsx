import React, { Component, Fragment } from 'react';
import { change, Field } from 'redux-form';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import get from 'lodash/get';
import PropTypes from 'prop-types';
import isEmpty from 'lodash/isEmpty';
import {
    booleanRequired,
    isNumber,
    positiveNumber,
    required,
    requiredMoney,
    requiredYesNo,
} from '../../../../utils/validation.helper';
import MultipleMoneyBlock from '../../../../components/MultipleMoneyBlock';
import InputCustom from '../../../../components/InputCustom';
import Select from '../../../../components/Select';
import YesNo from '../../../../components/YesNo';
import { InputPound } from '../../../../components/InputPound';
import RadioBlocks from '../../../../components/RadioBlocks';
import Radio from '../../../../components/Radio';
import AnythingElse from '../../../../components/AnythingElse';
import SurveyHoc from '../../../hoc/SurveyHoc';
import { addPlanAsset, updatePlanAsset } from '../../../../actions/PlanActions';
import { normalizeBoolean } from '../../../../utils/helper';
import { BackNextButtons } from '../../../../components/BackNextButtons';
import { TitleHeader } from '../../../../components/TitleHeader';
import UploadAssetsStatement from '../../../../components/UploadAssetsStatement';

/**
 * Render the content for user idle. Dumb component
 *
 */
class AddCashSavingsAccount extends Component {
    static contextTypes = {
        redux: PropTypes.object,
    };

    static formName = 'addCashSavingsAccount';

    static surveyData = {
        formName: AddCashSavingsAccount.formName,
        nextStage: 'nextStage',
        prevStage: 'prevStage',
        data: [
            {
                fields: [
                    'additional',
                    'owners',
                    'asset_type',
                    'cash_saving_account',
                    'is_not_sure_asset_type',
                    'provider_name',
                    'current_value',
                    'is_not_sure_regular_contribution',
                    'regular_contribution',
                    'is_not_sure',
                    'tax_benefit',
                    'update',
                    'plan_document_uuids',
                ],
                isUpdate: 'update',
                // additionalFields: [],
                selector: 'planUUID',
                methodCreate: addPlanAsset,
                methodUpdate: updatePlanAsset,
            },
        ],
    };

    constructor(props) {
        super(props);

        this.renderTypeRadioBtn = this.renderTypeRadioBtn.bind(this);
        this.renderConditionalBlock = this.renderConditionalBlock.bind(this);
        this.renderRegularAccount = this.renderRegularAccount.bind(this);

        const {
            handleGetMePlan,
        } = this.props;

        handleGetMePlan();
    }

    componentDidUpdate() {
        const { formValue, changeFieldValue, usersOptions } = this.props;
        if (get(formValue, 'values.owner', '') === 'joint') {
            changeFieldValue('owners', [usersOptions.main.profile_uuid, usersOptions.partner.profile_uuid]);
            changeFieldValue('is_tenants_in_common', false);
        } else {
            changeFieldValue('owners', [get(formValue, 'values.owner', '')]);
        }
        if (get(formValue, 'values.asset_type_info', '') === 'Not sure') {
            changeFieldValue('asset_type', 'cash');
            changeFieldValue('is_not_sure_asset_type', true);
        } else {
            changeFieldValue('asset_type', get(formValue, 'values.asset_type_info', ''));
            changeFieldValue('is_not_sure_asset_type', false);
        }
        if (get(formValue, 'values.files', []).length !== get(formValue, 'values.plan_document_uuids', []).length) {
            get(formValue, 'values.files', []).map(() => {
                changeFieldValue('uploadingFiles', true);
                return null;
            });
            setTimeout(() => {
                const fileList = [];
                get(formValue, 'values.files', []).map((item, i) => {
                    fileList[i] = item.planDocumentUuids;
                    if (get(formValue, 'values.files', []).length - 1 === i) {
                        changeFieldValue('uploadingFiles', false);
                    }
                    return null;
                });
                changeFieldValue('plan_document_uuids', fileList);
            }, 2000);
        }
    }

    optionOwnerAccount = () => {
        const ownersList = [];
        let id = 0;
        const {
            usersOptions,
        } = this.props;
        Object.keys(usersOptions).map((item) => {
            if (usersOptions[item] !== null) {
                if (item === 'main') {
                    ownersList.push({
                        id,
                        code: usersOptions[item].profile_uuid,
                        name: usersOptions[item].profile.first_name,
                        dob: usersOptions[item].profile.dob,
                        role: item,
                    });
                    id += 1;
                }
                if (item === 'partner') {
                    ownersList.push({
                        id,
                        code: usersOptions[item].profile_uuid,
                        name: usersOptions[item].profile.first_name,
                        dob: usersOptions[item].profile.dob,
                        role: item,
                    });
                    id += 1;
                    ownersList.push({
                        id: ownersList.length + 1,
                        code: 'joint',
                        name: 'Joint',
                    });
                }
                if (item === 'child') {
                    usersOptions[item].map((subitem) => {
                        ownersList.push({
                            id,
                            code: subitem.profile_uuid,
                            name: subitem.profile.first_name,
                            dob: subitem.profile.dob,
                            role: item,
                        });
                        id += 1;
                        return null;
                    });
                }
            }
            return null;
        });

        return ownersList;
    };

    renderRegularAccount() {
        const { formValue, isSubmitted } = this.props;
        if (get(formValue, 'values.is_not_sure_regular_contribution', null) === true) {
            return (
                <MultipleMoneyBlock
                    label="How much do you spend on childcare excluding nanny costs?"
                    name="regular_contribution"
                    poundFieldName="regular_contribution"
                    selectFieldName="childcareSpendPeriod"
                    extraClasses="mt-2rem"
                    isShowErrors={isSubmitted}
                    typeFieldName="cash_saving"
                />
            );
        }
        return null;
    }

    renderConditionalBlock() {
        const { formValue, isSubmitted } = this.props;
        const isIndeterminatePeriod = get(formValue, 'values.isIndeterminatePeriod', false);
        if (get(formValue, 'values.asset_type_info', '0') && get(formValue, 'values.asset_type_info', '0') !== '0') {
            return (
                <Fragment>
                    <div className="input-wrap col span-8">
                        <Field
                            component={InputCustom}
                            name="provider_name"
                            label="Provider name"
                            placeholder="Provider name"
                            extraClasses="col span-4 inline"
                            disabled={isIndeterminatePeriod}
                            isShowErrors={isSubmitted}
                            validate={[
                                required,
                            ]}
                        />
                        <Field
                            label="Estimated value"
                            component={InputPound}
                            name="current_value"
                            isShowErrors={isSubmitted}
                            extraClasses="col span-4 col-last"
                            validate={[
                                requiredMoney,
                                isNumber,
                                positiveNumber,
                            ]}
                        />
                    </div>
                    <div className="hr span-8" />
                    <Field
                        component={YesNo}
                        name="is_not_sure_regular_contribution"
                        label="Do you save regularly in this account?"
                        yesChecked={get(formValue, 'values.is_not_sure_regular_contribution', null) === true}
                        noChecked={get(formValue, 'values.is_not_sure_regular_contribution', null) === false}
                        isShowErrors={isSubmitted}
                        normalize={normalizeBoolean}
                        validate={[
                            booleanRequired,
                        ]}
                    />
                    {this.renderRegularAccount()}
                    <AnythingElse name="additional" />
                </Fragment>
            );
        }
        return null;
    }

    renderTypeRadioBtn() {
        const { formValue, isSubmitted, radioButton } = this.props;
        const selectedValue = get(formValue, 'values.asset_type_info', null);
        const valueNotSure = 'Not sure';
        return (
            <Fragment>
                <div className="dis-f span-8 no-padding">
                    <Field
                        component={RadioBlocks}
                        blocks={radioButton}
                        label="What type of cash saving account is this?"
                        name="asset_type_info"
                        fieldsName="asset_type_info"
                        extraClasses="pb-0"
                        selectedValue={selectedValue}
                        isShowErrors={isSubmitted}
                        validate={[
                            requiredYesNo,
                        ]}
                    />
                </div>
                <Field
                    component={Radio}
                    extraClasses={`span-4 ${selectedValue === valueNotSure ? '' : 'mb-2rem'}`}
                    name="asset_type_info"
                    fieldValue={valueNotSure}
                    label="I’m not sure"
                    checked={selectedValue === valueNotSure}
                    isShowErrors={isSubmitted}
                    validate={[
                        requiredYesNo,
                    ]}
                />
                {selectedValue === valueNotSure
                && (
                    <p className="mb-2rem greyText">
                        No problem, we can work out the finer details later.
                    </p>
                )
                }
            </Fragment>
        );
    }

    render() {
        const {
            handleSubmit, isSubmitted, formValue, handleBack,
        } = this.props;
        const isAnySyncErrors = isEmpty(get(formValue, 'syncErrors', {}));
        let activeSubmit = !isSubmitted || (isAnySyncErrors && isSubmitted);
        activeSubmit = get(formValue, 'values.uploadingFiles', false) === true ? false : activeSubmit;
        return (
            <Fragment>
                <TitleHeader
                    title="Add investment"
                />
                <div className="container dis-f fd-c pt-large">
                    <form className="know-you-form col span-8 dis-f fd-c">
                        <div className="dis-f col-last span-8">
                            <Field
                                component={Select}
                                extraClasses="span-4"
                                name="owner"
                                label="Select owner"
                                placeholder="Owner"
                                options={this.optionOwnerAccount()}
                                isShowErrors={isSubmitted}
                            />
                        </div>
                        {this.renderTypeRadioBtn()}
                        {this.renderConditionalBlock()}
                        <Field
                            component={UploadAssetsStatement}
                            disabled={false}
                            name="files"
                            isShowErrors={isSubmitted}
                        />
                    </form>
                </div>
                <BackNextButtons
                    isActiveBack={false}
                    onClickNext={handleSubmit}
                    isActiveNext={activeSubmit}
                    onClickBack={handleBack}
                    buttonClass="btn-purple"
                    nextText="Save"
                />
            </Fragment>
        );
    }
}

AddCashSavingsAccount.propTypes = {
    changeFieldValue: PropTypes.func.isRequired,
    formValue: PropTypes.object,
    handleSubmit: PropTypes.func.isRequired,
    isSubmitted: PropTypes.bool.isRequired,
    handleBack: PropTypes.func.isRequired,
    handleGetMePlan: PropTypes.func.isRequired,
    usersOptions: PropTypes.object,
    radioButton: PropTypes.arrayOf(PropTypes.object),
};

AddCashSavingsAccount.defaultProps = {
    formValue: {},
    usersOptions: {},
    radioButton: [],
};

const mapStateToProps = (state, ownProps) => {
    const assets = get(state, 'plan.assets', []);
    let id;
    let assetType;
    assets.map((item, i) => {
        if (item.plan_asset_uuid === ownProps.match.params.plan_uuid) {
            id = i;
            if (get(state, `plan.assets[${id}].is_not_sure_asset_type`, '') === true) {
                assetType = 'Not sure';
            } else {
                assetType = get(state, `plan.assets[${id}].asset_type`, '');
            }
        }
        return null;
    });
    const owner = get(state, `plan.assets[${id}].owners`, []).length > 1 ? 'joint' : get(state, `plan.assets[${id}].owners[0]`, '');
    let radioButton;
    get(state, 'options.asset_types', []).map((item) => {
        if (typeof item.cash !== 'undefined') {
            radioButton = item.cash;
        }
        return null;
    });
    return ({
        initialValues: {
            additional: get(state, `plan.assets[${id}].additional`, null),
            planUUID: get(state, `plan.assets[${id}].plan_asset_uuid`, null),
            update: get(state, `plan.assets[${id}].plan_asset_uuid`, null) !== null,
            asset_type_info: assetType,
            regular_contribution: get(state, `plan.assets[${id}].regular_contributions`, [{
                amount: 0, frequency: 'per_month', value_type: 'F', regular_contribution_type: 'cash_saving',
            }]),
            owners: get(state, `plan.assets[${id}].owners`, []),
            owner,
            current_value: get(state, `plan.assets[${id}].current_value`, ''),
            asset_type_info_other: get(state, `plan.assets[${id}].asset_type_info_other`, ''),
            provider_name: get(state, `plan.assets[${id}].provider_name`, ''),
            is_not_sure_asset_type: get(state, `plan.assets[${id}].is_not_sure_asset_type`, ''),
            plan_asset_uuid: get(state, `plan.assets[${id}].plan_asset_uuid`, ''),
            is_not_sure_regular_contribution: get(state, `plan.assets[${id}].is_not_sure_regular_contribution`, null),
            nextStage: '/survey/finance/about-cash-savings-account',
            prevStage: '/survey/finance/assets',
            uploadingFiles: false,
        },
        enableReinitialize: true,
        usersOptions: get(state, 'plan.users', {}),
        assets: get(state, 'plan.assets', []),
        radioButton: radioButton || [],
    });
};

const mapDispatchToProps = dispatch => ({
    changeFieldValue: (field, value) => {
        dispatch(change('addCashSavingsAccount', field, value));
    },
});

const AddCashSavingsAccountWithHoc = SurveyHoc(AddCashSavingsAccount, AddCashSavingsAccount.surveyData);

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(AddCashSavingsAccountWithHoc));
