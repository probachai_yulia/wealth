/* eslint-disable max-len */
import React, { Component, Fragment } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { withRouter } from 'react-router-dom';

import get from 'lodash/get';
import { change } from 'redux-form';
import { bindActionCreators } from 'redux';
import { BackNextButtons } from '../../../../components/BackNextButtons';
import FulfilledLine from '../../../../components/FulfilledLine';

import FinanceAboutTable from '../../../../components/FinanceAboutTable';
import { AddDashedButtonLink } from '../../../../components/AddDashedButtonLink';
import { getMePlan } from '../../../../actions/UserActions';
import ModalInformationIcon from '../../../../components/ModalInformationIcon';

/**
 * Render the content for user idle. Dumb component
 *
 */
class AboutInvestments extends Component {
    constructor(props) {
        super(props);

        this.handleBack = this.handleBack.bind(this);
        this.handleNext = this.handleNext.bind(this);
        this.getListSavings = this.getListSavings.bind(this);

        const {
            handleGetMePlan,
        } = this.props;

        handleGetMePlan();
    }

    getListSavings = () => {
        const { arrayData, users, radioButton } = this.props;
        const arrayList = [];
        arrayData.map((item) => {
            const regularContribution = [];
            let taxBenefit = '';
            let title;
            let user = {
                profile: { display_name: '' },
            };
            let imgUser = 'user.svg';
            Object.keys(users).map((userItem) => {
                // console.log(users[userItem], item.owners);
                if ((userItem === 'main' || userItem === 'partner') && users[userItem] !== null) {
                    if (item.owners.length > 1) {
                        user = { profile: { first_name: 'Joint' } };
                        imgUser = 'joint.svg';
                    } else if (users[userItem].profile_uuid === item.owners[0]) {
                        user = users[userItem];
                    }
                }
                if (typeof users[userItem][0] !== 'undefined') {
                    users[userItem].map((subitem) => {
                        if (subitem.profile_uuid === item.owners[0]) {
                            user = subitem;
                        }
                        return null;
                    });
                }
                return null;
            });
            item.regular_contributions.map((value) => {
                const frequency = value.frequency === 'per_month' ? 'p.m.' : 'p.a.';
                regularContribution.push(`£ ${value.amount.toLocaleString('en')} ${frequency}`);
                return null;
            });

            radioButton.map((taxBen) => {
                if (taxBen.code === item.tax_benefit) {
                    taxBenefit = taxBen.name;
                } else if (item.tax_benefit === 'unknown') {
                    taxBenefit = 'Unknown';
                }
                return null;
            });
            if (item.is_self_managed) {
                title = 'Self-managed';
            } else {
                title = item.provider_name;
            }
            if (item.asset_type === 'cash_other') {
                arrayList.push({
                    balance: item.current_value !== null ? `£ ${item.current_value.toLocaleString('en')}` : '£ 0',
                    clientName: user.profile.first_name,
                    client_image: imgUser,
                    contribution: regularContribution,
                    extra_information: item.additional,
                    tax_scheme: [taxBenefit],
                    edit_link: `/survey/finance/add-investments/${item.plan_asset_uuid}`,
                    icon: 'investments.svg',
                    subtitle: item.asset_type_other,
                    title,
                });
            }
            return null;
        });
        return arrayList;
    };

    handleBack() {
        const { history } = this.props;
        history.push('assets');
    }

    handleNext() {
        const { history } = this.props;
        history.push('assets');
    }

    renderTextInformation() {
        const {
            retirement,
        } = this.props;
        if (retirement && retirement > 0) {
            return (
                <ModalInformationIcon
                    title="Choosing wisely"
                    text=" It's essential to have the right mix of investments to give you both growth potential and tax efficiency. Picking the right assets can make a big difference: we helped one client in Hammersmith retire five years earlier after restructuring their portfolio."
                />
            );
        }
        return (
            <ModalInformationIcon
                title="Investment pick n’ mix"
                text=" We helped a retired client increase their after tax income by £1000 per month by restructuring their investments. The mix of investments you hold can have a significant impact on your overall wealth through a combination of growth and tax efficiency."
            />
        );
    }

    render() {
        return (
            <Fragment>
                <FulfilledLine loadingPercent={40} lineColor="#6E0A7B" />
                <div className="container dis-f mt-5rem mb-5rem">
                    <div className="col span-8">
                        <h2 className="fw-b mt-xlarge mb-mlarge">
                            Tell us about your other investments
                        </h2>
                        <p className="mb-2rem">
                            This could be any liquid assets from premium bonds to stocks and shares to cryptocurrency. We&apos;ll ask about non-liquid assets such as art later.
                        </p>
                        <h5 className="mb-2rem">
                            Other investments
                        </h5>

                        <FinanceAboutTable data={this.getListSavings()} section="investment" />

                        <AddDashedButtonLink
                            link="/survey/finance/add-investments"
                            className="mt-3rem btn_large border_dashed link_purple"
                            addName="+ Add investments"
                        />

                    </div>
                    <div className="col span-3 col-last explain_text mt-4rem">
                        {this.renderTextInformation()}
                    </div>
                </div>
                <BackNextButtons
                    isActiveBack={false}
                    onClickNext={this.handleNext}
                    onClickBack={this.handleBack}
                    extraClass="span-8"
                    buttonClass="btn-purple"
                />
            </Fragment>
        );
    }
}

AboutInvestments.propTypes = {
    arrayData: PropTypes.arrayOf(PropTypes.object),
    history: PropTypes.object.isRequired,
    users: PropTypes.object,
    handleGetMePlan: PropTypes.func.isRequired,
    retirement: PropTypes.number,
    radioButton: PropTypes.arrayOf(PropTypes.object),
};

AboutInvestments.defaultProps = {
    arrayData: [],
    users: {},
    retirement: 0,
    radioButton: [],
};

const mapStateToProps = state => ({
    arrayData: get(state, 'plan.assets', []),
    users: get(state, 'plan.users', {}),
    retirement: get(state, 'plan.users.main.retirement_age', 0),
    radioButton: get(state, 'options.tax_benefits', []),
});

const mapDispatchToProps = dispatch => ({
    changeFieldValue: (field, value) => {
        dispatch(change('addInvestments', field, value));
    },
    handleGetMePlan: bindActionCreators(getMePlan, dispatch),
});

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(AboutInvestments));
