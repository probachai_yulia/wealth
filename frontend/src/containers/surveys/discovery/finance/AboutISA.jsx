/* eslint-disable max-len */
import React, { Component, Fragment } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { withRouter } from 'react-router-dom';
import get from 'lodash/get';
import { change } from 'redux-form';
import { bindActionCreators } from 'redux';

import { BackNextButtons } from '../../../../components/BackNextButtons';
import FulfilledLine from '../../../../components/FulfilledLine';
import FinanceAboutTable from '../../../../components/FinanceAboutTable';
import { AddDashedButtonLink } from '../../../../components/AddDashedButtonLink';
import { getMePlan } from '../../../../actions/UserActions';
import ModalInformationIcon from '../../../../components/ModalInformationIcon';

/**
 * Render the content for user idle. Dumb component
 *
 */
class AboutISA extends Component {
    constructor(props) {
        super(props);

        this.handleBack = this.handleBack.bind(this);
        this.handleNext = this.handleNext.bind(this);
        this.getListSavings = this.getListSavings.bind(this);

        const {
            handleGetMePlan,
        } = this.props;

        handleGetMePlan();
    }

    getListSavings = () => {
        const { arrayData, users, radioButton } = this.props;
        const arrayList = [];
        arrayData.map((item) => {
            const regularContribution = [];
            let taxBenefit = '';
            let user = {
                profile: { display_name: '' },
            };
            let imgUser = 'user.svg';
            Object.keys(users).map((userItem) => {
                // console.log(users[userItem], item.owners);
                if ((userItem === 'main' || userItem === 'partner') && users[userItem] !== null) {
                    if (item.owners.length > 1) {
                        user = { profile: { first_name: 'Joint' } };
                        imgUser = 'joint.svg';
                    } else if (users[userItem].profile_uuid === item.owners[0]) {
                        user = users[userItem];
                    }
                }
                if (typeof users[userItem][0] !== 'undefined') {
                    users[userItem].map((subitem) => {
                        if (subitem.profile_uuid === item.owners[0]) {
                            user = subitem;
                        }
                        return null;
                    });
                }
                return null;
            });
            item.regular_contributions.map((value) => {
                regularContribution.push(`£ ${value.amount.toLocaleString('en')}`);
                return null;
            });

            radioButton.map((taxBen) => {
                if (taxBen.code === item.asset_type) {
                    taxBenefit = taxBen.name;
                }
                return null;
            });
            if (item.asset_type.indexOf('isa') !== -1) {
                arrayList.push({
                    balance: `£ ${item.current_value.toLocaleString('en')}`,
                    clientName: user.profile.first_name,
                    client_image: imgUser,
                    extra_information: item.additional,
                    isa_savings: regularContribution,
                    edit_link: `/survey/finance/add-isa/${item.plan_asset_uuid}`,
                    icon: 'ISAs.svg',
                    subtitle: item.asset_type === 'isa' ? 'Unknown ISA type' : taxBenefit,
                    title: item.provider_name,
                });
            }
            return null;
        });
        return arrayList;
    };

    handleBack() {
        const { history } = this.props;
        history.push('assets');
    }

    handleNext() {
        const { history } = this.props;
        history.push('assets');
    }

    render() {
        return (
            <Fragment>
                <FulfilledLine loadingPercent={40} lineColor="#6E0A7B" />
                <div className="container dis-f mt-5rem mb-5rem">
                    <div className="col span-8">
                        <h2 className="fw-b mt-xlarge mb-mlarge">
                            List all your ISAs
                        </h2>
                        <h5 className="mb-2rem">
                            ISAs
                        </h5>

                        <FinanceAboutTable data={this.getListSavings()} section="isa" />

                        <AddDashedButtonLink
                            link="/survey/finance/add-isa"
                            className="mt-3rem btn_large border_dashed link_purple"
                            addName="+ Add ISA"
                        />

                    </div>
                    <div className="col span-3 col-last explain_text mt-4rem">
                        <ModalInformationIcon
                            title="The untapped power of ISA"
                            text="You can invest up to £20,000 per year in most ISAs, which grow free of capital gains and income tax. If you invested £20,000 per year for 10 years in global equities within your ISA you could achieve a pot of around £320k free of capital gains and dividend tax."
                            tooltip_btn="See assumption"
                            tooltip_text="Assumes a growth rate of 8.4%, annual contributions of £20,000 and a dividend tax rate of 32.5%"
                        />
                    </div>
                </div>
                <BackNextButtons
                    isActiveBack={false}
                    onClickNext={this.handleNext}
                    onClickBack={this.handleBack}
                    extraClass="span-8"
                    buttonClass="btn-purple"
                />
            </Fragment>
        );
    }
}

AboutISA.propTypes = {
    arrayData: PropTypes.arrayOf(PropTypes.object),
    history: PropTypes.object.isRequired,
    users: PropTypes.object,
    handleGetMePlan: PropTypes.func.isRequired,
    radioButton: PropTypes.arrayOf(PropTypes.object),
};

AboutISA.defaultProps = {
    arrayData: [],
    users: {},
    radioButton: [],
};

const mapStateToProps = (state) => {
    let radioButton;
    get(state, 'options.asset_types', [])
        .map((item) => {
            if (typeof item.isa !== 'undefined') {
                radioButton = item.isa;
            }
            return null;
        });
    return ({
        arrayData: get(state, 'plan.assets', []),
        users: get(state, 'plan.users', {}),
        radioButton: radioButton || [],
    });
};

const mapDispatchToProps = dispatch => ({
    changeFieldValue: (field, value) => {
        dispatch(change('addInvestments', field, value));
    },
    handleGetMePlan: bindActionCreators(getMePlan, dispatch),
});

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(AboutISA));
