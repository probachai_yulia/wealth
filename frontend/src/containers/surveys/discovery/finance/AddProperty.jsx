import React, { Component, Fragment } from 'react';
import { change, Field } from 'redux-form';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import get from 'lodash/get';
import PropTypes from 'prop-types';
import isEmpty from 'lodash/isEmpty';
import {
    booleanRequired, isNumber, positiveNumber, required, requiredMoney,
} from '../../../../utils/validation.helper';
import InputCustom from '../../../../components/InputCustom';
import Select from '../../../../components/Select';
import { InputPound } from '../../../../components/InputPound';
import AnythingElse from '../../../../components/AnythingElse';
import { LoadableAddress } from '../../../../components/Loadable';
import SurveyHoc from '../../../hoc/SurveyHoc';
import { addPlanAddressProperty, updatePlanAsset } from '../../../../actions/PlanActions';
import YesNo from '../../../../components/YesNo';
import MoneyBlock from '../../../../components/MoneyBlock';
import { normalizeBoolean } from '../../../../utils/helper';
import { BackNextButtons } from '../../../../components/BackNextButtons';
import { TitleHeader } from '../../../../components/TitleHeader';

/**
 * Render the content for user idle. Dumb component
 *
 */
class AddProperty extends Component {
    static formName = 'addProperty';

    static surveyData = {
        formName: AddProperty.formName,
        nextStage: 'nextStage',
        prevStage: 'prevStage',
        data: [
            {
                fields: [
                    'is_tenants_in_common',
                    'postcode',
                    'address_1',
                    'address_2',
                    'city',
                    'country_cd',
                    'location',
                    'address_type_id',
                    'owners',
                    'asset_type',
                    'current_value',
                    'is_not_sure_regular_contribution',
                    'regular_contribution',
                    'additional',
                    'plan_asset_uuid',
                    'address_uuid',
                    'provider_name',
                    'isUpdate',
                ],
                additionalCreateFields: [
                    { is_primary_address: true },
                ],
                additionalUpdateFields: [
                    { is_primary_address: false },
                ],
                isUpdate: 'isUpdate',
                selector: 'planUUID',
                methodCreate: addPlanAddressProperty,
                methodUpdate: updatePlanAsset,
            },
        ],
    };

    constructor(props) {
        super(props);

        this.renderOtherOwnerAccount = this.renderOtherOwnerAccount.bind(this);
        this.renderAddress = this.renderAddress.bind(this);
        this.addressList = this.addressList.bind(this);
        this.renderFieldAdrress = this.renderFieldAdrress.bind(this);
        this.receiveRent = this.receiveRent.bind(this);

        const {
            handleGetMePlan,
        } = this.props;

        handleGetMePlan();
    }

    componentWillUpdate() {
        const { formValue, changeFieldValue, usersOptions } = this.props;
        const regularContribution = [{
            amount: get(formValue, 'values.rent-value', '0'),
            frequency: get(formValue, 'values.rent-value-select', 'per_month'),
            value_type: 'F',
            regular_contribution_type: 'personal_contribution',
        }];
        if (get(formValue, 'values.owner', '') === 'joint') {
            changeFieldValue('owners', [usersOptions.main.profile_uuid, usersOptions.partner.profile_uuid]);
            changeFieldValue('is_tenants_in_common', false);
        } else if (get(formValue, 'values.owner', '') === 'tennants') {
            changeFieldValue('is_tenants_in_common', true);
            changeFieldValue('owners', '');
        } else {
            changeFieldValue('owners', [get(formValue, 'values.owner', '')]);
            changeFieldValue('is_tenants_in_common', false);
        }
        changeFieldValue('is_not_sure_regular_contribution');
        changeFieldValue('regular_contribution', regularContribution);
    }

    optionOwnerAccount() {
        const ownersList = [];
        let id = 0;
        const {
            usersOptions,
        } = this.props;
        Object.keys(usersOptions).map((item) => {
            if (usersOptions[item] !== null) {
                if (item === 'main') {
                    ownersList.push({
                        id,
                        code: usersOptions[item].profile_uuid,
                        name: `${usersOptions[item].profile.first_name}`,
                    });
                    id += 1;
                }
                if (item === 'partner') {
                    ownersList.push({
                        id,
                        code: usersOptions[item].profile_uuid,
                        name: `${usersOptions[item].profile.first_name}`,
                    });
                    id += 1;
                    ownersList.push({
                        id: ownersList.length + 1,
                        code: 'joint',
                        name: 'Joint',
                    });
                }
            }
            return null;
        });
        ownersList.push({
            id: ownersList.length + 2,
            code: 'tennants',
            name: 'Tenants in common',
        });
        return ownersList;
    }

    addressList() {
        const { addressList } = this.props;
        const addresses = [];
        if (addressList.length > 0) {
            addressList.map((item, i) => {
                addresses.push({
                    id: i,
                    code: item.address_uuid,
                    name: item.address_1,
                });
                return null;
            });
        }
        addresses.push({
            id: addresses.length,
            code: 'other',
            name: 'Your address is not in the list? Insert manually',
        });
        return addresses;
    }

    receiveRent() {
        const { formValue } = this.props;
        const receiveRent = get(formValue, 'values.is_not_sure_regular_contribution', null);

        if (receiveRent === true) {
            return (
                <MoneyBlock
                    label="Income amount"
                    moneyPlaceholder="2,000"
                    poundFieldName="rent-value"
                    selectFieldName="rent-value-select"
                />
            );
        }
        return null;
    }

    renderAddress() {
        const { formValue, isSubmitted, countriesOptions } = this.props;
        const addAdress = get(formValue, 'values.address_uuid', '');
        if (addAdress === this.addressList()[this.addressList().length - 1].code) {
            return (
                <Fragment>
                    <LoadableAddress
                        isShowErrors={isSubmitted}
                        formName="addProperty"
                        postcode={get(formValue, 'values.postcode', '0')}
                        selectAddress={get(formValue, 'values.select-address', null)}
                        countriesOptions={countriesOptions}
                    />
                    <div className="hr span-8" />
                </Fragment>
            );
        }
        return null;
    }

    renderOtherOwnerAccount() {
        const { formValue, isSubmitted } = this.props;
        const ownerAccount = get(formValue, 'values.ownerAccount', '');
        if (ownerAccount === this.optionOwnerAccount()[3].code) {
            return (
                <Field
                    component={InputCustom}
                    type="text"
                    name="custom-dependency"
                    extraClasses="col-last span-4"
                    isShowErrors={isSubmitted}
                    validate={[
                        required,
                    ]}
                />);
        }
        return null;
    }

    renderFieldAdrress() {
        const { formValue, isSubmitted } = this.props;
        const addAdress = get(formValue, 'values.address_uuid', '');
        if (addAdress) {
            return (
                <Fragment>
                    <div className="dis-f col-last span-8">
                        <Field
                            component={Select}
                            extraClasses="span-4"
                            name="owner"
                            label="Owner"
                            placeholder="Owner"
                            options={this.optionOwnerAccount()}
                            isShowErrors={isSubmitted}
                            validate={[
                                required,
                            ]}
                        />
                        <Field
                            label="Estimated current value"
                            component={InputPound}
                            name="current_value"
                            isShowErrors={isSubmitted}
                            extraClasses="col span-4 col-last"
                            validate={[
                                requiredMoney,
                                isNumber,
                                positiveNumber,
                            ]}
                        />
                    </div>
                    <div className="hr span-8" />
                    <div className="dis-f col-last span-8">
                        <Field
                            component={YesNo}
                            extraClasses="span-2"
                            componentClass="span-8"
                            name="is_not_sure_regular_contribution"
                            label="Do you receive any rental income from this property?"
                            yesChecked={get(formValue, 'values.is_not_sure_regular_contribution', null) === true}
                            noChecked={get(formValue, 'values.is_not_sure_regular_contribution', null) === false}
                            isShowErrors={isSubmitted}
                            normalize={normalizeBoolean}
                            validate={[
                                booleanRequired,
                            ]}
                        />
                    </div>
                    {this.receiveRent()}
                    <AnythingElse open={get(formValue, 'values.additional', null) !== null} name="additional" placeholder="E.g. are you planning to renovate or sell this property?" />
                </Fragment>
            );
        }
        return null;
    }

    render() {
        const {
            handleSubmit, isSubmitted, formValue, handleBack, radioButton,
        } = this.props;
        const isAnySyncErrors = isEmpty(get(formValue, 'syncErrors', {}));
        return (
            <Fragment>
                <TitleHeader
                    title="Add investment"
                />
                <div className="mt-fix-action-header container dis-f fd-c pt-large">
                    <form onSubmit={this.handleSubmit} className="know-you-form col span-8 dis-f fd-c">
                        <div className="dis-f col-last span-8">
                            <Field
                                component={Select}
                                extraClasses="span-4 col"
                                name="asset_type"
                                label="Select property type"
                                placeholder="Type of Property"
                                options={radioButton}
                                isShowErrors={isSubmitted}
                                validate={[
                                    required,
                                ]}
                            />
                            <Field
                                component={Select}
                                extraClasses="span-4 col-last"
                                name="address_uuid"
                                label="Address"
                                placeholder="Select addresses"
                                options={this.addressList()}
                                isShowErrors={isSubmitted}
                                validate={[
                                    required,
                                ]}
                            />
                        </div>
                        <div className="hr span-8" />
                        {this.renderAddress()}
                        {this.renderFieldAdrress()}
                    </form>
                </div>
                <BackNextButtons
                    isActiveBack={false}
                    onClickNext={handleSubmit}
                    isActiveNext={!isSubmitted || (isAnySyncErrors && isSubmitted)}
                    onClickBack={handleBack}
                    buttonClass="btn-purple"
                    nextText="Save"
                />
            </Fragment>
        );
    }
}

AddProperty.propTypes = {
    changeFieldValue: PropTypes.func.isRequired,
    formValue: PropTypes.object,
    handleSubmit: PropTypes.func.isRequired,
    isSubmitted: PropTypes.bool.isRequired,
    handleBack: PropTypes.func.isRequired,
    usersOptions: PropTypes.object,
    countriesOptions: PropTypes.arrayOf(PropTypes.object),
    handleGetMePlan: PropTypes.func.isRequired,
    radioButton: PropTypes.arrayOf(PropTypes.object),
    addressList: PropTypes.arrayOf(PropTypes.object),
};

AddProperty.defaultProps = {
    formValue: {},
    usersOptions: {},
    countriesOptions: [],
    radioButton: [],
    addressList: [],
};

const mapStateToProps = (state, ownProps) => {
    const assets = get(state, 'plan.assets', []);
    let id;
    assets.map((item, i) => {
        if (item.plan_asset_uuid === ownProps.match.params.plan_uuid) {
            id = i;
        }
        return null;
    });
    const owner = get(state, `plan.assets[${id}].owners`, []).length > 1 ? 'joint' : get(state, `plan.assets[${id}].owners[0]`, '');
    let radioButton;
    get(state, 'options.asset_types', []).map((item) => {
        if (typeof item.property !== 'undefined') {
            radioButton = item.property;
        }
        return null;
    });
    return ({
        initialValues: {
            planUUID: get(state, `plan.assets[${id}].plan_asset_uuid`, null),
            isUpdate: get(state, `plan.assets[${id}].plan_asset_uuid`, null) !== null,
            additional: get(state, `plan.assets[${id}].additional`, null),
            regular_contribution: get(state, `plan.assets[${id}].regular_contributions`, []),
            owners: get(state, `plan.assets[${id}].owners`, []),
            owner,
            current_value: get(state, `plan.assets[${id}].current_value`, ''),
            asset_type_other: get(state, `plan.assets[${id}].asset_type_other`, ''),
            provider_name: 'empty',
            address_uuid: get(state, `plan.assets[${id}].address_uuid`, ''),
            asset_type: get(state, `plan.assets[${id}].asset_type`, ''),
            not_sure_additional: get(state, `plan.assets[${id}].is_not_sure_regular_contribution`, null),
            plan_asset_uuid: get(state, `plan.assets[${id}].plan_asset_uuid`, ''),
            tax_benefit: get(state, `plan.assets[${id}].tax_benefit`),
            is_not_sure_regular_contribution: get(state, `plan.assets[${id}].is_not_sure_regular_contribution`, null),
            country_cd: 'GB',
            'rent-value-select': 'per_month',
            nextStage: '/survey/finance/about-property',
            prevStage: '/survey/finance/assets',
        },
        enableReinitialize: true,
        usersOptions: get(state, 'plan.users', {}),
        assets: get(state, 'plan.assets', []),
        countriesOptions: get(state, 'options.countries', []),
        addressList: get(state, 'plan.addresses', []),
        radioButton: radioButton || [],
    });
};

const mapDispatchToProps = dispatch => ({
    changeFieldValue: (field, value) => {
        dispatch(change(AddProperty.formName, field, value));
    },
});

const AddPropertyWithHoc = SurveyHoc(AddProperty, AddProperty.surveyData);

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(AddPropertyWithHoc));
