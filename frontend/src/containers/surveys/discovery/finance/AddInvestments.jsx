import React, { Component, Fragment } from 'react';
import { change, Field } from 'redux-form';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import get from 'lodash/get';
import PropTypes from 'prop-types';
import isEmpty from 'lodash/isEmpty';
import {
    booleanRequired,
    isNumber,
    positiveNumber,
    required,
    requiredMoney,
} from '../../../../utils/validation.helper';
import MultipleMoneyBlock from '../../../../components/MultipleMoneyBlock';
import InputCustom from '../../../../components/InputCustom';
import Select from '../../../../components/Select';
import { TitleHeader } from '../../../../components/TitleHeader';
import YesNo from '../../../../components/YesNo';
import { InputPound } from '../../../../components/InputPound';
import RadioBlocks from '../../../../components/RadioBlocks';
import Checkbox from '../../../../components/Checkbox';
import Radio from '../../../../components/Radio';
import UploadAssetsStatement from '../../../../components/UploadAssetsStatement';
import AnythingElse from '../../../../components/AnythingElse';
import SurveyHoc from '../../../hoc/SurveyHoc';
import { addPlanAsset, updatePlanAsset } from '../../../../actions/PlanActions';
import { normalizeBoolean } from '../../../../utils/helper';
import { BackNextButtons } from '../../../../components/BackNextButtons';

/**
 * Render the content for user idle. Dumb component
 *
 */
class AddInvestments extends Component {
    static formName = 'addInvestments';

    static surveyData = {
        formName: AddInvestments.formName,
        nextStage: 'nextStage',
        prevStage: 'prevStage',
        data: [
            {
                fields: [
                    'owners',
                    'asset_type',
                    'tax_benefit',
                    'is_self_managed',
                    'asset_type_other',
                    'provider_name',
                    'current_value',
                    'is_not_sure_regular_contribution',
                    'is_not_sure',
                    'regular_contribution',
                    'tax_benefit',
                    'additional',
                    'plan_asset_uuid',
                    'update',
                    'plan_document_uuids',
                ],
                isUpdate: 'update',
                // additionalFields: [],
                selector: 'planUUID',
                methodCreate: addPlanAsset,
                methodUpdate: updatePlanAsset,
            },
        ],
    };

    constructor(props) {
        super(props);

        this.renderOtherOwnerAccount = this.renderOtherOwnerAccount.bind(this);
        this.renderTypeRadioBtn = this.renderTypeRadioBtn.bind(this);
        this.renderContributionBlock = this.renderContributionBlock.bind(this);

        const {
            handleGetMePlan,
        } = this.props;

        handleGetMePlan();
    }

    componentDidUpdate() {
        const { formValue, changeFieldValue, usersOptions } = this.props;
        if (get(formValue, 'values.owner', '') === 'joint') {
            changeFieldValue('owners', [usersOptions.main.profile_uuid, usersOptions.partner.profile_uuid]);
            changeFieldValue('is_tenants_in_common', false);
        } else if (get(formValue, 'values.owner', '') === 'tennants') {
            changeFieldValue('is_tenants_in_common', true);
            changeFieldValue('owners', '');
        } else {
            changeFieldValue('owners', [get(formValue, 'values.owner', '')]);
            changeFieldValue('is_tenants_in_common', false);
        }
        let valAdditional;
        switch (get(formValue, 'values.not_sure_additional', '')) {
            case '1':
            case '2':
                valAdditional = false;
                break;
            case '3':
                valAdditional = true;
                break;
            default:
                valAdditional = false;
        }
        changeFieldValue('is_not_sure_regular_contribution', valAdditional.toString());
        changeFieldValue('is_not_sure', valAdditional.toString());
        if (get(formValue, 'values.files', []).length !== get(formValue, 'values.plan_document_uuids', []).length) {
            get(formValue, 'values.files', []).map(() => {
                changeFieldValue('uploadingFiles', true);
                return null;
            });
            setTimeout(() => {
                const fileList = [];
                get(formValue, 'values.files', []).map((item, i) => {
                    fileList[i] = item.planDocumentUuids;
                    if (get(formValue, 'values.files', []).length - 1 === i) {
                        changeFieldValue('uploadingFiles', false);
                    }
                    return null;
                });
                changeFieldValue('plan_document_uuids', fileList);
            }, 2000);
        }
        // changeFieldValue('is_self_managed', get(formValue, 'values.is_self_managed_info', false).toString());
    }

    optionOwnerAccount() {
        const ownersList = []; let id = 0;
        const {
            usersOptions,
        } = this.props;
        Object.keys(usersOptions).map((item) => {
            if (usersOptions[item] !== null) {
                if (item === 'main') {
                    ownersList.push({
                        id,
                        code: usersOptions[item].profile_uuid,
                        name: `${usersOptions[item].profile.first_name}`,
                    });
                    id += 1;
                }
                if (item === 'partner') {
                    ownersList.push({
                        id,
                        code: usersOptions[item].profile_uuid,
                        name: `${usersOptions[item].profile.first_name}`,
                    });
                    id += 1;
                    ownersList.push({
                        id: ownersList.length + 1,
                        code: 'joint',
                        name: 'Joint',
                    });
                }
                if (item === 'child') {
                    usersOptions[item].map((subitem) => {
                        ownersList.push({
                            id,
                            code: subitem.profile_uuid,
                            name: subitem.profile.first_name,
                            dob: subitem.profile.dob,
                            role: item,
                        });
                        id += 1;
                        return null;
                    });
                }
            }
            return null;
        });

        return ownersList;
    }

    renderContributionBlock() {
        const { formValue, isSubmitted } = this.props;
        if (get(formValue, 'values.not_sure_additional', null) === true) {
            return (
                <MultipleMoneyBlock
                    label="Contribution amount"
                    name="regular_contribution"
                    poundFieldName="childcareSpend"
                    selectFieldName="regular_contribution"
                    extraClasses="mt-2rem"
                    typeFieldName="investments"
                    isShowErrors={isSubmitted}
                />
            );
        }
        return null;
    }

    renderTypeRadioBtn() {
        const { formValue, isSubmitted, radioButton } = this.props;
        const selectedValue = get(formValue, 'values.tax_benefit', null);
        const valueNotSure = 'unknown';
        return (
            <Fragment>
                <div className="dis-f span-8 no-padding">
                    <Field
                        component={RadioBlocks}
                        blocks={radioButton}
                        label="Does this investment benefit from any special tax treatment?"
                        name="tax_benefit"
                        fieldsName="tax_benefit"
                        extraClasses="pb-0"
                        selectedValue={selectedValue}
                        isShowErrors={isSubmitted}
                    />
                </div>
                <Field
                    component={Radio}
                    extraClasses={`span-4 ${selectedValue === valueNotSure ? '' : 'mb-2rem'}`}
                    name="tax_benefit"
                    fieldValue={valueNotSure}
                    label="I’m not sure"
                    checked={selectedValue === valueNotSure}
                    isShowErrors={isSubmitted}
                />
                {selectedValue === valueNotSure
                && (
                    <p className="mb-2rem greyText">
                        No problem, we can work out the finer details later.
                    </p>
                )
                }
            </Fragment>
        );
    }

    renderOtherOwnerAccount() {
        const { formValue, isSubmitted } = this.props;
        const ownerAccount = get(formValue, 'values.ownerAccount', {});
        if (this.optionOwnerAccount().length > 0) {
            if (ownerAccount === this.optionOwnerAccount()[this.optionOwnerAccount().length - 1].code) {
                return (
                    <Field
                        component={InputCustom}
                        type="text"
                        name="custom-dependency"
                        extraClasses="col-last span-4"
                        isShowErrors={isSubmitted}
                        validate={[
                            required,
                        ]}
                    />);
            }
        }
        return null;
    }

    render() {
        const {
            handleSubmit, isSubmitted, formValue, handleBack,
        } = this.props;
        const isAnySyncErrors = isEmpty(get(formValue, 'syncErrors', {}));
        let activeSubmit = !isSubmitted || (isAnySyncErrors && isSubmitted);
        activeSubmit = get(formValue, 'values.uploadingFiles', false) === true ? false : activeSubmit;
        return (
            <Fragment>
                <TitleHeader
                    title="Add investment"
                />
                <div className="container dis-f fd-c pt-large">
                    <form className="know-you-form col span-8 dis-f fd-c">
                        <div className="dis-f col-last span-8">
                            <Field
                                component={Select}
                                extraClasses="span-4"
                                name="owner"
                                label="Who owns the investment?"
                                placeholder="Select owner"
                                options={this.optionOwnerAccount()}
                                isShowErrors={isSubmitted}
                            />
                            {this.renderOtherOwnerAccount()}
                        </div>
                        <Field
                            component={InputCustom}
                            name="asset_type_other"
                            label="Type of investment/holding"
                            placeholder="Explain briefly this investment in your own words"
                            extraClasses="span-8"
                            isShowErrors={isSubmitted}
                            validate={[
                                required,
                            ]}
                        />
                        <div className="input-wrap span-8">
                            <Field
                                component={InputCustom}
                                name="provider_name"
                                label="Provider/Manager or company name"
                                extraClasses="span-4"
                                isShowErrors={isSubmitted}
                                disabled={get(formValue, 'values.is_self_managed', false)}
                                validate={get(formValue, 'values.is_self_managed', false) ? '' : [
                                    required,
                                ]}
                            />
                            <Field
                                component={Checkbox}
                                extraClasses="span-4"
                                name="is_self_managed"
                                fieldValue="I manage the investment by myself"
                                label="I manage the investment by myself"
                                checked={get(formValue, 'values.is_self_managed', false)}
                                normalize={normalizeBoolean}
                                isShowErrors={isSubmitted}
                            />
                        </div>
                        <div className="span-8 mt-2rem">
                            <Field
                                label="Estimated value"
                                component={InputPound}
                                name="current_value"
                                isShowErrors={isSubmitted}
                                extraClasses="span-4"
                                validate={[
                                    requiredMoney,
                                    isNumber,
                                    positiveNumber,
                                ]}
                            />
                        </div>
                        <div className="hr span-8" />
                        <Field
                            component={YesNo}
                            extraClasses="span-2"
                            name="not_sure_additional"
                            componentClass="span-8"
                            label="Do you make regular contributions to this investment?"
                            yesChecked={get(formValue, 'values.not_sure_additional', null) === true}
                            noChecked={get(formValue, 'values.not_sure_additional', null) === false}
                            normalize={normalizeBoolean}
                            isShowErrors={isSubmitted}
                            validate={[
                                booleanRequired,
                            ]}
                        />
                        {this.renderContributionBlock()}
                        <div className="hr span-8" />
                        {this.renderTypeRadioBtn()}
                        <Field
                            component={UploadAssetsStatement}
                            disabled={false}
                            name="files"
                            isShowErrors={isSubmitted}
                        />
                        <AnythingElse name="additional" open={get(formValue, 'values.additional', null) !== null} placeholder="E.g reason for investment" />
                    </form>
                </div>
                <BackNextButtons
                    isActiveBack={false}
                    onClickNext={handleSubmit}
                    isActiveNext={activeSubmit}
                    onClickBack={handleBack}
                    buttonClass="btn-purple"
                    nextText="Save"
                />
            </Fragment>
        );
    }
}

AddInvestments.propTypes = {
    changeFieldValue: PropTypes.func.isRequired,
    formValue: PropTypes.object,
    handleSubmit: PropTypes.func.isRequired,
    isSubmitted: PropTypes.bool.isRequired,
    handleBack: PropTypes.func.isRequired,
    usersOptions: PropTypes.object,
    handleGetMePlan: PropTypes.func.isRequired,
    radioButton: PropTypes.arrayOf(PropTypes.object),
};

AddInvestments.defaultProps = {
    formValue: {},
    usersOptions: {},
    radioButton: [],
};

const mapStateToProps = (state, ownProps) => {
    const assets = get(state, 'plan.assets', []);
    let id;
    assets.map((item, i) => {
        if (item.plan_asset_uuid === ownProps.match.params.plan_uuid) {
            id = i;
        }
        return null;
    });
    return ({
        initialValues: {
            planUUID: get(state, `plan.assets[${id}].plan_asset_uuid`, null),
            update: get(state, `plan.assets[${id}].plan_asset_uuid`, null) !== null,
            additional: get(state, `plan.assets[${id}].additional`, null),
            asset_type: 'cash_other',
            is_self_managed: get(state, `plan.assets[${id}].is_self_managed`, false),
            regular_contribution: get(state, `plan.assets[${id}].regular_contributions`, [{
                amount: 0, frequency: 'per_month', value_type: 'F', regular_contribution_type: 'investments',
            }]),
            owners: get(state, `plan.assets[${id}].owners`, []),
            owner: get(state, `plan.assets[${id}].owners[0]`, ''),
            current_value: get(state, `plan.assets[${id}].current_value`, '').toString(),
            asset_type_other: get(state, `plan.assets[${id}].asset_type_other`, ''),
            provider_name: get(state, `plan.assets[${id}].provider_name`, ''),
            not_sure_additional: get(state, `plan.assets[${id}].is_not_sure_regular_contribution`, null),
            plan_asset_uuid: get(state, `plan.assets[${id}].plan_asset_uuid`, '').toString(),
            tax_benefit: get(state, `plan.assets[${id}].tax_benefit`, ''),
            nextStage: '/survey/finance/about-investments',
            prevStage: '/survey/finance/assets',
            uploadingFiles: false,
        },
        enableReinitialize: true,
        usersOptions: get(state, 'plan.users', {}),
        assets: get(state, 'plan.assets', []),
        radioButton: get(state, 'options.tax_benefits', []),
    });
};

const mapDispatchToProps = dispatch => ({
    changeFieldValue: (field, value) => {
        dispatch(change('addInvestments', field, value));
    },
});

const AddInvestmentsWithHoc = SurveyHoc(AddInvestments, AddInvestments.surveyData);

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(AddInvestmentsWithHoc));
