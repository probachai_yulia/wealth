import React, { Component, Fragment } from 'react';
import { change, Field } from 'redux-form';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import get from 'lodash/get';
import PropTypes from 'prop-types';
import isEmpty from 'lodash/isEmpty';
import {
    booleanRequired,
    isNumber,
    positiveNumber,
    required,
    requiredData,
    requiredMoney,
    requiredYesNo,
} from '../../../../utils/validation.helper';
import AnythingElse from '../../../../components/AnythingElse';
import PercentageBlock from '../../../../components/PercentageBlock';
import InputCustom from '../../../../components/InputCustom';
import { PERCENTAGE_SALARY, PERIODS } from '../../../../helpers/constants';
import Select from '../../../../components/Select';
import YesNo from '../../../../components/YesNo';
import YesNoNotSure from '../../../../components/YesNoNotSure';
import { InputPound } from '../../../../components/InputPound';
import RadioBlocks from '../../../../components/RadioBlocks';
import Checkbox from '../../../../components/Checkbox';
import Radio from '../../../../components/Radio';
import MoneyBlock from '../../../../components/MoneyBlock';
import SurveyHoc from '../../../hoc/SurveyHoc';
import { addPlanAsset, updatePlanAsset } from '../../../../actions/PlanActions';
import { LoadableDateYearPicker } from '../../../../components/Loadable';
import { normalizeBoolean, normalizeTimeValue } from '../../../../utils/helper';
import { BackNextButtons } from '../../../../components/BackNextButtons';
import { TitleHeader } from '../../../../components/TitleHeader';
import UploadAssetsStatement from '../../../../components/UploadAssetsStatement';


class AddPensions extends Component {
    static formName = 'addPensions';

    static surveyData = {
        formName: AddPensions.formName,
        nextStage: 'nextStage',
        prevStage: 'prevStage',
        data: [
            {
                fields: [
                    'owners',
                    'asset_type',
                    'asset_type_other',
                    'provider_name',
                    'current_value',
                    'is_not_sure_regular_contribution',
                    'is_not_sure_asset_type',
                    'is_not_sure',
                    'expected_retirement',
                    'is_indeterminate_period',
                    'regular_contribution',
                    'time_spent_working',
                    'additional',
                    'plan_asset_uuid',
                    'update',
                    'plan_document_uuids',
                ],
                // additionalFields: [],
                isUpdate: 'update',
                selector: 'planUUID',
                methodCreate: addPlanAsset,
                methodUpdate: updatePlanAsset,
            },
        ],
    };

    constructor(props) {
        super(props);

        this.renderTypeRadioBtn = this.renderTypeRadioBtn.bind(this);
        this.renderConditionalBlock = this.renderConditionalBlock.bind(this);
        this.renderEmployerContribution = this.renderEmployerContribution.bind(this);
        this.renderEmployerContribution = this.renderEmployerContribution.bind(this);
        this.renderPersonalContribution = this.renderPersonalContribution.bind(this);
        this.renderIcomeContribution = this.renderIcomeContribution.bind(this);

        const {
            handleGetMePlan,
        } = this.props;

        handleGetMePlan();
    }

    componentDidUpdate() {
        const { formValue, changeFieldValue } = this.props;
        changeFieldValue('owners', [get(formValue, 'values.owner', '')]);
        changeFieldValue('is_indeterminate_period', get(formValue, 'values.is_indeterminate_period_info', ''));
        const regularContribution = [];
        switch (get(formValue, 'values.is_not_sure_employer_contribution_info', '0')) {
            case '1':
                changeFieldValue('is_not_sure_employer_contribution', false);
                regularContribution[regularContribution.length] = {
                    amount: get(formValue, 'values.is_not_sure_employer_contribution_amount', 0),
                    frequency: 'per_year',
                    regular_contribution_type: 'employer_contribution',
                    value_type: get(formValue, 'values.is_not_sure_employer_contribution_frequency', 'F'),
                };
                break;
            case '2':
                changeFieldValue('is_not_sure_employer_contribution', false);
                break;
            case '3':
                changeFieldValue('is_not_sure_employer_contribution', true);
                break;
            default:
                changeFieldValue('is_not_sure_employer_contribution', null);
        }
        if (get(formValue, 'values.asset_type_info', '') === 'Not sure') {
            changeFieldValue('asset_type', 'pension');
            changeFieldValue('is_not_sure_asset_type', true);
        } else if (get(formValue, 'values.asset_type_info', '') === 'annuity') {
            regularContribution[regularContribution.length] = {
                amount: get(formValue, 'values.annuity_amount', 0),
                frequency: get(formValue, 'values.annuity_frequency', 'per_month'),
                regular_contribution_type: 'personal_contribution',
                value_type: 'F',
            };
            changeFieldValue('asset_type', get(formValue, 'values.asset_type_info', ''));
            changeFieldValue('current_value', get(formValue, 'values.annuity_amount', 0));
        } else {
            changeFieldValue('asset_type', get(formValue, 'values.asset_type_info', ''));
            changeFieldValue('is_not_sure_asset_type', false);
        }

        if (get(formValue, 'values.is_not_sure_regular_contribution', null) === true) {
            regularContribution[regularContribution.length] = {
                amount: get(formValue, 'values.is_not_sure_regular_contribution_amount', 0),
                frequency: 'per_year',
                regular_contribution_type: 'personal_contribution',
                value_type: get(formValue, 'values.is_not_sure_regular_contribution_frequency', 'F'),
            };
        }
        switch (get(formValue, 'values.is_not_sure_receive_income_info', '0')) {
            case '1':
                changeFieldValue('is_not_sure_receive_income', 'true');
                regularContribution[regularContribution.length] = {
                    amount: get(formValue, 'values.is_not_sure_regular_contribution_amount', 0),
                    frequency: get(formValue, 'values.is_not_sure_regular_contribution_frequency', 'per_month'),
                    regular_contribution_type: 'receive_income',
                    value_type: 'F',
                };
                break;
            case '2':
                changeFieldValue('is_not_sure_receive_income', 'false');
                break;
            default:
                changeFieldValue('is_not_sure_receive_income', null);
        }
        if (regularContribution) {
            changeFieldValue('regular_contribution', regularContribution);
        }
        if (get(formValue, 'values.files', []).length !== get(formValue, 'values.plan_document_uuids', []).length) {
            get(formValue, 'values.files', []).map(() => {
                changeFieldValue('uploadingFiles', true);
                return null;
            });
            setTimeout(() => {
                const fileList = [];
                get(formValue, 'values.files', []).map((item, i) => {
                    fileList[i] = item.planDocumentUuids;
                    if (get(formValue, 'values.files', []).length - 1 === i) {
                        changeFieldValue('uploadingFiles', false);
                    }
                    return null;
                });
                changeFieldValue('plan_document_uuids', fileList);
            }, 2000);
        }
        return true;
    }

    optionowner() {
        const ownersList = []; let
            id = 0;
        const {
            usersOptions,
        } = this.props;
        Object.keys(usersOptions).map((item) => {
            if (usersOptions[item] !== null) {
                if (item === 'main' || item === 'partner') {
                    ownersList.push({
                        id,
                        code: usersOptions[item].profile_uuid,
                        name: usersOptions[item].profile.first_name,
                        dob: usersOptions[item].profile.dob,
                    });
                    id += 1;
                }
                if (item === 'child') {
                    usersOptions[item].map((subitem) => {
                        ownersList.push({
                            id,
                            code: subitem.profile_uuid,
                            name: subitem.profile.first_name,
                            dob: subitem.profile.dob,
                            role: item,
                        });
                        id += 1;
                        return null;
                    });
                }
            }
            return null;
        });

        return ownersList;
    }

    blockFinance() {
        const { formValue, radioButton } = this.props;
        let financePension = [];
        const selUser = this.optionowner().find(item => item.code === get(formValue, 'values.owner', ''));
        if (typeof selUser !== 'undefined') {
            if (selUser.dob !== null) {
                const birthYear = new Date(selUser.dob);
                const ageDifMs = Date.now() - birthYear.getTime();
                const ageDate = new Date(ageDifMs); // miliseconds from epoch
                const age = Math.abs(ageDate.getUTCFullYear() - 1970);
                radioButton.map((item) => {
                    if (item.code !== 'annuity' || age > 55) {
                        financePension.push({
                            id: item.id,
                            name: item.name,
                            description: item.description,
                            code: item.code,
                        });
                    }
                    return null;
                });
            } else {
                financePension = radioButton;
            }
        } else {
            financePension = radioButton;
        }
        return financePension;
    }

    renderIcomeContribution() {
        const { formValue, isSubmitted } = this.props;
        if (get(formValue, 'values.is_not_sure_receive_income_info', '0') === '1') {
            return (
                <MoneyBlock
                    label="Amount"
                    poundFieldName="is_not_sure_receive_income_info_amount"
                    selectFieldName="is_not_sure_receive_income_info_frequency"
                    isShowErrors={isSubmitted}
                    optionsPeriod={PERIODS}
                />
            );
        }
        return null;
    }

    renderPersonalContribution() {
        const { formValue, isSubmitted } = this.props;
        if (get(formValue, 'values.is_not_sure_regular_contribution', null) === true) {
            if (get(formValue, 'values.is_not_sure_regular_contribution_frequency', 'F') === 'F') {
                return (
                    <MoneyBlock
                        label="Amount (pa)"
                        poundFieldName="is_not_sure_regular_contribution_amount"
                        selectFieldName="is_not_sure_regular_contribution_frequency"
                        hintMessage="Please add what percentage of your base salary your employer contributes or the cash amount."
                        isShowErrors={isSubmitted}
                        optionsPeriod={PERCENTAGE_SALARY}
                    />
                );
            }
            return (
                <PercentageBlock
                    label="Amount (pa)"
                    poundFieldName="is_not_sure_regular_contribution_amount"
                    selectFieldName="is_not_sure_regular_contribution_frequency"
                    hintMessage="Please add what percentage of your base salary your employer contributes or the cash amount."
                    isShowErrors={isSubmitted}
                    optionVal={PERCENTAGE_SALARY}
                />
            );
        }
        return null;
    }

    renderConditionalBlock() {
        const { formValue, isSubmitted, radioButton } = this.props;
        const isIndeterminatePeriod = get(formValue, 'values.isIndeterminatePeriod', false);
        const stillMemberChecked = get(formValue, 'values.still_member', false);
        let retired = false; let
            age;
        const selUser = this.optionowner().find(item => item.code === get(formValue, 'values.owner', ''));
        if (typeof selUser !== 'undefined') {
            if (selUser.dob !== null) {
                retired = typeof selUser !== 'undefined' ? selUser.current_situation_type === 'retired' : false;
                const birthYear = new Date(selUser.dob);
                const ageDifMs = Date.now() - birthYear.getTime();
                const ageDate = new Date(ageDifMs); // miliseconds from epoch
                age = Math.abs(ageDate.getUTCFullYear() - 1970);
            }
        }
        switch (get(formValue, 'values.asset_type_info', '0')) {
            case radioButton[0].code:
                return (
                    <Fragment>
                        <div className="span-8">
                            <Field
                                component={InputCustom}
                                name="provider_name"
                                label="Employer or scheme name"
                                placeholder=""
                                extraClasses="col span-5"
                                isShowErrors={isSubmitted}
                                validate={[
                                    required,
                                ]}
                            />
                        </div>
                        <div>
                            <Field
                                component={InputCustom}
                                name="time_spent_working"
                                label="For how many years were you a member of this scheme?"
                                placeholder="Years"
                                extraClasses="col span-5"
                                disabled={isIndeterminatePeriod}
                                isShowErrors={isSubmitted}
                                validate={!stillMemberChecked ? [
                                    required,
                                ] : []}
                            />
                            {retired === false
                            && (
                                <Field
                                    component={Checkbox}
                                    extraClasses="span-4"
                                    name="is_indeterminate_period"
                                    fieldValue="Still member"
                                    label="I’m still a member"
                                    checked={stillMemberChecked}
                                    isShowErrors={isSubmitted}
                                    normalize={normalizeBoolean}
                                />
                            )
                            }
                        </div>
                        <div className="input-wrap col span-8 mt-2rem">
                            <Field
                                label={retired === true ? 'How much income do you receive each year?' : 'Expected income on retirement (pa)'}
                                component={InputPound}
                                name="current_value"
                                isShowErrors={isSubmitted}
                                extraClasses="col span-4"
                                validate={[
                                    isNumber,
                                    positiveNumber,
                                ]}
                            />
                            <Field
                                component={LoadableDateYearPicker}
                                name="expected_retirement"
                                label="Expected retirement date"
                                formName={AddPensions.formName}
                                hintMessage="Age at which scheme will start to pay out"
                                minAge={-100}
                                extraClass="span-4 col-last"
                                isShowErrors={isSubmitted}
                                normalize={normalizeTimeValue}
                                validate={[
                                    requiredData,
                                ]}
                            />
                        </div>
                        <AnythingElse open={get(formValue, 'values.additional', null) !== null} name="additional" />
                    </Fragment>
                );
            case radioButton[1].code:
                return (
                    <Fragment>
                        <div className="input-wrap col span-8 mt-2rem">
                            <Field
                                label="Provider or employer name"
                                placeholder="Provider or employer name"
                                component={InputCustom}
                                name="provider_name"
                                isShowErrors={isSubmitted}
                                extraClasses="col span-4 inline"
                                validate={[
                                    required,
                                ]}
                            />
                            <Field
                                label="Current value"
                                component={InputPound}
                                name="current_value"
                                isShowErrors={isSubmitted}
                                extraClasses="col span-4 col-last"
                                validate={[
                                    isNumber,
                                    positiveNumber,
                                ]}
                            />
                        </div>
                        <Field
                            component={YesNoNotSure}
                            extraClasses="span-2"
                            name="is_not_sure_employer_contribution_info"
                            label="Are any employer contributions made?"
                            yesChecked={get(formValue, 'values.is_not_sure_employer_contribution_info', '0') === '1'}
                            noChecked={get(formValue, 'values.is_not_sure_employer_contribution_info', '0') === '2'}
                            notSureChecked={get(formValue, 'values.is_not_sure_employer_contribution_info', '0') === '3'}
                            isShowErrors={isSubmitted}
                            validate={[
                                required,
                            ]}
                        />
                        {this.renderEmployerContribution()}
                        <div className="hr span-8" />
                        <div className="span-6">
                            <Field
                                component={YesNo}
                                name="is_not_sure_regular_contribution"
                                label="Are any personal contributions made?"
                                yesChecked={get(formValue, 'values.is_not_sure_regular_contribution', null) === true}
                                noChecked={get(formValue, 'values.is_not_sure_regular_contribution', null) === false}
                                isShowErrors={isSubmitted}
                                normalize={normalizeBoolean}
                                validate={[
                                    booleanRequired,
                                ]}
                            />
                        </div>
                        {this.renderPersonalContribution()}
                        <div className="hr span-8" />
                        {age > 55
                        && (
                            <div className="span-6">
                                <Field
                                    component={YesNo}
                                    name="is_not_sure_receive_income_info"
                                    label="Do they receive any income from this?"
                                    yesChecked={get(formValue, 'values.is_not_sure_receive_income_info', null) === true}
                                    noChecked={get(formValue, 'values.is_not_sure_receive_income_info', null) === false}
                                    isShowErrors={isSubmitted}
                                    normalize={normalizeBoolean}
                                    validate={[
                                        booleanRequired,
                                    ]}
                                />
                            </div>
                        )
                        }
                        {this.renderIcomeContribution()}
                        <AnythingElse open={get(formValue, 'values.additional', null) !== null} name="additional" placeholder="E.g. lump sum contributions" />
                    </Fragment>
                );
            case radioButton[2].code:
                return (
                    <Fragment>
                        <div className="span-8">
                            <Field
                                component={InputCustom}
                                name="provider_name"
                                label="Provider name"
                                placeholder=""
                                extraClasses="col span-5"
                                isShowErrors={isSubmitted}
                                validate={[
                                    required,
                                ]}
                            />
                        </div>
                        <MoneyBlock
                            label="Income amount"
                            poundFieldName="annuity_amount"
                            selectFieldName="annuity_frequency"
                            isShowErrors={isSubmitted}
                            optionsPeriod={PERIODS}
                        />
                        <AnythingElse open={get(formValue, 'values.additional', null) !== null} name="additional" placeholder="E.g. joint life policy or linked to inflation" />
                    </Fragment>
                );
            case 'Not sure':
                return (
                    <Fragment>
                        <div className="input-wrap col span-8">
                            <Field
                                component={InputCustom}
                                name="provider_name"
                                label="Provider name"
                                placeholder="Provider name"
                                extraClasses="col span-4 inline"
                                disabled={isIndeterminatePeriod}
                                isShowErrors={isSubmitted}
                                validate={[
                                    required,
                                ]}
                            />
                            <Field
                                label="Current value"
                                component={InputPound}
                                name="current_value"
                                isShowErrors={isSubmitted}
                                extraClasses="col span-4 col-last"
                                validate={[
                                    requiredMoney,
                                    isNumber,
                                    positiveNumber,
                                ]}
                            />
                        </div>
                        <div className="span-6">
                            <Field
                                component={YesNo}
                                name="is_not_sure_regular_contribution"
                                label="Do they make any personal contributions per year?"
                                yesChecked={get(formValue, 'values.is_not_sure_regular_contribution', null) === true}
                                noChecked={get(formValue, 'values.is_not_sure_regular_contribution', null) === false}
                                isShowErrors={isSubmitted}
                                normalize={normalizeBoolean}
                                validate={[
                                    booleanRequired,
                                ]}
                            />
                        </div>
                        {this.renderPersonalContribution()}
                        <AnythingElse open={get(formValue, 'values.additional', null) !== null} name="additional" />
                    </Fragment>
                );
            default:
                return null;
        }
    }

    renderTypeRadioBtn() {
        const { formValue, isSubmitted } = this.props;
        const selectedValue = get(formValue, 'values.asset_type_info', null);
        const valueNotSure = 'Not sure';
        return (
            <Fragment>
                <div className="dis-f span-8 no-padding">
                    <Field
                        component={RadioBlocks}
                        blocks={this.blockFinance()}
                        label="What type of pension is this?"
                        name="asset_type_info"
                        fieldsName="asset_type_info"
                        extraClasses="pb-0"
                        selectedValue={selectedValue}
                        isShowErrors={isSubmitted}
                        validate={[
                            requiredYesNo,
                        ]}
                    />
                </div>
                <Field
                    component={Radio}
                    extraClasses={`span-4 ${selectedValue === valueNotSure ? '' : 'mb-2rem'}`}
                    name="asset_type_info"
                    fieldValue={valueNotSure}
                    label="I’m not sure"
                    checked={selectedValue === valueNotSure}
                    isShowErrors={isSubmitted}
                    validate={[
                        requiredYesNo,
                    ]}
                />
                {selectedValue === valueNotSure
                && (
                    <p className="mb-2rem greyText">
                        No problem, we can work out the finer details later.
                    </p>
                )
                }
            </Fragment>
        );
    }

    renderEmployerContribution() {
        const { formValue, isSubmitted } = this.props;
        if (get(formValue, 'values.is_not_sure_employer_contribution_info', '0') === '1') {
            if (get(formValue, 'values.is_not_sure_employer_contribution_frequency', 'F') === 'F') {
                return (
                    <MoneyBlock
                        label="Employer contribution of salary"
                        poundFieldName="is_not_sure_employer_contribution_amount"
                        selectFieldName="is_not_sure_employer_contribution_frequency"
                        hintMessage="Please add what percentage of your base salary your employer contributes or the cash amount."
                        isShowErrors={isSubmitted}
                        optionsPeriod={PERCENTAGE_SALARY}
                    />
                );
            }
            return (
                <PercentageBlock
                    label="Employer contribution of salary"
                    poundFieldName="is_not_sure_employer_contribution_amount"
                    selectFieldName="is_not_sure_employer_contribution_frequency"
                    hintMessage="Please add what percentage of your base salary your employer contributes or the cash amount."
                    isShowErrors={isSubmitted}
                    optionVal={PERCENTAGE_SALARY}
                />
            );
        }
        return null;
    }

    render() {
        const {
            handleSubmit, isSubmitted, formValue, handleBack,
        } = this.props;
        const isAnySyncErrors = isEmpty(get(formValue, 'syncErrors', {}));

        let activeSubmit = !isSubmitted || (isAnySyncErrors && isSubmitted);
        activeSubmit = get(formValue, 'values.uploadingFiles', false) === true ? false : activeSubmit;
        return (
            <Fragment>
                <TitleHeader
                    title="Add investment"
                />
                <div className="container dis-f fd-c pt-large">
                    <form onSubmit={this.handleSubmit} className="know-you-form col span-8 dis-f fd-c">
                        <div className="dis-f col-last span-8">
                            <Field
                                component={Select}
                                extraClasses="span-4"
                                name="owner"
                                label="Owner"
                                placeholder="Select owner"
                                options={this.optionowner()}
                                isShowErrors={isSubmitted}
                            />
                        </div>
                        {this.renderTypeRadioBtn()}
                        {this.renderConditionalBlock()}
                        <Field
                            component={UploadAssetsStatement}
                            disabled={false}
                            name="files"
                            isShowErrors={isSubmitted}
                        />
                    </form>
                </div>
                <BackNextButtons
                    isActiveBack={false}
                    onClickNext={handleSubmit}
                    isActiveNext={activeSubmit}
                    onClickBack={handleBack}
                    buttonClass="btn-purple"
                    nextText="Save"
                />
            </Fragment>
        );
    }
}

AddPensions.propTypes = {
    changeFieldValue: PropTypes.func.isRequired,
    formValue: PropTypes.object,
    handleSubmit: PropTypes.func.isRequired,
    isSubmitted: PropTypes.bool.isRequired,
    handleBack: PropTypes.func.isRequired,
    usersOptions: PropTypes.object,
    handleGetMePlan: PropTypes.func.isRequired,
    radioButton: PropTypes.arrayOf(PropTypes.object),
};

AddPensions.defaultProps = {
    formValue: {},
    usersOptions: {},
    radioButton: [],
};

const mapStateToProps = (state, ownProps) => {
    const assets = get(state, 'plan.assets', []);
    let id; let
        isNotSureRegularContribution;
    const arrayContribution = {
        personal_contribution: {
            amount: null,
            frequency: 'per_month',
        },
        employer_contribution: {
            amount: null,
            frequency: 'F',
        },
        receive_income: {
            amount: null,
            frequency: 'per_month',
        },
    };
    let assetType;
    assets.map((item, i) => {
        // noinspection JSUnresolvedVariable
        if (item.plan_asset_uuid === ownProps.match.params.plan_uuid) {
            id = i;
            const regularContribution = get(state, `plan.assets[${id}].regular_contributions`, []);
            regularContribution.map((subitem) => {
                let frequency;
                switch (item.regular_contribution_type) {
                    case 'personal_contribution':
                        arrayContribution.personal_contribution = {
                            amount: subitem.amount,
                            frequency: subitem.frequency,
                        };
                        break;
                    case 'employer_contribution':
                        frequency = subitem.value_type;
                        arrayContribution.employer_contribution = {
                            amount: subitem.amount,
                            frequency: subitem.value_type,
                        };
                        break;
                    case 'receive_income':
                        arrayContribution.receive_income = {
                            amount: subitem.amount,
                            frequency,
                        };
                        break;
                    default:
                        return null;
                }
                return null;
            });
            if (get(state, `plan.assets[${id}].is_not_sure_employer_contribution`, null) === true) {
                isNotSureRegularContribution = '3';
            } else if (typeof arrayContribution.employer_contribution.amount !== 'undefined' && arrayContribution.employer_contribution.amount !== null) {
                if (arrayContribution.employer_contribution.amount !== 0) {
                    isNotSureRegularContribution = '1';
                } else {
                    isNotSureRegularContribution = '2';
                }
            } else {
                isNotSureRegularContribution = null;
            }
            if (get(state, `plan.assets[${id}].is_not_sure_asset_type`, '') === true) {
                assetType = 'Not sure';
            } else {
                assetType = get(state, `plan.assets[${id}].asset_type`, '');
            }
        }
        return null;
    });
    let radioButton;
    get(state, 'options.asset_types', []).map((item) => {
        // noinspection JSUnusedAssignment
        // noinspection JSUnresolvedVariable
        if (typeof item.pension !== 'undefined') {
            // noinspection JSUnresolvedVariable
            radioButton = item.pension;
        }
        return null;
    });
    // noinspection JSUnusedAssignment
    return ({
        initialValues: {
            planUUID: get(state, `plan.assets[${id}].plan_asset_uuid`, null) || get(state, 'common.localId', null),
            annuity_amount: arrayContribution.personal_contribution.amount,
            annuity_frequency: arrayContribution.personal_contribution.frequency || 'per_month',
            update: get(state, `plan.assets[${id}].plan_asset_uuid`, null) !== null || get(state, 'common.localId', null) !== null,
            is_not_sure: 'false',
            additional: get(state, `plan.assets[${id}].additional`, null),
            is_indeterminate_period: get(state, `plan.assets[${id}].is_indeterminate_period`, false),
            asset_type_info: assetType,
            is_self_managed_info: get(state, `plan.assets[${id}].is_self_managed`, false),
            is_not_sure_employer_contribution_info: isNotSureRegularContribution,
            is_not_sure_employer_contribution_amount: arrayContribution.employer_contribution.amount,
            is_not_sure_employer_contribution_frequency: arrayContribution.employer_contribution.frequency || 'F',
            is_not_sure_regular_contribution: get(state, `plan.assets[${id}].is_not_sure_regular_contribution`, null),
            is_not_sure_regular_contribution_amount: arrayContribution.personal_contribution.amount,
            is_not_sure_regular_contribution_frequency: arrayContribution.personal_contribution.frequency || 'F',
            is_not_sure_receive_income: get(state, `plan.assets[${id}].is_not_sure_receive_income`, null),
            is_not_sure_receive_income_amount: arrayContribution.receive_income.amount,
            is_not_sure_receive_income_frequency: arrayContribution.receive_income.frequency,
            regular_contribution: get(state, `plan.assets[${id}].regular_contributions`, []),
            owners: get(state, `plan.assets[${id}].owners`, []),
            owner: get(state, `plan.assets[${id}].owners[0]`, ''),
            current_value: get(state, `plan.assets[${id}].current_value`, ''),
            asset_type_info_other: get(state, `plan.assets[${id}].asset_type_info_other`, ''),
            provider_name: get(state, `plan.assets[${id}].provider_name`, ''),
            not_sure_additional: isNotSureRegularContribution,
            plan_asset_uuid: get(state, `plan.assets[${id}].plan_asset_uuid`, ''),
            tax_benefit: get(state, `plan.assets[${id}].tax_benefit`, ''),
            expected_retirement: get(state, `plan.assets[${id}].expected_retirement`, ''),
            time_spent_working: get(state, `plan.assets[${id}].time_spent_working`, ''),
            nextStage: '/survey/finance/about-pensions',
            prevStage: '/survey/finance/assets',
            uploadingFiles: false,
        },
        enableReinitialize: true,
        usersOptions: get(state, 'plan.users', {}),
        assets: get(state, 'plan.assets', []),
        radioButton: radioButton || [],
    });
};

const mapDispatchToProps = dispatch => ({
    changeFieldValue: (field, value) => {
        dispatch(change(AddPensions.formName, field, value));
    },
});

const AddPensionsWithHoc = SurveyHoc(AddPensions, AddPensions.surveyData);

// noinspection JSUnusedGlobalSymbols
export default withRouter(connect(mapStateToProps, mapDispatchToProps)(AddPensionsWithHoc));
