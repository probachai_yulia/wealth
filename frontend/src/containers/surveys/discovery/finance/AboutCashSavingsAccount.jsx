/* eslint-disable max-len */
import React, { Component, Fragment } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { withRouter } from 'react-router-dom';

import get from 'lodash/get';
import { change } from 'redux-form';
import { bindActionCreators } from 'redux';
import { BackNextButtons } from '../../../../components/BackNextButtons';
import FulfilledLine from '../../../../components/FulfilledLine';

import FinanceAboutTable from '../../../../components/FinanceAboutTable';
import { AddDashedButtonLink } from '../../../../components/AddDashedButtonLink';
import { getMePlan } from '../../../../actions/UserActions';
import ModalInformationIcon from '../../../../components/ModalInformationIcon';

/**
 * Render the content for user idle. Dumb component
 *
 */
class AboutCashSavingsAccount extends Component {
    constructor(props) {
        super(props);

        this.handleBack = this.handleBack.bind(this);
        this.handleNext = this.handleNext.bind(this);
        this.getListSavings = this.getListSavings.bind(this);

        const {
            handleGetMePlan,
        } = this.props;

        handleGetMePlan();
    }

    getListSavings = () => {
        const { arrayData, users, radioButton } = this.props;
        const arrayList = [];
        arrayData.map((item) => {
            const regularContribution = [];
            let taxBenefit = '';
            let user = {
                profile: { display_name: '' },
            };
            let imgUser = 'user.svg';
            Object.keys(users).map((userItem) => {
                // console.log(users[userItem], item.owners);
                if ((userItem === 'main' || userItem === 'partner') && users[userItem] !== null) {
                    if (item.owners.length > 1) {
                        user = { profile: { first_name: 'Joint' } };
                        imgUser = 'joint.svg';
                    } else if (users[userItem].profile_uuid === item.owners[0]) {
                        user = users[userItem];
                    }
                }
                if (typeof users[userItem][0] !== 'undefined') {
                    users[userItem].map((subitem) => {
                        if (subitem.profile_uuid === item.owners[0]) {
                            user = subitem;
                        }
                        return null;
                    });
                }
                return null;
            });
            item.regular_contributions.map((value) => {
                const frequency = value.frequency === 'per_month' ? 'p.m.' : 'p.a.';
                regularContribution.push(`£ ${value.amount.toLocaleString('en')} ${frequency}`);
                return null;
            });

            radioButton.map((taxBen) => {
                if (taxBen.code === item.asset_type) {
                    taxBenefit = taxBen.name;
                }
                return null;
            });
            if (item.asset_type === 'cash_current_account' || item.asset_type === 'cash_term_deposit' || item.asset_type === 'cash_savings' || item.asset_type === 'cash') {
                arrayList.push({
                    balance: `£ ${item.current_value.toLocaleString('en')}`,
                    extra_information: item.additional,
                    clientName: user.profile.first_name,
                    client_image: imgUser,
                    contribution: regularContribution,
                    edit_link: `/survey/finance/add-cash-savings-account/${item.plan_asset_uuid}`,
                    icon: 'cash_savings.svg',
                    subtitle: item.asset_type === 'cash' ? 'Unknown cash savings type' : taxBenefit,
                    title: item.provider_name,
                });
            }
            return null;
        });
        return arrayList;
    };

    handleBack() {
        const { history } = this.props;
        history.push('assets');
    }

    handleNext() {
        const { history } = this.props;
        history.push('assets');
    }

    render() {
        return (
            <Fragment>
                <FulfilledLine loadingPercent={40} lineColor="#6E0A7B" />
                <div className="container dis-f mt-5rem mb-5rem">
                    <div className="col span-8">
                        <h2 className="fw-b mt-xlarge mb-mlarge">
                            List all your cash savings
                        </h2>
                        <h5 className="mb-2rem">
                            Cash savings
                        </h5>

                        <FinanceAboutTable data={this.getListSavings()} section="cash" />

                        <AddDashedButtonLink
                            link="/survey/finance/add-cash-savings-account"
                            className="mt-3rem btn_large border_dashed link_purple"
                            addName="+ Add cash savings account"
                        />

                    </div>
                    <div className="col span-3 col-last explain_text mt-4rem">
                        <ModalInformationIcon
                            title="Cashing in"
                            text=" While it's important to hold a minimum level of cash to cover day-to-day expenses and in case your circumstances suddenly change, holding too much in cash can mean you may be missing out on growth opportunities.<br /><br />For example, £1,000 invested over 100 years may grow to £2,449 in cash accounts, compared to £2.9m if invested in global equities."
                            tooltip_btn="See assumption"
                            tooltip_text="Assumes a growth rate of 0.9% for cash and 8.4% for global equities"
                        />
                    </div>
                </div>
                <BackNextButtons
                    isActiveBack={false}
                    onClickNext={this.handleNext}
                    onClickBack={this.handleBack}
                    extraClass="span-8"
                    buttonClass="btn-purple"
                />
            </Fragment>
        );
    }
}

AboutCashSavingsAccount.propTypes = {
    arrayData: PropTypes.arrayOf(PropTypes.object),
    history: PropTypes.object.isRequired,
    users: PropTypes.object,
    handleGetMePlan: PropTypes.func.isRequired,
    radioButton: PropTypes.arrayOf(PropTypes.object),
};

AboutCashSavingsAccount.defaultProps = {
    arrayData: [],
    users: {},
    radioButton: [],
};

const mapStateToProps = (state) => {
    let radioButton;
    get(state, 'options.asset_types', []).map((item) => {
        if (typeof item.cash !== 'undefined') {
            radioButton = item.cash;
        }
        return null;
    });
    return ({
        arrayData: get(state, 'plan.assets', []),
        users: get(state, 'plan.users', {}),
        radioButton: radioButton || [],
    });
};

const mapDispatchToProps = dispatch => ({
    changeFieldValue: (field, value) => {
        dispatch(change('addCashSavingsAccount', field, value));
    },
    handleGetMePlan: bindActionCreators(getMePlan, dispatch),
});

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(AboutCashSavingsAccount));
