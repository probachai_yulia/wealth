/* eslint-disable max-len */
import React, { Component, Fragment } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { withRouter } from 'react-router-dom';

import get from 'lodash/get';
import { change } from 'redux-form';
import { bindActionCreators } from 'redux';
import { BackNextButtons } from '../../../../components/BackNextButtons';
import FulfilledLine from '../../../../components/FulfilledLine';

import FinanceAboutTable from '../../../../components/FinanceAboutTable';
import { AddDashedButtonLink } from '../../../../components/AddDashedButtonLink';
import { getMePlan } from '../../../../actions/UserActions';
import ModalInformationIcon from '../../../../components/ModalInformationIcon';

/**
 * Render the content for user idle. Dumb component
 *
 */
class AboutProperty extends Component {
    constructor(props) {
        super(props);

        this.handleBack = this.handleBack.bind(this);
        this.handleNext = this.handleNext.bind(this);
        this.getListSavings = this.getListSavings.bind(this);

        const {
            handleGetMePlan,
        } = this.props;

        handleGetMePlan();
    }

    getListSavings = () => {
        const {
            arrayData, users, addresses, radioButton,
        } = this.props;
        const arrayList = [];
        arrayData.map((item) => {
            const regularContribution = [];
            let user = {
                profile: { display_name: '' },
            };
            let imgUser = 'user.svg';
            Object.keys(users).map((userItem) => {
                // console.log(users[userItem], item.owners);
                if ((userItem === 'main' || userItem === 'partner') && users[userItem] !== null) {
                    if (item.owners.length > 1) {
                        user = { profile: { first_name: 'Joint' } };
                        imgUser = 'joint.svg';
                    } else if (users[userItem].profile_uuid === item.owners[0]) {
                        user = users[userItem];
                    }
                }
                if (typeof users[userItem][0] !== 'undefined') {
                    users[userItem].map((subitem) => {
                        if (subitem.profile_uuid === item.owners[0]) {
                            user = subitem;
                        }
                        return null;
                    });
                }
                return null;
            });
            if (item.is_tenants_in_common) {
                user = { profile: { first_name: 'Tenants in common' } };
            }
            item.regular_contributions.map((value) => {
                const frequency = value.frequency === 'per_month' ? 'p.m.' : 'p.a.';
                regularContribution.push(`£ ${value.amount.toLocaleString('en')} ${frequency}`);
                return null;
            });

            if (item.asset_type.indexOf('property') !== -1) {
                const selAddress = item.address_uuid;
                const currentValue = addresses.find(subitem => selAddress === subitem.address_uuid);
                const propertyType = radioButton.find(subitem => subitem.code === item.asset_type);
                arrayList.push({
                    address: [`${currentValue.address_1}, ${currentValue.address_2}, ${currentValue.city}, ${currentValue.country_cd}`],
                    clientName: user.profile.first_name,
                    client_image: imgUser,
                    extra_information: item.additional,
                    rental_income: [regularContribution],
                    edit_link: `/survey/finance/add-property/${item.plan_asset_uuid}`,
                    icon: 'properties.svg',
                    subtitle: currentValue.address_1,
                    title: propertyType.name,
                    value: `£ ${item.current_value.toLocaleString('en')}`,
                });
            }
            return null;
        });
        return arrayList;
    };

    handleBack() {
        const { history } = this.props;
        history.push('assets');
    }

    handleNext() {
        const { history } = this.props;
        history.push('assets');
    }

    render() {
        return (
            <Fragment>
                <FulfilledLine loadingPercent={40} lineColor="#6E0A7B" />
                <div className="container dis-f mt-5rem">
                    <div className="col span-8">
                        <h2 className="fw-b mt-xlarge mb-mlarge">
                            Tell us about your property portfolio
                        </h2>
                        <h5 className="mb-2rem">
                            Properties
                        </h5>

                        <FinanceAboutTable data={this.getListSavings()} section="property" />

                        <AddDashedButtonLink
                            link="/survey/finance/add-property"
                            className="mt-3rem btn_large border_dashed link_purple"
                            addName="+ Add property"
                        />

                    </div>
                    <div className="col span-3 col-last explain_text mt-4rem">
                        <ModalInformationIcon
                            title="The tax man versus the realtor"
                            text="Typically your main home is not subject to capital gains tax. The tax regime around buy-to-let and second home ownership is very complex and constantly changing. We can help you make sense of this and make use of tax reliefs."
                        />
                        <ModalInformationIcon
                            title="Did you know?"
                            text="You can hold commercial property in a pension. This can help you avoid capital gains tax."
                        />
                    </div>
                </div>
                <BackNextButtons
                    isActiveBack={false}
                    onClickNext={this.handleNext}
                    onClickBack={this.handleBack}
                    extraClass="span-8"
                    buttonClass="btn-purple"
                />
            </Fragment>
        );
    }
}

AboutProperty.propTypes = {
    arrayData: PropTypes.arrayOf(PropTypes.object),
    history: PropTypes.object.isRequired,
    users: PropTypes.object,
    addresses: PropTypes.arrayOf(PropTypes.object),
    handleGetMePlan: PropTypes.func.isRequired,
    radioButton: PropTypes.arrayOf(PropTypes.object),
};

AboutProperty.defaultProps = {
    addresses: [],
    arrayData: [],
    users: {},
    radioButton: [],
};

const mapStateToProps = (state) => {
    let radioButton;
    get(state, 'options.asset_types', []).map((item) => {
        if (typeof item.property !== 'undefined') {
            radioButton = item.property;
        }
        return null;
    });
    return ({
        arrayData: get(state, 'plan.assets', []),
        users: get(state, 'plan.users', {}),
        addresses: get(state, 'plan.addresses', []),
        radioButton: radioButton || [],
    });
};

const mapDispatchToProps = dispatch => ({
    changeFieldValue: (field, value) => {
        dispatch(change('addCashSavingsAccount', field, value));
    },
    handleGetMePlan: bindActionCreators(getMePlan, dispatch),
});

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(AboutProperty));
