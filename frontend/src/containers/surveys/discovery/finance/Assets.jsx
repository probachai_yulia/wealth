import React, { Component, Fragment } from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import PropTypes from 'prop-types';

import get from 'lodash/get';
import { change } from 'redux-form';
import { bindActionCreators } from 'redux';
import BoxImgText from '../../../../components/BoxImgText';
import { BackNextButtons } from '../../../../components/BackNextButtons';
import FulfilledLine from '../../../../components/FulfilledLine';
import { getMePlan } from '../../../../actions/UserActions';
import { SECTION_FINANCE } from '../../../../helpers/constants';

/**
 * Render the content for user idle. Dumb component
 *
 */
class AssetsFinance extends Component {
    static formName = 'assetFinance';

    constructor(props) {
        super(props);

        this.handleBack = this.handleBack.bind(this);
        this.handleNext = this.handleNext.bind(this);

        const {
            handleGetMePlan,
        } = this.props;

        handleGetMePlan();
    }

    getDoneSection(task) {
        const {
            assets,
        } = this.props;
        let num = 0;
        if (assets.length > 0) {
            const cashOther = assets.filter(item => item.asset_type === 'cash_other');
            const cash = assets.filter(item => item.asset_type === 'cash_current_account' || item.asset_type === 'cash_term_deposit' || item.asset_type === 'cash_savings' || item.asset_type === 'cash');
            const pensions = assets.filter(item => item.asset_type === 'benefit' || item.asset_type === 'contribution' || item.asset_type === 'annuity' || item.asset_type === 'pension');
            const property = assets.filter(item => item.asset_type.indexOf('property') !== -1);
            const isa = assets.filter(item => item.asset_type.indexOf('isa') !== -1);
            switch (task) {
                case 'investment':
                    num = cashOther.length;
                    break;
                case 'property':
                    num = property.length;
                    break;
                case 'cash':
                    num = cash.length;
                    break;
                case 'isa':
                    num = isa.length;
                    break;
                case 'pension':
                    num = pensions.length;
                    break;
                default:
                    num = 0;
            }
        }
        return num;
    }

    titleFunction(textInfo) {
        const ownersList = [];
        let id = 0;
        const {
            usersOptions,
            partnerName,
        } = this.props;
        Object.keys(usersOptions).map((item) => {
            if (usersOptions[item] !== null) {
                if (item === 'partner') {
                    ownersList.push({
                        id,
                        code: usersOptions[item].profile_uuid,
                        name: `${item}`,
                        user: usersOptions[item],
                    });
                    id += 1;
                }
            }
            return null;
        });
        if (textInfo === 'title') {
            if (ownersList.length > 0) {
                return (
                    <h2 className="fw-b mt-xlarge mb-medium">
                        {`You and ${partnerName}'s Financial Holdings`}
                    </h2>
                );
            }
            return (<h2 className="fw-b mt-xlarge mb-medium">Your Financial Holdings</h2>);
        }
        if (ownersList.length > 0) {
            return (
                <p className="mb-large">
                    {`Please add the assets one at a time that you or ${partnerName} own as well as any assets you own jointly.`}
                </p>
            );
        }
        return (
            <p className="mb-large">Please add the assets one at a time that you own as well as any assets you own jointly.</p>
        );
    }

    handleBack() {
        const { history } = this.props;
        history.push('../dashboard');
    }

    handleNext() {
        const { history } = this.props;
        const self = this;
        SECTION_FINANCE.map((item) => {
            if (self.getDoneSection(item.section) === 0) {
                history.push(item.link);
                return false;
            }
            return null;
        });
        history.push('/survey/dashboard');
    }

    render() {
        return (
            <Fragment>
                <FulfilledLine loadingPercent={20} lineColor="#6e0a7b" />
                <section className="container dis-f fd-c  mt-5rem mb-15rem">
                    {this.titleFunction('title')}
                    {this.titleFunction('paragraph')}
                    {
                        SECTION_FINANCE.map((item, i) => (
                            <Fragment key={i.toString()}>
                                <BoxImgText
                                    key={item.id}
                                    btnLink={this.getDoneSection(item.section) > 0 ? item.edit_link : item.link}
                                    btnText={item.button_text}
                                    done={this.getDoneSection(item.section) > 0}
                                    taskCompleted={this.getDoneSection(item.section)}
                                    icon={item.icon}
                                    subtitle={item.description}
                                    title={item.title}
                                />
                                {item.id < SECTION_FINANCE.length - 1
                                && <div className="hr_nm span-8 mt-small mb-small" />
                                }
                            </Fragment>
                        ))
                    }
                </section>
                <BackNextButtons
                    isActiveBack={false}
                    onClickNext={this.handleNext}
                    onClickBack={this.handleBack}
                    extraClass="span-8"
                    buttonClass="btn-purple"
                />
            </Fragment>
        );
    }
}

AssetsFinance.propTypes = {
    history: PropTypes.object.isRequired,
    usersOptions: PropTypes.object,
    assets: PropTypes.arrayOf(PropTypes.object),
    handleGetMePlan: PropTypes.func,
    partnerName: PropTypes.string,
};

AssetsFinance.defaultProps = {
    handleGetMePlan: () => {},
    usersOptions: {},
    assets: [],
    partnerName: '',
};

const mapStateToProps = state => ({
    usersOptions: get(state, 'plan.users', {}),
    partnerName: get(state, 'plan.users.partner.profile.first_name', ''),
    clientName: get(state, 'plan.users.main.profile.first_name', ''),
    assets: get(state, 'plan.assets', []),
});

const mapDispatchToProps = dispatch => ({
    changeFieldValue: (field, value) => {
        dispatch(change(AssetsFinance.formName, field, value));
    },
    handleGetMePlan: bindActionCreators(getMePlan, dispatch),
});

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(AssetsFinance));
