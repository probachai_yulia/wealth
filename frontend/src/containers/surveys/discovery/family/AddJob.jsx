import React, { Component, Fragment } from 'react';
import { Field } from 'redux-form';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import get from 'lodash/get';
import isEmpty from 'lodash/isEmpty';
import PropTypes from 'prop-types';

import { required } from '../../../../utils/validation.helper';
import InputCustom from '../../../../components/InputCustom';
import { TitleHeader } from '../../../../components/TitleHeader';
import { BackNextButtons } from '../../../../components/BackNextButtons';
import Select from '../../../../components/Select';
import AnythingElse from '../../../../components/AnythingElse';
import SurveyHoc from '../../../hoc/SurveyHoc';
import { addPlanJob, updatePlanJob } from '../../../../actions/PlanActions';
import { findJobByID, normalizeInverse } from '../../../../utils/helper';
import Checkbox from '../../../../components/Checkbox';

/**
 * Render the content for user idle. Dumb component
 * @return {JSX.Element}
 */
class AddJob extends Component {
    static formName = 'addJob';

    static surveyData = {
        formName: AddJob.formName,
        nextStage: 'nextStage',
        prevStage: 'prevStage',
        data: [
            {
                fields: [
                    'company',
                    'employment_type',
                    'additional_info',
                    'profile_uuid',
                    'is_company_director',
                ],
                isUpdate: 'updateJob',
                selector: 'jobId',
                methodCreate: addPlanJob,
                methodUpdate: updatePlanJob,
            },
        ],
    };

    constructor(props) {
        super(props);

        const {
            handleGetMePlan,
        } = this.props;

        handleGetMePlan();

        this.renderCompanyDirectorCheckbox = this.renderCompanyDirectorCheckbox.bind(this);
    }

    renderCompanyDirectorCheckbox = (isSelfEmployed) => {
        const { formValue } = this.props;

        if (isSelfEmployed) {
            return (
                <Field
                    component={Checkbox}
                    extraClasses="form-inline"
                    name="is_company_director"
                    fieldValue="is_company_director"
                    label="I’m a company director"
                    normalize={normalizeInverse}
                    checked={get(formValue, 'values.is_company_director', null) === true}
                />
            );
        }

        return null;
    };

    render() {
        const {
            formValue,
            isSubmitted,
            employmentTypes,
            match: {
                params,
            },
            handleSubmit,
            handleBack,
            editMode,
        } = this.props;

        const isAnySyncErrors = isEmpty(get(formValue, 'syncErrors', {}));
        const isSelfEmployed = get(formValue, 'values.employment_type', null) === 'self_employed';

        return (
            <Fragment>
                <TitleHeader title={editMode ? 'Edit Job' : 'Add Job'} />
                <div className="container dis-f fd-c mt-5rem mb-2rem">
                    <form className="know-you-form col span-8 dis-f fd-c mt-xlarge">
                        <div className="col-span-8">
                            <div className="form-inline dis-f fd-c span-8">
                                <div className="input-wrap col span-4">
                                    <Field
                                        component={InputCustom}
                                        label={params.user_type === 'partner' ? 'Where are they working?' : 'Where are you working?'}
                                        type="text"
                                        name="company"
                                        placeholder="Company name"
                                        isShowErrors={isSubmitted}
                                        validate={[
                                            required,
                                        ]}
                                    />
                                </div>
                                <div className="input-wrap col span-4">
                                    <Field
                                        component={Select}
                                        extraClasses={`span-4 ${isSelfEmployed ? 'pb-small' : null}`}
                                        label="What is your employment status?"
                                        options={employmentTypes}
                                        name="employment_type"
                                        placeholder="Select status"
                                        isShowErrors={isSubmitted}
                                        validate={[
                                            required,
                                        ]}
                                    />
                                </div>
                                {this.renderCompanyDirectorCheckbox(isSelfEmployed)}
                                <AnythingElse
                                    name="additional_info"
                                    isShowErrors={isSubmitted}
                                    placeholder="E.g. expecting a promotion or job change"
                                />
                            </div>
                        </div>
                    </form>
                </div>
                <BackNextButtons
                    isActiveBack={false}
                    backText="Cancel"
                    nextText="Save"
                    onClickNext={handleSubmit}
                    isActiveNext={!isSubmitted || (isAnySyncErrors && isSubmitted)}
                    onClickBack={handleBack}
                    buttonClass="btn-purple"
                />
            </Fragment>
        );
    }
}

AddJob.propTypes = {
    formValue: PropTypes.object,
    employmentTypes: PropTypes.arrayOf(PropTypes.object),
    handleSubmit: PropTypes.func.isRequired,
    handleBack: PropTypes.func.isRequired,
    handleGetMePlan: PropTypes.func.isRequired,
    isSubmitted: PropTypes.bool.isRequired,
    match: PropTypes.object,
    editMode: PropTypes.bool,
};

AddJob.defaultProps = {
    formValue: {},
    employmentTypes: [],
    match: null,
    editMode: false,
};

const mapStateToProps = (state, { match: { params } }) => {
    const job = findJobByID(state, `plan.users.${params.user_type}.jobs`, params.job_uuid);

    return ({
        employmentTypes: get(state, 'options.employment_types', []),
        initialValues: {
            profile_uuid: get(state, `plan.users.${params.user_type}.profile_uuid`, null),
            company: get(job, 'company', null),
            employment_type: get(job, 'employment_type', null),
            additional_info: get(job, 'additional_info', null),
            is_company_director: get(job, 'is_company_director', null),
            updateJob: !isEmpty(params.job_uuid),
            jobId: params.job_uuid,
            nextStage: `/survey/family/work/${params.user_type}`,
            prevStage: `/survey/family/work/${params.user_type}`,
        },
        editMode: !isEmpty(params.job_uuid),
    });
};

const AddJobWithHoc = SurveyHoc(AddJob, AddJob.surveyData);

export default withRouter(connect(mapStateToProps)(AddJobWithHoc));
