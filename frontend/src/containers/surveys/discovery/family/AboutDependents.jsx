import React, { Component, Fragment } from 'react';
import { change, Field } from 'redux-form';
import get from 'lodash/get';
import isEmpty from 'lodash/isEmpty';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { withRouter } from 'react-router-dom';
import { bindActionCreators } from 'redux';
import moment from 'moment';

import YesNoManual from '../../../../components/YesNoManual';
import { BackNextButtons } from '../../../../components/BackNextButtons';
import FulfilledLine from '../../../../components/FulfilledLine';
import AddDashedButton from '../../../../components/AddDashedButton';
import { booleanRequired } from '../../../../utils/validation.helper';
import SurveyHoc from '../../../hoc/SurveyHoc';
import { getPersonAge, normalizeBoolean } from '../../../../utils/helper';
import FinanceAboutTable from '../../../../components/FinanceAboutTable';
import { patchPlan } from '../../../../actions/PlanActions';
import { requestsReset } from '../../../../actions/CommonActions';
import { ErrorMessage } from '../../../../components/ErrorMessage';

/**
 * Render the content for user idle. Dumb component
 *
 */
class AboutDependents extends Component {
    static formName = 'aboutDependents';

    static surveyData = {
        formName: AboutDependents.formName,
        nextStage: 'nextStage',
        prevStage: 'prevStage',
        data: [
            {
                fields: [
                    'family_has_dependents',
                ],
                additionalCreateFields: [
                    {
                        is_active: true,
                    },
                ],
                isUpdate: 'updatePlan',
                selector: 'planId',
                methodUpdate: patchPlan,
            },
        ],
    };

    constructor(props) {
        super(props);

        this.state = {
            showErrors: false,
            submittedLocally: false,
            hasErrors: false,
        };

        this.renderDependentsBlock = this.renderDependentsBlock.bind(this);
        this.renderDependents = this.renderDependents.bind(this);
        this.getDataFromDependant = this.getDataFromDependant.bind(this);
        this.handleClick = this.handleClick.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleRedirect = this.handleRedirect.bind(this);

        const {
            handleGetMePlan,
        } = this.props;

        handleGetMePlan();
    }

    componentDidUpdate() {
        const {
            isLocalRequestsDone,
            handleRequestsReset,
            history,
        } = this.props;
        if (isLocalRequestsDone === 1) {
            handleRequestsReset();
            history.push('/survey/family/add-dependents');
        }
    }

    getDataFromDependant = dependant => [{
        icon: 'user.svg',
        edit_link: `/survey/family/add-dependents/${dependant.profile_uuid}`,
        // eslint-disable-next-line max-len
        subtitle: `${getPersonAge(dependant.profile.dob)} | ${dependant.plan_user_type || dependant.plan_user_type_other}`,
        title: `${dependant.profile.first_name} ${dependant.profile.last_name}`,
        date_of_birth: [`${moment(dependant.dob).format('LL').toString()} (${getPersonAge(dependant.profile.dob)})`],
        relationship_to_client: [dependant.plan_user_type || dependant.plan_user_type_other],
        extra_information: dependant.additional_info,
        hideAccordion: isEmpty(dependant.additional_info),
    }];

    handleSubmit = (event) => {
        event.preventDefault();
        const { formValue: { values }, dependents, handleSubmit } = this.props;
        const familyHasDependents = get(values, 'family_has_dependents', null);

        this.setState(() => ({ submittedLocally: true }));

        if (familyHasDependents === true && !dependents.length) {
            this.setState(() => ({ hasErrors: true }));
        } else handleSubmit(event);
    };

    handleClick({ target: { value } }) {
        const { changeFieldValue } = this.props;

        const valueClicked = normalizeBoolean(value);

        changeFieldValue('family_has_dependents', valueClicked);

        this.setState(({ hasErrors }) => ({
            showErrors: valueClicked,
            hasErrors: valueClicked === false ? valueClicked : hasErrors,
        }));
    }

    handleRedirect() {
        const {
            handlePatchPlan,
            formValue,
        } = this.props;
        const data = {
            family_has_dependents: get(formValue, 'values.family_has_dependents', null),
            selector: get(formValue, 'values.planId', null),
            isLocalRequest: true,
        };
        handlePatchPlan(data);
    }

    renderDependents() {
        const {
            dependents,
        } = this.props;

        return (
            <div>
                {dependents.map(dependant => (
                    <FinanceAboutTable
                        key={dependant.profile_uuid}
                        data={this.getDataFromDependant(dependant)}
                        section="work"
                    />
                ))}
            </div>
        );
    }

    renderDependentsBlock() {
        const { formValue } = this.props;
        const isHaveDependents = get(formValue, 'values.family_has_dependents', null);
        if (isHaveDependents === true) {
            return (
                <Fragment>
                    <div className="hr span-8" />
                    {this.renderDependents()}
                    <AddDashedButton
                        name="Other dependents"
                        addName="+ Add other dependants"
                        handleRedirect={this.handleRedirect}
                    />
                </Fragment>
            );
        }
        return null;
    }

    renderErrorMessage() {
        const { hasErrors, showErrors } = this.state;

        if (hasErrors && showErrors) {
            return <ErrorMessage message="Please add a dependent first" />;
        }

        return null;
    }

    render() {
        const {
            formValue,
            isSubmitted,
            handleBack,
        } = this.props;

        const { hasErrors, submittedLocally } = this.state;

        const isAnySyncErrors = isEmpty(get(formValue, 'syncErrors', {}));
        const familyHasDependents = get(formValue, 'values.family_has_dependents', null);

        return (
            <Fragment>
                <FulfilledLine loadingPercent={40} lineColor="#6E0A7B" />
                <div className="container dis-f fd-c">
                    <h2 className="fw-b mt-xlarge mb-mlarge">
                        Tell us about anyone else you look after
                    </h2>
                    <form onSubmit={this.handleSubmit} className="know-you-form col span-8 dis-f fd-c">
                        <Field
                            component={YesNoManual}
                            componentClass="span-6"
                            name="family_has_dependents"
                            label="Besides children, do you have any other financial dependants?"
                            yesChecked={familyHasDependents === true}
                            noChecked={familyHasDependents === false}
                            normalize={normalizeBoolean}
                            handleClick={this.handleClick}
                            isShowErrors={isSubmitted}
                            validate={[
                                booleanRequired,
                            ]}
                        />
                        {this.renderDependentsBlock()}
                        {this.renderErrorMessage()}
                    </form>
                </div>
                <BackNextButtons
                    isActiveBack={false}
                    onClickNext={this.handleSubmit}
                    isActiveNext={(!isSubmitted || (isAnySyncErrors && isSubmitted)) && (!submittedLocally || (submittedLocally && !hasErrors))}
                    onClickBack={handleBack}
                    buttonClass="btn-purple"
                />
            </Fragment>
        );
    }
}

AboutDependents.propTypes = {
    formValue: PropTypes.object,
    dependents: PropTypes.arrayOf(PropTypes.object),
    handleSubmit: PropTypes.func.isRequired,
    handleBack: PropTypes.func.isRequired,
    isSubmitted: PropTypes.bool.isRequired,
    handleGetMePlan: PropTypes.func.isRequired,
    handlePatchPlan: PropTypes.func.isRequired,
    handleRequestsReset: PropTypes.func.isRequired,
    changeFieldValue: PropTypes.func.isRequired,
    isLocalRequestsDone: PropTypes.number.isRequired,
    history: PropTypes.object.isRequired,
};

AboutDependents.defaultProps = {
    formValue: {},
    dependents: [],
};

const mapStateToProps = state => ({
    dependents: get(state, 'plan.users.dependent', []),
    initialValues: {
        family_has_dependents: get(state, 'plan.family_has_dependents', null),
        updatePlan: true,
        planId: get(state, 'plan.plan_uuid', null),
        nextStage: 'work/main',
        prevStage: 'about-children',
    },
    isLocalRequestsDone: get(state, 'common.isLocalRequestsDone', 0),
});

const mapDispatchToProps = dispatch => ({
    changeFieldValue: (field, value) => {
        dispatch(change(AboutDependents.formName, field, value));
    },
    handlePatchPlan: bindActionCreators(patchPlan, dispatch),
    handleRequestsReset: bindActionCreators(requestsReset, dispatch),
});

const AboutDependentsWithHoc = SurveyHoc(AboutDependents, AboutDependents.surveyData);

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(AboutDependentsWithHoc));
