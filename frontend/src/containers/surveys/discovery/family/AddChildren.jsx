import React, { Component, Fragment } from 'react';
import { Field } from 'redux-form';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import PropTypes from 'prop-types';
import isEmpty from 'lodash/isEmpty';
import get from 'lodash/get';
import moment from 'moment';

import { required } from '../../../../utils/validation.helper';
import { LoadableDateYearPicker } from '../../../../components/Loadable';
import InputCustom from '../../../../components/InputCustom';
import { TitleHeader } from '../../../../components/TitleHeader';
import { BackNextButtons } from '../../../../components/BackNextButtons';
import AnythingElse from '../../../../components/AnythingElse';
import SurveyHoc from '../../../hoc/SurveyHoc';
import { createPlanUser } from '../../../../actions/PlanActions';
import { findUserByID, normalizeTimeValue } from '../../../../utils/helper';
import { updateUserInPlan } from '../../../../actions/UserActions';

/**
 * Render the content for user idle. Dumb component
 *
 */
class AddChildren extends Component {
    static formName = 'addChildren';

    static surveyData = {
        formName: AddChildren.formName,
        nextStage: 'nextStage',
        prevStage: 'prevStage',
        data: [
            {
                fields: [
                    'dob',
                    'first_name',
                    'last_name',
                    'additional_info',
                ],
                additionalCreateFields: [
                    {
                        is_active: false,
                        plan_user_type: 'child',
                    },
                ],
                additionalUpdateFields: [
                    {
                        is_active: false,
                    },
                ],
                isUpdate: 'updateChild',
                selector: 'childId',
                methodCreate: createPlanUser,
                methodUpdate: updateUserInPlan,
            },
        ],
    };

    constructor(props) {
        super(props);

        const {
            handleGetMePlan,
        } = this.props;

        handleGetMePlan();
    }

    render() {
        const {
            isSubmitted,
            formValue,
            handleSubmit,
            handleBack,
            editMode,
        } = this.props;

        const isAnySyncErrors = isEmpty(get(formValue, 'syncErrors', {}));

        return (
            <Fragment>
                <TitleHeader title={editMode ? 'Edit child' : 'Add child'} />
                <div className="container dis-f fd-c mt-5rem mb-2rem">
                    <form className="know-you-form col span-8 dis-f fd-c mt-xlarge">
                        <div className="col-span-8">
                            <div className="form-inline dis-f span-8">
                                <div className="input-wrap col span-4">
                                    <Field
                                        component={InputCustom}
                                        label="First Name"
                                        type="text"
                                        name="first_name"
                                        placeholder="First name"
                                        isShowErrors={isSubmitted}
                                        validate={[
                                            required,
                                        ]}
                                    />
                                </div>
                                <div className="input-wrap col-last span-4">
                                    <Field
                                        component={InputCustom}
                                        label="Last Name"
                                        type="text"
                                        name="last_name"
                                        placeholder="Last name"
                                        isShowErrors={isSubmitted}
                                        validate={[
                                            required,
                                        ]}
                                    />
                                </div>
                            </div>
                            <Field
                                component={LoadableDateYearPicker}
                                formName={AddChildren.formName}
                                label="Date of birth"
                                name="dob"
                                isShowErrors={isSubmitted}
                                normalize={normalizeTimeValue}
                                validate={[
                                    required,
                                ]}
                            />
                            <AnythingElse
                                name="additional_info"
                                isShowErrors={isSubmitted}
                                placeholder="E.g. are you expecting to send them to a new school, help them buy their first home, etc"
                            />
                        </div>
                    </form>
                </div>
                <BackNextButtons
                    isActiveBack={false}
                    backText="Cancel"
                    nextText="Save"
                    onClickNext={handleSubmit}
                    isActiveNext={!isSubmitted || (isAnySyncErrors && isSubmitted)}
                    onClickBack={handleBack}
                    buttonClass="btn-purple"
                />
            </Fragment>
        );
    }
}

AddChildren.propTypes = {
    formValue: PropTypes.object,
    handleSubmit: PropTypes.func.isRequired,
    handleBack: PropTypes.func.isRequired,
    isSubmitted: PropTypes.bool.isRequired,
    handleGetMePlan: PropTypes.func.isRequired,
    editMode: PropTypes.bool,
};

AddChildren.defaultProps = {
    formValue: {},
    editMode: false,
};

const mapStateToProps = (state, { match: { params } }) => {
    const child = findUserByID(state, 'plan.users.child', params.children_uuid);

    return ({
        initialValues: {
            dob: !isEmpty(child) ? moment(get(child, 'profile.dob', null))
                .format('LL')
                .toString() : null,
            first_name: get(child, 'profile.first_name', null),
            last_name: get(child, 'profile.last_name', null),
            additional_info: get(child, 'additional_info', null),
            updateChild: !isEmpty(params.children_uuid),
            childId: params.children_uuid,
            nextStage: '/survey/family/about-children',
            prevStage: '/survey/family/about-children',
        },
        editMode: !isEmpty(params.children_uuid),
    });
};

const AddChildrenWithHoc = SurveyHoc(AddChildren, AddChildren.surveyData);

export default withRouter(connect(mapStateToProps)(AddChildrenWithHoc));
