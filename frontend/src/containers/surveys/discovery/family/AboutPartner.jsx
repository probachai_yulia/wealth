import React, { Component, Fragment } from 'react';
import { change, Field } from 'redux-form';
import { withRouter } from 'react-router-dom';
import get from 'lodash/get';
import isEmpty from 'lodash/isEmpty';
import isNull from 'lodash/isNull';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

import moment from 'moment/moment';
import { LoadableAddress, LoadableDateYearPicker } from '../../../../components/Loadable';
import YesNo from '../../../../components/YesNo';
import CheckboxesBlock from '../../../../components/CheckboxesBlock';
import { BackNextButtons } from '../../../../components/BackNextButtons';
import InputCustom from '../../../../components/InputCustom';
import FulfilledLine from '../../../../components/FulfilledLine';
import ModalForm from '../../../../components/ModalForm';
import ModalText from '../../../../components/ModalText';
import {
    booleanRequired,
    isEmail,
    isPhoneRequired,
    required,
    requiredData,
} from '../../../../utils/validation.helper';
import AnythingElse from '../../../../components/AnythingElse';
import {
    createPlanUser, patchPlan, addPartnerAddress, updatePlanAddress,
} from '../../../../actions/PlanActions';
import { updateUserInPlan, acceptPartnerTerms } from '../../../../actions/UserActions';
import {
    getAddressByID,
    normalizeBoolean,
    normalizeTimeValue,
} from '../../../../utils/helper';
import ModalInformationIcon from '../../../../components/ModalInformationIcon';
import ComplexSurveyHoc from '../../../hoc/ComplexSurveyHoc';
import YesNoManual from '../../../../components/YesNoManual';

/**
 * Render the content for user idle. Dumb component
 * @return {JSX.Element}
 */
class AboutPartner extends Component {
    static formName = 'aboutPartner';

    static surveyData = {
        formName: AboutPartner.formName,
        data: [
            {
                fields: [
                    'email',
                    'phone',
                    'first_name',
                    'last_name',
                    'dob',
                    'us_connections',
                    'has_us_connections',
                    'additional_info',
                ],
                additionalCreateFields: [
                    {
                        is_active: true,
                        plan_user_type: 'partner',
                    },
                ],
                additionalUpdateFields: [
                    {
                        is_active: true,
                    },
                ],
                isSend: 'addPartner',
                isUpdate: 'updatePartner',
                selector: 'partnerId',
                methodCreate: createPlanUser,
                methodUpdate: updateUserInPlan,
            },
            {
                fields: [
                    'family_has_partner',
                ],
                additionalUpdateFields: [
                    {},
                ],
                isSend: 'updatePlan',
                isUpdate: 'updatePlan',
                selector: 'planId',
                methodUpdate: patchPlan,
            },
        ],
        nextData: [
            {
                fields: [],
                isSend: 'partnerAcceptTerms',
                isUpdate: 'updatePartnerAccept',
                selector: 'partnerId',
                methodCreate: acceptPartnerTerms,
            },
            {
                fields: [
                    'postcode',
                    'address_1',
                    'address_2',
                    'country_cd',
                    'city',
                ],
                additionalCreateFields: [
                    {
                        is_active: true,
                    },
                ],
                additionalUpdateFields: [
                    {
                        is_active: true,
                    },
                ],
                isSend: 'partner_does_not_has_same_address',
                isUpdate: 'updatePartnerAddress',
                selector: 'partnerAddressId',
                methodCreate: addPartnerAddress,
                methodUpdate: updatePlanAddress,
            },
            {
                fields: [
                    'address_uuid',
                ],
                additionalUpdateFields: [
                    {
                        is_active: true,
                    },
                ],
                isSend: 'partner_has_same_address',
                isUpdate: 'saveMainAddressToPartner',
                selector: 'partnerId',
                methodUpdate: updateUserInPlan,
            },
        ],
    };

    constructor(props) {
        super(props);

        this.state = {
            partnerModalIsVisible: false,
            textModalIsVisible: false,
        };
        const {
            handleGetMePlan,
        } = this.props;

        handleGetMePlan();

        this.renderPartnerBlock = this.renderPartnerBlock.bind(this);
        this.renderContactBlock = this.renderContactBlock.bind(this);
        this.renderConnectionBlock = this.renderConnectionBlock.bind(this);
        this.renderPartnerAddressBlock = this.renderPartnerAddressBlock.bind(this);
        this.changeHasPartner = this.changeHasPartner.bind(this);
        this.closeAllModals = this.closeAllModals.bind(this);
        this.openConfirmModal = this.openConfirmModal.bind(this);
        this.openPartnerModal = this.openPartnerModal.bind(this);
        this.setHasPartnerTrue = this.setHasPartnerTrue.bind(this);
        moment.locale('en-gb');
    }

    setHasPartnerTrue() {
        const {
            changeFieldValue,
        } = this.props;

        this.setState(() => ({
            textModalIsVisible: false,
            renderPartnerBlock: false,
        }));

        changeFieldValue('family_has_partner', true);
    }

    changeHasPartner = (e) => {
        e.stopPropagation();
        const {
            changeFieldValue,
            reset,
        } = this.props;
        const clickedValue = normalizeBoolean(e.target.value);
        if (clickedValue) {
            this.setState({
                partnerModalIsVisible: true,
            });
        } else {
            this.closeAllModals();
            reset();
            changeFieldValue('family_has_partner', false);
        }
    };

    closeAllModals() {
        this.setState({
            partnerModalIsVisible: false,
            textModalIsVisible: false,
        });
    }

    openConfirmModal() {
        const {
            formValue,
        } = this.props;

        const partnerName = get(formValue, 'values.first_name', null);
        const partnerLastName = get(formValue, 'values.last_name', null);
        if (!(isEmpty(partnerName) || isEmpty(partnerLastName))) {
            this.setState(() => ({
                partnerModalIsVisible: false,
                textModalIsVisible: true,
            }));
        }
    }

    openPartnerModal() {
        this.setState(() => ({
            partnerModalIsVisible: true,
            textModalIsVisible: false,
        }));
    }

    renderModalForm() {
        const {
            formValue,
            isSubmitted,
        } = this.props;
        const {
            partnerModalIsVisible,
            textModalIsVisible,
        } = this.state;

        const partnerName = get(formValue, 'values.first_name', null);
        const partnerLastName = get(formValue, 'values.last_name', null);
        return (
            <Fragment>
                <ModalForm
                    nameFirst="first_name"
                    nameSecond="last_name"
                    placeholderFirst="First name"
                    placeholderSecond="Last name"
                    labelFirst="First name"
                    labelSecond="Last name"
                    modalTitle="What is your partner's name?"
                    visible={partnerModalIsVisible}
                    isSubmitted={isSubmitted}
                    onBlurClick={this.closeAllModals}
                    buttonDisabled={isEmpty(partnerName) || isEmpty(partnerLastName)}
                    handleNext={this.openConfirmModal}
                    closeButton
                />
                <ModalText
                    modalTitle={`${partnerName}'s Consent`}
                    // eslint-disable-next-line max-len
                    text={`Our <a href="/terms-of-business" target="_blank" rel="noreferrer noopener">Terms of Business</a> outline how we will work with both of you. Please confirm you have ${partnerName}'s consent to accept the Terms of Business on their behalf.`}
                    visible={textModalIsVisible}
                    handleCancel={this.openPartnerModal}
                    onBlurClick={this.closeAllModals}
                    handleConfirm={this.setHasPartnerTrue}
                    textBackButton="Back"
                    closeButton
                />
            </Fragment>
        );
    }

    renderContactBlock() {
        const {
            hasPartner,
            isSubmitted,
        } = this.props;

        return (
            <Fragment>
                <div className="input-label">
                    Contact details
                </div>
                <div className="dis-f span-8 mt-1rem">
                    <Field
                        name="email"
                        component={InputCustom}
                        readOnly={hasPartner}
                        extraClasses="col span-4"
                        label="Email"
                        type="text"
                        placeholder="Email"
                        isShowErrors={isSubmitted}
                        validate={[
                            required,
                            isEmail,
                        ]}
                    />
                    <Field
                        name="phone"
                        component={InputCustom}
                        extraClasses="col-last span-4"
                        label="Phone"
                        type="number"
                        placeholder="Phone"
                        isShowErrors={isSubmitted}
                        validate={[
                            required,
                            isPhoneRequired,
                        ]}
                    />
                </div>
            </Fragment>
        );
    }

    renderConnectionBlock() {
        const {
            formValue,
            connectionOptions,
            isSubmitted,
        } = this.props;
        const isConnectedToUSA = get(formValue, 'values.has_us_connections', null);
        const filedNames = connectionOptions.map(fieldName => `values.${fieldName.type}`);
        let isHideErrors = false;
        filedNames.forEach((name) => {
            isHideErrors = isHideErrors || get(formValue, name, false);
        });

        return (
            isConnectedToUSA === true
            && (
                <CheckboxesBlock
                    checkboxes={connectionOptions}
                    label="Select all that apply"
                    name="us_connections"
                    customErrors="Select at least one"
                    isShowErrors={isSubmitted}
                    formValue={formValue}
                />
            )
        );
    }

    renderPartnerAddressBlock() {
        const {
            formValue,
            countriesOptions,
            isSubmitted,
        } = this.props;

        const partnerHasSameAddress = get(formValue, 'values.partner_has_same_address', null);

        return (
            <Fragment>
                {partnerHasSameAddress === false
                && (
                    <LoadableAddress
                        isShowErrors={isSubmitted}
                        formName={AboutPartner.formName}
                        postcode={get(formValue, 'values.postcode', '0')}
                        selectAddress={get(formValue, 'values.select-address', null)}
                        label="What is their address?"
                        countriesOptions={countriesOptions}
                    />
                )
                }
            </Fragment>
        );
    }

    renderPartnerBlock() {
        const { formValue, isSubmitted } = this.props;
        const showPartnerBlock = get(formValue, 'values.family_has_partner', null) === true;
        return (showPartnerBlock
            && (
                <Fragment>
                    <div className="hr span-8" />
                    <div className="dis-f span-8">
                        <div className="input-wrap col span-4">
                            <Field
                                component={InputCustom}
                                label="First Name"
                                type="text"
                                name="first_name"
                                placeholder="First name"
                                isShowErrors={isSubmitted}
                                validate={[
                                    required,
                                ]}
                            />
                        </div>
                        <div className="input-wrap col-last span-4">
                            <Field
                                component={InputCustom}
                                label="Last Name"
                                name="last_name"
                                type="text"
                                placeholder="Last name"
                                isShowErrors={isSubmitted}
                                validate={[
                                    required,
                                ]}
                            />
                        </div>
                    </div>
                    <Field
                        component={LoadableDateYearPicker}
                        formName={AboutPartner.formName}
                        name="dob"
                        label="Date of birth"
                        minAge={18}
                        isShowErrors={isSubmitted}
                        normalize={normalizeTimeValue}
                        validate={[
                            requiredData,
                        ]}
                    />
                    <div className="hr span-8" />
                    <Field
                        component={YesNo}
                        name="has_us_connections"
                        label="Do they have any connections to the US?"
                        isShowErrors={isSubmitted}
                        yesChecked={get(formValue, 'values.has_us_connections', null) === true}
                        noChecked={get(formValue, 'values.has_us_connections', null) === false}
                        normalize={normalizeBoolean}
                        validate={[
                            booleanRequired,
                        ]}
                    />
                    {this.renderConnectionBlock()}
                    <div className="hr span-8" />
                    <Field
                        component={YesNo}
                        name="partner_has_same_address"
                        label={`Does ${get(formValue, 'values.first_name', null)} live at the same address as you?`}
                        isShowErrors={isSubmitted}
                        yesChecked={get(formValue, 'values.partner_has_same_address', null) === true}
                        noChecked={get(formValue, 'values.partner_has_same_address', null) === false}
                        normalize={normalizeBoolean}
                        validate={[
                            booleanRequired,
                        ]}
                    />
                    {this.renderPartnerAddressBlock()}
                    <div className="hr span-8" />
                    {this.renderContactBlock()}
                    <AnythingElse
                        name="additional_info"
                        isShowErrors={isSubmitted}
                        placeholder="E.g. recently engaged"
                    />
                </Fragment>
            ));
    }

    render() {
        const {
            formValue,
            handleSubmit,
            handleBack,
            isSubmitted,
        } = this.props;

        const isAnySyncErrors = isEmpty(get(formValue, 'syncErrors', {}));

        return (
            <Fragment>
                <FulfilledLine
                    loadingPercent={20}
                    lineColor="#6E0A7B"
                />
                <div className="container dis-fs">
                    <div className="col span-8">
                        <h2 className="fw-b mb-mlarge">
                            Tell us about your partner
                        </h2>
                        <form onSubmit={this.handleSubmit} className="know-you-form col span-8  dis-f fd-c mb-5rem">
                            <Field
                                component={YesNoManual}
                                name="family_has_partner"
                                label="Do you want to add a partner to your plan?"
                                yesChecked={get(formValue, 'values.family_has_partner', null) === true}
                                noChecked={get(formValue, 'values.family_has_partner', null) === false}
                                normalize={normalizeBoolean}
                                handleClick={this.changeHasPartner}
                                isShowErrors={isSubmitted}
                                validate={[
                                    booleanRequired,
                                ]}
                            />
                            {this.renderModalForm()}
                            {this.renderPartnerBlock()}
                        </form>
                    </div>
                    <div className="col span-3 col-last explain_text mt-4rem">
                        <ModalInformationIcon
                            title="Why we’re asking this"
                            // eslint-disable-next-line max-len
                            text="By understanding your family circumstances in full, we can build a plan that helps create the best possible future for all of you."
                        />
                    </div>
                </div>
                <BackNextButtons
                    isActiveBack={false}
                    onClickNext={handleSubmit}
                    isActiveNext={!isSubmitted || (isAnySyncErrors && isSubmitted)}
                    onClickBack={handleBack}
                    buttonClass="btn-purple"
                />
            </Fragment>
        );
    }
}

AboutPartner.propTypes = {
    formValue: PropTypes.object,
    reset: PropTypes.func,
    connectionOptions: PropTypes.arrayOf(PropTypes.object),
    countriesOptions: PropTypes.arrayOf(PropTypes.object),
    handleGetMePlan: PropTypes.func.isRequired,
    hasPartner: PropTypes.bool.isRequired,
    // eslint-disable-next-line react/no-unused-prop-types
    partnerAcceptTerms: PropTypes.bool,
    // eslint-disable-next-line react/no-unused-prop-types
    partnerId: PropTypes.oneOfType([PropTypes.string, PropTypes.object]),
    isSubmitted: PropTypes.bool.isRequired,
    changeFieldValue: PropTypes.func.isRequired,
    handleSubmit: PropTypes.func.isRequired,
    handleBack: PropTypes.func.isRequired,
};

AboutPartner.defaultProps = {
    formValue: {},
    reset: null,
    partnerId: '',
    connectionOptions: [],
    countriesOptions: [],
    partnerAcceptTerms: false,
};

const mapStateToProps = (state) => {
    const formValue = get(state, `form.${AboutPartner.formName}`, {});
    const partner = get(state, 'plan.users.partner', null);
    const partnerAddress = getAddressByID(state, 'plan.addresses', 'plan.users.partner.address_uuid');
    const partnerAddressId = get(state, 'plan.users.partner.address_uuid', null);
    const userAddressId = get(state, 'plan.users.main.address_uuid', null);
    const familyHasPartnerValue = get(formValue, 'values.family_has_partner') === true;

    return {
        formValue,
        connectionOptions: get(state, 'options.us_connections', []),
        countriesOptions: get(state, 'options.countries', []),
        initialValues: {
            email: get(partner, 'profile.email', null),
            first_name: get(partner, 'profile.first_name', null),
            last_name: get(partner, 'profile.last_name', null),
            additional_info: get(partner, 'additional_info', null),
            dob: !isEmpty(get(partner, 'profile.dob', null)) ? moment(get(partner, 'profile.dob', null))
                .format('LL')
                .toString() : null,
            address_uuid: userAddressId,
            phone: get(partner, 'profile.phone', null),
            family_has_partner: get(state, 'plan.family_has_partner', null),
            has_us_connections: get(state, 'plan.users.partner.has_us_connections', null),
            us_connections: get(state, 'plan.users.partner.us_connections', []),
            partner_has_same_address: isEmpty(partnerAddressId) ? null : userAddressId === partnerAddressId,
            postcode: get(partnerAddress, 'postcode', null),
            address_1: get(partnerAddress, 'address_1', null),
            address_2: get(partnerAddress, 'address_2', null),
            city: get(partnerAddress, 'city', null),
            country_cd: get(partnerAddress, 'country_cd', null),
        },
        mainUserAddressId: get(state, 'plan.users.main.address_uuid', null),
        partnerAcceptTerms: familyHasPartnerValue && isNull(get(state, 'plan.users.partner.terms_of_business_date', null)),
        updatePartnerAccept: false,
        partnerAddressId: get(state, 'plan.users.partner.address_uuid', null),
        hasPartner: !isNull(get(state, 'plan.users.partner', null)),
        partnerId: get(state, 'plan.users.partner.profile_uuid', null) || get(state, 'users.temporaryUser', null),
        isLocalRequestsDone: get(state, 'common.isLocalRequestsDone', 0),
        isRequestsDone: get(state, 'common.isRequestsDone', 0),
        nextStage: 'about-children',
        prevStage: 'know-you',
        planId: get(state, 'plan.plan_uuid', null),
        updatePlan: true,
        updatePartner: !isEmpty(get(state, 'plan.users.partner', null)),
        updatePartnerAddress: false,
        saveMainAddressToPartner: true,
        partner_has_same_address: familyHasPartnerValue && get(formValue, 'values.partner_has_same_address', null) === true,
        partner_does_not_has_same_address: familyHasPartnerValue && get(formValue, 'values.partner_has_same_address', null) === false,
        addPartner: familyHasPartnerValue,
    };
};

const mapDispatchToProps = dispatch => ({
    changeFieldValue: (field, value) => {
        dispatch(change(AboutPartner.formName, field, value));
    },
});

const AboutPartnerWithComplexHoc = ComplexSurveyHoc(AboutPartner, AboutPartner.surveyData);

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(AboutPartnerWithComplexHoc));
