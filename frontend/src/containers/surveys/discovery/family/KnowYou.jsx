import React, { Component, Fragment } from 'react';
import { change, Field } from 'redux-form';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import PropTypes from 'prop-types';
import get from 'lodash/get';
import isEmpty from 'lodash/isEmpty';
import findIndex from 'lodash/findIndex';
import moment from 'moment';
import arrayMove from 'array-move';
import { bindActionCreators } from 'redux';

import Select from '../../../../components/Select';
import { BackNextButtons } from '../../../../components/BackNextButtons';
import {
    LoadableAddress,
    LoadableDateYearPicker,
} from '../../../../components/Loadable';
import FulfilledLine from '../../../../components/FulfilledLine';
import {
    booleanRequired,
    required,
    requiredData,
    requiredMaritalStatus,
    requiredAtLeastOne,
} from '../../../../utils/validation.helper';
import {
    patchPlan,
    addUserAddress,
    addUserPostalAddress,
} from '../../../../actions/PlanActions';
import { getMePlan, updateUserInPlan } from '../../../../actions/UserActions';
import MultipleSelectBlock from '../../../../components/MultipleSelectBlock';
import CheckboxesBlockArray from '../../../../components/CheckboxesBlockArray';
import YesNo from '../../../../components/YesNo';
import {
    normalizeBoolean, normalizeTimeValue, getAddressByID, isPrimaryAddress,
} from '../../../../utils/helper';
import CheckedCheckboxesBlock from '../../../../components/CheckedCheckboxesBlock';
import ModalInformationIcon from '../../../../components/ModalInformationIcon';
import ComplexSurveyHoc from '../../../hoc/ComplexSurveyHoc';

/**
 * Render the content for user idle. Dumb component
 *
 */

class KnowYou extends Component {
    static formName = 'knowYou';

    static surveyData = {
        formName: KnowYou.formName,
        nextStage: 'nextStage',
        prevStage: 'prevStage',
        data: [
            {
                fields: [
                    'marital_status',
                ],
                isSend: 'sendUserData',
                isUpdate: 'updatePlan',
                selector: 'planId',
                methodUpdate: patchPlan,
            },
            {
                fields: [
                    'first_name',
                    'last_name',
                    'dob',
                    'birth_country_cd',
                    'has_us_connections',
                    'us_connections',
                    'citizenships',
                ],
                additionalCreateFields: [
                    {
                        plan_user_type: 'main',
                        is_active: true,
                    },
                ],
                additionalUpdateFields: [
                    {
                        is_active: true,
                    },
                ],
                isSend: 'sendUserData',
                isUpdate: 'updateUser',
                selector: 'userId',
                methodUpdate: updateUserInPlan,
            },
        ],
        nextData: [
            {
                fields: [
                    'postcode',
                    'address_1',
                    'address_2',
                    'city',
                    'country_cd',
                    'is_primary_address',
                ],
                additionalCreateFields: [
                    {
                        is_active: true,
                    },
                ],
                isSend: 'sendAddress',
                isUpdate: 'updateAddress',
                methodCreate: addUserAddress,
            },
            {
                fields: [
                    'postal_address_uuid',
                ],
                additionalCreateFields: [
                    {
                        is_active: true,
                    },
                ],
                additionalUpdateFields: [
                    {
                        is_active: true,
                    },
                ],
                isSend: 'sendNullPostalAddress',
                isUpdate: 'sendNullPostalAddress',
                selector: 'userId',
                methodUpdate: updateUserInPlan,
            },
            {
                fields: [
                    'postal_postcode',
                    'postal_address_1',
                    'postal_address_2',
                    'postal_city',
                    'postal_country_cd',
                ],
                additionalCreateFields: [
                    {
                        is_active: true,
                    },
                ],
                isSend: 'sendPostalAddress',
                isUpdate: 'updatePostalAddress',
                methodCreate: addUserPostalAddress,
            },
        ],
    };

    constructor(props) {
        super(props);

        this.renderPostalAddressBlock = this.renderPostalAddressBlock.bind(this);
        this.handleBack = this.handleBack.bind(this);

        const {
            handleGetMePlan,
        } = this.props;

        handleGetMePlan();
    }

    handleBack() {
        const { history, formValue } = this.props;
        history.push(get(formValue, 'values.prevStage', null));
    }

    renderPostalAddressBlock = () => {
        const { formValue, countriesOptions, isSubmitted } = this.props;

        const isPostalAddress = get(formValue, 'values.is_primary_address', null);

        if (isPostalAddress === false) {
            return (
                <LoadableAddress
                    isPostal
                    isShowErrors={isSubmitted}
                    countriesOptions={countriesOptions}
                    formName={KnowYou.formName}
                    postcode={get(formValue, 'values.postal_postcode', null)}
                    selectAddress={get(formValue, 'values.select-primary-address', null)}
                    label="What is your postal address?"
                />
            );
        }

        return null;
    };

    render() {
        const {
            formValue,
            changeFieldValue,
            maritalOptions,
            countriesOptions,
            connectionOptions,
            isSubmitted,
            handleSubmit,
            handleBack,
        } = this.props;

        // eslint-disable-next-line max-len
        const orderedCountriesOptions = arrayMove(countriesOptions, findIndex(countriesOptions, o => o.code === 'GB', 0));
        const isAnySyncErrors = isEmpty(get(formValue, 'syncErrors', {}));

        return (
            <Fragment>
                <FulfilledLine loadingPercent={10} lineColor="#6E0A7B" />
                <section className="container dis-f jc-sb">
                    <div className="col span-8">
                        <h2 className="fw-b mt-xlarge mb-mlarge">
                            Getting to know you
                        </h2>
                        <form className="know-you-form col span-8 dis-f fd-c">
                            <div className="dis-f jc-sb">
                                <Field
                                    component={LoadableDateYearPicker}
                                    formName={KnowYou.formName}
                                    name="dob"
                                    label="What is your date of birth?"
                                    minAge={18}
                                    isShowErrors={isSubmitted}
                                    normalize={normalizeTimeValue}
                                    validate={[
                                        requiredData,
                                    ]}
                                />
                                <Field
                                    name="birth_country_cd"
                                    component={Select}
                                    extraClasses="span-4 col-last"
                                    label="Where were you born?"
                                    placeholder="Select country"
                                    options={orderedCountriesOptions}
                                    isShowErrors={isSubmitted}
                                    validate={[
                                        required,
                                    ]}
                                />
                            </div>
                            <MultipleSelectBlock
                                name="citizenships"
                                fieldsName="citizenships"
                                options={orderedCountriesOptions}
                                isShowErrors={isSubmitted}
                                extraClasses="mt-2rem"
                                label="+ Add another citizenship"
                                placeholder="Select country"
                                buttonLabel="Remove Citizenship"
                                blockLabel="What is your citizenship?"
                            />
                            <Field
                                name="marital_status"
                                component={Select}
                                extraClasses="span-4"
                                label="What's your marital status?"
                                placeholder="Select your marital status"
                                options={maritalOptions}
                                isShowErrors={isSubmitted}
                                validate={[
                                    requiredMaritalStatus,
                                ]}
                            />
                            <div className="hr span-8" />
                            <Field
                                component={YesNo}
                                componentClass="span-8"
                                name="has_us_connections"
                                label="Do you have any connections to the US?"
                                yesChecked={get(formValue, 'values.has_us_connections', null) === true}
                                noChecked={get(formValue, 'values.has_us_connections', null) === false}
                                normalize={normalizeBoolean}
                                isShowErrors={isSubmitted}
                                hintMessage="We need to take special care of our US clients as specific tax rules may apply."
                                validate={[
                                    booleanRequired,
                                ]}
                            />
                            {get(formValue, 'values.has_us_connections', null) === true
                                ? (
                                    <CheckboxesBlockArray
                                        formValue={formValue}
                                        checkboxes={connectionOptions}
                                        isSubmitted={isSubmitted}
                                        name="us_connections"
                                        fieldName="us_connections"
                                        blockLabel="Select all that apply"
                                        validate={[
                                            requiredAtLeastOne,
                                        ]}
                                    />
                                ) : null}
                            <div className="hr span-8" />
                            <LoadableAddress
                                isShowErrors={isSubmitted}
                                formName={KnowYou.formName}
                                postcode={get(formValue, 'values.postcode', null)}
                                selectAddress={get(formValue, 'values.select-address', null)}
                                label="At what address do you currently live?"
                                countriesOptions={countriesOptions}
                            />
                            <Field
                                component={CheckedCheckboxesBlock}
                                label="Is this your postal address?"
                                name="is_primary_address"
                                fieldName="is_primary_address"
                                formValue={formValue}
                                yesChecked={get(formValue, 'values.is_primary_address', null) === true}
                                noChecked={get(formValue, 'values.is_primary_address', null) === false}
                                changeFieldValue={changeFieldValue}
                                isShowErrors={isSubmitted}
                                validate={[
                                    booleanRequired,
                                ]}
                            />
                            {this.renderPostalAddressBlock()}
                        </form>
                    </div>
                    <div className="col span-3 col-last explain_text mt-4rem p-r">
                        <ModalInformationIcon
                            title="Partner with (tax) benefits"
                            // eslint-disable-next-line max-len
                            text="There are a number of tax perks available to married couples and civil partners. It can often be beneficial to transfer assets to the partner with the lower tax rate. In addition, any transfers between partners are free of capital gains tax."
                        />
                    </div>
                </section>
                <BackNextButtons
                    isActiveBack={false}
                    onClickNext={handleSubmit}
                    isActiveNext={!isSubmitted || (isAnySyncErrors && isSubmitted)}
                    buttonClass="btn-purple"
                    onClickBack={handleBack}
                />
            </Fragment>
        );
    }
}

KnowYou.propTypes = {
    history: PropTypes.object.isRequired,
    formValue: PropTypes.object,
    isSubmitted: PropTypes.bool,
    maritalOptions: PropTypes.arrayOf(PropTypes.object),
    connectionOptions: PropTypes.arrayOf(PropTypes.object),
    countriesOptions: PropTypes.arrayOf(PropTypes.object),
    handleGetMePlan: PropTypes.func.isRequired,
    changeFieldValue: PropTypes.func.isRequired,
    // eslint-disable-next-line react/no-unused-prop-types
    addressId: PropTypes.string,
    // eslint-disable-next-line react/no-unused-prop-types
    postalAddressId: PropTypes.string,
    // eslint-disable-next-line react/no-unused-prop-types
    userId: PropTypes.string,
    handleSubmit: PropTypes.func.isRequired,
    handleBack: PropTypes.func.isRequired,
};

KnowYou.defaultProps = {
    formValue: {},
    isSubmitted: false,
    maritalOptions: [],
    connectionOptions: [],
    countriesOptions: [],
    addressId: null,
    userId: null,
    postalAddressId: null,
};

const mapStateToProps = (state) => {
    const { values } = get(state, `form.${KnowYou.formName}`, {});
    const addressId = get(state, 'plan.users.main.address_uuid', null);
    const postalAddressId = get(state, 'plan.users.main.postal_address_uuid', null);
    const address = getAddressByID(state, 'plan.addresses', 'plan.users.main.address_uuid');
    const postalAddress = getAddressByID(state, 'plan.addresses', 'plan.users.main.postal_address_uuid');
    const isPostalAddress = get(values, 'is_primary_address', null);
    const isNotPostalAddress = get(values, 'is_primary_address', null) === false;

    return ({
        initialValues: {
            // eslint-disable-next-line max-len
            dob: !isEmpty(get(state, 'plan.users.main.profile.dob', null)) ? moment(get(state, 'plan.users.main.profile.dob', null))
                .format('LL')
                .toString() : null,
            first_name: get(state, 'plan.users.main.profile.first_name', null),
            last_name: get(state, 'plan.users.main.profile.last_name', null),
            marital_status: get(state, 'plan.marital_status', null),
            birth_country_cd: get(state, 'plan.users.main.profile.birth_country_cd', null),
            // eslint-disable-next-line max-len
            citizenships: !isEmpty(get(state, 'plan.users.main.profile.citizenships', [null])) ? get(state, 'plan.users.main.profile.citizenships', [null]) : [null],
            postcode: get(address, 'postcode', null),
            address_1: get(address, 'address_1', null),
            address_2: get(address, 'address_2', null),
            city: get(address, 'city', null),
            country_cd: get(address, 'country_cd', null),
            has_us_connections: get(state, 'plan.users.main.has_us_connections', null),
            // eslint-disable-next-line max-len
            us_connections: !isEmpty(get(state, 'plan.users.main.us_connections', [])) ? get(state, 'plan.users.main.us_connections', []) : [],
            // eslint-disable-next-line max-len
            is_primary_address: isPrimaryAddress(get(state, 'plan.users.main.address_uuid', null), get(state, 'plan.users.main.postal_address_uuid', null)),
            postal_postcode: get(postalAddress, 'postcode', null),
            postal_address_1: get(postalAddress, 'address_1', null),
            postal_address_2: get(postalAddress, 'address_2', null),
            postal_city: get(postalAddress, 'city', null),
            postal_country_cd: get(postalAddress, 'country_cd', null),
            planId: get(state, 'plan.plan_uuid', null),
            userId: get(state, 'plan.users.main.profile_uuid', null),
        },
        maritalOptions: get(state, 'options.marital_statuses', []),
        connectionOptions: get(state, 'options.us_connections', []),
        countriesOptions: get(state, 'options.countries', []),
        updateUserPostalAddress: !isEmpty(get(state, 'plan.users.main.postal_address_uuid', null)),
        userId: get(state, 'plan.users.main.profile_uuid', null),
        planId: get(state, 'plan.plan_uuid', null),
        updateUser: true,
        updatePlan: true,
        sendUserData: true,
        sendAddress: true,
        updateAddress: false,
        addressId,
        postalAddressId,
        sendPostalAddress: isNotPostalAddress,
        updatePostalAddress: false,
        postal_address_uuid: null,
        sendNullPostalAddress: isPostalAddress && !isEmpty(postalAddressId),
        nextStage: 'about-partner',
        prevStage: '../dashboard',
    });
};

const mapDispatchToProps = dispatch => ({
    changeFieldValue: (field, value) => {
        dispatch(change(KnowYou.formName, field, value));
    },
    handleGetMePlan: bindActionCreators(getMePlan, dispatch),
});

const KnowYouWithComplexHoc = ComplexSurveyHoc(KnowYou, KnowYou.surveyData);

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(KnowYouWithComplexHoc));
