import React, { Component, Fragment } from 'react';
import { change, Field } from 'redux-form';
import get from 'lodash/get';
import isEmpty from 'lodash/isEmpty';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import PropTypes from 'prop-types';
import { bindActionCreators } from 'redux';

import YesNoManual from '../../../../components/YesNoManual';
import { BackNextButtons } from '../../../../components/BackNextButtons';
import FulfilledLine from '../../../../components/FulfilledLine';
import AddDashedButton from '../../../../components/AddDashedButton';
import { booleanRequired } from '../../../../utils/validation.helper';
import SurveyHoc from '../../../hoc/SurveyHoc';
import { patchPlan } from '../../../../actions/PlanActions';
import FinanceAboutTable from '../../../../components/FinanceAboutTable';
import { getPersonAge, normalizeBoolean } from '../../../../utils/helper';
import ModalInformationIcon from '../../../../components/ModalInformationIcon';
import { requestsReset } from '../../../../actions/CommonActions';
import { ErrorMessage } from '../../../../components/ErrorMessage';

/**
 * Render the content for user idle. Dumb component
 *
 */
class AboutChildren extends Component {
    static formName = 'aboutChildren';

    static surveyData = {
        formName: AboutChildren.formName,
        nextStage: 'nextStage',
        prevStage: 'prevStage',
        data: [
            {
                fields: [
                    'family_has_children',
                ],
                additionalCreateFields: [
                    {},
                ],
                isUpdate: 'updatePlan',
                selector: 'planId',
                methodUpdate: patchPlan,
            },
        ],
    };

    constructor(props) {
        super(props);

        this.state = {
            showErrors: false,
            submittedLocally: false,
            hasErrors: false,
        };

        this.renderChildrenBlock = this.renderChildrenBlock.bind(this);
        this.renderErrorMessage = this.renderErrorMessage.bind(this);
        this.handleClick = this.handleClick.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.renderChildren = this.renderChildren.bind(this);
        this.getDataFromChild = this.getDataFromChild.bind(this);
        this.handleRedirect = this.handleRedirect.bind(this);

        const {
            handleGetMePlan,
        } = this.props;

        handleGetMePlan();
    }

    componentDidUpdate() {
        const {
            isLocalRequestsDone,
            handleRequestsReset,
            history,
        } = this.props;

        if (isLocalRequestsDone === 1) {
            handleRequestsReset();
            history.push('/survey/family/add-children');
        }
    }

    getDataFromChild = child => [{
        icon: 'user.svg',
        edit_link: `/survey/family/add-children/${child.profile_uuid}`,
        subtitle: getPersonAge(child.profile.dob),
        title: child.profile.first_name,
        extra_information: child.additional_info,
        hideAccordion: isEmpty(child.additional_info),
    }];

    handleSubmit = (event) => {
        event.preventDefault();
        const { formValue: { values }, children, handleSubmit } = this.props;
        const familyHasChildren = get(values, 'family_has_children', null);

        this.setState(() => ({ submittedLocally: true }));

        if (familyHasChildren === true && !children.length) {
            this.setState(() => ({ hasErrors: true }));
        } else handleSubmit(event);
    };

    handleClick({ target: { value } }) {
        const { changeFieldValue } = this.props;

        const valueClicked = normalizeBoolean(value);

        changeFieldValue('family_has_children', valueClicked);

        this.setState(({ hasErrors }) => ({
            showErrors: valueClicked,
            hasErrors: valueClicked === false ? valueClicked : hasErrors,
        }));
    }

    handleRedirect() {
        const {
            handlePatchPlan,
            formValue,
        } = this.props;

        const data = {
            family_has_children: get(formValue, 'values.family_has_children', null),
            selector: get(formValue, 'values.planId', null),
            isLocalRequest: true,
        };

        handlePatchPlan(data);
    }

    renderChildrenBlock() {
        const { formValue: { values } } = this.props;
        const isHaveChildren = get(values, 'family_has_children', null);

        if (isHaveChildren === true) {
            return (
                <Fragment>
                    <div className="hr span-8" />
                    {this.renderChildren()}
                    <AddDashedButton
                        name="Children"
                        addName="+ Add children"
                        handleRedirect={this.handleRedirect}
                    />
                </Fragment>
            );
        }

        return null;
    }

    renderChildren() {
        const {
            children,
        } = this.props;

        return (
            <div>
                {children.map(child => (
                    <FinanceAboutTable key={child.profile_uuid} data={this.getDataFromChild(child)} section="work" />
                ))}
            </div>
        );
    }

    renderErrorMessage() {
        const { hasErrors, showErrors } = this.state;

        if (hasErrors && showErrors) {
            return <ErrorMessage message="Please add a child first" />;
        }

        return null;
    }

    render() {
        const {
            formValue,
            isSubmitted,
            handleBack,
        } = this.props;

        const { hasErrors, submittedLocally } = this.state;

        const isAnySyncErrors = isEmpty(get(formValue, 'syncErrors', {}));
        const familyHasChildren = get(formValue, 'values.family_has_children', null);

        return (
            <Fragment>
                <FulfilledLine loadingPercent={30} lineColor="#6E0A7B" />
                <section className="container dis-f">
                    <div className="col-span-8">
                        <h2 className="fw-b mt-xlarge mb-mlarge">
                            Tell us about the people who matter to you
                        </h2>
                        <form className="know-you-form col span-8 dis-f fd-c">
                            <Field
                                component={YesNoManual}
                                name="family_has_children"
                                label="Do you have any children?"
                                yesChecked={familyHasChildren === true}
                                noChecked={familyHasChildren === false}
                                handleClick={this.handleClick}
                                isShowErrors={isSubmitted}
                                normalize={normalizeBoolean}
                                validate={[
                                    booleanRequired,
                                ]}
                            />
                            {this.renderChildrenBlock()}
                            {this.renderErrorMessage()}
                        </form>
                    </div>
                    <div className="col span-3 col-last explain_text mt-4rem">
                        <ModalInformationIcon
                            title="Avoiding the parent tax trap"
                            // eslint-disable-next-line max-len
                            text="Be wary of setting up general savings and investment accounts under your child’s name. Any income they recieve over £100 may be taxed at your rate.
There are however a number of tax efficient ways to save for your children which your adviser will be able to talk you through in your first meeting."
                        />
                    </div>
                </section>
                <BackNextButtons
                    isActiveBack={false}
                    onClickNext={this.handleSubmit}
                    isActiveNext={((!isSubmitted || (isAnySyncErrors && isSubmitted)) && (!submittedLocally || (submittedLocally && !hasErrors)))}
                    onClickBack={handleBack}
                    buttonClass="btn-purple"
                />
            </Fragment>
        );
    }
}

AboutChildren.propTypes = {
    formValue: PropTypes.object,
    children: PropTypes.arrayOf(PropTypes.object),
    handleSubmit: PropTypes.func.isRequired,
    handleBack: PropTypes.func.isRequired,
    isSubmitted: PropTypes.bool.isRequired,
    handleGetMePlan: PropTypes.func.isRequired,
    changeFieldValue: PropTypes.func.isRequired,
    handlePatchPlan: PropTypes.func.isRequired,
    handleRequestsReset: PropTypes.func.isRequired,
    isLocalRequestsDone: PropTypes.number.isRequired,
    history: PropTypes.object.isRequired,
};

AboutChildren.defaultProps = {
    formValue: {},
    children: [],
};

const mapDispatchToProps = dispatch => ({
    changeFieldValue: (field, value) => {
        dispatch(change(AboutChildren.formName, field, value));
    },
    handlePatchPlan: bindActionCreators(patchPlan, dispatch),
    handleRequestsReset: bindActionCreators(requestsReset, dispatch),
});

const mapStateToProps = (state) => {
    const { values } = get(state, `form.${AboutChildren.formName}`, {});

    return ({
        children: get(state, 'plan.users.child', []),
        initialValues: {
            // eslint-disable-next-line max-len
            family_has_children: isEmpty(get(state, 'plan.users.child', null)) ? get(state, 'plan.family_has_children', null) : true,
            updatePlan: true,
            planId: get(state, 'plan.plan_uuid', null),
            nextStage: 'about-dependents',
            // eslint-disable-next-line max-len
            prevStage: !(get(values, 'marital_status', null) === 'widowed' || (get(values, 'marital_status', null) === 'divorced')) ? 'about-partner' : 'know-you',
        },
        isLocalRequestsDone: get(state, 'common.isLocalRequestsDone', 0),
    });
};

const AboutChildrenWithHoc = SurveyHoc(AboutChildren, AboutChildren.surveyData);

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(AboutChildrenWithHoc));
