import React, { Component, Fragment } from 'react';
import { change, Field } from 'redux-form';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import get from 'lodash/get';
import PropTypes from 'prop-types';
import isEmpty from 'lodash/isEmpty';
import moment from 'moment';

import { LoadableDateYearPicker } from '../../../../components/Loadable';
import MoneyBlock from '../../../../components/MoneyBlock';
import InputCustom from '../../../../components/InputCustom';
import CheckboxManual from '../../../../components/CheckboxManual';
import Select from '../../../../components/Select';
import { BackNextButtons } from '../../../../components/BackNextButtons';
import { TitleHeader } from '../../../../components/TitleHeader';
import { required } from '../../../../utils/validation.helper';
import ComplexSurveyHoc from '../../../hoc/ComplexSurveyHoc';
import {
    normalizeTimeValue, normalizeInverse, findUserByID, findCashflowById, normalizeBoolean,
} from '../../../../utils/helper';
import { createPlanUser, addDependentsCashflow, updateCashflow } from '../../../../actions/PlanActions';
import AnythingElse from '../../../../components/AnythingElse';
import { updateUserInPlan } from '../../../../actions/UserActions';

/**
 * Render the content for user idle. Dumb component
 *
 */
class AddDependents extends Component {
    static formName = 'addDependents';

    static surveyData = {
        formName: AddDependents.formName,
        nextStage: 'nextStage',
        prevStage: 'prevStage',
        data: [
            {
                fields: [
                    'first_name',
                    'last_name',
                    'dob',
                    'additional_info',
                    'plan_user_type',
                    'plan_user_type_other',
                    'family_has_dependents',
                ],
                additionalCreateFields: [
                    {
                        is_active: false,
                    },
                ],
                additionalUpdateFields: [
                    {
                        is_active: false,
                    },
                ],
                isSend: 'addDependents',
                isUpdate: 'updateDependents',
                selector: 'dependantsId',
                methodCreate: createPlanUser,
                methodUpdate: updateUserInPlan,
            },
        ],
        nextData: [
            {
                fields: [
                    'amount',
                    'frequency',
                    'duration',
                    'value_type',
                    'user_сashflow_type',
                    'is_indeterminate',
                ],
                additionalUpdateFields: [
                    {
                        is_active: false,
                    },
                ],
                isSend: 'addDependentsCashflow',
                isUpdate: 'updateDependentsCashflow',
                selector: 'dependentsCashflowId',
                methodCreate: addDependentsCashflow,
                methodUpdate: updateCashflow,
            },
        ],
    };

    constructor(props) {
        super(props);

        this.renderOtherDependency = this.renderOtherDependency.bind(this);
        this.setDuration = this.setDuration.bind(this);

        const {
            handleGetMePlan,
        } = this.props;

        handleGetMePlan();
    }

    setDuration(event) {
        event.stopPropagation();

        const { target: { value } } = event;
        const { changeFieldValue, formValue: { values } } = this.props;

        if (normalizeBoolean(value)) {
            changeFieldValue('duration', null);
        }

        changeFieldValue('is_indeterminate', !get(values, 'is_indeterminate', null));
    }

    renderOtherDependency() {
        const { formValue, isSubmitted } = this.props;
        const dependencyRelationship = get(formValue, 'values.plan_user_type', null);
        if (dependencyRelationship === 'other') {
            return (
                <Field
                    component={InputCustom}
                    type="text"
                    name="plan_user_type_other"
                    placeholder="Type the relationship to you"
                    extraClasses="col-last span-4"
                    isShowErrors={isSubmitted}
                    label=" "
                    validate={[
                        required,
                    ]}
                />);
        }
        return null;
    }

    render() {
        const {
            formValue,
            isSubmitted,
            handleSubmit,
            handleBack,
            relationshipToClient,
            editMode,
        } = this.props;

        const isAnySyncErrors = isEmpty(get(formValue, 'syncErrors', {}));
        const isIndeterminatePeriod = get(formValue, 'values.is_indeterminate', null);

        return (
            <Fragment>
                <TitleHeader title={editMode ? 'Edit other dependant' : 'Add other dependant'} />
                <div className="container dis-f fd-c mt-5rem">
                    <form className="know-you-form col span-8 dis-f fd-c mt-xlarge">
                        <div className="dis-f col-last span-8">
                            <Field
                                component={Select}
                                extraClasses="span-4"
                                name="plan_user_type"
                                label="Relationship to you"
                                placeholder="Select Relationship"
                                options={relationshipToClient}
                                isShowErrors={isSubmitted}
                                validate={[
                                    required,
                                ]}
                            />
                            {this.renderOtherDependency()}
                        </div>
                        <div className="form-inline dis-f span-8">
                            <div className="input-wrap col span-4">
                                <Field
                                    component={InputCustom}
                                    label="First Name"
                                    type="text"
                                    name="first_name"
                                    placeholder="First name"
                                    isShowErrors={isSubmitted}
                                    validate={[
                                        required,
                                    ]}
                                />
                            </div>
                            <div className="input-wrap col-last span-4">
                                <Field
                                    component={InputCustom}
                                    label="Last Name"
                                    name="last_name"
                                    type="text"
                                    placeholder="Last name"
                                    isShowErrors={isSubmitted}
                                    validate={[
                                        required,
                                    ]}
                                />
                            </div>
                        </div>
                        <Field
                            component={LoadableDateYearPicker}
                            formName={AddDependents.formName}
                            label="Date of birth (optional)"
                            name="dob"
                            isShowErrors={isSubmitted}
                            normalize={normalizeTimeValue}
                        />
                        <div className="hr span-8" />
                        <div className="input-label">
                            Dependant’s costs
                        </div>
                        <MoneyBlock
                            label="Current costs"
                            name="frequency"
                            poundFieldName="amount"
                            selectFieldName="frequency"
                            hintMessage="Enter 0 if less than a year"
                            extraClasses="mt-2rem"
                            isShowErrors={isSubmitted}
                        />
                        <Field
                            component={InputCustom}
                            name="duration"
                            label="How many years will this person be dependent?"
                            placeholder="Years"
                            extraClasses="col span-3 pb-small"
                            disabled={isIndeterminatePeriod}
                            hintMessage="Enter 0 if less than a year"
                            isShowErrors={isSubmitted}
                            validate={
                                !isIndeterminatePeriod ? [required] : null
                            }
                        />
                        <Field
                            component={CheckboxManual}
                            extraClasses="form-inline"
                            name="is_indeterminate"
                            label="I'm not sure"
                            normalize={normalizeInverse}
                            isShowErrors={isSubmitted}
                            checked={isIndeterminatePeriod === true}
                            onClick={this.setDuration}
                            fieldValue={!isIndeterminatePeriod}
                        />
                        <AnythingElse
                            name="additional_info"
                            open
                            isShowErrors={isSubmitted}
                            placeholder="E.g. any expected increases in support"
                        />
                    </form>
                </div>
                <BackNextButtons
                    isActiveBack={false}
                    backText="Cancel"
                    nextText="Save"
                    buttonClass="btn-purple"
                    onClickNext={handleSubmit}
                    isActiveNext={!isSubmitted || (isAnySyncErrors && isSubmitted)}
                    onClickBack={handleBack}
                />
            </Fragment>
        );
    }
}

AddDependents.propTypes = {
    formValue: PropTypes.object,
    handleSubmit: PropTypes.func.isRequired,
    handleBack: PropTypes.func.isRequired,
    changeFieldValue: PropTypes.func.isRequired,
    isSubmitted: PropTypes.bool.isRequired,
    handleGetMePlan: PropTypes.func.isRequired,
    relationshipToClient: PropTypes.arrayOf(PropTypes.object),
    editMode: PropTypes.bool,
};

AddDependents.defaultProps = {
    formValue: {},
    relationshipToClient: [],
    editMode: false,
};

const mapStateToProps = (state, { match: { params } }) => {
    const cashflow = findCashflowById(state, 'plan.cashflows', params.dependants_uuid);
    const dependent = findUserByID(state, 'plan.users.dependent', params.dependants_uuid);

    return ({
        initialValues: {
            dob: !isEmpty(dependent) ? moment(get(dependent, 'profile.dob', null))
                .format('LL')
                .toString() : null,
            first_name: get(dependent, 'profile.first_name', null),
            last_name: get(dependent, 'profile.last_name', null),
            additional_info: get(dependent, 'additional_info', null),
            plan_user_type: get(dependent, 'plan_user_type', null),
            plan_user_type_other: get(dependent, 'plan_user_type_other', null),
            value_type: 'F',
            user_cashflow_type: 'dependants',
            amount: get(cashflow, 'amount', null),
            duration: get(cashflow, 'duration', null),
            is_indeterminate: get(cashflow, 'is_indeterminate', null),
            frequency: !isEmpty(get(cashflow, 'frequency', null)) ? get(cashflow, 'frequency', null) : 'per_month',
        },
        addDependents: true,
        addDependentsCashflow: true,
        updateDependents: !isEmpty(params.dependants_uuid),
        dependantsId: params.dependants_uuid,
        updateDependentsCashflow: cashflow && !isEmpty(cashflow.plan_cashflow_uuid),
        dependentsCashflowId: cashflow && get(cashflow, 'plan_cashflow_uuid', null),
        relationshipToClient: get(state, 'options.plan_user_types[3].dependent', []),
        editMode: !isEmpty(params.dependants_uuid),
        nextStage: '/survey/family/about-dependents',
        prevStage: '/survey/family/about-dependents',
    });
};

const mapDispatchToProps = dispatch => ({
    changeFieldValue: (field, value) => {
        dispatch(change(AddDependents.formName, field, value));
    },
});

const AddDependentsWithHoc = ComplexSurveyHoc(AddDependents, AddDependents.surveyData);

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(AddDependentsWithHoc));
