import React, { Component, Fragment } from 'react';
import { Field, reduxForm } from 'redux-form';
import PropTypes from 'prop-types';
import get from 'lodash/get';
import isEmpty from 'lodash/isEmpty';
import { withRouter } from 'react-router-dom';
import { connect } from 'react-redux';

import { bindActionCreators } from 'redux';
import { BackNextButtons } from '../../../../components/BackNextButtons';
import FulfilledLine from '../../../../components/FulfilledLine';
import {
    maxValue100,
    minValue10,
    required,
    requiredYesNo,
} from '../../../../utils/validation.helper';
import {
    INPUT_TYPES,
    RETIRE_APPROXIMATE_AGE,
    RETIRE_OPTIONS_PARTNER,
    RETIRE_OPTIONS_PERSON, RETIRE_PARTNER_APPROXIMATE_AGE,
} from '../../../../helpers/constants';
import Select from '../../../../components/Select';
import InputCustom from '../../../../components/InputCustom';
import RadioBlocks from '../../../../components/RadioBlocks';
import { getMePlan, updateUserInPlan } from '../../../../actions/UserActions';
import { requestsReset } from '../../../../actions/CommonActions';
import { getUserRetirementType } from '../../../../utils/helper';

/**
 * Render the content for user idle. Dumb component
 * @return {JSX.Element}
 */
class Retirement extends Component {
    static formName = 'retirement';

    constructor(props) {
        super(props);

        this.renderPersonalRetireBlock = this.renderPersonalRetireBlock.bind(this);
        this.renderPersonalSelectedBlock = this.renderPersonalSelectedBlock.bind(this);
        this.renderPartnerRetireBlock = this.renderPartnerRetireBlock.bind(this);
        this.renderPartnerSelectedBlock = this.renderPartnerSelectedBlock.bind(this);

        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleBack = this.handleBack.bind(this);

        const {
            handleGetMePlan,
        } = this.props;

        this.state = {
            isSubmitted: false,
        };
        handleGetMePlan();
    }

    componentDidUpdate() {
        const {
            isRequestsDone,
            handleRequestsReset,
            mainClientNotRetire,
            partnerClientNotRetire,
            hasPartner,
            history,
            formValue,
        } = this.props;

        const {
            isSubmitted,
        } = this.state;

        let amountOfSavingRequests = 0;

        if (mainClientNotRetire) {
            amountOfSavingRequests += 1;
        }

        if (partnerClientNotRetire && hasPartner) {
            amountOfSavingRequests += 1;
        }

        if (isRequestsDone === amountOfSavingRequests && isSubmitted) {
            handleRequestsReset();
            history.push(get(formValue, 'values.nextStage', null));
        }

        if ((!mainClientNotRetire && !hasPartner) || (hasPartner && !partnerClientNotRetire && !mainClientNotRetire)) {
            history.push('../dashboard');
        }
    }

    renderPersonalSelectedBlock = (selectedOption) => {
        const { isSubmitted } = this.state;
        if (selectedOption === 'exact-age') {
            return (
                <Field
                    component={InputCustom}
                    name="retirement_age_main"
                    type={INPUT_TYPES.NUMBER}
                    label="At what age do you want to retire?"
                    placeholder="Age"
                    isShowErrors={isSubmitted}
                    validate={[
                        required, minValue10, maxValue100,
                    ]}
                />
            );
        }
        if (selectedOption === 'approximate-age') {
            return (
                <Field
                    component={Select}
                    name="retirement_age_main"
                    options={RETIRE_APPROXIMATE_AGE}
                    label="Please select a range"
                    placeholder="Select range"
                    isShowErrors={isSubmitted}
                    validate={[
                        required,
                    ]}
                />
            );
        }
        return null;
    };

    renderPartnerSelectedBlock = (selectedOption) => {
        const { isSubmitted } = this.state;
        if (selectedOption === 'exact-age') {
            return (
                <Field
                    component={InputCustom}
                    name="retirement_age_partner"
                    type={INPUT_TYPES.NUMBER}
                    label="At what age do you want to retire?"
                    placeholder="Age"
                    isShowErrors={isSubmitted}
                    validate={[
                        required, minValue10, maxValue100,
                    ]}
                />
            );
        }
        if (selectedOption === 'approximate-age') {
            return (
                <Field
                    component={Select}
                    name="retirement_age_partner"
                    options={RETIRE_PARTNER_APPROXIMATE_AGE}
                    label="Please select a range"
                    placeholder="Select range"
                    isShowErrors={isSubmitted}
                    validate={[
                        required,
                    ]}
                />
            );
        }
        return null;
    };

    handleSubmit = (event) => {
        const {
            formValue,
            handleUpdateUserInPlan,
            mainClientNotRetire,
            partnerClientNotRetire,
            hasPartner,
        } = this.props;
        this.setState({ isSubmitted: true });
        if (!isEmpty(get(formValue, 'syncErrors', {}))) {
            event.preventDefault();
        } else {
            console.log('good submit');
            if (mainClientNotRetire) {
                const body = {};
                const retirementMainUser = get(formValue, 'values.retirement_age_main_type', null);
                if (retirementMainUser === 'do-not-have-plan') {
                    body.is_never_want_to_retire = true;
                    body.retirement_age = null;
                } else {
                    body.retirement_age = get(formValue, 'values.retirement_age_main', null);
                    body.is_never_want_to_retire = null;
                }
                body.selector = get(formValue, 'values.mainClientId', null);
                handleUpdateUserInPlan(body);
            }
            if (partnerClientNotRetire && hasPartner) {
                const body = {};
                const retirementMainUser = get(formValue, 'values.retirement_age_partner_type', null);
                if (retirementMainUser === 'do-not-have-plan') {
                    body.is_never_want_to_retire = true;
                    body.retirement_age = null;
                } else {
                    body.retirement_age = get(formValue, 'values.retirement_age_partner', null);
                    body.is_never_want_to_retire = null;
                }
                body.selector = get(formValue, 'values.partnerClientId', null);
                handleUpdateUserInPlan(body);
            }
        }
    };

    handleBack() {
        const {
            formValue,
            history,
        } = this.props;
        history.push(get(formValue, 'values.prevStage', null));
    }

    renderPartnerRetireBlock() {
        const { formValue, partnerName, mainClientNotRetire } = this.props;
        const { isSubmitted } = this.state;
        const selectedOption = get(formValue, 'values.retirement_age_partner_type', null);
        return (
            <Fragment>
                {mainClientNotRetire
                && <div className="hr span-8" />}
                <Field
                    component={RadioBlocks}
                    blocks={RETIRE_OPTIONS_PARTNER}
                    selectedValue={selectedOption}
                    extraClasses="pb-0"
                    name="retirement_age_partner_type"
                    fieldsName="retirement_age_partner_type"
                    // eslint-disable-next-line max-len
                    label={mainClientNotRetire ? `And what about ${partnerName}?` : `When would ${partnerName} like to retire?`}
                    isShowErrors={isSubmitted}
                    validate={[
                        requiredYesNo,
                    ]}
                />
                {this.renderPartnerSelectedBlock(selectedOption)}
            </Fragment>
        );
    }

    renderPersonalRetireBlock() {
        const { isSubmitted } = this.state;
        const { formValue } = this.props;
        const selectedOption = get(formValue, 'values.retirement_age_main_type', null);
        return (
            <Fragment>
                <Field
                    component={RadioBlocks}
                    blocks={RETIRE_OPTIONS_PERSON}
                    selectedValue={selectedOption}
                    extraClasses="pb-0"
                    name="retirement_age_main_type"
                    fieldsName="retirement_age_main_type"
                    label="When would you like to retire?"
                    isShowErrors={isSubmitted}
                    validate={[
                        requiredYesNo,
                    ]}
                />
                {this.renderPersonalSelectedBlock(selectedOption)}
            </Fragment>
        );
    }

    render() {
        const {
            formValue,
            mainClientNotRetire,
            partnerClientNotRetire,
            hasPartner,
        } = this.props;
        const {
            isSubmitted,
        } = this.state;

        const isAnySyncErrors = isEmpty(get(formValue, 'syncErrors', {}));

        return (
            <Fragment>
                <FulfilledLine loadingPercent={70} lineColor="#6E0A7B" />
                <div className="container dis-f fd-c">
                    <h2 className="fw-b mt-xlarge mb-mlarge">
                        Tell us about your retirement
                    </h2>
                    <form
                        onSubmit={this.handleSubmit}
                        className="know-you-form col span-8 dis-f fd-c"
                    >
                        {mainClientNotRetire
                            ? this.renderPersonalRetireBlock()
                            : null
                        }
                        {partnerClientNotRetire && hasPartner
                            ? this.renderPartnerRetireBlock()
                            : null
                        }
                    </form>
                </div>
                <BackNextButtons
                    isActiveBack={false}
                    onClickNext={this.handleSubmit}
                    isActiveNext={!isSubmitted || (isAnySyncErrors && isSubmitted)}
                    onClickBack={this.handleBack}
                    buttonClass="btn-purple"
                />
            </Fragment>
        );
    }
}

Retirement.propTypes = {
    formValue: PropTypes.object,
    handleGetMePlan: PropTypes.func.isRequired,
    handleUpdateUserInPlan: PropTypes.func.isRequired,
    handleRequestsReset: PropTypes.func.isRequired,
    mainClientNotRetire: PropTypes.bool.isRequired,
    partnerClientNotRetire: PropTypes.bool.isRequired,
    hasPartner: PropTypes.bool.isRequired,
    isRequestsDone: PropTypes.number.isRequired,
    history: PropTypes.object.isRequired,
    partnerName: PropTypes.string,
};

Retirement.defaultProps = {
    formValue: {},
    partnerName: '',
};

const mapStateToProps = state => ({
    formValue: get(state, `form.${Retirement.formName}`, {}),
    partnerName: get(state, 'plan.users.partner.profile.first_name', null),
    // eslint-disable-next-line max-len
    mainClientNotRetire: isEmpty(get(state, 'plan.users.main.current_situation_type', null)) || get(state, 'plan.users.main.current_situation_type', null) === 'currently_working' || get(state, 'plan.users.main.current_situation_type', null) === 'semi_retired',
    // eslint-disable-next-line max-len
    partnerClientNotRetire: isEmpty(get(state, 'plan.users.main.current_situation_type', null)) || get(state, 'plan.users.partner.current_situation_type', null) === 'currently_working' || get(state, 'plan.users.partner.current_situation_type', null) === 'semi_retired',
    hasPartner: !isEmpty(get(state, 'plan.users.partner', null)),
    initialValues: {
        nextStage: '../dashboard',
        prevStage: isEmpty(get(state, 'plan.users.partner', null)) ? 'work/main' : 'work/partner',
        retirement_age_main_type: getUserRetirementType(state, 'main'),
        retirement_age_partner_type: getUserRetirementType(state, 'partner'),
        retirement_age_main: get(state, 'plan.users.main.retirement_age', null),
        retirement_age_partner: get(state, 'plan.users.partner.retirement_age', null),
        mainClientId: get(state, 'plan.users.main.profile_uuid', null),
        partnerClientId: get(state, 'plan.users.partner.profile_uuid', null),
    },
    isRequestsDone: get(state, 'common.isRequestsDone', 0),
});

const mapDispatchToProps = dispatch => ({
    handleGetMePlan: bindActionCreators(getMePlan, dispatch),
    handleUpdateUserInPlan: bindActionCreators(updateUserInPlan, dispatch),
    handleRequestsReset: bindActionCreators(requestsReset, dispatch),
});

const initializeForm = reduxForm({
    form: Retirement.formName,
    enableReinitialize: true,
})(Retirement);

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(initializeForm));
