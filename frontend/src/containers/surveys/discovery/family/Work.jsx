import React, { Component, Fragment } from 'react';
import { change, Field } from 'redux-form';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import PropTypes from 'prop-types';
import get from 'lodash/get';
import isEmpty from 'lodash/isEmpty';

import { bindActionCreators } from 'redux';
import SelectManual from '../../../../components/SelectManual';
import { BackNextButtons } from '../../../../components/BackNextButtons';
import FulfilledLine from '../../../../components/FulfilledLine';
import { required } from '../../../../utils/validation.helper';
import { getResultForRetirementPage } from '../../../../utils/helper';
import AddDashedButton from '../../../../components/AddDashedButton';
import FreeAdditionalBox from '../../../../components/FreeAdditionalBox';
import ComplexSurveyHoc from '../../../hoc/ComplexSurveyHoc';
import { updateUserInPlan } from '../../../../actions/UserActions';
import FinanceAboutTable from '../../../../components/FinanceAboutTable';
import ModalInformationIcon from '../../../../components/ModalInformationIcon';
import { requestsReset } from '../../../../actions/CommonActions';
import { ErrorMessage } from '../../../../components/ErrorMessage';

/**
 * Render the content for user idle. Dumb component
 * @return {JSX.Element}
 */
class Work extends Component {
    static formName = 'work';

    static surveyData = {
        formName: Work.formName,
        nextStage: 'nextStage',
        prevStage: 'prevStage',
        data: [
            {
                fields: [
                    'current_situation_type',
                ],
                additionalUpdateFields: [
                    {
                        is_active: true,
                    },
                ],
                isUpdate: 'updateUserInPlan',
                selector: 'userId',
                methodUpdate: updateUserInPlan,
            },
        ],
    };

    constructor(props) {
        super(props);

        this.state = {
            showErrors: false,
            submittedLocally: false,
            hasErrors: false,
        };

        this.renderJobBlock = this.renderJobBlock.bind(this);
        this.renderJobs = this.renderJobs.bind(this);
        this.getDataFromJob = this.getDataFromJob.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.onChangeSelect = this.onChangeSelect.bind(this);
        this.handleRedirect = this.handleRedirect.bind(this);

        const {
            handleGetMePlan,
            handleRequestsReset,
        } = this.props;

        handleGetMePlan();
        handleRequestsReset();
    }

    componentDidUpdate() {
        const {
            isLocalRequestsDone,
            handleRequestsReset,
            history,
            match: {
                params,
            },
        } = this.props;
        if (isLocalRequestsDone === 1) {
            handleRequestsReset();
            history.push(`/survey/family/add-job/${params.user_type}`);
        }
    }

    getDataFromJob = (job) => {
        const {
            match: {
                params,
            },
            employmentTypes,
        } = this.props;
        return [{
            icon: 'user.svg',
            edit_link: `/survey/family/add-job/${params.user_type}/${job.plan_user_job_uuid}`,
            subtitle: employmentTypes.find(result => result.code === job.employment_type).name,
            title: job.company,
            hideAccordion: true,
        }];
    };

    handleSubmit = (event) => {
        event.preventDefault();
        const { formValue: { values }, jobs, handleSubmit } = this.props;
        const isHaveWork = get(values, 'current_situation_type', null);

        this.setState(() => ({ submittedLocally: true }));

        if ((isHaveWork === 'currently_working' || isHaveWork === 'semi_retired') && !jobs.length) {
            this.setState(() => ({ hasErrors: true }));
        } else handleSubmit(event);
    };

    onChangeSelect = ({ target: { value } }) => {
        const { changeFieldValue } = this.props;

        changeFieldValue('current_situation_type', value);

        if (value === 'currently_working' || value === 'semi_retired') {
            this.setState(() => ({ showErrors: true }));
        } else {
            this.setState(() => ({ showErrors: false, hasErrors: false }));
        }
    };

    handleRedirect() {
        const {
            handleUpdateUserInPlan,
            formValue,
            userId,
        } = this.props;
        const data = {
            current_situation_type: get(formValue, 'values.current_situation_type', null),
            selector: userId,
            isLocalRequest: true,
        };
        handleUpdateUserInPlan(data);
    }

    renderJobBlock() {
        const {
            formValue,
            isSubmitted,
            match: {
                params,
            },
            partnerFirstName,
        } = this.props;

        const isHaveWork = get(formValue, 'values.current_situation_type', null);

        if (isHaveWork === 'currently_working' || isHaveWork === 'semi_retired') {
            return (
                <Fragment>
                    <div className="hr span-8" />
                    {this.renderJobs()}
                    <AddDashedButton
                        name="Jobs"
                        addName="+ Add a job"
                        handleRedirect={this.handleRedirect}
                    />
                </Fragment>
            );
        } if (isHaveWork === 'not_working') {
            return (
                <Fragment>
                    <div className="hr span-8" />
                    <Field
                        component={FreeAdditionalBox}
                        label={params.user_type === 'main' ? 'What are you up to at the moment? Do you have plans to resume work?' : `What are ${partnerFirstName} up to at the moment? Do ${partnerFirstName} have plans to resume work?`}
                        extraClasses="wealth-textarea"
                        placeholder="Explain briefly..."
                        name="additional-info"
                        id="1"
                        cols={30}
                        rows={10}
                        isShowErrors={isSubmitted}
                    />
                </Fragment>
            );
        }
        return null;
    }

    renderJobs() {
        const {
            jobs,
        } = this.props;

        return (
            <div>
                {jobs.map(job => (
                    <FinanceAboutTable key={job.plan_user_job_uuid} data={this.getDataFromJob(job)} section="work" />
                ))}
            </div>
        );
    }

    renderErrorMessage() {
        const { hasErrors, showErrors } = this.state;

        if (hasErrors && showErrors) {
            return <ErrorMessage message="First you need to add a job" />;
        }

        return null;
    }

    render() {
        const {
            formValue,
            workOptions,
            isSubmitted,
            handleBack,
            match: {
                params,
            },
            partnerFirstName,
        } = this.props;

        const { hasErrors, submittedLocally } = this.state;

        const isAnySyncErrors = isEmpty(get(formValue, 'syncErrors', {}));

        return (
            <Fragment>
                <FulfilledLine loadingPercent={60} lineColor="#6E0A7B" />
                <div className="container dis-f">
                    <div className="col span-8">
                        <h2 className="fw-b mt-xlarge mb-mlarge">
                            {params.user_type === 'partner' ? `Tell us about ${partnerFirstName}'s situation` : 'Tell us about your current situation' }
                        </h2>
                        <form onSubmit={this.handleSubmit} className="know-you-form col span-8 dis-f fd-c">
                            <Field
                                name="current_situation_type"
                                component={SelectManual}
                                // eslint-disable-next-line max-len
                                label={params.user_type === 'partner' ? `Tell us about ${partnerFirstName}'s situation` : 'Are you working at the moment?'}
                                placeholder="Select work situation"
                                options={workOptions}
                                isShowErrors={isSubmitted}
                                handleChange={this.onChangeSelect}
                                validate={[
                                    required,
                                ]}
                            />
                            {this.renderJobBlock()}
                            {this.renderErrorMessage()}
                        </form>
                    </div>
                    <div className="col span-3 col-last explain_text mt-4rem">
                        <ModalInformationIcon
                            title="Reap the dividend"
                            // eslint-disable-next-line max-len
                            text="As a director of your own company perhaps you have already considered taking more of your remuneration as dividends rather than salary, to benefit from the lower rates of tax and national insurance. This needs to be balanced against the limitations that this can place on your pension contributions."
                        />
                    </div>
                </div>
                <BackNextButtons
                    isActiveBack={false}
                    onClickNext={this.handleSubmit}
                    isActiveNext={(!isSubmitted || (isAnySyncErrors && isSubmitted)) && (!submittedLocally || (submittedLocally && !hasErrors))}
                    onClickBack={handleBack}
                    buttonClass="btn-purple"
                />
            </Fragment>
        );
    }
}

Work.propTypes = {
    formValue: PropTypes.object,
    match: PropTypes.object,
    workOptions: PropTypes.arrayOf(PropTypes.object),
    employmentTypes: PropTypes.arrayOf(PropTypes.object),
    handleSubmit: PropTypes.func.isRequired,
    handleBack: PropTypes.func.isRequired,
    isSubmitted: PropTypes.bool.isRequired,
    handleGetMePlan: PropTypes.func.isRequired,
    changeFieldValue: PropTypes.func.isRequired,
    handleUpdateUserInPlan: PropTypes.func.isRequired,
    handleRequestsReset: PropTypes.func.isRequired,
    isLocalRequestsDone: PropTypes.number.isRequired,
    history: PropTypes.object.isRequired,
    jobs: PropTypes.arrayOf(PropTypes.object),
    userId: PropTypes.string,
    partnerFirstName: PropTypes.string,
};

Work.defaultProps = {
    formValue: {},
    match: null,
    workOptions: [],
    employmentTypes: [],
    jobs: [],
    userId: '',
    partnerFirstName: null,
};

const mapStateToProps = (state, { match: { params } }) => {
    const { values } = get(state, `form.${Work.formName}`, {});
    const userType = params.user_type;
    const hasPartner = !isEmpty(get(state, 'plan.users.partner', null));
    const currentSituationType = get(state, `plan.users.${userType}.current_situation_type`, null);
    const mainRetired = (userType === 'main' ?
        get(values, 'values.current_situation_type', null)
        :
        get(state, 'plan.user.main.current_situation_type', null)) === 'retired';
    const partnerRetired = (userType === 'main' ?
        get(state, 'plan.user.partner.current_situation_type', null)
        :
        get(values, 'current_situation_type', null)) === 'retired';

    return ({
        initialValues: {
            current_situation_type: currentSituationType,
        },
        workOptions: get(state, 'options.current_situations', []),
        employmentTypes: get(state, 'options.employment_types', []),
        updateUserInPlan: true,
        userId: get(state, `plan.users.${userType}.profile_uuid`, null),
        nextStage: getResultForRetirementPage(state, userType, mainRetired, partnerRetired, hasPartner),
        prevStage: userType === 'main' ? '../about-dependents' : 'main',
        jobs: get(state, `plan.users.${userType}.jobs`, []),
        partnerFirstName: get(state, 'plan.users.partner.profile.first_name', null),
        isLocalRequestsDone: get(state, 'common.isLocalRequestsDone', 0),
    });
};

const mapDispatchToProps = dispatch => ({
    changeFieldValue: (field, value) => {
        dispatch(change(Work.formName, field, value));
    },
    handleUpdateUserInPlan: bindActionCreators(updateUserInPlan, dispatch),
    handleRequestsReset: bindActionCreators(requestsReset, dispatch),
});

const WorkWithHoc = ComplexSurveyHoc(Work, Work.surveyData);

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(WorkWithHoc));
