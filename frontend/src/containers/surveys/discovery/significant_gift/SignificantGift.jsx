import React, { Component, Fragment } from 'react';
import { Field } from 'redux-form';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import PropTypes from 'prop-types';
import get from 'lodash/get';
import isEmpty from 'lodash/isEmpty';
import { BackNextButtons } from '../../../../components/BackNextButtons';
import FulfilledLine from '../../../../components/FulfilledLine';
import {
    booleanRequired,
} from '../../../../utils/validation.helper';
import SurveyHoc from '../../../hoc/SurveyHoc';
import {
    patchPlan,
} from '../../../../actions/PlanActions';
import YesNo from '../../../../components/YesNo';
import {
    normalizeBoolean,
} from '../../../../utils/helper';

/**
 * Render the content for user idle. Dumb component
 * @return {JSX.Element}
 */

class SignificantGift extends Component {
    static formName = 'significantGift';

    static surveyData = {
        formName: SignificantGift.formName,
        nextStage: 'nextStage',
        prevStage: 'prevStage',
        data: [
            {
                fields: [],
                isUpdate: 'updatePlan',
                selector: 'planId',
                methodUpdate: patchPlan,
            },
        ],
    };

    constructor(props) {
        super(props);

        const {
            handleGetMePlan,
        } = this.props;

        handleGetMePlan();
    }

    render() {
        const {
            formValue,
            handleBack,
            isSubmitted,
        } = this.props;
        const isAnySyncErrors = isEmpty(get(formValue, 'syncErrors', {}));

        return (
            <Fragment>
                <FulfilledLine loadingPercent={10} lineColor="#1ED66C" />
                <section className="container dis-f">
                    <div className="col span-8">
                        <h2 className="fw-b mt-xlarge mb-mlarge">
                            More details about you
                        </h2>
                        <form className="details-form col span-8 dis-f fd-c">
                            <Field
                                component={YesNo}
                                componentClass="span-8"
                                name="non-domicile"
                                label="Are you an UK Non-Domicile?(if are, you'd know"
                                yesChecked={get(formValue, 'values.non-domicile', null) === true}
                                normalize={normalizeBoolean}
                                noChecked={get(formValue, 'values.non-domicile', null) === false}
                                validate={[
                                    booleanRequired,
                                ]}
                            />
                            <Field
                                component={YesNo}
                                componentClass="span-8"
                                name="uk_resident"
                                label="Are you a UK resident?"
                                yesChecked={get(formValue, 'values.uk_resident', null) === true}
                                noChecked={get(formValue, 'values.uk_resident', null) === false}
                                normalize={normalizeBoolean}
                                validate={[
                                    booleanRequired,
                                ]}
                            />
                            <Field
                                component={YesNo}
                                componentClass="span-8"
                                name="pay_other_jurisdiction"
                                label="Do you pay tax in other jurisdiction?"
                                yesChecked={get(formValue, 'values.pay_other_jurisdiction', null) === true}
                                noChecked={get(formValue, 'values.pay_other_jurisdiction', null) === false}
                                normalize={normalizeBoolean}
                                validate={[
                                    booleanRequired,
                                ]}
                            />
                            <div className="hr span-8" />
                            <h2 className="fw-b mb-mlarge">
                                Health
                            </h2>
                            <Field
                                component={YesNo}
                                componentClass="span-8"
                                name="good_health"
                                label="Are you in goob health?"
                                yesChecked={get(formValue, 'values.good_health', null) === true}
                                noChecked={get(formValue, 'values.good_health', null) === false}
                                normalize={normalizeBoolean}
                                validate={[
                                    booleanRequired,
                                ]}
                            />
                            <Field
                                component={YesNo}
                                componentClass="span-8"
                                name="smoking"
                                label="Are you smoked int the last 12 months?"
                                yesChecked={get(formValue, 'values.smoking', null) === true}
                                noChecked={get(formValue, 'values.smoking', null) === false}
                                normalize={normalizeBoolean}
                                validate={[
                                    booleanRequired,
                                ]}
                            />
                            <div className="input-label">
                                SignificantGift of any medical conditions
                            </div>
                            <p>
                                Tell us about major conditions that would affect life assurance and health insurance premiums.
                                It’s crucial to note that just because you suffer from one of the conditions listed bellow, it doesn’t mean you won’t be able to take out life insurance.
                            </p>
                        </form>
                    </div>
                </section>
                <BackNextButtons
                    isActiveBack={false}
                    onClickNext={this.handleSubmit}
                    isActiveNext={!isSubmitted || (isAnySyncErrors && isSubmitted)}
                    onClickBack={handleBack}
                />
            </Fragment>
        );
    }
}

SignificantGift.propTypes = {
    handleBack: PropTypes.func.isRequired,
    isSubmitted: PropTypes.bool.isRequired,
    formValue: PropTypes.object,
    handleGetMePlan: PropTypes.func.isRequired,
};

SignificantGift.defaultProps = {
    formValue: {},
};

const mapStateToProps = () => ({
    initialValues: {},
});

const SignificantGiftWithHoc = SurveyHoc(SignificantGift, SignificantGift.surveyData);

export default withRouter(connect(mapStateToProps)(SignificantGiftWithHoc));
