import React, { Component, Fragment } from 'react';
// import { Field, reduxForm } from 'redux-form';
import get from 'lodash/get';
import isEmpty from 'lodash/isEmpty';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { withRouter } from 'react-router-dom';

import { BackNextButtons } from '../../../../components/BackNextButtons';
import FulfilledLine from '../../../../components/FulfilledLine';

import RiskToleranceQuestion from '../../../../components/RiskToleranceQuestion';
import { RISK_TOLERANCE } from '../../../../helpers/constants';
import SurveyHoc from '../../../hoc/SurveyHoc';
import { addPlanAddressProperty, updatePlanAsset } from '../../../../actions/PlanActions';

/**
 * Render the content for user idle. Dumb component
 *
 */
class AddRiskTolerance extends Component {
    static formName = 'AddRiskTolerance';

    static surveyData = {
        formName: AddRiskTolerance.formName,
        nextStage: 'nextStage',
        prevStage: 'prevStage',
        data: [
            {
                fields: [
                ],
                isUpdate: 'isUpdate',
                methodCreate: addPlanAddressProperty,
                methodUpdate: updatePlanAsset,
            },
        ],
    };

    constructor(props) {
        super(props);

        this.state = {
            isSubmitted: false,
            step: 0,
        };

        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleBack = this.handleBack.bind(this);
        this.renderStepForm = this.renderStepForm.bind(this);
    }

    handleSubmit = (event) => {
        const { formValue } = this.props;
        const { step } = this.state;
        this.setState({ isSubmitted: true });
        if (!isEmpty(get(formValue, 'syncErrors', {}))) {
            event.preventDefault();
        } else {
            console.log('good submit');
            // TODO
            // implement send to the server
            this.setState({ step: step + 1, isSubmitted: false });
            // this.props.history.push('add-insurance');
        }
    };

    handleBack() {
        const { history } = this.props;
        history.push('../dashboard');
    }

    renderStepForm() {
        // const { formValue } = this.props;
        const { isSubmitted, step } = this.state;
        const countStep = Object.keys(RISK_TOLERANCE).length;
        if (step === 0) {
            return (
                <Fragment>
                    <h2 className="fw-b mt-xlarge mb-medium">
                        Finally, we need to measure your tolerance for risk
                    </h2>
                    <p className="mb-medium">
                        Finally, we need to understand how much risk you&apos;d like to take with your investments,
                        and what balance you want to strike between security and maximising returns. This allows us to recommend the most appropriate investments for both your financial needs and risk appetite.
                    </p>
                    <button
                        type="button"
                        className="btn small purple_button"
                        onClick={this.handleSubmit}
                    >
                        Start risk questionnaire
                        <img src="/static/img/arrow-white-right.svg" alt="Start Questionnaire" />
                    </button>
                </Fragment>
            );
        } if (step < countStep) {
            return (
                <div className="mt-large">
                    <RiskToleranceQuestion
                        formName={AddRiskTolerance.formName}
                        step={step}
                        name={`risk_tolerance_step${step}`}
                        extraClass=" span-4 mb-small"
                        isSubmitted={isSubmitted}
                    />
                </div>
            );
        }
        return (
            <Fragment>
                <h2 className="fw-b mt-xlarge mb-medium">
                    Risk tolerance score
                </h2>
                <p className="mb-medium">
                    We’ll discusss your risk tolerance score at the meeting.
                </p>
                <button
                    type="button"
                    className="btn small purple_button"
                    onClick={this.handleBack}
                >
                        Ok, got it
                </button>
            </Fragment>
        );
    }

    render() {
        const { formValue } = this.props;
        const { isSubmitted, step } = this.state;
        const isAnySyncErrors = isEmpty(get(formValue, 'syncErrors', {}));
        const loadPercent = (100 / (Object.keys(RISK_TOLERANCE).length) + 2) * step;
        return (
            <Fragment>
                <FulfilledLine loadingPercent={loadPercent} lineColor="#6E0A7B" />
                <div className="container dis-f mt-xlarge">
                    <div className="col span-8">
                        {this.renderStepForm()}
                    </div>
                    <div className="col span-3 col-last explain_text mt-4rem" />
                </div>
                {step > 0
                && (
                    <BackNextButtons
                        isActiveBack={false}
                        onClickNext={this.handleSubmit}
                        isActiveNext={!isSubmitted || (isAnySyncErrors && isSubmitted)}
                        onClickBack={this.handleBack}
                        extraClass="span-8"
                        buttonClass="btn-purple"
                    />
                )
                }
            </Fragment>
        );
    }
}

AddRiskTolerance.propTypes = {
    formValue: PropTypes.object,
    history: PropTypes.object.isRequired,
};

AddRiskTolerance.defaultProps = {
    formValue: {},
};

const mapStateToProps = state => ({
    formValue: get(state, AddRiskTolerance.formName, {}),
});

const mapDispatchToProps = () => ({});

const AddRiskToleranceWithHoc = SurveyHoc(AddRiskTolerance, AddRiskTolerance.surveyData);

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(AddRiskToleranceWithHoc));
