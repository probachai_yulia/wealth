/* eslint-disable max-len */
import React, { Component, Fragment } from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import PropTypes from 'prop-types';

import get from 'lodash/get';
import { bindActionCreators } from 'redux';
import BoxImgText from '../../../../components/BoxImgText';
import { BackNextButtons } from '../../../../components/BackNextButtons';
import FulfilledLine from '../../../../components/FulfilledLine';
import { getMePlan } from '../../../../actions/UserActions';
import ModalInformationIcon from '../../../../components/ModalInformationIcon';
import { SECTION_DEBTS, SECTION_FINANCE } from '../../../../helpers/constants';

/**
 * Render the content for user idle. Dumb component
 *
 */
class AssetsDebts extends Component {
    constructor(props) {
        super(props);

        this.handleBack = this.handleBack.bind(this);
        this.handleNext = this.handleNext.bind(this);
        this.getDoneSection = this.getDoneSection.bind(this);

        const {
            handleGetMePlan,
        } = this.props;
        handleGetMePlan();
    }

    getDoneSection(task) {
        const {
            debts,
        } = this.props;
        let num = 0;
        if (debts.length > 0) {
            const mortgages = debts.filter(item => item.plan_debt_type.indexOf('mortgage') !== -1);
            const otherLoans = debts.filter(item => item.plan_debt_type.indexOf('loan') !== -1);
            const creditCard = debts.filter(item => item.plan_debt_type === 'credit_card');
            switch (task) {
                case 'mortgages':
                    num = mortgages.length;
                    break;
                case 'other_loans':
                    num = otherLoans.length;
                    break;
                case 'credit_card':
                    num = creditCard.length;
                    break;
                default:
                    return null;
            }
        }
        return num;
    }

    handleNext() {
        const { history } = this.props;
        const self = this;
        SECTION_FINANCE.map((item) => {
            if (self.getDoneSection(item.section) === 0) {
                history.push(item.link);
                return false;
            }
            return null;
        });
        history.push('/survey/dashboard');
    }

    handleBack() {
        const { history } = this.props;
        history.push('../dashboard');
    }

    render() {
        const {
            partner,
        } = this.props;
        const introText = (typeof partner !== 'undefined' && partner !== null) ? `you or ${partner.profile.first_name}` : 'you';
        return (
            <Fragment>
                <FulfilledLine loadingPercent={20} lineColor="#6E0A7B" />
                <section className="container dis-f mt-5rem mb-15rem">
                    <div className="col span-8">
                        <h2 className="fw-b mt-xlarge mb-medium">
                            Debts and liabilities
                        </h2>
                        <p className="mb-large">
                            Just as important as understanding your income, is capturing any outstanding debts. Please list any debt that either
                            {' '}
                            {introText}
                            {' '}
hold, including any debt held jointly.
                        </p>
                        {
                            SECTION_DEBTS.map((item, i) => (
                                <Fragment key={i.toString()}>
                                    <BoxImgText
                                        key={item.id}
                                        btnLink={this.getDoneSection(item.section) > 0 ? item.edit_link : item.link}
                                        btnText={item.button_text}
                                        done={this.getDoneSection(item.section) > 0}
                                        taskCompleted={this.getDoneSection(item.section)}
                                        icon={item.icon}
                                        subtitle={item.description}
                                        title={item.title}
                                    />
                                    {item.id < SECTION_DEBTS.length - 1
                                    && <div className="hr_nm span-8 mt-small mb-small" />
                                    }
                                </Fragment>
                            ))
                        }
                    </div>
                    <div className="col span-3 col-last explain_text mt-4rem">
                        {
                            partner !== null
                                ? (
                                    <ModalInformationIcon
                                        title=""
                                        text={`Just as important as understanding your income, is capturing any outstanding debts. Please list any debt that [either] you or ${partner.profile.first_name} hold, [including any debt held jointly.]`}
                                    />
                                )
                                : (
                                    <ModalInformationIcon
                                        title=""
                                        text="Just as important as understanding your income, is capturing any outstanding debts. Please list any debt that you hold."
                                    />
                                )}
                    </div>
                </section>
                <BackNextButtons
                    isActiveBack={false}
                    onClickNext={this.handleNext}
                    onClickBack={this.handleBack}
                    buttonClass="btn-purple"
                    extraClass="span-8"
                />
            </Fragment>
        );
    }
}

AssetsDebts.propTypes = {
    history: PropTypes.object.isRequired,
    partner: PropTypes.object,
    handleGetMePlan: PropTypes.func,
    debts: PropTypes.arrayOf(PropTypes.object),
};

AssetsDebts.defaultProps = {
    handleGetMePlan: () => {},
    partner: null,
    debts: [],
};

const mapStateToProps = state => ({
    initialValues: {
    },
    users: get(state, 'plan.users', {}),
    partner: get(state, 'plan.users.partner', null),
    debts: get(state, 'plan.debts', []),
    cashflows: get(state, 'plan.cashflows', []),
    addresses: get(state, 'plan.addresses', []),
});

const mapDispatchToProps = dispatch => ({
    handleGetMePlan: bindActionCreators(getMePlan, dispatch),
});

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(AssetsDebts));
