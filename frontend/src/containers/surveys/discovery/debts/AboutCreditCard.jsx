import React, { Component, Fragment } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { withRouter } from 'react-router-dom';

import get from 'lodash/get';
import { change } from 'redux-form';
import { bindActionCreators } from 'redux';
import { BackNextButtons } from '../../../../components/BackNextButtons';
import FulfilledLine from '../../../../components/FulfilledLine';

import FinanceAboutTable from '../../../../components/FinanceAboutTable';
import { AddDashedButtonLink } from '../../../../components/AddDashedButtonLink';
import { getMePlan } from '../../../../actions/UserActions';

/**
 * Render the content for user idle. Dumb component
 *
 */
class AboutCreditCard extends Component {
    static formName = 'aboutCreditCard';

    constructor(props) {
        super(props);

        this.getListSavings = this.getListSavings.bind(this);
        this.handleBack = this.handleBack.bind(this);
        this.handleNext = this.handleNext.bind(this);

        const {
            handleGetMePlan,
        } = this.props;
        handleGetMePlan();
    }

    getListSavings = () => {
        const {
            debts, users,
        } = this.props;
        const arrayList = [];
        debts.map((item) => {
            let user = {
                profile: { display_name: '' },
            };
            let imgUser = 'user.svg';
            Object.keys(users).map((userItem) => {
                // console.log(users[userItem], item.owners);
                if ((userItem === 'main' || userItem === 'partner') && users[userItem] !== null) {
                    if (item.owners.length > 1) {
                        user = { profile: { first_name: 'Joint' } };
                        imgUser = 'joint.svg';
                    } else if (users[userItem].profile_uuid === item.owners[0]) {
                        user = users[userItem];
                    }
                }
                if (typeof users[userItem][0] !== 'undefined') {
                    users[userItem].map((subitem) => {
                        if (subitem.profile_uuid === item.owners[0]) {
                            user = subitem;
                        }
                        return null;
                    });
                }
                return null;
            });
            if (item.plan_debt_type === 'credit_card' && item.outstanding_amount) {
                arrayList.push({
                    extra_information: item.additional,
                    interest_rate: [`${item.interest_rate}%`],
                    clientName: user.profile.first_name,
                    client_image: imgUser,
                    edit_link: `/survey/debts/add-credit-card/${item.plan_debt_uuid}`,
                    icon: 'credit_card.svg',
                    subtitle: 'Credit card',
                    title: item.provider_name,
                    outstanding: `£${item.outstanding_amount.toLocaleString('en')}`,
                });
            } else if (item.plan_debt_type === 'credit_card') {
                arrayList.push({
                    extra_information: item.additional,
                    clientName: user.profile.first_name,
                    client_image: imgUser,
                    edit_link: `/survey/debts/add-credit-card/${item.plan_debt_uuid}`,
                    icon: 'credit_card.svg',
                    subtitle: 'Credit card',
                    title: item.provider_name,
                    textResume: 'Paid in full each month',
                    hideAccordion: true,
                });
            }
            return null;
        });
        return arrayList;
    };

    handleBack() {
        const { history } = this.props;
        history.push('assets');
    }

    handleNext() {
        const { history } = this.props;
        history.push('assets');
    }

    render() {
        return (
            <Fragment>
                <FulfilledLine loadingPercent={40} lineColor="#6E0A7B" />
                <div className="container dis-f mt-5rem">
                    <div className="col span-8">
                        <h2 className="fw-b mt-xlarge mb-mlarge">
                            Credit card debt
                        </h2>
                        <h5 className="mb-2rem">
                            Outstanding credit card
                        </h5>

                        <FinanceAboutTable data={this.getListSavings()} section="credit card" />

                        <AddDashedButtonLink
                            link="/survey/debts/add-credit-card"
                            className="mt-3rem btn_large border_dashed link_purple"
                            addName="+ Add credit card"
                        />

                    </div>
                    <div className="col span-3 col-last explain_text mt-4rem" />
                </div>
                <BackNextButtons
                    isActiveBack={false}
                    onClickNext={this.handleNext}
                    onClickBack={this.handleBack}
                    extraClass="span-8"
                    buttonClass="btn-purple"
                />
            </Fragment>
        );
    }
}

AboutCreditCard.propTypes = {
    history: PropTypes.object.isRequired,
    users: PropTypes.object,
    handleGetMePlan: PropTypes.func.isRequired,
    debts: PropTypes.arrayOf(PropTypes.object),
};

AboutCreditCard.defaultProps = {
    users: {},
    debts: [],
};

const mapStateToProps = state => ({
    initialValues: {
    },
    users: get(state, 'plan.users', {}),
    debts: get(state, 'plan.debts', []),
    cashflows: get(state, 'plan.cashflows', []),
    addresses: get(state, 'plan.addresses', []),
});

const mapDispatchToProps = dispatch => ({
    changeFieldValue: (field, value) => {
        dispatch(change(AboutCreditCard.formName, field, value));
    },
    handleGetMePlan: bindActionCreators(getMePlan, dispatch),
});

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(AboutCreditCard));
