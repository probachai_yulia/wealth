import React, { Component, Fragment } from 'react';
import { change, Field } from 'redux-form';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import get from 'lodash/get';
import PropTypes from 'prop-types';
import isEmpty from 'lodash/isEmpty';
import {
    booleanRequired, isNumber, positiveNumber, required, requiredMoney,
} from '../../../../utils/validation.helper';
import InputCustom from '../../../../components/InputCustom';
import Select from '../../../../components/Select';
import { CancelSaveHeader } from '../../../../components/CancelSaveHeader';
import { InputPound } from '../../../../components/InputPound';
import { InputPercentage } from '../../../../components/InputPercentage';
import AnythingElse from '../../../../components/AnythingElse';
import SurveyHoc from '../../../hoc/SurveyHoc';
import { addPlanDebt, updatePlanDebt } from '../../../../actions/PlanActions';
import YesNo from '../../../../components/YesNo';
import { normalizeBoolean } from '../../../../utils/helper';
import { BackNextButtons } from '../../../../components/BackNextButtons';

/**
 * Render the content for user idle. Dumb component
 *
 */
class AddCreditCard extends Component {
    static formName = 'addCreditCard';

    static surveyData = {
        formName: AddCreditCard.formName,
        nextStage: 'nextStage',
        prevStage: 'prevStage',
        data: [
            {
                fields: [
                    'owners',
                    'plan_debt_type',
                    'provider_name',
                    'outstanding_amount',
                    'interest_rate',
                    'has_monthly_overpayment',
                    'additional',
                ],
                isUpdate: 'update',
                selector: 'plan_debt_uuid',
                // additionalFields: [],
                methodCreate: addPlanDebt,
                methodUpdate: updatePlanDebt,
            },
        ],
    };

    constructor(props) {
        super(props);

        this.optionOwnerAccount = this.optionOwnerAccount.bind(this);
        this.renderOverpayPayments = this.renderOverpayPayments.bind(this);
        this.renderOverPayment = this.renderOverPayment.bind(this);
        const {
            handleGetMePlan,
        } = this.props;

        handleGetMePlan();
    }

    componentWillUpdate() {
        const { formValue, changeFieldValue } = this.props;
        changeFieldValue('owners', [get(formValue, 'values.owner', '')]);
    }

    optionOwnerAccount = () => {
        const ownersList = []; let id = 0;
        const {
            usersOptions,
        } = this.props;
        Object.keys(usersOptions).map((item) => {
            if (usersOptions[item] !== null) {
                if (item === 'main' || item === 'partner') {
                    ownersList.push({
                        id,
                        code: usersOptions[item].profile_uuid,
                        name: usersOptions[item].profile.first_name,
                    });
                    id += 1;
                }
                if (item === 'child') {
                    usersOptions[item].map((subitem) => {
                        ownersList.push({
                            id,
                            code: subitem.profile_uuid,
                            name: subitem.profile.first_name,
                            dob: subitem.profile.dob,
                            role: item,
                        });
                        id += 1;
                        return null;
                    });
                }
            }
            return null;
        });

        return ownersList;
    };

    renderOverpayPayments() {
        const { formValue, isSubmitted } = this.props;
        if (get(formValue, 'values.overpay_repayments', null) === true) {
            return (
                <Field
                    label=""
                    component={InputPound}
                    name="estimated-value"
                    isShowErrors={isSubmitted}
                    extraClasses="col span-4 col-last ml-2rem"
                    validate={[
                        requiredMoney,
                        isNumber,
                        positiveNumber,
                    ]}
                />
            );
        }
        return null;
    }

    renderOverPayment() {
        const { formValue, isSubmitted } = this.props;
        const hasOverpayment = get(formValue, 'values.has_monthly_overpayment', null);
        if (!hasOverpayment) {
            return (
                <Fragment>
                    <div className="input-wrap col span-8">
                        <Field
                            component={InputCustom}
                            name="provider_name"
                            label="Provider name"
                            placeholder="Provider name"
                            extraClasses="col span-4 inline"
                            isShowErrors={isSubmitted}
                            validate={[
                                required,
                            ]}
                        />
                        <Field
                            label="Amount outstanding"
                            component={InputPound}
                            name="outstanding_amount"
                            isShowErrors={isSubmitted}
                            extraClasses="col span-4 col-last"
                            validate={[
                                requiredMoney,
                                isNumber,
                                positiveNumber,
                            ]}
                        />
                    </div>
                    <div className="input-wrap col span-8">
                        <Field
                            label="Interest rate"
                            component={InputPercentage}
                            name="interest_rate"
                            isShowErrors={isSubmitted}
                            extraClasses="col span-4"
                            validate={[
                                requiredMoney,
                                isNumber,
                                positiveNumber,
                            ]}
                        />
                    </div>
                    <AnythingElse open={get(formValue, 'values.additional', null) !== null} name="additional" placeholder="E.g. do you receive cashback or airmiles points with this card?" />
                </Fragment>
            );
        }
        return null;
    }

    render() {
        const {
            isSubmitted,
            handleSubmit,
            handleBack,
            formValue,
        } = this.props;
        const isAnySyncErrors = isEmpty(get(formValue, 'syncErrors', {}));
        return (
            <Fragment>
                <CancelSaveHeader
                    title="Add credit card"
                />
                <div className="mt-fix-action-header container dis-f fd-c">
                    <form className="know-you-form col span-8 dis-f fd-c mt-xlarge">
                        <div className="dis-f col-last span-8">
                            <Field
                                component={Select}
                                extraClasses="span-4"
                                name="owner"
                                label="In whose name is the credit card?"
                                placeholder="Select card owner"
                                options={this.optionOwnerAccount()}
                                isShowErrors={isSubmitted}
                            />
                        </div>
                        <div className="dis-f col-last span-8">
                            <Field
                                component={YesNo}
                                // extraClasses="span-3"
                                // containerClass='span-6'
                                name="has_monthly_overpayment"
                                label="Do you pay off this card in full each month?"
                                yesChecked={get(formValue, 'values.has_monthly_overpayment', null) === true}
                                noChecked={get(formValue, 'values.has_monthly_overpayment', null) === false}
                                isShowErrors={isSubmitted}
                                normalize={normalizeBoolean}
                                validate={[
                                    booleanRequired,
                                ]}
                            />
                        </div>
                        {this.renderOverPayment()}
                    </form>
                </div>
                <BackNextButtons
                    isActiveBack={false}
                    onClickNext={handleSubmit}
                    isActiveNext={!isSubmitted || (isAnySyncErrors && isSubmitted)}
                    onClickBack={handleBack}
                    buttonClass="btn-purple"
                    nextText="Save"
                />
            </Fragment>
        );
    }
}

AddCreditCard.propTypes = {
    changeFieldValue: PropTypes.func.isRequired,
    formValue: PropTypes.object,
    handleSubmit: PropTypes.func.isRequired,
    isSubmitted: PropTypes.bool.isRequired,
    handleBack: PropTypes.func.isRequired,
    usersOptions: PropTypes.object,
    handleGetMePlan: PropTypes.func.isRequired,
};

AddCreditCard.defaultProps = {
    formValue: {},
    usersOptions: {},
};

const mapStateToProps = (state, ownProps) => {
    const editSection = get(state, 'plan.debts', []).find((item) => {
        if (item.plan_debt_uuid === ownProps.match.params.debts_uuid && item.plan_debt_type.indexOf('credit_card') !== -1) {
            return true;
        }
        return null;
    });
    const id = get(state, 'plan.debts', []).indexOf(editSection);
    return ({
        initialValues: {
            additional: get(state, `plan.debts[${id}].additional`, ''),
            plan_debt_type: 'credit_card',
            owner: get(state, `plan.debts[${id}].owners[0]`, ''),
            provider_name: get(state, `plan.debts[${id}].provider_name`, ''),
            interest_rate: get(state, `plan.debts[${id}].interest_rate`, ''),
            outstanding_amount: get(state, `plan.debts[${id}].outstanding_amount`, null),
            has_monthly_overpayment: get(state, `plan.debts[${id}].has_monthly_overpayment`, null),
            update: get(state, `plan.debts[${id}].plan_debt_uuid`, null) !== null,
            plan_debt_uuid: get(state, `plan.debts[${id}].plan_debt_uuid`, ''),
            nextStage: '/survey/debts/about-credit-card',
            prevStage: '/survey/debts/assets',
        },
        usersOptions: get(state, 'plan.users', {}),
        assets: get(state, 'plan.assets', []),
    });
};

const mapDispatchToProps = dispatch => ({
    changeFieldValue: (field, value) => {
        dispatch(change(AddCreditCard.formName, field, value));
    },
});

const AddCreditCardWithHoc = SurveyHoc(AddCreditCard, AddCreditCard.surveyData);

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(AddCreditCardWithHoc));
