/* eslint-disable max-len */
import React, { Component, Fragment } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { withRouter } from 'react-router-dom';

import get from 'lodash/get';
import { change } from 'redux-form';
import { bindActionCreators } from 'redux';
import { BackNextButtons } from '../../../../components/BackNextButtons';
import FulfilledLine from '../../../../components/FulfilledLine';

import { AddDashedButtonLink } from '../../../../components/AddDashedButtonLink';

import FinanceAboutTable from '../../../../components/FinanceAboutTable';
import { getMePlan } from '../../../../actions/UserActions';
import ModalInformationIcon from '../../../../components/ModalInformationIcon';

/**
 * Render the content for user idle. Dumb component
 *
 */
class AboutMortgage extends Component {
    static formName = 'addMortgage';

    constructor(props) {
        super(props);

        this.getListSavings = this.getListSavings.bind(this);
        this.handleBack = this.handleBack.bind(this);
        this.handleNext = this.handleNext.bind(this);

        const {
            handleGetMePlan,
        } = this.props;

        handleGetMePlan();
    }

    getListSavings = () => {
        const {
            debts, users, radioButton, rateTypes, addresses,
        } = this.props;
        const arrayList = [];
        let rateType = 'Unknown';
        debts.map((item) => {
            let taxBenefit = '';
            let title;
            let user = {
                profile: { display_name: '' },
            };
            let imgUser = 'user.svg';
            Object.keys(users).map((userItem) => {
                // console.log(users[userItem], item.owners);
                if ((userItem === 'main' || userItem === 'partner') && users[userItem] !== null) {
                    if (item.owners.length > 1) {
                        user = { profile: { first_name: 'Joint' } };
                        imgUser = 'joint.svg';
                    } else if (users[userItem].profile_uuid === item.owners[0]) {
                        user = users[userItem];
                    }
                }
                if (typeof users[userItem][0] !== 'undefined') {
                    users[userItem].map((subitem) => {
                        if (subitem.profile_uuid === item.owners[0]) {
                            user = subitem;
                        }
                        return null;
                    });
                }
                return null;
            });
            radioButton.map((taxBen) => {
                if (taxBen.code === item.plan_debt_type) {
                    taxBenefit = taxBen.name;
                } else if (item.tax_benefit === 'unknown') {
                    taxBenefit = 'Unknown';
                }
                return null;
            });
            rateTypes.map((rate) => {
                if (rate.code === item.rate_type) {
                    rateType = rate.name;
                }
                return null;
            });
            let monthlyPayment = '£0';
            if (typeof item.cashflows[0] !== 'undefined') {
                monthlyPayment = `£${item.cashflows[0].amount}`;
                if (item.has_monthly_overpayment === true) {
                    monthlyPayment = `£${item.cashflows[1].amount} (overpay £${item.cashflows[0].amount}/mo)`;
                }
            }
            if (item.is_self_managed) {
                title = 'Self-managed';
            } else {
                title = item.provider_name;
            }
            const address = addresses.find(subitem => subitem.address_uuid === item.address_uuid);
            let formattedAddress;
            if (typeof address !== 'undefined') {
                formattedAddress = `${address.address_1}, ${address.address_2}, ${address.city}`;
            }
            if (item.plan_debt_type.indexOf('mortgage') !== -1) {
                arrayList.push({
                    extra_information: item.additional,
                    interest_rate: [`${item.interest_rate}%`],
                    years_remaining: [item.outstanding_term],
                    rate_type: [rateType],
                    monthly_repayment: [monthlyPayment],
                    address: [formattedAddress],
                    clientName: user.profile.first_name,
                    client_image: imgUser,
                    edit_link: `/survey/debts/add-mortgage/${item.plan_debt_uuid}`,
                    icon: 'mortgage.svg',
                    subtitle: taxBenefit,
                    mortgage_type: [taxBenefit],
                    title,
                    outstanding: `£${item.outstanding_amount.toLocaleString('en')}`,
                });
            }
            return null;
        });
        return arrayList;
    };

    handleBack() {
        const { history } = this.props;
        history.push('assets');
    }

    handleNext() {
        const { history } = this.props;
        history.push('assets');
    }

    render() {
        return (
            <Fragment>
                <FulfilledLine loadingPercent={40} lineColor="#6E0A7B" />
                <div className="container dis-f mt-5rem">
                    <div className="col span-8">
                        <h2 className="fw-b mt-xlarge mb-mlarge">
                            Mortgages
                        </h2>
                        <h5 className="mb-2rem">
                            Mortgages
                        </h5>

                        <FinanceAboutTable data={this.getListSavings()} section="mortgage" />

                        <AddDashedButtonLink
                            link="/survey/debts/add-mortgage"
                            className="mt-3rem btn_large border_dashed link_purple"
                            addName="+ Add mortgage"
                        />

                    </div>
                    <div className="col span-3 col-last explain_text mt-4rem">
                        <ModalInformationIcon
                            title="Debt: friend or foe?"
                            text="Paying off your mortgage quickly can often be a goal, however in the current low interest rate environment it may be more beneficial to avoid repaying your mortgage early and look to grow your wealth elsewhere."
                        />
                    </div>
                </div>
                <BackNextButtons
                    isActiveBack={false}
                    onClickNext={this.handleNext}
                    onClickBack={this.handleBack}
                    extraClass="span-8"
                    buttonClass="btn-purple"
                />
            </Fragment>
        );
    }
}

AboutMortgage.propTypes = {
    history: PropTypes.object.isRequired,
    users: PropTypes.object,
    handleGetMePlan: PropTypes.func,
    debts: PropTypes.arrayOf(PropTypes.object),
    addresses: PropTypes.arrayOf(PropTypes.object),
    radioButton: PropTypes.arrayOf(PropTypes.object),
    rateTypes: PropTypes.arrayOf(PropTypes.object),
};

AboutMortgage.defaultProps = {
    handleGetMePlan: () => {},
    users: {},
    debts: [],
    addresses: [],
    radioButton: [],
    rateTypes: [],
};

const mapStateToProps = (state) => {
    let radioButton;
    get(state, 'options.plan_debt_types', []).map((item) => {
        if (typeof item.mortgage !== 'undefined') {
            radioButton = item.mortgage;
        }
        return null;
    });
    return ({
        initialValues: {
        },
        users: get(state, 'plan.users', {}),
        debts: get(state, 'plan.debts', []),
        cashflows: get(state, 'plan.cashflows', []),
        addresses: get(state, 'plan.addresses', []),
        radioButton: radioButton || [],
        rateTypes: get(state, 'options.rate_types', []),
    });
};

const mapDispatchToProps = dispatch => ({
    changeFieldValue: (field, value) => {
        dispatch(change(AboutMortgage.formName, field, value));
    },
    handleGetMePlan: bindActionCreators(getMePlan, dispatch),
});

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(AboutMortgage));
