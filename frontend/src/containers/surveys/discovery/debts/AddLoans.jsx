import React, { Component, Fragment } from 'react';
import { change, Field } from 'redux-form';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import get from 'lodash/get';
import PropTypes from 'prop-types';
import isEmpty from 'lodash/isEmpty';
import {
    isNumber, positiveNumber, required, requiredMoney, requiredYesNo,
} from '../../../../utils/validation.helper';
import InputCustom from '../../../../components/InputCustom';
import Select from '../../../../components/Select';
import { CancelSaveHeader } from '../../../../components/CancelSaveHeader';
import { InputPound } from '../../../../components/InputPound';
import { InputPercentage } from '../../../../components/InputPercentage';
import RadioBlocks from '../../../../components/RadioBlocks';
import Radio from '../../../../components/Radio';
import AnythingElse from '../../../../components/AnythingElse';
import SurveyHoc from '../../../hoc/SurveyHoc';
import { addPlanDebt, updatePlanDebt } from '../../../../actions/PlanActions';
import { BackNextButtons } from '../../../../components/BackNextButtons';

/**
 * Render the content for user idle. Dumb component
 *
 */
class AddLoans extends Component {
    static formName = 'addLoans';

    static surveyData = {
        formName: AddLoans.formName,
        nextStage: 'nextStage',
        prevStage: 'prevStage',
        data: [
            {
                fields: [
                    'owners',
                    'plan_debt_type',
                    'name',
                    'provider_name',
                    'outstanding_amount',
                    'interest_rate',
                    'outstanding_term',
                    'outstanding_term_months',
                    'overpayment_amount',
                    'additional',
                    'cashflows_new',
                    'cashflow_update',
                    'cashflows_to_update',
                ],
                isUpdate: 'update',
                selector: 'plan_debt_uuid',
                // additionalFields: [],
                methodCreate: addPlanDebt,
                methodUpdate: updatePlanDebt,
            },
        ],
    };

    constructor(props) {
        super(props);

        this.optionOwnerAccount = this.optionOwnerAccount.bind(this);
        this.renderTypeRadioBtn = this.renderTypeRadioBtn.bind(this);

        const {
            handleGetMePlan,
        } = this.props;

        handleGetMePlan();
    }

    componentWillUpdate() {
        const { formValue, changeFieldValue, usersOptions } = this.props;
        if (get(formValue, 'values.owner', '') === 'joint') {
            changeFieldValue('owners', [usersOptions.main.profile_uuid, usersOptions.partner.profile_uuid]);
            changeFieldValue('is_tenants_in_common', false);
        } else if (get(formValue, 'values.owner', '') === 'tennants') {
            changeFieldValue('is_tenants_in_common', true);
            changeFieldValue('owners', '');
        } else {
            changeFieldValue('owners', [get(formValue, 'values.owner', '')]);
            changeFieldValue('is_tenants_in_common', false);
        }
        const arrayPayment = [];
        if (get(formValue, 'values.update', null) === true) {
            const planId = typeof get(formValue, 'values.cashflow_update', 0)[0] !== 'undefined' ? get(formValue, 'values.cashflow_update', 0)[0].plan_cashflow_uuid : null;
            arrayPayment.push({
                amount: get(formValue, 'values.overpayment_amount', 0),
                frequency: 'per_month',
                value_type: 'F',
                plan_cashflow_id: planId,
            });
            changeFieldValue('cashflows_to_update', arrayPayment);
        } else {
            arrayPayment.push({
                amount: get(formValue, 'values.overpayment_amount', 0),
                user_cashflow_type: 'debt_repayment',
                frequency: 'per_month',
                value_type: 'F',
            });
            changeFieldValue('cashflows_new', arrayPayment);
        }
    }

    optionOwnerAccount() {
        const ownersList = []; let id = 0;
        const {
            usersOptions,
        } = this.props;
        Object.keys(usersOptions).map((item) => {
            if (usersOptions[item] !== null) {
                if (item === 'main') {
                    ownersList.push({
                        id,
                        code: usersOptions[item].profile_uuid,
                        name: usersOptions[item].profile.first_name,
                        dob: usersOptions[item].profile.dob,
                        role: item,
                    });
                    id += 1;
                }
                if (item === 'partner') {
                    ownersList.push({
                        id,
                        code: usersOptions[item].profile_uuid,
                        name: usersOptions[item].profile.first_name,
                        dob: usersOptions[item].profile.dob,
                        role: item,
                    });
                    id += 1;
                    ownersList.push({
                        id: ownersList.length + 1,
                        code: 'joint',
                        name: 'Joint',
                    });
                }
                if (item === 'child') {
                    usersOptions[item].map((subitem) => {
                        ownersList.push({
                            id,
                            code: subitem.profile_uuid,
                            name: subitem.profile.first_name,
                            dob: subitem.profile.dob,
                            role: item,
                        });
                        id += 1;
                        return null;
                    });
                }
            }
            return null;
        });

        return ownersList;
    }

    renderTypeRadioBtn() {
        const { formValue, isSubmitted, radioButton } = this.props;
        const selectedValue = get(formValue, 'values.plan_debt_type', null);
        const valueNotSure = 'Not sure';
        return (
            <Fragment>
                <div className="dis-f span-8 no-padding">
                    <Field
                        component={RadioBlocks}
                        blocks={radioButton}
                        label="Loan type"
                        name="plan_debt_type"
                        fieldsName="plan_debt_type"
                        extraClasses="pb-0"
                        selectedValue={selectedValue}
                        isShowErrors={isSubmitted}
                        validate={[
                            requiredYesNo,
                        ]}
                    />
                </div>
                <Field
                    component={Radio}
                    extraClasses={`span-4 ${selectedValue === valueNotSure ? '' : 'mb-2rem'}`}
                    name="plan_debt_type"
                    fieldValue={valueNotSure}
                    label="I’m not sure"
                    checked={selectedValue === valueNotSure}
                    isShowErrors={isSubmitted}
                    validate={[
                        requiredYesNo,
                    ]}
                />
                {selectedValue === valueNotSure
                && (
                    <p className="mb-2rem mt-0-5rem greyText">
                        No problem, we can work out the finer details later.
                    </p>
                )
                }
            </Fragment>
        );
    }

    render() {
        const {
            isSubmitted,
            handleSubmit,
            handleBack,
            formValue,
        } = this.props;
        const isAnySyncErrors = isEmpty(get(formValue, 'syncErrors', {}));
        return (
            <Fragment>
                <CancelSaveHeader title="Add loan" />
                <div className="mt-fix-action-header container dis-f fd-c mt-xlarge">
                    <form className="know-you-form col span-8 dis-f fd-c mt-xlarge">
                        <div className="dis-f col-last span-8">
                            <Field
                                component={Select}
                                extraClasses="span-4"
                                name="owner"
                                label="In whose name is the loan?"
                                placeholder="Select owner"
                                options={this.optionOwnerAccount()}
                                isShowErrors={isSubmitted}
                            />
                        </div>
                        <div className="input-wrap col span-8">
                            <Field
                                component={InputCustom}
                                name="name"
                                label="Name your loan"
                                placeholder="Name your loan"
                                extraClasses="col span-4 inline"
                                isShowErrors={isSubmitted}
                                validate={[
                                    required,
                                ]}
                            />
                            <Field
                                label="Provider name"
                                component={InputCustom}
                                name="provider_name"
                                placeholder="Provider name"
                                isShowErrors={isSubmitted}
                                extraClasses="col span-4 col-last"
                                validate={[
                                    required,
                                ]}
                            />
                        </div>
                        {this.renderTypeRadioBtn()}
                        <div className="input-wrap col span-8">
                            <Field
                                label="Amount outstanding"
                                component={InputPound}
                                name="outstanding_amount"
                                isShowErrors={isSubmitted}
                                extraClasses="col span-4"
                                validate={[
                                    requiredMoney,
                                    isNumber,
                                    positiveNumber,
                                ]}
                            />
                            <Field
                                label="Interest rate"
                                component={InputPercentage}
                                name="interest_rate"
                                isShowErrors={isSubmitted}
                                extraClasses="col span-4 col-last"
                                validate={[
                                    requiredMoney,
                                    isNumber,
                                    positiveNumber,
                                ]}
                            />
                        </div>
                        <div className="input-wrap col span-8">
                            <Field
                                label="Years remaining"
                                placeholder="Years remaining"
                                component={InputCustom}
                                name="outstanding_term"
                                isShowErrors={isSubmitted}
                                extraClasses="col span-4"
                                validate={[
                                    isNumber,
                                    positiveNumber,
                                ]}
                            />
                            <Field
                                label="Months"
                                placeholder="0"
                                component={InputCustom}
                                name="outstanding_term_months"
                                isShowErrors={isSubmitted}
                                extraClasses="col span-4 col-last"
                                validate={[
                                    isNumber,
                                    positiveNumber,
                                ]}
                            />
                        </div>
                        <div className="dis-f col-last span-8">
                            <Field
                                label="Minimum monthly repayment"
                                component={InputPound}
                                name="overpayment_amount"
                                isShowErrors={isSubmitted}
                                extraClasses="col span-4 col-last"
                                validate={[
                                    requiredMoney,
                                    isNumber,
                                    positiveNumber,
                                ]}
                            />
                        </div>
                        <AnythingElse open={get(formValue, 'values.additional', null) !== null} name="additional" placeholder="E.g. early repayment charges" />
                    </form>
                </div>
                <BackNextButtons
                    isActiveBack={false}
                    onClickNext={handleSubmit}
                    isActiveNext={!isSubmitted || (isAnySyncErrors && isSubmitted)}
                    onClickBack={handleBack}
                    buttonClass="btn-purple"
                    nextText="Save"
                />
            </Fragment>
        );
    }
}

AddLoans.propTypes = {
    changeFieldValue: PropTypes.func.isRequired,
    formValue: PropTypes.object,
    handleSubmit: PropTypes.func.isRequired,
    isSubmitted: PropTypes.bool.isRequired,
    handleBack: PropTypes.func.isRequired,
    usersOptions: PropTypes.object,
    handleGetMePlan: PropTypes.func.isRequired,
    radioButton: PropTypes.arrayOf(PropTypes.object),
};

AddLoans.defaultProps = {
    formValue: {},
    usersOptions: {},
    radioButton: [],
};

const mapStateToProps = (state, ownProps) => {
    const editSection = get(state, 'plan.debts', []).find((item) => {
        if (item.plan_debt_uuid === ownProps.match.params.debts_uuid && item.plan_debt_type.indexOf('loan') !== -1) {
            return true;
        }
        return null;
    });
    let radioButton;
    get(state, 'options.plan_debt_types', []).map((item) => {
        if (typeof item.other_loans !== 'undefined') {
            radioButton = item.other_loans;
        }
        return null;
    });
    const id = get(state, 'plan.debts', []).indexOf(editSection);
    return ({
        initialValues: {
            owner: get(state, `plan.debts[${id}].owners[0]`, ''),
            name: get(state, `plan.debts[${id}].name`, ''),
            provider_name: get(state, `plan.debts[${id}].provider_name`, ''),
            interest_rate: get(state, `plan.debts[${id}].interest_rate`, ''),
            outstanding_term_months: get(state, `plan.debts[${id}].outstanding_term_months`, ''),
            overpayment_amount: get(state, `plan.debts[${id}].overpayment_amount`, ''),
            plan_debt_type: get(state, `plan.debts[${id}].plan_debt_type`, null),
            outstanding_amount: get(state, `plan.debts[${id}].outstanding_amount`, null),
            outstanding_term: get(state, `plan.debts[${id}].outstanding_term`, null),
            update: get(state, `plan.debts[${id}].plan_debt_uuid`, null) !== null,
            plan_debt_uuid: get(state, `plan.debts[${id}].plan_debt_uuid`, ''),
            nextStage: '/survey/debts/about-loans',
            prevStage: '/survey/debts/assets',
        },
        usersOptions: get(state, 'plan.users', {}),
        assets: get(state, 'plan.assets', []),
        radioButton: radioButton || [],
    });
};

const mapDispatchToProps = dispatch => ({
    changeFieldValue: (field, value) => {
        dispatch(change(AddLoans.formName, field, value));
    },
});

const AddLoansWithHoc = SurveyHoc(AddLoans, AddLoans.surveyData);

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(AddLoansWithHoc));
