import React, { Component, Fragment } from 'react';
import { change, Field } from 'redux-form';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import get from 'lodash/get';
import PropTypes from 'prop-types';
import isEmpty from 'lodash/isEmpty';
import {
    isNumber, maxValue100, positiveNumber, required, requiredMoney, requiredYesNo,
} from '../../../../utils/validation.helper';
import InputCustom from '../../../../components/InputCustom';
import Select from '../../../../components/Select';
import { CancelSaveHeader } from '../../../../components/CancelSaveHeader';
import { InputPound } from '../../../../components/InputPound';
import { InputPercentage } from '../../../../components/InputPercentage';
import AnythingElse from '../../../../components/AnythingElse';
import RadioBlocks from '../../../../components/RadioBlocks';
import Radio from '../../../../components/Radio';
import Checkbox from '../../../../components/Checkbox';
import SurveyHoc from '../../../hoc/SurveyHoc';
import { addPlanDebt, updatePlanDebt } from '../../../../actions/PlanActions';
import { normalizeBoolean } from '../../../../utils/helper';
import { BackNextButtons } from '../../../../components/BackNextButtons';

/**
 * Render the content for user idle. Dumb component
 *
 */
class AddMortgage extends Component {
    static formName = 'addMortgage';

    static surveyData = {
        formName: AddMortgage.formName,
        nextStage: 'nextStage',
        prevStage: 'prevStage',
        data: [
            {
                fields: [
                    'owners',
                    'cashflows_new',
                    'plan_debt_type',
                    'additional',
                    'provider_name',
                    'outstanding_amount',
                    'outstanding_term',
                    'rate_type',
                    'interest_rate',
                    'has_monthly_overpayment',
                    'plan_debt_uuid',
                    'address_uuid',
                    'cashflow_update',
                    'cashflows_to_update',
                ],
                isUpdate: 'update',
                selector: 'plan_debt_uuid',
                // additionalFields: [],
                methodCreate: addPlanDebt,
                methodUpdate: updatePlanDebt,
            },
        ],
    };

    constructor(props) {
        super(props);

        this.optionOwnerAccount = this.optionOwnerAccount.bind(this);
        this.renderTypeRadioBtn = this.renderTypeRadioBtn.bind(this);
        this.renderOverpayPayments = this.renderOverpayPayments.bind(this);
        this.addressList = this.addressList.bind(this);
        const {
            handleGetMePlan,
        } = this.props;

        handleGetMePlan();
    }

    componentWillUpdate() {
        const { formValue, changeFieldValue, assets } = this.props;
        if (get(formValue, 'values.update', null) === true) {
            const arrayPayment = [];
            let planId = typeof get(formValue, 'values.cashflow_update', 0)[0] !== 'undefined' ? get(formValue, 'values.cashflow_update', 0)[0].plan_cashflow_uuid : null;
            if (get(formValue, 'values.has_monthly_overpayment', null) === true) {
                arrayPayment.push({
                    amount: get(formValue, 'values.cashflow_overpay_repayments', 0),
                    user_cashflow_type: 'debt_overpayment',
                    frequency: 'per_month',
                    value_type: 'F',
                    plan_cashflow_id: planId,
                });
            }
            planId = typeof get(formValue, 'values.cashflow_update', 0)[0] !== 'undefined' ? get(formValue, 'values.cashflow_update', 0)[0].plan_cashflow_uuid : null;
            const planIdAlt = typeof get(formValue, 'values.cashflow_update', 0)[1] !== 'undefined' ? get(formValue, 'values.cashflow_update', 0)[1].plan_cashflow_uuid : null;
            arrayPayment.push({
                amount: get(formValue, 'values.cashflow_overpayment_amount', 0),
                user_cashflow_type: 'debt_repayment',
                frequency: 'per_month',
                value_type: 'F',
                plan_cashflow_id: get(formValue, 'values.has_monthly_overpayment', null) === true ? planIdAlt : planId,
            });
            changeFieldValue('cashflows_to_update', arrayPayment);
        } else {
            const arrayPayment = [];
            if (get(formValue, 'values.has_monthly_overpayment', null) === true) {
                arrayPayment.push({
                    amount: get(formValue, 'values.cashflow_overpay_repayments', 0),
                    user_cashflow_type: 'debt_overpayment',
                    frequency: 'per_month',
                    value_type: 'F',
                });
            }
            arrayPayment.push({
                amount: get(formValue, 'values.cashflow_overpayment_amount', 0),
                user_cashflow_type: 'debt_repayment',
                frequency: 'per_month',
                value_type: 'F',
            });
            changeFieldValue('cashflows_new', arrayPayment);
        }
        const userInfo = assets.find(item => item.address_uuid === get(formValue, 'values.address_uuid', ''));
        if (typeof userInfo !== 'undefined') {
            changeFieldValue('owners', userInfo.owners);
            changeFieldValue('owner', userInfo.owners[0]);
        }
    }

    optionOwnerAccount() {
        const ownersList = []; let id = 0;
        const {
            usersOptions,
        } = this.props;
        Object.keys(usersOptions).map((item) => {
            if (usersOptions[item] !== null) {
                if (item === 'main' || item === 'partner') {
                    ownersList.push({
                        id,
                        code: usersOptions[item].profile_uuid,
                        name: usersOptions[item].profile.first_name,
                    });
                    id += 1;
                }
                if (item === 'child') {
                    usersOptions[item].map((subitem) => {
                        ownersList.push({
                            id,
                            code: subitem.profile_uuid,
                            name: subitem.profile.first_name,
                            dob: subitem.profile.dob,
                            role: item,
                        });
                        id += 1;
                        return null;
                    });
                }
            }
            return null;
        });
        return ownersList;
    }

    addressList() {
        const { addressList } = this.props;
        const addresses = [];
        if (addressList.length > 0) {
            addressList.map((item, i) => {
                addresses.push({
                    id: i,
                    code: item.address_uuid,
                    name: item.address_1,
                });
                return null;
            });
        }
        return addresses;
    }

    renderTypeRadioBtn() {
        const { formValue, isSubmitted, radioButton } = this.props;
        const selectedValue = get(formValue, 'values.plan_debt_type', null);
        const valueNotSure = 'mortgage_not_sure';
        return (
            <Fragment>
                <div className="dis-f span-8 no-padding">
                    <Field
                        component={RadioBlocks}
                        blocks={radioButton}
                        label="Mortgage type"
                        name="plan_debt_type"
                        fieldsName="plan_debt_type"
                        extraClasses="pb-0"
                        selectedValue={selectedValue}
                        isShowErrors={isSubmitted}
                        validate={[
                            requiredYesNo,
                        ]}
                    />
                </div>
                <Field
                    component={Radio}
                    extraClasses={`span-4 ${selectedValue === valueNotSure ? '' : 'mb-2rem'}`}
                    name="plan_debt_type"
                    fieldValue={valueNotSure}
                    label="I’m not sure"
                    checked={selectedValue === valueNotSure}
                    isShowErrors={isSubmitted}
                    validate={[
                        requiredYesNo,
                    ]}
                />
                {selectedValue === valueNotSure
                && (
                    <p className="mb-2rem greyText">
                        No problem, we can work out the finer details later.
                    </p>
                )
                }
            </Fragment>
        );
    }

    renderOverpayPayments() {
        const { formValue, isSubmitted } = this.props;
        if (get(formValue, 'values.has_monthly_overpayment', null) === true) {
            return (
                <Field
                    label="Monthly repayment"
                    component={InputPound}
                    name="cashflow_overpay_repayments"
                    isShowErrors={isSubmitted}
                    extraClasses="span-4"
                    validate={[
                        requiredMoney,
                        isNumber,
                        positiveNumber,
                    ]}
                />
            );
        }
        return null;
    }

    render() {
        const {
            formValue,
            isSubmitted,
            handleSubmit,
            handleBack,
            rateTypes,
        } = this.props;
        const isAnySyncErrors = isEmpty(get(formValue, 'syncErrors', {}));
        return (
            <Fragment>
                <CancelSaveHeader
                    title="Add mortgage"
                />
                <div className="mt-fix-action-header container dis-f fd-c">
                    <form className="know-you-form col span-8 dis-f fd-c mt-xlarge">
                        <div className="dis-f col-last span-8">
                            <Field
                                component={Select}
                                extraClasses="span-4 col-last"
                                name="address_uuid"
                                label="Which property is this mortgage for?"
                                placeholder="Select property"
                                options={this.addressList()}
                                isShowErrors={isSubmitted}
                                validate={[
                                    required,
                                ]}
                            />
                        </div>
                        {this.renderTypeRadioBtn()}
                        <div className="input-wrap col span-8">
                            <Field
                                component={InputCustom}
                                name="provider_name"
                                label="Provider name"
                                placeholder="Provider name"
                                extraClasses="col span-4 inline"
                                isShowErrors={isSubmitted}
                                validate={[
                                    required,
                                ]}
                            />
                            <Field
                                label="Estimated value"
                                component={InputPound}
                                name="outstanding_amount"
                                isShowErrors={isSubmitted}
                                extraClasses="col span-4 col-last"
                                validate={[
                                    requiredMoney,
                                    isNumber,
                                    positiveNumber,
                                ]}
                            />
                        </div>
                        <div className="input-wrap col span-8">
                            <Field
                                component={InputCustom}
                                name="outstanding_term"
                                label="Years remaning"
                                placeholder="Years"
                                extraClasses="col span-4 inline"
                                isShowErrors={isSubmitted}
                                validate={[
                                    required,
                                    isNumber,
                                    positiveNumber,
                                ]}
                            />
                            <Field
                                label="Interest rate"
                                component={InputPercentage}
                                name="interest_rate"
                                isShowErrors={isSubmitted}
                                extraClasses="col span-4 col-last"
                                validate={[
                                    requiredMoney,
                                    isNumber,
                                    positiveNumber,
                                    maxValue100,
                                ]}
                            />
                        </div>
                        <div className="dis-f col-last span-8">
                            <Field
                                component={RadioBlocks}
                                blocks={rateTypes}
                                label="Rate type"
                                name="rate_type"
                                fieldsName="rate_type"
                                extraClasses="pb-0"
                                selectedValue={get(formValue, 'values.rate_type', null)}
                                isShowErrors={isSubmitted}
                                validate={[
                                    requiredYesNo,
                                ]}
                            />
                        </div>
                        <div className="dis-f col-last span-8">
                            <Field
                                label="Minimum monthly repayment"
                                component={InputPound}
                                name="cashflow_overpayment_amount"
                                isShowErrors={isSubmitted}
                                extraClasses="col span-4 col-last"
                                validate={[
                                    requiredMoney,
                                    isNumber,
                                    positiveNumber,
                                ]}
                            />
                        </div>
                        <div className="dis-f col-last span-8">
                            <Field
                                component={Checkbox}
                                extraClasses="span-4 form-inline"
                                name="has_monthly_overpayment"
                                fieldValue="I overpay repayments each month"
                                label="I overpay repayments each month"
                                checked={get(formValue, 'values.has_monthly_overpayment', null)}
                                isShowErrors={isSubmitted}
                                normalize={normalizeBoolean}
                            />
                        </div>
                        {this.renderOverpayPayments()}
                        <AnythingElse open={get(formValue, 'values.additional', null) !== null} name="additional" placeholder="E.g. early repayment charges" />
                    </form>
                </div>

                <BackNextButtons
                    isActiveBack={false}
                    onClickNext={handleSubmit}
                    isActiveNext={!isSubmitted || (isAnySyncErrors && isSubmitted)}
                    onClickBack={handleBack}
                    buttonClass="btn-purple"
                    nextText="Save"

                />
            </Fragment>
        );
    }
}

AddMortgage.propTypes = {
    changeFieldValue: PropTypes.func.isRequired,
    formValue: PropTypes.object,
    handleSubmit: PropTypes.func.isRequired,
    isSubmitted: PropTypes.bool.isRequired,
    handleBack: PropTypes.func.isRequired,
    usersOptions: PropTypes.object,
    assets: PropTypes.arrayOf(PropTypes.object),
    handleGetMePlan: PropTypes.func.isRequired,
    addressList: PropTypes.arrayOf(PropTypes.object),
    radioButton: PropTypes.arrayOf(PropTypes.object),
    rateTypes: PropTypes.arrayOf(PropTypes.object),
};

AddMortgage.defaultProps = {
    formValue: {},
    usersOptions: {},
    assets: [],
    addressList: [],
    radioButton: [],
    rateTypes: [],
};

const mapStateToProps = (state, ownProps) => {
    const editSection = get(state, 'plan.debts', []).find(item => (item.plan_debt_uuid === ownProps.match.params.debts_uuid && item.plan_debt_type.indexOf('mortgage') !== -1));
    const id = get(state, 'plan.debts', []).indexOf(editSection);
    let cashflowOverpaymentAmount = get(state, `plan.debts[${id}].cashflows[0].amount`, null);
    if (get(state, `plan.debts[${id}].has_monthly_overpayment`, false) === true) {
        cashflowOverpaymentAmount = get(state, `plan.debts[${id}].cashflows[1].amount`, null);
    }
    let radioButton;
    get(state, 'options.plan_debt_types', []).map((item) => {
        if (typeof item.mortgage !== 'undefined') {
            radioButton = item.mortgage;
        }
        return null;
    });
    return ({
        initialValues: {
            address_uuid: get(state, `plan.debts[${id}].address_uuid`, null),
            additional: get(state, `plan.debts[${id}].additional`, null),
            owner: get(state, `plan.debts[${id}].owners[0]`, ''),
            has_monthly_overpayment: get(state, `plan.debts[${id}].has_monthly_overpayment`, false),
            provider_name: get(state, `plan.debts[${id}].provider_name`, ''),
            rate_type: get(state, `plan.debts[${id}].rate_type`, ''),
            cashflow_overpay_repayments: get(state, `plan.debts[${id}].cashflows[0].amount`, null),
            cashflow_overpayment_amount: cashflowOverpaymentAmount,
            interest_rate: get(state, `plan.debts[${id}].interest_rate`, null),
            plan_debt_type: get(state, `plan.debts[${id}].plan_debt_type`, null),
            outstanding_amount: get(state, `plan.debts[${id}].outstanding_amount`, null),
            outstanding_term: get(state, `plan.debts[${id}].outstanding_term`, null),
            plan_debt_uuid: get(state, `plan.debts[${id}].plan_debt_uuid`, '').toString(),
            update: get(state, `plan.debts[${id}].plan_debt_uuid`, null) !== null,
            cashflow_update: get(state, `plan.debts[${id}].cashflows`, null),
            nextStage: '/survey/debts/about-mortgage',
            prevStage: '/survey/debts/assets',
        },
        usersOptions: get(state, 'plan.users', {}),
        assets: get(state, 'plan.assets', []),
        debts: get(state, 'plan.debts', []),
        addressList: get(state, 'plan.addresses', []),
        radioButton: radioButton || [],
        rateTypes: get(state, 'options.rate_types', []),
    });
};

const mapDispatchToProps = dispatch => ({
    changeFieldValue: (field, value) => {
        dispatch(change(AddMortgage.formName, field, value));
    },
});

const AddMortgageWithHoc = SurveyHoc(AddMortgage, AddMortgage.surveyData);

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(AddMortgageWithHoc));
