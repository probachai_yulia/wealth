import React, { Component, Fragment } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { withRouter } from 'react-router-dom';

import get from 'lodash/get';
import { change } from 'redux-form';
import { bindActionCreators } from 'redux';
import { BackNextButtons } from '../../../../components/BackNextButtons';
import FulfilledLine from '../../../../components/FulfilledLine';

import FinanceAboutTable from '../../../../components/FinanceAboutTable';
import { AddDashedButtonLink } from '../../../../components/AddDashedButtonLink';
import { getMePlan } from '../../../../actions/UserActions';

/**
 * Render the content for user idle. Dumb component
 *
 */
class AboutLoans extends Component {
    static formName = 'aboutLoans';

    constructor(props) {
        super(props);

        this.getListSavings = this.getListSavings.bind(this);
        this.handleBack = this.handleBack.bind(this);
        this.handleNext = this.handleNext.bind(this);

        const {
            handleGetMePlan,
        } = this.props;
        handleGetMePlan();
    }

    getListSavings = () => {
        const { debts, users, radioButton } = this.props;
        const arrayList = [];
        debts.map((item) => {
            let taxBenefit = '';
            let user = {
                profile: { display_name: '' },
            };
            let imgUser = 'user.svg';
            Object.keys(users).map((userItem) => {
                // console.log(users[userItem], item.owners);
                if ((userItem === 'main' || userItem === 'partner') && users[userItem] !== null) {
                    if (item.owners.length > 1) {
                        user = { profile: { first_name: 'Joint' } };
                        imgUser = 'joint.svg';
                    } else if (users[userItem].profile_uuid === item.owners[0]) {
                        user = users[userItem];
                    }
                }
                if (typeof users[userItem][0] !== 'undefined') {
                    users[userItem].map((subitem) => {
                        if (subitem.profile_uuid === item.owners[0]) {
                            user = subitem;
                        }
                        return null;
                    });
                }
                return null;
            });
            radioButton.map((taxBen) => {
                if (taxBen.code === item.plan_debt_type) {
                    taxBenefit = taxBen.name;
                } else if (item.tax_benefit === 'unknown') {
                    taxBenefit = 'Unknown';
                }
                return null;
            });

            const yearsRemaining = `${item.outstanding_term} (${item.outstanding_term_months} months)`;

            if (item.plan_debt_type.indexOf('loan') !== -1) {
                arrayList.push({
                    extra_information: item.additional,
                    interest_rate: [`${item.interest_rate}%`],
                    years_remaining: [yearsRemaining],
                    monthly_repayment: [`£${item.overpayment_amount.toLocaleString('en')}`],
                    loan_type: [taxBenefit],
                    clientName: user.profile.first_name,
                    client_image: imgUser,
                    edit_link: `/survey/debts/add-loans/${item.plan_debt_uuid}`,
                    icon: 'mortgage.svg',
                    subtitle: item.provider_name,
                    title: item.name,
                    outstanding: `£${item.outstanding_amount.toLocaleString('en')}`,
                });
            }
            return null;
        });

        return arrayList;
    };

    handleBack() {
        const { history } = this.props;
        history.push('assets');
    }

    handleNext() {
        const { history } = this.props;
        history.push('assets');
    }

    render() {
        return (
            <Fragment>
                <FulfilledLine loadingPercent={40} lineColor="#6E0A7B" />
                <div className="container dis-f mt-5rem">
                    <div className="col span-8">
                        <h2 className="fw-b mt-xlarge mb-mlarge">
                            Other loans
                        </h2>
                        <p className="mb-2rem">
                            This could be anything from a bank loan to money borrowed from friends or family.
                        </p>
                        <h5 className="mb-2rem">
                            Loans
                        </h5>

                        <FinanceAboutTable data={this.getListSavings()} section="cash" />

                        <AddDashedButtonLink
                            link="/survey/debts/add-loans"
                            className="mt-3rem btn_large border_dashed link_purple"
                            addName="+ Add loan"
                        />

                    </div>
                    <div className="col span-3 col-last explain_text mt-4rem" />
                </div>
                <BackNextButtons
                    isActiveBack={false}
                    onClickNext={this.handleNext}
                    onClickBack={this.handleBack}
                    extraClass="span-8"
                    buttonClass="btn-purple"
                />
            </Fragment>
        );
    }
}

AboutLoans.propTypes = {
    debts: PropTypes.arrayOf(PropTypes.object),
    history: PropTypes.object.isRequired,
    users: PropTypes.object,
    handleGetMePlan: PropTypes.func.isRequired,
    radioButton: PropTypes.arrayOf(PropTypes.object),
};

AboutLoans.defaultProps = {
    users: {},
    radioButton: [],
    debts: [],
};

const mapStateToProps = (state) => {
    let radioButton;
    get(state, 'options.plan_debt_types', []).map((item) => {
        if (typeof item.other_loans !== 'undefined') {
            radioButton = item.other_loans;
        }
        return null;
    });
    return ({
        initialValues: {
        },
        users: get(state, 'plan.users', {}),
        debts: get(state, 'plan.debts', []),
        cashflows: get(state, 'plan.cashflows', []),
        addresses: get(state, 'plan.addresses', []),
        radioButton: radioButton || [],
    });
};

const mapDispatchToProps = dispatch => ({
    changeFieldValue: (field, value) => {
        dispatch(change(AboutLoans.formName, field, value));
    },
    handleGetMePlan: bindActionCreators(getMePlan, dispatch),
});

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(AboutLoans));
