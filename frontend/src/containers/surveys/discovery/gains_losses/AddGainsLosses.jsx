import React, { Component, Fragment } from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import PropTypes from 'prop-types';
import get from 'lodash/get';
import isEmpty from 'lodash/isEmpty';
import { BackNextButtons } from '../../../../components/BackNextButtons';
import FulfilledLine from '../../../../components/FulfilledLine';
import {
    patchPlan,
} from '../../../../actions/PlanActions';
import SurveyHoc from '../../../hoc/SurveyHoc';
import MultipleGainsBlock from '../../../../components/MultipleGainsBlock';
import MultipleLossesBlock from '../../../../components/MultipleLossesBlock';
import { OWNERS } from '../../../../helpers/constants';
import { TitleHeader } from '../../../../components/TitleHeader';

/**
 * Render the content for user idle. Dumb component
 * @return {JSX.Element}
 */

class AddGainsLosses extends Component {
    static formName = 'gainsLosses';

    static surveyData = {
        formName: AddGainsLosses.formName,
        nextStage: 'nextStage',
        prevStage: 'prevStage',
        data: [
            {
                fields: [
                    'gains',
                    'losses',
                ],
                isUpdate: 'updatePlan',
                selector: 'planId',
                methodUpdate: patchPlan,
            },
        ],
    };

    render() {
        const {
            formValue,
            handleBack,
            isSubmitted,
            handleSubmit,
        } = this.props;
        const isAnySyncErrors = isEmpty(get(formValue, 'syncErrors', {}));

        return (
            <Fragment>
                <FulfilledLine loadingPercent={10} lineColor="#1ED66C" />
                <TitleHeader title="Add gains & losses" />
                <section className="container add-gains-losses dis-f">
                    <div className="col">
                        <MultipleGainsBlock
                            name="gains"
                            fieldsName="gains"
                            isShowErrors={isSubmitted}
                            extraClasses="mt-2rem"
                            label="+ Add another gain"
                            placeholder="Select owner"
                            buttonLabel="Remove Gain"
                            options={OWNERS}
                            blockLabel="Gains this year"
                            blockLabelExtraClasses="block-label mb-medium"
                        />
                        <div className="hr" />
                        <MultipleLossesBlock
                            name="losses"
                            fieldsName="losses"
                            isShowErrors={isSubmitted}
                            extraClasses="mt-2rem"
                            label="+ Add another loss"
                            placeholderSelect="Select owner"
                            placeholderInput="Year of loss"
                            buttonLabel="Remove Loss"
                            options={OWNERS}
                            blockLabel="Losses over the last 3 years"
                            blockLabelExtraClasses="block-label mb-0-5rem"
                            hintMessage="Please only list unclaimed losses from the last 3 years."
                            hintMessageExtraClasses="hint-block-label mb-large mt-0"
                        />
                    </div>
                </section>
                <BackNextButtons
                    isActiveBack={false}
                    backText="Cancel"
                    nextText="Save"
                    onClickNext={handleSubmit}
                    isActiveNext={!isSubmitted || (isAnySyncErrors && isSubmitted)}
                    onClickBack={handleBack}
                    buttonClass="btn-purple"
                />
            </Fragment>
        );
    }
}

AddGainsLosses.propTypes = {
    handleBack: PropTypes.func.isRequired,
    handleSubmit: PropTypes.func.isRequired,
    isSubmitted: PropTypes.bool.isRequired,
    formValue: PropTypes.object,
};

AddGainsLosses.defaultProps = {
    formValue: {},
};

const mapStateToProps = () => ({
    initialValues: {
        gains: [null],
        losses: [null],
        updatePlan: true,
        planId: null,
        nextStage: './pension-protection',
        prevStage: './gains-losses',
    },
});

const AddGainsLossesWithHoc = SurveyHoc(AddGainsLosses, AddGainsLosses.surveyData);

export default withRouter(connect(mapStateToProps, null)(AddGainsLossesWithHoc));
