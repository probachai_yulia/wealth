import React, { Component, Fragment } from 'react';
import { Field } from 'redux-form';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import PropTypes from 'prop-types';
import get from 'lodash/get';
import isEmpty from 'lodash/isEmpty';
import { BackNextButtons } from '../../../../components/BackNextButtons';
import FulfilledLine from '../../../../components/FulfilledLine';
import {
    booleanRequired,
} from '../../../../utils/validation.helper';
import {
    patchPlan,
} from '../../../../actions/PlanActions';
import YesNo from '../../../../components/YesNo';
import {
    normalizeBoolean,
} from '../../../../utils/helper';
import SurveyHoc from '../../../hoc/SurveyHoc';
import AddDashedButton from '../../../../components/AddDashedButton';
import ModalInformationIcon from '../../../../components/ModalInformationIcon';

/**
 * Render the content for user idle. Dumb component
 * @return {JSX.Element}
 */

class GainsLosses extends Component {
    static formName = 'gainsLosses';

    static surveyData = {
        formName: GainsLosses.formName,
        nextStage: 'nextStage',
        prevStage: 'prevStage',
        data: [
            {
                fields: [],
                isUpdate: 'updatePlan',
                selector: 'planId',
                methodUpdate: patchPlan,
            },
        ],
    };

    constructor(props) {
        super(props);

        this.handleRedirect = this.handleRedirect.bind(this);
        this.renderGainsLossesBlock = this.renderGainsLossesBlock.bind(this);
    }

    handleRedirect() {
        const { history } = this.props;

        history.push('./add-gains-losses');
    }

    renderGainsLossesBlock() {
        const { formValue } = this.props;

        const hasGainsLosses = get(formValue, 'values.has_gains_losses', null);

        if (hasGainsLosses) {
            return (
                <AddDashedButton
                    name="Gains & losses"
                    addName="+ Add gains & losses"
                    handleRedirect={this.handleRedirect}
                />
            );
        }

        return null;
    }

    render() {
        const {
            formValue,
            handleBack,
            isSubmitted,
            handleSubmit,
        } = this.props;
        const isAnySyncErrors = isEmpty(get(formValue, 'syncErrors', {}));

        return (
            <Fragment>
                <FulfilledLine loadingPercent={10} lineColor="#1ED66C" />
                <section className="container dis-f">
                    <div className="col span-8">
                        <h2 className="fw-b mt-xlarge mb-mlarge">
                            Tell us about your gains and losses
                        </h2>
                        <form className="details-form col span-8 dis-f fd-c">
                            <Field
                                component={YesNo}
                                componentClass="span-8"
                                name="has_gains_losses"
                                // eslint-disable-next-line max-len
                                label="Have you [or Client2] realised any gains this year or losses over the last 3 years?"
                                yesChecked={get(formValue, 'values.has_gains_losses', null) === true}
                                normalize={normalizeBoolean}
                                noChecked={get(formValue, 'values.has_gains_losses', null) === false}
                                validate={[
                                    booleanRequired,
                                ]}
                            />
                            {this.renderGainsLossesBlock()}
                        </form>
                    </div>
                    {
                        get(formValue, 'values.has_gains_losses', null) && (
                            <div className="col span-3 col-last explain_text mt-4rem">
                                <ModalInformationIcon
                                    title="Why we’re asking this"
                                    // eslint-disable-next-line max-len
                                    text="By understanding your family circumstances in full, we can build a plan that helps create the best possible future for all of you."
                                />
                            </div>
                        )
                    }
                </section>
                <BackNextButtons
                    isActiveBack={false}
                    onClickNext={handleSubmit}
                    isActiveNext={!isSubmitted || (isAnySyncErrors && isSubmitted)}
                    onClickBack={handleBack}
                />
            </Fragment>
        );
    }
}

GainsLosses.propTypes = {
    handleBack: PropTypes.func.isRequired,
    handleSubmit: PropTypes.func.isRequired,
    isSubmitted: PropTypes.bool.isRequired,
    formValue: PropTypes.object,
    history: PropTypes.object.isRequired,
};

GainsLosses.defaultProps = {
    formValue: {},
};

const mapStateToProps = () => ({
    initialValues: {
        nextStage: './pension-protection',
        prevStage: './valuables',
    },
});

const GainsLossesWithHoc = SurveyHoc(GainsLosses, GainsLosses.surveyData);

export default withRouter(connect(mapStateToProps, null)(GainsLossesWithHoc));
