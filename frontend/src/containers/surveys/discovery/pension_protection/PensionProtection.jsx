import React, { Component, Fragment } from 'react';
import { Field } from 'redux-form';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import PropTypes from 'prop-types';
import get from 'lodash/get';
import isEmpty from 'lodash/isEmpty';
import { BackNextButtons } from '../../../../components/BackNextButtons';
import FulfilledLine from '../../../../components/FulfilledLine';
import {
    booleanRequired,
} from '../../../../utils/validation.helper';
import SurveyHoc from '../../../hoc/SurveyHoc';
import {
    patchPlan,
} from '../../../../actions/PlanActions';
import YesNo from '../../../../components/YesNo';
import {
    normalizeBoolean,
} from '../../../../utils/helper';
import AnythingElse from '../../../../components/AnythingElse';
import ModalInformationIcon from '../../../../components/ModalInformationIcon';

/**
 * Render the content for user idle. Dumb component
 * @return {JSX.Element}
 */

class PensionProtection extends Component {
    static formName = 'pensionProtection';

    static surveyData = {
        formName: PensionProtection.formName,
        nextStage: 'nextStage',
        prevStage: 'prevStage',
        data: [
            {
                fields: [
                    'lifetime_allowance',
                    'partner_lifetime_allowance',
                    'lifetime_allowance_details',
                    'partner_lifetime_allowance_details',
                ],
                isUpdate: 'updatePlan',
                selector: 'planId',
                methodUpdate: patchPlan,
            },
        ],
    };

    constructor(props) {
        super(props);

        this.renderLifetimeAllowanceDetails = this.renderLifetimeAllowanceDetails.bind(this);
        this.renderPartnerLifetimeAllowanceDetails = this.renderPartnerLifetimeAllowanceDetails.bind(this);

        const {
            handleGetMePlan,
        } = this.props;

        handleGetMePlan();
    }

    renderLifetimeAllowanceDetails() {
        const { formValue } = this.props;

        const isLifetimeAllowance = get(formValue, 'values.lifetime_allowance', null);

        if (isLifetimeAllowance) {
            return (
                <AnythingElse
                    open
                    openText="Please provide some details"
                    placeholder="Please provide details about your lifetime allowance protection..."
                    name="lifetime_allowance_details"
                />
            );
        }

        return null;
    }

    renderPartnerLifetimeAllowanceDetails() {
        const { formValue } = this.props;

        const isPartnerLifetimeAllowance = get(formValue, 'values.partner_lifetime_allowance', null);

        if (isPartnerLifetimeAllowance) {
            return (
                <AnythingElse
                    open
                    openText="Please provide some details"
                    placeholder="Please provide details about [Client 2]'s lifetime allowance protection..."
                    name="partner_lifetime_allowance_details"
                />
            );
        }

        return null;
    }

    render() {
        const {
            formValue,
            handleBack,
            isSubmitted,
            handleSubmit,
        } = this.props;
        const isAnySyncErrors = isEmpty(get(formValue, 'syncErrors', {}));

        return (
            <Fragment>
                <FulfilledLine loadingPercent={10} lineColor="#1ED66C" />
                <section className="container dis-f">
                    <div className="col span-8">
                        <h2 className="fw-b mt-xlarge mb-mlarge">
                            Pension protection
                        </h2>
                        <form className="details-form col span-8 dis-f fd-c">
                            <Field
                                component={YesNo}
                                componentClass="span-8"
                                name="lifetime_allowance"
                                label="Do you have any form of lifetime allowance protection?"
                                yesChecked={get(formValue, 'values.lifetime_allowance', null) === true}
                                normalize={normalizeBoolean}
                                noChecked={get(formValue, 'values.lifetime_allowance', null) === false}
                                validate={[
                                    booleanRequired,
                                ]}
                            />
                            {this.renderLifetimeAllowanceDetails()}
                            <Field
                                component={YesNo}
                                componentClass="span-8"
                                name="partner_lifetime_allowance"
                                label="Does [Client2] have any form of lifetime allowance protection?"
                                yesChecked={get(formValue, 'values.partner_lifetime_allowance', null) === true}
                                noChecked={get(formValue, 'values.partner_lifetime_allowance', null) === false}
                                normalize={normalizeBoolean}
                                validate={[
                                    booleanRequired,
                                ]}
                            />
                            {this.renderPartnerLifetimeAllowanceDetails()}
                        </form>
                    </div>
                    <div className="col span-3 col-last explain_text mt-4rem">
                        <ModalInformationIcon
                            title="Why we’re asking this"
                            // eslint-disable-next-line max-len
                            text="There is a limit to how much you can accumulate within your pension over your life and still receive tax relief. Lifetime allowance protection can help protect extra pension benefits you may have accrued over the current year maximum."
                        />
                    </div>
                </section>
                <BackNextButtons
                    isActiveBack={false}
                    onClickNext={handleSubmit}
                    isActiveNext={!isSubmitted || (isAnySyncErrors && isSubmitted)}
                    onClickBack={handleBack}
                />
            </Fragment>
        );
    }
}

PensionProtection.propTypes = {
    handleSubmit: PropTypes.func.isRequired,
    handleBack: PropTypes.func.isRequired,
    isSubmitted: PropTypes.bool.isRequired,
    formValue: PropTypes.object,
    handleGetMePlan: PropTypes.func.isRequired,
};

PensionProtection.defaultProps = {
    formValue: {},
};

const mapStateToProps = () => ({
    initialValues: {},
});

const PensionProtectionWithHoc = SurveyHoc(PensionProtection, PensionProtection.surveyData);

export default withRouter(connect(mapStateToProps)(PensionProtectionWithHoc));
