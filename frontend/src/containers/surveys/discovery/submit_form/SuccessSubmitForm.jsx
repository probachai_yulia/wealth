import React, { Component, Fragment } from 'react';
import { reduxForm } from 'redux-form';
import get from 'lodash/get';
import isEmpty from 'lodash/isEmpty';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { withRouter } from 'react-router-dom';
import { bindActionCreators } from 'redux';

import FulfilledLine from '../../../../components/FulfilledLine';
import { getMePlan } from '../../../../actions/UserActions';

/**
 * Render the content for user idle. Dumb component
 *
 */
class SubmitForm extends Component {
    constructor(props) {
        super(props);

        this.state = {
            expandedAnswer: false,
            viewModal: false,
        };

        this.handleSubmit = this.handleSubmit.bind(this);
        this.toggleList = this.toggleList.bind(this);
        this.renderModal = this.renderModal.bind(this);
        this.viewModal = this.viewModal.bind(this);

        const {
            handleGetMePlan,
        } = this.props;

        handleGetMePlan();
    }

    handleSubmit = (event) => {
        const { formValue } = this.props;
        if (!isEmpty(get(formValue, 'syncErrors', {}))) {
            event.preventDefault();
        } else {
            console.log('good submit');
            // TODO
            // implement send to the server
            // this.props.history.push('add-insurance');
        }
    };

    toggleList() {
        const { expandedAnswer } = this.state;
        this.setState({
            expandedAnswer: !expandedAnswer,
        });
    }

    viewModal() {
        const { viewModal } = this.state;
        this.setState({
            viewModal: !viewModal,
        });
    }

    renderModal() {
        const { viewModal } = this.state;
        if (viewModal) {
            return (
                <div className="modal_form">
                    <div className="modal_form_body">
                        <p>
                            You won’t be able to edit your answers after submitting.
                        </p>
                        <div className="modal_form_action mt-2rem">
                            <button
                                type="button"
                                className="btn small btn-border-purple"
                                onClick={this.viewModal}
                            >
                                Cancel
                            </button>
                            <button
                                type="button"
                                className="btn small purple_button"
                                onClick={this.handleSubmit}
                            >
                                Submit now
                            </button>
                        </div>
                    </div>
                </div>
            );
        }
        return null;
    }

    render() {
        return (
            <Fragment>
                <FulfilledLine loadingPercent={90} lineColor="#6E0A7B" />
                <div className="container dis-f mt-5rem mb-10rem">
                    <div className="col span-8">
                        <h2 className="fw-b  mt-xlarge mb-mlarge">
                            Get ready for our first meeting
                        </h2>
                        <p className="mb-2rem">
                            We will start to build the first version of your Lifeline, and in the next few days we’ll be sending a link with the meeting data confirmation and other small details.
                        </p>
                        <p>
                            We’ll be in touch soon! We look forward to showing you what we can do and achieve together.
                        </p>

                        <div className="links-block mt-1rem done">
                            <h6>
                                Discovery questionnaire
                            </h6>
                            <p>
                                Submitted Jun 29 2018
                            </p>
                        </div>

                    </div>
                    <div className="col span-3 col-last explain_text mt-4rem" />
                </div>
                {this.renderModal()}
            </Fragment>
        );
    }
}

SubmitForm.propTypes = {
    formValue: PropTypes.object,
    handleGetMePlan: PropTypes.func.isRequired,
};

SubmitForm.defaultProps = {
    formValue: {},
};

const initializeForm = reduxForm({
    form: 'submitForm',
})(SubmitForm);

const mapStateToProps = state => ({
    formValue: get(state, 'form.submitForm', {}),
});

const mapDispatchToProps = dispatch => ({
    handleGetMePlan: bindActionCreators(getMePlan, dispatch),
});

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(initializeForm));
