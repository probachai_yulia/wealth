import React, { Component, Fragment } from 'react';
import { change } from 'redux-form';
import { Link, withRouter } from 'react-router-dom';
import get from 'lodash/get';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { bindActionCreators } from 'redux';
import { SECTION_FACTFIND } from '../../../../helpers/constants';
import FulfilledLine from '../../../../components/FulfilledLine';
import { submitPlan } from '../../../../actions/PlanActions';
import SurveyHoc from '../../../hoc/SurveyHoc';
import { getMePlan } from '../../../../actions/UserActions';

/**
 * Render the content for user idle. Dumb component
 *
 */
class SubmitForm extends Component {
    static formName = 'submitForm';

    static surveyData = {
        formName: SubmitForm.formName,
        nextStage: 'nextStage',
        prevStage: 'prevStage',
        data: [
            {
                fields: [
                    'planUUID',
                ],
                methodCreate: submitPlan,
            },
        ],
    };

    constructor(props) {
        super(props);

        this.state = {
            expandedAnswer: false,
            viewModal: false,
        };

        this.toggleList = this.toggleList.bind(this);
        this.renderModal = this.renderModal.bind(this);
        this.viewModal = this.viewModal.bind(this);

        const {
            handleGetMePlan,
        } = this.props;

        handleGetMePlan();
    }

    toggleList() {
        const { expandedAnswer } = this.state;
        this.setState({
            expandedAnswer: !expandedAnswer,
        });
    }

    viewModal() {
        const { viewModal } = this.state;
        this.setState({
            viewModal: !viewModal,
        });
    }

    renderModal() {
        const { viewModal } = this.state;
        const { handleSubmit } = this.props;
        if (viewModal) {
            return (
                <div className="modal_form">
                    <div className="modal_form_body">
                        <p>
                            You won’t be able to edit your answers after submitting.
                        </p>
                        <div className="modal_form_action mt-2rem">
                            <button
                                type="button"
                                className="btn small btn-border-purple"
                                onClick={this.viewModal}
                            >
                                Cancel
                            </button>
                            <button
                                type="button"
                                className="btn small purple_button"
                                onClick={handleSubmit}
                            >
                                Submit now
                            </button>
                        </div>
                    </div>
                </div>
            );
        }
        return null;
    }

    render() {
        const { expandedAnswer } = this.state;
        // const isSubmitted = this.state.isSubmitted;
        // const isAnySyncErrors = isEmpty(get(this.props.formValue, 'syncErrors', {}));
        return (
            <Fragment>
                <FulfilledLine loadingPercent={90} lineColor="#6E0A7B" />
                <div className="container dis-f mt-5rem mb-10rem">
                    <div className="col span-8">
                        <h2 className="fw-b mt-xlarge mb-mlarge">
                            Just click to complete
                        </h2>
                        <p className="mb-2rem">
                            Thanks for taking the time to provide all this information. Just click the button below to complete and submit the form.
                        </p>
                        <p>
                            We&apos;ll now get to work on building your first LifeLine which we&apos;ll talk you through at our first meeting. We look forward to seeing you then.
                        </p>

                        <div className="mt-2rem box_border">
                            <div className="dis-f end_questionary">
                                <div className="icon">
                                    <img src="/static/img/tick-circle_green.svg" alt="Thick " />
                                </div>
                                <h6>
                                    Discovery questionnaire
                                </h6>
                                <button
                                    type="button"
                                    onClick={this.toggleList}
                                    className={`${expandedAnswer ? 'openBtn' : 'closeBtn'} link_text_icon full_width`}
                                >
                                    Edit answers
                                </button>

                            </div>

                            <ul className={`${expandedAnswer ? 'open' : 'close'} answer_list`}>
                                <li>
                                    {SECTION_FACTFIND[0].title}
                                    <Link to={SECTION_FACTFIND[0].link}>
                                        Edit
                                    </Link>
                                </li>
                                <li>
                                    {SECTION_FACTFIND[1].title}
                                    <Link to={SECTION_FACTFIND[1].link}>
                                        Edit
                                    </Link>
                                </li>
                                <li>
                                    {SECTION_FACTFIND[2].title}
                                    <Link to={SECTION_FACTFIND[2].link}>
                                        Edit
                                    </Link>
                                </li>
                                <li>
                                    {SECTION_FACTFIND[3].title}
                                    <Link to={SECTION_FACTFIND[3].link}>
                                        Edit
                                    </Link>
                                </li>
                                <li>
                                    {SECTION_FACTFIND[4].title}
                                    <Link to={SECTION_FACTFIND[4].link}>
                                        Edit
                                    </Link>
                                </li>
                                <li>
                                    {SECTION_FACTFIND[5].title}
                                    <Link to={SECTION_FACTFIND[5].link}>
                                        Edit
                                    </Link>
                                </li>
                                <li>
                                    {SECTION_FACTFIND[6].title}
                                    <Link to={SECTION_FACTFIND[6].link}>
                                        Edit
                                    </Link>
                                </li>
                                <li>
                                    {SECTION_FACTFIND[7].title}
                                    <Link to={SECTION_FACTFIND[7].link}>
                                        Edit
                                    </Link>
                                </li>
                                <li>
                                    {SECTION_FACTFIND[8].title}
                                    <Link to={SECTION_FACTFIND[8].link}>
                                        Edit
                                    </Link>
                                </li>
                            </ul>
                        </div>

                        <button
                            type="button"
                            className="btn small purple_button"
                            onClick={this.viewModal}
                        >
                            Submit questionnaire
                        </button>

                    </div>
                    <div className="col span-3 col-last explain_text mt-4rem" />
                </div>
                {this.renderModal()}
            </Fragment>
        );
    }
}

SubmitForm.propTypes = {
    handleSubmit: PropTypes.func.isRequired,
    handleGetMePlan: PropTypes.func.isRequired,
};

SubmitForm.defaultProps = {
    // formValue: {},
};

const mapStateToProps = state => ({
    formValue: get(state, 'form.submitForm', {}),
    initialValues: {
        planUUID: get(state, 'plan.plan_uuid', null),
        nextStage: '/survey/succes-submit-form',
        prevStage: '/survey/dashboard',
    },
});

const mapDispatchToProps = dispatch => ({
    changeFieldValue: (field, value) => {
        dispatch(change(SubmitForm.formName, field, value));
    },
    handleGetMePlan: bindActionCreators(getMePlan, dispatch),
});

const SubmitFormWithHoc = SurveyHoc(SubmitForm, SubmitForm.surveyData);

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(SubmitFormWithHoc));
