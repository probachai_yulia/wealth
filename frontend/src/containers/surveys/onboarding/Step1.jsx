import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';
import PropTypes from 'prop-types';

class Step1 extends Component {
    constructor(props) {
        super(props);

        this.handleContinue = this.handleContinue.bind(this);
    }

    handleContinue() {
        const { history } = this.props;

        history.push('./step-2');
    }

    render() {
        return (
            <section className="onboarding-wrapper dis-f jc-sb">
                <div className="placeholder placeholder-purple dis-f ai-c jc-c">
                    <p className="placeholder-heading">
                        <img src="/static/img/Octopus_Wealth_white.svg" alt="octopuswealth" />
                    </p>
                </div>
                <section className="onboard p-a">
                    <section className="onboard-wrapper">
                        <h1 className="mt-xlarge onboard-heading mb-small">
                            The power of planning
                        </h1>
                        <p className="onboard-text mb-medium">
                            Good financal planning can have a significant impact on your life and give you the freedom to do the things that are important to you whether its helping your children get ahead, early retirement or taking on
                            the next adventure in life. Using the Octopus LifeLine we can start to visually present your financial situation and show you some of the many ways advice may benefit you. To do that, we need to collect some
                            information from you first. The more accurate your responses, the more accurate we can be.
                        </p>
                        <p className="onboard-text mb-medium">
                            Using the Octopus LifeLine we can start to visually present your financial situation and show you some of the many ways advice may benefit you. To do that, we need to collect some information from you first.
                            The more accurate your responses, the more accurate we can be.
                        </p>
                        <div className="mb-small">
                            <div className="onboard-links-block mt-1rem done list-style-1">
                                <p>
                                    Initial Discovery Call with your Adviser
                                </p>
                            </div>
                            <div className="onboard-links-block mt-1rem todo list-style-2">
                                <p>
                                    Discovery Questionnaire
                                </p>
                            </div>
                            <div className="onboard-links-block mt-1rem todo list-style-3">
                                <p>
                                    Discovery Meeting and presentation of your LifeLine
                                </p>
                            </div>
                        </div>
                        <button
                            onClick={this.handleContinue}
                            className="btn btn-onboard btn-custom"
                            type="button"
                        >
                            Continue
                        </button>
                    </section>
                </section>
            </section>
        );
    }
}

Step1.propTypes = {
    history: PropTypes.object.isRequired,
};

export default withRouter(Step1);
