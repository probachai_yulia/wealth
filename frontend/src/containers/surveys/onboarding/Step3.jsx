import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { reduxForm, Field } from 'redux-form';
import get from 'lodash/get';
import { connect } from 'react-redux';
import Checkbox from '../../../components/Checkbox';
import { normalizeInverse } from '../../../utils/helper';
import { booleanRequired } from '../../../utils/validation.helper';

class Step3 extends Component {
    constructor(props) {
        super(props);

        this.state = {
            isSubmitted: false,
        };

        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleSubmit() {
        this.setState(() => ({ isSubmitted: true }));
    }

    render() {
        const { formValue } = this.props;
        const { isSubmitted } = this.state;

        const termsOfBusinessLink = '<a class="onboard-link" href="/terms-and-conditions" target="_blank">Terms of Business</a>';
        const privacyPolicyLink = '<a class="onboard-link" href="/privacy-policy" target="_blank">Privacy Policy.</a>';

        return (
            <section className="onboarding-wrapper dis-f jc-sb">
                <div className="placeholder placeholder-purple dis-f ai-c jc-c">
                    <p className="placeholder-heading">
                        <img src="/static/img/Octopus_Wealth_white.svg" alt="octopuswealth" />
                    </p>
                </div>
                <section className="onboard p-a">
                    <section className="onboard-wrapper">
                        <h1 className="mt-xlarge span-8 onboard-heading mb-small">
                            Confirm you’re happy
                        </h1>
                        <p className="onboard-text mt-15rem mb-2rem">
                            Our Terms of Business outlines all aspects of how our relationship is going to work, including details on:
                            <ul className="mt-medium mb-medium ml-2rem">
                                <li>
                                    The type of restricted advice we can provide you
                                </li>
                                <li>
                                    How we are paid for our services
                                </li>
                                <li>
                                    How to complain if we don’t quite get things right
                                </li>
                            </ul>
                            At Octopus Wealth we take your privacy seriously and will only use your personal information to help us do our job:
                            providing the products and services that allow us to deliver the best-possible financial plan for your future.
                        </p>
                        <Field
                            component={Checkbox}
                            name="terms_and_privacy"
                            fieldValue="terms_and_privacy"
                            label={`I agree to the ${termsOfBusinessLink} and ${privacyPolicyLink}`}
                            normalize={normalizeInverse}
                            checked={get(formValue, 'values.terms_and_policy', null)}
                            customErrors="This field is required"
                            isShowErrors={isSubmitted}
                            validate={[
                                booleanRequired,
                            ]}
                        />
                        <Field
                            component={Checkbox}
                            extraClasses="mt-1rem mb-15rem"
                            name="email_connection"
                            fieldValue="email_connection"
                            label="I'm happy to be contacted by email about other Octopus Products."
                            normalize={normalizeInverse}
                            checked={get(formValue, 'values.email_connection', null)}
                        />
                        <Field
                            component={Checkbox}
                            extraClasses="mb-15rem"
                            name="phone_connection"
                            fieldValue="phone_connection"
                            label="I'm happy to be contacted by phone about other Octopus Products."
                            normalize={normalizeInverse}
                            checked={get(formValue, 'values.phone_connection', null)}
                        />
                        <button
                            onClick={this.handleSubmit}
                            className="btn btn-onboard mt-small"
                            type="button"
                            disabled={!get(formValue, 'values.terms_and_privacy', null) && isSubmitted}
                        >
                            Get started
                        </button>
                    </section>
                </section>
            </section>
        );
    }
}

const initializeForm = reduxForm({
    form: 'onboardingStep3',
    enableReinitialize: true,
})(Step3);

Step3.propTypes = {
    formValue: PropTypes.object,
};

Step3.defaultProps = {
    formValue: {},
};

const mapStateToProps = state => ({
    formValue: get(state, 'form.onboardingStep3', {}),
});

export default connect(mapStateToProps)(initializeForm);
