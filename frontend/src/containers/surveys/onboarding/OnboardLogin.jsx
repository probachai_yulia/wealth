import React, { Component, Fragment } from 'react';
import { reduxForm, Field } from 'redux-form';
import InputCustom from '../../../components/InputCustom';
import {
    required, isEmail,
} from '../../../utils/validation.helper';

class OnboardLogin extends Component {
    constructor(props) {
        super(props);

        this.state = {
            isSubmitted: false,
        };

        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleSubmit() {
        this.setState(() => ({ isSubmitted: true }));
    }

    render() {
        const { isSubmitted } = this.state;
        return (
            <Fragment>
                <section className="onboarding-wrapper dis-f">
                    <div className="placeholder placeholder-purple dis-f ai-c jc-c">
                        <p className="placeholder-heading">
                            <img src="../../../../../static/img/Octopus_Wealth_white.svg" alt="octopuswealth" />
                        </p>
                    </div>
                    <section className="onboard p-r as-c">
                        <section className="onboard-wrapper">
                            <h1 className="span-8 onboard-heading">
                                Log in
                            </h1>
                            <Field
                                component={InputCustom}
                                label="Email"
                                type="text"
                                name="email"
                                isShowErrors={isSubmitted}
                                validate={[
                                    required,
                                    isEmail,
                                ]}
                            />
                            <Field
                                component={InputCustom}
                                label="Password"
                                type="password"
                                name="password"
                                hintMessage="Minimum of 8 characters, 1 uppercase letter, 1 lowercase, 1 number"
                                isShowErrors={isSubmitted}
                                validate={[
                                    required,
                                ]}
                            />
                            <button
                                onClick={this.handleSubmit}
                                className="btn btn-login mb-1rem"
                                type="button"
                            >
                                Log in
                            </button>
                            <div
                                className="cur-p col fw-b"
                            >
                                Forgot password?
                            </div>
                        </section>
                    </section>
                </section>
            </Fragment>
        );
    }
}

const initializeForm = reduxForm({
    form: 'onboardLogin',
    enableReinitialize: true,
})(OnboardLogin);

export default initializeForm;
