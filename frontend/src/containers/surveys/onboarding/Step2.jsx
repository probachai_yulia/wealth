import React, { Component } from 'react';
import { reduxForm, Field } from 'redux-form';
import InputCustom from '../../../components/InputCustom';
import {
    required,
    isEmail,
    isLongEnough,
    containsAllRequiredCharacters,
    samePasswords,
} from '../../../utils/validation.helper';
import InputCustomPassword from '../../../components/InputCustomPassword';

class Step2 extends Component {
    constructor(props) {
        super(props);

        this.state = {
            isSubmitted: false,
        };

        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleSubmit() {
        this.setState(() => ({ isSubmitted: true }));
    }

    render() {
        const { isSubmitted } = this.state;
        return (
            <section className="onboarding-wrapper dis-f jc-sb">
                <div className="placeholder placeholder-purple dis-f ai-c jc-c">
                    <p className="placeholder-heading">
                        <img src="/static/img/Octopus_Wealth_white.svg" alt="octopuswealth" />
                    </p>
                </div>
                <section className="onboard p-a">
                    <section className="onboard-wrapper">
                        <h1 className="mt-xlarge onboard-heading mb-small">
                            Create an account to start
                        </h1>
                        <p className="onboard-text mb-medium">
                            Create an account to complete the questionnaire and log in to your portal at any time.
                        </p>
                        <Field
                            component={InputCustom}
                            label="Email"
                            type="text"
                            name="email"
                            isShowErrors={isSubmitted}
                            validate={[
                                required,
                                isEmail,
                            ]}
                        />
                        <Field
                            component={InputCustomPassword}
                            label="Password"
                            name="password"
                            isShowErrors={isSubmitted}
                            hintMessage="Minimum of 8 characters, 1 uppercase letter, 1 lowercase, 1 number"
                            hintMessageIfValid="Strong password"
                            validate={[
                                required,
                                isLongEnough,
                                containsAllRequiredCharacters,
                            ]}
                        />
                        <Field
                            component={InputCustomPassword}
                            label="Confirm password"
                            type="password"
                            name="password-confirm"
                            isShowErrors={isSubmitted}
                            hintMessageIfValid="Passwords are match"
                            validate={[
                                required,
                                isLongEnough,
                                samePasswords('password'),
                            ]}
                        />
                        <button
                            onClick={this.handleSubmit}
                            className="btn btn-onboard"
                            type="button"
                        >
                            Create and continue
                        </button>
                    </section>
                </section>
            </section>
        );
    }
}

const initializeForm = reduxForm({
    form: 'onboardingStep2',
    enableReinitialize: true,
})(Step2);

export default initializeForm;
