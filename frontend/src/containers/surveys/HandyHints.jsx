import React, { Component } from 'react';
import { Link, withRouter } from 'react-router-dom';

class HandyHints extends Component {
    render() {
        return (
            <section className="handy-hints dis-f fd-c jc-c">
                <h2 className="heading">
                    Handy hints before you start
                </h2>
                <section className="item dis-f mt-large">
                    <img
                        src="/static/img/icons/handy-hint-progress.svg"
                        alt="Handy hint progress"
                        title="Save your progress"
                    />
                    <section className="text dis-f fd-c ml-small">
                        <h5>
                            Save your progress
                        </h5>
                        <p className="mt-0-5rem">
                            You can save your progress and return at any time by clicking the X button.
                        </p>
                    </section>
                </section>
                <section className="item dis-f mt-medium">
                    <img
                        src="/static/img/icons/handy-hint-info.svg"
                        alt="Handy hint progress"
                        title="Add additional information"
                    />
                    <section className="text dis-f fd-c ml-small">
                        <h5>
                            Add additional information
                        </h5>
                        <p className="mt-0-5rem">
                            You can tell us additional information in each section about your family, your situation or your finances by using the “Anything else to add” fields.
                        </p>
                    </section>
                </section>
                <section className="item dis-f mt-medium">
                    <img
                        src="/static/img/icons/handy-hint-upload.svg"
                        alt="Handy hint progress"
                        title="Upload statements"
                    />
                    <section className="text dis-f fd-c ml-small">
                        <h5>
                            Upload statements
                        </h5>
                        <p className="mt-0-5rem">
                            Have a policy document to hand? Don&apos;t worry about completing the fields and upload your policy statement instead.
                        </p>
                    </section>
                </section>
                <Link
                    to="/survey/dashboard"
                    className="btn button"
                    type="button"
                >
                    Continue
                </Link>
            </section>
        );
    }
}

export default withRouter(HandyHints);
