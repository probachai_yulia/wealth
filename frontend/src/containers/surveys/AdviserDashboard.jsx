import ReactDOM from 'react-dom';
import React, { Component, Fragment } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import get from 'lodash/get';
import PropTypes from 'prop-types';
import moment from 'moment';
import { Link, withRouter } from 'react-router-dom';
import { change } from 'redux-form';
import InfoRow from '../../components/InfoRow';
import AdviserPagination from '../../components/AdviserPagination';
import AddUserModal from '../../components/AddUserModal';
import AddInternalUserModal from '../../components/AddInternalUserModal';

import {
    addUser, getAll, getMe, getUserByRole,
} from '../../actions/UserActions';
import AdviserHoc from '../hoc/AdviserHoc';
import { requestsReset } from '../../actions/CommonActions';

class AdviserDashboard extends Component {
    static formName = 'adviserDashboard';

    static surveyData = {
        formName: AdviserDashboard.formName,
        nextStage: 'nextStage',
        prevStage: 'prevStage',
        data: [
            {
                fields: [
                    'first_name',
                    'last_name',
                    'role',
                    'email',
                    'phone',
                    'adviser',
                    'partner',
                ],
                action: 'closeModal',
                methodCreate: addUser,
            },
        ],
    };

    constructor(props) {
        super(props);
        const page = 1;
        const perPage = 10;
        this.state = {
            showList: false,
            showClientModal: false,
            showInternalUserModal: false,
            // isSubmitted: false,
            page,
            perPage,
        };

        this.renderModal = this.renderModal.bind(this);
        this.handleClientModal = this.handleClientModal.bind(this);
        this.handleInternalUserModal = this.handleInternalUserModal.bind(this);
        this.handleAddUserDropdown = this.handleAddUserDropdown.bind(this);
        this.changePage = this.changePage.bind(this);
        const {
            handleGetAllUsers, handleGetMe, handleGetByRole,
        } = this.props;
        handleGetByRole('adviser');
        const body = {};
        body.page = page;
        body.perPage = perPage;
        handleGetAllUsers(body);
        handleGetMe();
    }

    componentDidMount() {
        document.addEventListener('click', this.handleClickOutside, true);
    }

    componentWillUpdate() {
        const {
            formValue, changeFieldValue, currentID, currentUserType,
        } = this.props;
        const hasPartner = get(formValue, 'values.family_has_partner', null) === true;
        const { showClientModal } = this.state;
        if (showClientModal) { changeFieldValue('role', 'client'); }
        if (currentUserType === 'adviser') { changeFieldValue('adviser', currentID); }
        if (hasPartner) {
            const partner = {
                plan_user_type: 'partner',
                first_name: get(formValue, 'values.partner_first_name', ''),
                last_name: get(formValue, 'values.partner_last_name', ''),
                email: get(formValue, 'values.partner_email', ''),
                phone: get(formValue, 'values.partner_mobile', ''),
            };
            changeFieldValue('partner', partner);
        }
    }

    componentDidUpdate() {
        const {
            isRequestsDone, handleGetAllUsers, handleRequestsReset, handleGetByRole,
        } = this.props;
        if (isRequestsDone !== 0) {
            this.handleClientModal('close');
            this.handleInternalUserModal('close');
            handleRequestsReset();
            const { page, perPage } = this.state;
            const body = {};
            body.page = page;
            body.perPage = perPage;
            handleGetAllUsers(body);
            handleGetByRole('adviser');
        }
    }

    componentWillUnmount() {
        document.removeEventListener('click', this.handleClickOutside, true);
    }

    handleClickOutside = (event) => {
        // eslint-disable-next-line react/no-find-dom-node
        const domNode = ReactDOM.findDOMNode(this);

        if (!domNode || !domNode.contains(event.target)) {
            this.setState({
                showList: false,
            });
        }
    };

    handleClientModal(action) {
        const { showClientModal } = this.state;
        const { reset } = this.props;
        reset();
        const modalAction = action === 'close' ? false : !showClientModal;
        this.setState(() => ({
            showClientModal: modalAction,
            showList: false,
        }));
    }

    handleInternalUserModal(action) {
        const { showInternalUserModal } = this.state;
        const { reset } = this.props;
        reset();
        const modalAction = action === 'close' ? false : !showInternalUserModal;
        this.setState(() => ({
            showInternalUserModal: modalAction,
            showList: false,
        }));
    }

    handleAddUserDropdown() {
        const { showList } = this.state;
        this.setState(() => ({
            showList: !showList,
        }));
    }

    changePage(val) {
        const { handleGetAllUsers } = this.props;
        const { perPage } = this.state;
        const body = {};
        this.setState({ page: val });
        body.page = val;
        body.perPage = perPage;
        handleGetAllUsers(body);
    }

    renderModal() {
        const { showClientModal, showInternalUserModal } = this.state;
        const {
            formValue, isSubmitted, handleSubmit, users, currentUserType,
        } = this.props;
        return (
            <Fragment>
                <AddUserModal
                    modalTitle="Add user"
                    isSubmitted={isSubmitted}
                    visible={showClientModal}
                    formValue={formValue}
                    users={users}
                    handleSubmit={handleSubmit}
                    currentUserType={currentUserType}
                    handleClose={this.handleClientModal}
                />
                <AddInternalUserModal
                    modalTitle="Add internal user"
                    isSubmitted={isSubmitted}
                    visible={showInternalUserModal}
                    formValue={formValue}
                    handleSubmit={handleSubmit}
                    handleClose={this.handleInternalUserModal}
                />
            </Fragment>
        );
    }

    render() {
        const {
            users, currentUserType, pagination, adviserList,
        } = this.props;
        const { showList, page, perPage } = this.state;

        return (
            <section className="adviser-dashboard">
                <div className="table-title dis-f jc-sb ai-c">
                    <h3>
                        Users
                    </h3>
                    <div className="p-r">
                        <button
                            onClick={this.handleAddUserDropdown}
                            type="button"
                            className="btn btn-purple"
                        >
                            + Add user
                        </button>
                        {showList && (
                            <div
                                className="list p-a"
                            >
                                {
                                    adviserList.length > 0 && (
                                        <span
                                            role="presentation"
                                            onClick={this.handleClientModal}
                                            className="mb-medium"
                                        >
                                            Client
                                        </span>
                                    )
                                }
                                {currentUserType === 'admin' && (
                                    <span
                                        role="presentation"
                                        onClick={this.handleInternalUserModal}
                                    >
                                        Internal user
                                    </span>
                                )}
                            </div>
                        )}
                        {this.renderModal()}
                    </div>
                </div>
                <div className="table-header mt-large dis-f ai-c jc-sb">
                    <div className="column-heading">
                        <span>
                            Name
                        </span>
                        <img
                            src="/static/img/icons/arrow-sort.svg"
                            alt="Sort"
                            title="Sort"
                        />
                    </div>
                    <div className="column-heading">
                        <span>
                            User type
                        </span>
                        <img
                            src="/static/img/icons/arrow-sort.svg"
                            alt="Sort"
                            title="Sort"
                        />
                    </div>
                    <div className="column-heading">
                        <span>
                            Last updated
                        </span>
                        <img
                            src="/static/img/icons/arrow-sort.svg"
                            alt="Sort"
                            title="Sort"
                        />
                    </div>
                </div>
                <div className="info-rows">
                    {
                        users.map((user, index) => {
                            const linkByRole = user.user_type === 'client' ? 'user' : 'internal-user';
                            return (
                                <Link to={`./${linkByRole}/${user.profile_uuid}`} key={index.toString()}>
                                    <div
                                        key={user.user_id}
                                        className="info-row-link no-link"
                                    >
                                        <InfoRow
                                            extraClass="three_columns"
                                            data={{
                                                userName: `${user.profile.first_name} ${user.profile.last_name}`,
                                                userType: user.user_type,
                                                lastUpdated: moment(user.update_dttm).fromNow(),
                                            }}
                                        />
                                    </div>
                                </Link>
                            );
                        })}
                </div>
                <AdviserPagination
                    pagination={pagination}
                    itemsPerPage={perPage}
                    currentPage={page}
                    handleClick={this.changePage}
                />
            </section>
        );
    }
}

AdviserDashboard.propTypes = {
    changeFieldValue: PropTypes.func.isRequired,
    users: PropTypes.arrayOf(PropTypes.object),
    handleGetAllUsers: PropTypes.func.isRequired,
    handleGetMe: PropTypes.func.isRequired,
    formValue: PropTypes.object.isRequired,
    currentUserType: PropTypes.string,
    isSubmitted: PropTypes.bool.isRequired,
    handleSubmit: PropTypes.func.isRequired,
    handleRequestsReset: PropTypes.func.isRequired,
    currentID: PropTypes.string,
    isRequestsDone: PropTypes.number.isRequired,
    pagination: PropTypes.object,
    handleGetByRole: PropTypes.func.isRequired,
    adviserList: PropTypes.arrayOf(PropTypes.object),
    reset: PropTypes.func.isRequired,
};

AdviserDashboard.defaultProps = {
    users: [],
    currentID: '',
    currentUserType: '',
    pagination: {
        pages: 1,
        has_next: false,
        has_previous: false,
        next_page: null,
        total: 0,
    },
    adviserList: [],
};

const mapStateToProps = state => ({
    users: get(state, 'users.items.items', []),
    me: get(state, 'users.me', []),
    currentID: get(state, 'users.me.profile.profile_id', null),
    pagination: {
        pages: get(state, 'users.items.pages', 1),
        has_next: get(state, 'users.items.has_next', false),
        has_previous: get(state, 'users.items.has_previous', false),
        next_page: get(state, 'users.items.next_page', null),
        total: get(state, 'users.items.total', null),
    },
    adviserList: get(state, 'users.filteredUser.items', []),
});

const mapDispatchToProps = dispatch => ({
    handleGetAllUsers: bindActionCreators(getAll, dispatch),
    handleGetMe: bindActionCreators(getMe, dispatch),
    changeFieldValue: (field, value) => {
        dispatch(change(AdviserDashboard.formName, field, value));
    },
    handleRequestsReset: bindActionCreators(requestsReset, dispatch),
    handleGetByRole: bindActionCreators(getUserByRole, dispatch),
});

const AdviserDashboardWithHoc = AdviserHoc(AdviserDashboard, AdviserDashboard.surveyData);

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(AdviserDashboardWithHoc));
