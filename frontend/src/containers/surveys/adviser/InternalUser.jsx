import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Field } from 'redux-form';
import { connect } from 'react-redux';
import get from 'lodash/get';
import { withRouter } from 'react-router-dom';

import isEmpty from 'lodash/isEmpty';
import { bindActionCreators } from 'redux';
import InputCustom from '../../../components/InputCustom';
import { getUser, updateUserById } from '../../../actions/UserActions';
import AdviserHoc from '../../hoc/AdviserHoc';
import SelectDescription from '../../../components/SelectDescription';
import { ROLE_OPTIONS } from '../../../helpers/constants';

class InternalUser extends Component {
    static formName = 'editInternalUser';

    static surveyData = {
        formName: InternalUser.formName,
        data: [
            {
                fields: [
                    'first_name',
                    'last_name',
                    'role',
                    'email',
                    'phone',
                    'role',
                ],
                action: 'closeModal',
                selector: 'profileUUID',
                isUpdate: 'update',
                methodUpdate: updateUserById,
            },
        ],
    };

    constructor(props) {
        super(props);

        this.state = {
            tab: 1,
        };

        this.renderTabContent = this.renderTabContent.bind(this);

        const { handleGetUserByID, userUUID } = this.props;

        handleGetUserByID(userUUID);
    }

    renderTabContent = (tabNumber) => {
        const { formValue, handleSubmit } = this.props;
        if (tabNumber === 1) {
            return (
                <div className="user dis-f fd-c col-last span-8">
                    <div className="title dis-f jc-sb ai-c">
                        <h3>
                            Andrew McLean
                        </h3>
                    </div>
                    <div className="profile-details">
                        <div className="main-info mt-15rem">
                            <div className="avatar dis-f fd-c ai-c jc-c">
                                <img
                                    src="/static/img/icons/adviser-user.svg"
                                    alt="Andrew McLean"
                                    title="Andrew McLean"
                                />
                                <p className="mt-1rem">
                                    Upload
                                </p>
                            </div>
                            <div className="fields col-last span-6">
                                <Field
                                    component={InputCustom}
                                    label="First name"
                                    type="text"
                                    name="first_name"
                                />
                                <Field
                                    component={InputCustom}
                                    label="Last name"
                                    type="text"
                                    name="last_name"
                                />
                                <Field
                                    component={InputCustom}
                                    label="Email"
                                    type="text"
                                    name="email"
                                />
                                <Field
                                    component={SelectDescription}
                                    formValue={formValue}
                                    selectedValue={get(formValue, 'values.role', '')}
                                    options={ROLE_OPTIONS}
                                    formName={InternalUser.formName}
                                    fieldName="role"
                                    name="role"
                                    label="User role"
                                    extraClasses="col span-6 pb-small"
                                />
                                <button
                                    className="btn btn-purple small"
                                    type="button"
                                    onClick={handleSubmit}
                                >
                                    Save changes
                                </button>
                                <button
                                    type="button"
                                    className="btn remove ml-small"
                                >
                                    Delete
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            );
        }

        return null;
    };

    render() {
        const { tab } = this.state;
        const { history } = this.props;
        return (
            <section className="adviser-internal-user container">
                <div className="controls col span-3">
                    <div
                        role="presentation"
                        className="button-back dis-f"
                        onClick={() => history.push('/adviser/dashboard')}
                    >
                        <img
                            src="/static/img/icons/arrow-back.svg"
                            alt="Back"
                            title="Back"
                        />
                        <span>
                            Back to User list
                        </span>
                    </div>
                    <div className="tabs mt-1rem">
                        <div
                            role="presentation"
                            className={`tab ${tab === 1 ? 'tab-active' : null}`}
                            onClick={() => this.setState(() => ({ tab: 1 }))}
                        >
                            Profile details
                        </div>
                    </div>
                </div>
                {this.renderTabContent(tab)}
            </section>
        );
    }
}

InternalUser.propTypes = {
    formValue: PropTypes.object.isRequired,
    history: PropTypes.object.isRequired,
    handleGetUserByID: PropTypes.func.isRequired,
    userUUID: PropTypes.string.isRequired,
    handleSubmit: PropTypes.func.isRequired,
};

InternalUser.defaultProps = {};

const mapStateToProps = (state, props) => {
    const activePlan = get(state, 'users.selectedUser.active_plan', null);
    return ({
        formValue: get(state, 'form.user', null),
        countriesOptions: get(state, 'options.countries', []),
        // eslint-disable-next-line max-len
        userFirstName: isEmpty(get(state, 'users.selectedUser.user', null)) ? get(state, 'users.selectedUser.profile.first_name', null) : get(state, 'users.selectedUser.user.profile.first_name', null),
        // eslint-disable-next-line max-len
        userLastName: isEmpty(get(state, 'users.selectedUser.user', null)) ? get(state, 'users.selectedUser.profile.last_name', null) : get(state, 'users.selectedUser.user.profile.last_name', null),
        userUUID: get(props, 'match.params.profile_uuid', null),
        planUUID: get(state, 'users.selectedUser.active_plan', null),
        partner: get(state, `users.selectedUser.connected_persons['${activePlan}'].partner`, {}),
        child: get(state, `users.selectedUser.connected_persons['${activePlan}'].child`, []),
        initialValues: {
            update: true,
            userUUID: get(props, 'match.params.profile_uuid', null),
            profileUUID: get(props, 'match.params.profile_uuid', null),
            // eslint-disable-next-line max-len
            first_name: isEmpty(get(state, 'users.selectedUser.user', null)) ? get(state, 'users.selectedUser.profile.first_name', null) : get(state, 'users.selectedUser.user.profile.first_name', null),
            // eslint-disable-next-line max-len
            last_name: isEmpty(get(state, 'users.selectedUser.user', null)) ? get(state, 'users.selectedUser.profile.last_name', null) : get(state, 'users.selectedUser.user.profile.last_name', null),
            // eslint-disable-next-line max-len
            email: isEmpty(get(state, 'users.selectedUser.user', null)) ? get(state, 'users.selectedUser.profile.email', null) : get(state, 'users.selectedUser.user.profile.email', null),
            // eslint-disable-next-line max-len
            phone: isEmpty(get(state, 'users.selectedUser.user', null)) ? get(state, 'users.selectedUser.profile.phone', null) : get(state, 'users.selectedUser.user.profile.phone', null),
            // eslint-disable-next-line max-len
            role: get(state, 'users.selectedUser.user_type', null),

        },
    });
};

const mapDispatchToProps = dispatch => ({
    handleGetUserByID: bindActionCreators(getUser, dispatch),
});

const InternalUserWithHoc = AdviserHoc(InternalUser, InternalUser.surveyData);

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(InternalUserWithHoc));
