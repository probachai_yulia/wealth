import React, { Component, Fragment } from 'react';
import PropTypes from 'prop-types';
import { Field } from 'redux-form';
import { connect } from 'react-redux';
import get from 'lodash/get';
import isEmpty from 'lodash/isEmpty';
import { Link, withRouter } from 'react-router-dom';

import { bindActionCreators } from 'redux';
import InputCustom from '../../../components/InputCustom';
import { LoadableAddress } from '../../../components/Loadable';
import InfoRowTable from '../../../components/InfoRowTable';
import {
    checkAml, deleteUserById, getUser, updateUserInPlan,
} from '../../../actions/UserActions';
import AdviserHoc from '../../hoc/AdviserHoc';
import { addUserAddress, updatePlanAddress } from '../../../actions/PlanActions';
import ModalText from '../../../components/ModalText';

class User extends Component {
    static formName = 'editClient';

    static surveyData = {
        formName: User.formName,
        data: [
            {
                fields: [
                    'planUUID',
                    'first_name',
                    'last_name',
                    'role',
                    'email',
                    'phone',
                    'adviser',
                    'partner',
                ],
                action: 'closeModal',
                selector: 'userUUID',
                selectorPlus: 'planUUID',
                isUpdate: 'update',
                methodCreate: updateUserInPlan,
                methodUpdate: updateUserInPlan,
            },
            {
                fields: [
                    'postcode',
                    'address_line_1',
                    'address_line_2',
                    'city',
                    'country_cd',
                    'location',
                ],
                additionalFields: {
                    is_primary_address: true,
                },
                methodUpdate: updatePlanAddress,
                methodCreate: addUserAddress,
                isUpdate: 'updateAddress',
                selector: 'addressUUID',
                selectorPlus: 'planUUID',
            },
        ],
    };

    constructor(props) {
        super(props);

        this.state = {
            tab: 1,
            deleteUserModal: false,
        };

        this.renderTabContent = this.renderTabContent.bind(this);
        this.connectedPersons = this.connectedPersons.bind(this);
        this.handleCheckAML = this.handleCheckAML.bind(this);
        this.renderModalForm = this.renderModalForm.bind(this);
        this.handleRemoveUser = this.handleRemoveUser.bind(this);
        this.changeTab = this.changeTab.bind(this);

        const { handleGetUserByID, userUUID } = this.props;

        handleGetUserByID(userUUID);
    }

    componentWillUpdate() {
        const { isRequestsDone, history } = this.props;
        const { deleteUserModal } = this.state;
        if (isRequestsDone !== 0 && deleteUserModal) {
            history.push('/adviser/dashboard');
        }
    }

    handleCheckAML = (event) => {
        event.preventDefault();
        const { userUUID, handleCheckAML } = this.props;

        handleCheckAML(userUUID);
    };

    handleRemoveUser = (event) => {
        event.preventDefault();
        const { userUUID, handleDeleteUser } = this.props;
        handleDeleteUser(userUUID);
    };

    connectedPersons($type) {
        const { partner, child } = this.props;
        switch ($type) {
            case 'partner':
                if (partner !== null) {
                    if (typeof partner.profile !== 'undefined') {
                        return (
                            <div className="dependence dis-f ai-c jc-sb">
                                <div className="dis-f ai-c jc-sb">
                                    <img
                                        className="mr-small"
                                        src="/static/img/icons/adviser-user.svg"
                                        alt="Anna Smith"
                                        title="Anna Smith"
                                    />
                                    <span>{partner.profile.display_name}</span>
                                </div>
                                <span>Client 2</span>
                            </div>
                        );
                    }
                }
                break;
            case 'child':
                return child.map((item, i) => (
                    <div className="dependence dis-f ai-c jc-sb" key={i.toString()}>
                        <div className="dis-f ai-c jc-sb">
                            <img
                                className="mr-small"
                                src="/static/img/icons/adviser-user.svg"
                                alt="Tim Smith"
                                title="Tim Smith"
                            />
                            <span>{item.profile.display_name}</span>
                        </div>
                        <span>Child</span>
                    </div>
                ));
            default:
                return null;
        }
        return null;
    }

    changeTab(numTab) {
        this.setState({ tab: numTab });
    }

    renderTabContent(tabNumber) {
        const {
            formValue,
            countriesOptions,
            userFirstName,
            userLastName,
            handleSubmit,
            isSubmitted,
            userUUID,
        } = this.props;
        // const { deleteUserModal } = this.state;
        if (tabNumber === 1) {
            return (
                <div className="user col-last span-8 dis-f fd-c">
                    <div className="title dis-f jc-sb">
                        <h3>{`${userFirstName} ${userLastName}`}</h3>
                        <Link
                            to={`/adviser/plan/${userUUID}`}
                            type="button"
                            className="btn btn-purple small"
                        >
                            View connected plan
                        </Link>
                    </div>
                    <div className="profile-details">
                        <div className="main-info mt-15rem">
                            <img
                                src="/static/img/icons/adviser-user.svg"
                                alt="John Smith"
                                title="John Smith"
                            />
                            <div className="fields col-last span-6">
                                <Field
                                    component={InputCustom}
                                    label="First name"
                                    type="text"
                                    name="first_name"
                                />
                                <Field
                                    component={InputCustom}
                                    label="Last name"
                                    type="text"
                                    name="last_name"
                                />
                                <button
                                    className="btn btn-purple small"
                                    type="button"
                                    onClick={handleSubmit}
                                >
                                    Save changes
                                </button>
                            </div>
                        </div>
                        <div className="hr mt-large mb-large" />
                        <div className="dependents">
                            <h3 className="mb-medium">
                                Connected persons
                            </h3>
                            {this.connectedPersons('partner')}
                            {this.connectedPersons('child')}
                            <div className="terms dis-f ai-c jc-sb mb-medium">
                                <span>
                                    Terms of business
                                </span>
                                <span className="italic">
                                    No terms accepted
                                </span>
                            </div>
                            <button
                                type="button"
                                className="btn remove"
                                onClick={prevState => this.setState({ deleteUserModal: !prevState.deleteUserModal })}
                            >
                                Delete
                            </button>
                            {this.renderModalForm()}
                        </div>
                    </div>
                </div>
            );
        }

        if (tabNumber === 2) {
            return (
                <div className="contacts col-last span-8">
                    <Fragment>
                        <h3>
                            Contact details
                        </h3>
                        <div className="dis-f mt-medium">
                            <Field
                                name="email"
                                component={InputCustom}
                                extraClasses="col span-4"
                                label="Email"
                                type="text"
                                placeholder="Email"
                            />
                            <Field
                                name="phone"
                                component={InputCustom}
                                extraClasses="col-last span-4"
                                label="Phone"
                                type="number"
                                placeholder="Phone"
                            />
                        </div>
                        <div className="hr mt-1rem mb-large" />
                        <h3>
                            Address
                        </h3>
                        <div className="address">
                            <LoadableAddress
                                isShowErrors={isSubmitted}
                                formName={User.formName}
                                postcode={get(formValue, 'values.postcode', '0')}
                                selectAddress={get(formValue, 'values.select-address', null)}
                                label="At what address do you currently live?"
                                countriesOptions={countriesOptions}
                            />
                        </div>
                        <button
                            className="btn btn-purple small mt-small"
                            type="button"
                            onClick={handleSubmit}
                        >
                            Save changes
                        </button>
                    </Fragment>
                </div>
            );
        }

        if (tabNumber === 3) {
            return (
                <div className="kyc-aml-status col-last span-8">
                    <div className="title dis-f jc-sb ai-c">
                        <h3>
                            KYC / AML status
                        </h3>
                        <span>
                            View changelog
                        </span>
                    </div>
                    <div className="info-rows mt-medium">
                        <InfoRowTable
                            data={{
                                fieldName: 'TS',
                                fieldValue: '--',
                            }}
                        />
                        <InfoRowTable
                            data={{
                                fieldName: 'WC',
                                fieldValue: '--',
                            }}
                        />
                        <InfoRowTable
                            data={{
                                fieldName: 'HTAR',
                                fieldValue: '--',
                            }}
                        />
                        <InfoRowTable
                            data={{
                                fieldName: 'Case ID',
                                fieldValue: '9d19f211-6602-43dc-8987-b634e86ea2fa',
                            }}
                        />
                    </div>
                    <button
                        className="btn btn-purple small mt-medium"
                        type="button"
                        onClick={this.handleCheckAML}
                    >
                        Start AML/KYC checks
                    </button>
                </div>
            );
        }

        return null;
    }

    renderModalForm() {
        const { formValue } = this.props;
        const { deleteUserModal } = this.state;

        if (deleteUserModal) {
            return (
                <Fragment>
                    <ModalText
                        modalTitle={`${get(formValue, 'values.first_name', null)} delete user`}
                        // eslint-disable-next-line max-len
                        text="Are you sure you want to delete this user?"
                        visible={deleteUserModal}
                        handleCancel={() => this.setState({ deleteUserModal: false })}
                        handleConfirm={this.handleRemoveUser}
                    />
                </Fragment>
            );
        }
        return null;
    }

    render() {
        const { tab } = this.state;
        const { history } = this.props;

        return (
            <section className="adviser-user container">
                <div className="controls">
                    <div
                        role="presentation"
                        className="button-back dis-f"
                        onClick={() => history.push('/adviser/dashboard')}
                    >
                        <img
                            src="/static/img/icons/arrow-back.svg"
                            alt="Back"
                            title="Back"
                        />
                        <span>
                            Back
                        </span>
                    </div>
                    <div className="tabs col span-3 mt-1rem">
                        <div
                            role="presentation"
                            className={`tab ${tab === 1 ? 'tab-active' : null}`}
                            onClick={this.changeTab.bind(this, 1)}
                        >
                            Profile details
                        </div>
                        <div
                            role="presentation"
                            className={`tab ${tab === 2 ? 'tab-active' : null}`}
                            onClick={this.changeTab.bind(this, 2)}
                        >
                            Contacts
                        </div>
                        <div
                            role="presentation"
                            className={`tab ${tab === 3 ? 'tab-active' : null}`}
                            onClick={this.changeTab.bind(this, 3)}
                        >
                            KYC / AML status
                        </div>
                    </div>
                </div>
                {this.renderTabContent(tab)}
            </section>
        );
    }
}

User.propTypes = {
    countriesOptions: PropTypes.arrayOf(PropTypes.object),
    formValue: PropTypes.object.isRequired,
    history: PropTypes.object.isRequired,
    handleGetUserByID: PropTypes.func.isRequired,
    userUUID: PropTypes.string.isRequired,
    userFirstName: PropTypes.string,
    userLastName: PropTypes.string,
    handleSubmit: PropTypes.func.isRequired,
    isSubmitted: PropTypes.bool.isRequired,
    partner: PropTypes.object,
    child: PropTypes.arrayOf(PropTypes.object),
    handleCheckAML: PropTypes.func,
    handleDeleteUser: PropTypes.func,
    isRequestsDone: PropTypes.number,
};

User.defaultProps = {
    handleCheckAML: () => {},
    handleDeleteUser: () => {},
    isRequestsDone: () => {},
    userFirstName: '',
    userLastName: '',
    countriesOptions: [],
    partner: {},
    child: [],
};

const mapStateToProps = (state, props) => {
    const activePlan = get(state, 'users.selectedUser.active_plan', null);
    return ({
        aml: get(state, 'users.aml', null),
        formValue: get(state, 'form.user', null),
        countriesOptions: get(state, 'options.countries', []),
        // eslint-disable-next-line max-len
        userFirstName: isEmpty(get(state, 'users.selectedUser.user', null)) ? get(state, 'users.selectedUser.profile.first_name', null) : get(state, 'users.selectedUser.user.profile.first_name', null),
        // eslint-disable-next-line max-len
        userLastName: isEmpty(get(state, 'users.selectedUser.user', null)) ? get(state, 'users.selectedUser.profile.last_name', null) : get(state, 'users.selectedUser.user.profile.last_name', null),
        userUUID: get(props, 'match.params.profile_uuid', null),
        planUUID: get(state, 'users.selectedUser.active_plan', null),
        partner: get(state, `users.selectedUser.connected_persons['${activePlan}'].partner`, {}),
        child: get(state, `users.selectedUser.connected_persons['${activePlan}'].child`, []),
        isRequestsDone: get(state, 'common.isRequestsDone', 0),
        initialValues: {
            update: true,
            userUUID: get(props, 'match.params.profile_uuid', null),
            planUUID: get(state, 'users.selectedUser.active_plan', null),
            addressUUID: get(state, 'users.selectedUser.plan_user.primary_address.address_uuid', null),
            updateAddress: get(state, 'users.selectedUser.plan_user.primary_address.address_uuid', null) !== null,
            // eslint-disable-next-line max-len
            first_name: isEmpty(get(state, 'users.selectedUser.user', null)) ? get(state, 'users.selectedUser.profile.first_name', null) : get(state, 'users.selectedUser.user.profile.first_name', null),
            // eslint-disable-next-line max-len
            last_name: isEmpty(get(state, 'users.selectedUser.user', null)) ? get(state, 'users.selectedUser.profile.last_name', null) : get(state, 'users.selectedUser.user.profile.last_name', null),
            // eslint-disable-next-line max-len
            email: isEmpty(get(state, 'users.selectedUser.user', null)) ? get(state, 'users.selectedUser.profile.email', null) : get(state, 'users.selectedUser.user.profile.email', null),
            // eslint-disable-next-line max-len
            phone: isEmpty(get(state, 'users.selectedUser.user', null)) ? get(state, 'users.selectedUser.profile.phone', null) : get(state, 'users.selectedUser.user.profile.phone', null),
            // eslint-disable-next-line max-len
            postcode: get(state, 'users.selectedUser.plan_user.primary_address.postcode', null),
            address_1: get(state, 'users.selectedUser.plan_user.primary_address.address_1', null),
            address_2: get(state, 'users.selectedUser.plan_user.primary_address.address_2', null),
            city: get(state, 'users.selectedUser.plan_user.primary_address.city', null),
            country_cd: get(state, 'users.selectedUser.plan_user.primary_address.country_cd', null),

        },
    });
};

const mapDispatchToProps = dispatch => ({
    handleGetUserByID: bindActionCreators(getUser, dispatch),
    handleCheckAML: bindActionCreators(checkAml, dispatch),
    handleDeleteUser: bindActionCreators(deleteUserById, dispatch),
});

const UserWithHoc = AdviserHoc(User, User.surveyData);

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(UserWithHoc));
