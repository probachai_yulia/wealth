import React, { Component, Fragment } from 'react';
import PropTypes from 'prop-types';
import { withRouter } from 'react-router-dom';
import { change, reduxForm } from 'redux-form';
import moment from 'moment';

import get from 'lodash/get';
import { bindActionCreators } from 'redux';
import connect from 'react-redux/es/connect/connect';
import InfoRowTable from '../../../components/InfoRowTable';
import InfoRow from '../../../components/InfoRow';
import AdviserPartnerModal from '../../../components/AdviserPartnerModal';
import SoftFacts from '../../../components/SoftFacts';
import { getMe, getUser } from '../../../actions/UserActions';
import { getSoftFacts } from '../../../actions/PlanActions';
import { getPersonAge } from '../../../utils/helper';

class UserPlan extends Component {
    constructor(props) {
        super(props);

        this.state = {
            isSubmitted: false,
            contactsAreOpened: false,
            contactsAreOpenedPartner: false,
            showPartnerModal: false,
            openTextArea: false,
        };

        this.renderContacts = this.renderContacts.bind(this);
        this.renderContactsPartner = this.renderContactsPartner.bind(this);
        this.renderPartnerModal = this.renderPartnerModal.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleClosePartnerModal = this.handleClosePartnerModal.bind(this);
        this.renderDependent = this.renderDependent.bind(this);

        const {
            handleGetUserByID, userUUID, handleGetMe, handleGetSoftFact, planUUID,
        } = this.props;
        handleGetMe();
        handleGetUserByID(userUUID);
        if (planUUID !== null) {
            handleGetSoftFact(planUUID);
        }
    }

    handleClosePartnerModal() {
        this.setState(() => ({ showPartnerModal: false }));
    }

    handleSubmit() {
        this.setState(() => ({ isSubmitted: true }));
    }

    renderContacts = (areOpened) => {
        const { selectUserProfile, selectUser } = this.props;
        if (areOpened) {
            return (
                <div className="contacts">
                    <div className="dis-f ai-t jc-sb">
                        <span className="contact-title">Email</span>
                        <span className="contact-item">{selectUserProfile.email}</span>
                    </div>
                    <div className="dis-f ai-t jc-sb">
                        <span className="contact-title">Phone</span>
                        <span className="contact-item">{selectUserProfile.phone}</span>
                    </div>
                    <div className="dis-f ai-t jc-sb">
                        <span className="contact-title">Home addr.</span>
                        <span className="contact-item">
                            {selectUser.plan_user.primary_address.address.address_1}
                            ,
                            {' '}
                            {selectUser.plan_user.primary_address.address.address_2}
                            ,
                            {' '}
                            {selectUser.plan_user.primary_address.address.city}
                            ,
                            {' '}
                            {selectUser.plan_user.primary_address.address.postcode}
                        </span>
                    </div>
                    <div className="dis-f ai-t jc-sb">
                        <span className="contact-title">Postal addr.</span>
                        {
                            selectUser.plan_user.postal_address
                                ? (
                                    <span className="contact-item">
                                        {selectUser.plan_user.postal_address.address.address_1}
                                        ,
                                        {' '}
                                        {selectUser.plan_user.postal_address.address.address_2}
                                        ,
                                        {' '}
                                        {selectUser.plan_user.postal_address.address.city}
                                        ,
                                        {' '}
                                        {selectUser.plan_user.postal_address.address.postcode}
                                    </span>
                                )
                                : (
                                    <span className="contact-item">
                                        {selectUser.plan_user.primary_address.address.address_1}
                                        ,
                                        {' '}
                                        {selectUser.plan_user.primary_address.address.address_2}
                                        ,
                                        {' '}
                                        {selectUser.plan_user.primary_address.address.city}
                                        ,
                                        {' '}
                                        {selectUser.plan_user.primary_address.address.postcode}
                                    </span>
                                )
                        }
                    </div>
                </div>
            );
        }

        return null;
    };

    renderContactsPartner = (areOpened) => {
        const { partner, selectUser } = this.props;
        if (areOpened) {
            return (
                <div className="contacts">
                    <div className="dis-f ai-t jc-sb">
                        <span className="contact-title">Email</span>
                        <span className="contact-item">{partner.profile.email}</span>
                    </div>
                    <div className="dis-f ai-t jc-sb">
                        <span className="contact-title">Phone</span>
                        <span className="contact-item">{partner.profile.phone}</span>
                    </div>
                    <div className="dis-f ai-t jc-sb">
                        <span className="contact-title">Home addr.</span>
                        {
                            partner.primary_address
                                ? (
                                    <span className="contact-item">
                                        {partner.primary_address.address.address_1}
                                        ,
                                        {' '}
                                        {partner.primary_address.address.address_2}
                                        ,
                                        {' '}
                                        {partner.primary_address.address.city}
                                        ,
                                        {' '}
                                        {partner.primary_address.address.postcode}
                                    </span>
                                )
                                : (
                                    <span className="contact-item">
                                        {selectUser.plan_user.primary_address.address.address_1}
                                        ,
                                        {' '}
                                        {selectUser.plan_user.primary_address.address.address_2}
                                        ,
                                        {' '}
                                        {selectUser.plan_user.primary_address.address.city}
                                        ,
                                        {' '}
                                        {selectUser.plan_user.primary_address.address.postcode}
                                    </span>
                                )

                        }
                    </div>
                </div>
            );
        }

        return null;
    };

    renderPartnerModal() {
        const { showPartnerModal, isSubmitted } = this.state;
        const { selectUserProfile } = this.props;
        return (
            <AdviserPartnerModal
                modalTitle={`Add partner to ${selectUserProfile.first_name}’s plan`}
                isSubmitted={isSubmitted}
                visible={showPartnerModal}
                handleSubmit={this.handleSubmit}
                handleClose={this.handleClosePartnerModal}
            />
        );
    }

    renderDependent() {
        const { child, dependent } = this.props;
        const allDep = [];
        if (child !== null) {
            child.map((item) => {
                allDep.push(`${item.profile.first_name}, ${getPersonAge(item.profile.dob)} <span>(child)</span>`);
                return null;
            });
        }
        if (dependent !== null) {
            dependent.map((item) => {
                const relationship = item.plan_user_type === 'other' ? item.plan_user_type_other : item.plan_user_type;
                allDep.push(`${item.profile.first_name} ${item.profile.last_name}, ${getPersonAge(item.profile.dob)} <span>(${relationship})</span>`);
                return null;
            });
        }
        if (allDep.length === 0) {
            return (<span className="info-item no-info">No dependants added</span>);
        }
        return allDep.map((item, i) => (
            <span key={i.toString()} className="info-item" dangerouslySetInnerHTML={{ __html: item }} />
        ));
    }

    render() {
        const {
            history, selectUser, selectUserProfile, partner,
        } = this.props;
        const { contactsAreOpened, contactsAreOpenedPartner, openTextArea } = this.state;
        return (
            <section className="adviser-user-plan">
                <div className="right-column col span-4 dis-f fd-c">
                    <div className="controls mb-medium">
                        <div
                            role="presentation"
                            className="button-back dis-f"
                            onClick={() => history.goBack()}
                        >
                            <img
                                src="/static/img/icons/arrow-back.svg"
                                alt="Back"
                                title="Back"
                            />
                            <span>
                                Back to Profile
                            </span>
                        </div>
                    </div>
                    {selectUser.plan_user
                    && (
                        <Fragment>
                            <div className="user-info">
                                <img
                                    className="user-info-icon mb-1rem"
                                    src="/static/img/icons/adviser-user.svg"
                                    alt="John Smith"
                                    title="John Smith"
                                />
                                <span className="user-name mb-0-5rem">
                                    {selectUserProfile.first_name}
                                    {' '}
                                    {selectUserProfile.last_name}
                                </span>
                                <span className="info-item">
                                    {moment().diff(selectUserProfile.dob, 'years')}
                                    {' '}
years old |
                                    {moment(selectUserProfile.dob).format('MMM Do YY')}
                                </span>

                                {
                                    selectUser.plan_user.retirement_age !== null
                                        ? <span className="info-item">{selectUser.plan_user.retirement_age}</span>
                                        : <span className="info-item no-info">No retirement age</span>
                                }
                                <span
                                    role="presentation"
                                    onClick={() => this.setState(prevState => ({ contactsAreOpened: !prevState.contactsAreOpened }))}
                                    className="section-label ai-c mb-0-5rem mt-0-5rem cursor-p"
                                >
                            Contacts
                                    <img
                                        src={`/static/img/stre-${contactsAreOpened ? 'up' : 'down'}.svg`}
                                        alt={contactsAreOpened ? 'Close' : 'Open'}
                                        title={contactsAreOpened ? 'Close' : 'Open'}
                                    />
                                </span>
                                {this.renderContacts(contactsAreOpened)}
                            </div>
                            <div className="user-info">
                                <div className="hr mt-small mb-small" />
                            </div>
                        </Fragment>
                    )
                    }
                    <div className="ai-c user-info">
                        {
                            partner === null
                                ? (
                                    <Fragment>
                                        <span className="section-label jc-sb">
                                        Client 2
                                            <button
                                                onClick={() => this.setState(prevState => ({ showPartnerModal: !prevState.showPartnerModal }))}
                                                className="btn button-add"
                                                type="button"
                                            >
                                        + Add partner
                                            </button>
                                        </span>
                                        <span className="info-item no-info">No partner added</span>
                                    </Fragment>
                                )
                                : (
                                    <Fragment>
                                        <span className="section-label ai-c jc-sb">
                                        Client 2
                                        </span>
                                        <span className="info-item">
                                            {partner.profile.first_name}
                                            {' '}
                                            {partner.profile.last_name}
                                        </span>
                                        <span className="info-item">
                                            {moment().diff(partner.profile.dob, 'years')}
                                            {' '}
years old |
                                            {moment(partner.profile.dob).format('MMM Do YY')}
                                        </span>
                                        {
                                            partner.retirement_age !== null
                                                ? <span className="info-item">{partner.retirement_age}</span>
                                                : <span className="info-item no-info">No retirement age</span>
                                        }
                                        <span
                                            role="presentation"
                                            onClick={() => this.setState(prevState => ({ contactsAreOpenedPartner: !prevState.contactsAreOpenedPartner }))}
                                            className="section-label ai-c mb-0-5rem mt-0-5rem cursor-p"
                                        >
                                            Contacts
                                            <img
                                                src={`/static/img/stre-${contactsAreOpenedPartner ? 'up' : 'down'}.svg`}
                                                alt={contactsAreOpenedPartner ? 'Close' : 'Open'}
                                                title={contactsAreOpenedPartner ? 'Close' : 'Open'}
                                            />
                                        </span>
                                        {this.renderContactsPartner(contactsAreOpenedPartner)}
                                    </Fragment>
                                )
                        }
                    </div>
                    {this.renderPartnerModal()}
                    <div className="user-info">
                        <div className="hr mt-small mb-small" />
                    </div>
                    <div className="user-info">
                        <span className="section-label ai-c jc-sb">
                            Dependants
                        </span>
                        {this.renderDependent()}
                    </div>
                    <div className="user-info">
                        <div className="hr mt-small mb-small" />
                    </div>
                    <div className="user-info">
                        <span className="section-label ai-c jc-sb">
                            Terms of business
                        </span>
                        <span className="info-item no-info">
                            No terms accepted
                        </span>
                    </div>
                    <div className="user-info">
                        <div className="hr mt-small mb-small" />
                    </div>
                    <div className="user-info">
                        <span className="section-label ai-c jc-sb">
                            Risk profile
                        </span>
                        <span className="info-item no-info">
                            No risk profile
                        </span>
                    </div>
                    <div className="user-info">
                        <div className="hr mt-small mb-small" />
                    </div>
                    <div className="user-info">
                        <div className="section-label dis-f jc-sb ai-c mb-medium">
                            <span>
                                KYC / AML status
                            </span>
                            <span>
                                View changelog
                            </span>
                        </div>
                        <InfoRowTable
                            data={{
                                fieldName: 'TS',
                                fieldValue: '--',
                            }}
                        />
                        <InfoRowTable
                            data={{
                                fieldName: 'WC',
                                fieldValue: '--',
                            }}
                        />
                        <InfoRowTable
                            data={{
                                fieldName: 'HTAR',
                                fieldValue: '--',
                            }}
                        />
                        <button
                            className="btn check-status mt-1rem"
                            type="button"
                        >
                            Start AML/KYC checks
                        </button>
                    </div>
                </div>
                <div className="left-column col span-8 ml-large">
                    <h3>
                        Wealth plan
                    </h3>
                    <InfoRow
                        data={{
                            surveys: 'Surveys',
                            dateSubmitted: 'Date submitted',
                            status: 'Status',
                            actions: 'Actions',
                        }}
                    />
                    <InfoRow
                        data={{
                            surveys: 'Discovery survey',
                            dateSubmitted: '-',
                            status: 'Not sent',
                            actions: 'Send',
                        }}
                    />
                    <InfoRow
                        data={{
                            surveys: 'Advice survey',
                            dateSubmitted: '-',
                            status: 'Not sent',
                            actions: 'Send',
                        }}
                    />
                    <div className="soft-facts dis-f jc-sb ai-c mt-xlarge">
                        <h3>
                            Soft facts
                        </h3>
                        <button
                            type="button"
                            className="btn_simple"
                            onClick={() => this.setState(prevState => ({ openTextArea: !prevState.openTextArea }))}
                        >
                            + Add soft facts
                        </button>
                    </div>
                    <SoftFacts openTextArea={openTextArea} closeSoftFact={val => this.setState({ openTextArea: val })} />
                </div>
            </section>
        );
    }
}

UserPlan.propTypes = {
    history: PropTypes.object.isRequired,
    handleGetUserByID: PropTypes.func.isRequired,
    handleGetSoftFact: PropTypes.func.isRequired,
    handleGetMe: PropTypes.func.isRequired,
    planUUID: PropTypes.string,
    userUUID: PropTypes.string,
    selectUser: PropTypes.object,
    selectUserProfile: PropTypes.object,
    partner: PropTypes.object,
    child: PropTypes.arrayOf(PropTypes.object),
    dependent: PropTypes.arrayOf(PropTypes.object),
};

UserPlan.defaultProps = {
    planUUID: null,
    userUUID: null,
    selectUser: {},
    selectUserProfile: {},
    partner: null,
    child: null,
    dependent: null,
};

const initializeForm = reduxForm({
    form: 'userPlan',
    enableReinitialize: true,
})(UserPlan);

const mapStateToProps = (state, props) => {
    const planUUID = get(state, 'users.selectedUser.active_plan', null);
    return ({
        users: get(state, 'users.items', []),
        me: get(state, 'users.me', []),
        selectUser: get(state, 'users.selectedUser', {}),
        selectUserProfile: get(state, 'users.selectedUser.profile', {}),
        currentID: get(state, 'users.me.profile.profile_id', null),
        formValue: get(state, 'form.adviserDashboard', null),
        planUUID,
        userUUID: get(props, 'match.params.plan_uuid', null),
        partner: get(state, `users.selectedUser.connected_persons.${planUUID}.partner`, null),
        child: get(state, `users.selectedUser.connected_persons.${planUUID}.child`, null),
        dependent: get(state, `users.selectedUser.connected_persons.${planUUID}.dependent`, null),
    });
};

const mapDispatchToProps = dispatch => ({
    handleGetMe: bindActionCreators(getMe, dispatch),
    handleGetUserByID: bindActionCreators(getUser, dispatch),
    handleGetSoftFact: bindActionCreators(getSoftFacts, dispatch),
    changeFieldValue: (field, value) => {
        dispatch(change('adviserDashboard', field, value));
    },
});

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(initializeForm));
