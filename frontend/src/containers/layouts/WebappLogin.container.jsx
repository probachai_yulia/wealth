import React, { Component } from 'react';
import PropTypes from 'prop-types';

import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';

import { LoadableHeader, LoadableSpinner } from '../../components/Loadable';

// import {getLogin, ReloadPlan, saveData} from '../../actions/WebappActions';

class Layout extends Component {
    render() {
        const {
            children,
            classPage,
            menuHeader,
            isLoading,
        } = this.props;

        return (
            <section className={`${classPage} LoginPage`}>
                <LoadableHeader menu_header={menuHeader} isAuthorized={false} />
                {React.cloneElement(children)}
                <LoadableSpinner isLoading={isLoading !== 0} />
            </section>
        );
    }
}

Layout.propTypes = {
    isLoading: PropTypes.number,
    children: PropTypes.object.isRequired,
    classPage: PropTypes.string,
    menuHeader: PropTypes.string,
};
Layout.defaultProps = {
    isLoading: 0,
    classPage: '',
    menuHeader: '',
};

const mapStateToProps = state => ({
    user: state.user,
    isAuthorized: state.isAuthorized,
    isLoading: state.common.isLoading,
});

const mapDispatchToProps = () => ({
});
// noinspection JSUnusedGlobalSymbols
export default withRouter(connect(mapStateToProps, mapDispatchToProps)(Layout));
