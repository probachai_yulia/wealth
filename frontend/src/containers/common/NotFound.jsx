import React, { Component } from 'react';

/**
 * Render the content for user idle. Dumb component
 *
 */
class NotFound extends Component {
    render() {
        return (
            <section className="set-password container fullHeight">
                <h1 className="ta-c">Page not found</h1>
            </section>
        );
    }
}

export default NotFound;
