import React, { Component, Fragment } from 'react';
import PropTypes from 'prop-types';
import get from 'lodash/get';
import _isArray from 'lodash/isArray';
import { withRouter } from 'react-router-dom';
import connect from 'react-redux/es/connect/connect';
import { Field } from 'redux-form';
import RadioButtonVerticalList from './RadioButtonVerticalList';
import { RISK_TOLERANCE } from '../helpers/constants';
import { requiredYesNo } from '../utils/validation.helper';

class RiskToleranceQuestion extends Component {
    constructor(props) {
        super(props);
        this.renderQuestion = this.renderQuestion.bind(this);
    }

    renderQuestion() {
        const { step } = this.props;

        if (typeof RISK_TOLERANCE[step].table !== 'undefined') {
            return (
                <Fragment>
                    <div dangerouslySetInnerHTML={{ __html: RISK_TOLERANCE[step].text }} />
                    <table className="docs-table ta-c">
                        <tbody>
                            {
                                RISK_TOLERANCE[step].table.map((item, i) => (
                                    <Fragment key={i.toString()}>
                                        {
                                            _isArray(item.text)
                                                ? item.text.map((subitem, f) => (
                                                    <tr key={f.toString()}>
                                                        {
                                                            subitem.map((subs, index) => (
                                                                // eslint-disable-next-line max-len
                                                                <td key={index.toString()} colSpan={item.colspan} className="ta-c">
                                                                    {subs}
                                                                </td>
                                                            ))
                                                        }
                                                    </tr>
                                                ))
                                                : (
                                                    <tr>
                                                        <td colSpan={item.colspan} className="ta-c">
                                                            {item.text}
                                                        </td>
                                                    </tr>
                                                )
                                        }
                                    </Fragment>
                                ))
                            }
                        </tbody>
                    </table>
                </Fragment>
            );
        }
        return (
            <div dangerouslySetInnerHTML={{ __html: RISK_TOLERANCE[step].text }} />
        );
    }

    render() {
        const {
            isSubmitted, name, formValue, step, extraClass, extraClasses, onNextStep,
        } = this.props;

        const valSel = get(formValue, `values.${name}`, '0');

        return (
            <section className={`${extraClasses} single_question`}>
                {this.renderQuestion()}
                <Field
                    component={RadioButtonVerticalList}
                    blocks={RISK_TOLERANCE[step].answers}
                    name={name}
                    fieldsName={name}
                    extraClass={extraClass}
                    selectedValue={valSel}
                    isShowErrors={isSubmitted}
                    validate={[
                        requiredYesNo,
                    ]}
                    onNextStep={onNextStep}
                />
            </section>
        );
    }
}

RiskToleranceQuestion.propTypes = {
    isSubmitted: PropTypes.bool,
    name: PropTypes.string,
    extraClasses: PropTypes.string,
    formValue: PropTypes.object,
    extraClass: PropTypes.string,
    step: PropTypes.number.isRequired,
    onNextStep: PropTypes.func,
};
RiskToleranceQuestion.defaultProps = {
    onNextStep: null,
    name: '',
    extraClass: '',
    extraClasses: '',
    isSubmitted: false,
    formValue: {},
};

const mapStateToProps = (state, { formName }) => ({
    formValue: get(state, `form.${formName}`, {}),
});

const mapDispatchToProps = () => ({});

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(RiskToleranceQuestion));
