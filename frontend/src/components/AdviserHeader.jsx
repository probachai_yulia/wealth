import React from 'react';

export const AdviserHeader = () => (
    <section className="adviser-header">
        <section className="purple main-block dis-f ai-c">
            <div className="title fw-n">
                <img
                    src="/static/img/Octopus.svg"
                    alt="Octopus"
                    title="Octopus"
                />
            </div>
        </section>
        <section className="bottom-shadow" />
    </section>
);
