/* eslint-disable max-len */
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { withRouter } from 'react-router';
import connect from 'react-redux/es/connect/connect';
import LetStarted from './LetStarted';
import WhatWillCost from './WhatWillCost';
import WhatWillCostBox from './WhatWillCostBox';


class Text extends Component {
    constructor(props) {
        super(props);

        this.handleChangeArray = this.handleChangeArray.bind(this);
        this.handleChageText = this.handleChageText.bind(this);
    }

    handleChangeArray(labelId, arrayId, arrayName, e) {
        const { editText, label1 } = this.props;
        let value = e.target.value.toString();
        let priceText;
        let labId = labelId;
        let item;
        if (arrayName === 'discount') {
            if (e.target.value === 'discount') {
                labId = 1;
                value = 'This covers the up-front work to set up your account with us, build your LifeLine, conduct a full analysis of your existing arrangements and make our initial recommendations.<br/><br/>As one of our early adopter clients, I have agreed that we will be waiving this fee to you.';
                priceText = '<span style="text-decoration: line-through;">£2,000</span> £0';
            } else {
                labId = 1;
                value = 'This covers the up-front work to set up your account with us, build your LifeLine, conduct a full analysis of your existing arrangements and make our initial recommendations.';
                priceText = '£2,000';
            }
            editText(labId, arrayId, 'multi', `${priceText}//${value}`);
        } else if (arrayName === 'price') {
            item = JSON.parse(label1);
            let price = item[0].price.replace('<span style="text-decoration: line-through;">', '');
            price = price.replace('</span>', '');
            priceText = price.split(' ');
            priceText = `<span style="text-decoration: line-through;">${e.target.value}</span> ${priceText[1]}`;
            editText(labId, arrayId, 'price', priceText);
        } else if (arrayName === 'price_discount') {
            item = JSON.parse(label1);
            let price = item[0].price.replace('<span style="text-decoration: line-through;">', '');
            price = price.replace('</span>', '');
            priceText = price.split(' ');
            priceText = `<span style="text-decoration: line-through;">${priceText[0]}</span> ${e.target.value}`;
            editText(labId, arrayId, 'price', priceText);
        } else {
            value = value.replace(/\r?\n/g, '<br>');
            editText(labId, arrayId, arrayName, value);
        }
    }

    handleChageText(val) {
        const { label } = this.props;
        label(val);
    }

    render() {
        const {
            label1,
            label2,
            label3,
            label4,
            chartsType,
            width,
            height,
            edit,
            advisor,
        } = this.props;
        const {
            handleChangeArray,
            handleChageText,
        } = this;
        return (

            <div
                className={`textPresentation textSection text_version_${chartsType}`}
                style={{
                    width,
                    height,
                }}
            >
                {
                    chartsType === 'what_will_cost_box'
                    && (
                        <WhatWillCostBox
                            label1={label1}
                            label2={label2}
                            label3={label3}
                            label4={label4}
                            edit={edit}
                            handleChangeArray={(id, i, type, e) => handleChangeArray(id, i, type, e)}
                        />
                    )
                }
                {
                    chartsType === 'what_will_cost'
                    && (
                        <WhatWillCost
                            label1={label1}
                            label2={label2}
                            label3={label3}
                            edit={edit}
                            handleChangeArray={(id, i, type, e) => handleChangeArray(id, i, type, e)}
                        />
                    )
                }
                {
                    chartsType === 'let_started'
                        && (
                            <LetStarted
                                edit={edit}
                                handleChageText={e => handleChageText(e)}
                                value={label1}
                                advisor={advisor}
                            />
                        )
                }
            </div>
        );
    }
}

Text.propTypes = {
    label: PropTypes.func,
    width: PropTypes.number,
    height: PropTypes.number,
    edit: PropTypes.bool,
    chartsType: PropTypes.string,
    label1: PropTypes.string,
    label2: PropTypes.string,
    label3: PropTypes.string,
    label4: PropTypes.string,
    editText: PropTypes.func,
    advisor: PropTypes.string,
};

Text.defaultProps = {
    label: () => {},
    editText: () => {},
    label1: '',
    label2: '',
    label3: '',
    label4: '',
    chartsType: '',
    width: 1440,
    height: 810,
    edit: false,
    advisor: '',
};

export default withRouter(connect()(Text));
