import React, { Component } from 'react';
import Dropzone from 'react-dropzone';
import PropTypes from 'prop-types';

class UploadImage extends Component {
    render() {
        const {
            extraClasses,
        } = this.props;
        let dropzoneRef;
        return (
            <div className={`form-inline ${extraClasses}`}>
                <Dropzone
                    className="wrap-dropzone"
                    ref={(node) => {
                        dropzoneRef = node;
                    }}
                >
                    <p className="greyText">Drop documents here or click to upload</p>
                    <button
                        className="btn small btn-border-purple"
                        type="button"
                        onClick={() => {
                            dropzoneRef.open();
                        }}
                    >
                        Choose files
                    </button>
                </Dropzone>

            </div>
        );
    }
}

UploadImage.propTypes = {
    extraClasses: PropTypes.string,
};

UploadImage.defaultProps = {
    extraClasses: '',
};

export default UploadImage;
