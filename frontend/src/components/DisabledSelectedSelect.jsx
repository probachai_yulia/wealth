import React, { Component } from 'react';
import PropTypes from 'prop-types';

class DisabledSelectedSelect extends Component {
    render() {
        const {
            extraClasses, label, selectedValue, placeholder,
        } = this.props;
        return (
            <section className={`form-inline col ${extraClasses}`}>
                <label className="input-label">
                    {label}
                </label>
                <select disabled className="select-comp">
                    <option defaultValue value={selectedValue}>
                        {placeholder}
                    </option>
                </select>
            </section>
        );
    }
}

DisabledSelectedSelect.propTypes = {
    label: PropTypes.string.isRequired,
    selectedValue: PropTypes.string,
    placeholder: PropTypes.string,
    extraClasses: PropTypes.string,
};

DisabledSelectedSelect.defaultProps = {
    placeholder: '',
    selectedValue: '',
    extraClasses: '',
};

export default DisabledSelectedSelect;
