import React, { Component, Fragment } from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router';
import PropTypes from 'prop-types';
import { newData } from '../actions/WebappButtonActions';


class ModalNewDataWebapp extends Component {
    constructor(props) {
        super(props);

        this.renderAction = this.renderAction.bind(this);
    }

    loginAction = () => {
        localStorage.setItem('token', '');
        window.location.href = '/webapp/login';
    };

    renderAction() {
        const {
            toSave, dismiss, type, link, location,
        } = this.props;

        if (type === 'login') {
            return (
                <div className="button_action">
                    <button type="button" onClick={() => this.loginAction()} className="primary_button">
                        LOGIN
                    </button>
                    <button type="button" onClick={() => toSave('login')} className="alert_button">
                        SAVE & LOGIN
                    </button>
                    <button type="button" onClick={() => dismiss()} className="primary_button">
                        CLOSE
                    </button>
                </div>
            );
        } if (type === 'wrong_plan') {
            return (
                <div className="button_action">
                    <a href={link} className="primary_button">
                        GO TO GOOGLE SHEET
                    </a>
                </div>
            );
        }

        return (
            <div className="button_action">
                <button type="button" onClick={() => location.reload()} className="primary_button">
                    RELOAD
                </button>
                <button type="button" onClick={() => toSave()} className="alert_button">
                    SAVE & RELOAD
                </button>
            </div>
        );
    }

    renderPopup() {
        const { newDataFromProps, title, text } = this.props;

        if (newDataFromProps) {
            return (
                <div id="wrap_user_change">
                    <div id="user_change">
                        <h4>
                            {title}
                        </h4>
                        <p dangerouslySetInnerHTML={{ __html: text }} />
                        {this.renderAction()}
                    </div>
                </div>
            );
        }
        return null;
    }

    render() {
        return (
            <Fragment>
                {this.renderPopup()}
            </Fragment>
        );
    }
}


ModalNewDataWebapp.propTypes = {
    newDataFromProps: PropTypes.bool,
    title: PropTypes.string,
    text: PropTypes.string,
    type: PropTypes.string,
    dismiss: PropTypes.func,
    link: PropTypes.string,
    toSave: PropTypes.func,
    location: PropTypes.object.isRequired,
};
ModalNewDataWebapp.defaultProps = {
    title: '',
    text: '',
    type: '',
    link: '',
    newDataFromProps: false,
    dismiss: null,
    toSave: null,
};

const mapStateToProps = state => ({
    ...state,
});

const mapDispatchToProps = dispatch => ({
    toSave: type => dispatch(newData(type)),
});

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(ModalNewDataWebapp));
