/**
 * Options actions
 *
 */

// Getting options
export const GET_OPTIONS = 'GET_OPTIONS';
export const GET_OPTIONS_ERROR = 'GET_OPTIONS_ERROR';

// Setting options
export const SET_OPTIONS = 'SET_OPTIONS';
export const SET_OPTIONS_ERROR = 'SET_OPTIONS_ERROR';
