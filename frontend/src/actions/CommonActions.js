import { createActions } from 'redux-actions';
import {
    CHECK_ACCESS,
    DEFERRED_REDIRECT,
    LOCAL_ID,
    REQUEST_RECEIVED_DONE,
    REQUEST_RECEIVED_ERROR,
    REQUESTS_RESET,
    // SET_COMMON,
    SPINNER_OFF,
    SPINNER_ON,
} from './types/CommonTypes';

export const {
    spinnerOn,
    spinnerOff,
    checkAccess,
    requestsReset,
    requestReceivedDone,
    requestReceivedError,
    deferredRedirect,
    localId,
} = createActions({
    [SPINNER_ON]: payload => payload,
    [SPINNER_OFF]: payload => payload,
    [CHECK_ACCESS]: payload => payload,
    [REQUESTS_RESET]: payload => payload,
    [REQUEST_RECEIVED_DONE]: payload => payload,
    [REQUEST_RECEIVED_ERROR]: payload => payload,
    [DEFERRED_REDIRECT]: payload => payload,
    [LOCAL_ID]: payload => payload,
});
