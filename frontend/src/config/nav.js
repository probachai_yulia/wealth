export default {
    octopus_platform: [
        {
            path: '/terms-and-conditions',
            name: 'Terms & Conditions',
        },
        {
            path: '/octopus-gia',
            name: 'Octopus GIA',
        },
        {
            path: '/octopus-isa',
            name: 'Octopus ISA',
        },
        {
            path: '/octopus-sipp',
            name: 'Octopus SIPP',
        },
    ],
    advisors: [
        {
            path: '/our-service',
            name: 'Our Service',
        },
        {
            path: '/terms-of-business',
            name: 'Terms of Business',
        },
        {
            path: '/privacy-policy',
            name: 'Privacy Policy',
        },
        {
            path: '/complaints',
            name: 'Complaints',
        },
    ],
    website: [
        {
            path: '/our-service',
            name: 'Our Service',
        },
        {
            path: '/terms-of-business',
            name: 'Terms of Business',
        },
        {
            path: '/privacy-policy',
            name: 'Privacy Policy',
        },
        {
            path: '/complaints',
            name: 'Complaints',
        },
    ],
    guides: [
        {
            path: '/guides',
            name: 'Guides',
        },
    ],
    webapp: [
        {
            path: '/webapp/search-client',
            name: 'Search Client',
        },
        {
            path: '/webapp/configuration',
            name: 'Icon Setting',
        },
    ],
};
