import Loadable from 'react-loadable';
import { Loading } from '../../components/Loading';

// LAYOUT

const AdviserLayout = Loadable({
    loader: () => import('../../containers/layouts/AdviserLayout.container' /* webpackChunkName: "adviser-layout" */),
    loading: Loading,
});

// ADVISER PORTAL

const AdviserDashboard = Loadable({
    loader: () => import('../../containers/surveys/AdviserDashboard' /* webpackChunkName: "adviser-dashboard" */),
    loading: Loading,
});

const AdviserClient = Loadable({
    loader: () => import('../../containers/surveys/adviser/User' /* webpackChunkName: "adviser-client" */),
    loading: Loading,
});

const AdviserInternalUser = Loadable({
    loader: () => import('../../containers/surveys/adviser/InternalUser' /* webpackChunkName: "adviser-internal-user" */),
    loading: Loading,
});

const AdviserUserPlan = Loadable({
    loader: () => import('../../containers/surveys/adviser/UserPlan' /* webpackChunkName: "adviser-user-plan" */),
    loading: Loading,
});

const classPage = 'htmlPage';

export default [
    {
        exact: true,
        layout: AdviserLayout,
        path: '/adviser/dashboard',
        component: AdviserDashboard,
        menu: '',
        menu_footer: '',
        classPage,
    },
    {
        exact: true,
        layout: AdviserLayout,
        path: '/adviser/user/:profile_uuid',
        component: AdviserClient,
        menu: '',
        menuFooter: '',
        classPage,
    },
    {
        exact: true,
        layout: AdviserLayout,
        path: '/adviser/internal-user/:profile_uuid',
        component: AdviserInternalUser,
        menu: '',
        menuFooter: '',
        classPage,
    },
    {
        exact: true,
        layout: AdviserLayout,
        path: '/adviser/plan/:plan_uuid',
        component: AdviserUserPlan,
        menu: '',
        menuFooter: '',
        classPage,
    },
];
