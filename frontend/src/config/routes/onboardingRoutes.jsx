import Loadable from 'react-loadable';
import { Loading } from '../../components/Loading';

// LAYOUT

const LoginLayout = Loadable({
    loader: () => import('../../containers/layouts/LoginLayout.container' /* webpackChunkName: "login-layout" */),
    loading: Loading,
});

const OnboardLayout = Loadable({
    loader: () => import('../../containers/layouts/OnboardLayout.container' /* webpackChunkName: "onboard-layout" */),
    loading: Loading,
});

// ONBOARDING

const OnboardLogin = Loadable({
    loader: () => import('../../containers/surveys/onboarding/OnboardLogin' /* webpackChunkName: "onboard-login" */),
    loading: Loading,
});

const OnboardingStep1 = Loadable({
    loader: () => import('../../containers/surveys/onboarding/Step1' /* webpackChunkName: "step-1" */),
    loading: Loading,
});

const OnboardingStep2 = Loadable({
    loader: () => import('../../containers/surveys/onboarding/Step2' /* webpackChunkName: "step-2" */),
    loading: Loading,
});

const OnboardingStep3 = Loadable({
    loader: () => import('../../containers/surveys/onboarding/Step3' /* webpackChunkName: "step-3" */),
    loading: Loading,
});

const classPage = 'htmlPage';

export default [
    /* HOMEPAGE - WEBSITE */
    {
        exact: true,
        layout: LoginLayout,
        path: '/onboarding/login',
        component: OnboardLogin,
        menu: '',
        menuFooter: '',
        classPage,
    },
    {
        exact: true,
        layout: OnboardLayout,
        path: '/onboarding/step-1',
        component: OnboardingStep1,
        menu: '',
        menuFooter: '',
        classPage,
    },
    {
        exact: true,
        layout: OnboardLayout,
        path: '/onboarding/step-2',
        component: OnboardingStep2,
        menu: '',
        menuFooter: '',
        classPage,
    },
    {
        exact: true,
        layout: OnboardLayout,
        path: '/onboarding/step-3',
        component: OnboardingStep3,
        menu: '',
        menuFooter: '',
        classPage,
    },
];
