import Loadable from 'react-loadable';
import { Loading } from '../../components/Loading';

// LAYOUT

const Layout = Loadable({
    loader: () => import('../../containers/layouts/Layout.container' /* webpackChunkName: "layout", */),
    loading: Loading,
});

const SimpleLayout = Loadable({
    loader: () => import('../../containers/layouts/SimpleLayout.container' /* webpackChunkName: "simple-layout", */),
    loading: Loading,
});

// DISCOVERY

const SurveyDashboard = Loadable({
    loader: () => import('../../containers/surveys/SurveyDashboard' /* webpackChunkName: "survey-dashboard", */),
    loading: Loading,
});

const HandyHints = Loadable({
    loader: () => import('../../containers/surveys/HandyHints' /* webpackChunkName: "handy-hints" */),
    loading: Loading,
});

// DISCOVERY - FAMILY

const KnowYou = Loadable({
    loader: () => import('../../containers/surveys/discovery/family/KnowYou' /* webpackChunkName: "know-you" */),
    loading: Loading,
});

const AboutPartner = Loadable({
    // eslint-disable-next-line max-len
    loader: () => import('../../containers/surveys/discovery/family/AboutPartner' /* webpackChunkName: "about-partner" */),
    loading: Loading,
});

const AboutChildren = Loadable({
    // eslint-disable-next-line max-len
    loader: () => import('../../containers/surveys/discovery/family/AboutChildren' /* webpackChunkName: "about-children" */),
    loading: Loading,
});

const AboutDependents = Loadable({
    // eslint-disable-next-line max-len
    loader: () => import('../../containers/surveys/discovery/family/AboutDependents' /* webpackChunkName: "about-dependents" */),
    loading: Loading,
});

const AddDependents = Loadable({
    // eslint-disable-next-line max-len
    loader: () => import('../../containers/surveys/discovery/family/AddDependents' /* webpackChunkName: "add-dependents" */),
    loading: Loading,
});

const AddJob = Loadable({
    loader: () => import('../../containers/surveys/discovery/family/AddJob' /* webpackChunkName: "add-job", */),
    loading: Loading,
});
const AddChildren = Loadable({
    // eslint-disable-next-line max-len
    loader: () => import('../../containers/surveys/discovery/family/AddChildren' /* webpackChunkName: "add-children" */),
    loading: Loading,
});

const Work = Loadable({
    loader: () => import('../../containers/surveys/discovery/family/Work' /* webpackChunkName: "work" */),
    loading: Loading,
});

const Retirement = Loadable({
    loader: () => import('../../containers/surveys/discovery/family/Retirement' /* webpackChunkName: "retirement" */),
    loading: Loading,
});

// DISCOVERY - FINANCE

const Assets = Loadable({
    loader: () => import('../../containers/surveys/discovery/finance/Assets' /* webpackChunkName: "assets" */),
    loading: Loading,
});

const AddCashSavingsAccount = Loadable({
    // eslint-disable-next-line max-len
    loader: () => import('../../containers/surveys/discovery/finance/AddCashSavingsAccount' /* webpackChunkName: "add-cash-savings-account" */),
    loading: Loading,
});

const AboutCashSavingsAccount = Loadable({
    // eslint-disable-next-line max-len
    loader: () => import('../../containers/surveys/discovery/finance/AboutCashSavingsAccount' /* webpackChunkName: "about-cash-savings-account" */),
    loading: Loading,
});

const AddPensions = Loadable({
    // eslint-disable-next-line max-len
    loader: () => import('../../containers/surveys/discovery/finance/AddPensions' /* webpackChunkName: "add-pensions" */),
    loading: Loading,
});

const AboutPensions = Loadable({
    // eslint-disable-next-line max-len
    loader: () => import('../../containers/surveys/discovery/finance/AboutPensions' /* webpackChunkName: "about-pensions" */),
    loading: Loading,
});

const AddISA = Loadable({
    loader: () => import('../../containers/surveys/discovery/finance/AddISA' /* webpackChunkName: "add-isa" */),
    loading: Loading,
});

const AboutISA = Loadable({
    loader: () => import('../../containers/surveys/discovery/finance/AboutISA' /* webpackChunkName: "about-isa" */),
    loading: Loading,
});

const AddInvestments = Loadable({
    // eslint-disable-next-line max-len
    loader: () => import('../../containers/surveys/discovery/finance/AddInvestments' /* webpackChunkName: "add-investments" */),
    loading: Loading,
});

const AboutInvestments = Loadable({
    // eslint-disable-next-line max-len
    loader: () => import('../../containers/surveys/discovery/finance/AboutInvestments' /* webpackChunkName: "about-investments" */),
    loading: Loading,
});

const AddProperty = Loadable({
    // eslint-disable-next-line max-len
    loader: () => import('../../containers/surveys/discovery/finance/AddProperty' /* webpackChunkName: "add-investments" */),
    loading: Loading,
});

const AboutProperty = Loadable({
    // eslint-disable-next-line max-len
    loader: () => import('../../containers/surveys/discovery/finance/AboutProperty' /* webpackChunkName: "about-investments" */),
    loading: Loading,
});

// DISCOVERY - INCOME

const SalaryIncome = Loadable({
    // eslint-disable-next-line max-len
    loader: () => import('../../containers/surveys/discovery/income/SalaryIncome' /* webpackChunkName: "salary-income" */),
    loading: Loading,
});

const AddSalary = Loadable({
    loader: () => import('../../containers/surveys/discovery/income/AddSalary' /* webpackChunkName: "add-salary" */),
    loading: Loading,
});

const AddAdditionalIncome = Loadable({
    // eslint-disable-next-line max-len
    loader: () => import('../../containers/surveys/discovery/income/AddAdditionalIncome' /* webpackChunkName: "add-salary" */),
    loading: Loading,
});

const IncomeChanges = Loadable({
    // eslint-disable-next-line max-len
    loader: () => import('../../containers/surveys/discovery/income/IncomeChanges' /* webpackChunkName: "add-salary" */),
    loading: Loading,
});

const AdditionalIncome = Loadable({
    // eslint-disable-next-line max-len
    loader: () => import('../../containers/surveys/discovery/income/AdditionalIncome' /* webpackChunkName: "add-salary" */),
    loading: Loading,
});

// DISCOVERY - CHARITY

const AddCharity = Loadable({
    // eslint-disable-next-line max-len
    loader: () => import('../../containers/surveys/discovery/charity/AddCharity' /* webpackChunkName: "about-property" */),
    loading: Loading,
});

// DISCOVERY - DEBTS

const AssetsDebts = Loadable({
    loader: () => import('../../containers/surveys/discovery/debts/Assets' /* webpackChunkName: "assets" */),
    loading: Loading,
});

const AddMortgage = Loadable({
    loader: () => import('../../containers/surveys/discovery/debts/AddMortgage' /* webpackChunkName: "add-mortgage" */),
    loading: Loading,
});

const AboutMortgage = Loadable({
    // eslint-disable-next-line max-len
    loader: () => import('../../containers/surveys/discovery/debts/AboutMortgage' /* webpackChunkName: "about-mortgage" */),
    loading: Loading,
});

const AddLoans = Loadable({
    loader: () => import('../../containers/surveys/discovery/debts/AddLoans' /* webpackChunkName: "add-loans" */),
    loading: Loading,
});

const AboutLoans = Loadable({
    loader: () => import('../../containers/surveys/discovery/debts/AboutLoans' /* webpackChunkName: "about-loans" */),
    loading: Loading,
});

const AddCreditCard = Loadable({
    // eslint-disable-next-line max-len
    loader: () => import('../../containers/surveys/discovery/debts/AddCreditCard' /* webpackChunkName: "add-credit-card" */),
    loading: Loading,
});

const AboutCreditCard = Loadable({
    // eslint-disable-next-line max-len
    loader: () => import('../../containers/surveys/discovery/debts/AboutCreditCard' /* webpackChunkName: "about-credit-card", */),
    loading: Loading,
});

// DISCOVERY - EXPENSES

const AddExpenses = Loadable({
    // eslint-disable-next-line max-len
    loader: () => import('../../containers/surveys/discovery/expenses/AddExpenses' /* webpackChunkName: "add-expenses" */),
    loading: Loading,
});

const AddChildrenExpenses = Loadable({
    // eslint-disable-next-line max-len
    loader: () => import('../../containers/surveys/discovery/expenses/AddChildrenExpenses' /* webpackChunkName: "add-children-expenses" */),
    loading: Loading,
});

const AddChildrenUnderageExpenses = Loadable({
    // eslint-disable-next-line max-len
    loader: () => import('../../containers/surveys/discovery/expenses/AddChildrenUnderageExpenses' /* webpackChunkName: "add-children-expenses" */),
    loading: Loading,
});

const AboutChildrenExpense = Loadable({
    // eslint-disable-next-line max-len
    loader: () => import('../../containers/surveys/discovery/expenses/AboutChildrenExpenses' /* webpackChunkName: "about-children-expenses" */),
    loading: Loading,
});

const AddInsurance = Loadable({
    // eslint-disable-next-line max-len
    loader: () => import('../../containers/surveys/discovery/expenses/AddInsurance' /* webpackChunkName: "add-insurance" */),
    loading: Loading,
});

// DISCOVERY - RISK & TOLERANCE

const AddRiskTolerance = Loadable({
    // eslint-disable-next-line max-len
    loader: () => import('../../containers/surveys/discovery/risk_tolerance/AddRiskTolerance' /* webpackChunkName: "add-risk-tolerance" */),
    loading: Loading,
});

// DISCOVERY - PRODUCT PROVIDERS

const AddProductProviders = Loadable({
    // eslint-disable-next-line max-len
    loader: () => import('../../containers/surveys/discovery/product_providers/AddProductProviders' /* webpackChunkName: "add-product-providers" */),
    loading: Loading,
});

// DISCOVERY - SECONDARY

const Details = Loadable({
    loader: () => import('../../containers/surveys/discovery/details/Details' /* webpackChunkName: "details" */),
    loading: Loading,
});

const DetailsPartner = Loadable({
    // eslint-disable-next-line max-len
    loader: () => import('../../containers/surveys/discovery/details/DetailsPartner' /* webpackChunkName: "details-partner" */),
    loading: Loading,
});

const Valuables = Loadable({
    loader: () => import('../../containers/surveys/discovery/valuables/Valuables' /* webpackChunkName: "Valuables" */),
    loading: Loading,
});

const ValidWill = Loadable({
    // eslint-disable-next-line max-len
    loader: () => import('../../containers/surveys/discovery/valid_will/ValidWill' /* webpackChunkName: "valid-will" */),
    loading: Loading,
});

const PensionProtection = Loadable({
    // eslint-disable-next-line max-len
    loader: () => import('../../containers/surveys/discovery/pension_protection/PensionProtection' /* webpackChunkName: "pension-protection" */),
    loading: Loading,
});

const POA = Loadable({
    loader: () => import('../../containers/surveys/discovery/poa/POA' /* webpackChunkName: "poa" */),
    loading: Loading,
});

const SignificantGift = Loadable({
    // eslint-disable-next-line max-len
    loader: () => import('../../containers/surveys/discovery/significant_gift/SignificantGift' /* webpackChunkName: "significant-gift" */),
    loading: Loading,
});

const GainsLosses = Loadable({
    // eslint-disable-next-line max-len
    loader: () => import('../../containers/surveys/discovery/gains_losses/GainsLosses' /* webpackChunkName: "gains-losses" */),
    loading: Loading,
});

const AddGainsLosses = Loadable({
    // eslint-disable-next-line max-len
    loader: () => import('../../containers/surveys/discovery/gains_losses/AddGainsLosses' /* webpackChunkName: "add-gains-losses" */),
    loading: Loading,
});

const LegacyDetails = Loadable({
    // eslint-disable-next-line max-len
    loader: () => import('../../containers/surveys/discovery/legacy/LegacyDetails' /* webpackChunkName: "legacy-details" */),
    loading: Loading,
});

const LegacyPlans = Loadable({
    // eslint-disable-next-line max-len
    loader: () => import('../../containers/surveys/discovery/legacy/LegacyPlans' /* webpackChunkName: "legacy-plans" */),
    loading: Loading,
});

const PowerOfAttorney = Loadable({
    // eslint-disable-next-line max-len
    loader: () => import('../../containers/surveys/discovery/legacy/PowerOfAttorney' /* webpackChunkName: "power-of-attorney" */),
    loading: Loading,
});

const AddPowerOfAttorney = Loadable({
    // eslint-disable-next-line max-len
    loader: () => import('../../containers/surveys/discovery/legacy/AddPowerOfAttorney' /* webpackChunkName: "add-power-of-attorney" */),
    loading: Loading,
});

// DISCOVERY - SUBMIT FORM

const SubmitForm = Loadable({
    // eslint-disable-next-line max-len
    loader: () => import('../../containers/surveys/discovery/submit_form/SubmitForm' /* webpackChunkName: "submit-form" */),
    loading: Loading,
});

const SuccessSubmitForm = Loadable({
    // eslint-disable-next-line max-len
    loader: () => import('../../containers/surveys/discovery/submit_form/SuccessSubmitForm' /* webpackChunkName: "success-submit" */),
    loading: Loading,
});

const classPage = 'htmlPage';
const classWebapp = 'Webapp';

const stickyMenu = 'sticky';
const titleHeaderFamily = 'You and your Family:';
const titleHeaderFinance = 'Financial Holdings:';

export default [
    /* DISCOVERY */
    {
        exact: true,
        layout: Layout,
        path: '/survey/dashboard',
        component: SurveyDashboard,
        menu: '',
        menu_footer: '',
        menu_position: stickyMenu,
        classPage,
    },
    {
        layout: Layout,
        path: '/survey/handy-hints',
        component: HandyHints,
        classPage: classWebapp,
        title_page: 'Handy hints',
    },
    /* DISCOVERY - FAMILY */
    {
        exact: true,
        layout: Layout,
        path: '/survey/family/know-you',
        component: KnowYou,
        menu: '',
        menu_footer: '',
        menu_position: stickyMenu,
        classPage,
        title_header: `${titleHeaderFamily} Getting to know you`,
    },
    {
        exact: true,
        layout: Layout,
        path: '/survey/family/about-partner',
        component: AboutPartner,
        menu: '',
        menu_footer: '',
        menu_position: stickyMenu,
        classPage,
        title_header: `${titleHeaderFamily} Partner`,
    },
    {
        exact: true,
        layout: Layout,
        path: '/survey/family/about-children',
        component: AboutChildren,
        menu: '',
        menu_footer: '',
        menu_position: stickyMenu,
        classPage,
        title_header: `${titleHeaderFamily} Children`,
    },
    {
        exact: true,
        layout: Layout,
        path: '/survey/family/about-dependents',
        component: AboutDependents,
        menu: '',
        menu_footer: '',
        menu_position: stickyMenu,
        classPage,
        title_header: `${titleHeaderFamily} Dependents`,
    },
    {
        layout: SimpleLayout,
        path: '/survey/family/add-children/:children_uuid',
        component: AddChildren,
        menu: '',
        menu_footer: '',
        menu_position: stickyMenu,
        classPage,
        title_header: `${titleHeaderFamily} Children`,
    },
    {
        layout: SimpleLayout,
        path: '/survey/family/add-children',
        component: AddChildren,
        menu: '',
        menu_footer: '',
        menu_position: stickyMenu,
        classPage,
        title_header: `${titleHeaderFamily} Children`,
    },
    {
        layout: SimpleLayout,
        path: '/survey/family/add-job/:user_type/:job_uuid',
        component: AddJob,
        menu: '',
        menu_footer: '',
        menu_position: stickyMenu,
        classPage,
        title_header: `${titleHeaderFamily} Job`,
    },
    {
        exact: true,
        layout: SimpleLayout,
        path: '/survey/family/add-job/:user_type',
        component: AddJob,
        menu: '',
        menu_footer: '',
        menu_position: stickyMenu,
        classPage,
        title_header: `${titleHeaderFamily} Job`,
    },
    {
        exact: true,
        layout: SimpleLayout,
        path: '/survey/family/add-dependents',
        component: AddDependents,
        menu: '',
        menu_footer: '',
        menu_position: stickyMenu,
        classPage,
        title_header: `${titleHeaderFamily} Depends`,
    },
    {
        exact: true,
        layout: SimpleLayout,
        path: '/survey/family/add-dependents/:dependants_uuid',
        component: AddDependents,
        menu: '',
        menu_footer: '',
        menu_position: stickyMenu,
        classPage,
        title_header: `${titleHeaderFamily} Depends`,
    },
    {
        exact: true,
        layout: Layout,
        path: '/survey/family/work/:user_type',
        component: Work,
        menu: '',
        menu_footer: '',
        menu_position: stickyMenu,
        classPage,
        title_header: `${titleHeaderFamily} Work`,
    },
    {
        exact: true,
        layout: Layout,
        path: '/survey/family/retirement',
        component: Retirement,
        menu: '',
        menu_footer: '',
        menu_position: stickyMenu,
        classPage,
        title_header: `${titleHeaderFamily} Retirement`,
    },
    /* DISCOVERY - FINANCE */
    {
        exact: true,
        layout: Layout,
        path: '/survey/finance/assets',
        component: Assets,
        menu: '',
        menu_footer: '',
        menu_position: stickyMenu,
        classPage,
        title_header: 'Financial Holdings',
    },
    {
        exact: true,
        layout: SimpleLayout,
        path: '/survey/finance/add-cash-savings-account',
        component: AddCashSavingsAccount,
        menu: '',
        menu_footer: '',
        menu_position: stickyMenu,
        classPage,
        title_header: `${titleHeaderFinance} Depends`,
    },
    {
        exact: true,
        layout: SimpleLayout,
        path: '/survey/finance/add-cash-savings-account/:plan_uuid',
        component: AddCashSavingsAccount,
        menu: '',
        menu_footer: '',
        menu_position: stickyMenu,
        classPage,
        title_header: `${titleHeaderFinance} Depends`,
    },
    {
        exact: true,
        layout: Layout,
        path: '/survey/finance/about-cash-savings-account',
        component: AboutCashSavingsAccount,
        menu: '',
        menu_footer: '',
        menu_position: stickyMenu,
        classPage,
        title_header: `${titleHeaderFinance} Cash Savings`,
    },
    {
        exact: true,
        layout: SimpleLayout,
        path: '/survey/finance/add-pensions',
        component: AddPensions,
        menu: '',
        menu_footer: '',
        menu_position: stickyMenu,
        classPage,
        title_header: `${titleHeaderFinance} Pensions`,
    },
    {
        exact: true,
        layout: SimpleLayout,
        path: '/survey/finance/add-pensions/:plan_uuid',
        component: AddPensions,
        menu: '',
        menu_footer: '',
        menu_position: stickyMenu,
        classPage,
        title_header: `${titleHeaderFinance} Pensions`,
    },
    {
        exact: true,
        layout: Layout,
        path: '/survey/finance/about-pensions',
        component: AboutPensions,
        menu: '',
        menu_footer: '',
        menu_position: stickyMenu,
        classPage,
        title_header: `${titleHeaderFinance} Pensions`,
    },
    {
        exact: true,
        layout: SimpleLayout,
        path: '/survey/finance/add-isa',
        component: AddISA,
        menu: '',
        menu_footer: '',
        menu_position: stickyMenu,
        classPage,
        title_header: `${titleHeaderFinance} ISAs`,
    },
    {
        exact: true,
        layout: SimpleLayout,
        path: '/survey/finance/add-isa/:plan_uuid',
        component: AddISA,
        menu: '',
        menu_footer: '',
        menu_position: stickyMenu,
        classPage,
        title_header: `${titleHeaderFinance} ISAs`,
    },
    {
        exact: true,
        layout: Layout,
        path: '/survey/finance/about-isa',
        component: AboutISA,
        menu: '',
        menu_footer: '',
        menu_position: stickyMenu,
        classPage,
        title_header: `${titleHeaderFinance} ISAs`,
    },
    {
        exact: true,
        layout: SimpleLayout,
        path: '/survey/finance/add-investments',
        component: AddInvestments,
        menu: '',
        menu_footer: '',
        menu_position: stickyMenu,
        classPage,
        title_header: `${titleHeaderFinance} Investments`,
    },
    {
        layout: SimpleLayout,
        path: '/survey/finance/add-investments/:plan_uuid',
        component: AddInvestments,
        menu: '',
        menu_footer: '',
        menu_position: stickyMenu,
        classPage,
        title_header: `${titleHeaderFinance} Investments`,
    },
    {
        exact: true,
        layout: Layout,
        path: '/survey/finance/about-investments',
        component: AboutInvestments,
        menu: '',
        menu_footer: '',
        menu_position: stickyMenu,
        classPage,
        title_header: `${titleHeaderFinance} Investments`,
    },
    {
        exact: true,
        layout: SimpleLayout,
        path: '/survey/finance/add-property',
        component: AddProperty,
        menu: '',
        menu_footer: '',
        menu_position: stickyMenu,
        classPage,
        title_header: `${titleHeaderFinance} Property`,
    },
    {
        exact: true,
        layout: SimpleLayout,
        path: '/survey/finance/add-property/:plan_uuid',
        component: AddProperty,
        menu: '',
        menu_footer: '',
        menu_position: stickyMenu,
        classPage,
        title_header: `${titleHeaderFinance} Property`,
    },
    {
        exact: true,
        layout: Layout,
        path: '/survey/finance/about-property',
        component: AboutProperty,
        menu: '',
        menu_footer: '',
        menu_position: stickyMenu,
        classPage,
        title_header: `${titleHeaderFinance} Property`,
    },
    /* DISCOVERY - INCOME */
    {
        exact: true,
        layout: Layout,
        path: '/survey/income/salary-income',
        component: SalaryIncome,
        menu: '',
        menu_footer: '',
        menu_position: stickyMenu,
        classPage,
        title_header: `${titleHeaderFinance} Salary income`,
    },
    {
        exact: true,
        layout: SimpleLayout,
        path: '/survey/income/add-salary/:jobs_uuid',
        component: AddSalary,
        menu: '',
        menu_footer: '',
        menu_position: stickyMenu,
        classPage,
        // title_header:`${titleHeaderFinance} Salary income`,
    },
    {
        exact: true,
        layout: SimpleLayout,
        path: '/survey/income/add-additional-income',
        component: AddAdditionalIncome,
        menu: '',
        menu_footer: '',
        menu_position: stickyMenu,
        classPage,
        // title_header:`${titleHeaderFinance} Salary income`,
    },
    {
        exact: true,
        layout: SimpleLayout,
        path: '/survey/income/add-additional-income/:cashflow_uuid',
        component: AddAdditionalIncome,
        menu: '',
        menu_footer: '',
        menu_position: stickyMenu,
        classPage,
        // title_header:`${titleHeaderFinance} Salary income`,
    },
    {
        exact: true,
        layout: Layout,
        path: '/survey/income/additional-income',
        component: AdditionalIncome,
        menu: '',
        menu_footer: '',
        menu_position: stickyMenu,
        classPage,
        title_header: `${titleHeaderFinance} Additional income`,
    },
    {
        exact: true,
        layout: Layout,
        path: '/survey/income/income-changes',
        component: IncomeChanges,
        menu: '',
        menu_footer: '',
        menu_position: stickyMenu,
        classPage,
        title_header: `${titleHeaderFinance} Income changes`,
    },
    /* CHARITY */
    {
        exact: true,
        layout: Layout,
        path: '/survey/charity/add-charity',
        component: AddCharity,
        menu: '',
        menu_footer: '',
        menu_position: stickyMenu,
        classPage,
        title_header: `${titleHeaderFinance} Charity & gifting`,
    },
    /* DEBTS */
    {
        exact: true,
        layout: Layout,
        path: '/survey/debts/assets',
        component: AssetsDebts,
        menu: '',
        menu_footer: '',
        menu_position: stickyMenu,
        classPage,
        title_header: `${titleHeaderFinance} Debts`,
    },
    {
        exact: true,
        layout: SimpleLayout,
        path: '/survey/debts/add-mortgage',
        component: AddMortgage,
        menu: '',
        menu_footer: '',
        menu_position: stickyMenu,
        classPage,
        title_header: `${titleHeaderFinance} Mortgage`,
    },
    {
        exact: true,
        layout: SimpleLayout,
        path: '/survey/debts/add-mortgage/:debts_uuid',
        component: AddMortgage,
        menu: '',
        menu_footer: '',
        menu_position: stickyMenu,
        classPage,
        title_header: `${titleHeaderFinance} Mortgage`,
    },
    {
        exact: true,
        layout: Layout,
        path: '/survey/debts/about-mortgage',
        component: AboutMortgage,
        menu: '',
        menu_footer: '',
        menu_position: stickyMenu,
        classPage,
        title_header: `${titleHeaderFinance} Mortgages`,
    },
    {
        exact: true,
        layout: SimpleLayout,
        path: '/survey/debts/add-loans',
        component: AddLoans,
        menu: '',
        menu_footer: '',
        menu_position: stickyMenu,
        classPage,
        title_header: `${titleHeaderFinance} Other Loans`,
    },
    {
        exact: true,
        layout: SimpleLayout,
        path: '/survey/debts/add-loans/:debts_uuid',
        component: AddLoans,
        menu: '',
        menu_footer: '',
        menu_position: stickyMenu,
        classPage,
        title_header: `${titleHeaderFinance} Other Loans`,
    },
    {
        exact: true,
        layout: Layout,
        path: '/survey/debts/about-loans',
        component: AboutLoans,
        menu: '',
        menu_footer: '',
        menu_position: stickyMenu,
        classPage,
        title_header: `${titleHeaderFinance} Other Loans`,
    },
    {
        exact: true,
        layout: SimpleLayout,
        path: '/survey/debts/add-credit-card',
        component: AddCreditCard,
        menu: '',
        menu_footer: '',
        menu_position: stickyMenu,
        classPage,
        title_header: `${titleHeaderFinance} Outstanding credit cards`,
    },
    {
        exact: true,
        layout: SimpleLayout,
        path: '/survey/debts/add-credit-card/:debts_uuid',
        component: AddCreditCard,
        menu: '',
        menu_footer: '',
        menu_position: stickyMenu,
        classPage,
        title_header: `${titleHeaderFinance} Outstanding credit cards`,
    },
    {
        exact: true,
        layout: Layout,
        path: '/survey/debts/about-credit-card',
        component: AboutCreditCard,
        menu: '',
        menu_footer: '',
        menu_position: stickyMenu,
        classPage,
        title_header: `${titleHeaderFinance} Outstanding credit cards`,
    },
    /* / EXPENSES */
    {
        exact: true,
        layout: Layout,
        path: '/survey/expenses/add-expenses',
        component: AddExpenses,
        menu: '',
        menu_footer: '',
        menu_position: stickyMenu,
        classPage,
        title_header: 'Overall expenses',
    },
    {
        exact: true,
        layout: Layout,
        path: '/survey/expenses/add-children-underage-expenses',
        component: AddChildrenUnderageExpenses,
        menu: '',
        menu_footer: '',
        menu_position: stickyMenu,
        classPage,
        title_header: 'Childcare expenses',
    },
    {
        exact: true,
        layout: SimpleLayout,
        path: '/survey/expenses/add-children-expenses/:child_uuid',
        component: AddChildrenExpenses,
        menu: '',
        menu_footer: '',
        menu_position: stickyMenu,
        classPage,
        title_header: 'Add childcare costs',
    },
    {
        exact: true,
        layout: Layout,
        path: '/survey/expenses/about-children-expenses',
        component: AboutChildrenExpense,
        menu: '',
        menu_footer: '',
        menu_position: stickyMenu,
        classPage,
        title_header: 'Childcare expenses',
    },
    {
        exact: true,
        layout: Layout,
        path: '/survey/expenses/add-insurance',
        component: AddInsurance,
        menu: '',
        menu_footer: '',
        menu_position: stickyMenu,
        classPage,
        title_header: 'Protection',
    },
    /* / RISK TOLERANCE */
    {
        exact: true,
        layout: Layout,
        path: '/survey/risk-tolerance/add-risk-tolerance',
        component: AddRiskTolerance,
        menu: '',
        menu_footer: '',
        menu_position: stickyMenu,
        classPage,
        title_header: 'Risk tolerance',
    },
    /* / PRODUCT PROVIDERS */
    {
        exact: true,
        layout: Layout,
        path: '/survey/product-providers/add-product-providers',
        component: AddProductProviders,
        menu: '',
        menu_footer: '',
        menu_position: stickyMenu,
        classPage,
        title_header: 'Your providers',
    },
    /* / PRODUCT PROVIDERS */
    {
        exact: true,
        layout: Layout,
        path: '/survey/product-providers/add-product-providers',
        component: AddProductProviders,
        menu: '',
        menu_footer: '',
        menu_position: stickyMenu,
        classPage,
        title_header: 'Your providers',
    },
    /* / SUBMIT FORM */
    {
        exact: true,
        layout: Layout,
        path: '/survey/submit-form',
        component: SubmitForm,
        menu: '',
        menu_footer: '',
        menu_position: stickyMenu,
        classPage,
    },
    {
        exact: true,
        layout: Layout,
        path: '/survey/success-submit',
        component: SuccessSubmitForm,
        menu: '',
        menu_footer: '',
        menu_position: stickyMenu,
        classPage,
    },
    {
        exact: true,
        layout: Layout,
        path: '/survey/secondary/more-details',
        component: Details,
        menu: '',
        menu_footer: '',
        menu_position: stickyMenu,
        classPage,
        title_header: 'Personal information',
    },
    {
        exact: true,
        layout: Layout,
        path: '/survey/secondary/more-details-partner',
        component: DetailsPartner,
        menu: '',
        menu_footer: '',
        menu_position: stickyMenu,
        classPage,
        title_header: 'Personal information',
    },
    {
        exact: true,
        layout: Layout,
        path: '/survey/secondary/valuables',
        component: Valuables,
        menu: '',
        menu_footer: '',
        menu_position: stickyMenu,
        classPage,
        title_header: 'Valuables',
    },
    {
        exact: true,
        layout: Layout,
        path: '/survey/secondary/valid-will',
        component: ValidWill,
        menu: '',
        menu_footer: '',
        menu_position: stickyMenu,
        classPage,
        title_header: 'Valuables',
    },
    {
        exact: true,
        layout: Layout,
        path: '/survey/secondary/pension-protection',
        component: PensionProtection,
        menu: '',
        menu_footer: '',
        menu_position: stickyMenu,
        classPage,
        title_header: 'Pension protection',
    },
    {
        exact: true,
        layout: Layout,
        path: '/survey/secondary/significant-gift',
        component: SignificantGift,
        menu: '',
        menu_footer: '',
        menu_position: stickyMenu,
        classPage,
        title_header: 'Valuables',
    },
    {
        exact: true,
        layout: Layout,
        path: '/survey/secondary/poa',
        component: POA,
        menu: '',
        menu_footer: '',
        menu_position: stickyMenu,
        classPage,
        title_header: 'Valuables',
    },
    {
        exact: true,
        layout: Layout,
        path: '/survey/secondary/gains-losses',
        component: GainsLosses,
        menu: '',
        menu_footer: '',
        menu_position: stickyMenu,
        classPage,
        title_header: 'Gains and losses',
    },
    {
        exact: true,
        layout: SimpleLayout,
        path: '/survey/secondary/add-gains-losses',
        component: AddGainsLosses,
        menu: '',
        menu_footer: '',
        menu_position: stickyMenu,
        classPage,
        title_header: 'Add gains & losses',
    },
    {
        exact: true,
        layout: Layout,
        path: '/survey/secondary/legacy-details',
        component: LegacyDetails,
        menu: '',
        menu_footer: '',
        menu_position: stickyMenu,
        classPage,
        title_header: 'Legacy',
    },
    {
        exact: true,
        layout: Layout,
        path: '/survey/secondary/legacy-plans',
        component: LegacyPlans,
        menu: '',
        menu_footer: '',
        menu_position: stickyMenu,
        classPage,
        title_header: 'Legacy',
    },
    {
        exact: true,
        layout: Layout,
        path: '/survey/secondary/power-of-attorney',
        component: PowerOfAttorney,
        menu: '',
        menu_footer: '',
        menu_position: stickyMenu,
        classPage,
        title_header: 'Legacy',
    },
    {
        exact: true,
        layout: SimpleLayout,
        path: '/survey/secondary/add-power-of-attorney',
        component: AddPowerOfAttorney,
        menu: '',
        menu_footer: '',
        menu_position: stickyMenu,
        classPage,
        title_header: 'Add Power of Attorney',
    },
];
