import Loadable from 'react-loadable';
import { Loading } from '../../components/Loading';
import NavPresentation from '../../view/webapp/meeting/NavPresentation';
import Presentation from '../../view/webapp/meeting/Presentation';

// LAYOUT
const WebappLayout = Loadable({
    loader: () => import('../../containers/layouts/Webapp.container' /* webpackChunkName: "webapp-layout", */),
    loading: Loading,
});

const WebappLoginLayout = Loadable({
    // eslint-disable-next-line max-len
    loader: () => import('../../containers/layouts/WebappLogin.container' /* webpackChunkName: "webapp-login-layout", */),
    loading: Loading,
});

// WEBAPP
const Review = Loadable({
    loader: () => import('../../view/webapp/meeting/Review' /* webpackChunkName: "review", */),
    loading: Loading,
});
const Slide = Loadable({
    loader: () => import('../../view/webapp/meeting/Slide' /* webpackChunkName: "slide", */),
    loading: Loading,
});

const Login = Loadable({
    loader: () => import('../../view/webapp/meeting/Login' /* webpackChunkName: "login", */),
    loading: Loading,
});

const SearchClient = Loadable({
    loader: () => import('../../view/webapp/meeting/SearchClient' /* webpackChunkName: "search-client", */),
    loading: Loading,
});
const Configuration = Loadable({
    loader: () => import('../../view/webapp/meeting/Configuration' /* webpackChunkName: "configuration", */),
    loading: Loading,
});

const classWebapp = 'Webapp';
const titlePage = 'Octopus Wealth -';

export default [
    /* WEBAPP */
    {
        layout: WebappLoginLayout,
        path: '/webapp/login',
        component: Login,
        classPage: classWebapp,
        titlePage: `${titlePage} Webapp Login`,
    },
    {
        layout: WebappLoginLayout,
        path: '/webapp/search-client',
        component: SearchClient,
        menu: 'webapp',
        classPage: classWebapp,
        titlePage: `${titlePage} Webapp Search Client`,
    },
    {
        layout: WebappLoginLayout,
        path: '/webapp/configuration',
        component: Configuration,
        menu: 'webapp',
        classPage: classWebapp,
        titlePage: `${titlePage} Webapp Icons Configuration`,
    },
    {
        layout: WebappLayout,
        path: '/webapp/:name/review',
        component: Review,
        classPage: classWebapp,
        titlePage: `${titlePage} Webapp Review`,
    },
    {
        layout: WebappLayout,
        path: '/webapp/:name/slide',
        component: Slide,
        classPage: classWebapp,
        exact: true,
        titlePage: `${titlePage} Webapp Add New Slide`,
    },
    {
        layout: WebappLayout,
        path: '/webapp/:name/slide/:id',
        component: Slide,
        classPage: classWebapp,
        exact: true,
        titlePage: `${titlePage} Webapp Edit Slide`,
    },
    {
        layout: WebappLayout,
        path: '/webapp/:name/nav_presentation',
        component: NavPresentation,
        classPage: classWebapp,
        titlePage: `${titlePage} Webapp Nav Presentation`,
    },
    {
        layout: WebappLayout,
        path: '/webapp/:name/presentation',
        component: Presentation,
        classPage: classWebapp,
        titlePage: `${titlePage} Webapp Presentation`,
    },
];
