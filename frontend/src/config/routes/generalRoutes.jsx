import Loadable from 'react-loadable';
import { Loading } from '../../components/Loading';

const WebsiteLayout = Loadable({
    loader: () => import('../../containers/layouts/Website.container' /* webpackChunkName: "website-layout" */),
    loading: Loading,
});

const NotFound = Loadable({
    loader: () => import('../../containers/common/NotFound' /* webpackChunkName: "not-found" */),
    loading: Loading,
});

const classHome = 'htmlPage guides staticMenu';

const titlePage = 'Octopus Wealth -';

export default [
    {
        exact: false,
        layout: WebsiteLayout,
        path: '*',
        component: NotFound,
        classPage: classHome,
        menu: 'website',
        menuFooter: '',
        menuName: '',
        menuPosition: 'static',
        titlePage: `${titlePage} Homepage`,
    },
];
