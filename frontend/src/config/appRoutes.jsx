import React from 'react';
import { Switch } from 'react-router-dom';
import { RouteWithLayout } from '../utils/route/route.helper';

import AdviserRoutes from './routes/adviserRoutes';
import OnboardingRoutes from './routes/onboardingRoutes';
import DiscoveryRoutes from './routes/discoveryRoutes';
import GeneralRoutes from './routes/generalRoutes';
import WebsiteRoutes from './routes/websiteRoutes';
import WebappRoutes from './routes/webappRoutes';

const allRoutes = WebsiteRoutes.concat(AdviserRoutes, DiscoveryRoutes, WebappRoutes, OnboardingRoutes, GeneralRoutes);

export default (
    <Switch>
        {
            allRoutes.map((item, i) => (
                <RouteWithLayout
                    key={i.toString()}
                    exact={item.exact}
                    layout={item.layout}
                    path={item.path}
                    component={item.component}
                    classPage={item.classPage}
                    menu={item.menu}
                    menuFooter={item.menuFooter}
                    menuName={item.menuName}
                    menuPosition={item.menuPosition}
                    titleHeader={item.titleHeader}
                />
            ))
        }
    </Switch>
);
