import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import get from 'lodash/get';
import isEmpty from 'lodash/isEmpty';
import isArray from 'lodash/isArray';
import { reduxForm } from 'redux-form';
import { bindActionCreators } from 'redux';
import { isBoolean } from '../../utils/helper';

export default (WrappedComponent, surveyData) => {
    class WebappHoc extends Component {
        constructor(props) {
            super(props);
            this.handleSubmit = this.handleSubmit.bind(this);
            this.handleBack = this.handleBack.bind(this);
            this.handleCancel = this.handleCancel.bind(this);
            this.state = {
                isSubmitted: false,
            };
        }

        handleSubmit = (event) => {
            const {
                formValue,
                dispatch,
                // history,
                match: {
                    params: {
                        name,
                    },
                },
            } = this.props;
            this.setState({ isSubmitted: true });
            if (!isEmpty(get(formValue, 'syncErrors', {}))) {
                event.preventDefault();
            } else {
                surveyData.data.forEach((data) => {
                    const body = {};
                    data.fields.forEach((field) => {
                        const valueByName = get(formValue, `values.${field}`, null);
                        if (!isEmpty(valueByName) || isArray(valueByName) || isBoolean(valueByName)) {
                            body[field] = valueByName;
                        }
                    });
                    body.idGoogle = name;
                    const isUpdate = get(formValue, `values.${data.isUpdate}`, false);
                    if (isUpdate) {
                        const method = bindActionCreators(data.methodUpdate, dispatch);

                        if (!isEmpty(data.additionalUpdateFields)) {
                            data.additionalUpdateFields.forEach((field) => {
                                Object.assign(body, field);
                            });
                        }

                        body.selector = get(formValue, `values.${data.selector}`, null);
                        method(body);
                    } else {
                        const method = bindActionCreators(data.methodCreate, dispatch);

                        if (!isEmpty(data.additionalCreateFields)) {
                            data.additionalCreateFields.forEach((field) => {
                                Object.assign(body, field);
                            });
                        }
                        method(body);
                    }
                });
            }
        };

        handleBack() {
            const { history } = this.props;
            history.push(surveyData.prevStage);
        }

        handleCancel() {
            const { history } = this.props;
            history.push(surveyData.prevStage);
        }

        render() {
            const {
                formValue,
            } = this.props;
            const { isSubmitted } = this.state;
            return (
                <WrappedComponent
                    {...this.props}
                    handleSubmit={this.handleSubmit}
                    handleBack={this.handleBack}
                    handleCancel={this.handleCancel}
                    formValue={formValue}
                    isSubmitted={isSubmitted}
                />
            );
        }
    }

    WebappHoc.propTypes = {
        history: PropTypes.object.isRequired,
        formValue: PropTypes.object.isRequired,
        isRequesting: PropTypes.number.isRequired,
    };

    const mapStateToProps = state => ({
        formValue: get(state, `form.${surveyData.formName}`, {}),
        isRequesting: get(state, 'common.isRequestsDone', 0),
    });

    const initializeForm = reduxForm({
        form: surveyData.formName,
        enableReinitialize: true,
    })(WebappHoc);

    return connect(mapStateToProps)(initializeForm);
};
