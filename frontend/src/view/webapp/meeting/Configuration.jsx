import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { getCurrentIcon } from '../../../components/Charts/assetsIcon';
import { loadIcons } from '../../../actions/WebappActionsConfiguration';

class IconConfiguration extends Component {
    constructor(props) {
        super(props);
        this.state = {
            icons: [],
            labels: [],
            toSave: false,
        };
        const { onGetIcons } = this.props;
        this.changeLabel = this.changeLabel.bind(this);
        this.addRow = this.addRow.bind(this);
        this.saveData = this.saveData.bind(this);
        this.updateState = this.updateState.bind(this);
        onGetIcons();
    }

    componentDidUpdate() {
        const {
            actionConfigurationWebapp: {
                data,
            },
        } = this.props;
        const {
            icons,
        } = this.state;
        // const data = this.props.actionConfigurationWebapp.data;
        if (icons.length < 1 && typeof data !== 'undefined') {
            const labels = [];

            const errors = [];
            data.forEach((item, i) => {
                icons.forEach(item[0]);
                labels[i] = [];
                errors[i] = [];
                if (item[1]) {
                    const label = JSON.parse(item[1]);
                    label.forEach((subitem) => {
                        labels[i].push(subitem);
                        errors[i].push(false);
                    });
                }
            });
            this.updateState(icons, labels, errors);
        }
    }

    updateState(icons, labels, errors) {
        this.setState({
            icons,
            labels,
            errors,
        });
    }

    changeLabel(e, arrayid, idLabel) {
        e.preventDefault();
        const {
            labels,
            errors,
        } = this.state;
        const editLabel = labels;
        const editError = errors;
        let flag = false;
        editLabel.forEach((item, i) => {
            if (item.length > 0) {
                item.forEach((subitem, f) => {
                    if (subitem === e.target.value) {
                        flag = true;
                        editError[i][f] = true;
                    } else {
                        editError[i][f] = false;
                    }
                });
            }
        });
        if (flag) {
            editError[arrayid][idLabel] = true;
        } else {
            editError[arrayid][idLabel] = false;
        }
        editLabel[arrayid][idLabel] = e.target.value;
        this.setState({
            labels: editLabel,
            errors: editError,
            toSave: true,
        });
    }

    addRow(e, arrayId) {
        e.preventDefault();
        const {
            labels,
        } = this.state;
        const editLabel = labels;
        if (editLabel[arrayId].length < 1 || editLabel[arrayId][editLabel[arrayId].length - 1]) {
            editLabel[arrayId].push('');
        }
        this.setState({
            labels: editLabel,
        });
    }

    saveData(e) {
        const { onSaveIcons } = this.props;
        const { labels, icons } = this.state;
        e.preventDefault();
        const allData = [];
        icons.map((item, i) => {
            allData[i] = [item, JSON.stringify(labels[i]).replace('""', '')];
            return null;
        });
        allData.splice(0, 0, ['Icon', 'Words']);
        onSaveIcons(allData);
    }

    render() {
        const {
            state: {
                icons, toSave, saving, labels, errors, saved,
            },
            changeLabel,
            addRow,
        } = this;

        return (
            <div>
                <div id="loginSection">
                    <div className="row">
                        {
                            icons.map((item, i) => (
                                <div className="col-6" key={parseInt(i.toString(), 10)}>
                                    <div className="row">
                                        <div className="col-4">
                                            <svg key={parseInt(i.toString(), 10)} viewBox="0 0 1024 1024">{getCurrentIcon[item]}</svg>
                                        </div>
                                        <div className="col-8">
                                            {
                                                labels[i].map((sublabel, f) => (
                                                    <div key={parseInt(f.toString(), 10)}>
                                                        <input
                                                            type="text"
                                                            className={errors[i][f] ? 'error_input' : ''}
                                                            value={sublabel}
                                                            onChange={changeLabel.bind(this, i, f)}
                                                        />
                                                    </div>
                                                ))
                                            }
                                            <button
                                                type="button"
                                                onClick={addRow.bind(this, i)}
                                                className="primary_button"
                                            >
                                                ADD NEW ROW
                                            </button>
                                        </div>
                                    </div>
                                </div>

                            ))
                        }
                    </div>
                    <div className="nav_edit_button">
                        {saved
                            ? <p>Changes Saved</p>
                            : (
                                <button
                                    type="button"
                                    onClick={this.saveData}
                                    className={toSave ? 'alert_button' : 'primary_button'}
                                >
                                    {saving ? 'SAVING CHANGES' : 'SAVE CHANGES'}
                                </button>
                            )
                        }
                    </div>
                </div>
            </div>
        );
    }
}

IconConfiguration.propTypes = {
    onGetIcons: PropTypes.func.isRequired,
    actionConfigurationWebapp: PropTypes.object,
    onSaveIcons: PropTypes.func.isRequired,
};

IconConfiguration.defaultProps = {
    actionConfigurationWebapp: {},
};

// const mapStateToProps = state => ({
//     ...state,
// });

const mapDispatchToProps = dispatch => ({
    onGetIcons: () => dispatch(loadIcons()),
});

export default withRouter(connect(mapDispatchToProps)(IconConfiguration));
