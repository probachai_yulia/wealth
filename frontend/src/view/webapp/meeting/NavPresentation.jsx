/* eslint-disable max-len */
import React, { Component, Fragment } from 'react';
import { Link, withRouter } from 'react-router-dom';
import 'slick-carousel/slick/slick.css';
import 'slick-carousel/slick/slick-theme.css';
import Slider from 'react-slick';
import { connect } from 'react-redux';
import Loadable from 'react-loadable';
import PropTypes from 'prop-types';
import get from 'lodash/get';
import { change } from 'redux-form';
// import { chunkArray } from '../../../helpers/WebappFunctions';
// import { getCurrentIcon } from '../../../components/Charts/assetsIcon';
import Preload from '../../../components/Loading/preload';
import { Loading } from '../../../components/Loading';
// import Logo_Octopus_Co from '../../../../../static/img/Octopus_Wealth.svg';
import { filterById } from '../../../helpers/WebappFunctions';
// import Table from '../../../components/Table/table';
const Table = Loadable({
    loader: () => import('./../../../components/Table/table' /* webpackChunkName: "table" */),
    loading: Loading,
});
// import Line_Charts from '../../../components/Charts/Line_Charts';
const LineCharts = Loadable({
    loader: () => import('./../../../components/Charts/LineCharts' /* webpackChunkName: "line-chart" */),
    loading: Loading,
});
// import Box from '../../../components/Box_Text/box';
const Box = Loadable({
    loader: () => import('./../../../components/BoxText/box' /* webpackChunkName: "box" */),
    loading: Loading,
});
// import Text from '../../../components/Box_Text/text';
const Text = Loadable({
    loader: () => import('./../../../components/BoxText/text' /* webpackChunkName: "text" */),
    loading: Loading,
});

class navPresentation extends Component {
    constructor(props) {
        super(props);
        const {
            idGoogle,
        } = this.props;
        this.state = {
            accessToken: localStorage.getItem('token') || '',
            printPage: false,
            allDataDef: [],
            gData: [],
            idGoogleSheet: idGoogle,
            loadNewReview: false,
            loading: true,
            width: 1440,
            height: 810,
            gloabalNotes: [],
            goNext: true,
            goNextTable: true,
            curStep: 6,
            fullScreen: false,
            globalClient: '',
            globalPartner: '',
            viewAxisYInterval: 5,
            step: 0,
            endStep: 0,
            printCode: 0,
            minAxisClient1: 0,
            minAxisClient2: 0,
            minAxisClient3: 0,
            maxAxisClient1: 0,
            maxAxisClient2: 0,
            maxAxisClient3: 0,
        };

        this.changeSlide = this.changeSlide.bind(this);
        this.assignSlide = this.assignSlide.bind(this);
        this.changeState = this.changeState.bind(this);
    }

    componentDidMount() {
        const {
            allData,
        } = this.props;
        localStorage.setItem('currentSlide', 0);
        localStorage.setItem('curStep', 6);
        const locaData = [];
        allData.slidePresentation.allDataDef.map((item) => {
            if (item.exclude === 0) {
                locaData.push(item);
            }
            return null;
        });
        try {
            localStorage.setItem('data', JSON.stringify(locaData));
        } catch (e) {
            localStorage.setItem('curStepss', 6);
        }
        localStorage.setItem('maxAxisClient', allData.slidePresentation.maxAxisClient);
        localStorage.setItem('minAxisClient', allData.slidePresentation.minAxisClient);
        localStorage.setItem('maxAxisClient1', allData.slidePresentation.maxAxisClient1);
        localStorage.setItem('minAxisClient1', allData.slidePresentation.minAxisClient1);
        localStorage.setItem('maxAxisClient2', allData.slidePresentation.maxAxisClient2);
        localStorage.setItem('minAxisClient2', allData.slidePresentation.minAxisClient2);
        localStorage.setItem('maxAxisClient3', allData.slidePresentation.maxAxisClient3);
        localStorage.setItem('minAxisClient3', allData.slidePresentation.minAxisClient3);
        localStorage.setItem('globalClient', allData.slidePresentation.globalClient);
        localStorage.setItem('globalPartner', allData.slidePresentation.globalPartner);
        this.setState({
            allDataDef: allData.slidePresentation.allDataDef,
            gData: allData.slidePresentation.gData,
            // changeGData: allData.slidePresentation.gData,
            loading: false,
            // sheetsList: listSheets,
            // maxAxisClient: allData.slidePresentation.maxAxisClient,
            // minAxisClient: allData.slidePresentation.minAxisClient,
            maxAxisClient1: allData.slidePresentation.maxAxisClient1,
            minAxisClient1: allData.slidePresentation.minAxisClient1,
            maxAxisClient2: allData.slidePresentation.maxAxisClient2,
            minAxisClient2: allData.slidePresentation.minAxisClient2,
            maxAxisClient3: allData.slidePresentation.maxAxisClient3,
            minAxisClient3: allData.slidePresentation.minAxisClient3,
            globalClient: allData.slidePresentation.globalClient,
            globalPartner: allData.slidePresentation.globalPartner,
            gloabalNotes: allData.slidePresentation.globalNotes,
            // advisor: allData.slidePresentation.advisor,
            loadNewReview: false,
            // saving: false,
            // toSave: false,
            // saved: false,
            // loadOriginalPlan: false,
            // reloadPlan: false,
        });
    }

    saveNote(type, arrayId, val) {
        const {
            gData,
        } = this.state;
        const arrayData = gData;
        arrayData[arrayId][38] = JSON.stringify(val);
        this.setState({
            gData: arrayData,
            // toSave: true,
        });
    }

    changeSlide(action, arrayId, e) {
        const self = this;
        const {
            allDataDef, goNext, goNextTable, step, endStep, curStep, printCode, idGoogleSheet,
        } = this.state;
        const {
            history,
        } = this.props;
        let done = true;
        if (arrayId > 0) {
            allDataDef[arrayId - 1].notes.map((item) => {
                if (item.done === 0) {
                    done = false;
                }
                return null;
            });
        }
        if (((action === 'keyboard' && (e.keyCode === 40 || e.keyCode === 39)) || action === 'next') && goNext && goNextTable && done) {
            this.slider.slickNext();
            // this.setState({ visibleLabel: 'hiddenLabel' });
            localStorage.setItem('slide', parseInt(localStorage.getItem('slide'), 10) + 1);
            localStorage.setItem('visibleLabel', 'hiddenLabel');
            self.setState({ slideStep: parseInt(localStorage.getItem('slide'), 10) + 1 });
        }
        if (e.keyCode === 38 || e.keyCode === 37 || action === 'prev') {
            localStorage.setItem('currentSlide', this.slider.slickCurrentSlide);
            this.slider.slickPrev();
            // this.setState({ visibleLabel: 'viewLabel' });
            localStorage.setItem('slide', parseInt(localStorage.getItem('slide'), 10) - 1);
            localStorage.setItem('visibleLabel', 'viewLabel');
            self.setState({ slideStep: parseInt(localStorage.getItem('slide'), 10) - 1 });
        }
        if (!goNext) {
            // this.setState({ visibleLabel: 'viewLabel', goNext: true });
            localStorage.setItem('visibleLabel', 'viewLabel');
            localStorage.setItem('goNext', true);
        }
        if (!goNextTable) {
            this.setState({ step: step + 1 });
            localStorage.setItem('step', step + 1);
            localStorage.setItem('endStep', endStep);
            localStorage.setItem('curStep', curStep + 1);
            self.setState({
                curStep: curStep + 1,
            });
            if (step + 1 >= endStep) {
                this.setState({ step: step + 1, goNextTable: true });
                localStorage.setItem('step', step + 1);
            }
        }
        if (e.keyCode === 27) {
            history.push(`/webapp/${idGoogleSheet}/review`);
        }
        if ((printCode === 93 || printCode === 91 || printCode === 17) && e.keyCode === 80) {
            this.setState({ printPage: true });
        }
        this.setState({
            printCode: e.keyCode,
        });
    }

    animationAreaLabel(id) {
        const self = this;
        localStorage.setItem('currentSlide', id);
        const elementTable = document.getElementsByClassName('making_money_full_table');
        Array.prototype.map.call(elementTable, (item) => {
            if (item.id === `divToPrint_${id}`) {
                self.setState({ goNextTable: false, step: 1, endStep: 4 });
                localStorage.setItem('goNextTable', false);
                localStorage.setItem('step', 1);
                localStorage.setItem('endStep', 4);
            }
        });
        const elementChartFull = document.getElementsByClassName('all_assets_full');
        Array.prototype.map.call(elementChartFull, (item) => {
            if (item.id === `divToPrint_${id}`) {
                self.setState({ goNextTable: false, step: 1, endStep: 4 });
                localStorage.setItem('goNextTable', false);
                localStorage.setItem('step', 1);
                localStorage.setItem('endStep', 4);
                self.setState({
                    curStep: 0,
                });
                localStorage.setItem('curStep', 0);
                let num = 0;
                const intervalID1 = setInterval(() => {
                    localStorage.setItem('curStep', self.state.curStep + 1);
                    self.setState({
                        curStep: self.state.curStep + 1,
                    });
                    num += 1;
                    if (num === 4) { window.clearInterval(intervalID1); }
                }, 3000);
            }
        });
        const elementChartBprNl = document.getElementsByClassName('all_assets_bprNl');
        Array.prototype.map.call(elementChartBprNl, (item) => {
            if (item.id === `divToPrint_${id}`) {
                self.setState({ goNextTable: false, step: 1, endStep: 3 });
                localStorage.setItem('goNextTable', false);
                localStorage.setItem('step', 1);
                localStorage.setItem('endStep', 3);
                self.setState({
                    curStep: 0,
                });
                localStorage.setItem('curStep', 0);
                let num = 0;
                const intervalID1 = setInterval(() => {
                    localStorage.setItem('curStep', self.state.curStep + 1);
                    self.setState({
                        curStep: self.state.curStep + 1,
                    });
                    num += 1;
                    if (num === 3) { window.clearInterval(intervalID1); }
                }, 3000);
            }
        });
        const elementChartAss = document.getElementsByClassName('all_assets_det');
        Array.prototype.map.call(elementChartAss, (item) => {
            if (item.id === `divToPrint_${id}`) {
                self.setState({ goNextTable: false, step: 1, endStep: 2 });
                localStorage.setItem('goNextTable', false);
                localStorage.setItem('step', 1);
                localStorage.setItem('endStep', 2);
                self.setState({
                    curStep: 0,
                });
                localStorage.setItem('curStep', 0);
                let num = 0;
                const intervalID1 = setInterval(() => {
                    localStorage.setItem('curStep', self.state.curStep + 1);
                    self.setState({
                        curStep: self.state.curStep + 1,
                    });
                    num += 1;
                    if (num === 3) { window.clearInterval(intervalID1); }
                }, 3000);
            }
        });
        const elementChartBpr = document.getElementsByClassName('all_assets_bpr');
        Array.prototype.map.call(elementChartBpr, (item) => {
            if (item.id === `divToPrint_${id}`) {
                self.setState({ goNextTable: false, step: 1, endStep: 2 });
                localStorage.setItem('goNextTable', false);
                localStorage.setItem('step', 1);
                localStorage.setItem('endStep', 2);
                self.setState({
                    curStep: 0,
                });
                localStorage.setItem('curStep', 0);
                let num = 0;
                const intervalID1 = setInterval(() => {
                    localStorage.setItem('curStep', self.state.curStep + 1);
                    self.setState({
                        curStep: self.state.curStep + 1,
                    });
                    num += 1;
                    if (num === 4) { window.clearInterval(intervalID1); }
                }, 3000);
            }
        });
        const elementChart4 = document.getElementsByClassName('liquid_assets_detailed');
        Array.prototype.map.call(elementChart4, (item) => {
            if (item.id === `divToPrint_${id}`) {
                self.setState({ goNextTable: false, step: 1, endStep: 1 });
                localStorage.setItem('goNextTable', false);
                localStorage.setItem('step', 1);
                localStorage.setItem('endStep', 1);
                self.setState({
                    curStep: 3,
                });
                localStorage.setItem('curStep', 3);
                let num = 0;
                const intervalID1 = setInterval(() => {
                    localStorage.setItem('curStep', self.state.curStep + 1);
                    self.setState({
                        curStep: self.state.curStep + 1,
                    });
                    num += 1;
                    if (num === 3) { window.clearInterval(intervalID1); }
                }, 3000);
            }
        });
    }

    checkToDoList(type, arrayId, elId, e) {
        const {
            gloabalNotes, allDataDef, idGoogleSheet, accessToken,
        } = this.state;
        const {
            checked,
        } = e.target;
        const val = checked;
        let currentNote;

        if (type === 'general') {
            const arrayTodo = gloabalNotes;
            arrayTodo[arrayId].done = val ? 1 : 0;
            // this.setState({
            //     globalNotes: arrayTodo,
            // });
            currentNote = arrayTodo;
        } else if (type === 'slide') {
            const arrayTodo = allDataDef;
            arrayTodo[elId].notes[arrayId].done = val ? 1 : 0;
            this.setState({
                allDataDef: arrayTodo,
            });
            currentNote = arrayTodo[elId].notes;
        }
        const googleRow = type === 'slide' ? elId + 2 : 1;
        const formDataSheet = new FormData();
        formDataSheet.append('idGoogleSheet', idGoogleSheet);
        formDataSheet.append('accessToken', accessToken);
        formDataSheet.append('notes', JSON.stringify(currentNote));
        formDataSheet.append('row', googleRow);
        formDataSheet.append('action', 'saveNote');
    }

    assignSlide(e) {
        this.slider = e;
    }

    changeState(state, val) {
        this.setState({
            [state]: val,
        });
    }

    render() {
        const {
            allDataDef, slidePresentation,
        } = this.props;
        const {
            width, height, printPage, fullScreen, loadNewReview, idGoogleSheet, globalClient, globalPartner, gloabalNotes, curStep, viewAxisYInterval, loading, minAxisClient1, minAxisClient2, minAxisClient3, maxAxisClient1, maxAxisClient2, maxAxisClient3, step,
        } = this.state;
        const {
            checkToDoList,
        } = this;
        const self = this;
        let zoom = 0;
        if (!printPage) {
            if (fullScreen) {
                const zoomWidth = window.innerWidth / width;
                const zoomHeight = window.innerHeight / height;
                zoom = zoomWidth > zoomHeight ? zoomHeight : zoomWidth;
            } else {
                zoom = ((window.innerWidth / 2) / self.state.width) - 0.05;
            }
        } else {
            zoom = 0.78;
        }
        const currentSlide = this.slider ? this.slider.innerSlider.state.currentSlide : 0;
        const filteredData = filterById(allDataDef, 'exclude', 0);
        const settings = {
            dots: !fullScreen,
            infinite: false,
            arrows: false,
            accessibility: false,
            draggable: false,
            speed: 1,
            slidesToShow: 1,
            slidesToScroll: 1,
            beforeChange: (current, next) => this.animationAreaLabel(next, current),
            afterChange: () => this.forceUpdate(),
        };
        if (loading) {
            return (
                <div>
                    <Preload createPresentation={loadNewReview} />
                </div>
            );
        }
        return (
            <div
                className="App presentation_page nav_page"
                style={{ width: window.innerWidth }}
                onKeyDown={this.changeSlide.bind(this, 'keyboard', currentSlide)}
                role="presentation"
            >
                <div>
                    <div id="nav_presentation_button" style={{ display: fullScreen ? 'none' : 'block' }}>
                        <div className="pull_left">
                            <a
                                className="primary_button"
                                href={`/webapp/${idGoogleSheet}/presentation`}
                                target="_blank"
                                rel="noopener noreferrer"
                            >
                                CLIENT WINDOW
                            </a>
                            <Link
                                to={`/webapp/${idGoogleSheet}/review`}
                                className="primary_button"
                            >
                                EXIT
                            </Link>
                            <button
                                type="button"
                                className="primary_button prev"
                                onClick={this.changeState.bind('fullScreen', !fullScreen)}
                            >
                                FULL SCREEN
                            </button>
                        </div>
                        <div className="pull_right">
                            <button
                                type="button"
                                className="primary_button prev"
                                onClick={this.changeSlide.bind(this, 'prev', currentSlide)}
                            >
                                PREVIOUS
                            </button>
                            <button
                                type="button"
                                className="primary_button next"
                                onClick={this.changeSlide.bind(this, 'next', currentSlide)}
                            >
                                NEXT
                            </button>
                        </div>
                    </div>
                    <header className="sticky_header" style={{ display: fullScreen ? 'none' : 'block' }}>
                        <div className="presentationHeader">
                            <div>
                                <Link
                                    to={`/webapp/${idGoogleSheet}/review`}
                                >
                                    <img src="/static/img/Octopus_Wealth.svg" alt="Logo Octopus" />
                                </Link>
                            </div>
                            {
                                filteredData.length > 0 && (
                                    <div>
                                        {`${globalClient} ${globalPartner && `& ${globalPartner}`}`}
                                    </div>
                                )
                            }
                        </div>
                    </header>
                    <Slider ref={this.assignSlide.bind(this)} {...settings} className="Section">
                        <div className="cover_slide single_slide" id="divToPrint_0">
                            <div className="rowFlex">
                                <div className={fullScreen ? 'col-md-12' : 'col-md-6'} style={{ backgroundColor: '#000000', padding: fullScreen && '0px' }}>
                                    <div
                                        style={{
                                            zoom,
                                            width: self.state.width,
                                            height: self.state.height,
                                            backgroundColor: '#FFFFFF',
                                            marginLeft: 'auto',
                                            marginRight: 'auto',
                                        }}
                                        className="wrapSlide"
                                    >
                                        <header>
                                            <div className="presentationHeader">
                                                <div>
                                                    <Link
                                                        to={`/webapp/${idGoogleSheet}/review`}
                                                    >
                                                        <img src="/static/img/Octopus_Wealth.svg" alt="Logo Octopus" />
                                                    </Link>
                                                </div>
                                                {
                                                    filteredData.length > 0 && (
                                                        <div>
                                                            {globalClient}
                                                            {globalPartner && ` & ${globalPartner}`}
                                                            {
                                                                fullScreen && (
                                                                    <button
                                                                        type="button"
                                                                        className="btn_icon close_presentation"
                                                                        onClick={this.changeState.bind('fullScreen', !fullScreen)}
                                                                    >
                                                                        <img src="/static/img/close.svg" alt="exit fullscreen" />
                                                                    </button>
                                                                )
                                                            }
                                                        </div>
                                                    )
                                                }
                                            </div>
                                        </header>
                                        <div
                                            className="intro"
                                            id="divToPrint_0"
                                            style={{ height: window.innerHeight - 50 }}
                                        >
                                            <div style={{
                                                width,
                                                height: height - 50,
                                                display: 'flex',
                                                alignItems: 'center',
                                            }}
                                            >
                                                <img src="/static/img/Octopus_Wealth.svg" alt="Logo Octopus" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div className="col-md-6 paddingRight40" style={{ display: fullScreen ? 'none' : 'block' }}>
                                    <div className="generalNotes">
                                        <h3>GENERAL NOTES</h3>
                                        <ul>
                                            {
                                                gloabalNotes.map((item, f) => (
                                                    <li key={f.toString()}>
                                                        <input
                                                            id={`general${f}`}
                                                            type="checkbox"
                                                            name={`general${f}`}
                                                            checked={parseInt(item.done, 10) === 1}
                                                            onChange={checkToDoList.bind(this, 'general', f, 0)}
                                                        />
                                                        <label
                                                            htmlFor={`general${f}`}
                                                            dangerouslySetInnerHTML={{ __html: item.text }}
                                                        />
                                                    </li>
                                                ))
                                            }
                                        </ul>
                                    </div>
                                    <div className="slideNotes">
                                        <h3>SLIDE NOTES</h3>
                                    </div>
                                </div>
                            </div>
                        </div>
                        {
                            filteredData.map((item, i) => {
                                let viewLegend = true;
                                if (typeof item.options.viewLegend !== 'undefined') {
                                    if (!item.options.viewLegend) {
                                        viewLegend = false;
                                    }
                                }
                                let chartType = '';
                                if (item.chartsType === 'all_assets_detailed') {
                                    chartType = 'all_assets_det';
                                }
                                if (typeof item.options.bprNlEnable !== 'undefined' && item.dataType === 'chart') {
                                    if (item.options.bprNlEnable && item.options.bprEnable && item.chartsType === 'all_assets_detailed') {
                                        chartType = 'all_assets_full';
                                    } else if ((!item.options.bprNlEnable || item.options.bprEnable) && item.chartsType === 'all_assets_detailed') {
                                        chartType = 'all_assets_bpr';
                                    } else if ((item.options.bprNlEnable || !item.options.bprEnable) && item.chartsType === 'all_assets_detailed') {
                                        chartType = 'all_assets_bprNl';
                                    } else if (item.chartsType === 'all_assets_detailed') {
                                        chartType = 'all_assets_det';
                                    }
                                }
                                if (item.exclude !== 1) {
                                    const nestBackImage = item.options.backgroundLine ? 'url(/static/img/presentation/octopus_first_slide.png)' : '';
                                    const nestAreaLabel = item.options.viewAreaLabel ? parseInt(item.areaLabel, 10) : -1;
                                    return (
                                        <div
                                            className={(parseInt(item.areaLabel, 10) >= 0 && item.dataType === 'chart') ? `${item.chartsType} areaLabel single_slide ${self.state.visibleLabel}` : `${item.dataType} single_slide ${item.chartsType} ${chartType}`}
                                            key={i.toString()}
                                            id={`divToPrint_${i + 1}`}
                                        >
                                            <div className="rowFlex">
                                                <div className={fullScreen ? 'col-md-12' : 'col-md-6'} style={{ backgroundColor: '#000000', padding: fullScreen && '0px' }}>
                                                    <div
                                                        style={{
                                                            zoom,
                                                            width: self.state.width,
                                                            height: self.state.height,
                                                            backgroundColor: '#FFF',
                                                            backgroundImage: typeof item.options.backgroundLine !== 'undefined' ? nestBackImage : '',
                                                            backgroundRepeat: 'no-repeat',
                                                            backgroundSize: 'contain',
                                                            backgroundPosition: 'bottom center',
                                                            marginLeft: 'auto',
                                                            marginRight: 'auto',
                                                        }}
                                                        className="wrapSlide"
                                                    >
                                                        <header>
                                                            <div className="presentationHeader">
                                                                <div>
                                                                    <Link
                                                                        to={`/webapp/${idGoogleSheet}/review`}
                                                                    >
                                                                        <img src="/static/img/Octopus_Wealth.svg" alt="Logo Octopus" />
                                                                    </Link>
                                                                </div>
                                                                {
                                                                    filteredData.length > 0 && (
                                                                        <div>
                                                                            {self.state.globalClient}
                                                                            {self.state.globalPartner && ` & ${self.state.globalPartner}`}
                                                                            {
                                                                                fullScreen && (
                                                                                    <button
                                                                                        type="button"
                                                                                        className="btn_icon close_presentation"
                                                                                        onClick={this.changeState('fullScreen', !fullScreen)}
                                                                                    >
                                                                                        <img src="/static/img/close.svg" alt="exit fullscreen" />
                                                                                    </button>
                                                                                )
                                                                            }
                                                                        </div>
                                                                    )
                                                                }
                                                            </div>
                                                        </header>

                                                        {(currentSlide > i || printPage) && (
                                                            <div style={{
                                                                width: self.state.width,
                                                                height: self.state.height - 50,
                                                                display: 'flex',
                                                                alignItems: 'center',
                                                            }}
                                                            >
                                                                {
                                                                    item.dataType === 'table' && (
                                                                        <div>
                                                                            <div className="heading_title">
                                                                                <div>
                                                                                    <h3>{item.title}</h3>
                                                                                    {
                                                                                        item.titleCompare &&
                                                                                        <h4>{item.titleCompare}</h4>
                                                                                    }
                                                                                </div>
                                                                            </div>
                                                                            <Table
                                                                                plan={item.plan}
                                                                                planCompare={item.planCompare}
                                                                                assumesPortfolio={item.assumesPortfolio}
                                                                                chartsType={item.chartsType}
                                                                                childName1={item.childA}
                                                                                childName2={item.areaLabel}
                                                                                label_area={item.areaLabel}
                                                                                competition={item.competition}
                                                                                data={item.data}
                                                                                edit={false}
                                                                                height={self.state.height - 200}
                                                                                idGoogle={self.state.idGoogleSheet}
                                                                                ihtAVal={parseFloat(item.ihta)}
                                                                                ihtBVal={parseFloat(item.ihtb)}
                                                                                label1={item.label1}
                                                                                label2={item.label2}
                                                                                label3={item.label3}
                                                                                label4={item.label4}
                                                                                totalTER={1000}
                                                                                octopusCarib={item.octopusCarib}
                                                                                year={parseFloat(item.year)}
                                                                                width={self.state.width}
                                                                                step={self.state.step}
                                                                                options={item.options}
                                                                                title={item.title}
                                                                                titleCompare={item.titleCompare}
                                                                            />
                                                                        </div>
                                                                    )
                                                                }
                                                                {
                                                                    item.dataType === 'chart' && (
                                                                        <div>
                                                                            <div className="heading_title">
                                                                                <div>
                                                                                    <h3>{item.title}</h3>
                                                                                    {
                                                                                        item.titleCompare &&
                                                                                        <h4>{item.titleCompare}</h4>
                                                                                    }
                                                                                </div>
                                                                                {
                                                                                    viewLegend && (
                                                                                        <div
                                                                                            className={(item.label3 && !item.clientCase) ? 'legendChart' : 'legendChart columnInverse'}
                                                                                        >
                                                                                            {
                                                                                                (
                                                                                                    (item.label3 && !item.label6 && !item.clientCase) ||
                                                                                                    (item.label6 && item.options.bprNlEnable && item.options.bprEnable && curStep > 5) ||
                                                                                                    (item.label6 && item.options.bprNlEnable === false && item.options.bprEnable === true && curStep > 4) ||
                                                                                                    (item.label6 && item.options.bprNlEnable === true && item.options.bprEnable === false && curStep > 4) ||
                                                                                                    (item.label6 && !item.options.bprNlEnable && !item.options.bprEnable && curStep > 3) ||
                                                                                                    (item.label6 && item.options.bprNlEnable === false && item.options.bprEnable === false && curStep > 3)) && (
                                                                                                    <div>
                                                                                                        {item.label3}
                                                                                                        <span
                                                                                                            style={{ backgroundColor: item.color3 }}
                                                                                                        />
                                                                                                    </div>
                                                                                                )
                                                                                            }
                                                                                            {((
                                                                                                item.label1 && !item.label6) ||
                                                                                                (item.label6 && item.options.bprNlEnable && curStep > 5) ||
                                                                                                (item.label6 && item.options.bprNlEnable && item.options.bprEnable === false && curStep > 3))
                                                                                            && (
                                                                                                <div>
                                                                                                    {item.label1}
                                                                                                    <span
                                                                                                        style={{ backgroundColor: item.color1 }}
                                                                                                    />
                                                                                                </div>
                                                                                            )
                                                                                            }
                                                                                            {((item.label2 && !item.label6) || (item.label6 && item.options.bprEnable && curStep > 3)) && (
                                                                                                <div>
                                                                                                    {item.label2}
                                                                                                    <span
                                                                                                        style={{ backgroundColor: item.color2 }}
                                                                                                    />
                                                                                                </div>
                                                                                            )
                                                                                            }
                                                                                            {
                                                                                                (item.label4 && !item.clientCase && curStep > 2) && (
                                                                                                    <div>
                                                                                                        {item.label4}
                                                                                                        <span
                                                                                                            style={{ backgroundColor: item.color4 }}
                                                                                                        />
                                                                                                    </div>
                                                                                                )
                                                                                            }
                                                                                            {
                                                                                                (item.label5 && !item.clientCase && curStep > 1) && (
                                                                                                    <div>
                                                                                                        {item.label5}
                                                                                                        <span
                                                                                                            style={{ backgroundColor: item.color5 }}
                                                                                                        />
                                                                                                    </div>
                                                                                                )
                                                                                            }
                                                                                            {
                                                                                                (item.label6 && !item.clientCase && curStep > 0) && (
                                                                                                    <div>
                                                                                                        {item.label6}
                                                                                                        <span
                                                                                                            style={{ backgroundColor: item.color6 }}
                                                                                                        />
                                                                                                    </div>
                                                                                                )
                                                                                            }
                                                                                        </div>
                                                                                    )
                                                                                }
                                                                            </div>
                                                                            <div>
                                                                                <div>
                                                                                    <LineCharts
                                                                                        plan={item.plan}
                                                                                        planCompare={item.planCompare}
                                                                                        editAreaLabel={typeof item.options.viewAreaLabel !== 'undefined' ? nestAreaLabel : parseInt(item.areaLabel, 10)}
                                                                                        chartsType={item.chartsType}
                                                                                        chartsCompareType={item.chartCompareType}
                                                                                        client={item.client}
                                                                                        clientCase={item.clientCase}
                                                                                        color1={item.color1}
                                                                                        color2={item.color2}
                                                                                        color3={item.color3}
                                                                                        color4={item.color4}
                                                                                        color5={item.color5}
                                                                                        color6={item.color6}
                                                                                        compare={item.compareGraph}
                                                                                        competition={item.competition}
                                                                                        data={item.data}
                                                                                        differenceAge={item.differenceAge}
                                                                                        edit={false}
                                                                                        editAxis={item.editAxis}
                                                                                        editIcon={item.icon}
                                                                                        editLabel={item.editLabel}
                                                                                        editPosition={item.editPosition}
                                                                                        editPositionX={item.editPositionX}
                                                                                        editPositionDot={item.editPositionDot}
                                                                                        editPositionDotX={item.editPositionDotX}
                                                                                        editPositionLabel={item.editPositionLabel}
                                                                                        editPositionLabelX={item.editPositionLabelX}
                                                                                        editLine={item.editLine}
                                                                                        id={item.id}
                                                                                        labelChart={item.label}
                                                                                        label1={item.label1}
                                                                                        label2={item.label2}
                                                                                        label3={item.label3}
                                                                                        label4={item.label4}
                                                                                        label5={item.label5}
                                                                                        label6={item.label6}
                                                                                        minAxis={item.minAxis}
                                                                                        maxAxis={item.maxAxis}
                                                                                        minAxisAllChart={item.chartScaleMin1}
                                                                                        maxAxisAllChart={item.clientCase ? item.chartScaleMax1 : slidePresentation.maxAxisClient}
                                                                                        charScale={item.chartScale}
                                                                                        minAxisAllChart1={minAxisClient1}
                                                                                        maxAxisAllChart1={maxAxisClient1}
                                                                                        minAxisAllChart2={minAxisClient2}
                                                                                        maxAxisAllChart2={maxAxisClient2}
                                                                                        minAxisAllChart3={minAxisClient3}
                                                                                        maxAxisAllChart3={maxAxisClient3}
                                                                                        octopusCarib={item.octopusCarib}
                                                                                        options={item.options}
                                                                                        partner={item.partner}
                                                                                        width={self.state.width}
                                                                                        height={height - 100}
                                                                                        year={item.year}
                                                                                        zoom={zoom}
                                                                                        exclude={item.exclude}
                                                                                        step={step}
                                                                                        viewAxis={typeof item.options.viewAxis !== 'undefined' ? item.options.viewAxis : true}
                                                                                        viewAxisY={typeof item.options.viewAxisY !== 'undefined' ? item.options.viewAxisY : false}
                                                                                        viewAxisYInterval={viewAxisYInterval}
                                                                                    />
                                                                                </div>
                                                                                {
                                                                                    typeof item.options.viewTextAfterChart !== 'undefined' && (
                                                                                        <Fragment>
                                                                                            {
                                                                                                item.options.viewTextAfterChart && (
                                                                                                    <div className="moreInfoTextChart">
                                                                                                        <h6>{item.options.title}</h6>
                                                                                                        <p>{item.options.text}</p>
                                                                                                    </div>
                                                                                                )
                                                                                            }
                                                                                        </Fragment>
                                                                                    )
                                                                                }
                                                                            </div>
                                                                        </div>
                                                                    )
                                                                }
                                                                {
                                                                    item.dataType === 'title' && (
                                                                        <div className="col-md-12">
                                                                            <div className="heading_title_slide">
                                                                                {
                                                                                    typeof item.options.backgroundLine !== 'undefined' ? (
                                                                                        <Fragment>
                                                                                            {
                                                                                                item.options.backgroundLine
                                                                                                    ? (
                                                                                                        <h1 dangerouslySetInnerHTML={{ __html: `${JSON.parse(item.title).title} <span>${JSON.parse(item.title).lightTitle}</span>` }} />
                                                                                                    )
                                                                                                    : (
                                                                                                        <h1>{item.title}</h1>
                                                                                                    )
                                                                                            }
                                                                                        </Fragment>
                                                                                    )
                                                                                        : (
                                                                                            <h1>{item.title}</h1>
                                                                                        )
                                                                                }
                                                                                <h2>{item.titleCompare}</h2>
                                                                            </div>
                                                                        </div>
                                                                    )
                                                                }
                                                                {
                                                                    item.dataType === 'box' && (
                                                                        <div>
                                                                            <div className="heading_title marginTopPres">
                                                                                <div>
                                                                                    <h3>{item.title}</h3>
                                                                                    {
                                                                                        item.titleCompare && (
                                                                                            <h4>{item.titleCompare}</h4>
                                                                                        )
                                                                                    }
                                                                                </div>
                                                                            </div>
                                                                            <Box
                                                                                arrayLabel1={item.arrayLabel1}
                                                                                arrayLabel2={item.arrayLabel2}
                                                                                arrayLabel3={item.arrayLabel3}
                                                                                arrayLabel4={item.arrayLabel4}
                                                                                edit={false}
                                                                                icon1={item.arrayLabel1.icon}
                                                                                icon2={item.arrayLabel2.icon}
                                                                                icon3={item.arrayLabel3.icon}
                                                                                icon4={item.arrayLabel4.icon}
                                                                                label1={item.label1}
                                                                                label2={item.label2}
                                                                                label3={item.label3}
                                                                                label4={item.label4}
                                                                                numBox={parseInt(item.chartsType, 10)}
                                                                                width={width}
                                                                                height={height - 200}
                                                                            />
                                                                        </div>
                                                                    )
                                                                }
                                                                {
                                                                    item.dataType === 'text_description' && (
                                                                        <div>
                                                                            <div className="heading_title">
                                                                                <div>
                                                                                    <h3>{item.title}</h3>
                                                                                    {
                                                                                        item.titleCompare && (
                                                                                            <h4 dangerouslySetInnerHTML={{ __html: item.titleCompare }} />
                                                                                        )
                                                                                    }
                                                                                </div>
                                                                            </div>
                                                                            <Text
                                                                                edit={false}
                                                                                icon1=""
                                                                                icon2=""
                                                                                icon3=""
                                                                                icon4=""
                                                                                label1={item.label1}
                                                                                label2={item.label2}
                                                                                label3={item.label3}
                                                                                label4={item.label4}
                                                                                chartsType={item.chartsType}
                                                                                width={width}
                                                                                height={height - 200}
                                                                            />
                                                                        </div>
                                                                    )
                                                                }
                                                            </div>
                                                        )
                                                        }
                                                    </div>
                                                </div>
                                                <div className="col-md-6 paddingRight40" style={{ display: fullScreen ? 'none' : 'block' }}>
                                                    <div className="generalNotes">
                                                        <h3>GENERAL NOTES</h3>
                                                        <ul>
                                                            {
                                                                gloabalNotes.map((itemNote, f) => (
                                                                    <li key={f.toString()}>
                                                                        <input
                                                                            id={`general${f}`}
                                                                            type="checkbox"
                                                                            name={`general${f}`}
                                                                            checked={parseInt(itemNote.done, 10) === 1}
                                                                            onChange={checkToDoList(this, 'general', f, 0)}
                                                                        />
                                                                        <label
                                                                            htmlFor={`general${f}`}
                                                                            dangerouslySetInnerHTML={{ __html: itemNote.text }}
                                                                        />
                                                                    </li>
                                                                ))
                                                            }
                                                        </ul>
                                                    </div>
                                                    <div className="slideNotes">
                                                        <h3>
                                                            SLIDE NOTES
                                                            {item.chartScale && item.chartScale !== 1 && ` - SCALE ${item.chartScale}`}
                                                        </h3>
                                                        <ul>
                                                            {
                                                                item.notes.map((itemSingNote, f) => (
                                                                    <li key={f.toString()}>
                                                                        <input
                                                                            id={`slide${f}${i}`}
                                                                            type="checkbox"
                                                                            name={`slide${f}${i}`}
                                                                            checked={parseInt(itemSingNote.done, 10) === 1}
                                                                            onChange={checkToDoList(this, 'slide', f, i)}
                                                                        />
                                                                        <label
                                                                            htmlFor={`slide${f}${i}`}
                                                                            dangerouslySetInnerHTML={{ __html: itemSingNote.text }}
                                                                        />
                                                                    </li>
                                                                ))
                                                            }
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    );
                                }
                                return null;
                            })
                        }
                    </Slider>
                </div>
            </div>
        );
    }
}

navPresentation.propTypes = {
    history: PropTypes.object.isRequired,
    formValue: PropTypes.object,
    allData: PropTypes.object,
    usersOptions: PropTypes.object,
    slidePresentation: PropTypes.object,
    allDataDef: PropTypes.arrayOf(PropTypes.object),
    idGoogle: PropTypes.string,
};

navPresentation.defaultProps = {
    formValue: {},
    allData: {},
    usersOptions: {},
    slidePresentation: {},
    allDataDef: [],
    idGoogle: null,
};

const mapStateToProps = (state, props) => ({
    initialValues: {},
    allData: get(state, 'webapp', {}),
    slidePresentation: get(state, 'webapp.slidePresentation', {}),
    gData: get(state, 'webapp.slidePresentation.gData', []),
    allDataDef: get(state, 'webapp.slidePresentation.allDataDef', []),
    idGoogle: get(props, 'match.params.name', null),
});

const mapDispatchToProps = dispatch => ({
    changeFieldValue: (field, value) => {
        dispatch(change('addISA', field, value));
    },
});


export default withRouter(connect(mapStateToProps, mapDispatchToProps)(navPresentation));
