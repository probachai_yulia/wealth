/* eslint-disable max-len */
/* eslint react/forbid-prop-types:0 */
import React, { Component } from 'react';
import { Link, withRouter } from 'react-router-dom';
import PropTypes from 'prop-types';
import Loadable from 'react-loadable';
import get from 'lodash/get';
import TextareaAutosize from 'react-autosize-textarea';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import isNaN from 'lodash/isNaN';
import { getCurrentIcon } from '../../../components/Charts/assetsIcon';
import Preload from '../../../components/Loading/preload';
import { loadDataGraph } from '../../../helpers/WebappPlanFunctions';
import { reloadPage, savePlan } from '../../../actions/WebappActions';
import { requestsReset } from '../../../actions/CommonActions';
import { Loading } from '../../../components/Loading';
// import Table from '../../../components/Table/table';
const Table = Loadable({
    loader: () => import('../../../components/Table/table' /* webpackChunkName: "table" */),
    loading: Loading,
});
// import Box from '../../../components/Box_Text/box';
const Box = Loadable({
    loader: () => import('../../../components/BoxText/box' /* webpackChunkName: "box" */),
    loading: Loading,
});
// import Text from '../../../components/Box_Text/text';
const Text = Loadable({
    loader: () => import('../../../components/BoxText/text' /* webpackChunkName: "text" */),
    loading: Loading,
});
// import LineCharts from '../../../components/Charts/LineCharts';
const LineCharts = Loadable({
    loader: () => import('../../../components/Charts/LineCharts' /* webpackChunkName: "line-chart" */),
    loading: Loading,
});

class Slide extends Component {
    constructor(props) {
        super(props);
        const {
            slideUUID,
        } = this.props;
        this.state = {
            // accessToken: localStorage.getItem('token') || '',
            printPage: false,
            areaLabel: 0,
            allData: [],
            // allDataDef: [],
            assumesPortfolio: '',
            changeSingleIcon: null,
            // chartName: '',
            chartsType: '',
            childName1: '',
            childName2: '',
            client: '',
            color1: '',
            color2: '',
            color3: '',
            color4: '',
            color5: '',
            color6: '',
            compare: false,
            compareGraph: false,
            competition: '',
            data: [],
            defaultData: [],
            differenceAge: '',
            editLabel: [],
            editIcon: [],
            editType: '',
            gData: [],
            idGoogleSheet: slideUUID,
            ihtAVal: 0,
            ihtBVal: 0,
            // firstLoad: false,
            label: '',
            label1: '',
            label2: '',
            label3: '',
            label4: '',
            label5: '',
            label6: '',
            // labelCompare: '',
            loadingGraph: false,
            // modalToSave: false,
            // orderModal: 0,
            octopusCarib: '',
            partner: '',
            popIcon: false,
            saved: false,
            // setIcon: [],
            // sheets: [],
            sheetsList: [],
            sheetSel: '',
            sheetSelCompare: '',
            title: '',
            titleCompare: '',
            totalTER: 0,
            toSave: false,
            year: '',
            // activeSection: 0,
            // modalAction: '',
            // onScroll: false,
            // loadNewReview: false,
            loading: true,
            // position: false,
            width: 1440,
            height: 810,
            editPositionLabel: 0,
            editPositionLabelX: -100,
            // gloabalNotes: [],
            editPosition: '',
            editPositionX: '',
            editAxis: '',
            editLine: '',
            chartsCompareType: 'line',
            editPositionDot: '',
            editPositionDotX: '',
            arrayLabel1: {
                icon: '',
                title: '',
                text: '',
            },
            arrayLabel2: {
                icon: '',
                title: '',
                text: '',
            },
            arrayLabel3: {
                icon: '',
                title: '',
                text: '',
            },
            arrayLabel4: {
                icon: '',
                title: '',
                text: '',
            },
            step: 10,
            loadPlan: true,
            reloadPlan: false,
            reloadPrePlan: false,
            chartScale: 1,
            // toReload: false,
            changeSlideData: false,
            viewOption: false,
            viewLegend: true,
            viewText: false,
            viewAreaLabel: true,
            viewAxis: true,
            viewAxisY: false,
            viewAxisYInterval: 5,
            input1: 'For illustrative purposes only',
            textarea2: 'This draft LifeLine cannot be taken as advice. It is a draft based on the limited information we have learnt from you up to now including your personal goals, and a range of assumptions. If we go ahead, we’ll be developing this into a more detailed picture. The full version of your LifeLine then becomes the yardstick against which we measure progress against your goals, and determine priorities and advice.',
            viewLine: false,
            titleLight: '',
            nextStep: '',
            order: false,
            // changeSingleIcon: true,
            idIcon: 1,
            editAreaLabel: '',
            type: '',
            bprEnable: false,
            bprNlEnable: false,
            numBox: null,
            dataType: '',
            activeLabel: null,
            activeIcon: null,
            editId: null,
            elemType: null,
            activeLine: null,
            arrayId: null,
            activeAxis: null,
            clientName: '',
            partnerName: '',
            icon1: '',
            icon2: '',
            icon3: '',
            icon4: '',
            oldSubtitle: '',
            oldTitle: '',
            advisorName: '',
            options: {},
            maxAxisClient: null,
            minAxisClient: null,
            saving: false,
            clientCase: false,
            exclude: 0,
            minAxisClient1: null,
            minAxisClient2: null,
            minAxisClient3: null,
            maxAxisClient1: null,
            maxAxisClient2: null,
            maxAxisClient3: null,
            yourPriorities: null,
        };

        this.changeLabel = this.changeLabel.bind(this);
        this.loadData = this.loadData.bind(this);
    }

    componentDidMount() {
        const { allAssetPlan } = this.props;
        this.loadData();
        this.setState({
            yourPriorities: allAssetPlan.priorities,
            defaultData: allAssetPlan.default_value,
        });
    }

    componentDidUpdate() {
        const {
            handleGetPlan, allAssetPlan, isRequestedDone, handleRequestsReset, slideUUID, slidePresentation, orderSlide, idGoogle,
        } = this.props;
        const {
            editType, reloadPrePlan, loadPlan, reloadPlan, order, viewLegend, viewText, viewAreaLabel, input1, textarea2, viewAxis, viewAxisY,
            viewAxisYInterval, bprEnable, bprNlEnable, title, titleLight, viewLine, type, sheetSel, sheetSelCompare, titleCompare,
            arrayLabel1, label1, arrayLabel2, label2, arrayLabel3, label3, arrayLabel4, label4, label5, label6, color1, color2, color3, color4, color5,
            color6, chartsType, dataType,
            chartsCompareType, editIcon, editLabel, editPosition, editPositionX, editPositionDot, editPositionDotX, editAxis, editLine, ihtAVal,
            ihtBVal, childName1, year, octopusCarib, competition,
            assumesPortfolio, numBox, changeSlideData, childName2, areaLabel,
        } = this.state;
        const self = this;
        if (loadPlan || reloadPlan) {
            this.loadData();
        }
        if (isRequestedDone === 1) {
            self.setState({
                saving: false,
                toSave: false,
                saved: true,
            });
            setTimeout(() => {
                self.setState({
                    saved: false,
                });
            }, 3000);
            handleRequestsReset();
        }
        if (reloadPrePlan) {
            let childNameNew2;
            let orderNew;
            let scale;
            let options;
            const allData = allAssetPlan;
            const arrayAll = allData.presentation;
            if (editType === 'table') {
                childNameNew2 = childName2;
            } else {
                childNameNew2 = areaLabel;
            }
            if (slideUUID !== null) {
                orderNew = parseInt(order, 10);
                scale = slidePresentation.allDataDef[self.props.match.params.id].chartScale;
            } else {
                if (order) {
                    orderNew = parseFloat(orderSlide) + 0.01;
                } else {
                    orderNew = 100;
                }
                scale = 1;
            }
            if (type === 'lifeline') {
                options = JSON.stringify(
                    {
                        viewLegend,
                        viewTextAfterChart: viewText,
                        viewAreaLabel,
                        title: input1,
                        text: textarea2,
                        viewAxis,
                        viewAxisY,
                        viewAxisYInterval,
                        bprEnable,
                        bprNlEnable,
                    },
                );
            }
            let titleNew;
            if (viewLine) {
                titleNew = JSON.stringify(
                    {
                        title,
                        lightTitle: titleLight,
                    },
                );
                options = JSON.stringify(
                    {
                        backgroundLine: true,
                    },
                );
            } else {
                titleNew = title;
            }
            if (editType === 'table') {
                options = JSON.stringify(
                    {
                        bprEnable,
                        bprNlEnable,
                    },
                );
            }
            if (type === 'lifeline' && slideUUID !== null) {
                options = JSON.stringify(
                    {
                        bprEnable,
                        bprNlEnable,
                    },
                );
            }
            let color5New;
            let label5New;
            if (label6 && label5) {
                label5New = `${label5};${label6}`;
                color5New = `${color5};${color6}`;
            } else if (label6) {
                label5New = label6;
                color5New = color6;
            } else if (label5) {
                label5New = label5;
                color5New = color5;
            }
            let chartsTypeNew;
            switch (dataType) {
                case 'box':
                    chartsTypeNew = numBox;
                    break;
                case 'pensions_contribution':
                    chartsTypeNew = 'pensions_contribution';
                    break;
                default:
                    chartsTypeNew = chartsType;
            }
            let typeNew;
            switch (chartsTypeNew) {
                case 'pensions_contribution':
                    typeNew = 'table';
                    break;
                default:
                    typeNew = dataType;
            }
            const curData = [
                idGoogle,
                type === 'lifeline' ? 'chart' : typeNew,
                sheetSel,
                sheetSelCompare,
                chartsTypeNew,
                titleNew,
                titleCompare,
                dataType === 'box' ? JSON.stringify(arrayLabel1) : label1,
                dataType === 'box' ? JSON.stringify(arrayLabel2) : label2,
                dataType === 'box' ? JSON.stringify(arrayLabel3) : label3,
                dataType === 'box' ? JSON.stringify(arrayLabel4) : label4,
                label5New,
                color1,
                color2,
                color3,
                color4,
                color5New,
                chartsCompareType,
                typeof editIcon !== 'undefined' ? editIcon.toString() : '',
                typeof editLabel !== 'undefined' ? editLabel.toString() : '',
                typeof editPosition !== 'undefined' ? editPosition.toString() : '',
                typeof editPositionX !== 'undefined' ? editPositionX.toString() : '',
                typeof editPositionDot !== 'undefined' ? editPositionDot.toString() : '',
                typeof editPositionDotX !== 'undefined' ? editPositionDotX.toString() : '',
                typeof editAxis !== 'undefined' ? editAxis.toString() : '',
                typeof editLine !== 'undefined' ? editLine.toString() : '',
                ihtAVal,
                ihtBVal,
                childName1,
                childNameNew2,
                year,
                octopusCarib,
                competition,
                assumesPortfolio,
                orderNew,
                0,
                '',
                '',
                '',
                isNaN(scale) ? 1 : scale,
                options,
            ];
            if (slideUUID !== 'undefined') {
                arrayAll.map((item, i) => {
                    if (i === (parseInt(orderSlide, 10) + 1)) {
                        arrayAll[i] = curData;
                    }
                    return null;
                });
            } else {
                arrayAll.push(curData);
            }
            handleGetPlan(arrayAll);
            self.setState({
                reloadPlan: true,
                reloadPrePlan: false,
            });
        }
        /* if (typeof this.props.actionWebappButton.type !== 'undefined') {
            const { onSaveData } = this.props;
            if (this.props.actionWebappButton.data === 'save_login') {
                onSaveData(gData, idGoogleSheet, 0, 'review', 'login');
            } else {
                onSaveData(gData, idGoogleSheet, 0, 'review', '');
            }
        } */
        if (changeSlideData) {
            this.changeSlideData();
            self.setState({ changeSlideData: false });
        }
    }

    loadData() {
        const self = this;
        const {
            slidePresentation, allAssetPlan, slideUUID, orderSlide,
        } = this.props;
        const {
            loadPlan, reloadPlan,
        } = this.state;
        const allData = allAssetPlan;
        let pres;
        const {
            gsheet,
        } = allAssetPlan;
        if (loadPlan) {
            pres = allAssetPlan.presentation;
        } else {
            pres = allAssetPlan.presentation;
        }
        const getData = slidePresentation.allDataDef;
        const listSheets = [];
        Object.keys(gsheet)
            .map((item) => {
                listSheets.push(item);
                return null;
            });
        listSheets.sort((a, b) => {
            const keyA = a;
            const keyB = b;
            if (keyA < keyB) return -1;
            if (keyA > keyB) return 1;
            return 0;
        });
        if (slideUUID !== null) {
            const curData = slidePresentation;
            const val = curData.allDataDef[slideUUID];
            let titleNew;
            let titleCompare;
            let title2;
            if (typeof val !== 'undefined') {
                titleNew = typeof val.title !== 'undefined' ? val.title.trim() : '';
                titleCompare = typeof val.titleCompare !== 'undefined' ? val.titleCompare.trim() : '';
                let typeNew;
                let chartsTypeNew = val.chartsType;
                switch (val.chartsType) {
                    case 'our_pricing':
                    case 'our_pricing_competition':
                    case 'what_will_cost':
                        typeNew = 'pricing';
                        break;
                    case 'let_started':
                    case 'title':
                        typeNew = 'other';
                        break;
                    case 'legacy':
                    case 'legacy_detailed':
                    case 'legacy_tax':
                    case 'legacy_tax_detailed':
                        typeNew = 'iht';
                        break;
                    default:
                        typeNew = '';
                }
                switch (val.dataType) {
                    case 'chart':
                        typeNew = 'lifeline';
                        chartsTypeNew = val.chartsType;
                        break;
                    case 'box':
                        typeNew = 'other';
                        chartsTypeNew = 'box';
                        break;
                    case 'pensions_contribution':
                        typeNew = 'other';
                        chartsTypeNew = 'pensions_contribution';
                        break;
                    case 'title':
                        typeNew = 'other';
                        chartsTypeNew = 'title';
                        if (typeof val.options.backgroundLine !== 'undefined') {
                            if (val.options.backgroundLine) {
                                title2 = JSON.parse(titleNew).lightTitle;
                                titleNew = JSON.parse(titleNew).title;
                            }
                        }
                        break;
                    default:
                        typeNew = '';
                        chartsTypeNew = val.chartsType;
                }
                const nestAreaLabelInfo = isNaN(val.areaLabel) ? 0 : val.areaLabel;
                console.log(val.areaLabel.split(';').length > 1 ? val.areaLabel.split(';')[0] : nestAreaLabelInfo);
                self.setState({
                    loadPlan: false,
                    reloadPlan: false,
                    areaLabel: val.areaLabel.split(';').length > 1 ? val.areaLabel.split(';')[0] : nestAreaLabelInfo,
                    chartName: val.titleCompare,
                    clientCase: val.clientCase,
                    gData: pres,
                    typeNew,
                    data: val.data,
                    dataType: val.dataType,
                    editType: val.dataType,
                    sheetSel: val.plan,
                    sheetSelCompare: val.planCompare,
                    chartsType: chartsTypeNew,
                    compare: true || false,
                    numBox: val.chartsType,
                    title: titleNew,
                    titleLight: title2,
                    titleCompare,
                    label1: val.label1,
                    label2: val.label2,
                    label3: val.label3,
                    label4: val.label4,
                    label5: val.label5,
                    label6: val.label6,
                    color1: val.color1,
                    color2: val.color2,
                    color3: val.color3,
                    color4: val.color4,
                    color5: val.color5,
                    color6: val.color6,
                    editIcon: val.icon,
                    editLabel: val.editLabel,
                    editPosition: val.editPosition,
                    editPositionX: val.editPositionX,
                    editPositionDot: val.editPositionDot,
                    editPositionDotX: val.editPositionDotX,
                    editPositionLabel: val.areaLabel.split(';').length > 1 ? val.areaLabel.split(';')[1].split(':')[1] : 0,
                    editPositionLabelX: val.areaLabel.split(';').length > 1 ? val.areaLabel.split(';')[1].split(':')[0] : -100,
                    chartsCompareType: val.chartCompareType,
                    editAxis: val.editAxis,
                    editLine: val.editLine,
                    ihtAVal: val.ihta,
                    ihtBVal: val.ihtb,
                    childName1: val.childA,
                    childName2: val.areaLabel,
                    year: val.year,
                    options: val.options || { bprEnable: false, bprNlEnable: false },
                    octopusCarib: val.octopusCarib,
                    competition: val.competition,
                    assumesPortfolio: val.assumesPortfolio,
                    order: val.order,
                    exclude: val.exclude,
                    client: pres[0][0] !== '0. Google Sheet' ? pres[0][0] : val.client,
                    partner: pres[0][1] !== '0. Data Type' ? pres[0][1] : val.partner,
                    clientName: pres[0][0] !== '0. Google Sheet' ? pres[0][0] : val.client,
                    partnerName: pres[0][1] !== '0. Data Type' ? pres[0][1] : val.partner,
                    compareGraph: val.compareGraph,
                    differenceAge: val.differenceAge,
                    arrayLabel1: val.arrayLabel1,
                    arrayLabel2: val.arrayLabel2,
                    arrayLabel3: val.arrayLabel3,
                    arrayLabel4: val.arrayLabel4,
                    icon1: (typeof val.arrayLabel1.icon !== 'undefined') ? val.arrayLabel1.icon : '',
                    icon2: (typeof val.arrayLabel2.icon !== 'undefined') ? val.arrayLabel2.icon : '',
                    icon3: (typeof val.arrayLabel3.icon !== 'undefined') ? val.arrayLabel3.icon : '',
                    icon4: (typeof val.arrayLabel4.icon !== 'undefined') ? val.arrayLabel4.icon : '',
                    loadingGraph: true,
                    allDataDef: getData,
                    allData: gsheet,
                    loading: false,
                    sheetsList: listSheets,
                    maxAxisClient: curData.maxAxisClient,
                    minAxisClient: curData.minAxisClient,
                    globalClient: getData.globalClient,
                    globalPartner: getData.globalPartner,
                    gloabalNotes: getData.globalNotes,
                    advisorName: slidePresentation.advisor,
                    loadNewReview: false,
                    maxAxisClient1: curData.maxAxisClient1,
                    minAxisClient1: curData.minAxisClient1,
                    maxAxisClient2: curData.maxAxisClient2,
                    minAxisClient2: curData.minAxisClient2,
                    maxAxisClient3: curData.maxAxisClient3,
                    minAxisClient3: curData.minAxisClient3,
                    chartScale: val.chartScale,
                    viewLegend: typeof val.options.viewLegend !== 'undefined' ? val.options.viewLegend : true,
                    viewText: typeof val.options.viewTextAfterChart !== 'undefined' ? val.options.viewTextAfterChart : false,
                    viewAreaLabel: typeof val.options.viewAreaLabel !== 'undefined' ? val.options.viewAreaLabel : true,
                    viewAxis: typeof val.options.viewAxis !== 'undefined' ? val.options.viewAxis : true,
                    viewAxisY: typeof val.options.viewAxisY !== 'undefined' ? val.options.viewAxisY : false,
                    viewAxisYInterval: typeof val.options.viewAxisYInterval !== 'undefined' ? val.options.viewAxisYInterval : 5,
                    viewLine: typeof val.options.backgroundLine !== 'undefined' ? val.options.backgroundLine : false,
                    bprEnable: val.options.bprEnable,
                    bprNlEnable: val.options.bprNlEnable,
                });
            }
        } else if (reloadPlan) {
            const newData = slidePresentation;
            const val = newData.allDataDef[pres.length - 2];
            if (typeof val !== 'undefined') {
                self.setState({
                    loadPlan: false,
                    reloadPlan: false,
                    data: val.data,
                    editIcon: val.icon,
                    editLabel: val.editLabel,
                    allDataDef: getData.allDataDef,
                    maxAxisClient: newData.maxAxisClient,
                    minAxisClient: newData.minAxisClient,
                    globalClient: newData.globalClient,
                    globalPartner: newData.globalPartner,
                    gloabalNotes: newData.globalNotes,
                    advisorName: newData.advisor,
                    maxAxisClient1: newData.maxAxisClient1,
                    minAxisClient1: newData.minAxisClient1,
                    maxAxisClient2: newData.maxAxisClient2,
                    minAxisClient2: newData.minAxisClient2,
                    maxAxisClient3: newData.maxAxisClient3,
                    minAxisClient3: newData.minAxisClient3,
                    chartScale: val.chartScale,
                    editLine: val.editLine,
                    editPosition: val.editPosition,
                    editPositionX: val.editPositionX,
                    editPositionLabel: val.editPositionLabel,
                    editPositionLabelX: val.editPositionLabelX,
                    editPositionDot: val.editPositionDot,
                    editPositionDotX: val.editPositionDotX,
                    editAxis: val.editAxis,
                });
            }
        } else {
            const val = slidePresentation.allDataDef[pres.length - 2];
            self.setState({
                loadPlan: false,
                reloadPlan: false,
                gData: allData.presentation,
                client: allData.presentation[0][0] !== '0. Google Sheet' ? allData.presentation[0][0] : val.client,
                partner: allData.presentation[0][1] !== '0. Data Type' ? allData.presentation[0][1] : val.partner,
                clientName: allData.presentation[0][0] !== '0. Google Sheet' ? allData.presentation[0][0] : val.client,
                partnerName: allData.presentation[0][1] !== '0. Data Type' ? allData.presentation[0][1] : val.partner,
                allDataDef: allData.slidePresentation.allDataDef,
                allData: allData.gsheet,
                loading: false,
                sheetsList: listSheets,
                maxAxisClient: allData.maxAxisClient,
                minAxisClient: allData.minAxisClient,
                globalClient: allData.globalClient,
                globalPartner: allData.globalPartner,
                gloabalNotes: allData.globalNotes,
                advisorName: allData.advisor,
                loadNewReview: false,
            });
        }
        if (orderSlide !== null) {
            this.setState({
                order: orderSlide,
            });
        }
    }

    saveSlideData(e, act) {
        e.preventDefault();
        const {
            onSaveData, slideUUID, orderSlide, idGoogle,
        } = this.props;
        const {
            editType, order, viewLegend, viewText, viewAreaLabel, input1, textarea2, viewAxis, viewAxisY,
            gData, editAreaLabel, editPositionLabelX, editPositionLabel,
            viewAxisYInterval, bprEnable, bprNlEnable, title, titleLight, viewLine, type, sheetSel, sheetSelCompare, titleCompare,
            arrayLabel1, label1, arrayLabel2, label2, arrayLabel3, label3, arrayLabel4, label4, label5, label6, color1, color2, color3, color4, color5,
            color6, chartsType, dataType,
            chartsCompareType, editIcon, editLabel, editPosition, editPositionX, editPositionDot, editPositionDotX, editAxis, editLine, ihtAVal,
            ihtBVal, childName1, year, octopusCarib, competition,
            assumesPortfolio, numBox, maxAxisClient, chartScale, childName2,
        } = this.state;
        let childName2New;
        let arrayNum = gData.length;
        let orderNew;
        if (slideUUID !== null) {
            arrayNum = parseInt(slideUUID, 10) + 1;
            const curOrd = gData[arrayNum];
            orderNew = curOrd[34];
        } else {
            arrayNum = gData.length;
            if (order) {
                orderNew = parseFloat(orderSlide) + 0.01;
            } else {
                orderNew = gData.length;
            }
        }
        if (editType === 'table') {
            childName2New = childName2;
        } else if (typeof editAreaLabel !== 'undefined') {
            childName2New = `${editAreaLabel};${editPositionLabelX}:${editPositionLabel}`;
        } else {
            childName2New = '';
        }
        let options;
        if (type === 'lifeline') {
            options = JSON.stringify(
                {
                    viewLegend,
                    viewTextAfterChart: viewText,
                    viewAreaLabel,
                    title: input1,
                    text: textarea2,
                    viewAxis,
                    viewAxisY,
                    viewAxisYInterval,
                    bprEnable,
                    bprNlEnable,
                },
            );
        }
        let titleNew;
        if (viewLine) {
            titleNew = JSON.stringify(
                {
                    title,
                    lightTitle: titleLight,
                },
            );
            options = JSON.stringify(
                {
                    backgroundLine: true,
                },
            );
        } else {
            titleNew = title;
        }
        if (editType === 'table') {
            options = JSON.stringify(
                {
                    bprEnable,
                    bprNlEnable,
                },
            );
        }
        let color5New;
        let label5New;
        if (label6 && label5) {
            label5New = `${label5};${label6}`;
            color5New = `${color5};${color6}`;
        } else if (label6) {
            label5New = label6;
            color5New = color6;
        } else if (label5) {
            label5New = label5;
            color5New = color5;
        }
        const nestType = (typeof dataType === 'undefined' ? 'Plan-A' : dataType);
        const curData = [
            idGoogle,
            type === 'lifeline' ? 'chart' : nestType,
            sheetSel,
            sheetSelCompare,
            dataType === 'box' ? numBox : chartsType,
            titleNew,
            titleCompare,
            dataType === 'box' ? JSON.stringify(arrayLabel1) : label1,
            dataType === 'box' ? JSON.stringify(arrayLabel2) : label2,
            dataType === 'box' ? JSON.stringify(arrayLabel3) : label3,
            dataType === 'box' ? JSON.stringify(arrayLabel4) : label4,
            typeof label5New === 'undefined' ? '' : label5New,
            color1,
            color2,
            color3,
            color4,
            typeof color5New === 'undefined' ? '' : color5New,
            chartsCompareType,
            typeof editIcon !== 'undefined' ? editIcon.toString() : '',
            typeof editLabel !== 'undefined' ? editLabel.toString() : '',
            typeof editPosition !== 'undefined' ? editPosition.toString() : '',
            typeof editPositionX !== 'undefined' ? editPositionX.toString() : '',
            typeof editPositionDot !== 'undefined' ? editPositionDot.toString() : '',
            typeof editPositionDotX !== 'undefined' ? editPositionDotX.toString() : '',
            typeof editAxis !== 'undefined' ? editAxis.toString() : '',
            typeof editLine !== 'undefined' ? editLine.toString() : '',
            ihtAVal,
            ihtBVal,
            childName1,
            childName2New,
            year,
            octopusCarib,
            competition,
            assumesPortfolio,
            `${orderNew.toString()}`,
            '0',
            '',
            '',
            '',
            isNaN(chartScale) ? '1' : chartScale.toString(),
            options,
        ];
        let action;
        if (order && slideUUID === null) {
            let newOrder = gData.length;
            if (orderSlide !== null) {
                newOrder = orderSlide;
            }
            gData.splice(parseFloat(newOrder) + 1, 0, curData);
            arrayNum = parseFloat(newOrder) + 1;
        } else {
            gData[arrayNum] = curData;
        }
        if (slideUUID === null) {
            action = 'add_new';
        } else {
            action = 'update';
        }
        const plan = [];
        if (act === 'single_save') {
            action = 'single_save';
        } else {
            let opt;
            try {
                opt = JSON.parse(options);
                if (opt.viewAxis === false) {
                    opt = false;
                } else {
                    opt = true;
                }
            } catch (f) {
                opt = true;
            }
            if (opt === true) {
                if (sheetSelCompare) {
                    plan[`${sheetSelCompare}`] = {
                        icon: gData[arrayNum][18],
                        label: gData[arrayNum][19],
                        axis: gData[arrayNum][24],
                        [`${gData[arrayNum][4]}_${gData[arrayNum][39]}`]: {
                            editPosition: gData[arrayNum][20],
                            editPositionX: gData[arrayNum][21],
                            editPositionDot: gData[arrayNum][22],
                            editPositionDotX: gData[arrayNum][23],
                            editLine: gData[arrayNum][25],
                            editArea: gData[arrayNum][29],
                        },
                    };
                } else {
                    plan[`${sheetSel}`] = {
                        icon: gData[arrayNum][18],
                        label: gData[arrayNum][19],
                        axis: gData[arrayNum][24],
                        [`${gData[arrayNum][4]}_${gData[arrayNum][39]}`]: {
                            editPosition: gData[arrayNum][20],
                            editPositionX: gData[arrayNum][21],
                            editPositionDot: gData[arrayNum][22],
                            editPositionDotX: gData[arrayNum][23],
                            editLine: gData[arrayNum][25],
                            editArea: gData[arrayNum][29],
                        },
                    };
                }
            }
            gData.map((item, i) => {
                if (i > 0) {
                    gData[i][34] = i;
                }
                if (item[0] === idGoogle && item[1] === 'chart') {
                    if (item[3] && item[3] !== '') {
                        if (typeof plan[item[3]] !== 'undefined') {
                            gData[i][18] = plan[item[3]].icon;
                            gData[i][19] = plan[item[3]].label;
                        } else {
                            plan[item[3]] = { icon: item[18], label: item[19] };
                        }
                    } else if (typeof plan[item[2]] !== 'undefined') {
                        gData[i][18] = plan[item[2]].icon;
                        gData[i][19] = plan[item[2]].label;
                    } else {
                        plan[item[2]] = { icon: item[18], label: item[19] };
                    }

                    let optionsSlide;
                    try {
                        optionsSlide = JSON.parse(item[40]);
                        if (optionsSlide.viewAxis === false) {
                            optionsSlide = false;
                        } else {
                            optionsSlide = true;
                        }
                    } catch (g) {
                        optionsSlide = true;
                    }
                    if (optionsSlide === true) {
                        if (item[3]) {
                            if (typeof plan[item[3]] !== 'undefined') {
                                if (typeof plan[item[3]][`${item[4]}_${item[39]}`] !== 'undefined') {
                                    gData[i][20] = plan[item[3]][`${item[4]}_${item[39]}`].editPosition;
                                    gData[i][21] = plan[item[3]][`${item[4]}_${item[39]}`].editPositionX;
                                    gData[i][22] = plan[item[3]][`${item[4]}_${item[39]}`].editPositionDot;
                                    gData[i][23] = plan[item[3]][`${item[4]}_${item[39]}`].editPositionDotX;
                                    gData[i][25] = plan[item[3]][`${item[4]}_${item[39]}`].editLine;
                                    gData[i][29] = plan[item[3]][`${item[4]}_${item[39]}`].editArea;
                                } else if (item[20] || item[21]) {
                                    plan[item[3]] = {
                                        icon: item[18],
                                        label: item[19],
                                        axis: item[24],
                                        [`${item[4]}_${item[39]}`]: {
                                            editPosition: item[20],
                                            editPositionX: item[21],
                                            editPositionDot: item[22],
                                            editPositionDotX: item[23],
                                            editLine: item[25],
                                            editArea: item[29],
                                        },
                                    };
                                }
                            } else if (item[20] || item[21]) {
                                plan[item[3]] = {
                                    icon: item[18],
                                    label: item[19],
                                    axis: item[24],
                                    [`${item[4]}_${item[39]}`]: {
                                        editPosition: item[20],
                                        editPositionX: item[21],
                                        editPositionDot: item[22],
                                        editPositionDotX: item[23],
                                        editLine: item[25],
                                        editArea: item[29],
                                    },
                                };
                            }
                        } else if (typeof plan[item[2]] !== 'undefined') {
                            if (typeof plan[item[2]][item[4]] !== 'undefined') {
                                gData[i][20] = plan[item[2]][`${item[4]}_${item[39]}`].editPosition;
                                gData[i][21] = plan[item[2]][`${item[4]}_${item[39]}`].editPositionX;
                                gData[i][22] = plan[item[2]][`${item[4]}_${item[39]}`].editPositionDot;
                                gData[i][23] = plan[item[2]][`${item[4]}_${item[39]}`].editPositionDotX;
                                gData[i][25] = plan[item[2]][`${item[4]}_${item[39]}`].editLine;
                                gData[i][29] = plan[item[2]][`${item[4]}_${item[39]}`].editArea;
                            } else if (item[20] || item[21]) {
                                plan[item[2]] = {
                                    icon: item[18],
                                    label: item[19],
                                    axis: item[24],
                                    [`${item[4]}_${item[39]}`]: {
                                        editPosition: item[20],
                                        editPositionX: item[21],
                                        editPositionDot: item[22],
                                        editPositionDotX: item[23],
                                        editLine: item[25],
                                        editArea: item[29],
                                    },
                                };
                            }
                        } else if (item[20] || item[21]) {
                            plan[item[2]] = {
                                icon: item[18],
                                label: item[19],
                                axis: item[24],
                                [`${item[4]}_${item[39]}`]: {
                                    editPosition: item[20],
                                    editPositionX: item[21],
                                    editPositionDot: item[22],
                                    editPositionDotX: item[23],
                                    editLine: item[25],
                                    editArea: item[29],
                                },
                            };
                        }
                    }
                }
                return null;
            });
        }
        this.setState({
            saving: true,
        });
        gData[0][36] = maxAxisClient;
        const payload = {
            gData, idGoogle, arrayNum, save_type: action,
        };
        onSaveData(payload);
    }

    saveNote(type, arrayId, val) {
        const {
            gData,
        } = this.state;
        const arrayData = gData;
        arrayData[arrayId][38] = JSON.stringify(val);
        this.setState({
            gData: arrayData,
            toSave: true,
        });
    }

    handleToUpdate(val, elemType, id) {
        const {
            editIcon, editLabel, editLine, popIcon,
        } = this.state;
        this.setState({
            activeIcon: elemType === 'single' ? editIcon[val] : editIcon[val].split(';')[id],
            activeLabel: elemType === 'single' ? editLabel[val] : editLabel[val].split(';')[id],
            activeLine: elemType === 'single' ? parseInt(editLine[val], 10) : parseInt(editLine[val].split(';')[id], 10),
            editId: val,
            elemType,
            arrayId: id,
            popIcon: !popIcon,
        });
    }

    changeIcon(icon, id) {
        const {
            idIcon, editType,
        } = this.state;
        const curID = id || idIcon;
        if (editType === 'box') {
            this.setState({
                [`icon${curID}`]: icon,
                popIcon: false,
                toSave: true,
                idIcon: curID,
            });
        } else {
            this.setState({
                activeIcon: icon,
                toSave: true,
            });
        }
    }

    changeLabel(val, id) {
        this.setState({
            [`arrayLabel${id}`]: val,
            [`label${id}`]: JSON.stringify(val),
            toSave: true,
        });
    }

    savePopIcon() {
        const {
            editLabel, editLine, editIcon, activeLabel, activeIcon, editId, elemType, activeLine, arrayId, popIcon,
        } = this.state;
        const arrayLabel = editLabel;
        const arrayLine = editLine;
        const arrayIcon = editIcon;

        if (elemType === 'single') {
            arrayLabel[editId] = activeLabel;
            arrayIcon[editId] = activeIcon === 'placeholder' ? '' : activeIcon;
            arrayLine[editId] = activeLine ? '1' : '0';
        } else {
            const singleLabel = arrayLabel[editId].split(';');
            const singleIcon = arrayIcon[editId].split(';');
            const singleLine = arrayLine[editId].split(';');
            singleLabel[arrayId] = activeLabel;
            singleIcon[arrayId] = activeIcon === 'placeholder' ? '' : activeIcon;
            singleLine[arrayId] = activeLine ? '1' : '0';
            arrayLabel[editId] = singleLabel.join(';');
            arrayIcon[editId] = singleIcon.join(';');
            arrayLine[editId] = singleLine.join(';');
        }
        this.setState({
            editLabel: arrayLabel,
            editLine: arrayLine,
            editIcon: arrayIcon,
            popIcon: !popIcon,
            toSave: true,
        });
    }

    editNewPosition(val, id, elemArray, arrayId, type) {
        const {
            editPosition, editPositionX, editPositionDot, editPositionDotX, editPositionLabel, editPositionLabelX, editAreaLabel,
        } = this.state;
        const arrayPosition = editPosition;
        const arrayPositionX = editPositionX;
        const arrayPositionDot = editPositionDot;
        const arrayPositionDotX = editPositionDotX;
        let changed = false;
        let labelY = editPositionLabel;
        let labelX = editPositionLabelX;
        let newAreaLabelPos = editAreaLabel;
        if (type === 'y') {
            if (elemArray === 'single') {
                if (arrayPosition[id] !== val.toString()) {
                    changed = true;
                }
                arrayPosition[id] = val.toString();
            } else {
                const singleEl = arrayPosition[id].split(';');
                singleEl[arrayId] = val.toString();
                if (singleEl[arrayId] !== val.toString()) {
                    changed = true;
                }
                arrayPosition[id] = singleEl.join(';');
            }
        } else if (type === 'x') {
            if (elemArray === 'single') {
                if (arrayPositionX[id] !== val.toString()) {
                    changed = true;
                }
                arrayPositionX[id] = val.toString();
            } else {
                const singleEl = arrayPositionX[id].split(';');
                singleEl[arrayId] = val.toString();
                if (singleEl[arrayId] !== val.toString()) {
                    changed = true;
                }
                arrayPositionX[id] = singleEl.join(';');
            }
        } else if (type === 'dot_y') {
            if (elemArray === 'single') {
                arrayPositionDot[id] = val.toString();
                if (arrayPositionDot[id] !== val.toString()) {
                    changed = true;
                }
            } else {
                const singleEl = arrayPositionDot[id].split(';');
                singleEl[arrayId] = val.toString();
                arrayPositionDot[id] = singleEl.join(';');
                if (singleEl[arrayId] !== val.toString()) {
                    changed = true;
                }
            }
        } else if (type === 'dot_x') {
            if (elemArray === 'single') {
                arrayPositionDotX[id] = val.toString();
                if (arrayPositionDotX[id] !== val.toString()) {
                    changed = true;
                }
            } else {
                const singleEl = arrayPositionDotX[id].split(';');
                singleEl[arrayId] = val.toString();
                arrayPositionDotX[id] = singleEl.join(';');
                if (singleEl[arrayId] !== val.toString()) {
                    changed = true;
                }
            }
        } else if (type === 'labelY') {
            if (editPositionLabel !== val.toString() && typeof val !== 'undefined' && !isNaN(parseFloat(val))) {
                changed = true;
                labelY = val.toString();
                newAreaLabelPos = 1;
            }
        } else if (type === 'labelX') {
            if (editPositionLabelX !== val.toString() && typeof val !== 'undefined' && !isNaN(parseFloat(val))) {
                changed = true;
                labelX = val.toString();
                newAreaLabelPos = 0;
            }
        }
        if (changed) {
            this.setState({
                editPosition: arrayPosition,
                editPositionX: arrayPositionX,
                editPositionDot: arrayPositionDot,
                editPositionDotX: arrayPositionDotX,
                editPositionLabel: labelY,
                editPositionLabelX: labelX,
                editAreaLabel: newAreaLabelPos,
                toSave: true,
            });
            this.setState({
                toSave: true,
            });
        }
    }

    changeSlideData() {
        const {
            allAssetPlan, slideUUID,
        } = this.props;
        const {
            sheetSel, editType, type, sheetSelCompare, chartsType, arrayLabel1, arrayLabel2, arrayLabel3, arrayLabel4, allData,
            clientName, partnerName, defaultData, title, titleCompare, numBox, icon1, icon2, icon3, icon4, yourPriorities, nextStep, changeSingleIcon, idIcon,
        } = this.state;
        const arrayAll = allAssetPlan.presentation;
        let arrayCurrent = [];
        if (slideUUID !== null) {
            arrayCurrent = arrayAll[slideUUID];
        } else {
            arrayCurrent = [];
        }
        if (type === 'lifeline') {
            this.setState({ editType: 'chart' });
            arrayCurrent[1] = 'chart';
        } else {
            this.setState({ editType: '' });
        }
        let titleNew;
        let titleCompareNew;
        switch (editType) {
            case 'title':
                titleNew = 'Title';
                titleCompareNew = 'Subtitle';
                break;
            default:
                titleNew = '';
                titleCompareNew = '';
                break;
        }
        if (sheetSel) {
            this.setState({
                data: [sheetSel].value,
                client: clientName || allData[sheetSel].client,
                clientName: clientName || allData[sheetSel].client,
                partnerName: clientName ? partnerName : allData[sheetSel].partner,
                partner: clientName ? partnerName : allData[sheetSel].partner,
                editPosition: allData[sheetSel].position_label,
                editPositionX: allData[sheetSel].position_labelX,
                editPositionDot: allData[sheetSel].position_dot,
                editPositionDotX: allData[sheetSel].position_dot_x,
                differenceAge: allData[sheetSel].differenceAge,
                editIcon: allData[sheetSel].icon,
                editLabel: allData[sheetSel].label,
                editAxis: allData[sheetSel].axis,
                editLine: allData[sheetSel].line,
                title: sheetSel,
                loadingGraph: true,
            });
            switch (sheetSel) {
                case 'Plan-A':
                    this.setState({
                        title: sheetSelCompare ? 'Current Position VS Our Advice' : 'Current Position',
                        label1: 'Current Position',
                    });
                    break;
                default:
                    this.setState({
                        title: sheetSelCompare ? 'Current Position VS Our Advice' : 'Our Advice',
                        label1: 'Our Advice',
                    });
            }
        }
        switch (chartsType) {
            case 'all_assets':
                this.setState({
                    compare: true,
                    color1: '#122ae0',
                    color2: '#6c087c',
                    color3: '',
                    color4: '',
                    color5: '',
                    color6: '',
                    label1: sheetSelCompare ? 'Our Advice' : 'Current Position',
                    label2: sheetSelCompare ? 'Current Position' : '',
                    titleCompare: 'All assets',
                    label3: '',
                    label4: '',
                    label5: '',
                });
                break;
            case 'liquid_assets':
                this.setState({
                    compare: true,
                    color1: '#32D7F3',
                    color2: '#6c087c',
                    titleCompare: 'Liquid assets',
                    color3: '',
                    color4: '',
                    color5: '',
                    color6: '',
                    label1: sheetSelCompare ? 'Our Advice' : 'Current Position',
                    label2: sheetSelCompare ? 'Current Position' : '',
                    label3: '',
                    label4: '',
                    label5: '',
                    label6: '',
                });
                break;
            case 'all_assets_detailed':
                this.setState({
                    titleCompare: 'All assets detailed',
                    compare: false,
                    label1: 'BPR NL',
                    label2: 'BPR',
                    label3: 'Net Property',
                    label4: 'Pension',
                    label5: 'Investment',
                    label6: 'Savings',
                    color1: '#BC56BE',
                    color2: '#8DE9A1',
                    color3: '#BDA8EB',
                    color4: '#57B5BB',
                    color5: '#84AFE6',
                    color6: '#EFD342',
                });
                break;
            case 'liquid_assets_detailed':
                this.setState({
                    titleCompare: 'Liquid assets detailed',
                    compare: false,
                    label1: 'BPR NL',
                    label2: 'BPR',
                    label3: '',
                    label4: 'Investment',
                    label5: 'Pension',
                    label6: 'Savings',
                    color1: '#BC56BE',
                    color2: '#8DE9A1',
                    color3: '#BDA8EB',
                    color4: '#84AFE6',
                    color5: '#57B5BB',
                    color6: '#EFD342',
                });
                break;
            case 'legacy':
                this.setState({
                    dataType: 'table',
                    editType: 'table',
                    compare: true,
                    title: 'Your Legacy',
                    childName1: 'In additional to legacy to your children (subject to IHT)',
                    label1: 'BPR NL',
                    label2: 'BPR',
                });
                break;
            case 'legacy_detailed':
                this.setState({
                    dataType: 'table',
                    editType: 'table',
                    compare: true,
                    title: 'Your Legacy',
                });
                break;
            case 'legacy_tax':
                this.setState({
                    dataType: 'table',
                    editType: 'table',
                    compare: true,
                    childName1: 'In additional to legacy to your children (subject to IHT)',
                    title: 'Legacy Tax',
                });
                break;
            case 'legacy_tax_detailed':
                this.setState({
                    dataType: 'table',
                    editType: 'table',
                    compare: true,
                    title: 'Legacy Tax',
                });
                break;
            case 'our_pricing_competition':
                this.setState({
                    editType: 'table',
                    title: 'Our Pricing vs Competition',
                });
                break;
            case 'our_pricing':
                this.setState({
                    dataType: 'table',
                    editType: 'table',
                    title: 'Our Pricing',
                    titleCompare: '',
                    loadingGraph: true,
                    label1: defaultData.our_pricing.label1,
                    label2: defaultData.our_pricing.label2,
                    label3: defaultData.our_pricing.label3,
                    label4: defaultData.our_pricing.label4,
                });
                break;
            case 'title':
                this.setState({
                    dataType: 'title',
                    editType: 'title',
                    title: 'Title',
                    titleCompare: 'Subtitle',
                    loadingGraph: true,
                    label1: '',
                    label2: '',
                    label3: '',
                    label4: '',
                });
                break;
            case 'box': {
                let numBoxNew;
                // let subtitleNew;
                titleNew = title;
                titleCompareNew = titleCompare;
                let arrayLabel1New = arrayLabel1;
                let arrayLabel2New = arrayLabel2;
                let arrayLabel3New = arrayLabel3;
                let arrayLabel4New = arrayLabel4;
                let icon1New = icon1;
                let icon2New = icon2;
                let icon3New = icon3;
                let icon4New = icon4;
                if (changeSingleIcon) {
                    switch (parseInt(idIcon, 10)) {
                        case 1:
                            arrayLabel1.icon = icon1;
                            break;
                        case 2:
                            arrayLabel2.icon = icon2;
                            break;
                        case 3:
                            arrayLabel3.icon = icon3;
                            break;
                        case 4:
                            arrayLabel4.icon = icon4;
                            break;
                        default:
                            arrayLabel1.icon = icon1;
                    }
                }
                numBoxNew = numBox;
                if (nextStep === 'next_step') {
                    numBoxNew = 2;
                    titleNew = 'Next steps';
                    titleCompareNew = '';
                    yourPriorities.map((item) => {
                        switch (item.title) {
                            case 'LOAs':
                                arrayLabel1New = {
                                    icon: item.icon,
                                    title: item.title,
                                    text: item.text,
                                };
                                icon1New = item.icon;
                                break;
                            case 'ID':
                                arrayLabel2New = {
                                    icon: item.icon,
                                    title: item.title,
                                    text: item.text,
                                };
                                icon2New = item.icon;
                                break;
                            default:
                                return null;
                        }
                        return null;
                    });
                    arrayLabel3New = {
                        icon: '',
                        title: '',
                        text: '',
                    };
                    arrayLabel4New = {
                        icon: '',
                        title: '',
                        text: '',
                    };
                    icon3New = '';
                    icon4New = '';
                } else if (nextStep === 'priorities') {
                    titleNew = 'Your Priorities';
                    titleCompareNew = 'And how we\'ll achieve them';
                    numBoxNew = 4;
                    yourPriorities.map((item) => {
                        switch (item.title) {
                            case 'Saving for your retirement':
                                arrayLabel1New = {
                                    icon: item.icon,
                                    title: item.title,
                                    text: item.text,
                                };
                                icon1New = item.icon;
                                break;
                            case 'Making your money work harder':
                                arrayLabel2New = {
                                    icon: item.icon,
                                    title: item.title,
                                    text: item.text,
                                };
                                icon2New = item.icon;
                                break;
                            case 'Review':
                                arrayLabel3New = {
                                    icon: item.icon,
                                    title: item.title,
                                    text: item.text,
                                };
                                icon3New = item.icon;
                                break;
                            case 'Anything to add...':
                                arrayLabel4New = {
                                    icon: item.icon,
                                    title: item.title,
                                    text: item.text,
                                };
                                icon4New = item.icon;
                                break;
                            default:
                                arrayLabel4New = {
                                    icon: '',
                                    title: '',
                                    text: '',
                                };
                                icon4New = '';
                        }
                        return null;
                    });
                }
                this.setState({
                    editType: 'box',
                    dataType: 'box',
                    title: titleNew,
                    titleCompare: titleCompareNew,
                    numBox: numBoxNew,
                    arrayLabel1: arrayLabel1New,
                    arrayLabel2: arrayLabel2New,
                    arrayLabel3: arrayLabel3New,
                    arrayLabel4: arrayLabel4New,
                    icon1: icon1New,
                    icon2: icon2New,
                    icon3: icon3New,
                    icon4: icon4New,
                    changeSingleIcon: false,
                });
                break;
            }
            case 'what_will_cost':
                this.setState({
                    editType: 'text_description',
                    dataType: 'text_description',
                    title: 'What will it cost?',
                    titleCompare: 'Financial advice fees can be complex, so we\'ve worked hard to make things simple.<br>We guarantee no hidden charges or exit penalties.',
                    label1: defaultData.what_will_cost.label1,
                    label2: defaultData.what_will_cost.label2,
                    label3: defaultData.what_will_cost.label3,
                    label4: defaultData.what_will_cost.label4,
                });
                break;
            case 'let_started':
                this.setState({
                    editType: 'text_description',
                    title: 'Let\'s get started',
                    titleCompare: '',
                    label1: defaultData.let_started.label1,
                    label2: defaultData.let_started.label2,
                    label3: defaultData.let_started.label3,
                    label4: defaultData.let_started.label4,
                });
                break;
            case 'pensions_contribution':
                this.setState({
                    editType: 'table',
                    title: `Insight - Pension Contributions for ${clientName}`,
                    childName1: 'your children',
                });
                break;
            default:
                return null;
        }
        if (sheetSelCompare) {
            const arrayData = allData;
            arrayData[sheetSelCompare].value = loadDataGraph(arrayData[sheetSelCompare].value);
            this.setState({
                allData: arrayData,
                label1: 'Current Position',
                label2: 'Our Advice',
                compare: true,
                compareGraph: true,
                loadingGraph: true,
            });
        } else {
            this.setState({
                compareGraph: false,
            });
        }
        this.setState({
            changeSlideData: false,
            reloadPrePlan: true,
        });
        return null;
    }

    handleChange(e) {
        // const {
        // allAssetPlan, slideUUID,
        // } = this.props;
        const {
            oldSubtitle, oldTitle, title, titleCompare,
        } = this.state;
        let name;
        let value;
        let type;
        // const arrayAll = allAssetPlan.presentation;
        const self = this;
        if (typeof e.name !== 'undefined') {
            ({
                name, value,
            } = e);
        } else if (typeof e.target !== 'undefined') {
            ({
                name, value, type,
            } = e.target);
        } else {
            name = Object.keys(self.refs);
            name = name[0];
        }
        if (type === 'checkbox') {
            value = e.target.checked;
            if (name === 'editAreaLabel') {
                let areaLabel;
                if (value) {
                    areaLabel = value;
                } else {
                    areaLabel = 0;
                }
                this.setState({
                    areaLabel,
                });
            }
            if (name === 'viewText') {
                let titleNew;
                let subtitle;
                if (value) {
                    subtitle = 'Here\'s that first snapshot of your LifeLine, a visual representation of your finances which we\'ll use to track your progress towards goals over time. The blue line shows how things currently stand; the purple line shows what advice could do for you.';
                    titleNew = 'Your Lifeline';
                } else {
                    subtitle = oldSubtitle;
                    titleNew = oldTitle;
                }
                this.setState({
                    title: titleNew,
                    titleCompare: subtitle,
                    oldSubtitle: titleCompare,
                    oldTitle: title,
                });
            }
        }
        if (name === 'type') {
            this.setState({
                editType: '',
                dataType: '',
                loadingGraph: false,
                sheetSel: '',
                chartsType: '',
                sheetSelCompare: '',
                compareGraph: false,
                compare: false,
            });
        }
        if (name === 'numBox') {
            this.setState({
                nextStep: '',
            });
        }
        const changeSlideData = !((type === 'text' || type === 'textarea' || name === 'activeLine' || name === 'activeLabel' || name === 'activeAxis' || name === 'type' || name === 'titleCompare' || name === 'textarea2'));
        this.setState({
            [name]: value,
            saved: false,
            changeSlideData,
        });
    }

    editText(labelId, arrayId, arrayName, val) {
        const {
            label1, label2, label3, label4, editType,
        } = this.state;
        let currentLabel;
        let valLabel;
        if (arrayId === -1) {
            valLabel = val.replace(/\r?\n/g, '<br>');
        } else {
            switch (labelId) {
                case 1:
                    currentLabel = JSON.parse(label1);
                    break;
                case 2:
                    currentLabel = JSON.parse(label2);
                    break;
                case 3:
                    currentLabel = JSON.parse(label3);
                    break;
                case 4:
                    currentLabel = JSON.parse(label4);
                    break;
                default:
                    currentLabel = '';
            }
            if (editType === 'text_description') {
                if ((arrayName === 'text' && val.length <= 160) || (arrayName === 'subtext' && val.length <= 100)) {
                    currentLabel[arrayId][arrayName] = val;
                } else if (arrayName === 'multi') {
                    const multiVal = val.split('//');
                    currentLabel[arrayId] = {
                        price: multiVal[0],
                        text: multiVal[1],
                    };
                } else {
                    currentLabel[arrayId][arrayName] = val;
                }
            } else {
                currentLabel[arrayId][arrayName] = val;
            }
            valLabel = JSON.stringify(currentLabel);
        }
        this.setState({
            [`label${labelId}`]: valLabel,
            toSave: true,
        });
    }

    render() {
        const self = this;
        const {
            slideUUID, orderSlide, idGoogle,
        } = this.props;
        const {
            printPage, loading, popIcon, activeLabel, activeAxis, activeLine, type, chartsType, nextStep, editType, sheetSel, sheetSelCompare, sheetsList, compare, numBox, clientName, partnerName, advisorName,
            viewOption, toSave, saving, saved, viewLegend, viewText, viewAreaLabel, viewAxis, viewAxisY, maxAxisClient, bprNlEnable, bprEnable, viewLine, options,
            width, height, title, titleCompare, assumesPortfolio, childName1, childName2, competition, data, idGoogleSheet, ihtAVal, ihtBVal, label1, label2, label3, label4, label5, label6, totalTER, octopusCarib, year,
            loadingGraph, reloadPlan, color1, color2, color3, color4, color5, color6, client, partner, compareGraph, differenceAge, label, editLabel, editLine, editIcon, editPosition, editPositionX, editPositionDot, editPositionDotX, editPositionLabel, editPositionLabelX, editAxis, minAxisClient, clientCase, chartsCompareType,
            chartScale, exclude, step, viewAxisYInterval, minAxisClient1, minAxisClient2, minAxisClient3, maxAxisClient1, maxAxisClient2, maxAxisClient3, yourPriorities, arrayLabel1, arrayLabel2, arrayLabel3, arrayLabel4,
            icon1, icon2, icon3, icon4, reloadPrePlan, titleLight, areaLabel, chartScaleMax1,
        } = this.state;
        let zoom = 0;
        const allIcon = [];
        Object.keys(getCurrentIcon).map((k) => {
            if (typeof getCurrentIcon[k] !== 'undefined') {
                allIcon.push({ icon: getCurrentIcon[k], label: k });
            }
            return null;
        });
        if (!printPage) {
            zoom = ((window.innerWidth) / width) - 0.05;
        } else {
            zoom = 0.78;
        }
        if (loading) {
            return (
                <Preload createPresentation={false} />
            );
        }
        const saveCurSlideSingle = saved ? 'SAVED' : 'SINGLE SAVE';
        const saveCurSlide = saved ? 'SAVED' : 'SAVE';
        return (
            <div className="App build_page">
                {popIcon
                    && (
                        <div className="customModal">
                            <div className="contentModal">
                                <div className="modalHeader">
                                    <h3>Choose your different Icon</h3>
                                    <button
                                        type="button"
                                        onClick={() => this.setState({ popIcon: !popIcon })}
                                    >
                                        <i
                                            className="far fa-times-circle"
                                        />
                                    </button>
                                </div>
                                <div className="wrapContentModal">
                                    <ul className="listStyleIcon">
                                        {
                                            allIcon.map((item, i) => {
                                                const labelNew = typeof self.state.activeIcon === 'undefined' ? 'placeholder' : self.state.activeIcon;
                                                const iconClass = labelNew === item.label ? 'activeIcon' : '';
                                                return (
                                                    <li
                                                        key={i.toString()}
                                                        className={iconClass}
                                                    >
                                                        <button
                                                            onClick={(() => self.changeIcon(item.label))}
                                                            type="button"
                                                            className="no_btn"
                                                        >
                                                            <svg key={i.toString()} viewBox="0 0 1024 1024">{item.icon}</svg>
                                                        </button>
                                                    </li>
                                                );
                                            })
                                        }

                                    </ul>
                                </div>
                                {
                                    editType === 'chart'
                                && (
                                    <div className="editLabel">
                                        <div className="inputLabel">
                                            <input
                                                type="text"
                                                name="activeLabel"
                                                value={activeLabel}
                                                onChange={e => this.handleChange(e)}
                                            />
                                        </div>
                                        <div className="checkboxLabel">
                                            <div>
                                                <input
                                                    id="activeAxis"
                                                    type="checkbox"
                                                    name="activeAxis"
                                                    checked={activeAxis}
                                                    onChange={e => this.handleChange(e)}
                                                />
                                                <label htmlFor="activeAxis">Visible Age</label>
                                            </div>
                                            <div>
                                                <input
                                                    id="activeLine"
                                                    type="checkbox"
                                                    name="activeLine"
                                                    checked={activeLine}
                                                    onChange={e => this.handleChange(e)}
                                                />
                                                <label htmlFor="activeLine">Visible Line</label>
                                            </div>
                                        </div>
                                    </div>
                                )
                                }
                                <div className="modalFooter">
                                    <button
                                        type="button"
                                        className="alternative_button"
                                        onClick={() => self.savePopIcon()}
                                    >
                                        APPLY
                                    </button>
                                    <button
                                        type="button"
                                        className="secondary_button"
                                        onClick={() => self.setState({ popIcon: !popIcon })}
                                    >
                                        CANCEL
                                    </button>
                                </div>
                            </div>
                        </div>
                    )
                }
                <header className="App-header">
                    <div className="wrapHeader">
                        <div className="row_field_form">
                            <div>
                                <select value={type} name="type" onChange={this.handleChange.bind(this)}>
                                    <option value="">Type</option>
                                    <option value="lifeline">Lifeline</option>
                                    <option value="pricing">Pricing</option>
                                    <option value="iht">IHT</option>
                                    <option value="other">Other</option>
                                </select>
                            </div>
                            {
                                type === 'pricing'
                                && (
                                    <div>
                                        <select
                                            value={chartsType}
                                            name="chartsType"
                                            onChange={this.handleChange.bind(this)}
                                        >
                                            <option value="">Subtype</option>
                                            <option value="our_pricing">Our Pricing</option>
                                            {/* <option value="our_pricing_competition">Our Pricing vs Competition</option> */}
                                            <option value="what_will_cost">What Will Cost</option>
                                        </select>
                                    </div>
                                )
                            }
                            {
                                type === 'iht'
                                && (
                                    <div>
                                        <select
                                            value={chartsType}
                                            name="chartsType"
                                            onChange={this.handleChange.bind(this)}
                                        >
                                            <option value="">Choose Table</option>
                                            <option value="legacy">Your Legacy</option>
                                            <option value="legacy_tax">Legacy IHT</option>
                                        </select>
                                    </div>
                                )
                            }
                            {
                                type === 'other'
                                && (
                                    <div>
                                        <select
                                            value={chartsType}
                                            name="chartsType"
                                            onChange={this.handleChange.bind(this)}
                                        >
                                            <option value="">Choose Table</option>
                                            <option value="title">Title</option>
                                            <option value="box">Box</option>
                                            <option value="let_started">Let&apos;s get started</option>
                                            <option value="pensions_contribution">Pension Contributions</option>
                                        </select>
                                    </div>
                                )
                            }
                            {
                                chartsType === 'box'
                            && (
                                <div>
                                    <select
                                        value={nextStep}
                                        name="nextStep"
                                        onChange={this.handleChange.bind(this)}
                                    >
                                        <option value="">Choose Box</option>
                                        <option value="priorities">Priorities</option>
                                        <option value="next_step">Next Step</option>
                                    </select>
                                </div>
                            )
                            }

                            {
                                (editType !== 'title' && editType !== 'box' && chartsType !== 'box' && editType !== 'text_description' && chartsType !== 'our_pricing' && chartsType !== 'pensions_contribution' && chartsType !== 'our_pricing_competition')
                                && (
                                    <div>
                                        <select
                                            value={sheetSel}
                                            name="sheetSel"
                                            onChange={this.handleChange.bind(this)}
                                        >
                                            <option value="">Plan</option>
                                            {
                                                sheetsList.map((item, i) => (
                                                    <option key={i.toString()} value={item}>{item}</option>
                                                ))
                                            }
                                        </select>
                                    </div>
                                )
                            }
                            {
                                (editType === 'chart')
                                && (
                                    <div>
                                        <select
                                            value={chartsType}
                                            name="chartsType"
                                            onChange={this.handleChange.bind(this)}
                                        >
                                            <option value="">Choose Chart</option>
                                            <option value="all_assets">All Assets</option>
                                            <option value="liquid_assets">Liquid Assets</option>
                                            {/* <option value="liquid_assets_trust">Liquid Assets w/ Trust</option> */}
                                            <option value="all_assets_detailed">All Assets Detailed</option>
                                            <option value="liquid_assets_detailed">Liquid Assets Detailed</option>
                                        </select>
                                    </div>
                                )
                            }
                            {
                                (editType !== 'title' && editType !== 'box' && chartsType !== 'box' && editType !== 'text_description' && chartsType !== 'our_pricing' && chartsType !== 'pensions_contribution' && chartsType !== 'our_pricing_competition' && compare)
                                && (
                                    <div>
                                        <select
                                            value={sheetSelCompare}
                                            name="sheetSelCompare"
                                            onChange={this.handleChange.bind(this)}
                                        >
                                            <option value="">Plan</option>
                                            {
                                                sheetsList.map((item, i) => (
                                                    <option key={i.toString()} value={item}>{item}</option>
                                                ))
                                            }
                                        </select>
                                    </div>
                                )
                            }
                            {
                                (sheetSelCompare && editType === 'chart')
                                && (
                                    <div>
                                        <select
                                            value={chartsCompareType}
                                            name="chartsCompareType"
                                            onChange={this.handleChange.bind(this)}
                                        >
                                            <option value="">Chart Type</option>
                                            <option value="line">Line</option>
                                            <option value="area">Area</option>
                                        </select>
                                    </div>
                                )
                            }
                            {
                                editType === 'box'
                                && (
                                    <div>
                                        <select
                                            value={numBox}
                                            name="numBox"
                                            onChange={this.handleChange.bind(this)}
                                        >
                                            <option value="">Box Number</option>
                                            <option value={1}>1 Box</option>
                                            <option value={2}>2 Box</option>
                                            <option value={3}>3 Box</option>
                                            <option value={4}>4 Box</option>
                                        </select>
                                    </div>
                                )
                            }
                            {
                                clientName
                                && (
                                    <div>
                                        <input
                                            type="text"
                                            name="clientName"
                                            onChange={this.handleChange.bind(this)}
                                            value={clientName}
                                            placeholder="Client Name"
                                        />
                                    </div>
                                )
                            }
                            {
                                partnerName
                                && (
                                    <div>
                                        <input
                                            type="text"
                                            name="partnerName"
                                            placeholder="Partner Name"
                                            onChange={this.handleChange.bind(this)}
                                            value={partnerName}
                                        />
                                    </div>
                                )
                            }
                            <div>
                                <select
                                    name="advisorName"
                                    value={advisorName}
                                    onChange={e => this.setState({ advisorName: e.target.value })}
                                >
                                    <option value="">Advisor</option>
                                    <option value="Andrew">Andrew</option>
                                    <option value="Joseph">Joseph</option>
                                    <option value="Dan">Dan</option>
                                </select>
                            </div>
                        </div>
                        <div>
                            <button
                                type="button"
                                onClick={() => this.setState({ viewOption: !viewOption })}
                                className={viewOption ? 'alternative_button' : 'primary_button'}
                            >
                            OPTIONS
                            </button>
                            <button
                                type="button"
                                onClick={e => this.saveSlideData(e)}
                                className={toSave ? 'alert_button' : 'primary_button'}
                            >
                                {saving ? 'SAVING' : saveCurSlide}
                            </button>
                            {(slideUUID !== null && editType === 'chart')
                        && (
                            <button
                                type="button"
                                onClick={e => this.saveSlideData(e, 'single_save')}
                                className={toSave ? 'alert_button' : 'primary_button'}
                            >
                                {saving ? 'SAVING' : saveCurSlideSingle}
                            </button>
                        )
                            }
                            <Link
                                to={{
                                    pathname: `/webapp/${idGoogle}/review`,
                                    state: {
                                        position: slideUUID !== null ? slideUUID : orderSlide,
                                    },
                                }}
                                className="primary_button"
                            >
                                EXIT
                            </Link>
                        </div>
                    </div>
                    {
                        viewOption
                        && (
                            <div>
                                <h5>MORE OPTIONS</h5>
                                {
                                    editType === 'chart'
                                && (
                                    <div className="wrapHeader">
                                        <div>
                                            <input
                                                type="checkbox"
                                                name="viewLegend"
                                                id="viewLegend"
                                                checked={viewLegend}
                                                onChange={e => this.handleChange(e)}
                                            />
                                            <label htmlFor="viewLegend">View Legend</label>
                                        </div>
                                        <div>
                                            <input
                                                type="checkbox"
                                                name="viewText"
                                                id="viewText"
                                                checked={viewText}
                                                onChange={e => this.handleChange(e)}
                                            />
                                            <label htmlFor="viewText">View Textarea</label>
                                        </div>
                                        <div>
                                            <input
                                                type="checkbox"
                                                name="viewAreaLabel"
                                                id="viewAreaLabel"
                                                checked={viewAreaLabel}
                                                onChange={e => this.handleChange(e)}
                                            />
                                            <label htmlFor="viewAreaLabel">View Area Label</label>
                                        </div>
                                        <div>
                                            <input
                                                type="checkbox"
                                                name="viewAxis"
                                                id="viewAxis"
                                                checked={viewAxis}
                                                onChange={e => this.handleChange(e)}
                                            />
                                            <label htmlFor="viewAxis">View X Axis</label>
                                        </div>
                                        <div>
                                            <input
                                                type="checkbox"
                                                name="viewAxisY"
                                                id="viewAxisY"
                                                checked={viewAxisY}
                                                onChange={e => this.handleChange(e)}
                                            />
                                            <label htmlFor="viewAxisY">View y Axis</label>
                                        </div>
                                        <div>
                                            <select
                                                name="viewAxisYInterval"
                                                id="viewAxisYInterval"
                                                onChange={e => this.handleChange(e)}
                                            >
                                                <option>Interval Y AXis</option>
                                                <option value="5">5</option>
                                                <option value="7">7</option>
                                                <option value="9">9</option>
                                                <option value="10">10</option>
                                            </select>
                                        </div>
                                        <div>
                                            <input
                                                type="text"
                                                name="maxAxisClient"
                                                id="maxAxisClient"
                                                value={maxAxisClient}
                                                onChange={e => this.handleChange(e)}
                                            />
                                        </div>
                                        <div>
                                            <input
                                                type="checkbox"
                                                name="bprNlEnable"
                                                id="bprNlEnable"
                                                checked={bprNlEnable}
                                                onChange={e => this.handleChange(e)}
                                            />
                                            <label htmlFor="bprNlEnable">Enable BPR NL</label>
                                        </div>
                                        <div>
                                            <input
                                                type="checkbox"
                                                name="bprEnable"
                                                id="bprEnable"
                                                checked={bprEnable}
                                                onChange={e => this.handleChange(e)}
                                            />
                                            <label htmlFor="bprEnable">Enable BPR</label>
                                        </div>
                                    </div>
                                )
                                }
                                {
                                    editType === 'title'
                                && (
                                    <div className="wrapHeader">
                                        <div>
                                            <input
                                                type="checkbox"
                                                name="viewLine"
                                                id="viewLine"
                                                checked={viewLine}
                                                onChange={e => this.handleChange(e)}
                                            />
                                            <label htmlFor="viewLine">View Background Line</label>
                                        </div>
                                    </div>
                                )
                                }
                                {
                                    editType === 'table'
                                && (
                                    <div className="wrapHeader">
                                        <div>
                                            <input
                                                type="checkbox"
                                                name="bprNlEnable"
                                                id="bprNlEnable"
                                                checked={options.bprNlEnable}
                                                onChange={e => this.handleChange(e)}
                                            />
                                            <label htmlFor="bprNlEnable">Enable BPR NL</label>
                                        </div>
                                        <div>
                                            <input
                                                type="checkbox"
                                                name="bprEnable"
                                                id="bprEnable"
                                                checked={options.bprEnable}
                                                onChange={e => this.handleChange(e)}
                                            />
                                            <label htmlFor="bprEnable">Enable BPR</label>
                                        </div>
                                    </div>
                                )
                                }
                            </div>
                        )
                    }
                </header>
                <div>
                    <div
                        style={{
                            pageBreakInside: 'avoid',
                            position: 'relative',
                            width: '100%',
                        }}
                    >
                        <div>
                            <div className="row">
                                <div
                                    className="col-md-12"
                                    style={{ backgroundColor: !printPage ? '#000000' : '#FFFFFF' }}
                                >
                                    <div
                                        className="zoom_box"
                                        style={{
                                            zoom,
                                            width,
                                            height,
                                            backgroundColor: '#FFFFFF',
                                        }}
                                    >
                                        <div style={{
                                            width,
                                            height: height - 50,
                                            display: 'flex',
                                            alignItems: 'center',
                                            backgroundImage: viewLine ? 'url(/static/img/presentation/octopus_first_slide.png)' : '',
                                        }}
                                        >
                                            {
                                                editType === 'table'
                                                    && (
                                                        <div>
                                                            <div id="title_editing">
                                                                <div>
                                                                    <input
                                                                        type="text"
                                                                        name="title"
                                                                        onChange={e => this.handleChange(e)}
                                                                        value={title}
                                                                        className="title"
                                                                    />
                                                                </div>
                                                                <div />
                                                            </div>
                                                            <div className="subtitleEditing">
                                                                <input
                                                                    type="text"
                                                                    name="titleCompare"
                                                                    onChange={e => this.handleChange(e)}
                                                                    value={titleCompare}
                                                                />
                                                            </div>
                                                            {
                                                                (sheetSel || chartsType === 'our_pricing' || chartsType === 'pensions_contribution' || chartsType === 'our_pricing_competition')
                                                            && (
                                                                <Table
                                                                    plan={sheetSel}
                                                                    planCompare={sheetSelCompare}
                                                                    assumesPortfolio={assumesPortfolio}
                                                                    chartsType={chartsType}
                                                                    childName1={childName1}
                                                                    childName2={childName2}
                                                                    competition={competition}
                                                                    data={data}
                                                                    edit
                                                                    idGoogle={idGoogleSheet}
                                                                    ihtA={this.handleChange.bind(this)}
                                                                    iht_b={this.handleChange.bind(this)}
                                                                    ihtAVal={ihtAVal}
                                                                    ihtBVal={ihtBVal}
                                                                    editText={(labelId, arrayId, arrayName, val) => this.editText(labelId, arrayId, arrayName, val)}
                                                                    label1={label1}
                                                                    label2={label2}
                                                                    label3={label3}
                                                                    label4={label4}
                                                                    totalTER={totalTER}
                                                                    octopusCarib={octopusCarib}
                                                                    year={parseInt(year, 10)}
                                                                    width={width}
                                                                    height={height - 200}
                                                                    options={typeof options !== 'undefined' ? options : { bprEnable: false, bprNlEnable: false }}
                                                                    step={10}
                                                                />
                                                            )
                                                            }
                                                        </div>
                                                    )
                                            }
                                            {
                                                (loadingGraph && editType === 'chart' && !reloadPlan && !reloadPrePlan)
                                                    && (
                                                        <div className="wrapContent">
                                                            <div id="title_editing">
                                                                <div>
                                                                    <input
                                                                        type="text"
                                                                        name="title"
                                                                        onChange={e => this.handleChange(e)}
                                                                        value={title}
                                                                        className="title"
                                                                    />
                                                                </div>
                                                                <div
                                                                    className={!label3 ? 'legendChart columnInverse' : ' legendChart'}
                                                                >
                                                                    {viewLegend
                                                                && (
                                                                    <React.Fragment>
                                                                        {label3
                                                                    && (
                                                                        <div>
                                                                            <input
                                                                                type="text"
                                                                                name="label3"
                                                                                onChange={e => this.handleChange(e)}
                                                                                value={label3}
                                                                                key={1}
                                                                            />
                                                                            <button
                                                                                type="button"
                                                                                key={0}
                                                                                style={{ backgroundColor: color3 }}
                                                                                onClick={() => this.setState({ color3: color3 === '#d1d1d1' ? color3 : '#d1d1d1' })}
                                                                            >
                                                                            color
                                                                            </button>
                                                                        </div>
                                                                    )
                                                                        }
                                                                        {((label1 && !label6) || (label6 && bprNlEnable))
                                                                    && (
                                                                        <div>
                                                                            <input
                                                                                type="text"
                                                                                name="label1"
                                                                                onChange={e => this.handleChange(e)}
                                                                                value={label1}
                                                                                key={1}
                                                                            />
                                                                            <button
                                                                                type="button"
                                                                                key={0}
                                                                                style={{ backgroundColor: color1 }}
                                                                                onClick={() => this.setState({ color1: color1 === '#d1d1d1' ? color1 : '#d1d1d1' })}
                                                                            >
                                                                            color
                                                                            </button>
                                                                        </div>
                                                                    )
                                                                        }
                                                                        {((label2 && !label6) || (label6 && bprEnable))
                                                                    && (
                                                                        <div>
                                                                            <input
                                                                                type="text"
                                                                                name="label2"
                                                                                onChange={e => this.handleChange(e)}
                                                                                value={label2}
                                                                                key={1}
                                                                            />
                                                                            <button
                                                                                type="button"
                                                                                key={0}
                                                                                style={{ backgroundColor: color2 }}
                                                                                onClick={() => this.setState({ color2: color2 === '#d1d1d1' ? color2 : '#d1d1d1' })}
                                                                            >
                                                                            color
                                                                            </button>
                                                                        </div>
                                                                    )
                                                                        }
                                                                        {label4
                                                                    && (
                                                                        <div>
                                                                            <input
                                                                                type="text"
                                                                                name="label4"
                                                                                onChange={e => this.handleChange(e)}
                                                                                value={label4}
                                                                                key={1}
                                                                            />
                                                                            <button
                                                                                type="button"
                                                                                key={0}
                                                                                style={{ backgroundColor: color4 }}
                                                                                onClick={() => this.setState({ color4: color4 === '#d1d1d1' ? color4 : '#d1d1d1' })}
                                                                            >
                                                                            color
                                                                            </button>
                                                                        </div>
                                                                    )
                                                                        }
                                                                        {label5
                                                                    && (
                                                                        <div>
                                                                            <input
                                                                                type="text"
                                                                                name="label5"
                                                                                onChange={e => this.handleChange(e)}
                                                                                value={label5}
                                                                                key={1}
                                                                            />
                                                                            <button
                                                                                type="button"
                                                                                key={0}
                                                                                style={{ backgroundColor: color5 }}
                                                                                onClick={() => this.setState({ color5: color5 === '#d1d1d1' ? color5 : '#d1d1d1' })}
                                                                            >
                                                                            color
                                                                            </button>
                                                                        </div>
                                                                    )
                                                                        }
                                                                        {label6
                                                                    && (
                                                                        <div>
                                                                            <input
                                                                                type="text"
                                                                                name="label6"
                                                                                onChange={e => this.handleChange(e)}
                                                                                value={label6}
                                                                                key={1}
                                                                            />
                                                                            <button
                                                                                type="button"
                                                                                key={0}
                                                                                style={{ backgroundColor: color6 }}
                                                                                onClick={() => this.setState({ color5: color6 === '#d1d1d1' ? color6 : '#d1d1d1' })}
                                                                            >
                                                                            color
                                                                            </button>
                                                                        </div>
                                                                    )
                                                                        }
                                                                    </React.Fragment>
                                                                )
                                                                    }
                                                                </div>
                                                            </div>
                                                            <div className="subtitleEditing">
                                                                <TextareaAutosize
                                                                    type="text"
                                                                    name="titleCompare"
                                                                    onChange={e => this.handleChange(e)}
                                                                    value={titleCompare}
                                                                />
                                                            </div>
                                                            <LineCharts
                                                                id={1}
                                                                plan={sheetSel}
                                                                planCompare={sheetSelCompare}
                                                                editAreaLabel={!viewAreaLabel ? -1 : parseInt(areaLabel, 10)}
                                                                data={data}
                                                                chartsType={chartsType}
                                                                differenceAge={differenceAge}
                                                                client={client}
                                                                partner={partner}
                                                                compare={compareGraph}
                                                                clientCase={false}
                                                                labelChart={label}
                                                                color1={color1}
                                                                color2={color2}
                                                                color3={color3}
                                                                color4={color4}
                                                                color5={color5}
                                                                color6={color6}
                                                                label1={label1}
                                                                label2={label2}
                                                                label3={label3}
                                                                label4={label4}
                                                                label5={label5}
                                                                handleToUpdate={this.handleToUpdate.bind(this)}
                                                                editLabel={editLabel}
                                                                editLine={editLine}
                                                                editIcon={editIcon}
                                                                editPosition={editPosition}
                                                                editPositionX={editPositionX}
                                                                editPositionLabel={editPositionLabel}
                                                                editPositionLabelX={editPositionLabelX}
                                                                editPositionDot={editPositionDot}
                                                                editPositionDotX={editPositionDotX}
                                                                editAxis={editAxis}
                                                                minAxisAllChart={isNaN(minAxisClient) ? 0 : parseFloat(minAxisClient)}
                                                                maxAxisAllChart={clientCase ? chartScaleMax1 : parseFloat(maxAxisClient)}
                                                                chartsCompareType={chartsCompareType}
                                                                year={year}
                                                                edit
                                                                editNewPosition={(val, id, elemArray, arrayId) => self.editNewPosition(val, id, elemArray, arrayId, 'y')}
                                                                editNewPositionX={(val, id, elemArray, arrayId) => self.editNewPosition(val, id, elemArray, arrayId, 'x')}
                                                                editNewPositionLabel={(val, id, elemArray, arrayId) => self.editNewPosition(val, id, elemArray, arrayId, 'labelY')}
                                                                editNewPositionLabelX={(val, id, elemArray, arrayId) => self.editNewPosition(val, id, elemArray, arrayId, 'labelX')}
                                                                editNewPositionDot={(val, id, elemArray, arrayId) => self.editNewPosition(val, id, elemArray, arrayId, 'dot_y')}
                                                                editNewPositionDotX={(val, id, elemArray, arrayId) => self.editNewPosition(val, id, elemArray, arrayId, 'dot_x')}
                                                                width={width}
                                                                height={height - 100}
                                                                step={step}
                                                                exclude={exclude}
                                                                charScale={chartScale}
                                                                viewAxis={viewAxis}
                                                                viewAxisY={viewAxisY}
                                                                viewAxisYInterval={viewAxisYInterval}
                                                                minAxisAllChart1={minAxisClient1}
                                                                maxAxisAllChart1={maxAxisClient1}
                                                                minAxisAllChart2={minAxisClient2}
                                                                maxAxisAllChart2={maxAxisClient2}
                                                                minAxisAllChart3={minAxisClient3}
                                                                maxAxisAllChart3={maxAxisClient3}
                                                                options={options}
                                                            />
                                                            {viewText
                                                        && (
                                                            <div style={{
                                                                position: 'relative',
                                                                top: viewAxis ? '0px' : '-50px',
                                                                padding: '0 50px',
                                                            }}
                                                            >
                                                                <input
                                                                    name="input1"
                                                                    onChange={e => self.handleChange(e)}
                                                                    style={{
                                                                        fontSize: '14px',
                                                                        fontWeight: '700',
                                                                        width: '100%',
                                                                        textAlign: 'center',
                                                                        marginBottom: '25px',
                                                                    }}
                                                                    type="text"
                                                                    value={self.state.input1}
                                                                />
                                                                <TextareaAutosize
                                                                    onChange={e => self.handleChange(e)}
                                                                    name="textarea2"
                                                                    style={{ fontSize: '14px' }}
                                                                    value={self.state.textarea2}
                                                                />
                                                            </div>
                                                        )
                                                            }
                                                        </div>
                                                    )
                                            }
                                            {
                                                editType === 'title'
                                                    && (
                                                        <div className="title_slide_editing">
                                                            {
                                                                viewLine
                                                                    ? (
                                                                        <div className={viewLine ? 'titleWithLine containerFieldFlex' : 'containerFieldFlex'}>
                                                                            <input
                                                                                className="boldText"
                                                                                type="text"
                                                                                name="title"
                                                                                value={title}
                                                                                onChange={e => this.handleChange(e)}
                                                                            />
                                                                            <input
                                                                                className="lightText"
                                                                                type="text"
                                                                                name="titleLight"
                                                                                value={titleLight}
                                                                                onChange={e => this.handleChange(e)}
                                                                            />
                                                                        </div>
                                                                    )
                                                                    : (
                                                                        <div>
                                                                            <input
                                                                                type="text"
                                                                                name="title"
                                                                                value={title}
                                                                                onChange={e => this.handleChange(e)}
                                                                            />
                                                                        </div>
                                                                    )
                                                            }
                                                            <div className="containerFieldFlex">
                                                                <input
                                                                    type="text"
                                                                    name="titleCompare"
                                                                    value={titleCompare}
                                                                    onChange={e => this.handleChange(e)}
                                                                />
                                                            </div>
                                                        </div>
                                                    )
                                            }
                                            {
                                                editType === 'box'
                                                    && (
                                                        <div>
                                                            <div id="title_editing">
                                                                <div className="column-1">
                                                                    <input
                                                                        className="title"
                                                                        type="text"
                                                                        name="title"
                                                                        onChange={e => this.handleChange(e)}
                                                                        value={title}
                                                                    />
                                                                    <input
                                                                        className="subtitle"
                                                                        type="text"
                                                                        name="titleCompare"
                                                                        onChange={e => this.handleChange(e)}
                                                                        value={titleCompare}
                                                                    />
                                                                </div>
                                                                <div />
                                                            </div>
                                                            <Box
                                                                edit
                                                                preselect={yourPriorities}
                                                                numBox={parseInt(numBox, 10)}
                                                                editIcon={this.handleChange.bind(this)}
                                                                popupIcon={(val, elemType, id) => this.setState({
                                                                    popIcon: val,
                                                                    elemType: 'single',
                                                                    idIcon: id,
                                                                })}
                                                                icon1={icon1}
                                                                icon2={icon2}
                                                                icon3={icon3}
                                                                icon4={icon4}
                                                                arrayLabel1={arrayLabel1}
                                                                arrayLabel2={arrayLabel2}
                                                                arrayLabel3={arrayLabel3}
                                                                arrayLabel4={arrayLabel4}
                                                                label1={label1}
                                                                label2={label2}
                                                                label3={label3}
                                                                label4={label4}
                                                                changeIcon={(val, id) => this.changeIcon(val, id)}
                                                                changeLabel={(val, id) => this.changeLabel(val, id)}
                                                                width={width}
                                                                height={height - 200}
                                                            />
                                                        </div>
                                                    )
                                            }
                                            {
                                                editType === 'text_description'
                                                    && (
                                                        <div>
                                                            <div id="title_editing">
                                                                <div>
                                                                    <input
                                                                        type="text"
                                                                        name="title"
                                                                        className="title"
                                                                        onChange={e => this.handleChange(e)}
                                                                        value={title}
                                                                    />
                                                                    <textarea
                                                                        className="subtitle"
                                                                        name="titleCompare"
                                                                        onChange={e => this.handleChange(e)}
                                                                        value={typeof titleCompare !== 'undefined' ? titleCompare.replace(/<br\s*[/]?>/gi, '\n') : ''}
                                                                    />
                                                                </div>
                                                                <div />
                                                            </div>
                                                            <Text
                                                                advisor={advisorName}
                                                                chartsType={chartsType}
                                                                edit
                                                                icon1=""
                                                                icon2=""
                                                                icon3=""
                                                                icon4=""
                                                                label={val => this.changeLabel(val, 1)}
                                                                label1={label1}
                                                                label2={label2}
                                                                label3={label3}
                                                                label4={label4}
                                                                editText={(labelId, arrayId, arrayName, val) => this.editText(labelId, arrayId, arrayName, val)}
                                                                width={width}
                                                                height={height - 200}
                                                            />
                                                        </div>
                                                    )
                                            }
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

Slide.propTypes = {
    // arrayData: PropTypes.arrayOf(PropTypes.array),
    // history: PropTypes.object.isRequired,
    // users: PropTypes.object,
    // webappPlan: PropTypes.object,
    allAssetPlan: PropTypes.object,
    // actionWebappButton: PropTypes.object,
    isRequestedDone: PropTypes.number,
    handleRequestsReset: PropTypes.func.isRequired,
    handleGetPlan: PropTypes.func.isRequired,
    slidePresentation: PropTypes.object,
    slideUUID: PropTypes.string,
    orderSlide: PropTypes.number,
    onSaveData: PropTypes.func.isRequired,
    idGoogle: PropTypes.string,
    // onSaveClient: PropTypes.func.isRequired,
};

Slide.defaultProps = {
    // formValue: {},
    // usersOptions: {},
    // arrayData: [],
    // users: {},
    // webappPlan: {},
    allAssetPlan: {},
    // actionWebappButton: {},
    isRequestedDone: 0,
    slidePresentation: {},
    slideUUID: null,
    orderSlide: null,
    idGoogle: null,
};

const mapStateToProps = (state, props) => ({
    slidePresentation: get(state, 'webapp.slidePresentation', {}),
    allAssetPlan: get(state, 'webapp', {}),
    isRequestedDone: get(state, 'common.isRequestsDone', 0),
    actionWebappButton: get(state, 'actionWebappButton', {}),
    slideUUID: get(props, 'match.params.id', null),
    idGoogle: get(props, 'match.params.name', null),
    orderSlide: get(props, 'location.state.order', null),
});

const mapDispatchToProps = dispatch => ({
    onSaveData: (data, idGoogle, arrayNum, action, type) => dispatch(savePlan(data, idGoogle, arrayNum, action, type)),
    handleGetPlan: data => dispatch(reloadPage(data)),
    handleRequestsReset: bindActionCreators(requestsReset, dispatch),
});

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(Slide));
