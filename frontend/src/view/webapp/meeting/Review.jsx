/* eslint-disable max-len */
/* eslint react/forbid-prop-types:0 */
import React, { Component, Fragment } from 'react';
import { Link, withRouter } from 'react-router-dom';
import PropTypes from 'prop-types';
import { StickyContainer, Sticky } from 'react-sticky';
import { connect } from 'react-redux';
import Loadable from 'react-loadable';
import html2canvas from 'html2canvas';
import { change } from 'redux-form';
import get from 'lodash/get';
import * as Scroll from 'react-scroll';
import { bindActionCreators } from 'redux';
import { savePlan, reloadPage } from '../../../actions/WebappActions';
import { filterById } from '../../../helpers/WebappFunctions';
import { requestsReset } from '../../../actions/CommonActions';
import { Loading } from '../../../components/Loading';
import Preload from '../../../components/Loading/preload';
import ToDoList from '../../../components/ToDoList/index';

const JsPDF = require('jspdf');
// import Table from '../../../components/Table/table';
const Table = Loadable({
    loader: () => import('../../../components/Table/table' /* webpackChunkName: "table" */),
    loading: Loading,
});
// import Line_Charts from '../../../components/Charts/Line_Charts';
const LineCharts = Loadable({
    loader: () => import('../../../components/Charts/LineCharts' /* webpackChunkName: "line-chart" */),
    loading: Loading,
});
// import Box from '../../../components/Box_Text/box';
const Box = Loadable({
    loader: () => import('../../../components/BoxText/box' /* webpackChunkName: "box" */),
    loading: Loading,
});
// import Text from '../../../components/Box_Text/text';
const Text = Loadable({
    loader: () => import('../../../components/BoxText/text' /* webpackChunkName: "text" */),
    loading: Loading,
});
const AnchorLink = Scroll.Link;
const scroll = Scroll.animateScroll;

const logoOctopusCo = '/static/img/Octopus_Wealth.svg';

class Review extends Component {
    constructor(props) {
        super(props);
        const {
            idGoogle,
        } = this.props;
        this.state = {
            printPage: false,
            idGoogleSheet: idGoogle,
            saved: false,
            toSave: false,
            saving: false,
            hidetitle: false,
            SoASlide: false,
            width: 1440,
            height: 810,
            step: 15,
            viewAxisYInterval: 5,
            visibleLabel: false,
            minAxisClient: 0,
            maxAxisClient: 0,
        };
        this.setCurrentState = this.setCurrentState.bind(this);
        this.saveNotes = this.saveNotes.bind(this);
        this.removeClientCases = this.removeClientCases.bind(this);
        this.handleData = this.handleData.bind(this);
        this.chartScreen = this.chartScreen.bind(this);
        this.printPageFunc = this.printPageFunc.bind(this);
        this.printPDF = this.printPDF.bind(this);
        this.filterCard = this.filterCard.bind(this);
        this.saveSlideData = this.saveSlideData.bind(this);
    }

    componentDidUpdate() {
        const {
            handleRequestsReset,
            isRequestedDone,
            propsPosition,
        } = this.props;
        const {
            setCurrentState,
        } = this;
        if (isRequestedDone === 1) {
            let dataToSave = [false, false, true];
            setCurrentState(dataToSave);
            setTimeout(() => {
                dataToSave = [false, false, false];
                setCurrentState(dataToSave);
            }, 3000);
            handleRequestsReset();
        } if (propsPosition !== null) {
            const offsetScroll = document.getElementById(`el${propsPosition}`).offsetTop;
            scroll.scrollTo(offsetScroll);
        }
    }

    setCurrentState(dataToSave) {
        this.setState({
            saving: dataToSave[0],
            toSave: dataToSave[1],
            saved: dataToSave[2],
        });
    }

    handleData($type, $id, $order, $idNext, $idOrderNext) {
        const {
            allDataDef,
            gData,
            handleReloadPlan,
        } = this.props;
        const {
            idGoogleSheet,
        } = this.state;
        const allDataVar = gData;
        const arrayRendered = allDataDef;
        arrayRendered.sort((a, b) => {
            const keyA = parseInt(a.order, 10);
            const keyB = parseInt(b.order, 10);
            // Compare the 2 dates
            if (keyA < keyB) return -1;
            if (keyA > keyB) return 1;
            return 0;
        });
        allDataVar.sort((a, b) => {
            const keyA = parseInt(a[34], 10);
            const keyB = parseInt(b[34], 10);
            // Compare the 2 dates
            if (keyA < keyB) return -1;
            if (keyA > keyB) return 1;
            return 0;
        });
        switch ($type) {
            case 'order':
                allDataVar[$id][34] = $order;
                allDataVar[$idNext][34] = $idOrderNext;

                arrayRendered[$id - 1].order = $order;
                arrayRendered[$idNext - 1].order = $idOrderNext;

                break;
            case 'delete':
                allDataVar.splice($id + 1, 1);
                arrayRendered.splice($id, 1);
                break;
            case 'visibility':
                allDataVar[$id + 1][35] = parseInt(allDataVar[$id + 1][35], 10) === 1 ? 0 : 1;
                arrayRendered[$id].exclude = parseInt(arrayRendered[$id].exclude, 10) === 1 ? 0 : 1;
                break;
            case 'scale':
                allDataVar[$id - 1][39] = parseInt($order, 10);
                arrayRendered[$id - 2].chartScale = parseInt($order, 10);
                break;
            default:
                return null;
        }
        arrayRendered.sort((a, b) => {
            const keyA = parseInt(a.order, 10);
            const keyB = parseInt(b.order, 10);
            // Compare the 2 dates
            if (keyA < keyB) return -1;
            if (keyA > keyB) return 1;
            return 0;
        });
        allDataVar.sort((a, b) => {
            const keyA = parseInt(a[34], 10);
            const keyB = parseInt(b[34], 10);
            // Compare the 2 dates
            if (keyA < keyB) return -1;
            if (keyA > keyB) return 1;
            return 0;
        });
        this.setState({
            saved: false,
            toSave: true,
        });
        const dataPlan = {
            idGoogle: idGoogleSheet,
        };
        handleReloadPlan(dataPlan);
        return null;
    }

    saveNotes(type, arrayId, val) {
        const {
            gData,
        } = this.props;
        gData[arrayId][38] = JSON.stringify(val);
        this.setState({
            toSave: true,
            saved: false,
        });
    }

    chartScreen(getEl) {
        this.setState({
            hidetitle: true,
        });
        const self = this;
        html2canvas(document.getElementById(getEl), {
            width: 1400,
            height: 810,
            scale: 4,
            x: 70,
        }).then((canvas) => {
            // Export the canvas to its data URI representation
            const a = document.createElement('a');
            a.href = canvas.toDataURL({
                format: 'png',
                quality: 1,
                multiplier: 100,
                width: 2880,
                height: 1560,
            });
            a.download = 'chart_meeting.png';
            a.click();
            self.setState({
                hidetitle: false,
            });
        });
    }

    printPageFunc() {
        const {
            SoASlide,
        } = this.state;
        const self = this;
        self.setState({ printPage: true });
        setTimeout(() => {
            window.print();
        }, 5000);
        setTimeout(() => {
            self.setState({ printPage: false });
            if (SoASlide) {
                window.location.reload();
            }
        }, 5000);
    }

    printPDF() {
        const self = this;
        self.setState({ printPage: true });
        setTimeout(() => {
            const divToPrint = document.getElementsByClassName('pdf_print');
            const numEl = divToPrint.length;
            const doc = new JsPDF('l', 'px', [1440, 810]);
            let num = 1;
            Array.prototype.forEach.call(divToPrint, (item) => {
                html2canvas(document.getElementById(item.id), {
                    width: 1400,
                    height: 810,
                    scale: 3,
                }).then((canvas) => {
                    const imgData = canvas.toDataURL('image/jpeg').replace('image/jpeg', 'image/octet-stream');
                    if (num > 1) {
                        doc.addPage();
                    }
                    if (num === numEl) {
                        doc.addImage(imgData, 'JPEG', 0, 5, 1440, 810);
                        doc.link(0, 350, 1440, 200, { url: 'https://octopuswealth.com/terms-of-business' });
                        doc.save('sample-file.pdf');
                        self.setState({ printPage: false });
                    } else {
                        doc.addImage(imgData, 'JPEG', 0, 5, 1440, 810);
                    }
                    num += 1;
                });
            });
        }, 5000);
    }

    filterCard() {
        const {
            allDataDef,
        } = this.props;
        const {
            SoASlide,
        } = this.state;
        let allSlides;
        if (SoASlide) {
            // window.location.reload();
        } else {
            allSlides = allDataDef;
            let slide = false;
            allSlides.map((item, i) => {
                if (item.title.toLowerCase() === 'our proposal to you' || item.title === '{"title":"Our proposal","lightTitle":"to you"}') {
                    slide = true;
                    allSlides[i].title = '{"title":"Our proposal","lightTitle":"to you"}';
                    allSlides[i].options = { backgroundLine: true };
                }
                if (!slide || item.title === 'Next steps') {
                    allSlides[i].exclude = 1;
                } else {
                    allSlides[i].exclude = 0;
                }
                return null;
            });
            this.printPageFunc();
        }
        this.setState({
            SoASlide: !SoASlide,
        });
    }

    removeClientCases(e, data) {
        const { gData, handlePatchPlan } = this.props;
        const { idGoogleSheet } = this.state;
        const allDataVar = gData;
        // const arrayRendered = allDataDef;
        allDataVar.map((item, i) => {
            if (item[0] === 'client_cases') {
                allDataVar[i][35] = data.length > 0 ? 0 : 2;
            }
            return null;
        });
        const dataPlan = {
            idGoogle: idGoogleSheet,
            gData: allDataVar,
        };
        // onReloadPlan(allDataVar);
        handlePatchPlan(dataPlan);
        // onSaveData(allDataVar, this.state.idGoogleSheet, this.state.currentTime);
    }

    saveSlideData(e) {
        e.preventDefault();
        const { handlePatchPlan, gData } = this.props;
        const { idGoogleSheet } = this.state;
        this.setState({
            saving: true,
        });
        const payload = {
            gData, idGoogle: idGoogleSheet, arrayNum: 0, save_type: 'update',
        };
        handlePatchPlan(payload);
    }

    render() {
        const {
            allDataDef,
            slidePresentation,
            idGoogle,
        } = this.props;
        const {
            width,
            height,
            printPage,
            SoASlide,
            idGoogleSheet,
            step,
            hidetitle,
            viewAxisYInterval,
            saved,
            toSave,
            saving,
            visibleLabel,
            minAxisClient,
            maxAxisClient,
        } = this.state;
        const {
            saveNotes,
            handleData,
            chartScreen,
            saveSlideData,
            openModal,
            printPageFunc,
            removeClientCases,
            printPDF,
            filterCard,
        } = this;
        let zoom = 0;
        if (!printPage) {
            zoom = ((window.innerWidth / 2) / width) - 0.05;
        } else {
            zoom = 0.75;
        }
        // const filteredDataView = filterById(allDataDef, 'exclude', 0);
        // const filteredDataHide = filterById(allDataDef, 'exclude', 1);
        const filteredDataRemoved = filterById(allDataDef, 'exclude', 2);
        const filteredData = allDataDef;
        const saveData = saved ? 'SAVED' : 'SAVE';
        return (
            <div className={printPage ? 'App review_page printing' : 'App review_page'}>
                <StickyContainer>
                    {
                        !printPage && (
                            <div id="navigation_review">
                                <Sticky id="stickyMenu" distanceFromTop="0px">
                                    {
                                        () => (
                                            <ul>
                                                {
                                                    filteredData.map((item, i) => (
                                                        <li
                                                            key={parseInt(i.toString(), 10)}
                                                            className={(item.clientCase && item.title.toLowerCase() !== 'your lifeline') ? 'clientCase_list' : 'your_lifeine'}
                                                            style={{
                                                                display: item.exclude === 2 ? 'none' : '',
                                                            }}
                                                        >
                                                            <AnchorLink
                                                                activeClass="active"
                                                                to={`el${i}`}
                                                                spy
                                                                smooth
                                                                offset={0}
                                                                duration={500}
                                                            >
                                                                <span className="overlay">
                                                                    {item.title}
                                                                    <br />
                                                                    <small>{item.titleCompare}</small>
                                                                </span>
                                                            </AnchorLink>
                                                        </li>
                                                    ))
                                                }
                                            </ul>
                                        )
                                    }
                                </Sticky>
                            </div>
                        )
                    }
                    <div className="wrapContent">
                        {
                            !SoASlide && (
                                <div className="cover_slide single_slide">
                                    <div className="rowFlex">
                                        <div
                                            className={!printPage ? 'col-md-6' : 'col-md-12'}
                                            style={{
                                                backgroundColor: !printPage ? '#000000' : '#FFFFFF',
                                                height: printPage ? height - 100 : '',
                                            }}
                                        >
                                            <div
                                                style={{
                                                    zoom,
                                                    width,
                                                    height,
                                                    backgroundColor: '#FFFFFF',
                                                }}
                                                className="pdf_print"
                                                id="divToPrint_0"
                                            >
                                                <header>
                                                    <div className="presentationHeader">
                                                        <div>
                                                            <img src={logoOctopusCo} alt="Logo Octopus" />
                                                        </div>
                                                        {
                                                            allDataDef.length > 0 && (
                                                                <div>
                                                                    {slidePresentation.globalClient}
                                                                    {slidePresentation.globalPartner && ` & ${slidePresentation.globalPartner}`}
                                                                </div>
                                                            )
                                                        }
                                                    </div>
                                                </header>
                                                <div
                                                    className="intro"
                                                    style={{ height: window.innerHeight - 50 }}
                                                >
                                                    <div style={{
                                                        width,
                                                        height: height - 50,
                                                        display: 'flex',
                                                    }}
                                                    >
                                                        <img src={logoOctopusCo} alt="Logo Octopus" />
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        {
                                            !printPage && (
                                                <div className="col-md-6">
                                                    <div className="generalNotes">
                                                        <h3>GENERAL NOTES</h3>
                                                        <div key={0} className="toDoListNotes">
                                                            <ToDoList
                                                                saveNote={(type, arrayId, val) => saveNotes(type, arrayId, val)}
                                                                globaldata={slidePresentation.globalNotes}
                                                                arrayId={0}
                                                                gsheet={idGoogleSheet}
                                                            />
                                                        </div>
                                                    </div>
                                                </div>
                                            )
                                        }
                                    </div>
                                </div>
                            )
                        }
                        {
                            filteredData.map((item, i) => {
                                if (item.exclude !== 1 || !printPage) {
                                    let viewLegend = true;
                                    if (typeof item.options.viewLegend !== 'undefined') {
                                        if (!item.options.viewLegend) {
                                            viewLegend = false;
                                        }
                                    }
                                    const backColor = item.options.backgroundLine ? `${item.chartsType} pdf_print intro` : `${item.chartsType} pdf_print`;
                                    const lineChartArea = item.options.viewAreaLabel ? parseInt(item.areaLabel, 10) : -1;
                                    return (
                                        <div
                                            key={parseInt(i.toString(), 10)}
                                            id={`el${i}`}
                                            className={item.exclude === 1 ? 'slide_disabled' : ''}
                                            style={{
                                                pageBreakInside: 'avoid',
                                                position: 'relative',
                                                width: '100%',
                                                display: item.exclude === 2 ? 'none' : '',
                                            }}
                                        >
                                            <div
                                                className={parseInt(item.areaLabel, 10) >= 0 ? `${item.chartsType} areaLabel single_slide ${visibleLabel}` : `${item.dataType} single_slide ${item.chartsType}`}
                                                key={parseInt(i.toString(), 10)}
                                            >
                                                <div className="row">
                                                    <div
                                                        className={!printPage ? 'col-md-6' : 'col-md-12'}
                                                        style={{
                                                            backgroundColor: !printPage ? '#000000' : '#FFFFFF',
                                                            height: printPage ? height - 25 : '',
                                                            alignItems: 'center',
                                                        }}
                                                    >
                                                        <div
                                                            className={typeof item.options.backgroundLine !== 'undefined' ? backColor : `${item.chartsType} pdf_print`}
                                                            style={{
                                                                zoom,
                                                                width,
                                                                height,
                                                                backgroundColor: '#FFF',
                                                                backgroundRepeat: 'no-repeat',
                                                                backgroundSize: 'contain',
                                                                backgroundPosition: 'bottom center',
                                                            }}
                                                            id={`divToPrint_${i + 1}`}
                                                        >
                                                            <header>
                                                                <div className="presentationHeader">
                                                                    <div>
                                                                        <img src={logoOctopusCo} alt="Logo Octopus" />
                                                                    </div>
                                                                    {
                                                                        allDataDef.length > 0 && (
                                                                            <div>
                                                                                {slidePresentation.globalClient}
                                                                                {slidePresentation.globalPartner && ` & ${slidePresentation.globalPartner}`}
                                                                            </div>
                                                                        )
                                                                    }
                                                                </div>
                                                            </header>

                                                            <div style={{
                                                                width,
                                                                height: height - 50,
                                                                display: 'flex',
                                                            }}
                                                            >
                                                                {
                                                                    item.dataType === 'table' && (
                                                                        <div id={`chartScreen${i}`}>
                                                                            <div className="heading_title">
                                                                                <div>
                                                                                    <h3>{item.title}</h3>
                                                                                    {
                                                                                        item.titleCompare &&
                                                                                        <h4>{item.titleCompare}</h4>
                                                                                    }
                                                                                </div>
                                                                            </div>
                                                                            <Table
                                                                                plan={item.plan}
                                                                                planCompare={item.planCompare}
                                                                                assumesPortfolio={item.assumesPortfolio}
                                                                                chartsType={item.chartsType}
                                                                                childName1={item.childA}
                                                                                childName2={item.areaLabel}
                                                                                label_area={item.areaLabel}
                                                                                competition={item.competition}
                                                                                data={item.data}
                                                                                edit={false}
                                                                                height={height - 200}
                                                                                idGoogle={idGoogleSheet}
                                                                                ihtAVal={parseFloat(item.ihta)}
                                                                                ihtBVal={parseFloat(item.ihtb)}
                                                                                label1={item.label1}
                                                                                label2={item.label2}
                                                                                label3={item.label3}
                                                                                label4={item.label4}
                                                                                totalTER={1000}
                                                                                octopusCarib={item.octopusCarib}
                                                                                year={parseFloat(item.year)}
                                                                                width={width}
                                                                                step={step}
                                                                                options={item.options}
                                                                                title={item.title}
                                                                                titleCompare={item.titleCompare}
                                                                            />
                                                                        </div>
                                                                    )
                                                                }
                                                                {
                                                                    item.dataType === 'chart' && (
                                                                        <div id={`chartScreen${i}`} className="slide_chart_class">
                                                                            <div
                                                                                className="heading_title"
                                                                                style={{
                                                                                    width,
                                                                                }}
                                                                            >
                                                                                <div style={{ visibility: hidetitle ? 'hidden' : 'visible' }}>
                                                                                    <h3>{item.title}</h3>
                                                                                    {
                                                                                        item.titleCompare &&
                                                                                        <h4>{item.titleCompare}</h4>
                                                                                    }
                                                                                </div>
                                                                                {
                                                                                    viewLegend && (
                                                                                        <div
                                                                                            className={item.label3 ? 'legendChart' : 'legendChart columnInverse'}
                                                                                        >
                                                                                            <Fragment>
                                                                                                {
                                                                                                    (item.label3 && !item.clientCase) && (
                                                                                                        <div>
                                                                                                            {item.label3}
                                                                                                            <span
                                                                                                                style={{ backgroundColor: item.color3 }}
                                                                                                            />
                                                                                                        </div>
                                                                                                    )
                                                                                                }
                                                                                                {((item.label1 && !item.label6) || (item.label6 && item.options.bprNlEnable)) && (
                                                                                                    <div>
                                                                                                        {item.label1}
                                                                                                        <span
                                                                                                            style={{ backgroundColor: item.color1 }}
                                                                                                        />
                                                                                                    </div>
                                                                                                )
                                                                                                }
                                                                                                {((item.label2 && !item.label6) || (item.label6 && item.options.bprEnable)) && (
                                                                                                    <div>
                                                                                                        {item.label2}
                                                                                                        <span
                                                                                                            style={{ backgroundColor: item.color2 }}
                                                                                                        />
                                                                                                    </div>
                                                                                                )
                                                                                                }
                                                                                                {
                                                                                                    (item.label4 && !item.clientCase) && (
                                                                                                        <div>
                                                                                                            {item.label4}
                                                                                                            <span
                                                                                                                style={{ backgroundColor: item.color4 }}
                                                                                                            />
                                                                                                        </div>
                                                                                                    )
                                                                                                }
                                                                                                {
                                                                                                    (item.label5 && !item.clientCase) && (
                                                                                                        <div>
                                                                                                            {item.label5}
                                                                                                            <span
                                                                                                                style={{ backgroundColor: item.color5 }}
                                                                                                            />
                                                                                                        </div>
                                                                                                    )
                                                                                                }
                                                                                                {
                                                                                                    (item.label6 && !item.clientCase) && (
                                                                                                        <div>
                                                                                                            {item.label6}
                                                                                                            <span
                                                                                                                style={{ backgroundColor: item.color6 }}
                                                                                                            />
                                                                                                        </div>
                                                                                                    )
                                                                                                }
                                                                                            </Fragment>
                                                                                        </div>
                                                                                    )
                                                                                }
                                                                            </div>
                                                                            <div>
                                                                                <div>
                                                                                    <LineCharts
                                                                                        plan={item.plan}
                                                                                        planCompare={item.planCompare}
                                                                                        editAreaLabel={typeof item.options.viewAreaLabel !== 'undefined' ? lineChartArea : parseInt(item.areaLabel, 10)}
                                                                                        chartsType={item.chartsType}
                                                                                        chartsCompareType={item.chartCompareType}
                                                                                        client={item.client}
                                                                                        clientCase={item.clientCase}
                                                                                        color1={item.color1}
                                                                                        color2={item.color2}
                                                                                        color3={item.color3}
                                                                                        color4={item.color4}
                                                                                        color5={item.color5}
                                                                                        color6={item.color6}
                                                                                        compare={item.compareGraph}
                                                                                        competition={item.competition}
                                                                                        data={item.data}
                                                                                        differenceAge={item.differenceAge}
                                                                                        edit={false}
                                                                                        editAxis={item.edit_axis}
                                                                                        editIcon={item.icon}
                                                                                        editLabel={item.editLabel}
                                                                                        editPosition={item.editPosition}
                                                                                        editPositionX={item.editPositionX}
                                                                                        editPositionDot={item.editPositionDot}
                                                                                        editPositionDotX={item.editPositionDotX}
                                                                                        editPositionLabel={parseFloat(item.editPositionLabel)}
                                                                                        editPositionLabelX={parseFloat(item.editPositionLabelX)}
                                                                                        editLine={item.editLine}
                                                                                        id={item.id}
                                                                                        labelChart={item.label}
                                                                                        label1={item.label1}
                                                                                        label2={item.label2}
                                                                                        label3={item.label3}
                                                                                        label4={item.label4}
                                                                                        label5={item.label5}
                                                                                        label6={item.label6}
                                                                                        minAxis={item.minAxis}
                                                                                        maxAxis={item.maxAxis}
                                                                                        minAxisAllChart={item.chartScaleMin1}
                                                                                        maxAxisAllChart={item.clientCase ? item.chartScaleMax1 : parseFloat(slidePresentation.maxAxisClient)}
                                                                                        charScale={item.chartScale}
                                                                                        minAxisAllChart1={slidePresentation.minAxisClient1}
                                                                                        maxAxisAllChart1={slidePresentation.maxAxisClient1}
                                                                                        minAxisAllChart2={slidePresentation.minAxisClient2}
                                                                                        maxAxisAllChart2={slidePresentation.maxAxisClient2}
                                                                                        minAxisAllChart3={slidePresentation.minAxisClient3}
                                                                                        maxAxisAllChart3={slidePresentation.maxAxisClient3}
                                                                                        octopusCarib={item.octopusCarib}
                                                                                        partner={item.partner}
                                                                                        width={width}
                                                                                        height={height - 100}
                                                                                        year={item.year}
                                                                                        zoom={zoom}
                                                                                        exclude={item.exclude}
                                                                                        step={step}
                                                                                        viewAxis={typeof item.options.viewAxis !== 'undefined' ? item.options.viewAxis : true}
                                                                                        viewAxisY={typeof item.options.viewAxisY !== 'undefined' ? item.options.viewAxisY : false}
                                                                                        viewAxisYInterval={viewAxisYInterval}
                                                                                        options={item.options}
                                                                                    />
                                                                                </div>
                                                                                {
                                                                                    typeof item.options.viewTextAfterChart !== 'undefined' && (
                                                                                        <Fragment>
                                                                                            {
                                                                                                item.options.viewTextAfterChart && (
                                                                                                    <div className="moreInfoTextChart">
                                                                                                        <h6>{item.options.title}</h6>
                                                                                                        <p>{item.options.text}</p>
                                                                                                    </div>
                                                                                                )
                                                                                            }
                                                                                        </Fragment>
                                                                                    )
                                                                                }
                                                                            </div>
                                                                        </div>
                                                                    )
                                                                }
                                                                {
                                                                    item.dataType === 'title' && (
                                                                        <div id={`chartScreen${i}`} className="col-md-12">
                                                                            <div className="heading_title_slide">
                                                                                {
                                                                                    typeof item.options.backgroundLine !== 'undefined' ? (
                                                                                        <Fragment>
                                                                                            {
                                                                                                item.options.backgroundLine ? (
                                                                                                    <h1 dangerouslySetInnerHTML={{ __html: `${JSON.parse(item.title).title} <span>${JSON.parse(item.title).lightTitle}</span>` }} />
                                                                                                )
                                                                                                    :
                                                                                                    (
                                                                                                        <h1>{item.title}</h1>
                                                                                                    )
                                                                                            }
                                                                                        </Fragment>
                                                                                    )
                                                                                        :
                                                                                        (
                                                                                            <h1>{item.title}</h1>
                                                                                        )
                                                                                }
                                                                                <h2>{item.titleCompare}</h2>
                                                                            </div>
                                                                        </div>
                                                                    )
                                                                }
                                                                {
                                                                    item.dataType === 'box' && (
                                                                        <div id={`chartScreen${i}`}>
                                                                            <div
                                                                                className="heading_title marginTopPres"
                                                                            >
                                                                                <div>
                                                                                    <h3>{item.title}</h3>
                                                                                    {
                                                                                        item.titleCompare &&
                                                                                        <h4>{item.titleCompare}</h4>
                                                                                    }
                                                                                </div>
                                                                            </div>
                                                                            <Box
                                                                                arrayLabel1={item.arrayLabel1}
                                                                                arrayLabel2={item.arrayLabel2}
                                                                                arrayLabel3={item.arrayLabel3}
                                                                                arrayLabel4={item.arrayLabel4}
                                                                                edit={false}
                                                                                icon1={item.arrayLabel1.icon}
                                                                                icon2={item.arrayLabel2.icon}
                                                                                icon3={item.arrayLabel3.icon}
                                                                                icon4={item.arrayLabel4.icon}
                                                                                label1={item.label1}
                                                                                label2={item.label2}
                                                                                label3={item.label3}
                                                                                label4={item.label4}
                                                                                numBox={parseInt(item.chartsType, 10)}
                                                                                width={width}
                                                                                height={height - 200}
                                                                            />
                                                                        </div>
                                                                    )
                                                                }
                                                                {
                                                                    item.dataType === 'text_description' && (
                                                                        <div id={`chartScreen${i}`}>
                                                                            <div className="heading_title">
                                                                                <div>
                                                                                    <h3>{item.title}</h3>
                                                                                    {
                                                                                        item.titleCompare && (
                                                                                            <h4 dangerouslySetInnerHTML={{ __html: item.titleCompare }} />
                                                                                        )
                                                                                    }
                                                                                </div>
                                                                            </div>
                                                                            <Text
                                                                                advisor={slidePresentation.advisor}
                                                                                edit={false}
                                                                                icon1=""
                                                                                icon2=""
                                                                                icon3=""
                                                                                icon4=""
                                                                                label1={item.label1}
                                                                                label2={item.label2}
                                                                                label3={item.label3}
                                                                                label4={item.label4}
                                                                                chartsType={item.chartsType}
                                                                                width={width}
                                                                                height={height - 200}
                                                                            />
                                                                        </div>
                                                                    )
                                                                }
                                                            </div>
                                                        </div>
                                                    </div>
                                                    {
                                                        !printPage && (
                                                            <div className="col-md-6">
                                                                <div className="slideNotes">
                                                                    <h3>
                                                                        SLIDE NOTES
                                                                        {item.exclude === 1 && ' - SLIDE DISABLED'}
                                                                        {item.chartScale && item.chartScale !== 1 && ` - SCALE ${item.chartScale}`}
                                                                    </h3>
                                                                    <div
                                                                        key={0}
                                                                        className={item.exclude === 1 ? 'toDoListNotes disabledButton' : 'toDoListNotes enabledButton'}
                                                                    >
                                                                        <ToDoList
                                                                            saveNote={(type, arrayId, val) => saveNotes(type, arrayId, val)}
                                                                            data={slidePresentation.allDataDef}
                                                                            arrayId={i}
                                                                            gsheet={idGoogleSheet}
                                                                        />
                                                                    </div>
                                                                </div>
                                                                <div id="nav_presentation_button">
                                                                    <h3>SLIDE ACTION</h3>
                                                                    {
                                                                        !printPage &&
                                                                        [
                                                                            <div
                                                                                key={1}
                                                                                className={item.exclude === 1 ? 'disabledButton' : 'enabledButton'}
                                                                            >
                                                                                {item.getType !== 'clientCases' && (
                                                                                    <Link
                                                                                        to={{
                                                                                            pathname: `/webapp/${idGoogle}/slide/${i}`,
                                                                                            state: {
                                                                                                minAxis: minAxisClient,
                                                                                                maxAxis: maxAxisClient,
                                                                                            },
                                                                                        }}
                                                                                        className="primary_button"
                                                                                    >
                                                                                        Edit
                                                                                    </Link>
                                                                                )
                                                                                }
                                                                                <Link
                                                                                    to={{
                                                                                        pathname: `/webapp/${idGoogle}/slide/`,
                                                                                        state: {
                                                                                            order: i + 1,
                                                                                            add_new: i + 1,
                                                                                            minAxis: minAxisClient,
                                                                                            maxAxis: maxAxisClient,
                                                                                        },
                                                                                    }}
                                                                                    className="primary_button"
                                                                                >
                                                                                    Add New
                                                                                </Link>
                                                                                <button
                                                                                    type="button"
                                                                                    onClick={() => handleData(
                                                                                        'visibility',
                                                                                        i,
                                                                                    )}
                                                                                    className="primary_button"
                                                                                >
                                                                                    {item.exclude === 1 ? 'Enable' : 'Disable'}
                                                                                </button>
                                                                                {item.getType !== 'clientCases' && (
                                                                                    <button
                                                                                        type="button"
                                                                                        onClick={() => handleData(
                                                                                            'delete',
                                                                                            i,
                                                                                        )}
                                                                                        className="primary_button"
                                                                                    >
                                                                                        Delete
                                                                                    </button>
                                                                                )
                                                                                }
                                                                                {i !== 0 && (
                                                                                    <button
                                                                                        type="button"
                                                                                        onClick={() => handleData(
                                                                                            'order',
                                                                                            i + 1,
                                                                                            i - 1,
                                                                                            i,
                                                                                            i,
                                                                                        )}
                                                                                        className="primary_button"
                                                                                    >
                                                                                        Move Up
                                                                                    </button>
                                                                                )
                                                                                }
                                                                                {i !== allDataDef.length - 1 && (
                                                                                    <button
                                                                                        type="button"
                                                                                        onClick={() => handleData(
                                                                                            'order',
                                                                                            i + 1,
                                                                                            i + 2,
                                                                                            i + 2,
                                                                                            i + 1,
                                                                                        )}
                                                                                        className="primary_button"
                                                                                    >
                                                                                        Move Down
                                                                                    </button>
                                                                                )
                                                                                }
                                                                                {
                                                                                    (item.getType !== 'clientCases') && (
                                                                                        <button
                                                                                            type="button"
                                                                                            onClick={() => chartScreen(`chartScreen${i}`)}
                                                                                            className="primary_button"
                                                                                        >
                                                                                            EXPORT
                                                                                        </button>
                                                                                    )
                                                                                }
                                                                                {
                                                                                    (!item.clientCase && item.dataType === 'chart') && (
                                                                                        <select
                                                                                            value={item.chartScale}
                                                                                            name={`change_scale${i}`}
                                                                                            onChange={e => handleData(
                                                                                                'scale',
                                                                                                i + 2,
                                                                                                e.target.value,
                                                                                                i + 2,
                                                                                                i + 1,
                                                                                            )}
                                                                                        >
                                                                                            <option value={1}>
                                                                                                Scale 1
                                                                                            </option>
                                                                                            <option value={2}>
                                                                                                Scale 2
                                                                                            </option>
                                                                                            <option value={3}>
                                                                                                Scale 3
                                                                                            </option>
                                                                                        </select>
                                                                                    )
                                                                                }
                                                                            </div>,
                                                                        ]
                                                                    }
                                                                </div>
                                                            </div>
                                                        )
                                                    }
                                                </div>
                                            </div>
                                        </div>
                                    );
                                }
                                return null;
                            })
                        }
                    </div>
                </StickyContainer>
                {
                    printPage ? (
                        <Preload createPresentation={false} />
                    )
                        : (
                            <div className="nav_edit_button">
                                {saved
                                    ? (
                                        <p>Changes Saved</p>
                                    )
                                    : (
                                        <button
                                            type="button"
                                            onClick={e => saveSlideData(e)}
                                            className={toSave ? 'alert_button' : 'primary_button'}
                                        >
                                            {saving ? 'SAVING' : saveData}
                                        </button>
                                    )
                                }
                                <Link
                                    to={`/webapp/${idGoogleSheet}/nav_presentation`}
                                    className="primary_button"
                                    onClick={e => (toSave ? openModal(e, `/webapp/${idGoogleSheet}/presentation`, 0) : '')}
                                >
                                    PRESENTATION
                                </Link>
                                <button
                                    type="button"
                                    onClick={e => printPageFunc(e)}
                                    className="primary_button"
                                >
                                    PRINT
                                </button>
                                <button
                                    type="button"
                                    onClick={e => removeClientCases(e, filteredDataRemoved)}
                                    className="primary_button"
                                >
                                    {filteredDataRemoved.length > 0 ? 'CREATE CLIENT CASE' : 'REMOVE CLIENT CASE' }
                                </button>
                                <button
                                    type="button"
                                    onClick={e => printPDF(e)}
                                    className="primary_button"
                                >
                                    PDF 16:9
                                </button>
                                <button
                                    type="button"
                                    onClick={e => filterCard(e)}
                                    className="primary_button"
                                >
                                    PRINT SoA
                                </button>
                            </div>
                        )
                }
            </div>
        );
    }
}

Review.propTypes = {
    slidePresentation: PropTypes.object,
    gData: PropTypes.arrayOf(PropTypes.array),
    allDataDef: PropTypes.arrayOf(PropTypes.object),
    handlePatchPlan: PropTypes.func.isRequired,
    handleReloadPlan: PropTypes.func.isRequired,
    isRequestedDone: PropTypes.number,
    handleRequestsReset: PropTypes.func.isRequired,
    idGoogle: PropTypes.string,
    // propsState: PropTypes.string,
    propsPosition: PropTypes.string,
    // gsheet: PropTypes.arrayOf(PropTypes.array),
};

Review.defaultProps = {
    slidePresentation: {},
    gData: [],
    allDataDef: {},
    isRequestedDone: 0,
    idGoogle: null,
    // propsState: null,
    propsPosition: null,
    // gsheet: [],
};

const mapStateToProps = (state, props) => ({
    initialValues: {},
    slidePresentation: get(state, 'webapp.slidePresentation', {}),
    gData: get(state, 'webapp.slidePresentation.gData', []),
    isRequestedDone: get(state, 'common.isRequestsDone', 0),
    allDataDef: get(state, 'webapp.slidePresentation.allDataDef', {}),
    idGoogle: get(props, 'match.params.name', null),
    propsPosition: get(props, 'props.location.state.position', null),
    propsState: get(props, 'props.location.state', null),
    gsheet: get(state, 'webapp.gsheet', []),
});

const mapDispatchToProps = dispatch => ({
    changeFieldValue: (field, value) => {
        dispatch(change('addISA', field, value));
    },
    handlePatchPlan: bindActionCreators(savePlan, dispatch),
    handleReloadPlan: bindActionCreators(reloadPage, dispatch),
    handleRequestsReset: bindActionCreators(requestsReset, dispatch),
});

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(Review));
