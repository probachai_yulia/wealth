import React, { Component } from 'react';
import jsonp from 'jsonp';
import { APIKeyGoogle } from '../../../helpers/constants_webapp';

const ENTER_KEY = 13;

class SearchClient extends Component {
    constructor(props) {
        super(props);
        this.state = {
            accessToken: localStorage.getItem('token') || '',
            search: '',
            fileList: [],
            folderList: [],
            dataFolders: [],
            dataSheets: [],
            current: '',
            currentID: '',
        };
    }

    handleKeyInput(e, $type) {
        const {
            accessToken,
            search,
            fileList,
            folderList,
        } = this.state;
        const self = this;
        const value = encodeURIComponent(search.toLowerCase());
        if (e.keyCode === ENTER_KEY || $type === 'btn') {
            self.setState({
                loadingData: true,
            });
            const url = `https://www.googleapis.com/drive/v3/files?q=name+contains+'${value}'&key=${APIKeyGoogle}&access_token=${accessToken}`;
            let arrayFolder = []; let
                arraySheets = [];
            self.setState({
                fileList: [],
                folderList: [],
            });
            jsonp(url, null, (err, data) => {
                if (err) {
                    console.error(err.message);
                } else {
                    const rowLen = data.files.length;
                    data.files.map((item, i) => {
                        const urlToken = `https://www.googleapis.com/drive/v2/files/${item.id}/?key=${APIKeyGoogle}&access_token=${accessToken}`;
                        if (item.mimeType === 'application/vnd.google-apps.folder' || item.mimeType === 'application/vnd.google-apps.spreadsheet') {
                            jsonp(urlToken, null, (err2, data2) => {
                                if (typeof data2.labels !== 'undefined') {
                                    if (!data2.labels.trashed) {
                                        if (data2.mimeType === 'application/vnd.google-apps.folder') {
                                            arrayFolder = folderList;
                                            arrayFolder.push(data2);
                                            self.setState({
                                                folderList: arrayFolder,
                                                loadingData: false,
                                            });
                                        }
                                        if (data2.mimeType === 'application/vnd.google-apps.spreadsheet') {
                                            if (data2.title.indexOf('v9') !== -1) {
                                                arraySheets = fileList;
                                                arraySheets.push(data2);
                                                self.setState({
                                                    fileList: arraySheets,
                                                    loadingData: false,
                                                });
                                            }
                                        }
                                    }
                                }
                            });
                        }
                        if (rowLen === i + 1) {
                            self.setState({
                                loadingData: false,
                            });
                        }
                        return null;
                    });
                }
            });
        }
    }

    googleFolder(id) {
        const {
            accessToken,
            dataSheets,
            dataFolders,
        } = this.state;
        const self = this;
        const url = `https://www.googleapis.com/drive/v2/files/${id}/children?key=${APIKeyGoogle}&access_token=${accessToken}`;
        let arrayFolder = []; let
            arraySheets = [];
        self.setState({
            dataFolders: [],
            dataSheets: [],
            loadingData: true,
            current: id,
        });
        jsonp(url, null, (err, response) => {
            const rowLen = response.items.length;
            response.items.map((item, i) => {
                const url2 = `https://www.googleapis.com/drive/v2/files/${item.id}/?key=${APIKeyGoogle}&access_token=${accessToken}`;
                jsonp(url2, null, (err2, data2) => {
                    if (typeof data2.labels !== 'undefined') {
                        if (!data2.labels.trashed) {
                            if (data2.mimeType === 'application/vnd.google-apps.folder') {
                                arrayFolder = dataFolders;
                                arrayFolder.push(data2);
                                self.setState({
                                    dataFolders: arrayFolder,
                                    current: data2.parents,
                                    currentID: id,
                                });
                            }
                            if (data2.mimeType === 'application/vnd.google-apps.spreadsheet') {
                                if (data2.title.indexOf('v9') !== -1) {
                                    arraySheets = dataSheets;
                                    arraySheets.push(data2);
                                    self.setState({
                                        data2heets: arraySheets,
                                        current: data2.parents,
                                        currentID: id,
                                    });
                                }
                            }
                        }
                    }
                    if (rowLen === i + 1) {
                        self.setState({
                            loadingData: false,
                        });
                    }
                });
                return null;
            });
        });
    }

    handleInput(e) {
        // const name = e.target.name;
        // const value = e.target.value;
        const {
            name,
            value,
        } = e.target;
        this.setState({
            [name]: value,
        });
    }

    render() {
        const {
            fileList,
            folderList,
            search,
            current,
            currentID,
        } = this.state;
        const {
            googleFolder,
        } = this;
        const file = fileList;
        const folder = folderList;
        return (
            <div>
                <div id="loginSection">
                    <h2>Select the client 1st meeting Google sheet - e.g. Meeting 1 Client & Partner v9</h2>
                    <input
                        key={0}
                        type="text"
                        name="search"
                        className="search_field"
                        value={search}
                        onKeyDown={e => this.handleKeyInput(e, 'key')}
                        onChange={e => this.handleInput(e)}
                    />
                    <button
                        type="button"
                        key={1}
                        onClick={e => this.handleKeyInput(e, 'btn')}
                        className="primary_button"
                    >
                        Search
                        {' '}
                        <i className="fas fa-search" />
                    </button>
                    {
                        (current && currentID !== '1d0YLRO6FoJly1UbBD0caFFHk6xcXXpRQ')
                        && (
                            <button
                                type="button"
                                onClick={e => this.handleKeyInput(e, 'btn')}
                                className="secondary_button"
                            >
                                <i className="fas fa-undo" />
                                {' '}
Go Back
                            </button>
                        )
                    }
                    <div id="clientList">
                        <ul>
                            {
                                folder.map((item, i) => (
                                    <li
                                        style={{ cursor: 'pointer', marginBottom: '15px' }}
                                        key={parseInt(i.toString(), 10)}
                                    >
                                        <button
                                            type="button"
                                            className="no_btn"
                                            onClick={() => googleFolder(item.id)}
                                        >
                                            <i className="fas fa-folder-open" />
                                            {item.title}
                                        </button>
                                    </li>
                                ))
                            }
                            {
                                file.map((item, i) => (
                                    <li style={{ cursor: 'pointer', marginBottom: '15px' }} key={parseInt(i.toString(), 10)}>
                                        <a href={`/webapp/${item.id}/review`}>
                                            <i className="far fa-file-excel" />
                                            {' '}
                                            {item.title}
                                        </a>
                                    </li>
                                ))
                            }
                        </ul>
                    </div>
                </div>
            </div>
        );
    }
}

export default SearchClient;
