import React, { Component } from 'react';
import jsonp from 'jsonp';
import PropTypes from 'prop-types';
import { withRouter } from 'react-router-dom';
import connect from 'react-redux/es/connect/connect';

export const parseQueryString = (query) => {
    const vars = query.split('&');
    const queryString = {};
    for (let i = 0; i < vars.length; i += 1) {
        const pair = vars[i].split('=');
        // If first entry with this name
        if (typeof queryString[pair[0]] === 'undefined') {
            queryString[pair[0]] = decodeURIComponent(pair[1]);
            // If second entry with this name
        } else if (typeof queryString[pair[0]] === 'string') {
            queryString[pair[0]] = [queryString[pair[0]], decodeURIComponent(pair[1])];
            // If third or later entry with this name
        } else {
            queryString[pair[0]].push(decodeURIComponent(pair[1]));
        }
    }
    return queryString;
};

class Login extends Component {
    constructor(props) {
        super(props);
        this.state = { accessTokenLocal: localStorage.getItem('token') || '' };
        this.oauthSignIn = this.oauthSignIn.bind(this);
    }

    componentDidMount() {
        const { history } = this.props;
        const { accessTokenLocal } = this.state;
        const self = this;
        const parsedQs = parseQueryString(window.location.href);
        const accessToken = (typeof parsedQs.access_token !== 'undefined') ? parsedQs.access_token : accessTokenLocal;
        console.log(parsedQs);
        const urlCurrent = `https://www.googleapis.com/oauth2/v1/tokeninfo?access_token=${accessToken}`;
        jsonp(urlCurrent, null, (err, data) => {
            if (data.error) {
                self.setState({ accessTokenLocal: '' });
                // onLogin('');
                // localStorage.setItem('token','');
                history.push('/webapp/login');
            } else {
                // onLogin(accessToken);
                self.setState({ accessToken });
                localStorage.setItem('token', accessToken);
                const currentUser = `https://www.googleapis.com/oauth2/v1/userinfo?alt=json&access_token=${localStorage.getItem('token')}`;
                jsonp(currentUser, null, (err2, data2) => {
                    self.setState({
                        currentUser: data2.name,
                    });
                    localStorage.setItem('name', data2.name);
                    history.push('/webapp/search-client');
                });
            }
        });
    }

    oauthSignIn = () => {
        // Google's OAuth 2.0 endpoint for requesting an access token
        const oauth2Endpoint = 'https://accounts.google.com/o/oauth2/v2/auth';

        // Create <form> element to submit parameters to OAuth 2.0 endpoint.
        const form = document.createElement('form');
        form.setAttribute('method', 'GET'); // Send as a GET request.
        form.setAttribute('action', oauth2Endpoint);

        // Parameters to pass to OAuth 2.0 endpoint.
        const params = {
            client_id: '1015512365887-mhreis9vovvtdn01co9jrpj3t73kb137.apps.googleusercontent.com',
            redirect_uri: window.location.href,
            response_type: 'token',
            scope: 'https://www.googleapis.com/auth/drive https://www.googleapis.com/auth/userinfo.profile',
            include_granted_scopes: 'true',
            state: 'pass-through value',
        };

        // Add form parameters as hidden input values.
        Object.keys(params).map((p) => {
            const input = document.createElement('input');
            input.setAttribute('type', 'hidden');
            input.setAttribute('name', p);
            input.setAttribute('value', params[p]);
            form.appendChild(input);
            return null;
        });
        // Add form to page and submit it to open the OAuth 2.0 endpoint.
        document.body.appendChild(form);
        form.submit();
    }

    render() {
        // const { state: { accessToken }, props: { token, onLogin } } = this;
        return (
            <div>
                <div id="loginSection">

                    <h3>Please login using your Octopus Google account</h3>
                    <button
                        type="button"
                        id="sign-in-or-out-button"
                        onClick={this.oauthSignIn}
                        className="primary_button"
                    >
                        Sign In/Authorize
                    </button>
                </div>
            </div>
        );
    }
}

Login.propTypes = {
    history: PropTypes.object.isRequired,
};

Login.defaultProps = {
};

export default withRouter(connect()(Login));
