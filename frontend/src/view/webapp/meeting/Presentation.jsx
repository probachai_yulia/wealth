/* eslint-disable max-len */
import React, { Component } from 'react';
import { Link, withRouter } from 'react-router-dom';

import 'slick-carousel/slick/slick.css';
import 'slick-carousel/slick/slick-theme.css';
import Slider from 'react-slick';

import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import get from 'lodash/get';
import { change } from 'redux-form';
import Preload from '../../../components/Loading/preload';

import { filterById } from '../../../helpers/WebappFunctions';
import Table from '../../../components/Table/table';
// const Table = Loadable({
//    loader: () => import('./../../../components/Table/table' /* webpackChunkName: "table" */),
//    loading: Loading,
// });
import LineCharts from '../../../components/Charts/LineCharts';
// const Line_Charts = Loadable({
//    loader: () => import('./../../../components/Charts/Line_Charts' /* webpackChunkName: "line-chart" */),
//    loading: Loading,
// });
import Box from '../../../components/BoxText/box';
// const Box = Loadable({
//    loader: () => import('./../../../components/Box_Text/box' /* webpackChunkName: "box" */),
//   loading: Loading,
// });
import Text from '../../../components/BoxText/text';
// import { Loading } from '../../../components/Loading';
// const Text = Loadable({
//    loader: () => import('./../../../components/Box_Text/text' /* webpackChunkName: "text" */),
//    loading: Loading,
// });


class Presentation extends Component {
    constructor(props) {
        super(props);
        const {
            idGoogle,
        } = this.props;
        this.state = {
            printPage: false,
            allDataDef: [],
            idGoogleSheet: idGoogle,
            loadNewReview: false,
            loading: true,
            width: 1440,
            height: 810,
            curStep: 6,
            globalClient: '',
            globalPartner: '',
            viewAxisYInterval: 5,
            step: 0,
            maxAxisClient: 0,
            minAxisClient1: 0,
            minAxisClient2: 0,
            minAxisClient3: 0,
            maxAxisClient1: 0,
            maxAxisClient2: 0,
            maxAxisClient3: 0,
        };

        this.assignSlide = this.assignSlide.bind(this);
        this.changeRemoteSlide = this.changeRemoteSlide.bind(this);
    }

    componentDidMount() {
        const self = this;
        const {
            allData,
        } = this.props;
        const data = localStorage.getItem('data') !== null ? JSON.parse(localStorage.getItem('data')) : allData.slidePresentation.allDataDef;
        this.setState({
            allDataDef: data,
            loading: false,
            maxAxisClient: localStorage.getItem('maxAxisClient'),
            maxAxisClient1: localStorage.getItem('maxAxisClient1').split(','),
            minAxisClient1: localStorage.getItem('minAxisClient1').split(','),
            maxAxisClient2: localStorage.getItem('maxAxisClient2').split(','),
            minAxisClient2: localStorage.getItem('minAxisClient2').split(','),
            maxAxisClient3: localStorage.getItem('maxAxisClient3').split(','),
            minAxisClient3: localStorage.getItem('minAxisClient3').split(','),
            globalClient: localStorage.getItem('globalClient'),
            globalPartner: localStorage.getItem('globalPartner'),
            loadNewReview: false,
        });
        setInterval(() => {
            if (localStorage.getItem('currentSlide') > -1 && (localStorage.getItem('currentSlide') !== self.state.currentSlide || localStorage.getItem('step') !== self.state.remoteStep || localStorage.getItem('visibleLabel') !== self.state.visibleLabel || localStorage.getItem('curStep') !== self.state.curStep)) {
                self.setState({
                    currentSlide: localStorage.getItem('currentSlide'),
                    remoteStep: localStorage.getItem('step'),
                    visibleLabel: localStorage.getItem('visibleLabel'),
                });
                self.changeRemoteSlide();
            }
        }, 100);
    }

    changeRemoteSlide() {
        if (typeof this.slider !== 'undefined') {
            this.slider.slickGoTo(localStorage.getItem('currentSlide'));
            this.setState({
                step: localStorage.getItem('step'),
                curStep: localStorage.getItem('curStep'),
            });
        }
    }

    assignSlide(e) {
        this.slider = e;
    }

    render() {
        const {
            allDataDef, width, height, printPage, loadNewReview, idGoogleSheet, globalClient, globalPartner, curStep, viewAxisYInterval, loading, minAxisClient1, minAxisClient2, minAxisClient3, maxAxisClient1, maxAxisClient2, maxAxisClient3, step, maxAxisClient,
        } = this.state;
        const self = this;
        const zoomWidth = window.innerWidth / width;
        const zoomHeight = window.innerHeight / height;
        const zoom = zoomWidth > zoomHeight ? zoomHeight : zoomWidth;
        const currentSlide = this.slider ? this.slider.innerSlider.state.currentSlide : 0;
        const filteredData = filterById(allDataDef, 'exclude', 0);
        const settings = {
            dots: false,
            infinite: false,
            arrows: false,
            accessibility: false,
            draggable: false,
            speed: 1,
            slidesToShow: 1,
            slidesToScroll: 1,
            afterChange: () => this.forceUpdate(),
        };
        if (loading) {
            return (
                <div>
                    <Preload createPresentation={loadNewReview} />
                </div>
            );
        }

        return (
            <div
                className={printPage ? 'App presentation_page printing' : 'App presentation_page'}
            >
                <div style={{ zoom, width, height }}>
                    <header>
                        <div className="presentationHeader">
                            <div>
                                <Link
                                    to={`/webapp/${idGoogleSheet}/review`}
                                >
                                    <img src="/static/img/Octopus_Wealth.svg" alt="Logo Octopus" />
                                </Link>
                            </div>
                            {
                                filteredData.length > 0
                                    && (
                                        <div>
                                            {globalClient}
                                            {globalPartner && ` & ${globalPartner}`}
                                        </div>
                                    )
                            }
                        </div>
                    </header>
                    <div>
                        <Slider ref={this.assignSlide} {...settings} className="Section">
                            <div className="intro" id="divToPrint_0">
                                <div
                                    style={{
                                        width,
                                        height: height - 50,
                                        display: 'flex',
                                        alignItems: 'center',
                                    }}
                                    className="wrapSlide"
                                >
                                    <img src="/static/img/Octopus_Wealth.svg" alt="Logo Octopus" />
                                </div>
                            </div>
                            {
                                filteredData.map((item, i) => {
                                    let viewLegend = true;
                                    if (typeof item.options.viewLegend !== 'undefined') {
                                        if (!item.options.viewLegend) {
                                            viewLegend = false;
                                        }
                                    }
                                    let chartType = '';
                                    if (typeof item.options.bprNlEnable !== 'undefined' && item.dataType === 'chart') {
                                        if (item.options.bprNlEnable && item.options.bprEnable && item.chartsType === 'all_assets_detailed') {
                                            chartType = 'all_assets_full';
                                        } else if ((!item.options.bprNlEnable || item.options.bprEnable) && item.chartsType === 'all_assets_detailed') {
                                            chartType = 'all_assets_bpr';
                                        } else if ((item.options.bprNlEnable || !item.options.bprEnable) && item.chartsType === 'all_assets_detailed') {
                                            chartType = 'all_assets_bprNl';
                                        } else if (item.chartsType === 'all_assets_detailed') {
                                            chartType = 'all_assets_det';
                                        }
                                    }
                                    const nestBackImage = item.options.backgroundLine ? 'url(/static/img/presentation/octopus_first_slide.png)' : '';
                                    const nestAreaLabel = item.options.viewAreaLabel ? parseInt(item.areaLabel, 10) : -1;
                                    if (item.exclude !== 1) {
                                        return (
                                            <div
                                                className={(parseInt(item.areaLabel, 10) >= 0 && item.dataType === 'chart') ? `${item.chartsType} areaLabel single_slide ${self.state.visibleLabel}` : `${item.dataType} single_slide ${item.chartsType} ${chartType}`}
                                                key={i.toString()}
                                                id={`divToPrint_${i + 1}`}
                                            >
                                                {(currentSlide > i || printPage)
                                                    && (
                                                        <div
                                                            style={{
                                                                width,
                                                                height: height - 50,
                                                                display: 'flex',
                                                                alignItems: 'center',
                                                                backgroundImage: typeof item.options.backgroundLine !== 'undefined' ? nestBackImage : '',
                                                                backgroundRepeat: 'no-repeat',
                                                                backgroundSize: 'contain',
                                                                backgroundPosition: 'bottom center',
                                                            }}
                                                            className="wrapSlide"
                                                        >
                                                            {
                                                                item.dataType === 'table'
                                                            && (
                                                                <div>
                                                                    <div className="heading_title">
                                                                        <div>
                                                                            <h3>{item.title}</h3>
                                                                            {
                                                                                item.titleCompare
                                                                            && <h4>{item.titleCompare}</h4>
                                                                            }
                                                                        </div>
                                                                    </div>
                                                                    <Table
                                                                        plan={item.plan}
                                                                        planCompare={item.planCompare}
                                                                        assumesPortfolio={item.assumesPortfolio}
                                                                        chartsType={item.chartsType}
                                                                        childName1={item.childA}
                                                                        childName2={item.areaLabel}
                                                                        label_area={item.areaLabel}
                                                                        competition={item.competition}
                                                                        data={item.data}
                                                                        edit={false}
                                                                        height={height - 200}
                                                                        idGoogle={idGoogleSheet}
                                                                        ihtAVal={parseFloat(item.ihta)}
                                                                        ihtBVal={parseFloat(item.ihtb)}
                                                                        label1={item.label1}
                                                                        label2={item.label2}
                                                                        label3={item.label3}
                                                                        label4={item.label4}
                                                                        totalTER={1000}
                                                                        octopusCarib={item.octopusCarib}
                                                                        year={parseFloat(item.year)}
                                                                        width={width}
                                                                        step={parseFloat(step)}
                                                                        options={item.options}
                                                                        title={item.title}
                                                                        titleCompare={item.titleCompare}
                                                                    />
                                                                </div>
                                                            )
                                                            }
                                                            {
                                                                item.dataType === 'chart'
                                                            && (
                                                                <div>
                                                                    <div className="heading_title">
                                                                        <div>
                                                                            <h3>{item.title}</h3>
                                                                            {
                                                                                item.titleCompare
                                                                            && <h4>{item.titleCompare}</h4>
                                                                            }
                                                                        </div>
                                                                        {
                                                                            viewLegend
                                                                        && (
                                                                            <div
                                                                                className={(item.label3 && !item.clientCase) ? 'legendChart' : 'legendChart columnInverse'}
                                                                            >
                                                                                {
                                                                                    (
                                                                                        (item.label3 && !item.label6 && !item.clientCase)
                                                                                    || (item.label6 && item.options.bprNlEnable && item.options.bprEnable && curStep > 5)
                                                                                    || (item.label6 && item.options.bprNlEnable === false && item.options.bprEnable === true && curStep > 4)
                                                                                    || (item.label6 && item.options.bprNlEnable === true && item.options.bprEnable === false && curStep > 4)
                                                                                    || (item.label6 && !item.options.bprNlEnable && !item.options.bprEnable && curStep > 3)
                                                                                    || (item.label6 && item.options.bprNlEnable === false && item.options.bprEnable === false && curStep > 3))
                                                                                && (
                                                                                    <div>
                                                                                        {item.label3}
                                                                                        <span
                                                                                            style={{ backgroundColor: item.color3 }}
                                                                                        />
                                                                                    </div>
                                                                                )
                                                                                }
                                                                                {((item.label1 && !item.label6) || (item.label6 && item.options.bprNlEnable && curStep > 4) || (item.label6 && item.options.bprNlEnable && item.options.bprEnable === false && curStep > 3))
                                                                            && (
                                                                                <div>
                                                                                    {item.label1}
                                                                                    <span
                                                                                        style={{ backgroundColor: item.color1 }}
                                                                                    />
                                                                                </div>
                                                                            )
                                                                                }
                                                                                {((item.label2 && !item.label6) || (item.label6 && item.options.bprEnable && curStep > 3))
                                                                            && (
                                                                                <div>
                                                                                    {item.label2}
                                                                                    <span
                                                                                        style={{ backgroundColor: item.color2 }}
                                                                                    />
                                                                                </div>
                                                                            )
                                                                                }
                                                                                {
                                                                                    (item.label4 && !item.clientCase && curStep > 2)
                                                                                && (
                                                                                    <div>
                                                                                        {item.label4}
                                                                                        <span
                                                                                            style={{ backgroundColor: item.color4 }}
                                                                                        />
                                                                                    </div>
                                                                                )
                                                                                }
                                                                                {
                                                                                    (item.label5 && !item.clientCase && curStep > 1)
                                                                                && (
                                                                                    <div>
                                                                                        {item.label5}
                                                                                        <span
                                                                                            style={{ backgroundColor: item.color5 }}
                                                                                        />
                                                                                    </div>
                                                                                )
                                                                                }
                                                                                {
                                                                                    (item.label6 && !item.clientCase && curStep > 0)
                                                                                && (
                                                                                    <div>
                                                                                        {item.label6}
                                                                                        <span
                                                                                            style={{ backgroundColor: item.color6 }}
                                                                                        />
                                                                                    </div>
                                                                                )
                                                                                }
                                                                            </div>
                                                                        )
                                                                        }
                                                                    </div>
                                                                    <div>
                                                                        <div>
                                                                            <LineCharts
                                                                                plan={item.plan}
                                                                                planCompare={item.planCompare}
                                                                                editAreaLabel={typeof item.options.viewAreaLabel !== 'undefined' ? nestAreaLabel : parseInt(item.areaLabel, 10)}
                                                                                chartsType={item.chartsType}
                                                                                chartsCompareType={item.chartCompareType}
                                                                                client={item.client}
                                                                                clientCase={item.clientCase}
                                                                                color1={item.color1}
                                                                                color2={item.color2}
                                                                                color3={item.color3}
                                                                                color4={item.color4}
                                                                                color5={item.color5}
                                                                                color6={item.color6}
                                                                                compare={item.compareGraph}
                                                                                competition={item.competition}
                                                                                data={item.data}
                                                                                differenceAge={item.differenceAge}
                                                                                edit={false}
                                                                                editAxis={item.editAxis}
                                                                                editIcon={item.icon}
                                                                                editLabel={item.editLabel}
                                                                                editPosition={item.editPosition}
                                                                                editPositionX={item.editPositionX}
                                                                                editPositionDot={item.editPositionDot}
                                                                                editPositionDotX={item.editPositionDotX}
                                                                                editPositionLabel={item.editPositionLabel}
                                                                                editPositionLabelX={item.editPositionLabelX}
                                                                                editLine={item.editLine}
                                                                                id={item.id}
                                                                                labelChart={item.label}
                                                                                label1={item.label1}
                                                                                label2={item.label2}
                                                                                label3={item.label3}
                                                                                label4={item.label4}
                                                                                label5={item.label5}
                                                                                label6={item.label6}
                                                                                minAxis={item.minAxis}
                                                                                maxAxis={item.maxAxis}
                                                                                minAxisAllChart={item.chartScaleMin1}
                                                                                maxAxisAllChart={item.clientCase ? item.chartScaleMax1 : maxAxisClient}
                                                                                charScale={item.chartScale}
                                                                                minAxisAllChart1={minAxisClient1}
                                                                                maxAxisAllChart1={maxAxisClient1}
                                                                                minAxisAllChart2={minAxisClient2}
                                                                                maxAxisAllChart2={maxAxisClient2}
                                                                                minAxisAllChart3={minAxisClient3}
                                                                                maxAxisAllChart3={maxAxisClient3}
                                                                                options={item.options}
                                                                                octopusCarib={item.octopusCarib}
                                                                                partner={item.partner}
                                                                                width={width}
                                                                                height={height - 100}
                                                                                year={item.year}
                                                                                zoom={zoom}
                                                                                exclude={item.exclude}
                                                                                step={step}
                                                                                viewAxis={typeof item.options.viewAxis !== 'undefined' ? item.options.viewAxis : true}
                                                                                viewAxisY={typeof item.options.viewAxisY !== 'undefined' ? item.options.viewAxisY : false}
                                                                                viewAxisYInterval={viewAxisYInterval}
                                                                            />
                                                                        </div>
                                                                        {
                                                                            typeof item.options.viewTextAfterChart !== 'undefined'
                                                                        && (
                                                                            <React.Fragment>
                                                                                {
                                                                                    item.options.viewTextAfterChart
                                                                                && (
                                                                                    <div className="moreInfoTextChart">
                                                                                        <h6>{item.options.title}</h6>
                                                                                        <p>{item.options.text}</p>
                                                                                    </div>
                                                                                )
                                                                                }
                                                                            </React.Fragment>
                                                                        )
                                                                        }
                                                                    </div>
                                                                </div>
                                                            )
                                                            }
                                                            {
                                                                item.dataType === 'title'
                                                            && (
                                                                <div className="col-md-12">
                                                                    <div className="heading_title_slide">
                                                                        {
                                                                            typeof item.options.backgroundLine !== 'undefined'
                                                                                ? (
                                                                                    <React.Fragment>
                                                                                        {
                                                                                            item.options.backgroundLine
                                                                                                ? <h1 dangerouslySetInnerHTML={{ __html: `${JSON.parse(item.title).title} <span>${JSON.parse(item.title).lightTitle}</span>` }} />
                                                                                                : <h1>{item.title}</h1>
                                                                                        }
                                                                                    </React.Fragment>
                                                                                )
                                                                                : <h1>{item.title}</h1>
                                                                        }
                                                                        <h2>{item.titleCompare}</h2>
                                                                    </div>
                                                                </div>
                                                            )
                                                            }
                                                            {
                                                                item.dataType === 'box'
                                                            && (
                                                                <div>
                                                                    <div className="heading_title marginTopPres">
                                                                        <div>
                                                                            <h3>{item.title}</h3>
                                                                            {
                                                                                item.titleCompare
                                                                            && <h4>{item.titleCompare}</h4>
                                                                            }
                                                                        </div>
                                                                    </div>
                                                                    <Box
                                                                        arrayLabel1={item.arrayLabel1}
                                                                        arrayLabel2={item.arrayLabel2}
                                                                        arrayLabel3={item.arrayLabel3}
                                                                        arrayLabel4={item.arrayLabel4}
                                                                        edit={false}
                                                                        icon1={item.arrayLabel1.icon}
                                                                        icon2={item.arrayLabel2.icon}
                                                                        icon3={item.arrayLabel3.icon}
                                                                        icon4={item.arrayLabel4.icon}
                                                                        label1={item.label1}
                                                                        label2={item.label2}
                                                                        label3={item.label3}
                                                                        label4={item.label4}
                                                                        numBox={parseInt(item.chartsType, 10)}
                                                                        width={width}
                                                                        height={height - 200}
                                                                    />
                                                                </div>
                                                            )
                                                            }
                                                            {
                                                                item.dataType === 'text_description' && (
                                                                    <div>
                                                                        <div className="heading_title">
                                                                            <div>
                                                                                <h3>{item.title}</h3>
                                                                                {
                                                                                    item.titleCompare
                                                                                && <h4 dangerouslySetInnerHTML={{ __html: item.titleCompare }} />
                                                                                }
                                                                            </div>
                                                                        </div>
                                                                        <Text
                                                                            edit={false}
                                                                            icon1=""
                                                                            icon2=""
                                                                            icon3=""
                                                                            icon4=""
                                                                            label1={item.label1}
                                                                            label2={item.label2}
                                                                            label3={item.label3}
                                                                            label4={item.label4}
                                                                            chartsType={item.chartsType}
                                                                            width={width}
                                                                            height={height - 200}
                                                                        />
                                                                    </div>
                                                                )
                                                            }
                                                        </div>
                                                    )
                                                }
                                            </div>
                                        );
                                    }
                                    return null;
                                })
                            }
                        </Slider>
                    </div>
                </div>
            </div>
        );
    }
}

Presentation.propTypes = {
    history: PropTypes.object.isRequired,
    formValue: PropTypes.object,
    allData: PropTypes.object,
    usersOptions: PropTypes.object,
    // assets: PropTypes.arrayOf(PropTypes.object),
    slidePresentation: PropTypes.object,
    // gData: PropTypes.arrayOf(PropTypes.object),
    allDataDef: PropTypes.arrayOf(PropTypes.object),
    idGoogle: PropTypes.string,
};

Presentation.defaultProps = {
    formValue: {},
    allData: {},
    usersOptions: {},
    // assets: [],
    slidePresentation: {},
    // gData: [],
    allDataDef: [],
    idGoogle: null,
};

const mapStateToProps = (state, props) => ({
    initialValues: {},
    allData: get(state, 'webapp', {}),
    slidePresentation: get(state, 'webapp.slidePresentation', {}),
    gData: get(state, 'webapp.slidePresentation.gData', []),
    allDataDef: get(state, 'webapp.slidePresentation.allDataDef', []),
    idGoogle: get(props, 'match.params.name', null),
});

const mapDispatchToProps = dispatch => ({
    changeFieldValue: (field, value) => {
        dispatch(change('addISA', field, value));
    },
});


export default withRouter(connect(mapStateToProps, mapDispatchToProps)(Presentation));
