import re

from marshmallow import Schema, ValidationError, fields, validates
from tornado.options import options

from libraries.profile import ProfileClient
from models.model import (
    ApproximateAge,
    AssetType,
    CurrentSituationType,
    EmploymentType,
    Frequency,
    GivingType,
    MaritalStatus,
    Permission,
    PlanAsset,
    PlanCashflow,
    PlanDebt,
    PlanDebtType,
    PlanUser,
    PlanUserType,
    RateType,
    RegularContribution,
    RegularContributionType,
    Role,
    SoftFactTag,
    TaxBenefit,
    USConnection,
    UserCashflowType,
    PlanDocument,
)
from utils.constants import COUNTY_CD_LIST


class AddressSchema(Schema):
    address_1 = fields.Str(required=True)
    address_2 = fields.Str()

    postcode = fields.Str(required=True)
    is_primary_address = fields.Boolean(allow_none=True)
    city = fields.Str(required=True)
    country_cd = fields.Str()

    @validates("country_cd")
    def validate_country_cd(cls, key):
        if key not in COUNTY_CD_LIST:
            raise ValidationError("Invalid country cd: %s" % key)


class PasswordSchema(Schema):
    password = fields.Str(required=True)
    new_password = fields.Str(required=True)

    @validates("new_password")
    def validate_password(cls, key):
        if len(str(key)) > 36 and not re.match(
            r"^.*(?=.*[0-9])(?=.*[!@#$%^&*])(?=.*[a-z])(?=.*[A-Z])[0-9a-zA-Z!@#$%^&*]{6,}.*$", key
        ):
            raise ValidationError("Invalid password: %s" % key)


class PermissionSchema(Schema):
    name = fields.Str(required=True)
    code = fields.Str(required=True)

    @validates("name")
    def validate_name(cls, key):
        if len(str(key)) < 128 and Permission.query.filter_by(name=key).count() == 0:
            raise ValidationError("Permission name value is invalid : %s" % key)

    @validates("code")
    def validate_code(cls, key):
        if len(str(key)) > 32 and Permission.query.filter_by(code=key).count() == 0:
            raise ValidationError("Permission code value is invalid : %s" % key)


class RoleSchema(Schema):
    name = fields.Str(required=True)
    permissions = fields.List(fields.Nested(PermissionSchema))

    @validates("name")
    def validate_name(cls, key):
        if len(str(key)) > 64 and Role.query.filter_by(name=key).count() == 0:
            raise ValidationError("Role name value is invalid : %s" % key)


class RegularContributionSchema(Schema):
    regular_contribution_uuid = fields.Str()
    amount = fields.Float()
    value_type = fields.Str()
    frequency = fields.Str()
    regular_contribution_type = fields.Str(required=True)

    @validates("value_type")
    def validate_value_type(cls, key):
        if key not in ["P", "F"]:
            raise ValidationError("value_type value is invalid : %s" % key)

    @validates("frequency")
    def validate_frequency(cls, key):
        if Frequency.query.filter_by(code=key).count() == 0:
            raise ValidationError("frequency value is invalid : %s" % key)

    @validates("regular_contribution_type")
    def validate_regular_contribution_type(cls, key):
        if RegularContributionType.query.filter_by(code=key).count() == 0:
            raise ValidationError("regular_contribution_type value is invalid : %s" % key)

    @validates("regular_contribution_uuid")
    def validate_regular_contribution_uuid(cls, key):
        if RegularContribution.query.filter_by(regular_contribution_uuid=key).count() == 0:
            raise ValidationError("regular_contribution_uuid value is invalid : %s" % key)


class JobSchema(Schema):
    profile_uuid = fields.Str()
    employment_type = fields.Str(required=True)
    company = fields.Str(required=True)
    is_company_director = fields.Boolean(allow_none=True)
    has_remuneration = fields.Boolean(allow_none=True)
    additional_info = fields.Str(allow_none=True)
    salary_additional = fields.Str(allow_none=True)

    @validates("profile_uuid")
    def validate_profile_uuid(cls, key):
        if not PlanUser.get_plan_user_by_profile_uuid(key):
            raise ValidationError("profile_uuid value is invalid : %s" % key)

    @validates("employment_type")
    def validate_employment_type(cls, key):
        if EmploymentType.query.filter_by(code=key).count() == 0:
            raise ValidationError("employment_type value is invalid : %s" % key)


class GivingSchema(Schema):
    giving_type = fields.Str(required=True)
    plan_cashflow_uuid = fields.Str()

    @validates("plan_cashflow_uuid")
    def validate_plan_cashflow_uuid(cls, key):
        if PlanCashflow.query.filter_by(plan_cashflow_uuid=key).count() == 0:
            raise ValidationError("plan_user_cashflow_uuid value is invalid : %s" % key)

    @validates("giving_type")
    def validate_giving_type(cls, key):
        if GivingType.query.filter_by(code=key).count() == 0:
            raise ValidationError("giving_type value is invalid : %s" % key)


class CashflowSchema(Schema):
    plan_cashflow_uuid = fields.Str()
    user_cashflow_type = fields.Str()
    profile_uuid = fields.Str()
    other_income_type = fields.Str()
    additional = fields.Str(allow_none=True)
    amount = fields.Float(required=True)
    frequency = fields.Str()
    value_type = fields.Str(required=True)
    duration = fields.Integer()
    is_indeterminate = fields.Boolean(allow_none=True)

    @validates("plan_cashflow_uuid")
    def validate_plan_cashflow_uuid(cls, key):
        if PlanCashflow.query.filter_by(plan_cashflow_uuid=key).count() == 0:
            raise ValidationError("plan_cashflow_uuid value is invalid : %s" % key)

    @validates("profile_uuid")
    def validate_profile_uuid(cls, key):
        if not PlanUser.get_plan_user_by_profile_uuid(key):
            raise ValidationError("profile_uuid value is invalid : %s" % key)

    @validates("user_cashflow_type")
    def validate_user_cashflow_type(cls, key):
        if UserCashflowType.query.filter_by(code=key).count() == 0:
            raise ValidationError("user_cashflow_type value is invalid : %s" % key)

    @validates("value_type")
    def validate_value_type(cls, key):
        if key not in ["P", "F"]:
            raise ValidationError("value_type value is invalid : %s" % key)

    @validates("frequency")
    def validate_frequency(cls, key):
        if Frequency.query.filter_by(code=key).count() == 0:
            raise ValidationError("frequency value is invalid : %s" % key)


class AssetSchema(Schema):
    plan_asset_uuid = fields.Str()
    asset_type = fields.Str(required=True)
    asset_type_other = fields.Str(allow_none=True)
    is_tenants_in_common = fields.Boolean(allow_none=True)
    additional = fields.Str(allow_none=True)
    provider_name = fields.Str(allow_none=True)
    current_value = fields.Float()
    time_spent_working = fields.Integer(allow_none=True)
    expected_retirement = fields.Date(allow_none=True)
    is_self_managed = fields.Boolean(allow_none=True)
    current_tax_year_contributions = fields.Float()
    cashflows = fields.List(fields.Nested(CashflowSchema))
    tax_benefit = fields.Str(allow_none=True)
    policy_number = fields.Str(allow_none=True)
    address_uuid = fields.Str(allow_none=True)
    regular_contribution = fields.List(fields.Nested(RegularContributionSchema))
    owners = fields.List(fields.String())
    is_not_sure_asset_type = fields.Boolean(allow_none=True)
    is_not_sure_employer_contribution = fields.Boolean(allow_none=True)
    is_not_sure_regular_contribution = fields.Boolean(allow_none=True)
    is_not_sure_receive_income = fields.Boolean(allow_none=True)
    is_indeterminate_period = fields.Boolean(allow_none=True)
    plan_documents_uuids = fields.List(fields.Str())

    @validates("asset_type")
    def validate_asset_type(cls, key):
        if AssetType.query.filter_by(code=key).count() == 0:
            raise ValidationError("asset_type value is invalid : %s" % key)

    @validates("plan_documents_uuids")
    def validate_plan_documents_uuids(cls, key):
        for k in key:
            if not PlanDocument.get_by_uuid(k):
                raise ValidationError("plan_document_uuid doesn't exist : %s" % key)

    @validates("tax_benefit")
    def validate_tax_benefit(cls, key):
        if TaxBenefit.query.filter_by(code=key).count() == 0:
            raise ValidationError("tax_benefit value is invalid : %s" % key)

    @validates("plan_asset_uuid")
    def validate_plan_asset_uuid(cls, key):
        if PlanAsset.query.filter_by(plan_asset_uuid=key).count() == 0:
            raise ValidationError("plan_asset_uuid value is invalid : %s" % key)

    @validates("address_uuid")
    def validate_address_uuid(cls, key):
        result = ProfileClient.get_address(key)
        if isinstance(result, dict) and result.get("message"):
            raise ValidationError("address_uuid value is invalid : %s" % key)


class DebtSchema(Schema):
    plan_debt_uuid = fields.Str()
    plan_debt_type = fields.Str(required=True)
    provider_name = fields.Str()
    outstanding_amount = fields.Float()
    interest_rate = fields.Float()
    rate_type = fields.Str()
    additional = fields.Str(allow_none=True)
    outstanding_term = fields.Integer()
    name = fields.Str()
    has_monthly_overpayment = fields.Boolean(allow_none=True)
    outstanding_term_months = fields.Integer()
    overpayment_amount = fields.Float()
    policy_number = fields.Str()
    cashflows = fields.List(fields.Nested(CashflowSchema))
    owners = fields.List(fields.String())
    address_uuid = fields.Str()

    @validates("address_uuid")
    def validate_address_uuid(cls, key):
        result = ProfileClient.get_address(key)
        if isinstance(result, dict) and result.get("message"):
            raise ValidationError("address_uuid value is invalid : %s" % key)

    @validates("plan_debt_type")
    def validate_plan_debt_type_id(cls, key):
        if PlanDebtType.query.filter_by(code=key).count() == 0:
            raise ValidationError("plan_debt_type value is invalid : %s" % key)

    @validates("rate_type")
    def validate_rate_type(cls, key):
        if RateType.query.filter_by(code=key).count() == 0:
            raise ValidationError("rate_type value is invalid : %s" % key)

    @validates("plan_debt_uuid")
    def validate_plan_debt_uuid(cls, key):
        if PlanDebt.query.filter_by(plan_debt_uuid=key).count() == 0:
            raise ValidationError("plan_debt_uuid value is invalid : %s" % key)


class UserSchema(Schema):
    first_name = fields.Str()
    last_name = fields.Str()

    email = fields.Email()
    dob = fields.Date()
    phone = fields.Integer()
    cashflow_changes = fields.Str()
    additional_info = fields.Str(allow_none=True)
    us_connections = fields.List(fields.Str())
    has_us_connections = fields.Boolean(allow_none=True)
    current_situation_type = fields.Str()
    retirement_age = fields.Integer(allow_none=True)
    approximate_age = fields.Str()
    plan_user_type = fields.Str()
    national_insurance_nbr = fields.Str(allow_none=True)
    plan_user_type_other = fields.Str(allow_none=True)
    plan_user_cashflow = fields.Nested(CashflowSchema)
    is_never_want_to_retire = fields.Boolean(allow_none=True)
    birth_country_cd = fields.Str()
    citizenships = fields.List(fields.Str())
    address_uuid = fields.Str()
    is_address_postal = fields.Boolean(allow_none=True)
    postal_address_uuid = fields.Str(allow_none=True)
    is_not_sure_other_income = fields.Boolean(allow_none=True)
    other_income = fields.Str(allow_none=True)

    @validates("postal_address_uuid")
    def validate_postal_address_uuid(cls, key):
        if key:
            result = ProfileClient.get_address(key)
            if isinstance(result, dict) and result.get("message"):
                raise ValidationError("postal_address_uuid value is invalid : %s" % key)

    @validates("birth_country_cd")
    def validate_birth_country_cd(cls, key):
        if key not in COUNTY_CD_LIST:
            raise ValidationError("Invalid birth_country_cd value: %s" % key)

    @validates("citizenships")
    def validate_citizenships(cls, key):
        for k in key:
            if k not in COUNTY_CD_LIST:
                raise ValidationError("Invalid citizenship value: %s" % k)

    @validates("approximate_age")
    def validate_approximate_age(cls, key):
        if ApproximateAge.query.filter_by(code=key).count() == 0:
            raise ValidationError("Invalid approximate_age value: %s" % key)

    @validates("current_situation_type")
    def validate_current_situation_type(cls, key):
        if CurrentSituationType.query.filter_by(code=key).count() == 0:
            raise ValidationError("Invalid current_situation_type value: %s" % key)

    @validates("plan_user_type")
    def validate_plan_user_type(cls, key):
        if PlanUserType.query.filter_by(code=key).count() == 0:
            raise ValidationError("Invalid plan_user_type value: %s" % key)

    @validates("email")
    def validate_email(cls, key):
        url = "{}/profiles/search/email/{}".format(options.profile_url, key)
        result = ProfileClient.authenticated_request(url, method="GET")
        current_email = ProfileClient.get_email(cls.context.get("profile_uuid"))
        if result:
            if not current_email and result.get("email"):
                raise ValidationError("User with this email already exists: %s" % key)
            if current_email != key and result.get("email"):
                raise ValidationError(
                    "You try to change email {} to existing value : {}".format(current_email, key)
                )

    @validates("us_connections")
    def validate_us_connections(cls, key):
        for k in key:
            if USConnection.query.filter_by(code=k).count() == 0:
                raise ValidationError("us_connections value is invalid : %s" % k)

    @validates("phone")
    def validate_phone(cls, key):
        if len(str(key)) > 16:
            raise ValidationError("Phone value is invalid : %s" % key)

    @validates("address_uuid")
    def validate_address_uuid(cls, key):
        result = ProfileClient.get_address(key)
        if isinstance(result, dict) and result.get("message"):
            raise ValidationError("address_uuid value is invalid : %s" % key)


class PlanSchema(Schema):
    family_has_children = fields.Boolean(allow_none=True)
    family_has_dependents = fields.Boolean(allow_none=True)
    family_has_nanny = fields.Boolean(allow_none=True)
    family_has_partner = fields.Boolean(allow_none=True)
    family_has_debts = fields.Boolean(allow_none=True)
    family_has_childcare = fields.Boolean(allow_none=True)
    plan_has_other_income = fields.Boolean(allow_none=True)
    partner_has_same_address = fields.Boolean(allow_none=True)
    plan_has_charity = fields.Boolean(allow_none=True)
    plan_has_gifting = fields.Boolean(allow_none=True)
    family_additional = fields.Str(allow_none=True)
    expenditure_change_age = fields.Integer()
    expenditure_additional = fields.Str(allow_none=True)
    has_insurance = fields.Boolean(allow_none=True)
    insurance_additional = fields.Str(allow_none=True)
    is_submitted = fields.Boolean(allow_none=True)
    marital_status = fields.Str()
    users = fields.List(fields.Nested(UserSchema))

    @validates("marital_status")
    def validate_marital_status(cls, key):
        if MaritalStatus.query.filter_by(code=key).count() == 0:
            raise ValidationError("marital_status value is invalid : %s" % key)


class SoftFactSchema(Schema):
    comment = fields.Str(required=True)
    tags = fields.List(fields.Str())

    @validates("tags")
    def validate_tags(cls, key):
        for k in key:
            if SoftFactTag.query.filter_by(code=k).count() == 0:
                raise ValidationError("tag value is invalid : %s" % k)
