from marshmallow import Schema, fields, post_dump

from libraries.profile import ProfileClient
from models.model import (
    Plan,
    PlanAssetOwner,
    PlanCashflow,
    PlanDebt,
    PlanDebtOwner,
    PlanGiving,
    PlanUser,
    PlanUserCashflow,
    PlanUserCashflowChange,
    PlanUserJob,
    RegularContribution,
    User,
    UserUSConnection,
    PlanSoftFact,
    PlanDocument,
    TermsAndConditionAccept,
)
from utils.plan_utils import reformat_plan_user_list
from validation.validation_schemes import (
    AssetSchema,
    CashflowSchema,
    DebtSchema,
    GivingSchema,
    JobSchema,
    PlanSchema,
    RegularContributionSchema,
    SoftFactSchema,
)


class TypeSchema(Schema):
    code = fields.Str(required=True)

    @post_dump()
    def get_code(self, data):
        return data["code"]


class PlanUserCashflowChangesSchema(Schema):
    description = fields.Str()

    class Meta:
        model = PlanUserCashflowChange


class UserSchema(Schema):
    profile = fields.Method("get_profile")

    def get_profile(self, obj):
        return ProfileClient.get(obj.profile_uuid).get("profile")

    class Meta:
        model = User


class UserUSConnectionSchema(Schema):
    us_connection = fields.Nested(TypeSchema, many=True, attribute="us_connection")
    plan_user = fields.Nested(UserSchema, many=True)

    class Meta:
        model = UserUSConnection


class PlanUserCashflowSchema(Schema):
    other_income_type = fields.Str()
    additional = fields.Str()
    plan_user_id = fields.Integer()

    class Meta:
        model = PlanUserCashflow


class PlanCashflowSchema(CashflowSchema):
    frequency = fields.Nested(TypeSchema, attribute="frequency")
    plan_user_cashflow = fields.Nested(PlanUserCashflowSchema, attribute="plan_user_cashflow")
    other_income_type = fields.Method("get_other_income_type")
    profile_uuid = fields.Method("get_profile_uuid")
    user_cashflow_type = fields.Nested(
        TypeSchema, attribute="plan_user_cashflow.user_cashflow_type"
    )
    additional = fields.Method("get_additional")

    def get_other_income_type(self, obj):
        return obj.plan_user_cashflow.other_income_type if obj.plan_user_cashflow else None

    def get_additional(self, obj):
        return obj.plan_user_cashflow.additional if obj.plan_user_cashflow else None

    def get_profile_uuid(self, obj):
        return User.get_plan_user_profile_uuid(
            obj.plan_user_cashflow.plan_user_id if obj.plan_user_cashflow else None
        )

    def get_user_cashflow_type(self, obj):
        return obj.plan_user_cashflow.user_cashflow_type if obj.plan_user_cashflow else None

    class Meta:
        model = PlanCashflow


class PlanUserJobSchema(JobSchema):
    plan_user_job_uuid = fields.Str()
    employment_type = fields.Nested(TypeSchema, attribute="employment_type")
    cashflows = fields.Nested(PlanCashflowSchema, many=True, attribute="plan_cashflow")

    class Meta:
        model = PlanUserJob


class PlanRegularContributionSchema(RegularContributionSchema):
    regular_contribution_type = fields.Nested(TypeSchema, attribute="regular_contribution_type")
    frequency = fields.Nested(TypeSchema, attribute="frequency")

    class Meta:
        model = RegularContribution


class PlanUserDetailsSchema(UserSchema):
    profile_uuid = fields.Method("get_profile_uuid")
    cashflow_changes = fields.Nested(
        PlanUserCashflowChangesSchema, only=("description"), attribute="cashflow_changes"
    )
    us_connections = fields.Nested(TypeSchema, many=True, attribute="us_connections")
    current_situation_type = fields.Nested(TypeSchema, attribute="current_situation_type")
    plan_user_type = fields.Nested(TypeSchema, attribute="plan_user_type")
    approximate_age = fields.Nested(TypeSchema, attribute="approximate_age")
    plan_user_cashflow = fields.Nested(PlanCashflowSchema, many=True)
    jobs = fields.Nested(PlanUserJobSchema, many=True)

    additional_info = fields.Str(allow_none=True)
    has_us_connections = fields.Boolean()
    retirement_age = fields.Integer()
    plan_user_type_other = fields.Str(allow_none=True)
    is_never_want_to_retire = fields.Boolean()
    address_uuid = fields.Str()
    is_address_postal = fields.Boolean()
    postal_address_uuid = fields.Str()
    is_not_sure_other_income = fields.Boolean(allow_none=True)
    other_income = fields.Str(allow_none=True)

    token = fields.Method("get_user_token")
    is_active = fields.Method("get_is_active")
    primary_address = fields.Method("get_address")
    postal_address = fields.Method("get_postal_address")
    terms_of_business_date = fields.Method("get_terms_of_business")

    def get_address(self, obj):
        address = ProfileClient.get_address(obj.address_uuid)
        return address["address"] if address.get("status") not in ("error", "err") else None

    def get_postal_address(self, obj):
        address = ProfileClient.get_address(obj.postal_address_uuid)
        return address["address"] if address.get("status") not in ("error", "err") else None

    def get_profile_uuid(self, obj):
        return User.get_plan_user_profile_uuid(obj.plan_user_id)

    def get_user_token(self, obj):
        return obj.profile.token

    def get_is_active(self, obj):
        return obj.profile.is_active

    def get_terms_of_business(self, obj):
        terms_and_conditions = (
            TermsAndConditionAccept.query.filter_by(who_accept_user_id=obj.profile.user_id)
            .order_by("create_dttm")
            .first()
        )
        return terms_and_conditions.create_dttm if terms_and_conditions else None

    class Meta:
        model = PlanUser


class PlanUserSchema(PlanUserDetailsSchema):
    profile = fields.Nested(UserSchema, only=("profile"), attribute="profile")


class PlanAssetSchema(AssetSchema):
    asset_type = fields.Nested(TypeSchema, attribute="asset_type")
    tax_benefit = fields.Nested(TypeSchema, attribute="tax_benefit")

    cashflows = fields.Nested(PlanCashflowSchema, many=True, attribute="plan_cashflow")
    regular_contributions = fields.Nested(PlanRegularContributionSchema, many=True)
    owners = fields.Method("get_owners")
    plan_documents_uuids = fields.Method("get_plan_documents_uuids")

    def get_owners(self, obj):
        """I need to join 4 tables to get profile_uuid and return them as a list of uuid, not key-value
            The easiest way i found is a new query"""
        return [
            User.get_plan_user_profile_uuid(owner.plan_user_id)
            for owner in PlanAssetOwner.query.filter_by(plan_asset_id=obj.plan_asset_id).all()
        ]

    def get_plan_documents_uuids(self, obj):
        return [
            doc.plan_document_uuid
            for doc in PlanDocument.query.filter_by(asset_id=obj.plan_asset_id)
        ]


class PlanGivingSchema(GivingSchema):
    plan_cashflow_uuid = fields.Nested(
        PlanCashflowSchema, only=("plan_cashflow_uuid"), attribute="plan_cashflow"
    )
    plan_giving_uuid = fields.Str()
    giving_type = fields.Nested(TypeSchema, attribute="giving_type")

    class Meta:
        model = PlanGiving


class PlanDebtSchema(DebtSchema):
    plan_debt_type = fields.Nested(TypeSchema, attribute="plan_debt_type")
    rate_type = fields.Nested(TypeSchema, attribute="rate_type")
    owners = fields.Method("get_owners")
    cashflows = fields.Nested(PlanCashflowSchema, many=True)

    def get_owners(self, obj):
        """I need to join 4 tables to get profile_uuid and return them as a list of uuid, not key-value
        The easiest way i found is a new query"""
        return [
            User.get_plan_user_profile_uuid(owner.plan_user_id)
            for owner in PlanDebtOwner.query.filter_by(plan_debt_id=obj.plan_debt_id).all()
        ]

    class Meta:
        model = PlanDebt


class FullPlanSchema(PlanSchema):
    """
    Dumps all the data from a plan.
    Used on Base.get_plan_data to feed URLS like /api/v1/users/me/active_plan
    """

    plan_uuid = fields.Str()
    marital_status = fields.Nested(TypeSchema, attribute="marital_status")

    users = fields.Nested(PlanUserSchema, many=True, attribute="plan_users")
    cashflows = fields.Nested(PlanCashflowSchema, many=True)
    assets = fields.Nested(PlanAssetSchema, many=True)
    givings = fields.Nested(PlanGivingSchema, many=True)
    debts = fields.Nested(PlanDebtSchema, many=True)

    @post_dump()
    def get_user_by_type(self, data):
        result = data.copy()
        result["users"] = reformat_plan_user_list(data["users"])
        return result

    class Meta:
        model = Plan


class PlanSoftFactSchema(SoftFactSchema):
    plan_soft_fact_uuid = fields.Str()
    plan_uuid = fields.Str()
    profile = fields.Method("get_owner")
    user_id = fields.Integer()
    comment = fields.Str()
    tags = fields.Nested(TypeSchema, attribute="soft_fact_tags", many=True)
    create_dttm = fields.DateTime()

    def get_owner(self, obj):
        return ProfileClient.get(obj.user.profile_uuid).get("profile")

    class Meta:
        model = PlanSoftFact


class PlanDocumentSchema(SoftFactSchema):
    plan_document_uuid = fields.Str()
    plan_asset_uuid = fields.Method("get_asset_uuid")
    plan_uuid = fields.Method("get_plan_uuid")
    type = fields.Str()
    url = fields.Str()
    size = fields.Integer()
    create_dttm = fields.DateTime()

    def get_asset_uuid(self, obj):
        return obj.plan_asset.plan_asset_uuid if obj.plan_asset else None

    def get_plan_uuid(self, obj):
        return obj.plan.plan_uuid if obj.plan else None

    class Meta:
        model = PlanDocument
