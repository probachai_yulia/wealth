import json
import threading

import tornado
from tornado.options import options
import wrapt

threadlocal = threading.local()


@wrapt.decorator
def add_modification_history(wrapped, instance, args, kwargs):
    # TODO: remove this import from here
    from models.model import ModificationHistory

    # User is refused
    user = getattr(threadlocal, "user", None)
    if not user:
        raise Exception("User not found")
    result = wrapped(*args, **kwargs)
    request_id = getattr(threadlocal, "request_id", None)

    ModificationHistory.add_modification_history(
        operation=str(wrapped.__name__),
        table_name=str(instance.__name__),
        request_id=request_id,
        data=json.dumps(kwargs),
        result=str(result),
        who_user_id=int(user["user"]["user_id"]),
    )
    return result


@wrapt.decorator
def fact_find_enabled(func, instance, args, kwargs):
    """
    Decorator to prevent fact find api endpoints from being
    accessible in production during development of epics.
    Without forcing us to keep code off of develop branch.
    """
    if options.fact_find_enabled is False:
        raise tornado.web.HTTPError(404)
    return func(*args, **kwargs)


@wrapt.decorator
def develop_tool(func, instance, args, kwargs):
    """
    Decorator to prevent fact find api endpoints from being
    accessible in production during development of epics.
    Without forcing us to keep code off of develop branch.
    """
    if not options.dev_user_command_enabled:
        raise tornado.web.HTTPError(404)
    return func(*args, **kwargs)
