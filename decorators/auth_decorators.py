import functools
import http.client

import jwt
from tornado.escape import url_escape

from decorators.decorators import threadlocal
from libraries import utils
from oauthclient import AuthError, OAuth2Mixin


def _generate_token(self):
    refresh_token = self.get_secure_cookie("refresh_token")
    if not refresh_token:
        return self.redirect(OAuth2Mixin.login_url(self.callback_url, self.request.uri))
    try:
        OAuth2Mixin.validate_refresh_token(refresh_token)
    except AuthError as e:
        self.clear_cookie("refresh_token")
        return self.redirect(OAuth2Mixin.login_url(self.callback_url, self.request.uri))
    try:
        tokens = OAuth2Mixin.refresh_access_token(refresh_token)
    except AuthError as e:
        return self.redirect(
            "/auth_callback?error=" + url_escape(e.error if hasattr(e, "error") else e.message)
        )
    except Exception as e:
        msg = e.message
        if "cloudflare" in msg:
            msg = "network error (cloudflare)"
        return self.redirect("/auth_callback?error=" + url_escape(msg))
    OAuth2Mixin.set_cookies(self, tokens)
    token = tokens["access_token"]
    return token


def decode_token(self, token):
    if token:
        try:
            OAuth2Mixin.validate_access_token(token)
        except AuthError as e:
            self.clear_cookie("access_token")
            token = None
    if not token:
        token = _generate_token(self)
    if token:
        try:
            profile, user = OAuth2Mixin._decode_token(token)
            return profile, user
        except jwt.exceptions.DecodeError:
            return None, None


def oauth_authenticated(method):
    @functools.wraps(method)
    def wrapper(self, *args, **kwargs):
        access_token = self.get_secure_cookie("access_token")
        profile, user = decode_token(self, access_token)
        self.current_user = profile

        if self.current_user is not None:
            self.tmpl["current_user"] = self.current_user
            threadlocal.user = self.get_current_user()
            return method(self, *args, **kwargs)

    return wrapper


def _check_permissions(user_roles, roles, include_all=False):
    """ Check given role is inside or equals to roles """

    # Roles is a list not a single element
    if isinstance(roles, list):
        roles_diff = [i for i in roles if i not in user_roles]
        if include_all and any((True for x in user_roles if x in roles)):
            return True
        elif not roles_diff:
            return True

    # Roles is a single string
    else:
        if roles in user_roles:
            return True

    return False


def role_required(roles=None, include_all=False):
    def decorator(func):
        def decorated(self, *args, **kwargs):
            user = self.get_current_user()
            # User is refused
            if not user:
                self.json_response(
                    {
                        "status": "ERR",
                        "message": "You are not authenticated in the app. Please sign in or sign up.",
                    },
                    http.client.FORBIDDEN,
                )

            user_role = self.get_user_roles(utils.get_user_id(user))

            if not _check_permissions([user_role], roles, include_all):
                self.json_response(
                    {
                        "status": "ERR",
                        "message": "You don't have permissions to view this page. You need to log in as {}".format(
                            roles
                        ),
                    },
                    http.client.FORBIDDEN,
                )
                self.finish()
                return None

            return func(self, *args, **kwargs)

        return decorated

    return decorator


def role_forbidden(roles=None):
    def decorator(func):
        def decorated(self, *args, **kwargs):
            user = self.get_current_user()

            # User is refused
            if not user:
                self.json_response(
                    {
                        "status": "ERR",
                        "message": "You are not authenticated in the app. Please sign in or sign up.",
                    },
                    http.client.FORBIDDEN,
                )

            user_role = self.get_user_roles(utils.get_user_id(user))

            if _check_permissions([user_role], roles):
                self.json_response(
                    {
                        "status": "ERR",
                        "message": "You don't have permissions to view this page. This role is refused {}".format(
                            roles
                        ),
                    },
                    http.client.FORBIDDEN,
                )
                self.finish()
                return None

            return func(self, *args, **kwargs)

        return decorated

    return decorator


def permission_required(permissions=None):
    def decorator(func):
        def decorated(self, *args, **kwargs):
            user = self.get_current_user()
            # User is refused
            if not user:
                self.json_response(
                    {
                        "status": "ERR",
                        "message": "You are not authenticated in the app. Please sign in or sign up.",
                    },
                    http.client.FORBIDDEN,
                )

            user_permissions = self.get_permissions_by_user_id(utils.get_user_id(user))

            if not _check_permissions(user_permissions, permissions):
                self.json_response(
                    {
                        "status": "ERR",
                        "message": "You don't have permissions to view this page. You need to have these "
                        "permissions {}".format(permissions),
                    },
                    http.client.FORBIDDEN,
                )
                self.finish()
                return None

            return func(self, *args, **kwargs)

        return decorated

    return decorator


def permission_forbidden(permissions=None):
    def decorator(func):
        def decorated(self, *args, **kwargs):
            user = self.get_current_user()

            # User is refused
            if not user:
                self.json_response(
                    {
                        "status": "ERR",
                        "message": "You are not authenticated in the app. Please sign in or sign up.",
                    },
                    http.client.FORBIDDEN,
                )

            user_permissions = self.get_permissions_by_user_id(utils.get_user_id(user))

            if _check_permissions(user_permissions, permissions):
                self.json_response(
                    {
                        "status": "ERR",
                        "message": "You don't have permissions to view this page. These permissions are refused "
                        "{}".format(permissions),
                    },
                    http.client.FORBIDDEN,
                )
                self.finish()
                return None

            return func(self, *args, **kwargs)

        return decorated

    return decorator
