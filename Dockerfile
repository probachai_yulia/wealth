FROM python:3.6.5-slim

MAINTAINER danielkarpinski

ENV INSTALL_PATH /app
RUN mkdir -p $INSTALL_PATH

RUN apt-get update && apt-get install -y gcc git libmariadbclient-dev wget

ENV DOCKERIZE_VERSION v0.6.1
RUN wget https://github.com/jwilder/dockerize/releases/download/$DOCKERIZE_VERSION/dockerize-alpine-linux-amd64-$DOCKERIZE_VERSION.tar.gz \
    && tar -C /usr/local/bin -xzvf dockerize-alpine-linux-amd64-$DOCKERIZE_VERSION.tar.gz \
    && rm dockerize-alpine-linux-amd64-$DOCKERIZE_VERSION.tar.gz

COPY requirements.txt requirements.txt
RUN pip install --upgrade pip
RUN pip install -r requirements.txt

ARG env_requirements='-r requirements.txt'
COPY env_requirements.txt env_requirements.txt
RUN pip install $env_requirements

WORKDIR $INSTALL_PATH
ADD . .

ENV WEALTH_ENV local

EXPOSE 8080
CMD python main.py

