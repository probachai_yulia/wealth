import decimal
import itertools
import json
import logging
import re
import urllib.error
import urllib.parse
import urllib.request
from datetime import datetime, date, time
from functools import wraps
from http.client import HTTPException, UNAUTHORIZED, FORBIDDEN, NOT_FOUND
from socket import error

import cache
import dateutil.parser
import tornado.escape
import tornado.web
from pytz import utc


class JSONEncoder(json.JSONEncoder):
    DATE_FORMAT = "%Y-%m-%d"
    TIME_FORMAT = "%H:%M:%S"

    def default(self, o):
        if hasattr(o, "__json__"):
            return o.__json__()
        if isinstance(o, datetime):
            return o.isoformat()
        elif isinstance(o, date):
            return o.strftime(JSONEncoder.DATE_FORMAT)
        elif isinstance(o, time):
            return o.strftime(JSONEncoder.TIME_FORMAT)
        elif isinstance(o, decimal.Decimal) or repr(type(o)) == repr(decimal.Decimal):
            # the second part added as a work around for google app engine sdk bug
            # https://groups.google.com/forum/#!topic/google-appengine-stackoverflow/7-tRlyt31Ic
            return float(o)
        else:
            return super(JSONEncoder, self).default(o)

    def encode(self, o):
        return super(JSONEncoder, self).encode(o)


class JSON(object):
    @staticmethod
    def custom_decoder(o):
        if not isinstance(o, dict):
            return o
        new = {}
        for key, val in o.items():
            if key.endswith("_dt") or key.endswith("_dttm"):
                try:
                    val = dateutil.parser.parse(val)
                except BaseException:
                    pass
            new[key] = val
        return new

    @staticmethod
    def encode(obj, **kwargs):
        kwargs.setdefault("cls", JSONEncoder)
        kwargs.setdefault("indent", None)
        kwargs.setdefault("separators", (",", ":"))
        return json.dumps(obj, **kwargs)

    @staticmethod
    def decode(text, **kwargs):
        try:
            kwargs.setdefault("parse_float", decimal.Decimal)
            kwargs.setdefault("object_hook", JSON.custom_decoder)
            obj = json.loads(text, **kwargs)
        except Exception as e:
            logging.warning("Failed to decode text as JSON: '%s'", text)
            raise e
        else:
            return obj


class ErrorEmailSuppression(object):
    def __init__(self, key, timeout_sec, limit, *cache_args):
        self.key = key
        self.timeout = timeout_sec
        self.limit = limit
        if cache_args:
            self.key = self.key % tuple(cache_args)

    def incr(self,):
        c = cache.get_cache()
        data = c.get(self.key)
        if not data:
            c.set(self.key, 1, self.timeout, min_compress_len=1000)
            return 1 > self.limit, 1
        else:
            c.incr(self.key)
            return data > self.limit, data

    @staticmethod
    def get_counter(key):
        c = cache.get_cache()
        data = c.get(key)
        return data or 0


def inject_tzinfo(arg, tz=None):
    """ Helper function which injects a timezone to a naive datetime.

        Args:
            arg: Any value; if it's a datetime, it gets injected timezoneinfo.
                A date gets converted to datetime first, while any other type is ignored.
            tz (tzinfo): Timezone info object to be injected. If absent, UTC is used.

        Returns:
            The value of arg, optionally with timezone info injected.
    """
    if not isinstance(arg, date):
        return arg
    if not isinstance(arg, datetime):
        arg = datetime.combine(arg, datetime.min.time())
    if not arg.tzinfo:
        arg = utc.localize(arg)
    if tz is None:
        tz = arg.tzinfo
    if tz != arg.tzinfo:
        arg = arg.astimezone(tz)
    return arg


def with_tz(fn):
    """ Decorator which applies timezone to all of a function's arguments that are date or datetime.

        If the argument has no timezone information, the wrapper function checks if there is an argument
        named ``tz_``; if so, its value is applied as the ``tzinfo``, or UTC is assumed as the default.
    """

    @wraps(fn)
    def wrapper(*args, **kwargs):
        tz = kwargs.pop("tz_", None)
        args = [inject_tzinfo(arg, tz) for arg in args]
        kwargs = {key: inject_tzinfo(arg, tz) for key, arg in kwargs}
        return fn(*args, **kwargs)

    return wrapper


def get_user_id(user):
    try:
        return user["user"].user_id
    except Exception:
        return None


def _do_request(url, http, json_response, response_errors=[], timeout=300):
    """
    Does the request, handles the possible exceptions and
    returns the data.
    """
    try:
        response = urllib.request.urlopen(http, timeout=timeout)
    except urllib.error.HTTPError as e:  # if HTTP status is not 200
        response = e.read()
        if e.code in response_errors:
            return {"response": response, "error": e}
        elif e.code in [UNAUTHORIZED, FORBIDDEN]:
            raise ValueError("You are not authorised to access this page %s" % response)
        elif e.code == NOT_FOUND:
            raise ValueError("Resource not found: %s" % url)
        else:
            raise ValueError("Url %s returned an invalid response: %s" % (response, url))
    except error:
        hostname = urllib.parse.urlparse(url)
        raise ValueError("Cannot access %s" % hostname.netloc)
    except (HTTPException, error) as e:
        raise ValueError("Invalid response: %s" % e.message)
    data = response.read()
    if data and json_response:
        try:
            data = json.loads(data)
        except ValueError:
            raise ValueError("Invalid response: %s" % data)
    return data


def make_request(
    url,
    data=None,
    headers={},
    method=None,
    json_type=True,
    json_response=True,
    response_errors=[],
    timeout=300,
):
    if data:
        if json_type:
            data = JSON.encode(data)
            headers["Content-Type"] = "application/json"
        else:
            if isinstance(data, dict):
                for k, v in data.items():
                    if isinstance(v, str):
                        data[k] = v.encode("utf-8") if isinstance(v, str) else v
            data = urllib.parse.urlencode(data)

        if isinstance(data, str):
            data = data.encode("utf-8")

    http = urllib.request.Request(url, data, headers)
    if method:
        http.get_method = lambda: method

    return _do_request(url, http, json_response, response_errors, timeout=timeout)


def filter(result, filter):
    return [item for item in filter if item in result]


class FlashMessageMixin(object):
    """ Mixing for handlers to add the functionality of passing messages between pages.
    """

    FLASH_COOKIE_NAME = "flash_messages"

    def __getattr__(self, name):
        pattern = r"flash_(success|info|warning|danger)"
        message_type = re.search(pattern, name)
        if message_type:
            return lambda message: self.write_flash_message(
                {"type": message_type.group(1), "text": message}
            )
        raise AttributeError("'{}' object has no attribute '{}'".format(type(self).__name__, name))

    def write_flash_message(self, message):
        """ Sends a flash message by storing it into a cookie with a key.
        """
        existing_messages = self.read_flash_messages()
        existing_messages.append(message)
        self.set_secure_cookie(
            self.FLASH_COOKIE_NAME, tornado.escape.json_encode(existing_messages)
        )

    def read_flash_messages(self, grouped=False):
        """ Reads all flash messages and clears the cookie.
        """
        messages = self.get_secure_cookie(self.FLASH_COOKIE_NAME)
        self.clear_cookie(self.FLASH_COOKIE_NAME)
        if messages:
            messages = tornado.escape.json_decode(messages)
            if grouped:

                def key(x):
                    return x["type"]

                messages = dict(itertools.groupby(sorted(messages, key=key), key=key))
                for k, v in list(messages.items()):
                    messages[k] = list(v)
            return messages
        return []
