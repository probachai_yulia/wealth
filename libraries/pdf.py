from tornado.options import options
from oauthclient import OAuth2BackendClient


class PdfClient:
    @staticmethod
    def render(html, filename):
        data = {"document": html, "converter": options.pdf_service_converter, "pdf_name": filename}
        return OAuth2BackendClient.authenticated_request(
            options.pdf_service_url, data=data, method="POST", json_response=False
        )
