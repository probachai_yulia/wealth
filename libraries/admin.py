from models.model import Role, Permission, RolePermission, UserPermission


class AdminClient:
    @staticmethod
    def create_or_update_role(data, role_name=None):
        role = Role.get_role_by_name(name=role_name)
        if role:
            role = Role.update(role["role_id"], **data)
        else:
            role = Role.create(**data)

        return role

    @staticmethod
    def create_or_update_permission(data, code=None, role_id=None, user_id=None):
        permission = Permission.get_permission_by_code(code=code)
        if permission:
            permission = Permission.update(permission["permission_id"], **data)
        else:
            permission = Permission.create(**data)
        if role_id:
            AdminClient.create_or_update_role_permission(role_id, permission["permission_id"])
        if user_id:
            AdminClient.create_or_update_user_permission(user_id, permission["permission_id"])
        return permission

    @staticmethod
    def create_or_update_role_permission(role_id, permission_id):
        role_permission = RolePermission.query.filter_by(
            role_id=role_id, permission_id=permission_id
        )
        if not role_permission:
            role_permission = RolePermission.create(role_id=role_id, permission_id=permission_id)
        return role_permission[0]["role_permission_id"]

    @staticmethod
    def create_or_update_user_permission(user_id, permission_id):
        user_permission = UserPermission.query.filter_by(
            user_id=user_id, permission_id=permission_id
        )
        if not user_permission:
            user_permission = UserPermission.create(user_id=user_id, permission_id=permission_id)
        return user_permission[0]["user_permission_id"]
