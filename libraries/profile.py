from tornado.options import options
from oauthclient import OAuth2BackendClient


class ProfileClient:
    CUD_METHODS = "POST PUT PATCH DELETE"

    @classmethod
    def add_profile_operation(cls, url, method, data, operation, success=True):
        # TODO: importing here to avoid circular imports
        from models.model import ProfileOperationHistory

        if method in cls.CUD_METHODS.split():
            profile_operation_history_id = ProfileOperationHistory.add_profile_operation_history(
                operation=operation, url=url, data=data, success=success
            )
            return profile_operation_history_id
        return None

    @classmethod
    def authenticated_request(
        cls,
        url,
        data=None,
        method=None,
        json_response=True,
        operation=None,
        response_errors=[],
        timeout=60,
    ):
        result = OAuth2BackendClient.authenticated_request(
            url,
            data=data,
            method=method,
            json_response=json_response,
            response_errors=response_errors,
            timeout=timeout,
        )
        cls.add_profile_operation(url=url, data=data, operation=operation, method=method)
        return result

    @classmethod
    def create(cls, data):
        url = "{}/profiles".format(options.profile_url)
        return cls.authenticated_request(url, data, method="POST", operation="Create new profile")

    @classmethod
    def get(cls, profile_uuid):
        url = (
            "{}/profiles/{}?fields=profile_id,email,first_name,"
            "last_name,national_insurance_nbr,user_id,display_name,dob,"
            "birth_country_cd,citizen_state_cd,citizenships,phone".format(
                options.profile_url, profile_uuid
            )
        )
        return cls.authenticated_request(url, method="GET", operation="Get user profile")

    @classmethod
    def get_email(cls, profile_uuid):
        url = "{}/profiles/{}".format(options.profile_url, profile_uuid)
        result = cls.authenticated_request(url, method="GET", operation="Get user profile")
        try:
            return result["profile"]["email"]
        except KeyError:
            return None

    @classmethod
    def add_email(cls, profile_uuid, email):
        url = "{}/profiles/{}/emails".format(options.profile_url, profile_uuid)
        data = {"email": email}
        result = cls.authenticated_request(
            url, data=data, method="POST", operation="Add profile email"
        )
        try:
            return result["email"]["email_id"]
        except KeyError:
            return None

    @classmethod
    def make_email_primary(cls, profile_uuid, email_id):
        url = "{}/profiles/{}/emails/{}/primary".format(options.profile_url, profile_uuid, email_id)
        result = cls.authenticated_request(url, method="GET", operation="Make email primary")
        return result

    @classmethod
    def search(cls, data):
        url = "{}/profiles/search".format(options.profile_url)
        return cls.authenticated_request(
            url, data=data, method="POST", operation="Search users profiles"
        )

    @classmethod
    def set_access(cls, data, profile_id):
        url = "{}/profiles/{}/access".format(options.profile_url, profile_id)
        return cls.authenticated_request(
            url, data, method="PUT", operation="Set access to user profile"
        )

    @classmethod
    def set_relation(cls, data, profile_id):
        url = "{}/profiles/{}/relationships".format(options.profile_url, profile_id)
        return cls.authenticated_request(
            url, method="PUT", data=data, operation="Set relation to users profiles"
        )

    @classmethod
    def get_address(cls, address_id):
        url = "{}/addresses/{}".format(options.profile_url, address_id)
        return cls.authenticated_request(url, method="GET", operation="Get address")

    @classmethod
    def delete_address(cls, address_id):
        url = "{}/addresses/{}".format(options.profile_url, address_id)
        return cls.authenticated_request(url, method="DELETE", operation="Delete address")

    @classmethod
    def update(cls, profile_uuid, data):
        url = "{}/profiles/{}".format(options.profile_url, profile_uuid)
        return cls.authenticated_request(url, data, method="PATCH", operation="Update user profile")

    @classmethod
    def get_profiles_list(cls):
        url = "{}/company/{}".format(options.profile_url, options.company_profile_id)
        return cls.authenticated_request(url, method="GET", operation="Get profiles list")

    @classmethod
    def delete(cls, profile_uuid):
        url = "{}/profiles/{}".format(options.profile_url, profile_uuid)
        return cls.authenticated_request(url, method="DELETE", operation="Delete user profile")
