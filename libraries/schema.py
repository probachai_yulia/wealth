import io
import logging
from typing import Dict, Tuple, Sequence
from contextlib import contextmanager

import alembic.command
import alembic.config

ALEMBIC_INI = "alembic.ini"
commands = alembic.command


def migrate(revision: str, sql_mode: bool) -> Tuple:
    """
    Perform or produce SQL for an alembic migration

    :param revision: migration revision ('head' for latest)
    :param sql_mode: print sql lines
    :return: logging lines, sql lines (if sql_mode)
    """
    return _run_command(commands.upgrade, revision=revision, sql=sql_mode)


def rollback(revision: str, sql_mode: bool) -> Tuple:
    """
    Perform or produce SQL for an alembic rollback

    :param revision: migration revision ('head' for latest)
    :param sql_mode: print sql lines
    :return: logging lines, sql lines (if sql_mode)
    """
    return _run_command(commands.downgrade, revision=revision, sql=sql_mode)


def history() -> Sequence[Dict]:
    """
    Get list of performed migrations from database

    :return: logging lines, sql lines (None)
    """
    lines, _ = _run_command(commands.history, verbose=True, indicate_current=True)

    history_items = []
    for i, line in enumerate(lines):
        if "Rev: " in line:
            first = i + 4
            second = first + 5
            history_items.append(_get_history_item("head" in line, lines[first:second]))

    return history_items


def _get_history_item(is_head, lines):
    return dict(
        is_head=is_head,
        name=lines[0].strip(),
        revision=lines[2].strip()[-12:],
        previous=lines[3].strip()[-12:],
        date=lines[4].strip(),
    )


def _run_command(command, **kwargs):
    """

    :param command: alembic.commands accepting revision and sql_mode
    :param kwargs: command parameters
    :return: logging lines, sql lines (if sql_mode)
    """
    with alembic_capture() as logs:
        with io.StringIO() as output_buffer:
            command(config=_get_alembic_config(output_buffer=output_buffer, stdout=logs), **kwargs)

            sql = output_buffer.getvalue().split("\n")
            log = logs.getvalue().split("\n")
    return log, sql


def _get_alembic_config(output_buffer: io.StringIO, stdout: io.StringIO) -> alembic.config.Config:
    return alembic.config.Config(file_=ALEMBIC_INI, stdout=stdout, output_buffer=output_buffer)


@contextmanager
def alembic_capture():
    """Temporarily buffers a new logger so we can capture log output from alembic and return it"""
    with io.StringIO() as io_string:
        handler = logging.StreamHandler(io_string)
        logger = logging.getLogger()

        try:
            logger.addHandler(handler)
            yield io_string
        finally:
            logger.removeHandler(handler)
