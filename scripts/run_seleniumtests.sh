#!/usr/bin/env bash

if [ ! "$BASH_VERSION" ] ; then
    exec /bin/bash "$0" "$@"
fi
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null && pwd )"

target_folder="$DIR/../tests/selenium/"
target=${target_folder}
if [ $1 ]; then
    if [[ $1 == "tests/"* ]]; then
        target_folder="";
    fi

    target="${target_folder}${1}"

    if [ $2 ]; then
        target="${target_folder}${1}::${2}"

        if [ $3 ]; then
            target="${target_folder}${1}::${2}::${3}"
        fi
    fi

fi

source "$DIR/selenium.config"

# checks if the port is busy, kills the process if so
# and starts a new server
if lsof -Pi :8080 -sTCP:LISTEN -t >/dev/null ; then
    echo "Killing the process running on port 8080"
    kill $(lsof -Pi :8080 -sTCP:LISTEN -t)
fi


# (source "$DIR/selenium.config" && python main.py; alembic upgrade head;) &
PYTHONPATH=`pwd`/lib:`pwd`:$PYTHONPATH python -m pytest -s -vv -ra -m "selenium" --junitxml=./unit.xml --cov . --cov-report xml $target


# -s -vv -ra  --junitxml=./unit.xml --cov . --cov-report xml $target
