#!/usr/bin/env bash

if [ ! "$BASH_VERSION" ] ; then
    exec /bin/bash "$0" "$@"
fi
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null && pwd )"

target_folder="$DIR/../tests/"
target=${target_folder}
if [ $1 ]; then
    if [[ $1 == "tests/"* ]]; then
        target_folder="";
    fi

    target="${target_folder}${1}"

    if [ $2 ]; then
        target="${target_folder}${1}::${2}"

        if [ $3 ]; then
            target="${target_folder}${1}::${2}::${3}"
        fi
    fi

fi

export WEALTH_ENV=test
export FACT_FIND_ENABLED=True

PYTHONPATH=`pwd`/lib:`pwd`:$PYTHONPATH python -m pytest -s -vv -ra -m "unit" --junitxml=./unit.xml --cov . --cov-report xml $target

# -s -vv -ra  --junitxml=./unit.xml --cov . --cov-report xml $target
