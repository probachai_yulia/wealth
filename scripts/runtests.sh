#!/usr/bin/env bash

if [ ! "$BASH_VERSION" ] ; then
    exec /bin/bash "$0" "$@"
fi

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null && pwd )"

sh $DIR/run_unittests.sh
sh $DIR/run_functests.sh
sh $DIR/run_apitests.sh
