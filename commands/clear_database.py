import os
import sys

parent_dir = os.path.abspath(os.path.join(os.getcwd(), ""))
sys.path.append(parent_dir)
from helpers import truncate_all_tables  # noqa

truncate_all_tables()
