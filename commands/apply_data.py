import os

import sys

import main

dbsession = main.Session()
parent_dir = os.path.abspath(os.path.join(os.getcwd(), ""))
sys.path.append(parent_dir)
from commands.helpers import upgrade  # noqa

upgrade(dbsession)
