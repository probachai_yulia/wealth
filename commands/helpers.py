import logging
import os
import sys

from sqlalchemy import MetaData, Integer, String, column, table as tb

from models.session import Session
from utils.plan_utils import create_or_get_user, create_plan

parent_dir = os.path.abspath(os.path.join(os.getcwd(), ""))
sys.path.append(parent_dir)
from libraries.profile import ProfileClient  # noqa
from tornado.options import options  # noqa
from models.model import Role, UserRole  # noqa


# run it with python create_dev_user.py --dev_email=someuser@example.com --dev_user_type=adviser


def create_dev_user(
    email=options.dev_email, user_type=options.dev_user_type, silent=options.dev_silent
):
    result = "Please provide email"
    if email:
        name = email.split("@")[0]
        data = {
            "client_id": options.oauth_client_id,
            "email": email,
            "password": options.dev_user_password,
            "first_name": name,
            "last_name": "WealthDev",
        }
        result = ProfileClient.create(data)
        profile = result.get("profile")
        if profile:
            user_id = create_or_get_user(profile["profile_id"], {"is_active": 1})
            data = {
                "relation_type_id": options.firm_relation_type_id,
                "profile_id": options.company_profile_id,
            }
            ProfileClient.set_access({"user_id": user_id}, profile["profile_id"])
            ProfileClient.set_relation(data, profile["profile_id"])
            result["user_id"] = user_id
            result["role"] = create_user_role(user_type, user_id)

            if options.dev_user_type == "client":
                result["plan"] = create_plan(profile["profile_id"], user_id)

    if not silent:
        logging.warning(result)
    return result


def create_user_role(user_type, user_id, silent=options.dev_silent):
    role = Role.query.filter_by(name=user_type).first()
    if role:
        user_role = UserRole.query.filter_by(user_id=user_id, role_id=role.role_id).first()
        if not user_role:
            UserRole.create(role_id=role.role_id, user_id=user_id)
            if not silent:
                logging.warning("User role {} was created".format(user_type))
            return user_type
    elif not silent:
        logging.warning("Role {} doesn't exist".format(user_type))


local_tables = [
    "plan",
    "plan_asset",
    "plan_asset_owner",
    "plan_cashflow",
    "plan_debt",
    "plan_debt_owner",
    "plan_document",
    "plan_giving",
    "plan_soft_fact",
    "plan_user",
    "plan_user_cashflow",
    "plan_user_cashflow_change",
    "plan_user_job",
    "regular_contribution",
    "soft_fact_tag_joint",
    "terms_and_condition_accept",
    "user",
    "user_role",
    "user_us_connection",
]


def truncate_all_tables():
    # delete all table data (but keep tables)
    engine = Session().engine
    meta = MetaData(bind=engine, reflect=True)
    con = engine.connect()
    engine.execute("SET FOREIGN_KEY_CHECKS=0;")
    trans = con.begin()
    for table in reversed(meta.sorted_tables):
        if table.name in local_tables:
            logging.info("Truncating table {}".format(table.name))
            con.execute(table.delete())
    trans.commit()
    engine.execute("SET FOREIGN_KEY_CHECKS=1;")


def upgrade(session):
    us_connection = tb(
        "us_connection",
        column("us_connection_id", Integer),
        column("code", String),
        column("name", String),
    )
    session.execute(
        us_connection.insert(),
        [
            {"us_connection_id": 1, "code": "us_citizen", "name": "US citizen"},
            {"us_connection_id": 2, "code": "green_card", "name": "Green card holder"},
            {"us_connection_id": 3, "code": "pay_taxes", "name": "Pay taxes in the US"},
            {"us_connection_id": 4, "code": "us_bank_account", "name": "US bank account"},
            {"us_connection_id": 5, "code": "us_address", "name": "US address"},
        ],
    )
    marital_status = tb(
        "marital_status",
        column("marital_status_id", Integer),
        column("code", String),
        column("name", String),
    )
    session.execute(
        marital_status.insert(),
        [
            {"marital_status_id": 1, "code": "single", "name": "Single"},
            {"marital_status_id": 2, "code": "married", "name": "Married"},
            {"marital_status_id": 3, "code": "partner", "name": "Partner"},
            {"marital_status_id": 4, "code": "widowed", "name": "Widowed"},
            {"marital_status_id": 5, "code": "divorced", "name": "Divorced"},
        ],
    )
    rate_type = tb(
        "rate_type", column("rate_type_id", Integer), column("code", String), column("name", String)
    )
    session.execute(
        rate_type.insert(),
        [
            {"rate_type_id": 1, "code": "fixed", "name": "Fixed rate"},
            {"rate_type_id": 2, "code": "variable", "name": "Variable rate"},
            {"rate_type_id": 3, "code": "dont_know", "name": "Don't know"},
        ],
    )
    tax_benefit = tb(
        "tax_benefit",
        column("tax_benefit_id", Integer),
        column("code", String),
        column("description", String),
        column("name", String),
    )
    session.execute(
        tax_benefit.insert(),
        [
            {
                "tax_benefit_id": 1,
                "code": "enterprise",
                "name": "Enterprise Investment Scheme",
                "description": "Early stage company investments attracting special tax relief.",
            },
            {
                "tax_benefit_id": 2,
                "code": "seed",
                "name": "Seed Enterprise Investment Scheme",
                "description": "Very early stage company investments attracting special tax relief.",
            },
            {
                "tax_benefit_id": 3,
                "code": "venture",
                "name": "Venture Capital Trust",
                "description": "Smaller company investments attracting special tax relief.",
            },
            {"tax_benefit_id": 4, "code": "unknown", "name": "Unknown", "description": None},
        ],
    )
    asset_type = tb(
        "asset_type",
        column("asset_type_id", Integer),
        column("code", String),
        column("name", String),
        column("description", String),
        column("asset_parent_type_id", Integer),
    )
    session.execute(
        asset_type.insert(),
        [
            {
                "asset_type_id": 1,
                "code": "pension",
                "name": "Pensions",
                "asset_parent_type_id": None,
                "description": "Pensions, final salary schemes, and annuities.",
            },
            {
                "asset_type_id": 2,
                "code": "benefit",
                "name": "Defined benefit",
                "asset_parent_type_id": 1,
                "description": "A scheme whose value is linked to the number of years you were employed or a member.",
            },
            {
                "asset_type_id": 3,
                "code": "contribution",
                "name": "Defined contribution",
                "asset_parent_type_id": 1,
                "description": "Any scheme to which you or your employer make regular contributions.",
            },
            {
                "asset_type_id": 4,
                "code": "annuity",
                "name": "Annuity",
                "asset_parent_type_id": 1,
                "description": "A product paying a fixed amount of income each year in your retirement.",
            },
            {
                "asset_type_id": 5,
                "code": "isa",
                "name": "ISA",
                "asset_parent_type_id": None,
                "description": "Cash ISAs, Stocks & Shares ISAs and more.",
            },
            {
                "asset_type_id": 6,
                "code": "isa_cash",
                "name": "Cash ISAs",
                "asset_parent_type_id": 5,
                "description": "Select this for cash held in an individual savings account, whether it accrues "
                "interest or not.",
            },
            {
                "asset_type_id": 7,
                "code": "isa_stocks_shares",
                "name": "Stocks & Shares ISA",
                "asset_parent_type_id": 5,
                "description": "Select this for shares held in an individual savings account.",
            },
            {
                "asset_type_id": 8,
                "code": "isa_junior",
                "name": "Junior ISA",
                "asset_parent_type_id": 5,
                "description": "Select this if you are saving for a child under the age of 18 in either cash or "
                "shares.",
            },
            {
                "asset_type_id": 9,
                "code": "isa_innovative",
                "name": "Innovative finance ISA",
                "asset_parent_type_id": 5,
                "description": "Select this if you hold peer-to-peer products in an individual savings "
                "account.",
            },
            {
                "asset_type_id": 10,
                "code": "isa_lifetime",
                "name": "Lifetime ISA",
                "asset_parent_type_id": 5,
                "description": "Select this if you are saving for retirement using a specific type of individual "
                "savings account.",
            },
            {
                "asset_type_id": 11,
                "code": "isa_help_to_buy",
                "name": "Help to buy ISA",
                "asset_parent_type_id": 5,
                "description": "Select this if you are saving for a first home in an individual savings account.",
            },
            {
                "asset_type_id": 12,
                "code": "cash",
                "name": "Cash savings",
                "asset_parent_type_id": None,
                "description": "Any savings held in cash whether it earns interest or not.",
            },
            {
                "asset_type_id": 13,
                "code": "cash_current_account",
                "name": "Current account",
                "asset_parent_type_id": 12,
                "description": "A freely accessible account for managing your day-to-day income and expenses.",
            },
            {
                "asset_type_id": 14,
                "code": "cash_term_deposit",
                "name": "Term deposit",
                "asset_parent_type_id": 12,
                "description": "A freely accessible account for managing your day-to-day income and expenses.",
            },
            {
                "asset_type_id": 15,
                "code": "cash_savings",
                "name": "Savings account",
                "asset_parent_type_id": 12,
                "description": "An account that attracts interest, and may have minimum deposit limits and "
                "limitations on withdrawals.",
            },
            {
                "asset_type_id": 16,
                "code": "cash_other",
                "name": "Other investments",
                "asset_parent_type_id": None,
                "description": "Anything else including unit trusts, bonds, crypto and more.",
            },
            {
                "asset_type_id": 17,
                "code": "property",
                "name": "Property",
                "asset_parent_type_id": None,
                "description": "Any type of property ownership including your home or buy-to-lets.",
            },
            {
                "asset_type_id": 18,
                "code": "property_main",
                "name": "Main residence",
                "asset_parent_type_id": 17,
                "description": None,
            },
            {
                "asset_type_id": 19,
                "code": "property_second",
                "name": "Second home",
                "asset_parent_type_id": 17,
                "description": None,
            },
            {
                "asset_type_id": 20,
                "code": "property_buy_to_let",
                "name": "Buy-to-let property",
                "asset_parent_type_id": 17,
                "description": None,
            },
            {
                "asset_type_id": 21,
                "code": "property_holiday",
                "name": "Holiday home",
                "asset_parent_type_id": 17,
                "description": None,
            },
            {
                "asset_type_id": 22,
                "code": "property_woodland",
                "name": "Woodland",
                "asset_parent_type_id": 17,
                "description": None,
            },
            {
                "asset_type_id": 23,
                "code": "property_commercial",
                "name": "Commercial property",
                "asset_parent_type_id": 17,
                "description": None,
            },
            {
                "asset_type_id": 24,
                "code": "property_agricultural",
                "name": "Agricultural property",
                "asset_parent_type_id": 17,
                "description": None,
            },
            {
                "asset_type_id": 25,
                "code": "property_portfolio",
                "name": "Property portfolio",
                "asset_parent_type_id": 17,
                "description": None,
            },
            {
                "asset_type_id": 26,
                "code": "property_other",
                "name": "Other",
                "asset_parent_type_id": 17,
                "description": None,
            },
        ],
    )
    employment_type = tb(
        "employment_type",
        column("employment_type_id", Integer),
        column("code", String),
        column("name", String),
    )
    session.execute(
        employment_type.insert(),
        [
            {"employment_type_id": 1, "code": "employee", "name": "Employee"},
            {"employment_type_id": 2, "code": "self_employed", "name": "Self-employed"},
        ],
    )
    current_situation_type = tb(
        "current_situation_type",
        column("current_situation_type_id", Integer),
        column("code", String),
        column("name", String),
    )
    session.execute(
        current_situation_type.insert(),
        [
            {
                "current_situation_type_id": 1,
                "code": "currently_working",
                "name": "Currently working",
            },
            {"current_situation_type_id": 2, "code": "semi_retired", "name": "Semi-retired"},
            {"current_situation_type_id": 3, "code": "retired", "name": "Retired"},
            {
                "current_situation_type_id": 4,
                "code": "not_working",
                "name": "Not currently working",
            },
        ],
    )
    frequency = tb(
        "frequency", column("frequency_id", Integer), column("code", String), column("name", String)
    )
    session.execute(
        frequency.insert(),
        [
            {"frequency_id": 1, "code": "per_month", "name": "Per month"},
            {"frequency_id": 2, "code": "per_year", "name": "Per year"},
            {"frequency_id": 3, "code": "per_term", "name": "Per term"},
        ],
    )
    plan_user_type = tb(
        "plan_user_type",
        column("plan_user_type_id", Integer),
        column("code", String),
        column("name", String),
        column("plan_user_parent_type_id", Integer),
    )
    session.execute(
        plan_user_type.insert(),
        [
            {
                "plan_user_type_id": 1,
                "code": "main",
                "name": "Main user",
                "plan_user_parent_type_id": None,
            },
            {
                "plan_user_type_id": 2,
                "code": "partner",
                "name": "Partner",
                "plan_user_parent_type_id": None,
            },
            {
                "plan_user_type_id": 3,
                "code": "child",
                "name": "Child",
                "plan_user_parent_type_id": None,
            },
            {
                "plan_user_type_id": 4,
                "code": "grandchild",
                "name": "Grandchild",
                "plan_user_parent_type_id": 7,
            },
            {
                "plan_user_type_id": 5,
                "code": "parent",
                "name": "Parent",
                "plan_user_parent_type_id": 7,
            },
            {
                "plan_user_type_id": 6,
                "code": "other",
                "name": "Other",
                "plan_user_parent_type_id": 7,
            },
            {
                "plan_user_type_id": 7,
                "plan_user_parent_type_id": None,
                "code": "dependent",
                "name": "Dependent",
            },
        ],
    )
    user_cashflow_type = tb(
        "user_cashflow_type",
        column("user_cashflow_type_id", Integer),
        column("code", String),
        column("name", String),
    )
    session.execute(
        user_cashflow_type.insert(),
        [
            {"user_cashflow_type_id": 1, "code": "nanny", "name": "Nanny"},
            {"user_cashflow_type_id": 2, "code": "childcare", "name": "Childcare"},
            {"user_cashflow_type_id": 3, "code": "education", "name": "Education"},
            {"user_cashflow_type_id": 4, "code": "dependants", "name": "Dependants cost"},
            {"user_cashflow_type_id": 5, "code": "work", "name": "Work"},
            {"user_cashflow_type_id": 6, "code": "assets", "name": "Assets"},
            {"user_cashflow_type_id": 7, "code": "debt_repayment", "name": "Debt repayment"},
            {"user_cashflow_type_id": 8, "code": "debt_overpayment", "name": "Debt overpayment"},
            {"user_cashflow_type_id": 9, "code": "expenses_total", "name": "Expenses"},
            {"user_cashflow_type_id": 10, "code": "expenses_insurance", "name": "Insurance"},
            {"user_cashflow_type_id": 11, "code": "work_salary", "name": "Annual salary"},
            {"user_cashflow_type_id": 12, "code": "work_bonus", "name": "Average annual bonus"},
            {
                "user_cashflow_type_id": 13,
                "code": "work_dividends",
                "name": "Average annual dividends",
            },
            {"user_cashflow_type_id": 14, "code": "work_other", "name": "Other (Work)"},
            {"user_cashflow_type_id": 15, "code": "other", "name": "Other"},
        ],
    )
    giving_type = tb(
        "giving_type",
        column("giving_type_id", Integer),
        column("code", String),
        column("name", String),
    )
    session.execute(
        giving_type.insert(),
        [
            {"giving_type_id": 1, "code": "charity", "name": "Charity"},
            {"giving_type_id": 2, "code": "gifting", "name": "Gifting"},
        ],
    )
    plan_debt_type = tb(
        "plan_debt_type",
        column("plan_debt_type_id", Integer),
        column("code", String),
        column("name", String),
        column("description", String),
        column("plan_debt_parent_type_id", Integer),
    )
    session.execute(
        plan_debt_type.insert(),
        [
            {
                "plan_debt_type_id": 1,
                "code": "mortgage",
                "name": "Mortgage & Buy-to-let",
                "plan_debt_parent_type_id": None,
                "description": "Any outstanding property loans",
            },
            {
                "plan_debt_type_id": 2,
                "code": "other_loans",
                "name": "Other loans",
                "plan_debt_parent_type_id": None,
                "description": "Personal loans or loans from friends or family",
            },
            {
                "plan_debt_type_id": 3,
                "code": "credit_card",
                "name": "Outstanding credit card",
                "plan_debt_parent_type_id": None,
                "description": "Any credit cards which are outstanding at the end of the month",
            },
            {
                "plan_debt_type_id": 4,
                "code": "mortgage_repayment",
                "name": "Repayment",
                "plan_debt_parent_type_id": 1,
                "description": "Where you make repayments which cover the capital plus interest.",
            },
            {
                "plan_debt_type_id": 5,
                "code": "mortgage_interest",
                "name": "Interest only",
                "plan_debt_parent_type_id": 1,
                "description": "Where your payments only cover the interest portion of your loan.",
            },
            {
                "plan_debt_type_id": 6,
                "code": "mortgage_endowment",
                "name": "Endowment",
                "plan_debt_parent_type_id": 1,
                "description": "Where payments cover both interest as well as an investment designed to pay off "
                "the mortgage on maturity.",
            },
            {
                "plan_debt_type_id": 7,
                "code": "other_loans_repayment",
                "name": "Repayment",
                "plan_debt_parent_type_id": 2,
                "description": "Where you make repayments which cover the capital plus interest.",
            },
            {
                "plan_debt_type_id": 8,
                "code": "other_loans_interest",
                "name": "Interest only",
                "plan_debt_parent_type_id": 2,
                "description": "Where your payments only cover the interest portion of your loan.",
            },
            {
                "plan_debt_type_id": 9,
                "code": "other_loans_not_sure",
                "name": "Not Sure",
                "plan_debt_parent_type_id": 2,
                "description": None,
            },
            {
                "plan_debt_type_id": 10,
                "code": "mortgage_not_sure",
                "name": "Not Sure",
                "plan_debt_parent_type_id": 1,
                "description": None,
            },
        ],
    )

    soft_fact_tag = tb(
        "soft_fact_tag",
        column("soft_fact_tag_id", Integer),
        column("code", String),
        column("name", String),
    )
    session.execute(
        soft_fact_tag.insert(),
        [
            {"soft_fact_tag_id": 1, "code": "objectives", "name": "Objectives"},
            {
                "soft_fact_tag_id": 2,
                "code": "conversationstarters",
                "name": "Conversation starters",
            },
            {"soft_fact_tag_id": 3, "code": "hobbies", "name": "Hobbies"},
            {"soft_fact_tag_id": 4, "code": "riskprofile", "name": "Risk profile"},
            {"soft_fact_tag_id": 5, "code": "retirementage", "name": "Retirement age"},
            {"soft_fact_tag_id": 6, "code": "discoverycall", "name": "Discovery call"},
        ],
    )
    regular_contribution_type = tb(
        "regular_contribution_type",
        column("regular_contribution_type_id", Integer),
        column("code", String),
        column("name", String),
    )
    session.execute(
        regular_contribution_type.insert(),
        [
            {
                "regular_contribution_type_id": 1,
                "code": "employer_contribution",
                "name": "Employer contribution",
            },
            {
                "regular_contribution_type_id": 2,
                "code": "personal_contribution",
                "name": "Personal Contribution",
            },
            {"regular_contribution_type_id": 3, "code": "receive_income", "name": "Receive income"},
            {"regular_contribution_type_id": 4, "code": "cash_saving", "name": "Cash saving"},
            {"regular_contribution_type_id": 5, "code": "investments", "name": "Investments"},
        ],
    )

    role = tb("role", column("role_id", Integer), column("name", String))
    session.execute(
        role.insert(),
        [
            {"role_id": 1, "name": "client"},
            {"role_id": 2, "name": "adviser"},
            {"role_id": 3, "name": "paraplanner"},
            {"role_id": 4, "name": "admin"},
            {"role_id": 5, "name": "specialist"},
        ],
    )
    session.commit()
