import http.client

import tornado

from decorators.auth_decorators import oauth_authenticated
from models.model import Plan, PlanSoftFact
from validation.dump_schemes import PlanSoftFactSchema, SoftFactSchema
from web import FactFindHandler


class SoftFactHandler(FactFindHandler):
    @oauth_authenticated
    def get(self, plan_uuid):
        plan = Plan.get_by_uuid(plan_uuid)
        self.result_or_404(plan, "plan")
        tags = self.get_argument("tags", default="")
        if tags:
            tags_list = tags.split(",")
            comments = PlanSoftFact.filter_by_tags(plan_id=plan["plan_id"], tags=tags_list)
        else:
            comments = PlanSoftFact.query.filter_by(plan_id=plan["plan_id"]).all()

        schema = PlanSoftFactSchema(many=True)
        soft_facts = schema.dump(comments).data

        self.json_response(soft_facts)

    @oauth_authenticated
    def post(self, plan_uuid):
        data = tornado.escape.json_decode(self.request.body)
        plan = Plan.get_by_uuid(plan_uuid)
        self.result_or_404(plan, "plan")
        user_id = self.current_user["user_id"]
        result, errors = SoftFactSchema().load(data)
        if errors:
            self.json_response(
                {"status": "OK", "message": "Data failed validation", "errors": errors},
                http.client.BAD_REQUEST,
            )
        else:
            result["plan_id"] = plan["plan_id"]
            soft_fact_uuid = PlanSoftFact.upsert(result, user_id)
            response = {
                "profile_uuid": self.current_user["profile_id"],
                "plan_uuid": plan_uuid,
                "soft_fact_uuid": soft_fact_uuid,
                "status": "OK",
                "message": "SoftFact successfully added",
            }
            self.json_response(response, http.client.CREATED)


class UpdateDeleteSoftFactHandler(FactFindHandler):
    # TODO add permission checker for edit comments

    @oauth_authenticated
    def get(self, soft_fact_uuid):
        soft_fact = PlanSoftFact.get_by_uuid(soft_fact_uuid, to_dict=False)
        self.result_or_404(soft_fact, "plan_soft_fact")
        schema = PlanSoftFactSchema()
        users_data = schema.dump(soft_fact).data
        self.json_response(users_data)

    @oauth_authenticated
    def patch(self, soft_fact_uuid):
        data = tornado.escape.json_decode(self.request.body)
        soft_fact = PlanSoftFact.get_by_uuid(soft_fact_uuid)
        self.result_or_404(soft_fact, "plan_soft_fact")
        user_id = self.current_user["user_id"]
        schema = SoftFactSchema()
        result, errors = schema.load(data)
        if errors:
            self.json_response(
                {"status": "OK", "message": "Data failed validation", "errors": errors},
                http.client.BAD_REQUEST,
            )
        else:
            soft_fact_uuid = PlanSoftFact.upsert(result, user_id, soft_fact["plan_soft_fact_id"])
            response = {
                "profile_uuid": self.current_user["profile_id"],
                "soft_fact_uuid": soft_fact_uuid,
                "status": "OK",
                "message": "SoftFact successfully updated",
            }
            self.json_response(response, http.client.OK)

    @oauth_authenticated
    def delete(self, soft_fact_uuid):
        soft_fact = PlanSoftFact.get_by_uuid(soft_fact_uuid)
        self.result_or_404(soft_fact, "plan_soft_fact")
        PlanSoftFact.delete(soft_fact["plan_soft_fact_id"])
        self.set_status(204)
