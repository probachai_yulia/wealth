# -*- coding: utf-8 -*-

import http.client

from tornado.options import options

from decorators.decorators import fact_find_enabled
from libraries.profile import ProfileClient
from utils.plan_utils import create_or_get_user
from web import PageHandler
from models import model


class ProfileHandler(PageHandler):
    @fact_find_enabled
    def prepare(self):
        super(ProfileHandler, self).prepare()

    def has_profile_error(self, result, message):
        if isinstance(result, dict) and result.get("message"):
            self.json_response(
                {"status": "ERR", "message": message + "{}".format(result["message"])},
                http.client.BAD_REQUEST,
            )
            return True
        return False

    def get_user(self, profile_uuid):
        user = model.User.get_by_uuid(profile_uuid)
        if user is None:
            self.json_response(
                {
                    "status": "ERR",
                    "message": "No user found for profile_uuid {}".format(profile_uuid),
                },
                http.client.BAD_REQUEST,
            )
        return user

    def get_profile(self, profile_id):
        result = ProfileClient.get(profile_uuid=profile_id)
        self.has_profile_error(result, "Error with getting profile:")
        return result

    def get_profiles_list(self):
        result = ProfileClient.get_profiles_list()
        self.has_profile_error(result, "Error with getting profiles list:")
        return result

    def create_profile(self, data, token):
        result = None
        try:
            request = {"client_id": options.oauth_client_id, "password": token}
            keys = [
                "first_name",
                "last_name",
                "middle_name",
                "email",
                "dob",
                "phone",
                "national_insurance_nbr",
                "birth_country_cd",
                "citizenships",
            ]
            for key in keys:
                if data.get(key):
                    request[key] = data[key]
            result = ProfileClient.create(request)
            return result["profile"]["profile_id"]
        except Exception:
            self.json_response(
                {
                    "status": "ERR",
                    "message": "Error with creating profile".format(result["message"]),
                },
                http.client.BAD_REQUEST,
            )

    def update_profile(self, data, profile_id):
        request = {}
        keys = [
            "first_name",
            "last_name",
            "middle_name",
            "dob",
            "phone",
            "national_insurance_nbr",
            "birth_country_cd",
            "citizenships",
        ]
        for key in keys:
            if data.get(key):
                request[key] = data[key]
        result = ProfileClient.update(profile_id, request)
        self.has_profile_error(result, "Error with updating profile:")

        return profile_id

    def update_email(self, profile_id, data):
        email = data.get("email")
        if email and ProfileClient.get_email(profile_id) != email:
            email_id = ProfileClient.add_email(profile_id, email)
            result = ProfileClient.make_email_primary(profile_id, email_id)
            self.has_profile_error(result, "Error with updating email:")
            return result
        return None

    def create_or_update_profile(self, data, profile_id=None, token=""):
        if profile_id:
            profile_id = self.update_profile(data, profile_id)
        else:
            profile_id = self.create_profile(data, token)
        return profile_id

    def create_profile_user(self, result, profile_uuid=None, token=""):
        profile_id = self.create_or_update_profile(result, profile_uuid, token)
        user_id = create_or_get_user(profile_id, result, token)
        self.set_access_to_profile(user_id, profile_id)
        self.set_relation(options.company_profile_id, options.firm_relation_type_id, profile_id)
        return profile_id, user_id

    def delete_profile(self, profile_id):
        result = ProfileClient.delete(profile_id)
        self.has_profile_error(result, "Error with deleting profile:")
        return None

    def set_access_to_profile(self, user_id, profile_id):
        data = {"user_id": user_id}
        result = ProfileClient.set_access(data, profile_id)
        self.has_profile_error(result, "Error with setting access (user_id) to profile:")
        return True

    def search(self, user_ids="", email="", name=""):
        data = {"user_ids": user_ids, "email": email, "name": name}
        result = ProfileClient.search(data)
        self.has_profile_error(result, "Error with search at the profile:")
        return result

    # primary-address as default
    def set_addresses(self, data, profile_id, address_type_id=1):
        url = "{}/profiles/{}/addresses".format(options.profile_url, profile_id)
        data = {
            "address_1": data["address_1"],
            "address_2": data.get("address_2"),
            "postcode": data["postcode"],
            "city": data["city"],
            "country_cd": data.get("country_cd", ""),
            "address_type_id": address_type_id,
        }
        result = ProfileClient.authenticated_request(
            url, data, method="POST", operation="Set address to user profile"
        )
        self.has_profile_error(result, "Error with setting address:")
        return result

    def change_address(self, data, address_id, address_type_id=1):
        url = "{}/addresses/{}".format(options.profile_url, address_id)
        data = {
            "address_1": data["address_1"],
            "address_2": data.get("address_2", ""),
            "postcode": data["postcode"],
            "city": data["city"],
            "country_cd": data.get("country_cd", ""),
            "address_type_id": address_type_id,
        }
        result = ProfileClient.authenticated_request(
            url, data, method="PUT", operation="Change address"
        )
        self.has_profile_error(result, "Error with changing address:")
        return result

    def get_address(self, address_id):
        result = ProfileClient.get_address(address_id)
        self.has_profile_error(result, "Error with getting address:")
        return result

    def get_addresses_list(self, profile_id):
        url = "{}/profiles/{}/addresses".format(options.profile_url, profile_id)
        result = ProfileClient.authenticated_request(url, method="GET", operation="GET address")
        self.has_profile_error(result, "Error with getting address:")
        return result

    def delete_address(self, address_id):
        result = ProfileClient.delete_address(address_id)
        self.has_profile_error(result, "Error with deleting address:")
        return result

    def set_password(self, email):
        url = "{}/forgot_password".format(options.profile_url)
        data = {"email": email, "client_id": options.platform_id}
        result = ProfileClient.authenticated_request(
            url, data, method="POST", operation="Set password to user profile"
        )
        self.has_profile_error(result, "Error with setting password:")
        return result

    def change_password(self, profile_id, password, new_password):
        url = "{}/profiles/{}/password".format(options.profile_url, profile_id)
        data = {"password": password, "new_password": new_password}
        result = ProfileClient.authenticated_request(
            url, data, method="PUT", operation="Change user password with profile"
        )
        self.has_profile_error(result, "Error with changing password:")
        return result

    def set_del_admin_to_profile(self, method, profile_id):
        url = "{}/profiles/{}/admin".format(options.profile_url, profile_id)
        result = None
        if method == "PUT":
            result = ProfileClient.authenticated_request(
                url, method="PUT", operation="Set admin status to user profile"
            )
        if method == "DELETE":
            result = ProfileClient.authenticated_request(
                url, method="DELETE", operation="Delete admin status of user profile"
            )
        self.has_profile_error(result, "Error with setting access (user_id) to profile:")
        return None

    def get_related_users(self, profile_id, type, direction):
        url = "{}/profiles/{}/relationships?direction={}&relation_type_id={}".format(
            options.profile_url, profile_id, direction, type
        )
        result = ProfileClient.authenticated_request(
            url, method="GET", operation="Get related users"
        )
        self.has_profile_error(result, "Error with getting related users:")
        return result

    def set_relation(self, profile_id, relation_id, related_profile_id):
        data = {"relation_type_id": relation_id, "profile_id": profile_id}
        result = ProfileClient.set_relation(data, related_profile_id)
        self.has_profile_error(result, "Error with setting related users:")
        return result

    def set_related_user(self, profile_id, related_profile_id, role):
        try:
            self.user_or_404(profile_id)
            self.user_or_404(related_profile_id)
            self.set_relation(profile_id, role, related_profile_id)
        except Exception as e:
            self.json_response({"error": str(e), "status": "ERR"}, http.client.BAD_REQUEST)
        else:
            self.json_response(
                {
                    "profile_uuid": profile_id,
                    "related_profile_uuid": related_profile_id,
                    "status": "OK",
                    "message": "Relation was added",
                },
                http.client.OK,
            )
