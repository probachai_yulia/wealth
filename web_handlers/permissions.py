import http.client

# Octopus libraries
import tornado
import tornado.escape

# external imports
import tornado.httpclient

from libraries.admin import AdminClient
from models.model import Permission, Role, RolePermission, User, UserPermission, UserRole
from validation.validation_schemes import PermissionSchema, RoleSchema
from web import PageHandler


# Standard libraries


class RoleHandler(PageHandler):
    def get(self, role_name=None):
        if role_name:
            role = self.result_or_404(Role.get_role_by_name(name=role_name), "role")
            self.json_response(role)
        else:
            roles = Role.all()
            self.json_response(roles)

    def post(self, role_name=None):
        data = tornado.escape.json_decode(self.request.body)
        result, errors = RoleSchema().load(data)
        if not errors:
            role = AdminClient.create_or_update_role(result, role_name)
            permissions = []
            for permission in result.get("permissions", []):
                permission = AdminClient.create_or_update_permission(
                    permission, permission.get("code"), role["role_id"]
                )
                permissions.append(permission["code"])
            return self.json_response(
                {
                    "message": "Role was successfully created/updated",
                    "role": role["name"],
                    "permissions": permissions,
                    "status": "OK",
                },
                http.client.CREATED,
            )
        else:
            self.json_response(
                {"status": "ERR", "message": "Validation error", "errors": errors},
                http.client.BAD_REQUEST,
            )

    def delete(self, role_name):
        role = self.result_or_404(Role.get_role_by_name(name=role_name), "role")
        Role.delete(role["role_id"])
        return self.json_response(
            {"message": "Role was successfully deleted", "status": "OK"}, http.client.OK
        )


class UserToRoleHandler(PageHandler):
    def post(self, role_name, profile_uuid):
        role = self.result_or_404(Role.get_role_by_name(role_name), "role")
        user = self.user_or_404(profile_uuid)
        if UserRole.query.filter_by(role_id=role["role_id"], user_id=user["user_id"]).count() > 0:
            self.json_response(
                {"message": "User already added to this role", "status": "ERR"},
                http.client.ACCEPTED,
            )
        else:
            UserRole.clear_table(user_id=user["user_id"])
            UserRole.add_user_role(role_id=role["role_id"], user_id=user["user_id"])
            return self.json_response(
                {"message": "User was successfully added to role", "status": "OK"}, http.client.OK
            )

    def delete(self, role_name, profile_uuid):
        user = self.user_or_404(profile_uuid)
        role = self.result_or_404(Role.get_role_by_name(role_name), "role")
        user_role = UserRole.query.filter_by(
            role_id=role["role_id"], user_id=user["user_id"]
        ).first()
        if user_role:
            UserRole.delete(user_role.user_role_id)
            return self.json_response(
                {"message": "User was successfully removed from role", "status": "OK"},
                http.client.OK,
            )
        else:
            self.json_response(
                {"message": "Resource Not Found", "status": "ERR"}, http.client.NOT_FOUND
            )


class PermissionHandler(PageHandler):
    def get(self, code=None):
        if code:
            permission = self.result_or_404(
                Permission.get_permission_by_code(code=code), "permission"
            )
            self.json_response(permission)
        else:
            permissions = Permission.all()
            self.json_response(permissions)

    def post(self, code=None):
        data = tornado.escape.json_decode(self.request.body)
        result, errors = PermissionSchema().load(data)
        if not errors:
            permission = AdminClient.create_or_update_permission(result, code)
            return self.json_response(
                {
                    "message": "Role was successfully created/updated",
                    "permission": permission["code"],
                    "status": "OK",
                },
                http.client.CREATED,
            )
        else:
            self.json_response(
                {"status": "ERR", "message": "Validation error", "errors": errors},
                http.client.BAD_REQUEST,
            )


class UserTokenPermissionHandler(PageHandler):
    def get(self, token):
        user = self.result_or_404(User.query.filter_by(token=token).first(), "user")
        permission_code = self.get_argument("permission_code", None)
        if permission_code:
            self.json_response(User.has_permission(user["user_id"], permission_code))
        else:
            permissions = User.get_permissions_by_user_id(user["user_id"])
            self.json_response(permissions)


class UserPermissionHandler(PageHandler):
    def post(self, permission_code, profile_uuid):
        permission = self.result_or_404(
            Permission.get_permission_by_code(permission_code), "permission"
        )
        user = self.user_or_404(profile_uuid)
        if User.has_permission(user["user_id"], permission_code):
            self.json_response(
                {"message": "User already has this permission", "status": "ERR"},
                http.client.ACCEPTED,
            )
        else:
            UserPermission.add_user_permission(
                permission_id=permission["permission_id"], user_id=user["user_id"]
            )
            return self.json_response(
                {"message": "Permission was added to user", "status": "OK"}, http.client.OK
            )

    def delete(self, permission_id, user_uuid):
        self.result_or_404(Permission.get_by_id(permission_id), "permission")
        user = self.user_or_404(user_uuid)
        user_permission = UserPermission.query.filter_by(
            user_id=user["user_id"], permission_id=permission_id
        ).first()
        if user_permission:
            UserPermission.final_delete(user_permission.user_permission_id)
            return self.json_response(
                {"message": "Permission was refused from user", "status": "OK"}, http.client.OK
            )
        else:
            self.json_response(
                {"message": "Resource Not Found", "status": "ERR"}, http.client.NOT_FOUND
            )


class RolePermissionHandler(PageHandler):
    def post(self, permission_code, role_name):
        permission = self.result_or_404(
            Permission.get_permission_by_code(permission_code), "permission"
        )
        role = self.result_or_404(Role.get_role_by_name(role_name), "role")
        if RolePermission.has_permission(role_name, permission_code):
            self.json_response(
                {"message": "Role already has this permission", "status": "ERR"},
                http.client.ACCEPTED,
            )
        else:
            RolePermission.add_role_permission(
                permission_id=permission["permission_id"], role_id=role["role_id"]
            )
            return self.json_response(
                {"message": "Permission was added to role", "status": "OK"}, http.client.OK
            )

    def delete(self, permission_code, role_name):
        self.result_or_404(Permission.get_permission_by_code(permission_code), "permission")
        self.result_or_404(Role.get_role_by_name(role_name), "role")
        role_permission = RolePermission.has_permission(role_name, permission_code)
        if role_permission:
            RolePermission.final_delete(role_permission[0]["role_permission_id"])
            return self.json_response(
                {"message": "Permission was refused from role", "status": "OK"}, http.client.OK
            )
        else:
            self.json_response(
                {"message": "Resource Not Found", "status": "ERR"}, http.client.NOT_FOUND
            )
