from models.model import TokenToUserTable
from tornado.options import options
from web import PageHandler


class GetCreateUserTokenHandler(PageHandler):
    def get(self, profile_uuid):
        user = self.user_or_404(profile_uuid)
        user_tokens = (
            TokenToUserTable.query.filter_by(user_id=user.get("user_id"))
            .order_by("create_dttm")
            .first()
        )
        self.json_response(user_tokens.to_dict() if user_tokens else None)

    def post(self, profile_uuid):
        user = self.user_or_404(profile_uuid)
        TokenToUserTable.add_token_to_user_table(user.get("user_id"))
        self.json_response(user)


class CronExpireUserTokenHandler(PageHandler):
    def delete(self):
        TokenToUserTable.delete_token_to_user_table(options.user_token_expires)
