import http.client
import urllib
import uuid
from datetime import datetime

import tornado
from tornado.options import options

from commands.helpers import create_user_role
from decorators.auth_decorators import oauth_authenticated, role_required
from libraries import utils
from models.model import (
    ApproximateAge,
    AssetType,
    CurrentSituationType,
    EmploymentType,
    Frequency,
    GivingType,
    MaritalStatus,
    Plan,
    PlanAsset,
    PlanAssetOwner,
    PlanCashflow,
    PlanDebt,
    PlanDebtOwner,
    PlanDebtType,
    PlanDocument,
    PlanGiving,
    PlanUser,
    PlanUserJob,
    PlanUserType,
    RateType,
    RegularContribution,
    RegularContributionType,
    Role,
    SoftFactTag,
    TaxBenefit,
    TermsAndConditionAccept,
    USConnection,
    User,
    UserCashflowType,
)
from utils.constants import COUNTRY_LIST
from utils.plan_utils import create_password, delete_blob, download_blob, upload_blob
from validation.dump_schemes import PlanDebtSchema, PlanDocumentSchema
from validation.validation_schemes import (
    AddressSchema,
    AssetSchema,
    CashflowSchema,
    DebtSchema,
    GivingSchema,
    JobSchema,
    PlanSchema,
    UserSchema,
)
from web import FactFindHandler
from web_handlers.Base import BasePlanHandler
from web_handlers.profile import ProfileHandler


class SurveyHandler(FactFindHandler):
    pass


class GetAddressHandler(FactFindHandler):
    def get_address_by_post_code(self, post_code):
        url = "{}/find/{}?api-key={}".format(
            options.getaddress_url, post_code, options.getaddress_api_key
        )
        try:
            response = utils.make_request(url, method="get")
        except Exception as e:
            self.json_response(
                {"status": "ERR", "message": "Cannot find postcode {}".format(post_code)},
                http.client.BAD_REQUEST,
            )
        else:
            array = response.get("addresses", None)
            result = []
            if array:
                for address in array:
                    address = address.split(",")
                    result.append(
                        {
                            "Line1": address[0].strip(),
                            "Line2": address[1].strip(),
                            "Line3": address[2].strip(),
                            "Line4": address[3].strip(),
                            "Locality": address[4].strip(),
                            "Town/City": address[5].strip(),
                            "County": address[6].strip(),
                        }
                    )
            return result

    def get(self):
        post_code = self.get_argument("post_code", default=None)
        result = self.get_address_by_post_code(post_code) if post_code else []
        self.json_response(result)


class OptionsListHandler(FactFindHandler):
    def get(self):
        result = {}
        result["us_connections"] = USConnection.all_without_id_name()
        result["marital_statuses"] = MaritalStatus.all_without_id_name()
        result["approximate_ages"] = ApproximateAge.all_without_id_name()
        result["rate_types"] = RateType.all_without_id_name()
        result["plan_debt_types"] = PlanDebtType.get_plan_debt_types_with_parent()
        result["user_cashflow_types"] = UserCashflowType.all_without_id_name()
        result["giving_types"] = GivingType.all_without_id_name()
        result["tax_benefits"] = TaxBenefit.all_without_id_name()
        result["regular_contribution_types"] = RegularContributionType.all_without_id_name()
        result["asset_types"] = AssetType.get_asset_types_with_parent()
        result["employment_types"] = EmploymentType.all_without_id_name()
        result["current_situations"] = CurrentSituationType.all_without_id_name()
        result["frequency"] = Frequency.all_without_id_name()
        result["plan_user_types"] = PlanUserType.get_plan_user_types_with_parent()
        result["roles"] = Role.get_roles_list()
        result["soft_fact_tags"] = SoftFactTag.all_without_id_name()
        result["countries"] = OptionsListHandler.get_countries()
        self.json_response(result)

    @staticmethod
    def get_countries():
        result = []
        for country in COUNTRY_LIST:
            result.append({"code": country["iso_cd"], "name": country["name"]})
        return result


class PlanAssetHandler(FactFindHandler):
    def get(self, plan_uuid, plan_asset_uuid=None):
        plan = Plan.get_by_uuid(plan_uuid)
        self.result_or_404(plan, "plan")
        asset_type = self.get_argument("asset_type", default="")
        profile_uuid = self.get_argument("profile_uuid", default="")
        plan_user = PlanUser.get_plan_user_by_profile_uuid(profile_uuid)
        result = BasePlanHandler.get_asset_data(
            PlanAsset.query.filter_by(plan_id=plan["plan_id"]).all()
        )
        if asset_type:
            result = utils.filter(
                result,
                BasePlanHandler.get_asset_data(
                    PlanAsset.query.filter_by(
                        asset_type_id=AssetType.get_id_by_code(asset_type)
                    ).all()
                ),
            )
        if plan_user:
            result = utils.filter(
                result,
                BasePlanHandler.get_asset_data(
                    PlanAsset.get_assets_by_owner(plan_user["plan_user_id"], to_dict=False)
                ),
            )
        if plan_asset_uuid:
            asset = PlanAsset.get_by_uuid(plan_asset_uuid)
            self.result_or_404(asset, "asset")
            plan_asset = BasePlanHandler.get_asset_data(
                PlanAsset.query.filter_by(
                    plan_id=plan["plan_id"], plan_asset_id=asset["plan_asset_id"]
                ).all()
            )
            result = plan_asset[0] if plan_asset else {}
        self.json_response(result)

    def post(self, plan_uuid, plan_asset_uuid=None):
        data = tornado.escape.json_decode(self.request.body)
        plan = Plan.get_by_uuid(plan_uuid)
        self.result_or_404(plan, "plan")
        asset = PlanAsset.get_by_uuid(plan_asset_uuid)
        id = asset["plan_asset_id"] if asset else None

        result, errors = AssetSchema().load(data)
        if not errors:

            plan_asset = PlanAsset.upsert(result, plan["plan_id"], id)
            regs_ids = []
            for reg in result.get("regular_contribution", []):
                regular_contribution_id = RegularContribution.upsert(
                    reg, plan_asset["plan_asset_id"]
                )
                regs_ids.append(regular_contribution_id)
            cashflow_ids = []
            for cashflow in result.get("cashflows", []):
                cashflow["plan_asset_id"] = plan_asset["plan_asset_id"]
                plan_user = PlanUser.get_plan_user_by_profile_uuid(data.get("plan_user_uuid"))
                plan_user_id = plan_user["plan_user_id"] if plan_user else None
                plan_cashflow = PlanCashflow.upsert(plan, cashflow, plan_user_id)
                cashflow_ids.append(plan_cashflow["plan_cashflow_uuid"])

            plan_asset_id = plan_asset["plan_asset_id"]
            asset_owners = result.get("owners", [])
            if asset_owners:
                PlanAssetOwner.clear_table(plan_asset_id=plan_asset_id, commit=False)
                for owner in asset_owners:
                    PlanAssetOwner.upsert(owner, plan_asset_id)

            asset_documents = result.get("plan_documents_uuids", [])
            if asset_documents:
                for doc in asset_documents:
                    document = PlanDocument.get_by_uuid(doc)
                    PlanDocument.update(document["plan_document_id"], asset_id=plan_asset_id)

            self.json_response(
                {
                    "status": "OK",
                    "message": "Plan asset was successfully added",
                    "plan_asset_uuid": plan_asset["plan_asset_uuid"],
                    "regular_contribution_uuids": regs_ids,
                    "cashflow_uuids": cashflow_ids,
                    "owners": result.get("owners", []),
                    "plan_documents_uuids": result.get("plan_documents_uuids", []),
                }
            )
        else:
            self.json_response(
                {"status": "ERR", "message": "Validation error", "errors": errors},
                http.client.BAD_REQUEST,
            )

    def delete(self, plan_uuid, plan_asset_uuid):
        plan = Plan.get_by_uuid(plan_uuid)
        self.result_or_404(plan, "plan")
        asset = PlanAsset.get_by_uuid(plan_asset_uuid)
        self.result_or_404(plan, "plan")
        PlanAsset.delete_related(asset["plan_asset_id"])
        self.json_response(http.client.NO_CONTENT)


class PlanAssetCashFlowHandler(FactFindHandler):
    def post(self, plan_uuid, plan_asset_uuid):
        data = tornado.escape.json_decode(self.request.body)
        plan = Plan.get_by_uuid(plan_uuid)
        asset = PlanAsset.get_by_uuid(plan_asset_uuid)
        self.result_or_404(plan, "plan")
        self.result_or_404(asset, "asset")
        result, errors = CashflowSchema().load(data)
        if not errors:

            plan_user = PlanUser.get_plan_user_by_profile_uuid(data.get("profile_uuid"))
            plan_user_id = plan_user["plan_user_id"] if plan_user else None
            result["plan_asset_id"] = asset["plan_asset_id"]
            plan_cashflow = PlanCashflow.upsert(plan, result, plan_user_id)
            self.json_response(
                {
                    "status": "OK",
                    "message": "Plan cashflow was successfully added",
                    "plan_cashflow_uuid": plan_cashflow["plan_cashflow_uuid"],
                }
            )
        else:
            self.json_response(
                {"status": "ERR", "message": "Validation error", "errors": errors},
                http.client.BAD_REQUEST,
            )


class PlanDebtHandler(FactFindHandler):
    def get(self, plan_uuid, debt_uuid=None):
        plan_debt_type = self.get_argument("plan_debt_type", default="")
        plan_cashflow_uuid = self.get_argument("plan_cashflow_uuid", default="")
        plan_cashflow = PlanCashflow.get_by_uuid(plan_cashflow_uuid)
        profile_uuid = self.get_argument("profile_uuid", default="")
        plan_user = PlanUser.get_plan_user_by_profile_uuid(profile_uuid)

        plan = Plan.get_by_uuid(plan_uuid)
        self.result_or_404(plan, "plan")
        result = BasePlanHandler.get_debt_data(
            PlanDebt.query.filter_by(plan_id=plan["plan_id"]).all()
        )
        if plan_debt_type:
            result = utils.filter(
                result,
                BasePlanHandler.get_debt_data(PlanDebt.query.filter_by(code=plan_debt_type).all()),
            )
        if plan_user:
            result = utils.filter(
                result,
                BasePlanHandler.get_debt_data(
                    PlanDebt.get_plan_debt_by_owner(plan_user["plan_user_id"], to_dict=False)
                ),
            )
        if plan_cashflow:
            result = utils.filter(
                result,
                BasePlanHandler.get_debt_data(
                    PlanDebt.query.filter_by(
                        plan_cashflow_id=plan_cashflow["plan_cashflow_id"]
                    ).all()
                ),
            )
        if debt_uuid:
            debt = PlanDebt.get_by_uuid(debt_uuid)
            self.result_or_404(debt, "debt")
            plan_debt = PlanDebt.query.filter_by(
                plan_id=plan["plan_id"], plan_debt_id=debt["plan_debt_id"]
            ).first()
            schema = PlanDebtSchema(many=False)
            result = schema.dump(plan_debt).data
            # result = plan_debt.to_dict() if plan_debt else {}
        self.json_response(result)

    def post(self, plan_uuid, debt_uuid=None):
        data = tornado.escape.json_decode(self.request.body)
        plan = Plan.get_by_uuid(plan_uuid)
        self.result_or_404(plan, "plan")

        debt = PlanDebt.get_by_uuid(debt_uuid)
        plan_debt_id = debt["plan_debt_id"] if debt else None

        result, errors = DebtSchema().load(data)
        if not errors:
            plan_debt_id, plan_debt_uuid = PlanDebt.upsert(result, plan["plan_id"], plan_debt_id)
            cashflow_ids = []
            for cashflow in result.get("cashflows", []):
                cashflow["plan_debt_id"] = plan_debt_id
                plan_user = PlanUser.get_plan_user_by_profile_uuid(data.get("plan_user_uuid"))
                plan_user_id = plan_user["plan_user_id"] if plan_user else None
                plan_cashflow = PlanCashflow.upsert(plan, cashflow, plan_user_id)
                cashflow_ids.append(plan_cashflow["plan_cashflow_uuid"])
            debt_owners = result.get("owners", [])
            if debt_owners:
                PlanDebtOwner.clear_table(plan_debt_id=plan_debt_id, commit=False)
                for owner in debt_owners:
                    PlanDebtOwner.upsert(owner, plan_debt_id)

            self.json_response(
                {
                    "status": "OK",
                    "message": "Plan debt was successfully added",
                    "plan_debt_uuid": plan_debt_uuid,
                    "owners": result.get("owners", []),
                    "cashflow_uuids": cashflow_ids,
                }
            )
        else:
            self.json_response(
                {"status": "ERR", "message": "Validation error", "errors": errors},
                http.client.BAD_REQUEST,
            )

    def delete(self, plan_uuid, debt_uuid):
        plan = Plan.get_by_uuid(plan_uuid)
        self.result_or_404(plan, "plan")
        debt = PlanDebt.get_by_uuid(debt_uuid)
        self.result_or_404(plan, "plan")
        PlanDebt.delete_related(debt["plan_debt_id"])
        self.json_response(http.client.NO_CONTENT)


class PlanDebtCashFlowHandler(FactFindHandler):
    def post(self, plan_uuid, debt_uuid):
        data = tornado.escape.json_decode(self.request.body)
        plan = Plan.get_by_uuid(plan_uuid)
        debt = PlanDebt.get_by_uuid(debt_uuid)
        self.result_or_404(plan, "plan")
        self.result_or_404(debt, "debt")
        result, errors = CashflowSchema().load(data)
        if not errors:
            plan_user = PlanUser.get_plan_user_by_profile_uuid(data.get("profile_uuid"))
            plan_user_id = plan_user["plan_user_id"] if plan_user else None
            result["plan_debt_id"] = debt["plan_debt_id"]
            plan_cashflow = PlanCashflow.upsert(plan, result, plan_user_id)
            self.json_response(
                {
                    "status": "OK",
                    "message": "Plan cashflow was successfully added",
                    "plan_cashflow_uuid": plan_cashflow["plan_cashflow_uuid"],
                },
                http.client.CREATED,
            )
        else:
            self.json_response(
                {"status": "ERR", "message": "Validation error", "errors": errors},
                http.client.BAD_REQUEST,
            )


class PlanEmploymentHandler(FactFindHandler):
    def get(self, plan_uuid, profile_uuid):
        plan = Plan.get_by_uuid(plan_uuid)
        self.result_or_404(plan, "plan")
        plan_user = PlanUser.get_plan_user_by_profile_uuid(profile_uuid)
        self.result_or_404(plan_user, "plan_user")
        result = BasePlanHandler.get_user_jobs(plan_user["plan_user_id"])
        self.json_response(result)

    def post(self, plan_uuid, profile_uuid=None, employment_uuid=None):
        data = tornado.escape.json_decode(self.request.body)
        plan = Plan.get_by_uuid(plan_uuid)
        self.result_or_404(plan, "plan")
        employment = PlanUserJob.get_by_uuid(employment_uuid)
        id = employment["plan_user_job_id"] if employment else None
        result, errors = JobSchema().load(data)
        if not errors:
            if profile_uuid:
                result["profile_uuid"] = profile_uuid
            plan_employment_uuid = PlanUserJob.upsert(result, id)
            self.json_response(
                {
                    "status": "OK",
                    "message": "Plan user job was successfully added/updated",
                    "plan_employment_uuid": plan_employment_uuid,
                }
            )
        else:
            self.json_response(
                {"status": "ERR", "message": "Validation error", "errors": errors},
                http.client.BAD_REQUEST,
            )


class PlanEmploymentCashFlowHandler(FactFindHandler):
    def post(self, plan_uuid, employment_uuid):
        data = tornado.escape.json_decode(self.request.body)
        plan = Plan.get_by_uuid(plan_uuid)
        job = PlanUserJob.get_by_uuid(employment_uuid)
        self.result_or_404(plan, "plan")
        self.result_or_404(job, "job")
        result, errors = CashflowSchema().load(data)
        if not errors:
            result["plan_user_job_id"] = job["plan_user_job_id"]

            plan_user = PlanUser.get_plan_user_by_profile_uuid(data.get("profile_uuid"))
            plan_user_id = plan_user["plan_user_id"] if plan_user else None

            plan_cashflow = PlanCashflow.upsert(plan, result, plan_user_id)
            PlanUserJob.update(
                job["plan_user_job_id"], plan_cashflow_id=plan_cashflow["plan_cashflow_id"]
            )
            self.json_response(
                {
                    "status": "OK",
                    "message": "Plan cashflow was successfully added",
                    "plan_cashflow_uuid": plan_cashflow["plan_cashflow_uuid"],
                },
                http.client.CREATED,
            )
        else:
            self.json_response(
                {"status": "ERR", "message": "Validation error", "errors": errors},
                http.client.BAD_REQUEST,
            )


class PlanCashflowHandler(FactFindHandler):
    def get(self, plan_uuid, cashflow_uuid=None):
        plan = Plan.get_by_uuid(plan_uuid)
        self.result_or_404(plan, "plan")
        result = BasePlanHandler.get_cashflow_data(
            PlanCashflow.query.filter_by(plan_id=plan["plan_id"]).all()
        )
        if cashflow_uuid:
            cashflow = PlanCashflow.get_by_uuid(cashflow_uuid)
            self.result_or_404(cashflow, "cashflow")
            result = BasePlanHandler.get_cashflow_data(
                PlanCashflow.query.filter_by(
                    plan_id=plan["plan_id"], plan_cashflow_id=cashflow["plan_cashflow_id"]
                ).all()
            )
        self.json_response(result)

    def patch(self, plan_uuid, cashflow_uuid=None):
        data = tornado.escape.json_decode(self.request.body)
        plan = Plan.get_by_uuid(plan_uuid)
        self.result_or_404(plan, "plan")
        cashflow = PlanCashflow.get_by_uuid(cashflow_uuid)
        self.result_or_404(cashflow, "cashflow")
        result, errors = CashflowSchema().load(data)
        if not errors:
            plan_user = PlanUser.get_plan_user_by_profile_uuid(data.get("profile_uuid"))
            plan_user_id = plan_user["plan_user_id"] if plan_user else None

            plan_cashflow = PlanCashflow.upsert(
                plan, result, plan_user_id, cashflow["plan_cashflow_id"]
            )
            self.json_response(
                {
                    "status": "OK",
                    "message": "Plan cashflow was successfully updated",
                    "plan_cashflow_uuid": plan_cashflow["plan_cashflow_uuid"],
                }
            )
        else:
            self.json_response(
                {"status": "ERR", "message": "Validation error", "errors": errors},
                http.client.BAD_REQUEST,
            )

    def post(self, plan_uuid):
        """
        Endpoint to create a new PlanCashFlow.
        """
        data = tornado.escape.json_decode(self.request.body)
        plan = Plan.get_by_uuid(plan_uuid)
        self.result_or_404(plan, "plan")

        result, errors = CashflowSchema().load(data)
        if not errors:
            plan_user = PlanUser.get_plan_user_by_profile_uuid(data.get("profile_uuid"))
            plan_user_id = plan_user["plan_user_id"] if plan_user else None
            plan_cashflow = PlanCashflow.upsert(plan, result, plan_user_id)
            self.json_response(
                {
                    "status": "OK",
                    "message": "Plan cashflow was successfully created",
                    "plan_cashflow_uuid": plan_cashflow["plan_cashflow_uuid"],
                }
            )
        else:
            self.json_response(
                {"status": "ERR", "message": "Validation error", "errors": errors},
                http.client.BAD_REQUEST,
            )


class AddressHandler(ProfileHandler):
    def get(self, plan_uuid):
        plan = Plan.get_by_uuid(plan_uuid)
        self.result_or_404(plan, "plan")
        profile = User.get_main_user_profile_uuid(plan_id=plan["plan_id"])
        result = {}
        if profile:
            result.update(self.get_addresses_list(profile))
        self.json_response(result)

    def post(self, plan_uuid):
        data = tornado.escape.json_decode(self.request.body)
        plan = Plan.get_by_uuid(plan_uuid)
        self.result_or_404(plan, "plan")
        result, errors = AddressSchema().load(data)
        if not errors:
            profile_uuid = User.get_main_user_profile_uuid(plan_id=plan["plan_id"])
            address_type = self.get_argument("is_primary", default=1)
            addresses = self.get_addresses_list(profile_uuid)["addresses"]
            addresses_list = addresses if type(addresses) == list else []
            duplicated_address = {}
            for item in addresses_list:
                if (
                    item["address_1"] == result["address_1"]
                    and item["city"] == result["city"]
                    and item["postcode"].replace(" ", "") == result["postcode"].replace(" ", "")
                ):
                    duplicated_address = item
                    break
            if duplicated_address:
                self.json_response(
                    {
                        "address": {"address_uuid": duplicated_address.get("address_id")},
                        "message": "Duplicated address",
                    }
                )
            else:
                response = self.set_addresses(result, profile_uuid, address_type)
                self.json_response(response)
        else:
            self.json_response(
                {"status": "ERR", "message": "Validation error", "errors": errors},
                http.client.BAD_REQUEST,
            )


class UpdateDeleteAddressHandler(ProfileHandler):
    def get(self, address_uuid):
        self.json_response(self.get_address(address_uuid))

    def patch(self, address_uuid):
        data = tornado.escape.json_decode(self.request.body)

        result, errors = AddressSchema().load(data)

        if not errors:
            address_type = self.get_argument("is_primary", default=1)
            self.get_address(address_uuid)
            self.change_address(result, address_uuid, address_type)
            self.json_response(
                {
                    "status": "OK",
                    "message": "Address was successfully updated",
                    "address_uuid": address_uuid,
                }
            )
        else:
            self.json_response(
                {"status": "ERR", "message": "Validation error", "errors": errors},
                http.client.BAD_REQUEST,
            )

    def delete(self, address_uuid):
        self.delete_address(address_uuid)
        self.json_response(http.client.NO_CONTENT)


class PlanGivingHandler(FactFindHandler):
    def get(self, plan_uuid):
        plan = Plan.get_by_uuid(plan_uuid)
        self.result_or_404(plan, "plan")
        result = BasePlanHandler.get_giving_data(plan["plan_id"])
        self.json_response(result)

    def post(self, plan_uuid, plan_giving_uuid=None):
        data = tornado.escape.json_decode(self.request.body)
        plan = Plan.get_by_uuid(plan_uuid)
        self.result_or_404(plan, "plan")
        plan_giving = PlanGiving.get_by_uuid(plan_giving_uuid)
        id = plan_giving["plan_giving_id"] if plan_giving else None
        result, errors = GivingSchema().load(data)
        if not errors:
            giving_uuid = PlanGiving.upsert(result, plan["plan_id"], id)
            self.json_response(
                {
                    "status": "OK",
                    "message": "Plan giving was successfully added/updated",
                    "plan_giving_uuid": giving_uuid,
                }
            )
        else:
            self.json_response(
                {"status": "ERR", "message": "Validation error", "errors": errors},
                http.client.BAD_REQUEST,
            )


class PlanGivingCashFlowHandler(FactFindHandler):
    def post(self, plan_uuid, plan_giving_uuid):
        data = tornado.escape.json_decode(self.request.body)
        plan = Plan.get_by_uuid(plan_uuid)
        plan_giving = PlanGiving.get_by_uuid(plan_giving_uuid)
        self.result_or_404(plan, "plan")
        self.result_or_404(plan_giving, "plan_giving")
        result, errors = CashflowSchema().load(data)
        if not errors:
            plan_user = PlanUser.get_plan_user_by_profile_uuid(data.get("profile_uuid"))
            plan_user_id = plan_user["plan_user_id"] if plan_user else None

            plan_cashflow = PlanCashflow.upsert(plan, result, plan_user_id)
            PlanGiving.update(
                plan_giving["plan_giving_id"], plan_cashflow_id=plan_cashflow["plan_cashflow_id"]
            )
            self.json_response(
                {
                    "status": "OK",
                    "message": "Plan cashflow was successfully added",
                    "plan_cashflow_uuid": plan_cashflow["plan_cashflow_uuid"],
                },
                http.client.CREATED,
            )
        else:
            self.json_response(
                {"status": "ERR", "message": "Validation error", "errors": errors},
                http.client.BAD_REQUEST,
            )


class SubmitPlanHandler(BasePlanHandler):
    @oauth_authenticated
    def post(self, plan_uuid):
        plan = Plan.get_by_uuid(plan_uuid)
        self.result_or_404(plan, "plan")
        data = {"is_submitted": 1, "submit_dttm": datetime.utcnow()}
        plan = Plan.upsert(data, plan_uuid)
        self.json_response(
            {
                "status": "OK",
                "message": "Plan was successfully submited",
                "plan_uuid": plan["plan_uuid"],
            }
        )


class PlanHandler(BasePlanHandler):
    def get(self, plan_uuid):
        plan = Plan.get_by_uuid(plan_uuid, to_dict=False)
        self.result_or_404(plan, "plan")
        plan_id = plan.plan_id

        result = BasePlanHandler.get_plan_data(plan)

        profile = User.get_main_user_profile_uuid(plan_id=plan_id)
        result["addresses"] = []
        if profile:
            result["addresses"] = self.get_addresses_list(profile).get("addresses") or []

        self.json_response(result)

    @role_required("adviser")
    def post(self):
        data = tornado.escape.json_decode(self.request.body)
        result, errors = PlanSchema().load(data)
        if not errors:
            plan = Plan.upsert(result)
            user_uuids = []
            for user in result.get("users", []):
                user_uuids.append(self.add_user_to_plan(plan["plan_uuid"], user))
            self.json_response(
                {
                    "status": "OK",
                    "message": "Plan was successfully added",
                    "plan_uuid": plan["plan_uuid"],
                    "user_uuids": user_uuids,
                },
                http.client.CREATED,
            )
        else:
            self.json_response(
                {"status": "ERR", "message": "Validation error", "errors": errors},
                http.client.BAD_REQUEST,
            )

    def add_user_to_plan(self, plan_uuid, user_data):
        plan = Plan.get_by_uuid(plan_uuid)
        token = create_password()
        profile_id, user_id = self.create_profile_user(user_data, token=token)
        try:
            PlanUser.create_or_update_user(user_data, plan["plan_id"], profile_id, user_id)
            from utils.constants import UNIQUE_PLAN_USER_TYPES

            if user_data.get("plan_user_type") in UNIQUE_PLAN_USER_TYPES:
                from utils.constants import CLIENT_USER_TYPE

                create_user_role(user_id, CLIENT_USER_TYPE)
            return profile_id
        except Exception as e:
            return self.json_response(
                {"status": "ERR", "message": "Error with creating/updating user: ".format(str(e))},
                http.client.BAD_REQUEST,
            )

    def patch(self, plan_uuid):
        plan = Plan.get_by_uuid(plan_uuid)
        self.result_or_404(plan, "plan")
        data = tornado.escape.json_decode(self.request.body)
        result, errors = PlanSchema().load(data)
        if not errors:
            plan = Plan.upsert(result, plan_uuid)
            self.json_response(
                {
                    "status": "OK",
                    "message": "Plan was successfully updated",
                    "plan_uuid": plan["plan_uuid"],
                }
            )
        else:
            self.json_response(
                {"status": "ERR", "message": "Validation error", "errors": errors},
                http.client.BAD_REQUEST,
            )


class PlanUserHandler(BasePlanHandler):
    def get(self, plan_uuid, profile_uuid=None):
        plan = Plan.get_by_uuid(plan_uuid, to_dict=False)
        self.result_or_404(plan, "plan")
        if profile_uuid:
            user = PlanUser.get_plan_user_by_profile_uuid(profile_uuid, to_dict=False)
            self.result_or_404(user, "user")
            result = self.get_plan_user_data(user)
        else:
            result = self.get_users_data(plan.plan_id)
        schema = PlanSchema()
        result.update(schema.dump(plan).data)
        self.json_response(result)

    def post(self, plan_uuid, profile_uuid=None):
        plan = Plan.get_by_uuid(plan_uuid)
        self.result_or_404(plan, "plan")
        data = tornado.escape.json_decode(self.request.body)
        schema = UserSchema()
        schema.context = {"profile_uuid": profile_uuid}
        result, errors = schema.load(data)
        if errors:
            self.json_response(
                {"status": "OK", "message": "Client data failed validation", "errors": errors},
                http.client.BAD_REQUEST,
            )
        else:
            token = create_password()
            profile_id, user_id = self.create_profile_user(result, profile_uuid, token=token)

            try:
                plan_user_id = PlanUser.create_or_update_user(
                    result, plan["plan_id"], profile_id, user_id
                )
                from utils.constants import UNIQUE_PLAN_USER_TYPES

                if result.get("plan_user_type") in UNIQUE_PLAN_USER_TYPES:
                    from utils.constants import CLIENT_USER_TYPE

                    create_user_role(user_id, CLIENT_USER_TYPE)
                plan_user_cashflow = result.get("plan_user_cashflow", {})
                if plan_user_cashflow:
                    PlanCashflow.upsert(plan, plan_user_cashflow, plan_user_id)
            except Exception as e:
                return self.json_response(
                    {"status": "ERR", "message": "Error with creating/updating user: {}".format(e)},
                    http.client.BAD_REQUEST,
                )
            else:
                return self.json_response(
                    {"profile_uuid": profile_id, "status": "OK", "pwd": token}, http.client.OK
                )


class CurrentPlanHandler(BasePlanHandler):
    @oauth_authenticated
    def get(self):

        plans, active_plan = self.get_user_plans(self.current_user["profile_id"])
        plan = Plan.get_by_uuid(active_plan, to_dict=False)
        self.result_or_404(active_plan, "plan")
        result = BasePlanHandler.get_plan_data(plan)

        profile = User.get_main_user_profile_uuid(plan_id=plan.plan_id)
        result["addresses"] = []
        if profile:
            result["addresses"] = self.get_addresses_list(profile).get("addresses") or []

        self.json_response(result)


class AcceptTermsHandler(BasePlanHandler):
    @oauth_authenticated
    def get(self, profile_uuid):
        user = User.get_by_uuid(profile_uuid)
        TermsAndConditionAccept.create(**{"who_accept_user_id": user.user_id})
        self.json_response({"status": "OK"})


class UploadFileHandler(BasePlanHandler):
    @oauth_authenticated
    def get(self, plan_uuid, asset_uuid=None):
        plan = Plan.get_by_uuid(plan_uuid)
        self.result_or_404(plan, "plan")
        if asset_uuid:
            asset = PlanAsset.get_by_uuid(asset_uuid)
            self.result_or_404(asset, "asset")
            documents = PlanDocument.query.filter_by(
                asset_id=asset["plan_asset_id"], plan_id=plan["plan_id"]
            )
        else:
            documents = PlanDocument.query.filter_by(plan_id=plan["plan_id"])
        schema = PlanDocumentSchema(many=True)
        result = schema.dump(documents).data
        self.json_response(result)

    @oauth_authenticated
    def post(self, plan_uuid):
        plan = Plan.get_by_uuid(plan_uuid)
        self.result_or_404(plan, "plan")
        files = []
        try:
            files = self.request.files
        except Exception:
            pass

        plan_document_uuids = []
        if files:
            for file in files.values():
                filename = file[0]["filename"]
                type = file[0]["content_type"]
                body = file[0]["body"]
                plan_document_uuid = uuid.uuid4()
                size = len(body)
                filename = plan["plan_uuid"] + "/" + filename
                public_url = "{}/api/v1/documents/{}/download".format(
                    options.root_url, plan_document_uuid
                )
                data = {
                    "plan_document_uuid": plan_document_uuid,
                    "type": type,
                    # not sure about url
                    "url": public_url,
                    "filename": filename,
                    "size": size,
                    "plan_id": plan["plan_id"],
                }
                if not options.debug_mode:
                    upload_blob(body, filename, type)
                PlanDocument.create(**data)
                plan_document_uuids.append(str(plan_document_uuid))

        self.json_response(
            {
                "status": "OK",
                "message": "Document successfully upload to Google Storage",
                "plan_uuid": plan["plan_uuid"],
                "plan_document_uuids": plan_document_uuids,
            }
        )
        self.set_status(204)


class DownloadFileHandler(BasePlanHandler):
    @oauth_authenticated
    def get(self, plan_document_uuid, type="show"):
        document = PlanDocument.get_by_uuid(plan_document_uuid)
        self.result_or_404(document, "document")
        if options.debug_mode:
            self.set_header("Content-Type", "image/jpeg")
            file = urllib.request.urlopen(
                "https://www.elastic.co/assets/bltada7771f270d08f6/enhanced-buzz-1492-1379411828-15.jpg"
            ).read()
        else:
            file = download_blob(document["filename"])
            self.set_header("Content-Type", document["type"])
        if type == "download":
            disposition = "attachment"
        else:
            disposition = "inline"
        self.set_header(
            "Content-Disposition", "{}; filename={}".format(disposition, document["filename"])
        )
        self.write(file)
        self.finish()

    @oauth_authenticated
    def delete(self, plan_document_uuid):
        document = PlanDocument.get_by_uuid(plan_document_uuid)
        self.result_or_404(document, "document")
        if not options.debug_mode:
            delete_blob(document["filename"])
        PlanDocument.delete(document["plan_document_id"])
        self.json_response(
            {
                "status": "OK",
                "message": "Document successfully deleted from Google Storage",
                "plan_document_uuid": str(plan_document_uuid),
            }
        )
