from http.client import UNPROCESSABLE_ENTITY
import json
import traceback

import tornado
from tornado.options import options

from decorators.auth_decorators import oauth_authenticated
from libraries.profile import ProfileClient
from models import model
from web_handlers.profile import ProfileHandler


def verify_amlkyc(self, profile, profile_uuid):
    """
    - `type` can be either 'webhook' or 'slack'. then `to` can either be full url
    'https://octopuswealth.....' or '@slack_user'.
    - `notify_on_status` only sends webhook on specific responses [pass, fail, refer].
    if you do not send this field, webhooks will be sent for all responses.
    - `is_enabled` is false by default and will not run any checks until you set it to true,
    but it will save the case data on our side. this is because some registration flows
    first collect user data, and at the end ask them to accept terms and conditions
    (and we can't run any checks until those are accepted)
    - `custom_case_id` is required by some projects to add their own custom reference to a case
    """

    # we only accept one address, because the 3rd party providers we use also only check one.
    # it's up to you to decide which one to send.
    addresses = [
        ProfileClient.get_address(address) for address in profile.get("addresses", []) if address
    ]
    if len(addresses) > 0:
        address = addresses[0]
    else:
        address = {}

    data = {
        "person": {
            "first_name": profile.get("first_name"),
            "middle_name": profile.get("middle_name", ""),
            "last_name": profile.get("last_name"),
            "additional_citizenships": profile.get("citizenships"),
            "dob": profile.get("dob"),
            "country_citizenship": profile.get("citizen_state_cd"),
            "country_birth": profile.get("birth_country_cd"),
        },
        "address": {
            "line1": address.get("address_1", ""),
            "line2": address.get("address_2", ""),
            "postcode": address.get("postcode", ""),
            "country": address.get("country_cd", ""),
        },
        "webhooks": [
            {
                "type": "webhook",
                "to": "{}{}/aml/{}/check".format(
                    options.root_url.replace("http:", "https:"), options.api_version, profile_uuid
                ),
            }
        ],
        # flag to run the checks
        "is_enabled": True,
        # fact find platform id
        # TODO: fact find id of 21 returns "The selected platform id is invalid."
        "platform_id": 1,  # options.platform_id,
    }

    res = ProfileClient.authenticated_request(
        options.amlkyc_url + "/cases",
        data=data,
        method="POST",
        response_errors=[UNPROCESSABLE_ENTITY],
    )
    # case of http 422: error in field validation
    if isinstance(res, dict) and "response" in res:
        # in case the data sent to the aml/kyc api is invalid/empty
        return {
            "status": "ERR",
            "message": "AML/KYC validation error.",
            "fields": json.loads(res["response"]),
        }
    return res


def save_results(self, case, data):
    if len(data["results"]) == 0:
        self.json_error(
            {"message": "Unprocessable response received.", "status": "ERR", "error": str(data)}
        )
        return False

    # makes a list of pass/fail results to ignore updates on them
    # since they're solved/ended verifications
    concludeds = model.AmlResult.concludeds(case.case_id)

    for result in data["results"]:
        # ignores pass/fail results (no news are supposed to come)
        if result["status"] in concludeds:
            continue

        bank_account = result.get("bank_account_verification")
        if bank_account is not None:
            del result["bank_account_verification"]

        result["case_id"] = case.case_id

        # converts all keys to lowercase
        for key in result.keys():
            short = key.lower()
            if key != short:
                result[short] = result[key]
                del result[key]
        # TODO: put the correct values for trigger key
        result["trigger"] = "Created"
        res = model.AmlResult(**result)
        self.session.add(res)
        self.session.commit()

        if bank_account is not None and len(bank_account) > 0:
            bank_account["result_id"] = res.result_id
            bank = res.bank_account_verification
            if bank is None:
                bank = model.AmlResultBankAccount(**bank_account)
            self.session.add(bank)
            self.session.commit()

    return True


def save_response_data(self, profile_uuid, data):
    if data.get("status", "").lower() in ("error", "err"):
        self.json_error(data)
        return False
    if "case" in data:
        data = data["case"]

    try:
        if "id" not in data:
            self.json_error(
                {"message": "Unprocessable response received.", "status": "ERR", "error": str(data)}
            )
            return False
        data["user_id"] = self.get_user(profile_uuid).user_id
        data["case_id"] = data["id"]
        try:
            case = model.AmlCase.upsert({"case_id": data["case_id"]}, data)
        except Exception as e:
            data["error"] = str(e)
            self.json_response(data)
            return False

        # upsert the documents sent
        if len(data["documents"]) > 0:
            for doc in data["documents"]:
                model.AmlDocument.upsert({"document_id": doc["document_id"]}, doc)

        # insert the results from the webhook/response
        return save_results(self, case, data)

    except Exception:
        self.json_error(
            {
                "message": "Unexpected error happened while processing the aml webhook.",
                "error": str(traceback.format_exc()) + "\n" + str(data),
                "status": "ERR",
            }
        )
        return False


class AmlWebhookHandler(ProfileHandler):
    """
    AML/KYC: Anti Money Laundering / Know Your Customer API

    Handler to send the user profile data to AML api and store the responses.
    URL: r"/api/v1/aml/(?P<profile_uuid>[-\w]+)?/webhook"
    """

    def post(self, profile_uuid):
        """
        Endpoint that receives and save the webhook response.
        """
        try:
            data = tornado.escape.json_decode(self.request.body)
            if "data" in data:
                data = data["data"]
        except Exception:
            self.json_error("Invalid json data received from aml webhook.")
            return

        if save_response_data(self, profile_uuid, data):
            self.json_response({"status": "OK", "message": "Webhook call received and data saved."})


class AmlCheckHandler(ProfileHandler):
    """
    AML/KYC: Anti Money Laundering / Know Your Customer API

    Handler to send the user profile data to AML api and store the responses.
    URL: r"/api/v1/aml/(?P<profile_uuid>[-\w]+)?/check"
    """

    def is_running(self, profile_uuid):
        # check if an aml/kyc check is already running
        # if the newest aml/kyx check is still pending, blocks the user of starting a new one
        if not model.AmlCase.is_newest_concluded(self.get_user(profile_uuid).user_id):
            return {"status": "ERR", "message": "AML/KYC check is already in progress."}
        return None

    def fetch_data_from_profile(self, profile_uuid):
        profile = ProfileClient.get(profile_uuid)
        if profile.get("status") in ("error", "ERR"):
            return profile
        if "profile" in profile:
            profile = profile["profile"]
        if "person" in profile:
            profile = profile["person"]
        return profile

    @oauth_authenticated
    def get(self, profile_uuid):
        """
        Endpoint that triggers the the aml/kyc checking (fetches data from the profile api).
        """

        r = self.is_running(profile_uuid)
        if r:
            self.json_response(r)
            return

        profile = self.fetch_data_from_profile(profile_uuid)
        data = verify_amlkyc(profile, profile_uuid)
        if save_response_data(self, profile_uuid, data):
            self.json_response({"status": "OK", "message": "Response received and data saved."})

    @oauth_authenticated
    def post(self, profile_uuid):
        """
        Endpoint that triggers the the aml/kyc checking (receives the data).
        """

        r = self.is_running(profile_uuid)
        if r:
            self.json_response(r)
            return

        profile = tornado.escape.json_decode(self.request.body)
        data = verify_amlkyc(self, profile, profile_uuid)
        if save_response_data(self, profile_uuid, data):
            self.json_response({"status": "OK", "message": "Response received and data saved."})


class AmlCasesHandler:
    """
    Endpoint to list all aml cases related to the provided profile.
    URL: r"/api/v1/aml/(?P<profile_uuid>[-\w]+)?/cases"
    """

    @oauth_authenticated
    def get(self, profile_uuid):
        cases = model.AmlCase.query.filter(model.AmlCase.profile_uuid == profile_uuid)
        if cases.count() == 0:
            self.json_error("No aml cases found for this profile.")
        else:
            cases = cases.order_by(model.AmlCase.updated.desc())
            self.json_response(
                {"data": [{"id": case.case_id, "updated": case.updated} for case in cases]}
            )


class AmlStatusHandler:
    """
    Endpoint to return the status of the three checks of and aml case.
    URL: r"/api/v1/aml/(?P<case_id>[-\w]+)?/status"
    """

    @oauth_authenticated
    def get(self, case_id):
        cases = model.AmlCase.query.filter(model.AmlCase.case_id == case_id)
        if cases.count() == 0:
            self.json_error("Aml case not found.")
        else:
            self.json_response(model.AmlCase.newest_results(case_id=case_id))


class AmlNewestCaseHandler:
    """
    Endpoint to return the newest aml case of the provided profile.
    URL: r"/api/v1/aml/(?P<profile_uuid>[-\w]+)?/newest_case"
    """

    @oauth_authenticated
    def get(self, profile_uuid):
        cases = model.AmlCase.query.filter(model.AmlCase.profile_uuid == profile_uuid)
        if cases.count() == 0:
            self.json_error("No aml cases found for this profile.")
        else:
            cases = cases.order_by(model.AmlCase.updated.desc()).first()
            AmlCaseSchema = model.AmlCase.__marshmallow__()
            self.json_response({"data": [AmlCaseSchema.dump(case).data for case in cases]})


class AmlResultsHandler:
    """
    Endpoint to return the aml case results from an aml case.
    URL: r"/api/v1/aml/(?P<case_id>[-\w]+)?/results"
    """

    @oauth_authenticated
    def get(self, case_id):
        results = model.AmlResult.query.filter(model.AmlResult.case_id == case_id)
        if results.count() == 0:
            self.json_error("No aml results found for this case.")
        else:
            results = results.order_by(model.AmlResult.updated.desc()).first()
            AmlResultSchema = model.AmlResult.__marshmallow__()
            self.json_response({"data": [AmlResultSchema.dump(result).data for result in results]})


class AmlDocumentsHandler:
    """
    Endpoint to return the aml case documents from an aml case.
    URL: r"/api/v1/aml/(?P<case_id>[-\w]+)?/documents"
    """

    @oauth_authenticated
    def get(self, case_id):
        docs = model.AmlDocument.query.filter(model.AmlDocument.case_id == case_id)
        if docs.count() == 0:
            self.json_error("No aml documents found for this case.")
        else:
            docs = docs.order_by(model.AmlDocument.updated.desc()).first()
            AmlDocumentSchema = model.AmlDocument.__marshmallow__()
            self.json_response({"data": [AmlDocumentSchema.dump(doc).data for doc in docs]})
