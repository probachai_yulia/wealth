import json
import logging
from datetime import datetime

import google.oauth2.credentials
import google.oauth2.service_account
import googleapiclient.discovery
import googleapiclient.errors

# external imports
import tornado
import tornado.escape
import tornado.httpclient
import tornado.web
from tornado.options import options

API_SERVICE_NAME = "sheets"
API_VERSION = "v4"

SPREADSHEET_TITLE = "Presentation"
VALUE_INPUT_OPTION = "USER_ENTERED"


class PresentationHandler(tornado.web.RequestHandler):
    def post(self):
        data = tornado.escape.json_decode(self.request.body)

        action = data["action"]
        action_handlers = {
            "saveData": self.save_data,
            "firstSave": self.create_initial,
            "updateData": self.update_data,
            "updateConfig": self.update_config,
            "saveClient": self.save_client,
        }

        return action_handlers[action](data)

    def save_client(self, data):
        sheets = get_sheets(data["access_token"])
        spreadsheet_id = data["id_googleSheet"]

        spreadsheet = self.find_sheet(sheets, spreadsheet_id)
        if spreadsheet:
            write_range = f"{SPREADSHEET_TITLE}!A1:C1"
            values = [[data["client"], data["partner"], data["advisor"]]]
            request = (
                sheets.spreadsheets()
                .values()
                .update(
                    spreadsheetId=spreadsheet_id,
                    range=write_range,
                    body={"values": values, "range": write_range},
                    valueInputOption=VALUE_INPUT_OPTION,
                )
            )
            request.execute()

    def update_data(self, data):
        sheets = get_sheets(data["access_token"])
        spreadsheet_id = data["id_googleSheet"]
        values = data["allData"]

        write_range = f"{SPREADSHEET_TITLE}!A:ZZ"
        request = (
            sheets.spreadsheets()
            .values()
            .clear(spreadsheetId=spreadsheet_id, range=write_range, body={})
        )
        request.execute()

        request = (
            sheets.spreadsheets()
            .values()
            .update(
                spreadsheetId=spreadsheet_id,
                range=write_range,
                body={"values": values, "range": write_range},
                valueInputOption=VALUE_INPUT_OPTION,
            )
        )
        request.execute()

    def update_config(self, data):
        sheets = get_sheets(data["access_token"])
        spreadsheet_id = data["id_googleSheet"]
        filename = data["file"]
        values = data["allData"]

        write_range = f"{filename}!A:ZZ"
        request = (
            sheets.spreadsheets()
            .values()
            .clear(spreadsheetId=spreadsheet_id, range=write_range, body={})
        )
        request.execute()

        request = (
            sheets.spreadsheets()
            .values()
            .update(
                spreadsheetId=spreadsheet_id,
                range=write_range,
                body={"values": values, "range": write_range},
                valueInputOption=VALUE_INPUT_OPTION,
            )
        )
        request.execute()

    def create_initial(self, data):
        sheets = get_sheets(data["access_token"])
        spreadsheet_id = data["id_googleSheet"]
        body = {"requests": [{"addSheet": {"properties": {"title": "Presentation"}}}]}
        try:
            response = (
                sheets.spreadsheets().batchUpdate(spreadsheetId=spreadsheet_id, body=body).execute()
            )
            logging.info(repr(response))
        except googleapiclient.errors.HttpError as e:
            logging.exception(e)
            logging.info("Spreadsheet already exists")

        partner_name = " & " + data.get("partnerName", "") if data.get("partnerName", "") else ""
        file_path = (
            options.default_presentation_path
            if data["load_presentation"] == "complete"
            else options.default_presentation_reduced_path
        )
        with open(file_path, "r", encoding="utf-8") as filehandle:
            sheet_content = filehandle.read()

        replacements = [
            ("POST_id_googleSheet", spreadsheet_id),
            ("POST_clientName", data["clientName"]),
            ("POST_partnerName", data.get("partnerName", "")),
            ("POST_advisor", data["advisor"]),
            ("VAR_partnerName", partner_name),
            ("POST_gender_client", data["gender_client"]),
            ("POST_gender_partner", data["gender_partner"]),
        ]
        for replacement in replacements:
            sheet_content = sheet_content.replace(*replacement)

        sheet_content = json.loads(sheet_content)

        write_range = "Presentation!A:AO"
        request = (
            sheets.spreadsheets()
            .values()
            .update(
                spreadsheetId=data["id_googleSheet"],
                range=write_range,
                body={"values": sheet_content, "range": write_range},
                valueInputOption=VALUE_INPUT_OPTION,
            )
        )
        updated = request.execute()
        logging.info(repr(updated))

        return

    def save_data(self, data):
        sheets = get_sheets(data["access_token"])
        spreadsheet_id = data["id_googleSheet"]
        spreadsheet = self.find_sheet(sheets, spreadsheet_id)

        if spreadsheet:
            request_range = f"{SPREADSHEET_TITLE}!A:CE"
            returned_sheet = sheets.spreadsheets().values().get(spreadsheet_id, range=request_range)
            logging.info(repr(returned_sheet))
            values = returned_sheet.values

            order = data["order"] if data["order"] else len(values)
            count = len(values) + 1
            to_write = [
                [
                    spreadsheet_id,
                    data["dataType"],
                    data["sheetPage"],
                    data["sheetPageCompare"],
                    data["chartsType"],
                    data["title"],
                    data["titleCompare"],
                    data["label1"],
                    data["label2"],
                    data["label3"],
                    data["label4"],
                    data["label5"],
                    data["color1"],
                    data["color2"],
                    data["color3"],
                    data["color4"],
                    data["color5"],
                    data["chartCompareType"],
                    data["icon"],
                    data["edit_label"],
                    data["edit_position"],
                    data["edit_position_x"],
                    data["edit_position_dot"],
                    data["edit_position_dot_x"],
                    data["edit_axis"],
                    data["edit_line"],
                    data["ihta"],
                    data["ihtb"],
                    data["childA"],
                    data["label_area"],
                    data["year"],
                    data["octopus_carib"],
                    data["competition"],
                    data["assumes_portfolio"],
                    order,
                    "0",
                    data["note"],
                ]
            ]

            write_range = f"{SPREADSHEET_TITLE}!A{count}:AO{count}"
            updated = (
                sheets.spreadsheets()
                .values()
                .update(
                    spreadsheetId=spreadsheet_id,
                    range=write_range,
                    body={"values": to_write, "range": write_range},
                    valueInputOption=VALUE_INPUT_OPTION,
                )
            )

            logging.info(repr(updated))

            return

    @staticmethod
    def find_sheet(sheets, spreadsheet_id):
        spreadsheets = sheets.spreadsheets().get(spreadsheet_id)
        logging.info(repr(spreadsheets))
        for spreadsheet in spreadsheets:
            sheet_found = SPREADSHEET_TITLE in spreadsheet["properties"]["title"]
            if sheet_found:
                return spreadsheet


class ContactHandler(tornado.web.RequestHandler):
    CONTACT_FORM_SPREADSHEET_ID = "14QZw0Ox8UDy-Qo8NwpHmzv6HywamusGI42wtwU20ZwM"

    def post(self):
        data = tornado.escape.json_decode(self.request.body)

        sheets = get_sheets(use_service_account=True)
        spreadsheet_id = self.CONTACT_FORM_SPREADSHEET_ID

        read_range = "Sheet1!A:G"
        request = sheets.spreadsheets().values().get(spreadsheetId=spreadsheet_id, range=read_range)
        existing_rows = request.execute()["values"]
        new_row = len(existing_rows) + 1

        write_range = f"Sheet1!A{new_row}:G{new_row}"

        values = [
            [
                data["name"],
                data["last_name"],
                data["phone"],
                data["email"],
                data["contact_by_email"],
                f"{datetime.utcnow():%Y-%m-%d %H:%M:%S %z}",
            ]
        ]

        request = (
            sheets.spreadsheets()
            .values()
            .update(
                spreadsheetId=spreadsheet_id,
                range=write_range,
                body={"values": values, "range": write_range},
                valueInputOption=VALUE_INPUT_OPTION,
            )
        )
        request.execute()


def get_sheets(access_token=None, use_service_account=False):
    return googleapiclient.discovery.build(
        API_SERVICE_NAME,
        API_VERSION,
        credentials=get_client_credentials(access_token, use_service_account=use_service_account),
    )


def get_client_credentials(access_token, use_service_account):
    secret_key = options.gapi_secret_key
    scopes = options.gapi_scopes
    params = {"client_secret": secret_key, "scopes": scopes}
    if use_service_account:
        params["type"] = options.gapi_type
        params["project_id"] = options.gapi_project_id
        params["private_key_id"] = options.gapi_private_key_id
        params["private_key"] = options.gapi_private_key
        params["client_email"] = options.gapi_client_email
        params["client_id"] = options.gapi_client_id
        params["auth_uri"] = options.gapi_auth_uri
        params["token_uri"] = options.gapi_token_uri
        params["auth_provider_x509_cert_url"] = options.gapi_auth_provider_x509_cert_url
        params["client_x509_cert_url"] = options.gapi_client_x509_cert_url
        return google.oauth2.service_account.Credentials.from_service_account_info(params)
    else:
        params["token"] = access_token
        return google.oauth2.credentials.Credentials(**params)
