from models.model import (
    AmlCase,
    PlanGiving,
    PlanUser,
    PlanUserJob,
    RegularContribution,
    TermsAndConditionAccept,
    User,
)
from utils.plan_utils import reformat_plan_user_list
from validation.dump_schemes import (
    FullPlanSchema,
    PlanAssetSchema,
    PlanCashflowSchema,
    PlanDebtSchema,
    PlanGivingSchema,
    PlanRegularContributionSchema,
    PlanUserJobSchema,
    PlanUserSchema,
    PlanUserDetailsSchema,
)
from web_handlers.profile import ProfileHandler


class BasePlanHandler(ProfileHandler):
    def get_users_data(self, plan_id):
        plan_users = PlanUser.query.filter_by(plan_id=plan_id)
        schema = PlanUserSchema(many=True)
        users_data = schema.dump(plan_users).data
        result = reformat_plan_user_list(users_data)
        return result

    def get_plan_user_data(self, user):
        schema = PlanUserSchema()
        result = schema.dump(user).data
        return result

    def get_user_details(self, profile_id):
        profile = self.get_profile(profile_id)
        result = profile
        user = User.get_by_uuid(profile_id)
        user_id = user.user_id
        plan_user = PlanUser.query.filter_by(user_id=user_id).first()
        if plan_user:
            result["plan_user"] = PlanUserDetailsSchema().dump(plan_user).data
        result["user_type"] = User.get_user_role(user_id)
        result["permissions"] = User.get_permissions_by_user_id(user_id) if user_id else []

        plans_list, active_plan = self.get_user_plans(profile_id)
        result["plans"] = [plan["plan_uuid"] for plan in plans_list]
        result["connected_persons"] = {}
        for plan in plans_list:
            result["connected_persons"][plan["plan_uuid"]] = self.get_users_data(plan["plan_id"])

        result["active_plan"] = active_plan
        result["check_status"] = AmlCase.newest_results(user_id=user_id)
        terms_and_conditions = (
            TermsAndConditionAccept.query.filter_by(who_accept_user_id=user_id)
            .order_by("create_dttm")
            .first()
        )
        result["terms_of_business_date"] = (
            terms_and_conditions.create_dttm if terms_and_conditions else None
        )
        return result

    @staticmethod
    def get_giving_data(plan_id):
        givings = PlanGiving.query.filter_by(plan_id=plan_id).all()
        schema = PlanGivingSchema(many=True)
        result = schema.dump(givings).data
        return result

    @staticmethod
    def get_regular_contribution_data(plan_asset_id):
        regular_contributions = RegularContribution.query.filter_by(
            plan_asset_id=plan_asset_id
        ).all()
        schema = PlanRegularContributionSchema(many=True)
        result = schema.dump(regular_contributions).data
        return result

    @staticmethod
    def get_cashflow_data(query):
        schema = PlanCashflowSchema(many=True)
        result = schema.dump(query).data
        return result

    @staticmethod
    def get_user_jobs(plan_user_id):
        jobs = PlanUserJob.query.filter_by(plan_user_id=plan_user_id).all()
        schema = PlanUserJobSchema(many=True)
        result = schema.dump(jobs).data
        return result

    @staticmethod
    def get_plan_data(plan):
        schema = FullPlanSchema()
        result = schema.dump(plan).data
        return result

    @staticmethod
    def get_debt_data(query):
        schema = PlanDebtSchema(many=True)
        result = schema.dump(query).data
        return result

    @staticmethod
    def get_asset_data(query):
        schema = PlanAssetSchema(many=True)
        result = schema.dump(query).data
        return result
