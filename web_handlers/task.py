import web
import libraries.schema
from commands.helpers import create_dev_user, truncate_all_tables
from decorators.decorators import develop_tool, fact_find_enabled


class TaskHandler(web.PageHandler):
    """Base Handler to trigger back end tasks"""

    @fact_find_enabled
    def prepare(self):
        return super(TaskHandler, self).prepare()


class MigrateHandler(TaskHandler):
    """Handler to run database migrations"""

    def get(self, revision="head"):
        """Triggers alembic show a database update to the given version"""
        lines, sql = libraries.schema.migrate(revision, sql_mode=True)
        return self.json_response(data={"lines": lines, "sql": sql})

    def post(self, revision="head"):
        """Triggers alembic to update the database to the given version"""
        lines, _ = libraries.schema.migrate(revision, sql_mode=False)
        return self.json_response(data={"lines": lines})


class RollbackHandler(TaskHandler):
    """Handler to run database rollbacks"""

    def get(self, revision="head"):
        """Triggers alembic to show a database revert to the given version"""
        lines, sql = libraries.schema.rollback("head:" + revision, sql_mode=True)
        return self.json_response(data={"lines": lines, "sql": sql})

    def post(self, revision="head"):
        """Triggers alembic to revert the database to the given version"""
        lines, _ = libraries.schema.rollback(revision, sql_mode=False)
        return self.json_response(data={"lines": lines})


class MigrationHistoryHandler(TaskHandler):
    """Handler to view database migrations"""

    def get(self):
        """Returns list of database migrations"""
        return self.json_response(data={"revisions": libraries.schema.history()})


class DevUserHandler(TaskHandler):
    """Handler to create dev user"""

    @develop_tool
    def get(self):
        user_type = self.get_argument("user_type", default="")
        email = self.get_argument("email", default="")
        res = create_dev_user(email, user_type)
        return self.json_response(res)


class ClearDatabaseHandler(TaskHandler):
    """Handler to clear plan tables and related ones"""

    @develop_tool
    def get(self):

        truncate_all_tables()
        return self.json_response({"status": "OK", "message": "Truncated all plan tables"})
