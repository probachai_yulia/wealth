import http.client

# Octopus libraries
import tornado
import tornado.escape

# external imports
import tornado.httpclient
from tornado.options import options

from decorators.auth_decorators import oauth_authenticated, role_required
from libraries import utils
from models.model import Role, User, UserRole
from utils.constants import ADVISER_USER_TYPE, CLIENT_USER_TYPE
from utils.pagination import Page
from utils.plan_utils import create_plan, create_password
from validation.validation_schemes import PasswordSchema, UserSchema

# match esbaccess.octopusinvestments.com to its IP
# this is the only way to add custom DNS records in GAE
from web_handlers.Base import BasePlanHandler
from web_handlers.profile import ProfileHandler


# Standard libraries
# Third party libraries


class BaseUserHandler(BasePlanHandler):
    def get_users_list(self, profiles_users, local_users):
        users_list = []
        for user in profiles_users:
            local_user = User.get_by_uuid(user["profile_id"])
            if local_user in local_users:
                curr_user = User.get_by_id(local_user.user_id)
                curr_user.update(self.get_user_details(curr_user["profile_uuid"]))
                curr_user["link"] = "{}{}/users/{}".format(
                    options.root_url, options.api_version, curr_user["profile_uuid"]
                )
                users_list.append(curr_user)
        return users_list

    def search_users(self, local_users, page, per_page):
        result = Page.paginate(local_users, page, per_page)
        profiles_users = self.get_profiles_list()["company"]["profiles"]
        users = self.get_users_list(profiles_users, result["items"])
        result["items"] = users
        return result


class UserRoleHandler(BaseUserHandler):
    # only 3 type of users can see list of all users
    @role_required(["admin", "wealth_specialist", "paraplanner", "adviser"], include_all=True)
    def get(self, user_type=None):
        user_id = utils.get_user_id(self.current_user)
        page = self.get_argument("page", 1)
        per_page = self.get_argument("per_page", options.per_page)

        if ADVISER_USER_TYPE == User.get_user_role(user_id):
            profiles = self.get_related_users(
                self.current_user["profile_id"], options.adviser_relation_type_id, "ascending"
            )["relationships"]
            profiles_list = [profile["profile_id"] for profile in profiles]
            all_users = User.get_adviser_clients(profiles_list)
        elif user_type:
            all_users = User.get_users_by_role(user_type)
        else:
            all_users = User.get_users_with_role()
        # get params from url request
        users_list = self.search_users(all_users, int(page), int(per_page))
        self.json_response(users_list)

    # only admin can create planners/specialists/advisers
    @role_required("admin")
    def post(self, user_type="client"):
        data = tornado.escape.json_decode(self.request.body)
        schema = UserSchema()
        result, errors = schema.load(data)
        if errors:
            self.json_response(
                {"status": "OK", "message": "Data failed validation", "errors": errors},
                http.client.BAD_REQUEST,
            )
        else:
            response = {}
            role = Role.query.filter_by(name=user_type).first()
            self.result_or_404(role, "role")
            token = create_password()
            profile_id, user_id = self.create_profile_user(result, token=token)
            UserRole.clear_table(user_id=user_id)
            UserRole.create(role_id=role.role_id, user_id=user_id)
            if user_type == CLIENT_USER_TYPE:
                plan = create_plan(profile_id, user_id)
                response = {"plan_uuid": plan["plan_uuid"]}
            response.update(
                {
                    "profile_uuid": profile_id,
                    "user_role": user_type,
                    "status": "OK",
                    "message": "User successfully added",
                }
            )
            self.json_response(response, http.client.CREATED)


class UserHandler(BaseUserHandler):
    def get(self, profile_uuid):
        if profile_uuid:
            if User.get_by_uuid(profile_uuid):
                self.json_response(self.get_user_details(profile_uuid))
            self.json_response(
                {
                    "status": "ERR",
                    "message": "Sorry, user exists on Profile, but doesn't exist in the local db",
                    "user": self.get_profile(profile_uuid),
                }
            )

    def patch(self, profile_uuid):
        data = tornado.escape.json_decode(self.request.body)
        schema = UserSchema()
        schema.context = {"profile_uuid": profile_uuid}
        result, errors = schema.load(data)
        if errors:
            self.json_response(
                {"status": "OK", "message": "Data failed validation", "errors": errors},
                http.client.BAD_REQUEST,
            )
        else:
            profile_id, user_id = self.create_profile_user(result, profile_uuid)
            if "role" in data.keys():
                role = Role.query.filter_by(name=data["role"]).first()
                self.result_or_404(role, "role")
                UserRole.clear_table(user_id=user_id)
                UserRole.create(role_id=role[0]["role_id"], user_id=user_id)
            self.set_relation(options.company_profile_id, options.firm_relation_type_id, profile_id)
            self.json_response(
                {
                    "profile_uuid": profile_id,
                    "user_role": data.get("role"),
                    "status": "OK",
                    "message": "User successfully added",
                },
                http.client.CREATED,
            )

    def delete(self, profile_uuid):
        user = User.get_by_uuid(profile_uuid)
        self.delete_profile(profile_uuid)
        User.delete(user)
        self.json_response(
            {"profile_uuid": profile_uuid, "status": "OK", "message": "User successfully deleted"},
            http.client.OK,
        )


class ChangeUserEmailHandler(ProfileHandler):
    @oauth_authenticated
    @role_required("admin")
    def post(self, profile_uuid):
        data = tornado.escape.json_decode(self.request.body)
        self.user_or_404(profile_uuid)
        self.update_email(profile_uuid, data)
        self.json_response(
            {"status": "OK", "message": "Email successfully changed", "profile_uuid": profile_uuid},
            http.client.OK,
        )


class CurrentUserHandler(BaseUserHandler):
    """
    Handler that provides all the data about the current user.
    URL: r"/api/v1/users/me"
    """

    @oauth_authenticated
    def get(self):
        self.json_response(self.get_user_details(self.current_user["profile_id"]))

    @oauth_authenticated
    def post(self):
        data = tornado.escape.json_decode(self.request.body)
        user_id = utils.get_user_id(self.current_user)
        schema = UserSchema()
        schema.context = {"profile_uuid": self.current_user["profile_id"]}
        result, errors = schema.load(data)
        if errors:
            self.json_response(
                {"status": "OK", "message": "Data failed validation", "errors": errors},
                http.client.BAD_REQUEST,
            )
        else:
            profile_uuid = self.current_user["profile_id"]
            profile_id, user_id = self.create_profile_user(result, profile_uuid)
            self.json_response(
                {"profile_uuid": profile_id, "status": "OK", "message": "User successfully added"},
                http.client.CREATED,
            )

        self.json_response(result)


class ChangeUserPasswordHandler(ProfileHandler):
    @oauth_authenticated
    def put(self):
        data = tornado.escape.json_decode(self.request.body)

        result, errors = PasswordSchema().load(data)
        if errors:
            self.json_response(
                {"status": "OK", "message": "Password data failed validation", "errors": errors},
                http.client.BAD_REQUEST,
            )
        else:
            profile = self.current_user
            self.change_password(profile["profile_id"], result["password"], result["new_password"])
            self.json_response(
                {"status": "OK", "message": "Password successfully changed"}, http.client.OK
            )


class SetUserPasswordHandler(ProfileHandler):
    def post(self, profile_uuid):

        try:
            self.user_or_404(profile_uuid)
            profile = self.get_profile(profile_uuid)
            email = profile["profile"]["email"]
            self.set_password(email)
        except Exception as e:
            self.json_response({"error": str(e), "status": "ERR"}, http.client.BAD_REQUEST)
        else:
            self.json_response(
                {
                    "profile_uuid": profile_uuid,
                    "email": email,
                    "status": "OK",
                    "message": "Sent email with setting password",
                },
                http.client.OK,
            )

    def put(self, profile_uuid):
        self.user_or_404(profile_uuid)
        data = tornado.escape.json_decode(self.request.body)

        result, errors = PasswordSchema().load(data)
        user = User.get_by_uuid(profile_uuid)
        if not errors and result["password"] == user["token"]:
            self.change_password(profile_uuid, result["password"], result["new_password"])
            self.json_response(
                {"status": "OK", "message": "Password successfully changed"}, http.client.OK
            )
        else:
            self.json_response(
                {"status": "OK", "message": "Password data failed validation", "errors": errors},
                http.client.BAD_REQUEST,
            )


class SetClientToAdviserHandler(ProfileHandler):
    def post(self, adviser_profile_uuid, client_profile_uuid):
        self.set_related_user(
            client_profile_uuid, adviser_profile_uuid, options.adviser_relation_type_id
        )


class SetAdviserToParaplannerHandler(ProfileHandler):
    def post(self, paraplanner_profile_uuid, adviser_profile_uuid):
        self.set_related_user(
            paraplanner_profile_uuid, adviser_profile_uuid, options.paraplanner_relation_type_id
        )


class GetAdviserClientsHandler(BaseUserHandler):
    def get(self, profile_uuid):
        self.user_or_404(profile_uuid)
        users = self.get_related_users(profile_uuid, options.adviser_relation_type_id, "descending")

        users_list = self.search_users(users)
        self.json_response(users_list)


class GetParaplannerAdvisersHandler(BaseUserHandler):
    def get(self, profile_uuid):
        self.user_or_404(profile_uuid)
        users = self.get_related_users(
            profile_uuid, options.paraplanner_relation_type_id, "descending"
        )

        users_list = self.search_users(users)
        self.json_response(users_list)
