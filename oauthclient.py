# -*- coding: utf-8 -*-

import json
import logging
import urllib.error
import urllib.parse
import urllib.request
from base64 import b64decode
from datetime import timedelta
from http.client import HTTPException, UNAUTHORIZED, FORBIDDEN, INTERNAL_SERVER_ERROR
from socket import error
from urllib.parse import urlencode
from urllib.parse import urlparse
from uuid import uuid4

import config
import googleapiclient.discovery
import jwt
from libraries import utils

from models.model import User, Plan
from tornado.escape import url_escape
from tornado.options import options


class AuthError(Exception):
    def __init__(self, message=None, raw_data=None, **kwargs):
        self.message = message
        super(AuthError, self).__init__(message)
        if raw_data:
            self.raw_data = raw_data
        for k, v in kwargs.items():
            self.__setattr__(k, v)


class OAuth2Mixin(object):
    CLIENT_ID = options.oauth_client_id
    PLATFORM_ID = options.platform_id
    OAUTH_SERVER_URL = options.oauth_server_url

    @staticmethod
    def login_url(callback_uri, state):
        return OAuth2Mixin.OAUTH_SERVER_URL + "/login?client_id=%s&redirect_uri=%s&state=%s" % (
            OAuth2Mixin.CLIENT_ID,
            url_escape(callback_uri),
            url_escape(state),
        )

    @staticmethod
    def logout_url(redirect_uri):
        return OAuth2Mixin.OAUTH_SERVER_URL + "/logout?client_id=%s&redirect_uri=%s" % (
            OAuth2Mixin.CLIENT_ID,
            url_escape(redirect_uri),
        )

    @staticmethod
    def make_request(url, data=None, timeout=60):
        if data:
            data = urlencode(data).encode("utf-8")
        http = urllib.request.Request(url, data)

        try:
            response = urllib.request.urlopen(http, timeout=timeout)
        except urllib.error.HTTPError as e:  # if HTTP status is not 200
            response = e.read()
            if e.code in [UNAUTHORIZED, FORBIDDEN]:
                try:
                    response = json.loads(response)
                    message = (
                        response.pop("message")
                        if "message" in response
                        else "OAuth2 error: %s" % response
                    )
                    raise AuthError(message=message, **response)
                except ValueError:
                    pass
            raise AuthError(message="Invalid OAuth2 response: %s" % response, raw_data=response)
        except HTTPException as e:
            msg = e.message if hasattr(e, "message") else str(e)
            raise AuthError(message="Invalid OAuth2 response: %s" % msg)
        except error:
            hostname = urlparse(url)
            raise AuthError(message="Cannot access %s" % hostname.netloc)

        data = response.read()
        if data:
            try:
                data = json.loads(data)
            except ValueError:
                raise AuthError(message="Invalid OAuth2 response: %s" % data, raw_data=data)
        return data

    @staticmethod
    def exchange_code_for_access_token(callback_uri, code):
        url = OAuth2Mixin.OAUTH_SERVER_URL + "/web/oauth2"
        data = {
            "grant_type": "authorization_code",
            "code": code,
            "redirect_uri": callback_uri,
            "client_id": OAuth2Mixin.CLIENT_ID,
        }
        response = OAuth2Mixin.make_request(url, data)
        return response

    @staticmethod
    def refresh_access_token(refresh_token):
        url = OAuth2Mixin.OAUTH_SERVER_URL + "/web/oauth2"
        data = {
            "grant_type": "refresh_token",
            "refresh_token": refresh_token,
            "client_id": OAuth2Mixin.CLIENT_ID,
        }
        response = OAuth2Mixin.make_request(url, data)
        return response

    @staticmethod
    def set_cookies(handler, data):
        if "access_token" in data:
            expires_in = 10000 if options.env == "local" else data["expires_in"]
            expires = options.datetime_now() + timedelta(seconds=expires_in)
            handler.set_secure_cookie("access_token", data["access_token"], expires=expires)
            expires = options.datetime_now() + timedelta(days=14)
            handler.set_secure_cookie("refresh_token", data["refresh_token"], expires=expires)

    @staticmethod
    def validate_access_token(token):
        url = "{}/validate_token?token={}&platform_id={}".format(
            OAuth2Mixin.OAUTH_SERVER_URL, token, OAuth2Mixin.PLATFORM_ID
        )
        OAuth2Mixin.make_request(url)

    @staticmethod
    def validate_refresh_token(token):
        url = "{}/validate_refresh_token?token={}&platform_id={}".format(
            OAuth2Mixin.OAUTH_SERVER_URL, token, OAuth2Mixin.PLATFORM_ID
        )
        OAuth2Mixin.make_request(url)

    @staticmethod
    def revoke_token(token, redirect_uri=None):
        if token:
            data = {"token": token, "client_id": OAuth2Mixin.CLIENT_ID}
            if redirect_uri:
                data["redirect_uri"] = redirect_uri
            url = OAuth2Mixin.OAUTH_SERVER_URL + "/web/oauth2/revoke"
            try:
                OAuth2Mixin.make_request(url, data)
            except AuthError as e:  # just in case. We don't care about the request result
                pass

    def get_current_user(self):
        token = self.get_secure_cookie("access_token")
        if token:
            profile, user = OAuth2Mixin._decode_token(token)
            if not user:
                self.clear_cookie("access_token")
                logging.info(
                    "Sorry, something went wrong with the you login session. Try logging in again"
                )
                return self.redirect("/login")
            profile["user"] = user
            return profile
        return None

    @staticmethod
    def _decode_token(token):
        token = jwt.decode(token, verify=False)
        profile = token.get("profile", {})
        user = User.get_by_uuid(profile.get("profile_id"))
        return profile, user

    def get_user_plans(self, profile_id):
        plans = [plan for plan in Plan.get_plan_list_by_user(profile_id)]
        active_plan = next(iter(plans), None)
        active_plan_uuid = active_plan["plan_uuid"] if active_plan else None
        return plans, active_plan_uuid

    def get_request_id(self):
        return str(uuid4())


class OAuth2BackendClient(OAuth2Mixin):
    private_key = None

    @property
    def base_url(self):
        return OAuth2BackendClient.OAUTH_SERVER_URL + "/backend/oauth2"

    @classmethod
    def decode_key(cls, key):
        logging.info("entering decode_key")
        try:
            logging.info("trying to decode")
            kms_client = googleapiclient.discovery.build("cloudkms", "v1")

            # Read cipher text from the input file.
            # Use the KMS API to decrypt the text.
            cryptokeys = kms_client.projects().locations().keyRings().cryptoKeys()
            request = cryptokeys.decrypt(name=options.kms_key_path, body={"ciphertext": key})
            response = request.execute()
            encoded_key = response["plaintext"]
            result = b64decode(encoded_key)
        except Exception as e:
            if options.env != "test":
                logging.exception(e)
            result = key
        return result

    @classmethod
    def get_private_key(cls, cache_key="rsa_private_key"):
        logging.info("reading key file locally")
        key = open(options.rsa_private_key, "r").read()
        if not key:
            raise AuthError(
                "You must have private.pem file for local oauth. requests",
                code=INTERNAL_SERVER_ERROR,
            )
        private_key = cls.decode_key(key)
        return private_key

    @classmethod
    def get_backend_token(cls):
        if options.generate_token:
            exp = options.datetime_now() + timedelta(seconds=10000)
            claims = {"backend": True, "exp": exp, "platform_id": OAuth2Mixin.PLATFORM_ID}
            key = cls.get_private_key(config.get_env() + "rsa_private_key")
            token = jwt.encode(claims, key, "RS256").decode("utf-8")
        else:
            auth_params = {
                "grant_type": "client_credentials",
                "client_id": OAuth2Mixin.CLIENT_ID,
                "platform_id": OAuth2Mixin.PLATFORM_ID,
            }
            auth_url = "%s/backend/oauth2" % options.oauth_server_url
            token = OAuth2Mixin.make_request(url=auth_url, data=auth_params)
            token = token["access_token"]
        return token

    @classmethod
    def authenticated_request(
        cls,
        url,
        data=None,
        method=None,
        json_response=True,
        operation=None,
        response_errors=[],
        timeout=60,
    ):
        try:
            token = cls.get_backend_token()
        except ValueError as e:
            import traceback

            traceback.print_exc()
            result = {"status": "error", "message": str(e)}

            return result
        headers = {"Authorization": "Bearer " + token}
        for i in [1, 2, 3]:  # repeat 3 times to let PDF API warm up
            try:
                logging.info("auth. request %s" % url)
                logging.info("token %s" % token)
                result = utils.make_request(
                    url,
                    data,
                    headers,
                    method,
                    json_type=True,
                    json_response=json_response,
                    response_errors=response_errors,
                    timeout=timeout,
                )
            except ValueError as e:
                result = {"status": "error", "message": str(e)}

                return result
            if not isinstance(result, dict) or result.get("message", "").startswith(
                "Cannot access"
            ):
                break
            return result
