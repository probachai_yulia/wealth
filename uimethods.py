from datetime import timedelta
from numbers import Number

from libraries import utils
from tornado.options import options


@utils.with_tz
def date_format(handler, current_dttm):
    """
    Returns the provided date object in the format: 30 Dec 2100.
    In case of an invalid date, returns "**".
    """
    if not current_dttm:
        return "**"
    try:
        formatted_date = current_dttm.strftime("%d %b %Y")
    except BaseException:
        formatted_date = "**"
    return formatted_date


@utils.with_tz
def date_format_iso(handler, current_dttm):
    """
    Returns the provided date object in the format: 2100-12-30.
    In case of an invalid date, returns "**".
    """
    if not current_dttm:
        return "**"
    try:
        formatted_date = current_dttm.strftime("%Y-%m-%d")
    except BaseException:
        formatted_date = "**"
    return formatted_date


@utils.with_tz
def date_weekday_format(handler, current_dttm):
    """
    Returns the provided date object in the format: Thursday, December 30th.
    In case of an invalid date, returns "**".
    """
    try:
        formatted_date = current_dttm.strftime("%A, %B %d") + ordinal(
            handler, current_dttm.day, just_suffix=True
        )
    except BaseException:
        formatted_date = "**"
    return formatted_date


@utils.with_tz
def date_time_format(handler, ts):
    """
    Returns the provided date object in the format: December 30, 2100 11:59 PM.
    In case of an invalid date, returns "**".
    """
    try:
        return ts.strftime("%B %d, %Y %I:%M %p")
    except Exception:
        return "**"


def date_format_dmy(handler, ts):
    """
    Returns the provided date object in the format: 30/12/2100.
    In case of an invalid date, returns "**".
    """
    try:
        return ts.strftime("%d/%m/%Y") if ts and ts.year > 1 else "**"
    except Exception:
        return "**"


def ordinal(handler, num, just_suffix=False):
    """ Formats an integer number with the appropriate english suffix: st, nd, rd, th.

        Args:
            num (int, str): Number to format.
            just_suffix (bool): If True, only the suffix (st, nd, rd, th) will be returned.

        Returns:
            String containing number + suffix, or just the suffix.
    """
    try:
        num = int(num)
    except TypeError:
        return ""

    # there is no ordinal non positive
    if num <= 0:
        return ""

    suffixes = {1: "st", 2: "nd", 3: "rd"}
    suffix = "th" if 4 <= num % 100 <= 20 else suffixes.get(num % 10, "th")
    return suffix if just_suffix else str(num) + suffix


def number_format(handler, value):
    """
    Returns the number formated with comma separator.
    For example, 1000000 -> 1,000,000
    Returns blank if the value is an invalid number.
    """
    if not value:
        return ""

    if not isinstance(value, Number):
        try:
            value = float(value) if "." in value else int(value)
        except (TypeError, ValueError):
            return ""

    return "{:,}".format(value)


def poundify(value):
    if not value or not isinstance(value, Number):
        value = 0.0
    return "{:.2f}".format(float(value) / 100.0)


@utils.with_tz
def friendly_duration(handler, start_dttm, end_dttm=None):
    if end_dttm is None:
        end_dttm = utils.inject_tzinfo(options.datetime_now(), tz=start_dttm.tzinfo)
    duration = int((end_dttm - start_dttm).total_seconds())
    if duration < 60:
        return "{} seconds".format(duration)
    if duration < 3600:
        return "{} minutes".format(round(duration / 60))
    if duration < 86400:
        return "{} hours".format(round(duration / 3600))
    if duration < 2_592_000:
        return "{} days".format(round(duration / 86400))
    if duration < 7_776_000:
        return "{} weeks".format(round(duration / 604_800))
    return "{} months".format(round(duration / 2_592_000))


class FriendlyFormats:
    @staticmethod
    def seconds(ts, now, ts_hour, preposition):
        seconds = int((now - ts).seconds)
        if seconds < 10:
            friendly = "Just now"
        else:
            friendly = "%s second%s ago" % (seconds, {1: ""}.get(seconds, "s"))
        return friendly

    @staticmethod
    def minutes(ts, now, ts_hour, preposition):
        minutes = int(((now - ts).seconds) / 60)
        friendly = "%d minute%s ago" % (minutes, {1: ""}.get(minutes, "s"))
        return friendly

    @staticmethod
    def hours(ts, now, ts_hour, preposition):
        hours = int(((now - ts).seconds) / 3600)
        friendly = "%s hour%s ago" % (hours, {1: ""}.get(hours, "s"))
        return friendly

    @staticmethod
    def days(ts, now, ts_hour, preposition):
        days = int((now - ts).days)
        if days == 1:
            friendly = "Yesterday at %d:%s" % (ts_hour, ts.strftime("%M%p").lower())
        else:
            friendly = "%s @ %d:%s" % (ts.strftime("%A"), ts_hour, ts.strftime("%M%p").lower())
        return friendly

    @staticmethod
    def year(ts, now, ts_hour, preposition):
        friendly = "%s %d @ %d:%s" % (
            ts.strftime("%B"),
            ts.day,
            ts_hour,
            ts.strftime("%M%p").lower(),
        )
        if preposition:
            friendly = "on " + friendly
        return friendly

    @staticmethod
    def general(ts, now, ts_hour, preposition):
        friendly = "%s %d, %d" % (ts.strftime("%B"), ts.day, ts.year)
        if preposition:
            friendly = "on " + friendly
        return friendly


@utils.with_tz
def friendly_time(handler, ts, preposition=False):
    """returns ago format of time"""
    if not ts:
        return ""
    now = utils.inject_tzinfo(options.datetime_now(), tz=ts.tzinfo)
    ts_hour = {0: 12}.get(ts.hour % 12, ts.hour % 12)

    ops = ("seconds", "minutes", "hours", "days", "year", "general")
    ranges = (
        ts > (now - timedelta(seconds=60)),
        ts > (now - timedelta(hours=1)),
        ts > (now - timedelta(days=1)),
        ts > (now - timedelta(days=7)),
        ts > (now - timedelta(days=365)),
        True,
    )
    op = ops[ranges.index(True)]
    formatter = getattr(FriendlyFormats, op)
    return formatter(ts, now, ts_hour, preposition)


def friendly_filesize(handler, size):
    """
    Return the given bytes as a human friendly KB, MB, GB, or TB string
    """
    B = int(size)
    KB = float(1024)
    MB = float(KB ** 2)  # 1,048,576
    GB = float(KB ** 3)  # 1,073,741,824
    TB = float(KB ** 4)  # 1,099,511,627,776

    if B < KB:
        return "{0} {1}".format(B, "Bytes" if B > 1 or B == 0 else "Byte")
    elif KB <= B < MB:
        return "{0:.2f} KB".format(B / KB)
    elif MB <= B < GB:
        return "{0:.2f} MB".format(B / MB)
    elif GB <= B < TB:
        return "{0:.2f} GB".format(B / GB)
    else:
        return "{0:.2f} TB".format(B / TB)


def currency(handler, amount, prefix="£", suffix=""):
    """
    Formats a number as a currency.
    Args:
        handler: Currently active instance of RequestHandler.
        amount: The number to format.
        prefix: Any pre-pending currency symbols, such as '$' or '€'.
        suffix: Any following currency symbols, such as 'HRK' or 'JPY'.

    Returns:
        String formatted as currency.
    """
    if isinstance(amount, Number):
        return "{prefix}{amount:,.2f}{suffix}".format(amount=amount, prefix=prefix, suffix=suffix)
    else:
        return "N/A"


def include_google_tag_manager(handler):
    return options.include_google_tag_manager
