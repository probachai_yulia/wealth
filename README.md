# Octopus wealth

Octopus Wealth Fact Find and WebApp
The Octopus Wealth fact find is designed to be a an intuitive client oriented version of the traditional Financial Adviser fact find process.
We aim to:
* Gather all necessary data in as flexible a way as possible
* Store data as simply as possible
* Track a history of all changes
* Make all stored data available via API

## Getting started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

### Prerequisites

* Python 3.6.5 (for running outside Docker) <https://www.python.org/downloads/release/python-365/>
* Virtualenv <https://virtualenv.pypa.io/en/stable/installation/>
* Docker <https://docs.docker.com/install/>
* npm <https://www.npmjs.com/get-npm>

### Installing

1. `git clone git@github.com:octopus-investments/octopus-wealth-fact.git` - Clone the project
1. `cd octopus-wealth-fact` - Change directory to the project directory
1. `virtualenv -p python3 venv` - Initialise a new virtual environment
1. `source venv/bin/activate` - Activate your virtual environment
1. `pip install -r requirements.txt -r env_requirements.txt` - Install all project + development dependencies
1. `pre-commit install` - Install project code quality pre-commit hooks <https://pre-commit.com/>
1. `docker-compose run web alembic upgrade head` - Initialise your local docker mysql instance with the latest schema


To ensure you've installed correctly:
1. `docker-compose up` - Build and run the project
1. go to <http://localhost:8080/> to view the project

### Installing frontend

1. `cd frontend`
1. `npm install`
1. `npm start`

### Creating tables

1. `alembic upgrade head` - This runs all migrations if they have not been run before up to the most recent.

### Creating new migrations

1. Make a change to one of the database models.
1. `alembic revision --autogenerate -m "message explaining what changed e.g. created asset table"` - compares model to local db and generates python migration file.
1. Commit generated migration file

### Create test user command (*local* environment)

1. `python commands/create_dev_user.py --dev_email=JohnSmith@example.com --dev_user_type=adviser` - Create dev user in such view:
   ```json
     {
       "first_name": "JohnSmith", 
       "last_name": "WealthDev", 
       "profile_id": "f3fa051d-d8a8-4550-a782-82e2a907df73", 
       "email": "JohnSmith@example.com",
       "password": "Octopus!1"
    }
    ```
`--dev_user_type` -- optional argument, which add role to user. It's 'client' by default

### Create test user task   

1. GET `/api/v1/tasks/dev_user?email=example@email.com&user_type=client`

    Do the same as command but blocked for any environments except **dev**

### Truncate plan tables and related ones (*local* environment)

1. `python commands/clear_database.py` - truncates tables from the list
   ```[
    "plan",
    "plan_asset",
    "plan_asset_owner",
    "plan_cashflow",
    "plan_debt",
    "plan_debt_owner",
    "plan_document",
    "plan_giving",
    "plan_soft_fact",
    "plan_user",
    "plan_user_cashflow",
    "plan_user_cashflow_change",
    "plan_user_job",
    "regular_contribution",
    "soft_fact_tag_joint",
    "terms_and_condition_accept",
    "user",
    "user_role",
    "user_us_connection"
    ]
    ```
### Apply insert constants to database for tests

1. `python commands/apply_data.py`  

###Running tests

`chmod +x scripts/`

Run API tests   
`scripts/run_apitests.sh`   

Run functional tests   
`scripts/run_functests.sh`   

Run init tests   
`scripts/run_inittests.sh`

Run tests   
`scripts/runtests.sh`

Run selenium tests   
`install_chromedriver.sh`
`scripts/run_seleniumtests.sh`

### Coding style tests

Coding style is enforced by commit hooks install by [pre-commit](https://pre-commit.com/).
Currently checks are:
* [Black formatter](https://github.com/ambv/black) - On **fail** you just need to `git add` all changed files and re-commit
* [Flake8](http://flake8.pycqa.org/en/latest/) - Provides linting, [PEP8](https://www.python.org/dev/peps/pep-0008/) compliance and Cyclomatic Complexity checks. On **fail** you need to fix the code lines highlighted by *flake8*
These are run on any changed files automatically before you commit and fail if a change is required - usually giving a helpful message with what needs changing. To run the checks manually on all files: `pre-commit run --all-files`.
* **check-ast** - Simply check whether files parse as valid python. On **fail** you need to fix the syntax errors in your python code.
* [python-safety-dependencies-check](https://github.com/Lucas-C/pre-commit-hooks-safety) - Compare requirements files against [safety-db](https://github.com/pyupio/safety-db) - On **fail** you need to update the requirement's version to a safer option

## Development process

* [gitflow](https://www.atlassian.com/git/tutorials/comparing-workflows/gitflow-workflow) - Branching style
* `hotfix|feature/chXXXX` where `XXXX` is the [clubhouse](https://app.clubhouse.io/octopus/stories/) story id - Branch name
* Commit messages:
  - 1st line - Why the change was made (not what)
  - extra lines (optional) - Useful extra context information. E.g. links to bugs/tickets/documentation, exception trace
  - Empty for merge commits (as git auto-generates this, the `-m` parameter is unnecessary for merge commits)

## Deployment

The `develop` branch is automatically deployed by [Jenkins](https://jenkins.octopuslabs.com/) to the dev server at <https://dev.octopuswealth.com>.

The `master` branch is automatically deployed by [Jenkins](https://jenkins.octopuslabs.com/) to the prod server at <https://octopuswealth.com>.

### Production release
1. Create a pull request from `develop` branch to `master` branch with name "Release {current_date}" - E.g. "Release 19/07/18" (for multiple per day, append a no. e.g. Release 16/07/18 no. 2)
1. Share the pull request on the slack `#audit-deploy-approval` channel
1. On approval, merge the pull request
1. Jenkins will be automatically triggered and complete the release and switch the load balancer

## Built with

* [Tornado](http://www.tornadoweb.org/) - The web framework used
* [Mysql](https://www.mysql.com/) - Database
* [Memcache](https://memcached.org/) - Cache server
* [Docker](https://maven.apache.org/) - Containerisation
* [Kubernetes](https://kubernetes.io/) - Deployed container orchestration
