import logging
from collections import namedtuple

from marshmallow_sqlalchemy import ModelConversionError, ModelSchema
from sqlalchemy import create_engine, event
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import class_mapper, mapper, scoped_session, sessionmaker
from sqlalchemy.orm.query import Query
from tornado.options import options

import config

config.initialize()

logging.basicConfig()
# Comment in for query log
# logging.getLogger("sqlalchemy.engine").setLevel(logging.DEBUG)


class SoftDeleteQuery(Query):
    """
    Class created to globally allow soft deleting objects.
    It works by adding a filter by "delete_dttm=None" to all sqlalchemy queries.
    So, only the non deleted objects are going to be listed.
    To disable the effect and list all objects, you need to
    """

    _with_deleted = False

    def __new__(cls, *args, **kwargs):
        """
        Creates a new query object with soft delete logic builtin.
        """
        obj = super(SoftDeleteQuery, cls).__new__(cls)
        obj._with_deleted = kwargs.pop("_with_deleted", False)
        if len(args) > 0:
            super(SoftDeleteQuery, obj).__init__(*args, **kwargs)
            if obj._entities:
                for entitie in obj._entities:
                    if hasattr(entitie, "delete_dttm"):
                        # only applies the logic for models that have the attribute delete_dttm
                        # easier and safer than trying to get the class model and checking for attributes
                        return obj.filter_by(delete_dttm=None) if not obj._with_deleted else obj

        return obj

    def __call__(self, *args, **kwargs):
        """
        Method to turn it possible with this custom class to do Model.query(Column1, Column2...).
        """
        return self.session.query(*args)

    def with_deleted(self):
        """
        Creates a new Query instance to show the deleted objects.
        So, it's better to use it before any filter, etc. Since it throws away everything done
        before.
        """
        return self.__class__(
            class_mapper(self._mapper_zero().class_), session=Session().session, _with_deleted=True
        )

    def _get(self, *args, **kwargs):
        """
        This calls the original query.get function from the base class
        """
        return super(SoftDeleteQuery, self).get(*args, **kwargs)

    def get(self, *args, **kwargs):
        """
        The query.get method does not like it if there is a filter clause
        pre-loaded, so we need to implement it using a workaround.
        """
        obj = self.with_deleted()._get(*args, **kwargs)
        return (
            obj
            if obj is None
            or self._with_deleted
            or ((hasattr(obj, "delete_dttm") and not obj.delete_dttm))
            else None
        )


def setup_schema(Base, session):
    """
    Snipet from marshmallow_sqlalchemy documentation.
    This creates a function which incorporates the Base and session information
    into the models so they can return a ModelSchema ready to be used by the call:
    ClassModel.__marshmallow__().
    """

    def setup_schema_fn():
        for class_ in Base._decl_class_registry.values():
            if hasattr(class_, "__tablename__"):
                if class_.__name__.endswith("Schema"):
                    raise ModelConversionError(
                        "For safety, setup_schema can not be used when a"
                        "Model class ends with 'Schema'"
                    )

                class Meta(object):
                    model = class_
                    sqla_session = session

                schema_class_name = "%sSchema" % class_.__name__

                schema_class = type(schema_class_name, (ModelSchema,), {"Meta": Meta})

                setattr(class_, "__marshmallow__", schema_class)

    return setup_schema_fn


class Session:
    """
    Singleton to have a single database session throughout all the system.
    """

    def __new__(cls, env=options.env, force_new=False):
        if force_new or not hasattr(cls, "_instance"):
            cls._instance = object.__new__(cls)

            # the instance attributes have to stay as class
            # attributes or they're going to be refreshed/changed
            # every __init__ call and be a borg (and cause problems
            # to create the database tables, for example).
            self = cls._instance
            if env == "test":
                db_url = "sqlite://"  # in memory sqlite
                debug = None
            else:
                db = self.get_db_config()
                db_url = f"{db.server}://{db.user}:{db.pw}@{db.host}/{db.db}"
                debug = options.debug_mode

            self._engine = create_engine(db_url, echo=debug, echo_pool=debug)
            session_factory = sessionmaker(bind=self._engine, query_cls=SoftDeleteQuery)
            self._Session = scoped_session(session_factory)
            self._Base = declarative_base()
            self._Base.query = self._Session.query_property()
            self._Base.metadata.bind = self._engine
            self.logger = logging.getLogger(__name__)
            self.logger.info("Database initialized.")

            # Listen for the SQLAlchemy event and run setup_schema.
            # Note: This has to be done after Base and session are setup
            event.listen(mapper, "after_configured", setup_schema(self._Base, self._Session))

        return cls._instance

    def __getattr__(self, attr):
        """
        Try to get the attributes not present here from the sqlalchemy session.
        """
        return self.__dict__.get(attr, getattr(self._Session, attr))

    @staticmethod
    def get_db_config():
        DbConfig = namedtuple("DbConfig", "server, user, pw, host, db")
        return DbConfig(
            options.wealth_db_software,
            options.wealth_db_user,
            options.wealth_db_password,
            options.wealth_host,
            options.wealth_db_name,
        )

    @property
    def base_query_cls(self):
        return Query

    @property
    def engine(self):
        return self._engine

    @property
    def base(self):
        return self._Base

    @property
    def session(self):
        return self._Session()

    @property
    def metadata(self):
        return self._Base.metadata

    def create_tables(self):
        self.logger.info("Creating database...")
        self.metadata.create_all(self._engine)
        self.session.commit()

    def drop_tables(self):
        self.logger.info("Dropping database...")
        self.metadata.drop_all(self._engine)
