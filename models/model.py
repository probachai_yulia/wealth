"""
Demonstrates how to use AllFeaturesMixin.
It just combines other mixins, so look to their examples for details
"""
import uuid
from typing import Any  # noqa

import sqlalchemy as sa
from tornado.options import options

from models.model_mixin import ModelMixin
from models.session import Session


base = Session().base  # type: Any


class UniquePlanUserType(Exception):
    # TODO: find a better location to place this (probably create an exceptions.py)
    def __init__(self, user_type):
        self.message = "You can add only one user with this type to plan: {}".format(user_type)
        super().__init__(self.message)


class BaseModel(base, ModelMixin):
    __abstract__ = True


class User(BaseModel):
    __tablename__ = "user"

    user_id = sa.Column(sa.Integer, primary_key=True)
    profile_uuid = sa.Column(sa.String(36), unique=True)
    is_active = sa.Column(sa.Boolean, default=0)
    token = sa.Column(sa.String(36))
    create_dttm = sa.Column(sa.DateTime, default=options.datetime_now, index=True)
    update_dttm = sa.Column(sa.DateTime, onupdate=options.datetime_now, index=True)
    delete_dttm = sa.Column(sa.DateTime, default=None, index=True)

    amlcases = sa.orm.relationship("AmlCase", cascade="all,delete", back_populates="user")
    terms_and_conditions_accept = sa.orm.relationship(
        "TermsAndConditionAccept", cascade="all,delete"
    )
    token_to_user_table = sa.orm.relationship("TokenToUserTable", cascade="all,delete")
    user_role = sa.orm.relationship("UserRole", cascade="all,delete", back_populates="user")
    plan_soft_fact = sa.orm.relationship(
        "PlanSoftFact", cascade="all,delete", back_populates="user"
    )

    @classmethod
    def get_main_plan_user(cls, plan_id):
        from utils.constants import MAIN_USER_TYPE

        result = cls.instances_to_dict(
            cls.query.join(PlanUser.profile)
            .join(PlanUserType, PlanUserType.plan_user_type_id == PlanUser.plan_user_type_id)
            .filter(PlanUser.plan_id == plan_id)
            .filter(PlanUserType.code == MAIN_USER_TYPE)
            .all()
        )
        return result[0] if result else None

    @classmethod
    def get_main_user_profile_uuid(cls, plan_id):
        user = cls.get_main_plan_user(plan_id)
        return user["profile_uuid"] if user else None

    @classmethod
    def get_users_by_role(cls, role):
        result = (
            cls.query.join(UserRole.user)
            .join(Role, Role.role_id == UserRole.role_id)
            .filter(Role.name == role)
        )
        return result

    @classmethod
    def get_users_with_role(cls):
        result = cls.query.join(UserRole.user)
        return result

    @classmethod
    def get_profile_uuid(cls, user_id):
        user = User.query.filter_by(user_id=user_id).first()
        return user if user else None

    @classmethod
    def get_by_uuid(cls, profile_uuid):
        user = User.query.filter_by(profile_uuid=profile_uuid).first()
        return user if user else None

    @classmethod
    def get_plan_user_profile_uuid(cls, plan_user_id):
        result = (
            cls.query.join(PlanUser.profile).filter(PlanUser.plan_user_id == plan_user_id).first()
        )
        return result.profile_uuid if result else None

    @classmethod
    def has_permission(cls, user_id, permission_code):
        user_permissions = cls.get_permissions_by_user_id(user_id)
        permission = Permission.get_permission_by_code(permission_code)
        return True if permission in user_permissions else False

    @classmethod
    def get_permissions_by_user_id(cls, user_id):
        role_pemissions = cls.instances_to_dict(
            cls.query.join(UserRole.user)
            .join(Role, Role.role_id == UserRole.role_id)
            .join(RolePermission, Role.role_id == RolePermission.role_id)
            .join(Permission, Permission.permission_id == RolePermission.permission_id)
            .filter(User.user_id == user_id)
            .with_entities(Permission)
        )
        user_permissions = cls.instances_to_dict(
            cls.query.join(UserPermission.user)
            .join(Permission, Permission.permission_id == UserPermission.permission_id)
            .filter(User.user_id == user_id)
            .with_entities(Permission)
        )
        result = []
        result.extend(role_pemissions)
        result.extend(user_permissions)
        return result

    # TODO refactoring
    @classmethod
    def add_permission(cls, user_id, permission_id, group_name=None):
        if group_name:
            role = Role.query.filter_by(name=group_name).first()
            if role:
                role_id = Role.update_role(role_id=role.role_id, name=group_name)
            else:
                role_id = Role.add_role(name=group_name)
            user_role = UserRole.query.filter_by(user_id=user_id, role_id=role_id).first()
            if user_role:
                user_role_id = user_role.user_role_id
                UserRole.update_user_role(user_role_id, role_id=role_id, user_id=user_id)
            else:
                UserRole.add_user_role(role_id=role_id, user_id=user_id)
            role_permission = RolePermission.query.filter_by(
                permission_id=permission_id, role_id=role_id
            ).first()
            if role_permission:
                role_permission_id = role_permission.role_permission_id
                return RolePermission.update(
                    role_permission_id, role_id=role_id, permission_id=permission_id
                )
            else:
                return RolePermission.add_role_permission(
                    role_id=role_id, permission_id=permission_id
                )
        else:
            return UserPermission.add_user_permission(user_id, permission_id)

    @classmethod
    def get_user_role(cls, user_id):
        user_groups = UserRole.query.filter_by(user_id=user_id)
        groups = [Role.query.filter_by(role_id=role.role_id).first() for role in user_groups]
        return groups[0].name if groups else None

    @classmethod
    def get_adviser_clients(cls, profiles):
        return User.query.filter(User.profile_uuid.in_(profiles))


class Permission(BaseModel):
    __tablename__ = "permission"
    __repr_attrs__ = ["name", "code"]

    permission_id = sa.Column(sa.Integer, primary_key=True)
    name = sa.Column(sa.String(128))
    code = sa.Column(sa.String(64))
    create_dttm = sa.Column(sa.DateTime, default=options.datetime_now, index=True)
    update_dttm = sa.Column(sa.DateTime, onupdate=options.datetime_now, index=True)
    delete_dttm = sa.Column(sa.DateTime, default=None, index=True)

    @classmethod
    def add_permission(cls, name, code):
        permission_id = cls.create(name=name, code=code)
        return permission_id

    @classmethod
    def get_permission_by_code(cls, code):
        permission = cls.query.filter_by(code=code).first()
        return permission


class Role(BaseModel):
    __tablename__ = "role"
    __repr_attrs__ = ["name"]

    role_id = sa.Column(sa.Integer, primary_key=True)
    name = sa.Column(sa.String(64))
    create_dttm = sa.Column(sa.DateTime, default=options.datetime_now, index=True)
    update_dttm = sa.Column(sa.DateTime, onupdate=options.datetime_now, index=True)
    delete_dttm = sa.Column(sa.DateTime, default=None, index=True)

    @classmethod
    def add_role(cls, name):
        role_id = cls.create(name=name)
        return role_id

    @classmethod
    def update_role(cls, role_id, name):
        role_id = cls.update(role_id, name=name)
        return role_id

    @classmethod
    def get_role_by_name(cls, name):
        role = cls.query.filter_by(name=name).first()
        return role

    @classmethod
    def get_roles_list(cls):
        result = []
        for role in cls.all():
            result.append({"code": role["name"], "id": role["role_id"], "name": role["name"]})
        return result


class UserRole(BaseModel):
    __tablename__ = "user_role"
    __repr_attrs__ = ["user_role_id"]

    user_role_id = sa.Column(sa.Integer, primary_key=True)
    role_id = sa.Column(sa.Integer, sa.ForeignKey("role.role_id"), index=True)
    user_id = sa.Column(sa.Integer, sa.ForeignKey("user.user_id"), index=True)
    create_dttm = sa.Column(sa.DateTime, default=options.datetime_now, index=True)
    update_dttm = sa.Column(sa.DateTime, onupdate=options.datetime_now, index=True)
    delete_dttm = sa.Column(sa.DateTime, default=None, index=True)

    user = sa.orm.relationship("User")

    @classmethod
    def add_user_role(cls, role_id, user_id):
        user_role_id = cls.create(role_id=role_id, user_id=user_id)
        return user_role_id

    @classmethod
    def update_user_role(cls, user_role_id, role_id, user_id):
        user_role_id = cls.update(user_role_id, role_id=role_id, user_id=user_id)
        return user_role_id


class RolePermission(BaseModel):
    __tablename__ = "role_permission"
    __repr_attrs__ = ["role_permission_id"]

    role_permission_id = sa.Column(sa.Integer, primary_key=True)
    role_id = sa.Column(sa.Integer, sa.ForeignKey("role.role_id"), index=True)
    permission_id = sa.Column(sa.Integer, sa.ForeignKey("permission.permission_id"), index=True)
    create_dttm = sa.Column(sa.DateTime, default=options.datetime_now, index=True)
    update_dttm = sa.Column(sa.DateTime, onupdate=options.datetime_now, index=True)
    delete_dttm = sa.Column(sa.DateTime, default=None, index=True)

    @classmethod
    def add_role_permission(cls, role_id, permission_id):
        role_permission_id = cls.create(role_id=role_id, permission_id=permission_id)
        return role_permission_id

    @classmethod
    def has_permission(cls, role_name, permission_code):
        role_pemissions = cls.instances_to_dict(
            cls.query.join(Role, Role.role_id == UserRole.role_id)
            .join(Permission, Permission.permission_id == RolePermission.permission_id)
            .filter(Permission.code == permission_code)
            .filter(Role.name == role_name)
            .wall()
        )
        return bool(role_pemissions)


class UserPermission(BaseModel):
    __tablename__ = "user_permission"
    __repr_attrs__ = ["user_permission_id"]

    user_permission_id = sa.Column(sa.Integer, primary_key=True)
    user_id = sa.Column(sa.Integer, sa.ForeignKey("user.user_id"), index=True)
    permission_id = sa.Column(sa.Integer, sa.ForeignKey("permission.permission_id"), index=True)
    create_dttm = sa.Column(sa.DateTime, default=options.datetime_now, index=True)
    update_dttm = sa.Column(sa.DateTime, onupdate=options.datetime_now, index=True)
    delete_dttm = sa.Column(sa.DateTime, default=None, index=True)

    user = sa.orm.relationship("User")

    @classmethod
    def add_user_permission(cls, user_id, permission_id):
        user_permission_id = cls.create(user_id=user_id, permission_id=permission_id)
        return user_permission_id


class TokenToUserTable(BaseModel):
    __tablename__ = "token_to_user_table"
    __repr_attrs__ = ["token_to_user_table_id"]

    token_to_user_table_id = sa.Column(sa.Integer, primary_key=True)
    user_id = sa.Column(sa.Integer, sa.ForeignKey("user.user_id"), index=True)
    token = sa.Column(sa.String(36), unique=True)
    create_dttm = sa.Column(sa.DateTime, default=options.datetime_now, index=True)
    update_dttm = sa.Column(sa.DateTime, onupdate=options.datetime_now, index=True)
    delete_dttm = sa.Column(sa.DateTime, default=None, index=True)

    user = sa.orm.relationship("User")

    @classmethod
    def add_token_to_user_table(cls, user_id):
        token_to_user_table_id = cls.create(user_id=user_id, token=str(uuid.uuid4()))
        return token_to_user_table_id

    @classmethod
    def delete_token_to_user_table(cls, days):
        sql = """
              Delete * from `token_to_user_table` where `create_dttm` < CURRENT_DATE - INTERVAL {} DAY;
                """.format(
            days
        )
        return cls.run_sql(sql)


class ModificationHistory(BaseModel):
    __tablename__ = "modification_history"
    __repr_attrs__ = ["table_name", "operation", "user_id"]

    modification_history_id = sa.Column(sa.Integer, primary_key=True)
    user_id = sa.Column(sa.Integer, sa.ForeignKey("user.user_id"))
    operation = sa.Column(sa.String(255))
    table_name = sa.Column(sa.String(255))
    data = sa.Column(sa.Text, nullable=True)
    result = sa.Column(sa.Text, nullable=True)
    request_id = sa.Column(sa.String(36))
    create_dttm = sa.Column(sa.DateTime, default=options.datetime_now, index=True)
    update_dttm = sa.Column(sa.DateTime, onupdate=options.datetime_now, index=True)
    delete_dttm = sa.Column(sa.DateTime, default=None, index=True)

    @classmethod
    def add_modification_history(
        cls, operation, table_name, request_id, data, result, who_user_id=""
    ):
        modification_history_id = cls.create(
            operation=operation,
            table_name=table_name,
            request_id=request_id,
            data=data,
            result=result,
            who_user_id=who_user_id,
        )
        return modification_history_id


class ProfileOperationHistory(BaseModel):
    __tablename__ = "profile_operation_history"
    __repr_attrs__ = ["url", "user_id"]

    profile_operation_history_id = sa.Column(sa.Integer, primary_key=True)
    operation = sa.Column(sa.String(255))
    url = sa.Column(sa.String(255))
    data = sa.Column(sa.Text, nullable=True)
    success = sa.Column(sa.Boolean, default=False)
    create_dttm = sa.Column(sa.DateTime, default=options.datetime_now, index=True)
    update_dttm = sa.Column(sa.DateTime, onupdate=options.datetime_now, index=True)
    delete_dttm = sa.Column(sa.DateTime, default=None, index=True)

    @classmethod
    def add_profile_operation_history(cls, operation, url, data, success):
        profile_operations_history_id = cls.create(
            operation=operation, url=url, data=data, success=success
        )
        return profile_operations_history_id


class TermsAndConditionAccept(BaseModel):
    __tablename__ = "terms_and_condition_accept"
    __repr_attrs__ = ["terms_and_condition_accept_id", "who_accept_user_id", "create_dttm"]

    terms_and_condition_accept_id = sa.Column(sa.Integer, primary_key=True)
    who_accept_user_id = sa.Column(sa.Integer, sa.ForeignKey("user.user_id"), default=None)
    create_dttm = sa.Column(sa.DateTime, default=options.datetime_now, index=True)
    update_dttm = sa.Column(sa.DateTime, onupdate=options.datetime_now, index=True)
    delete_dttm = sa.Column(sa.DateTime, default=None, index=True)


class Log(BaseModel):
    __tablename__ = "log"
    __repr_attrs__ = ["table_name", "operation", "user_id"]

    log_id = sa.Column(sa.Integer, primary_key=True)
    user_id = sa.Column(sa.Integer, sa.ForeignKey("user.user_id"))
    field_name = sa.Column(sa.String(128))
    success = sa.Column(sa.Boolean, default=False)
    data = sa.Column(sa.Text, nullable=True)
    message = sa.Column(sa.String(1024), nullable=True)
    operation = sa.Column(sa.String(127), nullable=True)

    create_dttm = sa.Column(sa.DateTime, default=options.datetime_now, index=True)
    update_dttm = sa.Column(sa.DateTime, onupdate=options.datetime_now, index=True)
    delete_dttm = sa.Column(sa.DateTime, default=None, index=True)


class MaritalStatus(BaseModel):
    """
    Options of marital status.
    """

    __tablename__ = "marital_status"

    marital_status_id = sa.Column(sa.Integer, primary_key=True)
    code = sa.Column(sa.String(45), unique=True)
    name = sa.Column(sa.String(45), unique=True)


class RateType(BaseModel):
    """
    Types of rate: fixed, variable, unknown, etc.
    """

    __tablename__ = "rate_type"

    rate_type_id = sa.Column(sa.Integer, primary_key=True)
    code = sa.Column(sa.String(45), unique=True)
    name = sa.Column(sa.String(45), unique=True)


class USConnection(BaseModel):
    """
    Options of connection to US: us_citizen, green_card, us_address, etc.
    """

    __tablename__ = "us_connection"

    us_connection_id = sa.Column(sa.Integer, primary_key=True)
    code = sa.Column(sa.String(45), unique=True)
    name = sa.Column(sa.String(45), unique=True)


class PlanDebtType(BaseModel):
    """
    Types of debts: mortgage, loans, credit_card, etc.
    """

    __tablename__ = "plan_debt_type"

    plan_debt_type_id = sa.Column(sa.Integer, primary_key=True)
    code = sa.Column(sa.String(45), unique=True)
    plan_debt_parent_type_id = sa.Column(
        sa.Integer, sa.ForeignKey("plan_debt_type.plan_debt_type_id"), nullable=True
    )
    name = sa.Column(sa.String(45))
    description = sa.Column(sa.Text, nullable=True)

    @classmethod
    def get_plan_debt_types_with_parent(cls):
        result = []
        parents = cls.instances_to_dict(
            cls.query.filter_by(plan_debt_parent_type_id=None).all(), include_ids=False
        )
        for parent in parents:
            result.append(
                {
                    parent["code"]: cls.instances_to_dict(
                        cls.query.filter_by(plan_debt_parent_type_id=parent["id"]).all(),
                        include_ids=False,
                    )
                }
            )
        return result


class UserCashflowType(BaseModel):
    """
    Type of cashflow: education, dependants, work, assets, etc.
    """

    __tablename__ = "user_cashflow_type"

    user_cashflow_type_id = sa.Column(sa.Integer, primary_key=True)
    code = sa.Column(sa.String(45), unique=True)
    name = sa.Column(sa.String(45), unique=True)


class RegularContributionType(BaseModel):
    __tablename__ = "regular_contribution_type"

    regular_contribution_type_id = sa.Column(sa.Integer, primary_key=True)
    code = sa.Column(sa.String(45), unique=True)
    name = sa.Column(sa.String(45), unique=True)


class GivingType(BaseModel):
    """
    Type of donations: charity, gifting.
    """

    __tablename__ = "giving_type"

    giving_type_id = sa.Column(sa.Integer, primary_key=True)
    code = sa.Column(sa.String(45), unique=True)
    name = sa.Column(sa.String(45), unique=True)


class ApproximateAge(BaseModel):
    __tablename__ = "approximate_age"

    approximate_age_id = sa.Column(sa.Integer, primary_key=True)
    code = sa.Column(sa.String(45), unique=True)
    name = sa.Column(sa.String(45), unique=True)


class TaxBenefit(BaseModel):
    __tablename__ = "tax_benefit"

    tax_benefit_id = sa.Column(sa.Integer, primary_key=True)
    code = sa.Column(sa.String(45), unique=True)
    name = sa.Column(sa.String(45), unique=True)
    description = sa.Column(sa.Text, nullable=True)


class AssetType(BaseModel):
    """
    Types of assets: pensions, properties, money, shares, etc.
    """

    __tablename__ = "asset_type"

    asset_type_id = sa.Column(sa.Integer, primary_key=True)
    asset_parent_type_id = sa.Column(
        sa.Integer, sa.ForeignKey("asset_type.asset_type_id"), nullable=True
    )
    code = sa.Column(sa.String(45), unique=True)
    name = sa.Column(sa.String(45))
    description = sa.Column(sa.Text, nullable=True)

    @classmethod
    def get_asset_types_with_parent(cls):
        result = []
        parents = cls.instances_to_dict(
            cls.query.filter_by(asset_parent_type_id=None).all(), include_ids=False
        )
        for parent in parents:
            result.append(
                {
                    parent["code"]: cls.instances_to_dict(
                        cls.query.filter_by(asset_parent_type_id=parent["id"]).all(),
                        include_ids=False,
                    )
                }
            )
        return result


class EmploymentType(BaseModel):
    """
    Employment type: employee, self-employed.
    """

    __tablename__ = "employment_type"

    employment_type_id = sa.Column(sa.Integer, primary_key=True)
    code = sa.Column(sa.String(45), unique=True)
    name = sa.Column(sa.String(45), unique=True)


class CurrentSituationType(BaseModel):
    """
    Current working situation of a person.
    """

    __tablename__ = "current_situation_type"

    current_situation_type_id = sa.Column(sa.Integer, primary_key=True)
    code = sa.Column(sa.String(45), unique=True)
    name = sa.Column(sa.String(45), unique=True)


class Frequency(BaseModel):
    """
    Simply frequency: monthly, yearly, etc.
    """

    __tablename__ = "frequency"

    frequency_id = sa.Column(sa.Integer, primary_key=True)
    code = sa.Column(sa.String(45), unique=True)
    name = sa.Column(sa.String(45), unique=True)


class PlanUserType(BaseModel):
    """
    Type of the user/person: main, child, partner, etc.
    """

    __tablename__ = "plan_user_type"

    plan_user_type_id = sa.Column(sa.Integer, primary_key=True)
    plan_user_parent_type_id = sa.Column(
        sa.Integer, sa.ForeignKey("plan_user_type.plan_user_type_id"), nullable=True
    )
    code = sa.Column(sa.String(45), unique=True)
    name = sa.Column(sa.String(45), unique=True)

    @classmethod
    def get_plan_user_types_with_parent(cls):
        result = []
        parents = cls.instances_to_dict(
            cls.query.filter_by(plan_user_parent_type_id=None).all(), include_ids=False
        )
        for parent in parents:

            if parent["code"] == "dependent":
                result.append(
                    {
                        parent["code"]: cls.instances_to_dict(
                            cls.query.filter_by(plan_user_parent_type_id=parent["id"]).all(),
                            include_ids=False,
                        )
                    }
                )
            else:
                result.append(parent)
        return result

    @classmethod
    def get_plan_user_types_without_main(cls):
        from utils.constants import MAIN_USER_TYPE

        all_types = cls.all_without_id_name()
        plan_user_types = []
        for item in all_types:

            if item["code"] != MAIN_USER_TYPE:
                plan_user_types.append(item)
        return plan_user_types

    # add the first user as main
    @classmethod
    def add_plan_user_type(cls, data, plan_id):
        from utils.constants import MAIN_USER_TYPE, UNIQUE_PLAN_USER_TYPES

        user_type = data.get("plan_user_type")
        if user_type in UNIQUE_PLAN_USER_TYPES and Plan.get_plan_user_types(plan_id, user_type):
            raise UniquePlanUserType(user_type)
        elif user_type and PlanUser.query.filter_by(plan_id=plan_id).count() > 0:
            return PlanUserType.get_id_by_code(user_type)
        elif not user_type and User.get_main_plan_user(plan_id):
            raise UniquePlanUserType(MAIN_USER_TYPE)
        else:
            return PlanUserType.get_id_by_code(MAIN_USER_TYPE)


class Plan(BaseModel):
    __tablename__ = "plan"

    plan_id = sa.Column(sa.Integer, primary_key=True)
    plan_uuid = sa.Column(sa.String(36))
    family_has_children = sa.Column(sa.Boolean, nullable=True)
    family_has_dependents = sa.Column(sa.Boolean, nullable=True)
    family_has_partner = sa.Column(sa.Boolean, nullable=True)
    family_has_nanny = sa.Column(sa.Boolean, nullable=True)
    family_has_debts = sa.Column(sa.Boolean, nullable=True)
    family_has_childcare = sa.Column(sa.Boolean, nullable=True)
    plan_has_other_income = sa.Column(sa.Boolean, nullable=True)
    partner_has_same_address = sa.Column(sa.Boolean, nullable=True)
    plan_has_charity = sa.Column(sa.Boolean, nullable=True)
    plan_has_gifting = sa.Column(sa.Boolean, nullable=True)
    expenditure_change_age = sa.Column(sa.Integer, nullable=True)
    expenditure_additional = sa.Column(sa.Text, nullable=True)
    has_insurance = sa.Column(sa.Boolean, nullable=True)
    insurance_additional = sa.Column(sa.Text, nullable=True)
    is_submitted = sa.Column(sa.Boolean, nullable=False, default=0)
    family_additional = sa.Column(sa.Text, nullable=True)
    marital_status_id = sa.Column(sa.Integer, sa.ForeignKey("marital_status.marital_status_id"))
    submit_dttm = sa.Column(sa.DateTime, default=None)
    create_dttm = sa.Column(sa.DateTime, default=options.datetime_now, index=True)
    update_dttm = sa.Column(sa.DateTime, onupdate=options.datetime_now, index=True)
    delete_dttm = sa.Column(sa.DateTime, default=None, index=True)

    marital_status = sa.orm.relationship("MaritalStatus")
    assets = sa.orm.relationship("PlanAsset", cascade="all,delete")
    cashflows = sa.orm.relationship("PlanCashflow", cascade="all,delete")
    plan_users = sa.orm.relationship("PlanUser", cascade="all,delete")
    debts = sa.orm.relationship("PlanDebt", cascade="all,delete")
    givings = sa.orm.relationship("PlanGiving", cascade="all,delete")

    @classmethod
    def delete_related(cls, data):
        from utils.constants import CHILD_USER_TYPE, DEPENDENT_TYPES, PARTNER_USER_TYPE

        try:
            if data.get("family_has_children") is False:
                children = (
                    cls.query.join(PlanUser, PlanUser.plan_id == Plan.plan_id)
                    .join(
                        PlanUserType, PlanUserType.plan_user_type_id == PlanUser.plan_user_type_id
                    )
                    .filter(PlanUserType.code == CHILD_USER_TYPE)
                    .with_entities(PlanUser)
                )
                for user in children:
                    PlanUser.remove_user(user.plan_user_id)
            if data.get("family_has_dependents") is False:
                dependents = (
                    cls.query.join(PlanUser.plan)
                    .join(
                        PlanUserType, PlanUser.plan_user_type_id == PlanUserType.plan_user_type_id
                    )
                    .filter(PlanUserType.code.in_(DEPENDENT_TYPES))
                    .with_entities(PlanUser)
                )
                for user in dependents:
                    PlanUser.remove_user(user.plan_user_id)
            if data.get("family_has_partner") is False:
                partner = (
                    cls.query.join(PlanUser, Plan.plan_id == PlanUser.plan_id)
                    .join(
                        PlanUserType, PlanUserType.plan_user_type_id == PlanUser.plan_user_type_id
                    )
                    .filter(PlanUserType.code == PARTNER_USER_TYPE)
                    .with_entities(PlanUser)
                    .first()
                )
                if partner:
                    PlanUser.remove_user(partner.plan_user_id)

            if data.get("plan_has_charity") is False:
                charity = (
                    cls.query.join(Plan.givings)
                    .join(PlanCashflow, PlanCashflow.plan_id == Plan.plan_id)
                    .join(GivingType, PlanGiving.giving_type_id == GivingType.giving_type_id)
                    .filter(GivingType.code == "charity")
                    .with_entities(PlanGiving)
                )
                for giving in charity:
                    PlanGiving.clear_table(plan_giving_id=giving.plan_giving_id)

            if data.get("plan_has_gifting") is False:
                charity = (
                    cls.query.join(Plan.givings)
                    .join(PlanCashflow, PlanCashflow.plan_id == Plan.plan_id)
                    .join(GivingType, PlanGiving.giving_type_id == GivingType.giving_type_id)
                    .filter(GivingType.code == "gifting")
                    .with_entities(PlanGiving)
                )
                for giving in charity:
                    PlanGiving.clear_table(plan_giving_id=giving.plan_giving_id)

            if data.get("family_has_nanny") is False:
                cashflows = (
                    cls.query.join(PlanCashflow.plan)
                    .join(
                        PlanUserCashflow,
                        PlanUserCashflow.plan_cashflow_id == PlanCashflow.plan_cashflow_id,
                    )
                    .join(
                        UserCashflowType,
                        UserCashflowType.user_cashflow_type_id
                        == PlanUserCashflow.user_cashflow_type_id,
                    )
                    .filter(UserCashflowType.code == "nanny")
                    .with_entities(PlanCashflow)
                )
                for cashflow in cashflows:
                    PlanCashflow.clear_table(plan_id=cashflow.plan_id)
            if data.get("family_has_childcare") is False:
                cashflows = (
                    cls.query.join(PlanCashflow.plan)
                    .join(
                        PlanUserCashflow,
                        PlanUserCashflow.plan_cashflow_id == PlanCashflow.plan_cashflow_id,
                    )
                    .join(
                        UserCashflowType,
                        UserCashflowType.user_cashflow_type_id
                        == PlanUserCashflow.user_cashflow_type_id,
                    )
                    .filter(UserCashflowType.code == "childcare")
                    .with_entities(PlanCashflow)
                )
                for cashflow in cashflows:
                    PlanCashflow.clear_table(plan_id=cashflow.plan_id)
            if data.get("family_has_insurance") is False:
                cashflows = (
                    cls.query.join(PlanCashflow.plan)
                    .join(
                        PlanUserCashflow,
                        PlanUserCashflow.plan_cashflow_id == PlanCashflow.plan_cashflow_id,
                    )
                    .join(
                        UserCashflowType,
                        UserCashflowType.user_cashflow_type_id
                        == PlanUserCashflow.user_cashflow_type_id,
                    )
                    .filter(UserCashflowType.code == "expenses_insurance")
                    .with_entities(PlanCashflow)
                )
                for cashflow in cashflows:
                    PlanCashflow.clear_table(plan_id=cashflow.plan_id)
            if data.get("plan_has_other_income") is False:
                cashflows = (
                    cls.query.join(PlanCashflow.plan)
                    .join(
                        PlanUserCashflow,
                        PlanUserCashflow.plan_cashflow_id == PlanCashflow.plan_cashflow_id,
                    )
                    .join(
                        UserCashflowType,
                        UserCashflowType.user_cashflow_type_id
                        == PlanUserCashflow.user_cashflow_type_id,
                    )
                    .filter(UserCashflowType.code == "other")
                    .with_entities(PlanCashflow)
                    .all()
                )
                for cashflow in cashflows:
                    PlanCashflow.clear_table(plan_id=cashflow.plan_id)
            cls.session.commit()
        except Exception:
            cls.session.rollback()
            raise

    @classmethod
    def upsert(cls, data, plan_uuid=None):
        plan = Plan.get_by_uuid(plan_uuid)
        result = data.copy()
        if data.get("marital_status"):
            result["marital_status_id"] = MaritalStatus.get_id_by_code(data["marital_status"])
        if plan:
            cls.delete_related(data)
            new_plan = Plan.update(plan["plan_id"], **result)
        else:
            new_plan = Plan.create_with_uuid(**result)
        return new_plan

    @classmethod
    def get_plan_by_main_user(cls, profile_uuid, to_dict=True):
        from utils.constants import MAIN_USER_TYPE

        result = cls.instances_to_dict(
            cls.query.join(PlanUser.plan)
            .join(User, User.user_id == PlanUser.user_id)
            .join(PlanUserType, PlanUserType.plan_user_type_id == PlanUser.plan_user_type_id)
            .filter(User.profile_uuid == profile_uuid)
            .filter(PlanUserType.code == MAIN_USER_TYPE)
            .first(),
            to_dict,
        )
        return result

    @classmethod
    def get_plan_list_by_user(cls, profile_uuid):
        result = cls.instances_to_dict(
            cls.query.join(PlanUser.plan)
            .join(User, User.user_id == PlanUser.user_id)
            .join(PlanUserType, PlanUserType.plan_user_type_id == PlanUser.plan_user_type_id)
            .filter(User.profile_uuid == profile_uuid)
            .order_by(Plan.update_dttm.desc())
        )
        return result

    @classmethod
    def get_plan_user_types(cls, plan_id, type):
        result = cls.instances_to_dict(
            cls.query.join(PlanUser.plan)
            .join(PlanUserType, PlanUserType.plan_user_type_id == PlanUser.plan_user_type_id)
            .filter(cls.plan_id == plan_id)
            .filter(PlanUserType.code == type)
            .all()
        )
        return result


class PlanCashflow(BaseModel):
    __tablename__ = "plan_cashflow"

    plan_cashflow_id = sa.Column(sa.Integer, primary_key=True)
    plan_cashflow_uuid = sa.Column(sa.String(36))
    amount = sa.Column(sa.Numeric(20, 2), nullable=True)
    frequency_id = sa.Column(sa.Integer, sa.ForeignKey("frequency.frequency_id"))
    plan_id = sa.Column(sa.Integer, sa.ForeignKey("plan.plan_id"))
    value_type = sa.Column(sa.Enum("F", "P"), nullable=True)
    duration = sa.Column(sa.Integer, nullable=True)
    is_indeterminate = sa.Column(sa.Boolean, nullable=True)
    plan_debt_id = sa.Column(sa.Integer, sa.ForeignKey("plan_debt.plan_debt_id"))
    plan_asset_id = sa.Column(sa.Integer, sa.ForeignKey("plan_asset.plan_asset_id"))
    plan_user_job_id = sa.Column(sa.Integer, sa.ForeignKey("plan_user_job.plan_user_job_id"))

    plan = sa.orm.relationship("Plan")
    frequency = sa.orm.relationship("Frequency")
    plan_user_cashflow = sa.orm.relationship(
        "PlanUserCashflow", uselist=False, cascade="all,delete"
    )

    @classmethod
    def upsert(cls, plan, data, plan_user_id, cashflow_id=None):
        # TODO: improve this method
        plan_cashflow = cls.create_or_update_cashflow(data, plan["plan_id"], cashflow_id)
        if data.get("other_income_type") or plan_user_id or data.get("user_cashflow_type"):
            plan_user_cashflow = PlanUserCashflow.query.filter_by(
                plan_cashflow_id=plan_cashflow["plan_cashflow_id"]
            ).first()
            id = plan_user_cashflow.plan_user_cashflow_id if plan_user_cashflow else None
            cls.upsert_other_income(data, plan_user_id, plan_cashflow["plan_cashflow_id"], id)
        return plan_cashflow

    @classmethod
    def create_or_update_cashflow(cls, data, plan_id, cashflow_id=None):
        # TODO: improve this method
        result = data.copy()
        result["plan_id"] = plan_id
        if data.get("frequency"):
            result["frequency_id"] = Frequency.get_id_by_code(data["frequency"])
        if cashflow_id:
            plan_cashflow = cls.update(cashflow_id, **result)
        else:
            plan_cashflow = cls.create_with_uuid(**result)
        return plan_cashflow

    @staticmethod
    def upsert_other_income(data, plan_user_id, plan_cashflow_id, plan_user_cashflow_id=None):
        plan_user_cashflow = {}
        if plan_cashflow_id:
            plan_user_cashflow["plan_cashflow_id"] = plan_cashflow_id
        if plan_user_id:
            plan_user_cashflow["plan_user_id"] = plan_user_id

        if data.get("user_cashflow_type"):
            plan_user_cashflow.update(
                {
                    "user_cashflow_type_id": UserCashflowType.get_id_by_code(
                        data["user_cashflow_type"]
                    )
                }
            )
        keys = ["other_income_type", "additional"]
        for key in keys:
            if key in data.keys():
                plan_user_cashflow[key] = data[key]
        if plan_user_cashflow_id:
            plan_user_cashflow = PlanUserCashflow.update(
                plan_user_cashflow_id, **plan_user_cashflow
            )
        else:
            plan_user_cashflow = PlanUserCashflow.create(**plan_user_cashflow)
        return plan_user_cashflow["plan_user_cashflow_id"]


class PlanUser(BaseModel):
    __tablename__ = "plan_user"

    plan_user_id = sa.Column(sa.Integer, primary_key=True)
    current_situation_type_id = sa.Column(
        sa.Integer, sa.ForeignKey("current_situation_type.current_situation_type_id"), nullable=True
    )
    plan_id = sa.Column(sa.Integer, sa.ForeignKey("plan.plan_id"), index=True)
    plan_user_type_id = sa.Column(
        sa.Integer, sa.ForeignKey("plan_user_type.plan_user_type_id"), index=True
    )
    plan_user_type_other = sa.Column(sa.String(45), nullable=True)
    user_id = sa.Column(sa.Integer, sa.ForeignKey("user.user_id"), index=True)
    retirement_age = sa.Column(sa.Integer, nullable=True)
    approximate_age_id = sa.Column(
        sa.Integer, sa.ForeignKey("approximate_age.approximate_age_id"), nullable=True, index=True
    )
    is_never_want_to_retire = sa.Column(sa.Boolean, default=None, nullable=True)
    additional_info = sa.Column(sa.Text, nullable=True)
    has_us_connections = sa.Column(sa.Boolean, nullable=True)
    has_job = sa.Column(sa.Boolean, nullable=True)
    address_uuid = sa.Column(sa.String(36), nullable=True)
    is_address_postal = sa.Column(sa.Boolean, nullable=True)
    is_not_sure_other_income = sa.Column(sa.Boolean, nullable=True)
    other_income = sa.Column(sa.Text, nullable=True)
    postal_address_uuid = sa.Column(sa.String(36), nullable=True)
    create_dttm = sa.Column(sa.DateTime, default=options.datetime_now, index=True)
    update_dttm = sa.Column(sa.DateTime, onupdate=options.datetime_now, index=True)
    delete_dttm = sa.Column(sa.DateTime, default=None, index=True)

    current_situation_type = sa.orm.relationship("CurrentSituationType")
    approximate_age = sa.orm.relationship("ApproximateAge")
    us_connections = sa.orm.relationship(
        "USConnection", secondary="user_us_connection", backref="plan_users"
    )
    plan_user_type = sa.orm.relationship("PlanUserType")
    plan = sa.orm.relationship("Plan")
    profile = sa.orm.relationship("User")
    jobs = sa.orm.relationship("PlanUserJob", cascade="all,delete")
    cashflow_changes = sa.orm.relationship(
        "PlanUserCashflowChange", uselist=False, cascade="all,delete"
    )
    plan_user_cashflow = sa.orm.relationship("PlanUserCashflow", cascade="all,delete")

    @classmethod
    def get_plan_user_by_profile_uuid(cls, profile_uuid, to_dict=True):
        plan_user = (
            cls.query.join(PlanUser.profile).filter(User.profile_uuid == profile_uuid).first()
        )
        if plan_user:
            return plan_user.to_dict() if to_dict else plan_user
        return plan_user

    @classmethod
    def upsert(cls, user_id, profile_id, data, plan_id):
        from utils.constants import OTHER_USER_TYPE

        # TODO: improve/get rid of this method
        result = data.copy()
        if data.get("current_situation_type"):
            result["current_situation_type_id"] = CurrentSituationType.get_id_by_code(
                data["current_situation_type"]
            )
        if data.get("approximate_age"):
            result["approximate_age_id"] = ApproximateAge.get_id_by_code(data["approximate_age"])

        if data.get("plan_user_type"):
            result["plan_user_type_id"] = PlanUserType.get_id_by_code(data["plan_user_type"])
            if data["plan_user_type"] != OTHER_USER_TYPE:
                result["plan_user_type_other"] = None

        plan_user = PlanUser.get_plan_user_by_profile_uuid(profile_id)
        if plan_user:
            plan_user = PlanUser.update(id=plan_user["plan_user_id"], commit=False, **result)
        else:
            result["user_id"] = user_id
            result["plan_id"] = plan_id
            plan_user = PlanUser.create(**result, commit=True)
        return plan_user["plan_user_id"]

    @classmethod
    def create_or_update_user(cls, data, plan_id, profile_id, user_id):
        # TODO: improve/get rid of this method
        try:
            plan_user_id = PlanUser.upsert(user_id, profile_id, data, plan_id)
            if "has_us_connections" in data.keys() and "us_connections" in data.keys():
                UserUSConnection.upsert(plan_user_id, data)
            if data.get("has_job") is False:
                PlanUserJob.clear_table(plan_user_id=plan_user_id, commit=False)
            if data.get("cashflow_changes"):
                PlanUserCashflowChange.upsert(plan_user_id, data["cashflow_changes"])
            cls.session.commit()
            return plan_user_id
        except Exception as e:
            cls.session.rollback()
            raise

    @classmethod
    def remove_user(cls, plan_user_id):
        plan_user = cls.get_by_id(plan_user_id)
        try:
            UserUSConnection.clear_table(plan_user_id=plan_user_id, commit=False)
            PlanAssetOwner.clear_table(plan_user_id=plan_user_id, commit=False)
            PlanDebtOwner.clear_table(plan_user_id=plan_user_id, commit=False)

            from libraries.profile import ProfileClient

            profile_uuid = User.get_plan_user_profile_uuid(plan_user_id)
            ProfileClient.delete(profile_uuid)
            cls.hard_delete(plan_user_id)
            User.hard_delete(plan_user["user_id"])
            cls.session.rollback()
        except Exception as e:
            cls.session.rollback()
            raise


class PlanDebt(BaseModel):
    __tablename__ = "plan_debt"

    plan_debt_id = sa.Column(sa.Integer, primary_key=True)
    plan_debt_uuid = sa.Column(sa.String(36))
    plan_debt_type_id = sa.Column(sa.Integer, sa.ForeignKey("plan_debt_type.plan_debt_type_id"))
    provider_name = sa.Column(sa.Text, nullable=True)
    outstanding_amount = sa.Column(sa.Numeric(20, 2), nullable=True)
    interest_rate = sa.Column(sa.Numeric(20, 2), nullable=True)
    rate_type_id = sa.Column(sa.Integer, sa.ForeignKey("rate_type.rate_type_id"), nullable=True)
    plan_id = sa.Column(sa.Integer, sa.ForeignKey("plan.plan_id"))
    additional = sa.Column(sa.Text, nullable=True)
    outstanding_term = sa.Column(sa.Integer, nullable=True)
    outstanding_term_months = sa.Column(sa.Integer, nullable=True)
    name = sa.Column(sa.Text, nullable=True)
    overpayment_amount = sa.Column(sa.Numeric(20, 2), nullable=True)
    has_monthly_overpayment = sa.Column(sa.Boolean, nullable=True)
    policy_number = sa.Column(sa.Text, nullable=True)
    address_uuid = sa.Column(sa.String(36), nullable=True)

    plan_debt_type = sa.orm.relationship("PlanDebtType")
    rate_type = sa.orm.relationship("RateType")
    cashflows = sa.orm.relationship("PlanCashflow", cascade="all,delete")

    @classmethod
    def delete_related(cls, plan_debt_id):
        try:
            PlanDebtOwner.clear_table(plan_debt_id=plan_debt_id, commit=False)
            cls.hard_delete(plan_debt_id, commit=True)
        except Exception:
            cls.session.rollback()
            raise

    @classmethod
    def get_plan_debt_by_owner(cls, plan_user_id, to_dict=True):
        result = cls.instances_to_dict(
            cls.query.join(PlanDebtOwner.plan_debt)
            .filter(PlanDebtOwner.plan_user_id == plan_user_id)
            .all(),
            to_dict,
        )
        return result

    @classmethod
    def upsert(cls, data, plan_id, debt_id=None):
        # TODO: improve/clean this method
        result = data.copy()
        result["plan_id"] = plan_id
        result["plan_debt_type_id"] = PlanDebtType.get_id_by_code(data["plan_debt_type"])
        if data.get("rate_type"):
            result["rate_type_id"] = RateType.get_id_by_code(data["rate_type"])
        if debt_id:
            plan_debt = cls.update(debt_id, **result)
        else:
            plan_debt = cls.create_with_uuid(**result)
        return plan_debt["plan_debt_id"], plan_debt["plan_debt_uuid"]


class UserUSConnection(BaseModel):
    __tablename__ = "user_us_connection"

    user_us_connection_id = sa.Column(sa.Integer, primary_key=True)

    plan_user_id = sa.Column(sa.Integer, sa.ForeignKey("plan_user.plan_user_id"))
    us_connection_id = sa.Column(sa.Integer, sa.ForeignKey("us_connection.us_connection_id"))

    @classmethod
    def upsert(cls, plan_user_id, data):
        cls.clear_table(plan_user_id=plan_user_id, commit=False)
        has_us_connections = data.get("has_us_connections")
        PlanUser.update(id=plan_user_id, has_us_connections=has_us_connections, commit=False)
        ret = []
        if has_us_connections:
            for connection in data.get("us_connections", []):
                connection_id = USConnection.get_id_by_code(code=connection)
                uc = cls.create(
                    plan_user_id=plan_user_id, us_connection_id=connection_id, commit=False
                )
                ret.append(uc)
        return ret


class PlanUserCashflowChange(BaseModel):
    __tablename__ = "plan_user_cashflow_change"

    plan_user_cashflow_change_id = sa.Column(sa.Integer, primary_key=True)

    plan_user_id = sa.Column(sa.Integer, sa.ForeignKey("plan_user.plan_user_id"))
    description = sa.Column(sa.Text, nullable=True)

    @classmethod
    def upsert(cls, plan_user_id, changes):
        # TODO: improve/clean this method
        cashflow_changes = PlanUserCashflowChange.query.filter_by(plan_user_id=plan_user_id).first()
        if cashflow_changes:
            cashflow_changes.description = changes
            cls.session.add(cashflow_changes)
            cls.session.commit()
            return cashflow_changes.to_dict()
        else:
            return PlanUserCashflowChange.create(
                plan_user_id=plan_user_id, description=changes, commit=False
            )

    @classmethod
    def get_description_by_user(cls, plan_user_id):
        result = cls.query.filter_by(plan_user_id=plan_user_id).first()
        return result.description if result else None


class PlanUserCashflow(BaseModel):
    __tablename__ = "plan_user_cashflow"

    plan_user_cashflow_id = sa.Column(sa.Integer, primary_key=True)
    plan_user_id = sa.Column(sa.Integer, sa.ForeignKey("plan_user.plan_user_id"))
    plan_cashflow_id = sa.Column(sa.Integer, sa.ForeignKey("plan_cashflow.plan_cashflow_id"))
    user_cashflow_type_id = sa.Column(
        sa.Integer, sa.ForeignKey("user_cashflow_type.user_cashflow_type_id")
    )
    other_income_type = sa.Column(sa.Text, nullable=True)
    additional = sa.Column(sa.Text, nullable=True)

    plan_cashflow = sa.orm.relationship("PlanCashflow", uselist=False, cascade="delete,all")
    user_cashflow_type = sa.orm.relationship("UserCashflowType")


class PlanGiving(BaseModel):
    __tablename__ = "plan_giving"

    plan_giving_id = sa.Column(sa.Integer, primary_key=True)
    plan_giving_uuid = sa.Column(sa.String(36))
    plan_id = sa.Column(sa.Integer, sa.ForeignKey("plan.plan_id"))
    giving_type_id = sa.Column(sa.Integer, sa.ForeignKey("giving_type.giving_type_id"))
    plan_cashflow_id = sa.Column(
        sa.Integer, sa.ForeignKey("plan_cashflow.plan_cashflow_id"), nullable=True
    )

    giving_type = sa.orm.relationship("GivingType")
    plan_cashflow = sa.orm.relationship("PlanCashflow", cascade="all,delete")

    @classmethod
    def upsert(cls, data, plan_id, giving_id=None):
        # TODO: improve/clean this method
        result = data.copy()
        plan_cashflow = PlanCashflow.get_by_uuid(data.get("plan_cashflow_uuid"))
        result["plan_cashflow_id"] = plan_cashflow["plan_cashflow_id"] if plan_cashflow else None
        result["giving_type_id"] = GivingType.get_id_by_code(data["giving_type"])
        result["plan_id"] = plan_id
        if giving_id:
            plan_giving = cls.update(giving_id, **result)
        else:
            plan_giving = cls.create_with_uuid(**result)
        return plan_giving["plan_giving_uuid"]


class PlanAsset(BaseModel):
    __tablename__ = "plan_asset"

    plan_asset_id = sa.Column(sa.Integer, primary_key=True)
    plan_asset_uuid = sa.Column(sa.String(36))
    asset_type_id = sa.Column(sa.Integer, sa.ForeignKey("asset_type.asset_type_id"))
    plan_id = sa.Column(sa.Integer, sa.ForeignKey("plan.plan_id"), nullable=True)
    asset_type_other = sa.Column(sa.Text, nullable=True)
    is_tenants_in_common = sa.Column(sa.Boolean, default=0)
    additional = sa.Column(sa.Text, nullable=True)
    provider_name = sa.Column(sa.Text)
    current_value = sa.Column(sa.Numeric(20, 2))
    time_spent_working = sa.Column(sa.Integer, nullable=True)
    expected_retirement = sa.Column(sa.Date, nullable=True)
    tax_benefit_id = sa.Column(
        sa.Integer, sa.ForeignKey("tax_benefit.tax_benefit_id"), nullable=True
    )
    current_tax_year_contributions = sa.Column(sa.Numeric(20, 2), nullable=True)
    is_self_managed = sa.Column(sa.Boolean, default=0)
    policy_number = sa.Column(sa.Text, nullable=True)
    address_uuid = sa.Column(sa.String(36), nullable=True)
    is_not_sure_asset_type = sa.Column(sa.Boolean, nullable=True)
    is_not_sure_employer_contribution = sa.Column(sa.Boolean, nullable=True)
    is_not_sure_regular_contribution = sa.Column(sa.Boolean, nullable=True)
    is_not_sure_receive_income = sa.Column(sa.Boolean, nullable=True)
    is_indeterminate_period = sa.Column(sa.Boolean, nullable=True)

    asset_type = sa.orm.relationship("AssetType")
    plan_cashflow = sa.orm.relationship("PlanCashflow", cascade="all,delete")
    tax_benefit = sa.orm.relationship("TaxBenefit")
    regular_contributions = sa.orm.relationship("RegularContribution", cascade="all,delete")

    @classmethod
    def delete_related(cls, plan_asset_id):
        try:
            PlanAssetOwner.clear_table(plan_asset_id=plan_asset_id, commit=False)
            cls.hard_delete(plan_asset_id, commit=True)
        except Exception:
            cls.session.rollback()
            raise

    @classmethod
    def upsert(cls, data, plan_id, plan_asset_id=None):
        # TODO: improve/clean this method
        result = data.copy()
        result["plan_id"] = plan_id
        result["asset_type_id"] = AssetType.get_id_by_code(data["asset_type"])
        if data.get("tax_benefit"):
            result["tax_benefit_id"] = TaxBenefit.get_id_by_code(data["tax_benefit"])
        if plan_asset_id:
            plan_asset = PlanAsset.update(plan_asset_id, **result)
        else:
            plan_asset = PlanAsset.create_with_uuid(**result)
        return plan_asset

    @classmethod
    def get_assets_by_plan(cls, plan_id):
        return cls.query.filter_by(plan_id=plan_id).all()

    @classmethod
    def get_assets_by_type(cls, asset_type_id):
        return cls.query.filter_by(asset_type_id=asset_type_id).all()

    @classmethod
    def get_assets_by_owner(cls, plan_user_id, to_dict=True):
        result = cls.instances_to_dict(
            cls.query.join(PlanAssetOwner.plan_asset)
            .filter(PlanAssetOwner.plan_user_id == plan_user_id)
            .all(),
            to_dict,
        )
        return result


class PlanDebtOwner(BaseModel):
    __tablename__ = "plan_debt_owner"

    plan_debt_owner_id = sa.Column(sa.Integer, primary_key=True)
    plan_debt_id = sa.Column(sa.Integer, sa.ForeignKey("plan_debt.plan_debt_id"))
    plan_user_id = sa.Column(sa.Integer, sa.ForeignKey("plan_user.plan_user_id"))

    plan_debt = sa.orm.relationship("PlanDebt")
    plan_user = sa.orm.relationship("PlanUser")

    @classmethod
    def upsert(cls, profile_uuid, plan_debt_id):
        # TODO: improve/clean this method
        plan_user = PlanUser.get_plan_user_by_profile_uuid(profile_uuid)
        plan_user_id = plan_user["plan_user_id"] if plan_user else None
        if (
            PlanDebtOwner.query.filter_by(
                plan_user_id=plan_user_id, plan_debt_id=plan_debt_id
            ).count()
            == 0
        ):
            PlanDebtOwner.create(plan_user_id=plan_user_id, plan_debt_id=plan_debt_id)


class RegularContribution(BaseModel):
    __tablename__ = "regular_contribution"

    regular_contribution_id = sa.Column(sa.Integer, primary_key=True)
    regular_contribution_uuid = sa.Column(sa.String(36))
    regular_contribution_type_id = sa.Column(
        sa.Integer, sa.ForeignKey("regular_contribution_type.regular_contribution_type_id")
    )
    frequency_id = sa.Column(sa.Integer, sa.ForeignKey("frequency.frequency_id"))
    plan_asset_id = sa.Column(sa.Integer, sa.ForeignKey("plan_asset.plan_asset_id"))
    amount = sa.Column(sa.Numeric(20, 2))
    value_type = sa.Column(sa.Enum("F", "P"), nullable=True)

    frequency = sa.orm.relationship("Frequency")
    regular_contribution_type = sa.orm.relationship("RegularContributionType")

    @classmethod
    def upsert(cls, data, plan_asset_id=None):
        # TODO: improve/clean this method
        result = data.copy()
        if plan_asset_id:
            result["plan_asset_id"] = plan_asset_id
        if data.get("frequency"):
            result["frequency_id"] = Frequency.get_id_by_code(data["frequency"])
        result["regular_contribution_type_id"] = RegularContributionType.get_id_by_code(
            data["regular_contribution_type"]
        )
        reg = RegularContribution.get_by_uuid(result.get("regular_contribution_uuid"))
        if "regular_contribution_uuid" in data.keys() and reg:
            regular_contribution = RegularContribution.update(
                id=reg["regular_contribution_id"], **result
            )
        else:
            regular_contribution = RegularContribution.create_with_uuid(**result)
        return regular_contribution["regular_contribution_uuid"]


class PlanAssetOwner(BaseModel):
    __tablename__ = "plan_asset_owner"

    plan_asset_owner_id = sa.Column(sa.Integer, primary_key=True)
    plan_user_id = sa.Column(sa.Integer, sa.ForeignKey("plan_user.plan_user_id"))
    plan_asset_id = sa.Column(sa.Integer, sa.ForeignKey("plan_asset.plan_asset_id"))

    plan_user = sa.orm.relationship("PlanUser")
    plan_asset = sa.orm.relationship("PlanAsset")

    @classmethod
    def upsert(cls, profile_uuid, plan_asset_id):
        # TODO: improve/clean this method
        plan_user = PlanUser.get_plan_user_by_profile_uuid(profile_uuid)
        plan_user_id = plan_user["plan_user_id"] if plan_user else None
        if (
            PlanAssetOwner.query.filter_by(
                plan_user_id=plan_user_id, plan_asset_id=plan_asset_id
            ).count()
            == 0
        ):
            PlanAssetOwner.create(plan_user_id=plan_user_id, plan_asset_id=plan_asset_id)


class PlanUserJob(BaseModel):
    __tablename__ = "plan_user_job"

    plan_user_job_id = sa.Column(sa.Integer, primary_key=True)
    plan_user_job_uuid = sa.Column(sa.String(36))
    is_company_director = sa.Column(sa.Boolean, nullable=True)
    has_remuneration = sa.Column(sa.Boolean, nullable=True)
    company = sa.Column(sa.String(125))
    additional_info = sa.Column(sa.Text, nullable=True)
    salary_additional = sa.Column(sa.Text, nullable=True)
    plan_user_id = sa.Column(sa.Integer, sa.ForeignKey("plan_user.plan_user_id"))
    employment_type_id = sa.Column(
        sa.Integer, sa.ForeignKey("employment_type.employment_type_id"), nullable=True
    )

    plan_user = sa.orm.relationship("PlanUser")
    plan_cashflow = sa.orm.relationship(
        "PlanCashflow", cascade="all,delete", backref="plan_user_job"
    )
    employment_type = sa.orm.relationship("EmploymentType")

    @classmethod
    def upsert(cls, data, job_id=None):
        # TODO: improve/clean this method
        result = data.copy()
        result["employment_type_id"] = EmploymentType.get_id_by_code(data["employment_type"])
        plan_user = PlanUser.get_plan_user_by_profile_uuid(data.get("profile_uuid"))
        if plan_user:
            result["plan_user_id"] = plan_user["plan_user_id"]
        if job_id:
            user_job = cls.update(job_id, **result)
        else:
            user_job = cls.create_with_uuid(**result)
        return user_job["plan_user_job_uuid"]


class AmlCase(BaseModel):
    """
    Base model for the Anti Money Laundering (AML) check.
    Stores the case_id generate by the aml checking system and
    this system profile_uuid.
    """

    __tablename__ = "aml_kyc_case"
    __table_args__ = (sa.Index("ix_aml_kyc_case_is_newest_concluded", "user_id", "updated"),)

    case_id = sa.Column(sa.String(36), primary_key=True)
    user_id = sa.Column(sa.Integer, sa.ForeignKey("user.user_id"), nullable=False, index=True)
    is_enabled = sa.Column(sa.Boolean, nullable=False)

    created = sa.Column(sa.DateTime, nullable=False, index=True)
    updated = sa.Column(sa.DateTime, nullable=False, index=True)

    user = sa.orm.relationship(
        "User", uselist=False, back_populates="amlcases", foreign_keys=[user_id]
    )
    documents = sa.orm.relationship("AmlDocument", uselist=True)
    results = sa.orm.relationship(
        "AmlResult",
        uselist=True,
        back_populates="case",
        lazy="dynamic",  # allows the use of .filter
    )

    @classmethod
    def is_newest_concluded(self, user_id):
        """
        Checks if the newest AmlCase associated to the user with user_id
        has all AmlResults with concluded status (pass or fail).
        Returns True if all concluded.
        """

        case = (
            AmlCase.query.filter(AmlCase.user_id == user_id)
            .order_by(AmlCase.updated.desc())
            .first()
        )
        if case:
            results_count = case.results.filter(AmlResult.status.in_(["pass", "fail"])).count()
            return results_count == 0
        return True

    @classmethod
    def newest_results(self, **kw):
        """
        Retrives the newest results associated to the newest amlcase.
        """
        template = {"ts": None, "htar": None, "wc": None}
        case = (
            AmlCase.query(AmlCase.case_id).filter_by(**kw).order_by(AmlCase.updated.desc()).first()
        )
        if case:
            results = (
                AmlResult.query(
                    AmlResult.provider,
                    sa.func.max(AmlResult.updated).label("updated"),
                    AmlResult.case_id,
                    AmlResult.status,
                )
                .filter(AmlResult.case_id == case.case_id)
                .group_by(AmlResult.provider, AmlResult.case_id, AmlResult.status)
            )
            template.update(
                {
                    res.provider.lower(): {"updated": res.updated, "status": res.status}
                    for res in results
                }
            )
        return template


class AmlDocument(BaseModel):
    """
    Stores an url to a document provided in an AML case check.
    """

    __tablename__ = "aml_kyc_document"

    document_id = sa.Column(sa.Integer, primary_key=True)
    case_id = sa.Column(
        sa.String(36), sa.ForeignKey("aml_kyc_case.case_id"), nullable=False, index=True
    )
    type = sa.Column(sa.Enum("passport", "driver_licence"), nullable=False, comment="document type")
    url = sa.Column(sa.Text, nullable=False, comment="document url")

    created = sa.Column(sa.DateTime, nullable=False, index=True)
    updated = sa.Column(sa.DateTime, nullable=False, index=True)


class AmlResult(BaseModel):
    """
    Stores the updates/results on the AML/KYC check.
    """

    __tablename__ = "aml_kyc_result"
    __table_args__ = (
        sa.Index("ix_aml_kyc_case_newest_results", "provider", "case_id", "status", "updated"),
    )

    result_id = sa.Column(sa.Integer, primary_key=True)
    case_id = sa.Column(
        sa.String(36), sa.ForeignKey("aml_kyc_case.case_id"), nullable=False, index=True
    )
    provider = sa.Column(sa.Enum("TS", "WC", "HTAR"), nullable=False)
    status = sa.Column(
        sa.Enum("pass", "fail", "refer", "pending"),
        nullable=False,
        comment="pass: approved; fail: failed; pending; refer: needs more data, manual checking, etc",
        index=True,
    )

    valid_until = sa.Column(sa.DateTime, nullable=False, comment="result valid until time")
    url = sa.Column(sa.Text, nullable=True, comment="url to the result")
    pep = sa.Column(sa.Boolean, nullable=True, comment="Political Exposed Person")

    bank_account_valid = sa.Column(sa.Text, nullable=True)
    trigger = sa.Column(sa.Text, nullable=False)

    bank_account_verification = sa.orm.relationship(
        "AmlResultBankAccount", uselist=False, back_populates="result"
    )
    case = sa.orm.relationship("AmlCase", uselist=True, back_populates="results")

    archived_at = sa.Column(sa.String, nullable=True, index=True)
    created = sa.Column(sa.DateTime, nullable=False, index=True)
    updated = sa.Column(sa.DateTime, nullable=False, index=True)

    @classmethod
    def concludeds(cls, case_id):
        """
        Makes a set with all providers which check have already been completed.
        """
        return set(
            map(
                lambda x: x.provider,
                cls.query(cls.provider)
                .filter(cls.case_id == case_id)
                .filter(cls.status.in_("pass", "fail")),
            )
        )


class AmlResultBankAccount(BaseModel):
    """
    Store the bank account information that might come attached to an AmlResult.
    """

    __tablename__ = "aml_kyc_result_bank_account"

    result_id = sa.Column(sa.Integer, sa.ForeignKey("aml_kyc_result.result_id"), primary_key=True)

    sort_code_valid = sa.Column(sa.Boolean, nullable=True)
    account_number_valid = sa.Column(sa.Boolean, nullable=True)
    account_name_valid = sa.Column(sa.Boolean, nullable=True)
    account_address_valid = sa.Column(sa.Boolean, nullable=True)
    account_status = sa.Column(sa.Boolean, nullable=True)
    error = sa.Column(sa.Text, nullable=True)

    result = sa.orm.relationship("AmlResult", back_populates="bank_account_verification")


"""

Soft Fact tables

"""


class SoftFactTagJoint(BaseModel):
    """
    Store tags for soft facts
    """

    __tablename__ = "soft_fact_tag_joint"

    soft_fact_tag_joint_id = sa.Column(sa.Integer, primary_key=True)
    soft_fact_tag_id = sa.Column(
        sa.Integer, sa.ForeignKey("soft_fact_tag.soft_fact_tag_id"), nullable=False
    )
    plan_soft_fact_id = sa.Column(
        sa.Integer, sa.ForeignKey("plan_soft_fact.plan_soft_fact_id"), nullable=False
    )


class SoftFactTag(BaseModel):
    """
    Store tags for soft facts
    """

    __tablename__ = "soft_fact_tag"

    soft_fact_tag_id = sa.Column(sa.Integer, primary_key=True)
    name = sa.Column(sa.String(255))
    code = sa.Column(sa.String(255), unique=True)

    soft_facts = sa.orm.relationship(
        "PlanSoftFact", secondary="soft_fact_tag_joint", backref="tags"
    )


class PlanSoftFact(BaseModel):
    """
    Store soft facts
    """

    __tablename__ = "plan_soft_fact"

    plan_soft_fact_id = sa.Column(sa.Integer, primary_key=True)
    plan_soft_fact_uuid = sa.Column(sa.String(36), nullable=False)
    comment = sa.Column(sa.Text)
    user_id = sa.Column(sa.Integer, sa.ForeignKey("user.user_id"), nullable=False)
    plan_id = sa.Column(sa.Integer, sa.ForeignKey("plan.plan_id"), nullable=False)
    create_dttm = sa.Column(sa.DateTime, default=options.datetime_now, index=True)
    update_dttm = sa.Column(sa.DateTime, onupdate=options.datetime_now, index=True)

    soft_fact_tags = sa.orm.relationship(
        "SoftFactTag", secondary="soft_fact_tag_joint", backref="plan_soft_facts"
    )

    user = sa.orm.relationship("User")

    @classmethod
    def upsert(cls, data, user_id, plan_soft_fact_id=None):
        # TODO: improve/clean this method
        result = data.copy()
        result["user_id"] = user_id
        if plan_soft_fact_id:
            SoftFactTagJoint.clear_table(plan_soft_fact_id=plan_soft_fact_id, commit=False)
            soft_fact = cls.update(plan_soft_fact_id, **result)
        else:
            soft_fact = cls.create_with_uuid(**result)
        for tag in data.get("tags", []):
            SoftFactTagJoint.create(
                **{
                    "soft_fact_tag_id": SoftFactTag.get_id_by_code(tag),
                    "plan_soft_fact_id": soft_fact["plan_soft_fact_id"],
                }
            )
        return soft_fact["plan_soft_fact_uuid"]

    @classmethod
    def delete(cls, plan_soft_fact_id):
        SoftFactTagJoint.clear_table(plan_soft_fact_id=plan_soft_fact_id, commit=False)
        PlanSoftFact.clear_table(plan_soft_fact_id=plan_soft_fact_id)

    @classmethod
    def filter_by_tags(cls, plan_id, tags):
        result = cls.instances_to_dict(
            cls.query.join(SoftFactTag.soft_facts)
            .filter(cls.plan_id == plan_id)
            .filter(SoftFactTag.code.in_(tags))
            .all(),
            False,
        )
        return result


class PlanDocument(BaseModel):
    """
    Store documents
    """

    __tablename__ = "plan_document"

    plan_document_id = sa.Column(sa.Integer, primary_key=True)
    plan_document_uuid = sa.Column(sa.String(36), nullable=False)
    type = sa.Column(sa.String(255))
    filename = sa.Column(sa.String(255))
    url = sa.Column(sa.String(255))
    size = sa.Column(sa.Integer)
    asset_id = sa.Column(sa.Integer, sa.ForeignKey("plan_asset.plan_asset_id"), nullable=True)
    plan_id = sa.Column(sa.Integer, sa.ForeignKey("plan.plan_id"), nullable=False)
    create_dttm = sa.Column(sa.DateTime, default=options.datetime_now())
    update_dttm = sa.Column(sa.DateTime, onupdate=options.datetime_now())
    delete_dttm = sa.Column(sa.DateTime, onupdate=options.datetime_now())

    plan = sa.orm.relationship("Plan")
    plan_asset = sa.orm.relationship("PlanAsset")
