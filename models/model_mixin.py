import uuid
import re

from sqlalchemy.exc import IntegrityError
from tornado.options import options

from models.session import Session


class DoesNotExist(Exception):
    def __init__(self, class_name, **params):
        self.message = "{0} with provided params not found:{1}".format(class_name, params)
        super().__init__(self.message)


class FieldError(TypeError):
    def __init__(self, message):
        self.message = message
        super().__init__(self.message)


class ModelMixin:
    session = Session()

    @classmethod
    def pk(cls):
        return "{}_id".format(cls.__tablename__)

    def to_dict(self, query_instance=None, include_ids=True, uuids=False, dttm=True):
        """
        Converts an instance to a dictionary.
        `query_instance`: bases the values extraction on a query
        `include_ids`: boolean to add the id values as a key called `id`
        `uuids`: boolean to convert or not the keys ending in *_id* to *_uuid*
        `dttm`: boolean to show or hide the keys ending in *dttm* (datetime columns)
        """

        def col_func(key):
            """
            Auxiliary method to do apply the uuids logic.
            """
            return re.sub(r"_id", "_uuid", key) if uuids else key

        if hasattr(self, "__table__"):
            result = {col_func(c.name): getattr(self, c.name, None) for c in self.__table__.columns}
        else:
            cols = query_instance.column_descriptions
            result = {col_func(cols[i]["name"]): self.__dict__[i] for i in range(len(cols))}
        if not dttm:
            result = {k: v for k, v in result.items() if not k.endswith("_dttm")}
        if not include_ids:
            result["id"] = result.pop(self.pk())
        return result

    def from_dict(self, dict):
        for c in self.__table__.columns:
            setattr(self, c.name, dict[c.name])

    def get(self, key):
        # TODO: kill me
        return getattr(self, key, None)

    @classmethod
    def get_by_id(cls, id, to_dict=True, raise_exception=False):
        if id:
            if isinstance(id, cls):
                # if it's an instance of the class
                # well, we already have the instance
                return id

            # TODO impove this - looks hacky
            result = cls.query.filter_by(**{cls.pk(): id}).first()
            if not result:
                if raise_exception:
                    raise DoesNotExist(cls.__name__, id=id)
                else:
                    return None
            if to_dict:
                result = result.to_dict()
            return result
        return None

    @classmethod
    def all(cls):
        return cls.instances_to_dict(cls.query.all())

    @classmethod
    def exists(cls, **kw):
        """
        Issues an SELECT EXISTS query. It's the fastest way to check if some object is present in the database.
        Returns bool.
        """
        return cls.session.query(cls.query.filter_by(**kw).exists()).scalar()

    @classmethod
    def all_without_id_name(cls):
        return cls.instances_to_dict(cls.query.all(), False)

    @classmethod
    def instances_to_dict(cls, instances_list, include_ids=True):
        if not instances_list:
            return []
        return [inst.to_dict(include_ids=include_ids) for inst in instances_list]

    @classmethod
    def create_with_uuid(cls, **kwargs):
        id = "{}_uuid".format(cls.__tablename__)
        if id in cls.__table__.columns:
            kwargs[id] = str(uuid.uuid4())
        return cls.create(**kwargs)

    @classmethod
    def upsert(cls, pk_list, data, commit=False):
        """
        Updates or creates a new object.
        `pk_list` means primary keys list. It can be a list/tuple or a single value to query on.
        `data` is a dict with all data to fill the fields.
        `commit` flags if the operation should be commited, or not.
        """
        if isinstance(pk_list, (tuple, list)):
            inst = cls.query.filter_by(*pk_list).first()
        else:
            inst = cls.query.get(pk_list)
        if inst is None:
            inst = cls()

        try:
            for key, value in data.items():
                if key in cls.__table__.columns:
                    setattr(inst, key, value)
            cls.session.add(inst)
            if commit:
                cls.session.commit()
        except (IntegrityError, ValueError):
            raise
        except Exception:
            cls.session.rollback()
        return inst

    @classmethod
    def create(cls, commit=True, close=False, to_dict=True, **kwargs):
        args = {k: v for k, v in kwargs.items() if k in cls.__table__.columns}
        inst = cls(**args)
        try:
            cls.session.add(inst)
            if commit:
                cls.session.commit()
                cls.session.refresh(inst)
        except (TypeError, IntegrityError) as err:
            raise FieldError(err.args[0])
        except Exception:
            cls.session.rollback()
        if close:
            cls.session.close()
        result = inst
        if to_dict:
            result = inst.to_dict()
        return result

    @classmethod
    def update(cls, id, commit=True, close=False, **kwargs):
        inst = cls.get_by_id(id, to_dict=False)
        try:
            for key, value in kwargs.items():
                if key in cls.__table__.columns:
                    setattr(inst, key, value)
            if commit:
                cls.session.commit()
                cls.session.refresh(inst)
        except (IntegrityError, ValueError) as err:
            raise FieldError(err.args[0])
        except Exception:
            cls.session.rollback()
        if close:
            cls.session.close()
        return inst.to_dict()

    @classmethod
    def undelete(cls, id, commit=True, close=False):
        inst = cls.get_by_id(id, to_dict=False)
        setattr(inst, "delete_dttm", None)
        if commit:
            cls.session.commit()
        if close:
            cls.session.close()
        return inst.to_dict()

    @classmethod
    def delete(cls, id, commit=True, close=False):
        inst = cls.get_by_id(id, to_dict=False)
        setattr(inst, "delete_dttm", options.datetime_now())
        if commit:
            cls.session.commit()
        if close:
            cls.session.close()
        return inst.to_dict()

    @classmethod
    def hard_delete(cls, id, commit=True, close=False):
        inst = cls.get_by_id(id, to_dict=False)
        cls.session.delete(inst)
        if commit:
            cls.session.commit()
        if close:
            cls.session.close()

    @classmethod
    def get_by_uuid(cls, uuid, to_dict=True):
        id = "{}_uuid".format(cls.__tablename__)
        kwargs = {}
        if id in cls.__table__.columns:
            kwargs[id] = uuid
        inst = cls.query.filter_by(**kwargs).first()
        if to_dict:
            return inst.to_dict() if inst else None
        else:
            return inst

    @classmethod
    def clear_table(cls, commit=True, close=False, **kwargs):
        result = cls.query.filter_by(**kwargs).all()
        for item in result:
            cls.session.delete(item)
        if commit:
            cls.session.commit()
        if close:
            cls.session.close()
        return None

    @classmethod
    def run_sql(cls, query, to_dict=True):
        engine = Session()._engine
        with engine.connect() as con:
            result = con.execute(query)
        if not result:
            raise DoesNotExist(cls.__name__, id=id)
        if to_dict:
            result = cls.instances_to_dict(result)
        return result

    @classmethod
    def get_id_by_code(cls, code):
        if "code" in cls.__table__.columns:
            result = cls.query.filter_by(code=code).first()
            return getattr(result, cls.pk()) if result else None
        else:
            raise FieldError("{} table hasn't code field".format(cls.__table__.name))

    @classmethod
    def get_code_by_id(cls, id):
        if id:
            if "code" in cls.__table__.columns:
                result = cls.get_by_id(id)
                return result["code"] if result else None
            else:
                raise FieldError("{} table hasn't code field".format(cls.__table__.name))
