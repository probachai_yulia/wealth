from datetime import datetime
import logging as log
import os

import pytz
from tornado.options import define, options, parse_command_line, parse_config_file


def initialize(app_env=None, args=None):
    if app_env is None:
        app_env = {}
    if not get_env() == "test":
        parse_command_line(args, final=False)
        set_env(environments)
        if app_env:
            set_env(app_env)
        parse_command_line(args, final=False)
    else:
        set_env(environments)
    import_file_configs()


def import_file_configs():
    config_file = "./config/octopus-wealth-fact_%s.conf" % get_env()
    try:
        parse_config_file(config_file)
    except FileNotFoundError as e:
        log.info(f"config file {config_file} not found")
        # make config file optional for local environments
        if get_env() not in ("local", "test"):
            raise e


def get_env():
    """
    Get the current running environment option.
    """
    if options.env:
        return options.env
    elif os.getenv("WEALTH_ENV"):
        options.env = os.getenv("WEALTH_ENV")
        return os.getenv("WEALTH_ENV")
    else:
        log.warning(
            "Couldn't determine environment, exiting. Please 'export WEALTH_ENV=local' or specify '--env=local'"
        )


def set_env(environments):
    """
    Assign all the config options to the tornado.options singleton.
    """
    env_options = environments.get(get_env(), {})
    for setting, value in list(env_options.items()):
        if hasattr(options, setting):
            setattr(options, setting, value)


def define_options():
    define("defined", True)

    # Environment
    define(
        "env",
        default=os.getenv("WEALTH_ENV", "local"),
        help="[Required or set WEALTH_ENV] Environment (local, dev, test, prod)",
        type=str,
    )
    define(
        "no_access_noise",
        help="Disables messages like 'WARNING:tornado.access:404 GET /favicon.ico'",
        default=False,
        type=bool,
    )
    define("config", default=None, help="Set path to system configuration file", type=str)
    define("debug_mode", default=True)
    define("autoreload", default=False)
    define("basedir", default=os.path.abspath(os.path.dirname(__file__)))
    define(
        "fact_find_enabled",
        default=os.getenv("FACT_FIND_ENABLED", True),
        help="Enables or disables endpoints to avoid being accessible in production during development",
        type=bool,
    )
    define("static_path", default=os.path.join(os.path.dirname(__file__), "static"))
    define("exception_email_addr", default="errors@octopuslabs.com")
    define("port", 80)

    bst = pytz.timezone("Europe/London")
    define("datetime_now", default=lambda: bst.localize(datetime.now()).astimezone(bst))

    define("include_google_tag_manager", default=False)

    # OAuth settings
    define("oauth_server_url", default="https://auth-dev.octopusapi.com")
    define("oauth_client_id", default=21)
    define("platform_id", default=21)
    define("platform_name", "wealth")

    # Google settings
    define("gapi_secret_key", "AIzaSyCZFB5OlWvSQWOjtD9ZdTidTSkQq7I_oEM")
    define(
        "gapi_scopes",
        (
            "https://www.googleapis.com/auth/spreadsheets",
            "https://www.googleapis.com/auth/activity",
            "https://www.googleapis.com/auth/drive.metadata.readonly",
        ),
    )

    define("gapi_type")
    define("gapi_project_id")
    define("gapi_private_key_id")
    define("gapi_private_key")
    define("gapi_client_email")
    define("gapi_client_id")
    define("gapi_auth_uri")
    define("gapi_token_uri")
    define("gapi_auth_provider_x509_cert_url")
    define("gapi_client_x509_cert_url")

    # Presentation
    define(
        "default_presentation_path",
        default=os.path.join(os.path.dirname(__file__), "data/default_presentation.json"),
    )
    define(
        "default_presentation_reduced_path",
        default=os.path.join(os.path.dirname(__file__), "data/default_presentation_reduced.json"),
    )

    # The resource name of the CryptoKey.
    define(
        "kms_key_path",
        "projects/octopus-secrets-dev/locations/global/keyRings/labs/cryptoKeys/wealth",
    )
    define("gcloud_bucket", "plan_documents")
    # Cloud bucket path to KMS-encrypted RSA private key
    define("rsa_private_key", "private.pem")
    # generate backend tokens inside the project (True) or acquire them from oauth. server (False)
    define("generate_token", True)

    define("notification_url", "https://notification-dev.octopusapi.com/n/v2/email")
    define("root_url", default="https://octopus-wealth-dev-202411.appspot.com")
    define("profile_url", default="https://profile-dev.octopusapi.com/a/v2")
    define("api_version", default="/api/v1")

    # Slack
    define("slack_token", default="xoxb-42320048594-VLnlq7F3C3ewcTLxgMEsSOjP")
    define("slack_channels", default=tuple())
    define("slack_message", default=None)

    # Memcached
    define("cache_server_list", default=["localhost:11211"])

    # Mysql Credentials etc
    define("wealth_db_software", "mysql")
    define("wealth_host", default=os.getenv("MYSQL_DB_HOST", "mysql"))
    define("wealth_port", default=int(os.getenv("MYSQL_DB_PORT", 3306)))
    define("wealth_db_name", default="octopus_wealth")
    define("wealth_db_user", default=os.getenv("MYSQL_DB_USER", "octopus"))
    define("wealth_db_password", default=os.getenv("MYSQL_DB_PASSWORD", "octopus"))

    # https://getaddress.io/
    define("user_token_expires", default=7)
    define("getaddress_api_key", default="iGTu6N4-2UuHneiJ2f6-bA7292")
    define("getaddress_url", default="https://api.getAddress.io")

    define("adviser_relation_type_id", default=5)
    define("paraplanner_relation_type_id", default=3)
    define("firm_relation_type_id", default=1)
    define("company_profile_id", default="b2fb5f02-237f-459d-8b49-f2ca21107001")

    # PDF Service
    define("pdf_service_converter", default="weasyprint")
    define("pdf_service_url", default="https://octopus-pdf-dev.appspot.com/a/v1/pdf")

    # dev email
    define("dev_email", default=None)
    define("dev_user_type", default="client")
    define("dev_user_command_enabled", default=False)
    define("dev_user_password", default="Octopus!1")
    define("dev_silent", default=False)

    # AML / KYC url
    define("amlkyc_url", default="http://octopus-amlkyc-api-dev.appspot.com/api/v1")

    define(
        "redirect_page",
        default={
            "client": "/survey/dashboard",
            "adviser": "/adviser/dashboard",
            "paraplanner": "/adviser/dashboard",
            "specialist": "/adviser/dashboard",
            "admin": "/adviser/dashboard",
        },
    )
    define("first_login_redirect_page", default="/survey/handy-hints")
    define("per_page", default=20)


DEV = "dev"
PROD = "prod"


environments = {
    "test": {
        "logging": "debug",
        "autoreload": True,
        "root_url": "http://127.0.0.1:8080",
        "port": 8080,
        "wealth_db_user": "root",
        "wealth_db_password": "12345",
        "wealth_db_name": "wealth_test",
        "dev_user_command_enabled": True,
    },
    "local": {
        "logging": "debug",
        "autoreload": True,
        "root_url": "http://127.0.0.1:8080",
        "port": 8080,
        "dev_user_command_enabled": True,
    },
    "dev": {
        "logging": "debug",
        "autoreload": False,
        "debug_mode": False,
        "slack_message": "DEV release",
        "slack_channels": ("#audit-wealth",),
        "dev_user_command_enabled": True,
    },
    "prod": {
        "logging": "info",
        "autoreload": False,
        "debug_mode": False,
        "fact_find_enabled": False,
        "slack_message": "PROD release",
        "slack_channels": ("#audit-deploy", "#audit-wealth", "#scrum-wealth"),
        "pdf_service_url": "http://octopus-pdf-eu.appspot.com/a/v1/pdf",
        "include_google_tag_manager": True,
    },
}

# to silence the appengine cache not found warning
log.getLogger("googleapiclient.discovery_cache").setLevel(log.ERROR)

LOGIN_COOKIE_NAME = "secportal_login"
RESET_TOKEN_LENGTH = 15
SIGNUP_COOKIE_NAME = "secportal_signup"

PER_PAGE = 50  # default number of rows in a paginated query
PAGE_SIZES = [10, 20, 50, 100, 200]

CERTIFICATE = "OctopusLabs.pem"

if "defined" not in options:
    define_options()
