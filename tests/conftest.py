import functools
import logging
import os
import uuid

import httpretty
import pytest
import tornado

pytest_plugins = ["tests.factories.models"]


@pytest.fixture
def app(env_vars):
    """
    Provides a tornado application.
    """
    import main

    application = tornado.web.Application(main.url_list, **main.server_settings)
    # application = tornado.wsgi.WSGIAdapter(application)
    return application


@pytest.fixture(scope="session")
def env_vars():
    """
    Sets and configures the environment variables for testing.
    """
    os.environ["WEALTH_ENV"] = "test"
    os.environ["FACT_FIND_ENABLED"] = "True"
    os.environ["MYSQL_DB_HOST"] = "localhost"

    import config  # noqa
    from tornado.options import options

    options.env = "test"
    options.dev_silent = True
    options.no_access_noise = True
    config.set_env(config.environments)

    options.dev_email = f"{str(uuid.uuid4()).replace('-', '')[:16]}@email.com"
    options.fact_find_enabled = True
    return options


@pytest.fixture(scope="function", autouse=True)
def apply_env_vars(env_vars):
    """
    Fixture made just to apply everywhere the env_vars
    """
    import config  # noqa
    from tornado.options import options

    options.env = "test"
    options.fact_find_enabled = True


@pytest.fixture(scope="session")
def dbsession(env_vars):
    """
    Returns an sqlalchemy session, and after the test tears down everything properly.
    """
    from models import model

    dbsession = model.Session(env="test")
    dbsession.create_tables()
    dbsession.session.commit()
    yield dbsession

    # sqlite in memory: no need to delete the tables
    dbsession.close()


@pytest.fixture(scope="session")
def dbsession_method(request, env_vars):
    """
    Returns an sqlalchemy session, and after the test tears down everything properly.
    """
    from models import model

    def method():
        dbsession = model.Session(env="test")
        dbsession.create_tables()
        dbsession.session.commit()

        def fin():
            # sqlite in memory: no need to delete the tables
            dbsession.close()

        request.addfinalizer(fin)
        return dbsession

    return method


@pytest.yield_fixture(scope="function")
def http_mock(request):
    """
    Patchs the http requests so we can mock the responses as we prefer.
    """
    dbody = '{"data": "Hello", "message": "Flying pizzas"}'
    stub_base = functools.partial(httpretty.register_uri, body=dbody, match_querystring=False)
    httpretty.stub_get = functools.partial(stub_base, httpretty.GET)
    httpretty.stub_post = functools.partial(stub_base, httpretty.POST)
    methods = {"post": httpretty.POST, "get": httpretty.GET}

    httpretty.reset()
    httpretty.enable()

    # urls/responses definitions
    httpretty.stub_get("https://auth-dev.octopusapi.com")
    httpretty.stub_post("https://auth-dev.octopusapi.com")
    if hasattr(request, "param"):
        config = request.param
        confs = [config] if isinstance(config, dict) else config
        for c in confs:
            c["method"] = methods[c.get("method", "get").lower()]
            c["body"] = c.get("body", dbody)
            c["match_querystring"] = c.get("match_querystring", False)
            # urls/responses definitions
            httpretty.register_uri(**c)
    # to check registered uris
    # httpretty.httpretty._entries

    yield httpretty
    httpretty.disable()
    httpretty.reset()


@pytest.fixture(scope="function")
def result(request):
    return request.param


@pytest.yield_fixture(scope="function")
def handler():
    """
    Provides a mock for tornado handlers.
    """
    from .helpers import MockHandler

    yield MockHandler()


@pytest.yield_fixture(scope="function")
def shutlog(caplog):
    """
    Silences all the logs during the run of a test.
    """
    caplog.set_level(logging.CRITICAL)
    yield caplog
    caplog.set_level(logging.INFO)
