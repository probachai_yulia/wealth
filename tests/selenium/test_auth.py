import pytest


@pytest.mark.incremental
class TestDashboard:
    @pytest.mark.selenium
    def test_login(self, py_server, npm_server, user_client, session_browser, pages):
        """Test the proper work of the login page."""
        login = pages["login_page"](session_browser)
        login.create_user()
        login.start()
        assert login.path.startswith("/login")
        login.fill()
        assert login.path.startswith("/survey/dashboard")
        login.visit(login.api_url + "/users/me")
        assert login.url == login.api_url + "/users/me"

    @pytest.mark.selenium
    def test_dashboard(self, py_server, npm_server, user_client, session_browser, pages):
        """Test the proper work of the login page."""
        dashboard = pages["dashboard_page"](session_browser)
        dashboard.start()
        assert dashboard.path.startswith("/survey/dashboard")
        family_page_url = pages["family_page"](session_browser)._url
        assert dashboard.check_disabled(
            family_page_url
        ), "Links after {} should be disabled".format(family_page_url)
        dashboard.get_page(family_page_url)
        assert dashboard.url == dashboard.base_url + "/survey/family/know-you"

    @pytest.mark.selenium
    def test_family_page(self, py_server, npm_server, user_client, session_browser, pages):
        """Test the proper work of the login page."""
        family_page = pages["family_page"](session_browser)
        family_page.start()

        assert family_page.path.startswith("/survey/family/know-you")
        family_page.fill()
