from contextlib import contextmanager
import multiprocessing
import os
import socket
import subprocess
import sys

import pytest


pytest_plugins = ["tests.factories.pages"]


@contextmanager
def suppress_output():
    """
    Silences all the outputs to the stdout/stderr.
    """
    with open(os.devnull, "w") as devnull:
        old_stdout = sys.stdout
        old_stderr = sys.stderr
        sys.stdout = devnull
        sys.stderr = devnull
        try:
            yield
        finally:
            sys.stdout = old_stdout
            sys.stderr = old_stderr


def ping(server, port):
    """
    A python implementation of PING.
    """

    if "://" in server:
        server = server.split("://")[1]

    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    try:
        s.connect((server, port))
        s.close()
        return True
    except socket.error:
        s.close()
        return False
    finally:
        s.close()


def kill_processes_by_port(port=8080):
    p1 = subprocess.Popen(["netstat", "-ltnp"], stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    p2 = subprocess.Popen(["grep", "-i", str(port)], stdin=p1.stdout, stdout=subprocess.PIPE)
    p3 = subprocess.Popen(["awk", "{print $2}"], stdin=p2.stdout, stdout=subprocess.PIPE)
    ids = p3.communicate()[0]

    with suppress_output():
        if ids is not None:
            ids = ids.split()
            for p in ids:
                kill_process = subprocess.Popen(
                    ["kill", "-9", str(p)], stdout=subprocess.PIPE, stderr=subprocess.PIPE
                )
                kill_process.wait()
                kill_process.kill()


@pytest.fixture(scope="session")
def py_server(env_vars):
    """
    Starts a tornado server fully configured to run using sqlite in-memory database.
    """
    from commands.helpers import upgrade
    import main
    import tornado
    from tornado.options import options

    def start_tornado_server(port, application, name, instance_number, service):
        http_server = tornado.httpserver.HTTPServer(application)
        http_server.listen(port)
        tornado.ioloop.IOLoop.instance().start()

    app = tornado.web.Application(main.url_list, **main.server_settings)
    dbsession = main.Session()
    dbsession.create_tables()
    upgrade(dbsession)
    server_process = multiprocessing.Process(
        target=start_tornado_server,
        args=(options.port, app, "ff_tests_app", 12345, "testing_fact_find"),
    )
    kill_processes_by_port(options.port)
    with suppress_output():
        server_process.start()
    yield server_process
    server_process.terminate()
    dbsession.close()
    kill_processes_by_port(options.port)


def kill_processes_by_cmd(cmd_description):
    # kills the node processes left behind
    with suppress_output():
        kill_webpack = subprocess.Popen(
            ["pkill", "-cf", cmd_description], stdout=subprocess.PIPE, stderr=subprocess.PIPE
        )
        kill_webpack.wait()
    kill_webpack.kill()


@pytest.fixture(scope="session")
def npm_server(env_vars):
    """
    Starts a npm server to serve the frontend.
    """
    from tornado.options import options

    js_bundle = os.path.join(options.static_path, "js/main.bundle.js")
    # only runs webpack if the js bundle isn't present
    if not os.path.exists(js_bundle):
        kill_processes_by_cmd('"\-\-config webpack.dev.js \-d \-\-watch \-\-mode=development"')
        cwd = os.path.realpath(os.path.join(os.path.dirname(__file__), "../../frontend/"))
        with suppress_output():
            build_process = subprocess.Popen(
                ["npm", "prepare-selenium"], cwd=cwd, stdout=subprocess.PIPE, stderr=subprocess.PIPE
            )
            build_process.wait()
            build_process.kill()

            # to grant that all the webpack builds are killed
            kill_processes_by_cmd('"\-\-config webpack.dev.js"')


@pytest.fixture(scope="function")
def user_client():
    from tornado.options import options

    old_opt = options.dev_user_type
    options.dev_user_type = "client"
    yield options.dev_user_type
    options.dev_user_type = old_opt


@pytest.fixture(scope="session")
def splinter_webdriver():
    """Provides the splinter webdriver name."""
    return "chrome"


@pytest.fixture(scope="session")
def splinter_headless():
    """ Marks if it's supposed to run headless or not."""
    return False
