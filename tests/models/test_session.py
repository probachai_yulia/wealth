from tornado.options import options


class TestSoftDeleteQuery:
    def test_delete_dttm(self, dbsession):
        from models.model import User

        users_list = [
            User(user_id=1, delete_dttm=None),
            User(user_id=2, delete_dttm=None),
            User(user_id=3, delete_dttm=None),
            User(user_id=4, delete_dttm=options.datetime_now),
            User(user_id=5, delete_dttm=options.datetime_now),
        ]
        session = User.session
        session.add_all(users_list)
        session.commit()
        assert User.query.count() == 3
        assert User.query.with_deleted().count() == 5
        # DANGER: it works, but when with_deleted is used, it has to be the first query "filter" applied
        assert User.query.filter_by(user_id=1).with_deleted().count() == 5

        # get should work fine being called in anyway
        assert User.query.get(1).user_id == 1
        assert User.query.with_deleted().get(1).user_id == 1
