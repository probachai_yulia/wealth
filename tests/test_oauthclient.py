from google.auth.exceptions import DefaultCredentialsError
import pytest
from tornado.options import options

from oauthclient import AuthError, OAuth2BackendClient, OAuth2Mixin


class TestOAuth2Mixin:
    def test_login_url(self):
        resp = OAuth2Mixin.login_url("/callback", "/request_uri")
        assert resp == (
            OAuth2Mixin.OAUTH_SERVER_URL
            + "/login?client_id=21&redirect_uri=%2Fcallback&state=%2Frequest_uri"
        )

    def test_logout_url(self):
        resp = OAuth2Mixin.logout_url("/redirect_uri")
        assert resp == (
            OAuth2Mixin.OAUTH_SERVER_URL + "/logout?client_id=21&redirect_uri=%2Fredirect_uri"
        )

    @pytest.mark.parametrize("data", [None, {"login": "aaaa"}])
    @pytest.mark.parametrize("status", [200, 401, 404])
    @pytest.mark.parametrize(
        "http_mock",
        [{"uri": options.oauth_server_url, "body": "asdsa", "status": 401, "method": "get"}],
        indirect=True,
    )
    def test_make_request(self, http_mock, data, status):
        try:
            r = OAuth2Mixin.make_request(options.oauth_server_url, data=data)
            assert r["data"] == "Hello"
        except AuthError as e:
            assert "Invalid OAuth2 response:" in e.args[0]

    @pytest.mark.parametrize(
        "http_mock",
        [{"uri": options.oauth_server_url + "/web/oauth2", "method": "post"}],
        indirect=True,
    )
    def test_exchange_code_for_access_token(self, http_mock):
        r = OAuth2Mixin.exchange_code_for_access_token("/callback", "top_secret")
        assert r["message"] == "Flying pizzas"

    @pytest.mark.parametrize(
        "http_mock",
        [{"uri": options.oauth_server_url + "/web/oauth2", "method": "post"}],
        indirect=True,
    )
    def test_refresh_access_token(self, http_mock):
        r = OAuth2Mixin.refresh_access_token("top_secret")
        assert r["message"] == "Flying pizzas"

    def test_set_cookies(self, handler):
        OAuth2Mixin.set_cookies(
            handler,
            {
                "access_token": "WOW-MANY-TOKEN",
                "expires_in": 3600,
                "refresh_token": "THE LORD OF THE SODAS",
            },
        )
        assert "access_token" in handler.secure_cookies

    @pytest.mark.parametrize(
        "http_mock",
        [{"uri": options.oauth_server_url + "/validate_token", "method": "get"}],
        indirect=True,
    )
    def test_validate_access_token(self, http_mock):
        OAuth2Mixin.validate_access_token("WOW-MANY-TOKEN")

    @pytest.mark.parametrize(
        "http_mock",
        [{"uri": options.oauth_server_url + "/validate_refresh_token", "method": "get"}],
        indirect=True,
    )
    def test_validate_refresh_token(self, http_mock):
        OAuth2Mixin.validate_refresh_token("WOW-MANY-TOKEN")

    @pytest.mark.parametrize(
        "http_mock",
        [{"uri": options.oauth_server_url + "/web/oauth2/revoke", "method": "post"}],
        indirect=True,
    )
    def test_revoke_token(self, http_mock):
        OAuth2Mixin.revoke_token("WOW-MANY-TOKEN")

    @pytest.mark.skip(reason="Not implemented")
    def test_get_current_user(self):
        pass

    @pytest.mark.skip(reason="Not implemented")
    def test_get_request_id(self):
        pass


class TestOAuth2BackendClient:
    @pytest.mark.skip(reason="Not implemented")
    def test_decode_key(self, shutlog):
        """
        TODO: find something to mock the googleapi
        or to mock GOOGLE_APPLICATION_CREDENTIALS.
        """
        with pytest.raises(DefaultCredentialsError):
            r = OAuth2BackendClient.decode_key("key")
        assert r == "key"

    @pytest.mark.skip(reason="Not implemented")
    def test_get_private_key(self):
        pass

    @pytest.mark.skip(reason="Not implemented")
    def test_get_backend_token(self):
        pass

    @pytest.mark.skip(reason="Not implemented")
    def test_authenticated_request(self):
        pass
