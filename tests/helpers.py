from unittest.mock import MagicMock, Mock

import pytest
from tornado.httputil import HTTPServerRequest


class BaseAsyncTest:
    _fixture_names = ["app"]

    @pytest.fixture(autouse=True)
    def auto_injector_fixture(self, request):
        if hasattr(self, "fixture_names"):
            self._fixture_names.extend(self.fixture_names)

        names = self._fixture_names
        for name in names:
            setattr(self, name, request.getfixturevalue(name))

    @pytest.fixture(autouse=True)
    def set_app(self, app):
        self.app = app
        return self.app

    def prepare_handler(self, handler_cls):
        """
        Helper method to generate a handler ready to use.
        """
        request = HTTPServerRequest(method="GET", uri="/", connection=Mock())
        handler = handler_cls(self.app, request)
        handler.prepare()
        return handler


class MockHandler:
    def __init__(self, *ag, **kw):
        self.current_user = None
        self.secure_cookies = {}
        self.cookies = {}
        self.callback_url = ""
        self.request = MagicMock(uri="/")
        self.user_roles = {}
        self.user_permissions = {}
        if kw:
            self.__dict__.update(**kw)

    def prepare(self):
        pass

    def get(self):
        pass

    def finish(self):
        pass

    def json_response(self, data, http_status, *ag, **kw):
        raise Exception({"data": data, "status": http_status})

    def redirect(self, *ag, **kw):
        return ""

    def clear_cookie(self, *ag, **kw):
        self.cookies = {}

    def get_current_user(self):
        return self.current_user

    def get_secure_cookie(self, name):
        return self.secure_cookies.get(name)

    def set_secure_cookie(self, name, value, **kw):
        self.secure_cookies[name] = value

    def get_user_roles(self, user):
        return self.user_roles

    def get_permissions_by_user_id(self, userid):
        return self.user_permissions
