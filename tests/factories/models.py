import inspect
import sys
import uuid

import factory
import factory.fuzzy
import pytest

from models import model as ff_models

BOOLS = (True, False, None)


class PlanFactory(factory.alchemy.SQLAlchemyModelFactory):
    """
    Factory to build Plan instances.
    """

    plan_id = factory.Sequence(lambda n: n)
    plan_uuid = factory.LazyFunction(lambda: str(uuid.uuid4()))
    family_has_children = factory.fuzzy.FuzzyChoice(BOOLS)
    family_has_dependents = factory.fuzzy.FuzzyChoice(BOOLS)
    family_has_partner = factory.fuzzy.FuzzyChoice(BOOLS)
    family_has_nanny = factory.fuzzy.FuzzyChoice(BOOLS)
    family_has_childcare = factory.fuzzy.FuzzyChoice(BOOLS)
    plan_has_other_income = factory.fuzzy.FuzzyChoice(BOOLS)
    partner_has_same_address = factory.fuzzy.FuzzyChoice(BOOLS)
    plan_has_charity = factory.fuzzy.FuzzyChoice(BOOLS)
    plan_has_gifting = factory.fuzzy.FuzzyChoice(BOOLS)

    expenditure_change_age = factory.fuzzy.FuzzyInteger(1, 100)
    expenditure_additional = factory.fuzzy.FuzzyText(length=255)

    has_insurance = factory.fuzzy.FuzzyChoice(BOOLS)
    insurance_additional = factory.fuzzy.FuzzyText(length=255)

    family_additional = factory.fuzzy.FuzzyText(length=255)

    class Meta:
        model = ff_models.Plan
        sqlalchemy_session = ff_models.Session()


class TokenToUserTableFactory(factory.alchemy.SQLAlchemyModelFactory):
    """
    Factory to build TokenToUserTable instances.
    """

    token_to_user_table_id = factory.Sequence(lambda n: n)
    token = factory.LazyFunction(lambda: str(uuid.uuid4()))

    # generates a user and attributes it to user_id
    user = factory.SubFactory("tests.factories.models.UserFactory")
    user_id = factory.SelfAttribute("user.user_id")

    class Meta:
        model = ff_models.TokenToUserTable
        sqlalchemy_session = ff_models.Session()


class UserFactory(factory.alchemy.SQLAlchemyModelFactory):
    """
    Factory to build User instances.
    """

    user_id = factory.Sequence(lambda n: n)
    profile_uuid = factory.LazyFunction(lambda: str(uuid.uuid4()))

    class Meta:
        model = ff_models.User
        sqlalchemy_session = ff_models.Session()


def auto_generate_fixtures():
    """
    This method scans this module and generates factories fixtures automatically
    from the factories defined here.
    The fixtures are named as `{tablename}_factory`
    """

    def generate_factory_fixture(factory_class):
        @pytest.yield_fixture(scope="function")
        def my_fixture(dbsession):
            created_records = []

            def _generate_plans(number=1, **kw):
                created_records = factory_class.create_batch(number, **kw)
                return created_records

            yield _generate_plans

            for record in created_records:
                record.delete(record)

        return my_fixture

    def inject_factory_fixture(name, factory_class):
        globals()[name] = generate_factory_fixture(factory_class)

    clsmembers = inspect.getmembers(
        sys.modules[__name__],
        lambda member: (
            inspect.isclass(member)
            and member.__module__ == __name__
            and issubclass(member, factory.alchemy.SQLAlchemyModelFactory)
        ),
    )

    for _, fact in clsmembers:
        inject_factory_fixture(fact._meta.model.__tablename__ + "_factory", fact)


auto_generate_fixtures()
