import inspect
import json
import re
import sys

import pytest
from selenium.webdriver.support.select import Select
from splinter.exceptions import ElementDoesNotExist
from tornado.options import options


class wait_for_page_load(object):
    def __init__(self, selector):
        self.selector = selector

    def __enter__(self):
        self.old_page = self.selector()

    def __exit__(self, *_):
        self.wait_for(self.page_has_loaded)

    def wait_for(self, condition_function):
        import time

        start_time = time.time()
        while time.time() < start_time + 3:
            if condition_function():
                return True
            else:
                time.sleep(0.1)
        raise Exception("Timeout waiting for {}".format(condition_function.__name__))

    def page_has_loaded(self):
        new_page = self.selector()
        return new_page


class OperationMixin(object):
    @staticmethod
    def _check_selector(opts, selector_type):
        if selector_type not in opts:
            raise Exception(f"Invalid selector_type {selector_type}. It has to be one of {opts}")

    def _call_method(self, selector, selector_type, method_name):
        method = getattr(self.browser, method_name + selector_type)
        return method(selector)


class Page(OperationMixin):
    """
    This is the base class to implement the Page Pattern for selenium tests.
    It's actually pretty simple. We create a base page with common configurations,
    interface, methods, etc and use it as any class to be inherited by the ones
    actually representing pages. Like, login page, logout page, admin page, etc.
    """

    base_url = options.root_url
    api_url = options.root_url + options.api_version
    _url = ""
    browser = None
    user_email = ""
    token = ""
    TIMEOUT = 20

    def __init__(self, browser, *ag, **kw):
        self.browser = browser
        self.full_url = self._url = "{}{}".format(self.base_url, self._url)
        self.user_email = options.dev_email
        self.user_type = options.dev_user_type
        self.user_password = options.dev_user_password
        self.__dict__.update(**kw)

    def __getattr__(self, attr):
        """
        Try to get the attributes not present here from the driver.
        """
        return self.__dict__.get(attr, getattr(self.browser, attr))

    def _get_full_url(self):
        return self.base_url + self._url

    def click_link(self, selector, selector_type="id"):
        """
        Clicks a link on the page.
        """
        opts = {"id", "href", "partial_href", "partial_text", "text"}
        Page._check_selector(opts, selector_type)
        self._call_method(selector, selector_type, "click_link_by_")

    def find(self, selector, selector_type="id"):
        """
        Looks for objects on the page.
        """
        opts = {"css", "id", "name", "tag", "value", "xpath"}
        Page._check_selector(opts, selector_type)
        return self._call_method(selector, selector_type, "find_by_")

    def is_element_present(self, selector, selector_type="id", wait_time=TIMEOUT):
        """
        Looks for objects on the page.
        """
        opts = {"text", "id", "value", "name", "tag", "css", "xpath"}
        Page._check_selector(opts, selector_type)
        method = getattr(self.browser, "is_element_present_by_" + selector_type)
        return method(selector, wait_time)

    @property
    def json(self):
        """
        Extracts the body of the current page as json.
        """
        return json.loads(self.browser.find_by_tag("body").text)

    @property
    def path(self):
        """
        Returns the current page path.
        """
        url_broken = self.browser.url.split("/", 3)
        return "/" + (url_broken[3] if len(url_broken) == 4 else "")

    @property
    def protocol(self):
        """
        Returns the current page protocol.
        """
        return self.browser.url.split("://", 1)[0]

    @property
    def host(self):
        """
        Returns the current page host.
        """
        return self.browser.url.split("/", 3)[2]

    def sleep(self, timeout=TIMEOUT):
        """
        Just sleeps for `timeout` seconds.
        """
        self.browser.is_element_present_by_name("quwyeuqyweuteu19723612", wait_time=timeout)

    def wait_and_fill(self, selector, value, selector_type="id", timeout=TIMEOUT):
        """
        Waits `timeout` seconds for the presence of an element based on the `css_selector`.
        If it's found within the wait time, fills it with `value`.
        """
        if self.is_element_present(selector, selector_type, wait_time=timeout):
            elem = self.find(selector, selector_type)
            if elem:
                elem.fill(value)

    def wait_and_find(self, selector, selector_type="id", timeout=TIMEOUT):
        """
        Waits `timeout` seconds for the presence of an element based on the `css_selector`.
        If it's found within the wait time, returns it. Else, returns None.
        """
        if self.is_element_present(selector, selector_type, wait_time=timeout):
            return self.find(selector, selector_type)

    def start(self):
        self.browser.visit(self._get_full_url())


class LoginPage(Page):
    """
    Represents the login page.
    """

    def __init__(self, *ag, **kw):
        self._url = "/login"
        Page.__init__(self, *ag, **kw)

        # TODO: mock the login system to make the tests faster and more isolated
        self.create_user_url = self.api_url + "/tasks/dev_user"

    def fill(self):
        self.wait_and_fill("id_email", self.user_email)
        self.wait_and_fill("id_password", self.user_password)
        submit = self.find("button[type=submit]", "css")[0]
        submit.click()

    def create_user(self):
        """
        Creates a user.
        TODO: do all the creation and login locally through mocks
        """
        self.browser.visit(
            f"{self.create_user_url}?email={self.user_email}&user_type={self.user_type}"
        )


class FamilyPage(Page):
    """
    Represents the survey/family page.
    """

    def __init__(self, *ag, **kw):
        Page.__init__(self, *ag, **kw)
        self._url = "/survey/family/know-you"

    def fill(self):
        self.wait_and_fill("dob", "30 October 1980", "name")
        Select(self.wait_and_find("//select[@name='birth_country_cd']", "xpath")).select_by_value(
            "GB"
        )
        Select(self.wait_and_find("citizenships[0]", "name")).select_by_value("BR")
        another_citizenship = self.find("button[type=button]", "css")[0]
        another_citizenship.click()
        Select(self.wait_and_find("citizenships[1]", "name")).select_by_value("UA")
        Select(self.wait_and_find("marital_status", "name")).select_by_value("widowed")
        self.wait_and_find("input[name='has_us_connections'][value='false']", "css").click()
        assert self.is_element_present("//label[@class='input-label cur-p']", "xpath") is False
        self.wait_and_find("input[name='has_us_connections'][value='true']", "css").click()
        assert self.is_element_present("//label[@class='input-label cur-p']", "xpath") is True


class AssetPage(Page):
    """
    Represents the survey/asset page.
    """

    def __init__(self, *ag, **kw):
        Page.__init__(self, *ag, **kw)
        self._url = "/survey/assets"


class IncomePage(Page):
    """
    Represents the survey/income page.
    """

    def __init__(self, *ag, **kw):
        Page.__init__(self, *ag, **kw)
        self._url = "/survey/income/salary-income"


class GivingPage(Page):
    """
    Represents the survey/giving page.
    """

    def __init__(self, *ag, **kw):
        Page.__init__(self, *ag, **kw)
        self._url = "/survey/income/salary-income"


class DebtPage(Page):
    """
    Represents the survey/debt page.
    """

    def __init__(self, *ag, **kw):
        Page.__init__(self, *ag, **kw)
        self._url = "/survey/income/salary-income"


class ExpensesPage(Page):
    """
    Represents the survey/expenses page.
    """

    def __init__(self, *ag, **kw):
        Page.__init__(self, *ag, **kw)
        self._url = "/survey/expenses"


class ToleranceRiskPage(Page):
    """
    Represents the survey/tolerance risk page.
    """

    def __init__(self, *ag, **kw):
        Page.__init__(self, *ag, **kw)


class ProviderPage(Page):
    """
    Represents the survey/provider page.
    """

    def __init__(self, *ag, **kw):
        Page.__init__(self, *ag, **kw)
        self._url = "/survey/product-providers/add-product-providers"

    def create_user(self):
        """
        Creates a user.
        TODO: do all the creation and login locally through mocks
        """
        self.browser.visit(
            f"{self.create_user_url}?email={self.user_email}&user_type={self.user_type}"
        )


class DashboardPage(Page):
    """
    Represents the survey/dashboard page.
    """

    def __init__(self, *ag, **kw):
        Page.__init__(self, *ag, **kw)
        self._url = "/survey/dashboard"
        self.links = [
            FamilyPage(self.browser)._url,
            AssetPage(self.browser)._url,
            IncomePage(self.browser)._url,
            GivingPage(self.browser)._url,
            DebtPage(self.browser)._url,
            ExpensesPage(self.browser)._url,
            ToleranceRiskPage(self.browser)._url,
        ]

    def get_page(self, link):
        self.find(f"//a[@href='{self._url}']", "xpath")
        self.click_link(link, "href")

    def check_disabled(self, latest_section):
        """
        Method checks if the links to sections are disabled
        The idea is to return to the dashboard page and check completed sections
        #TODO add checks for 'Start', 'Completed' button types

        """
        wait_for_page_load(lambda: self.find("//section[@class='survey-wrap']", "xpath"))
        index = self.links.index(latest_section) + 1
        for link in self.links[index:]:
            try:
                self.find("//a[@href='{}']".format(link), "xpath")[0]
            except ElementDoesNotExist:
                pass
            else:
                return False
        return True


@pytest.yield_fixture()
def pages():
    """
    Fixture that returns a dictionary with all the page classes ready to
    be instantiated and used.
    """

    first_cap_re = re.compile("(.)([A-Z][a-z]+)")
    all_cap_re = re.compile("([a-z0-9])([A-Z])")

    def convert(name):
        """
        Converts CamelCase to snake_case.
        """
        s1 = first_cap_re.sub(r"\1_\2", name)
        return all_cap_re.sub(r"\1_\2", s1).lower()

    clsmembers = inspect.getmembers(
        sys.modules[__name__],
        lambda member: (
            inspect.isclass(member) and member.__module__ == __name__ and issubclass(member, Page)
        ),
    )

    pages = {convert(name): page for name, page in clsmembers}
    yield pages
