from datetime import datetime, timedelta
from numbers import Number

import pytest
from tornado.options import options

import uimethods


@pytest.mark.parametrize(
    "cdate,result", [(None, "**"), (50, "**"), (datetime(2100, 12, 30, 23, 59, 59), "30 Dec 2100")]
)
def test_date_format(handler, cdate, result):
    r = uimethods.date_format(handler, cdate)
    assert r == result


@pytest.mark.parametrize(
    "cdate,result", [(None, "**"), (50, "**"), (datetime(2100, 12, 30, 23, 59, 59), "2100-12-30")]
)
def test_date_format_iso(handler, cdate, result):
    r = uimethods.date_format_iso(handler, cdate)
    assert r == result


@pytest.mark.parametrize(
    "cdate,result",
    [(None, "**"), (50, "**"), (datetime(2100, 12, 30, 23, 59, 59), "Thursday, December 30th")],
)
def test_date_weekday_format(handler, cdate, result):
    r = uimethods.date_weekday_format(handler, cdate)
    assert r == result


@pytest.mark.parametrize(
    "cdate,result",
    [(None, "**"), (50, "**"), (datetime(2100, 12, 30, 23, 59, 59), "December 30, 2100 11:59 PM")],
)
def test_date_time_format(handler, cdate, result):
    r = uimethods.date_time_format(handler, cdate)
    assert r == result


@pytest.mark.parametrize(
    "cdate,result",
    [
        (None, "**"),
        (50, "**"),
        (datetime(2100, 12, 30, 23, 59, 59), "30/12/2100"),
        (datetime(1, 12, 30, 23, 59, 59), "**"),
    ],
)
def test_date_format_dmy(handler, cdate, result):
    r = uimethods.date_format_dmy(handler, cdate)
    assert r == result


@pytest.mark.parametrize(
    "num,sufix,result",
    [
        (1, False, "1st"),
        (2, False, "2nd"),
        (3, False, "3rd"),
        (4, False, "4th"),
        (1, True, "st"),
        (2, True, "nd"),
        (3, True, "rd"),
        (4, True, "th"),
        (None, False, ""),
        (options.datetime_now(), False, ""),
        (0, False, ""),
        (-10, False, ""),
    ],
)
def test_ordinal(handler, num, sufix, result):
    r = uimethods.ordinal(handler, num, just_suffix=sufix)
    assert r == result


@pytest.mark.parametrize(
    "value,result",
    [
        ("1", "1"),
        (2.5, "2.5"),
        (6_000_000, "6,000,000"),
        ("6000000", "6,000,000"),
        (-1, "-1"),
        (None, ""),
        ("headless_chicken", ""),
    ],
)
def test_number_format(handler, value, result):
    r = uimethods.number_format(handler, value)
    assert r == result


@pytest.mark.parametrize(
    "value,result",
    [("1", "0.00"), (2.5, "0.03"), (-1, "-0.01"), (None, "0.00"), ("headless_chicken", "0.00")],
)
def test_poundify(value, result):
    r = uimethods.poundify(value)
    assert r == result

    def poundify(value):
        if not value:
            value = 0.0
        return "{:.2f}".format(float(value) / 100.0)


@pytest.mark.parametrize(
    "end,result",
    [
        (None, "0 seconds"),
        (timedelta(seconds=0), "0 seconds"),
        (timedelta(seconds=10), "10 seconds"),
        (timedelta(minutes=10), "10 minutes"),
        (timedelta(hours=10), "10 hours"),
        (timedelta(days=10), "10 days"),
        (timedelta(days=50), "7 weeks"),
        (timedelta(days=500), "17 months"),
    ],
)
@pytest.mark.freeze_time("2018-01-14", tick=False)
def test_friendly_duration(handler, end, result):
    start = options.datetime_now()
    if end is not None:
        end = options.datetime_now() + end
    r = uimethods.friendly_duration(handler, start, end)
    assert r == result


@pytest.mark.parametrize(
    "preposition,td,result",
    [
        (False, None, ""),
        (False, timedelta(seconds=0), "Just now"),
        (False, timedelta(seconds=10), "10 seconds ago"),
        (False, timedelta(minutes=10), "10 minutes ago"),
        (False, timedelta(hours=10), "10 hours ago"),
        (False, timedelta(days=1), "Yesterday at 12:00am"),
        (False, timedelta(days=10), "January 4 @ 12:00am"),
        (False, timedelta(days=50), "November 25 @ 12:00am"),
        (False, timedelta(days=500), "September 1, 2016"),
        (True, None, ""),
        (True, timedelta(seconds=0), "Just now"),
        (True, timedelta(seconds=10), "10 seconds ago"),
        (True, timedelta(minutes=10), "10 minutes ago"),
        (True, timedelta(hours=10), "10 hours ago"),
        (True, timedelta(days=1), "Yesterday at 12:00am"),
        (True, timedelta(days=10), "on January 4 @ 12:00am"),
        (True, timedelta(days=50), "on November 25 @ 12:00am"),
        (True, timedelta(days=500), "on September 1, 2016"),
    ],
)
@pytest.mark.freeze_time("2018-01-14", tick=False)
def test_friendly_time(handler, td, preposition, result):
    datte = None if td is None else options.datetime_now() - td
    r = uimethods.friendly_time(handler, datte, preposition)
    assert r == result


@pytest.mark.parametrize(
    "size, result",
    [
        (0, "0 Bytes"),
        (1, "1 Byte"),
        (2 ** 2, "4 Bytes"),
        (1024, "1.00 KB"),
        (1024 ** 2, "1.00 MB"),
        (1024 ** 3, "1.00 GB"),
        (1024 ** 4, "1.00 TB"),
        (1024 ** 5, "1024.00 TB"),
    ],
)
def test_friendly_filesize(handler, size, result):
    r = uimethods.friendly_filesize(handler, size)
    assert r == result


@pytest.mark.parametrize("prefix", ["£", "€"])
@pytest.mark.parametrize("suffix", ["", ">"])
@pytest.mark.parametrize(
    "amount,result", [(None, "N/A"), (-50, "-50.00"), (0, "0.00"), (191910, "191,910.00")]
)
def test_currency(handler, amount, prefix, suffix, result):
    if isinstance(amount, Number):
        res = prefix + result + suffix
    else:
        res = result
    r = uimethods.currency(handler, amount, prefix, suffix)
    assert r == res


def test_include_google_tag_manager(handler):
    r = uimethods.include_google_tag_manager(handler)
    assert r == options.include_google_tag_manager
