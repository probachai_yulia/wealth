import json

import pytest


class TestGetCreateUserTokenHandler:
    @pytest.mark.unit
    @pytest.mark.skip(reason="Not implemented")
    def test_get(self):
        pass

    @pytest.mark.unit
    @pytest.mark.skip(reason="Not implemented")
    def test_post(self):
        pass

    @pytest.mark.api
    async def test_get_api_no_tokens(self, user_factory, http_server_client):
        """
        Tests GET to the url: /api/v1/users/(?P<profile_uuid>[-\w]+)?/token
        Should return null.
        """
        user = user_factory(1)[0]
        resp = await http_server_client.fetch(f"/api/v1/users/{user.profile_uuid}/token")
        assert resp.code == 200
        assert resp.headers["Content-Type"] == "application/json"
        assert resp.body == b"null"

    @pytest.mark.api
    async def test_get_api_tokens(self, token_to_user_table_factory, http_server_client):
        """
        Tests GET to the url: /api/v1/users/(?P<profile_uuid>[-\w]+)?/token
        Should return some tokens.
        """
        token2user = token_to_user_table_factory(1)[0]

        resp = await http_server_client.fetch(f"/api/v1/users/{token2user.user.profile_uuid}/token")
        assert resp.code == 200
        assert resp.headers["Content-Type"] == "application/json"
        assert json.loads(resp.body) == token2user.to_dict(uuids=True, dttm=False)

    @pytest.mark.api
    @pytest.mark.skip(reason="Not implemented")
    async def test_post_api(self):
        pass


class TestCronExpireUserTokenHandler:
    @pytest.mark.unit
    @pytest.mark.skip(reason="Not implemented")
    def test_delete(self):
        pass
