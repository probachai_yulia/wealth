import pytest

from tests import helpers


@pytest.mark.unit
class TestBasePlanHandler(helpers.BaseAsyncTest):
    profile_uuid = "73677123-455e-46f4-b737-61f7bdf7fe8e"

    @pytest.mark.unit
    @pytest.mark.skip(reason="Not implemented")
    def test_get_plan_data(self, plan_factory):
        from web_handlers.Base import BasePlanHandler

        plan = plan_factory()
        # plan = Plan.get_by_uuid(plan_uuid, lazy=False, to_dict=False)

        handler = self.prepare_handler(BasePlanHandler)
        handler.current_user = "Quack"
        res = handler.get_plan_data(plan)
        assert res == "asd"
