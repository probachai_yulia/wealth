import json

import pytest
from tornado.options import options

from tests import helpers
from web_handlers.checks import AmlCheckHandler, verify_amlkyc


class TestAmlCheckHandler(helpers.BaseAsyncTest):
    profile_uuid = "73677123-455e-46f4-b737-61f7bdf7fe8e"
    data = {
        "valid_profile": {
            "person": {
                "first_name": "John",
                "last_name": "Doe",
                "middle_name": "",
                "dob": "1991-01-01",
                "country_citizenship": "GB",
                "country_birth": "GB",
                "address": {
                    "line1": "Fleet str.",
                    "line2": "",
                    "postcode": "DF1234G",
                    "country": "GB",
                    "city": "City",
                },
            },
            "is_enabled": True,
        },
        "auth_error": {
            "message": 'You are not authorised to access this page b\'{"status": "ERR", '
            '"message": "Token is invalid, Signature has expired"}\'',
            "status": "error",
        },
        "inexistant": {"status": "error", "message": "Resource not found: b'None'"},
        "invalid_platform": {
            "status": "ERR",
            "message": "AML/KYC validation error.",
            "fields": {"platform_id": ["The selected platform id is invalid."]},
        },
        "case_success": {
            "case": {
                "id": "e1eafb9a-f784-44f3-a4e8-902d73b4de25",
                "is_enabled": True,
                "person": {},
                "created": "2018-09-10 16:48:33",
                "updated": "2018-09-10 16:48:33",
                "custom_case_id": None,
                "documents": [],
                "results": [
                    {
                        "provider": "TS",
                        "status": "pending",
                        "valid_until": "2018-09-10 16:48:33",
                        "url": "",
                        "PEP": None,
                        "bank_account_valid": None,
                        "bank_account_verification": None,
                        "archived_at": None,
                        "created": "2018-09-10 16:48:33",
                        "updated": "2018-09-10 16:48:33",
                    },
                    {
                        "provider": "WC",
                        "status": "pending",
                        "valid_until": "2018-09-10 16:48:33",
                        "url": "",
                        "PEP": None,
                        "bank_account_valid": None,
                        "bank_account_verification": None,
                        "archived_at": None,
                        "created": "2018-09-10 16:48:33",
                        "updated": "2018-09-10 16:48:33",
                    },
                    {
                        "provider": "HTAR",
                        "status": "pass",
                        "url": None,
                        "valid_until": "2019-09-10 16:48:33",
                        "created": "2018-09-10 04:48:33",
                        "updated": "2018-09-10 04:48:33",
                    },
                ],
            }
        },
    }

    profile_url = (
        "{}/profiles/{}?fields=profile_id,email,first_name,"
        "last_name,national_insurance_nbr,user_id,display_name,dob,"
        "birth_country_cd,citizenships,phone".format(options.profile_url, profile_uuid)
    )

    @pytest.mark.parametrize(
        "http_mock,result",
        (
            (
                [
                    {
                        "uri": profile_url,
                        "method": "get",
                        "body": json.dumps(data["valid_profile"]),
                    },
                    {
                        "uri": options.amlkyc_url + "/cases",
                        "method": "post",
                        "body": json.dumps(data["case_success"]),
                    },
                ],
                "case_success",
            ),
            (
                [
                    {"uri": profile_url, "method": "get", "body": json.dumps(data["auth_error"])},
                    {
                        "uri": options.amlkyc_url + "/cases",
                        "method": "post",
                        "body": json.dumps(data["auth_error"]),
                    },
                ],
                "auth_error",
            ),
            (
                [
                    {"uri": profile_url, "method": "get", "body": json.dumps(data["inexistant"])},
                    {
                        "uri": options.amlkyc_url + "/cases",
                        "method": "post",
                        "body": json.dumps(data["inexistant"]),
                    },
                ],
                "inexistant",
            ),
            (
                [
                    {
                        "uri": profile_url,
                        "method": "get",
                        "body": json.dumps(data["valid_profile"]),
                    },
                    {
                        "uri": options.amlkyc_url + "/cases",
                        "method": "post",
                        "body": json.dumps(data["invalid_platform"]),
                    },
                ],
                "invalid_platform",
            ),
        ),
        indirect=True,
    )
    def test_verify_amlkyc(self, http_mock, dbsession, result):
        from models import model

        user = model.User.query.filter_by(profile_uuid=self.profile_uuid).first()
        if user is None:
            user = model.User.create(profile_uuid=self.profile_uuid, to_dict=False)

        handler = self.prepare_handler(AmlCheckHandler)
        handler.current_user = "Quack"
        res = verify_amlkyc(handler, self.data["valid_profile"], self.profile_uuid)
        assert res == self.data[result]

        dbsession.delete(user)
        dbsession.commit()

    @pytest.mark.skip(reason="Not implemented")
    @pytest.mark.parametrize(
        "http_mock", [{"uri": options.amlkyc_url + "/cases", "method": "post"}], indirect=True
    )
    def test_get(self, http_mock, dbsession):
        from models import model

        user = model.User.query.filter_by(profile_uuid=self.profile_uuid).first()
        if user is None:
            user = model.User.create(profile_uuid=self.profile_uuid, to_dict=False)

        handler = AmlCheckHandler()
        res = handler.get(self.profile_uuid)
        assert res
        buffer = handler._write_buffer
        assert len(buffer) > 0
        assert buffer

        dbsession.delete(user)
        dbsession.commit()
