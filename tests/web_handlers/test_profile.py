import pytest


class TestProfileHandler:
    @pytest.mark.skip(reason="Not implemented")
    def test_prepare(self):
        pass

    @pytest.mark.skip(reason="Not implemented")
    def test_has_profile_error(self):
        pass

    @pytest.mark.skip(reason="Not implemented")
    def test_get_profile(self):
        pass

    @pytest.mark.skip(reason="Not implemented")
    def test_create_profile(self):
        pass

    @pytest.mark.skip(reason="Not implemented")
    def test_update_profile(self):
        pass

    @pytest.mark.skip(reason="Not implemented")
    def test_update_email(self):
        pass

    @pytest.mark.skip(reason="Not implemented")
    def test_create_or_update_profile(self):
        pass

    @pytest.mark.skip(reason="Not implemented")
    def test_delete_profile(self):
        pass

    @pytest.mark.skip(reason="Not implemented")
    def test_set_access_to_profile(self):
        pass

    @pytest.mark.skip(reason="Not implemented")
    def test_search(self):
        pass

    @pytest.mark.skip(reason="Not implemented")
    def test_set_addresses(self):
        pass

    @pytest.mark.skip(reason="Not implemented")
    def test_change_address(self):
        pass

    @pytest.mark.skip(reason="Not implemented")
    def test_get_address(self):
        pass

    @pytest.mark.skip(reason="Not implemented")
    def test_get_addresses_list(self):
        pass

    @pytest.mark.skip(reason="Not implemented")
    def test_delete_address(self):
        pass

    @pytest.mark.skip(reason="Not implemented")
    def test_set_password(self):
        pass

    @pytest.mark.skip(reason="Not implemented")
    def test_change_password(self):
        pass

    @pytest.mark.skip(reason="Not implemented")
    def test_set_del_admin_to_profile(self):
        pass

    @pytest.mark.skip(reason="Not implemented")
    def test_get_related_users(self):
        pass

    @pytest.mark.skip(reason="Not implemented")
    def test_set_relation(self):
        pass

    @pytest.mark.skip(reason="Not implemented")
    def test_set_related_user(self):
        pass
