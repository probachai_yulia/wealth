import pytest


class TestBaseUserHandler:
    @pytest.mark.skip(reason="Not implemented")
    def test_get_users_list(self):
        pass

    @pytest.mark.skip(reason="Not implemented")
    def test_search_users(self):
        pass


class TestUserRoleHandler:
    @pytest.mark.skip(reason="Not implemented")
    def test_get(self):
        pass

    @pytest.mark.skip(reason="Not implemented")
    def test_post(self):
        pass


class TestUserHandler:
    @pytest.mark.skip(reason="Not implemented")
    def test_get(self):
        pass

    @pytest.mark.skip(reason="Not implemented")
    def test_patch(self):
        pass

    @pytest.mark.skip(reason="Not implemented")
    def test_delete(self):
        pass


class TestChangeUserEmailHandler:
    @pytest.mark.skip(reason="Not implemented")
    def test_post(self):
        pass


class TestCurrentUserHandler:
    @pytest.mark.skip(reason="Not implemented")
    def test_get(self):
        pass

    @pytest.mark.skip(reason="Not implemented")
    def test_post(self):
        pass


class TestCreateUserPasswordHandler:
    @pytest.mark.skip(reason="Not implemented")
    def test_post(self):
        pass


class TestChangeUserPasswordHandler:
    @pytest.mark.skip(reason="Not implemented")
    def test_put(self):
        pass


class TestSetClientToAdviserHandler:
    @pytest.mark.skip(reason="Not implemented")
    def test_post(self):
        pass


class TestSetAdviserToParaplannerHandler:
    @pytest.mark.skip(reason="Not implemented")
    def test_post(self):
        pass


class TestGetAdviserClientsHandler:
    @pytest.mark.skip(reason="Not implemented")
    def test_get(self):
        pass


class TestGetParaplannerAdvisersHandler:
    @pytest.mark.skip(reason="Not implemented")
    def test_get(self):
        pass
