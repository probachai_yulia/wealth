# -*- coding: utf-8 -*-

import pytest


@pytest.mark.decorator
def test_check_permissions():
    from decorators.auth_decorators import _check_permissions

    # if it's a list, it has to have all the roles
    user_roles = ["a", "b"]
    roles = ["b", "c"]
    assert not _check_permissions(user_roles, roles)
    user_roles = ["a", "b"]
    roles = ["a", "b"]
    assert _check_permissions(user_roles, roles)

    # if it's a single string role
    user_roles = ["a", "b"]
    roles = "c"
    assert not _check_permissions(user_roles, roles)
    user_roles = ["a", "b"]
    roles = "b"
    assert _check_permissions(user_roles, roles)


@pytest.mark.decorator
def test_role_required():
    from tests.helpers import MockHandler
    from decorators.auth_decorators import role_required

    class TestHandler(MockHandler):
        @role_required(roles=["potatoes_baker"])
        def simple_method(self):
            return "Answer"

    handler = TestHandler()
    # no user
    with pytest.raises(Exception) as excinfo:
        handler.simple_method()
    answer = excinfo.value.args[0]
    assert "Please sign in" in answer["data"]["message"]
    assert "ERR" == answer["data"]["status"]

    # a user, but no roles
    handler.current_user = "any_user"
    with pytest.raises(Exception) as excinfo:
        handler.simple_method()
    answer = excinfo.value.args[0]
    assert "You don't have permissions" in answer["data"]["message"]
    assert "ERR" == answer["data"]["status"]

    # a user, with proper roles
    handler.user_roles = "potatoes_baker"
    answer = handler.simple_method()
    assert answer == "Answer"


@pytest.mark.decorator
def test_permission_required():
    from tests.helpers import MockHandler
    from decorators.auth_decorators import permission_required

    class TestHandler(MockHandler):
        @permission_required(permissions=["bake_potatoes"])
        def simple_method(self):
            return "Answer"

    handler = TestHandler()
    # no user
    with pytest.raises(Exception) as excinfo:
        handler.simple_method()
    answer = excinfo.value.args[0]
    assert "Please sign in" in answer["data"]["message"]
    assert "ERR" == answer["data"]["status"]

    # a user, but no permissions
    handler.current_user = "any_user"
    with pytest.raises(Exception) as excinfo:
        handler.simple_method()
    answer = excinfo.value.args[0]
    assert "You don't have permissions" in answer["data"]["message"]
    assert "ERR" == answer["data"]["status"]

    # a user, with proper permissions
    handler.user_permissions = "bake_potatoes"
    answer = handler.simple_method()
    assert answer == "Answer"


@pytest.mark.decorator
def test_role_forbidden():
    from tests.helpers import MockHandler
    from decorators.auth_decorators import role_forbidden

    class TestHandler(MockHandler):
        @role_forbidden(roles=["potatoes_baker"])
        def simple_method(self):
            return "Answer"

    handler = TestHandler()
    # no user
    with pytest.raises(Exception) as excinfo:
        handler.simple_method()
    answer = excinfo.value.args[0]
    assert "Please sign in" in answer["data"]["message"]
    assert "ERR" == answer["data"]["status"]

    # a user, but no roles
    handler.current_user = "any_user"
    answer = handler.simple_method()
    assert answer == "Answer"

    # a user, with blocking roles
    handler.user_roles = "potatoes_baker"
    with pytest.raises(Exception) as excinfo:
        handler.simple_method()
    answer = excinfo.value.args[0]
    assert "This role is refused" in answer["data"]["message"]
    assert "ERR" == answer["data"]["status"]


@pytest.mark.decorator
def test_permission_forbidden():
    from tests.helpers import MockHandler
    from decorators.auth_decorators import permission_forbidden

    class TestHandler(MockHandler):
        @permission_forbidden(permissions=["bake_potatoes"])
        def simple_method(self):
            return "Answer"

    handler = TestHandler()
    # no user
    with pytest.raises(Exception) as excinfo:
        handler.simple_method()
    answer = excinfo.value.args[0]
    assert "Please sign in" in answer["data"]["message"]
    assert "ERR" == answer["data"]["status"]

    # a user, but no roles
    handler.current_user = "any_user"
    answer = handler.simple_method()
    assert answer == "Answer"

    # a user, with blocking roles
    handler.user_permissions = "bake_potatoes"
    with pytest.raises(Exception) as excinfo:
        handler.simple_method()
    answer = excinfo.value.args[0]
    assert "These permissions are refused" in answer["data"]["message"]
    assert "ERR" == answer["data"]["status"]


@pytest.mark.decorator
@pytest.mark.skip(reason="Not implemented")
def test_oauth_authenticated():
    pass


@pytest.mark.decorator
@pytest.mark.skip(reason="Not implemented")
def test__generate_token():
    pass
