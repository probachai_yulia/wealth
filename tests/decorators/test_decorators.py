import pytest
from tornado.options import options
from tornado.web import HTTPError


@pytest.mark.skip(reason="Not implemented")
def test_add_modification_history():
    pass


@pytest.mark.decorator
def test_fact_find_enabled():
    from decorators.decorators import fact_find_enabled

    @fact_find_enabled
    def simple_method():
        return "Something"

    # has to just return
    options.fact_find_enabled = True
    resp = simple_method()
    assert resp == "Something"

    # has to raise an HTTPError
    options.fact_find_enabled = False
    with pytest.raises(HTTPError):
        simple_method()


@pytest.mark.skip(reason="Not implemented")
def test_develop_tool():
    pass
