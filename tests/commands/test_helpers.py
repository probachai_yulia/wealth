import uuid

import pytest
from tornado.options import options  # noqa


# run it with python create_dev_user.py --dev_email=someuser@example.com --dev_user_type=adviser


@pytest.mark.commands
@pytest.mark.skip(reason="Not implemented")
@pytest.mark.parametrize(
    "http_mock",
    [[{"uri": "https://auth-dev.octopusapi.com/web/oauth2", "method": "post"}]],
    indirect=True,
)
def test_create_dev_user(dbsession, http_mock, shutlog):
    from commands.helpers import create_dev_user

    # dbsession
    res = create_dev_user(email=None, user_type=None)
    assert res == "Please provide email"
    from libraries.profile import ProfileClient

    email = str(uuid.uuid4()) + "@test.com"
    name = email.split("@")[0]
    data = {
        "client_id": options.oauth_client_id,
        "email": email,
        "password": "Octopus!1",
        "first_name": name,
        "last_name": "WealthDev",
    }
    try:
        res = ProfileClient.create(data)
    except Exception:
        pass

    # q = dbsession.session.query(ProfileClient).filter(ProfileClient.email == email)
    # assert list(q) == 'asdasd'

    res = create_dev_user(email=None, user_type=None)
    assert res == "Please provide email"


@pytest.mark.skip(reason="Not implemented")
def test_create_plan():
    pass


@pytest.mark.skip(reason="Not implemented")
def test_create_user_role():
    pass
