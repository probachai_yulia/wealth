import pytest


class TestBaseUserSchema:
    @pytest.mark.skip(reason="Not implemented")
    def test_check_first_name(self):
        pass

    @pytest.mark.skip(reason="Not implemented")
    def test_validate_quantity(self):
        pass


class TestAddressSchema:
    @pytest.mark.skip(reason="Not implemented")
    def test_validate_postcode(self):
        pass

    @pytest.mark.skip(reason="Not implemented")
    def test_validate_country_cd(self):
        pass


class TestPasswordSchema:
    @pytest.mark.skip(reason="Not implemented")
    def test_validate_password(self):
        pass


class TestPermissionSchema:
    @pytest.mark.skip(reason="Not implemented")
    def test_validate_name(self):
        pass

    @pytest.mark.skip(reason="Not implemented")
    def test_validate_code(self):
        pass


class TestRoleSchema:
    @pytest.mark.skip(reason="Not implemented")
    def test_validate_name(self):
        pass


class TestRegularContributionSchema:
    @pytest.mark.skip(reason="Not implemented")
    def test_validate_value_type(self):
        pass

    @pytest.mark.skip(reason="Not implemented")
    def test_validate_frequency(self):
        pass

    @pytest.mark.skip(reason="Not implemented")
    def test_validate_regular_contribution_uuid(self):
        pass


class TestJobSchema:
    @pytest.mark.skip(reason="Not implemented")
    def test_validate_profile_uuid(self):
        pass

    @pytest.mark.skip(reason="Not implemented")
    def test_validate_plan_cashflow_uuid(self):
        pass

    @pytest.mark.skip(reason="Not implemented")
    def test_validate_employment_type(self):
        pass


class TestGivingSchema:
    @pytest.mark.skip(reason="Not implemented")
    def test_validate_plan_cashflow_uuid(self):
        pass

    @pytest.mark.skip(reason="Not implemented")
    def test_validate_giving_type(self):
        pass


class TestAssetSchema:
    @pytest.mark.skip(reason="Not implemented")
    def test_validate_asset_type(self):
        pass

    @pytest.mark.skip(reason="Not implemented")
    def test_validate_tax_benefit(self):
        pass

    @pytest.mark.skip(reason="Not implemented")
    def test_validate_plan_cashflow_uuid(self):
        pass

    @pytest.mark.skip(reason="Not implemented")
    def test_validate_plan_asset_uuid(self):
        pass

    @pytest.mark.skip(reason="Not implemented")
    def test_validate_address_uuid(self):
        pass


class TestDebtSchema:
    @pytest.mark.skip(reason="Not implemented")
    def test_validate_plan_debt_type_id(self):
        pass

    @pytest.mark.skip(reason="Not implemented")
    def test_validate_rate_type(self):
        pass

    @pytest.mark.skip(reason="Not implemented")
    def test_validate_plan_cashflow_uuid(self):
        pass

    @pytest.mark.skip(reason="Not implemented")
    def test_validate_plan_debt_uuid(self):
        pass


class TestCashflowSchema:
    @pytest.mark.skip(reason="Not implemented")
    def test_validate_plan_cashflow_uuid(self):
        pass

    @pytest.mark.skip(reason="Not implemented")
    def test_validate_user_cashflow_type(self):
        pass

    @pytest.mark.skip(reason="Not implemented")
    def test_validate_value_type(self):
        pass

    @pytest.mark.skip(reason="Not implemented")
    def test_validate_frequency(self):
        pass


class TestUserSchema:
    @pytest.mark.skip(reason="Not implemented")
    def test_validate_current_situation_type(self):
        pass

    @pytest.mark.skip(reason="Not implemented")
    def test_validate_plan_user_type(self):
        pass

    @pytest.mark.skip(reason="Not implemented")
    def test_validate_email(self):
        pass

    @pytest.mark.skip(reason="Not implemented")
    def test_validate_us_connections(self):
        pass

    @pytest.mark.skip(reason="Not implemented")
    def test_validate_phone(self):
        pass


class TestPlanSchema:
    @pytest.mark.skip(reason="Not implemented")
    def test_validate_marital_status(self):
        pass
