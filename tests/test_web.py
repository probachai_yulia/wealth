import json
from unittest.mock import Mock

import pytest
from tornado.httputil import HTTPServerRequest
from tornado.options import options
from tornado.web import Finish

from tests import helpers
import web


class TestPageHandler(helpers.BaseAsyncTest):
    fixture_names = ("shutlog",)

    def test_prepare(self):
        """
        Tests the prepare method by checking some of the properties it sets on the instance.
        """
        request = HTTPServerRequest(method="GET", uri="/", connection=Mock())
        handler = web.PageHandler(self.app, request)

        assert not hasattr(handler, "debug_mode")
        handler.prepare()

        assert handler.debug_mode == options.debug_mode
        assert handler.root_url == options.root_url
        assert handler.tmpl["current_user"] == handler.current_user

    def test_cookies(self):
        """
        Tests the custom method to set cookies.
        """
        handler = self.prepare_handler(web.PageHandler)

        cookie = handler.get_secure_cookie("quack")
        assert cookie is None

        handler.set_secure_cookie("quack", "value")
        cookie = handler.get_secure_cookie("quack")
        # get_secure_cookie does not see outgoing cookies set by
        # `set_secure_cookie` in this handler.
        assert cookie is None

    def test_methods(self):
        """
        Tests the HTTP methods of PageHandler.
        """
        handler = self.prepare_handler(web.PageHandler)

        methods = ("get", "put", "patch", "post", "delete", "head", "options", "__write_error__")
        for method in methods:
            with pytest.raises(Finish):
                getattr(handler, method)()
            buffer = handler._write_buffer
            assert len(buffer) > 0
            assert json.loads(buffer[-1])["code"] == 405

    def test_date_handler(self):
        """
        Tests the method to make date/datetime compatible with json.dumps.
        """
        obj = options.datetime_now()
        resp = web.PageHandler.date_handler(obj)
        assert isinstance(resp, str)

        resp = web.PageHandler.date_handler(str(obj))
        assert isinstance(resp, str)

    def test_json_response(self):
        """
        Checks the behaviour of json_response.
        """
        handler = self.prepare_handler(web.PageHandler)

        with pytest.raises(Finish):
            handler.json_response({"data": 1, "create_dttm": 2, "delete_dttm": 3, "update_dttm": 4})
        buffer = handler._write_buffer
        assert len(buffer) > 0
        buffer = json.loads(buffer[-1])
        assert buffer["data"] == 1
        assert "create_dttm" not in buffer
        assert "delete_dttm" not in buffer
        assert "update_dttm" not in buffer

    def test_json_error(self):
        """
        Checks if the passed message is correctly returned.
        """
        handler = self.prepare_handler(web.PageHandler)

        handler.json_error("a message")
        buffer = handler._write_buffer
        assert len(buffer) > 0
        buffer = json.loads(buffer[-1])
        assert buffer["message"] == "a message"

    def test_result_or_404(self):
        """
        Tests if returns an error when the result is None/False
        or if returns the result itself when it evaluates to True.
        """
        handler = self.prepare_handler(web.PageHandler)

        # no results
        with pytest.raises(Finish):
            handler.result_or_404(result=None, model="AModel")
        buffer = handler._write_buffer
        assert len(buffer) > 0
        buffer = json.loads(buffer[-1])
        assert buffer["status"] == "ERR"
        assert buffer["message"] == "AModel argument: None not found"

        # with a result
        resp = handler.result_or_404(result="A great result", model="AModel")
        assert resp == "A great result"


class TestError404(helpers.BaseAsyncTest):
    def test_methods(self):
        def mock_render(*ag, **kw):
            return ""

        tmp_render = web.Error404.render
        web.Error404.render = mock_render
        handler = self.prepare_handler(web.Error404)

        methods = ("get", "post", "put", "delete")
        for method in methods:
            with pytest.raises(Finish):
                getattr(handler, method)()
            buffer = handler._write_buffer
            assert len(buffer) > 0
            buffer = json.loads(buffer[-1])
            assert buffer["message"] == "404 - Page not found."
        web.Error404.render = tmp_render
