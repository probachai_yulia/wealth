import http.client
import json
import logging
import re

import tornado
import tornado.escape
import tornado.httpclient
from tornado.options import options

import config
from decorators.auth_decorators import decode_token
from decorators.decorators import fact_find_enabled, threadlocal
from models.model import User
from oauthclient import AuthError, OAuth2Mixin
from utils.constants import CLIENT_USER_TYPE


class PageHandler(OAuth2Mixin, tornado.web.RequestHandler):
    TEMPLATE_NAME = ""
    ENDPOINT = ""
    CACHE_SUFFIX = None

    @property
    def session(self):
        return self.application.session

    def prepare(self):
        """
        Sets the root and tmpl dictionary
        Args:
            *args:
            **kwargs:

        Returns:
            Nothing

        """
        super(PageHandler, self).prepare()
        self.debug_mode = options.debug_mode
        self.root_url = options.root_url  # "%s://%s" % (self.request.protocol, self.request.host)
        # for oauth
        self.callback_url = self.root_url + "/auth_callback"
        self.url = self.root_url + self.request.uri
        self.tmpl = {"flash_messages": None, "env": config.get_env()}
        self.start_time = options.datetime_now()
        self.form_fields = None
        self.json_object = None
        self.forms = None
        threadlocal.request_id = self.get_request_id()
        self.tmpl["current_user"] = self.current_user
        self.tmpl["login_url"] = OAuth2Mixin.login_url(self.callback_url, self.request.uri)

    def get_user_roles(self, user_id):
        return User.get_user_role(user_id)

    def get_permissions_by_user_id(self, user_id):
        return User.get_permissions_by_user_id(user_id)

    def get_secure_cookie(self, name, **kwargs):
        result = super(PageHandler, self).get_secure_cookie(name, **kwargs)
        if isinstance(result, bytes):
            result = result.decode("utf-8")
        return result

    def set_secure_cookie(self, name, value, expires_days=30, **kwargs):
        if options.env == config.PROD:
            kwargs["Secure"] = True
            kwargs["HttpOnly"] = True
        super(PageHandler, self).set_secure_cookie(name, value, expires_days, **kwargs)

    def render(self, template_name, **kwargs):
        kwargs["load_time"] = (options.datetime_now() - self.start_time).total_seconds()
        return super(PageHandler, self).render(template_name, **kwargs)

    def get(self, **kwargs):
        self.__write_error__()

    def screen(self):
        self.render("{}.html".format(self.TEMPLATE_NAME), **self.tmpl)

    @staticmethod
    def date_handler(obj):
        if hasattr(obj, "isoformat"):
            return obj.isoformat()
        else:
            if hasattr(obj, "dir"):
                logging.warning("date: " + str(obj.dir))
            else:
                logging.warning("date: " + str(obj))
            return obj

    def replace_id_names(self, data):
        return re.sub(r"_id", "_uuid", data)

    def json_response(self, data, status_code=http.client.OK, status_reason=None):
        if status_code >= http.client.BAD_REQUEST:
            self.set_header("Content-Type", "application/problem+json")
        else:
            self.set_header("Content-Type", "application/json")
        self.set_status(status_code, status_reason)
        try:
            if data and len(data) > 0:
                for key in ("create_dttm", "delete_dttm", "update_dttm"):
                    if key in data:
                        del data[key]
            result = json.dumps(data, default=self.date_handler, skipkeys=True, indent=2)
            self.write(self.replace_id_names(result))
        except Exception as e:
            logging.exception("Exception: {} {}".format(e, data))
            self.write(
                json.dumps(
                    {"status": "Internal Server Error"},
                    default=self.date_handler,
                    skipkeys=True,
                    indent=2,
                )
            )
        raise tornado.web.Finish

    def json_error(self, message):
        self.set_status(http.client.BAD_REQUEST)
        if isinstance(message, str):
            message = {"message": message}
        result = json.dumps(message, skipkeys=True)
        logging.error(result)
        self.write(result)

    def put(self):
        self.__write_error__()

    def patch(self):
        self.__write_error__()

    def post(self, **kwargs):
        self.__write_error__()

    def delete(self):
        self.__write_error__()

    def head(self):
        self.__write_error__()

    def options(self):
        self.__write_error__()

    def __write_error__(self):
        self.json_response(
            {
                "code": http.client.METHOD_NOT_ALLOWED,
                "message": "Method not allowed",
                "method": self.request.method,
                "url": self.request.uri,
            },
            http.client.METHOD_NOT_ALLOWED,
        )

    def user_or_404(self, profile_uuid):
        user = User.get_by_uuid(profile_uuid)
        return self.result_or_404(user, "user")

    def result_or_404(self, result, model):
        if not result:
            self.json_response(
                {"status": "ERR", "message": "{} argument: {} not found".format(model, result)},
                http.client.NOT_FOUND,
            )
        else:
            return result


class FactFindHandler(PageHandler):
    @fact_find_enabled
    def prepare(self):
        super(FactFindHandler, self).prepare()


class Error404(PageHandler):
    """ This class is used to return 404 messages to the API users
    """

    def get(self):
        self.render("404.html", **self.tmpl)
        self.json_object = dict(status="error", message="404 - Page not found.")
        self.screen()

    def post(self):
        self.json_object = dict(status="error", message="404 - Page not found.")
        self.screen()

    def put(self):
        self.json_object = dict(status="error", message="404 - Page not found.")
        self.screen()

    def delete(self):
        self.json_object = dict(status="error", message="404 - Page not found.")
        self.screen()

    def screen(self):
        self.json_response(self.json_object, http.client.NOT_FOUND)


class HomeHandler(PageHandler):
    """
    Handler responsible for the URL: /
    """

    def get(self):
        return self.render("home.html", **self.tmpl)


class BaseHandler(PageHandler):
    """
    Handler to serve the templates for URLs: /.*
    Despite the home page.
    """

    def get(self):
        return self.render("home.html", **self.tmpl)


class LoginHandler(PageHandler):
    """
    Handler responsible for the URL: /login
    """

    def get(self):
        callback_url = "%s://%s/auth_callback" % (self.request.protocol, self.request.host)
        self.redirect(OAuth2Mixin.login_url(callback_url, self.get_query_argument("next", "/")))


class LogoutHandler(PageHandler):
    """
    Handler responsible for the URL: /logout
    """

    def get_current_user(self):
        return None

    def clear_auth_cookies(self):
        self.clear_cookie("access_token")
        self.clear_cookie("refresh_token")

    def get(self):
        self.clear_auth_cookies()
        self.redirect(OAuth2Mixin.logout_url(self.request.protocol + "://" + self.request.host))


class OAuthCallbackHandler(PageHandler):
    """
    Handler responsible for the URL: /auth_callback
    """

    def get(self):
        self.clear_cookie("access_token")
        self.clear_cookie("refresh_token")
        code = self.get_query_argument("code", None)
        if code:
            try:
                tokens = OAuth2Mixin.exchange_code_for_access_token(self.callback_url, code)
            except AuthError as e:
                if hasattr(e, "error_message"):
                    self.tmpl["message"] = e.error_message
                elif hasattr(e, "error"):
                    self.tmpl["message"] = e.error
                else:
                    self.tmpl["message"] = e.message
            else:
                OAuth2Mixin.set_cookies(self, tokens)
                profile, user = decode_token(self, tokens.get("access_token"))
                if user is None:
                    self.tmpl["message"] = "Profile not found."
                    return self.redirect("/login")
                state = self.get_query_argument("state")
                role = User.get_user_role(user.user_id)
                if role == CLIENT_USER_TYPE and state == options.first_login_redirect_page:
                    return self.redirect(state)
                return self.redirect(options.redirect_page.get(role, "/"))
        elif self.request.headers.get("Content-type", None) == "application/json":
            response = json.loads(self.request.body)
            err = response.get("error_description", None)
            if not err:
                err = response.get("error", None)
            if err:
                self.tmpl["message"] = err
            else:
                self.tmpl["message"] = self.request.body
        elif self.get_argument("error_description", None) or self.get_argument("error", None):
            self.tmpl["message"] = self.get_argument(
                "error_description", None
            ) or self.get_argument("error", None)
        else:
            self.tmpl["message"] = 'No "code" argument'

        self.render("auth_error.html", **self.tmpl)


class StaticHandler(PageHandler):
    """
    Handler responsible for the URLs: /(privacy|terms)
    """

    def get(self, page_name):
        self.screen(page_name)

    def check_xsrf_cookie(self):
        pass

    def post(self, page_name):
        self.screen(page_name)

    def screen(self, page_name):
        self.render("%s.html" % page_name, **self.tmpl)
