import random
import string

# from google.cloud import storage
from google.cloud import storage
from tornado.options import options

from models.model import Plan, PlanUser, PlanUserType, User
from utils.constants import CHILD_USER_TYPE, MAIN_USER_TYPE, UNIQUE_PLAN_USER_TYPES


def create_or_get_user(profile_id, result, token=None):
    user = User.query.filter_by(profile_uuid=profile_id).first()
    if user:
        return user.user_id
    else:
        data = {"profile_uuid": profile_id}
        data.update({"is_active": result.get("is_active")})
        if token:
            data["token"] = token
        user = User.create(**data)
        return user["user_id"]


def create_plan(profile_id, user_id):
    plan_data = {"is_submitted": 0}
    plan = Plan.upsert(plan_data)
    user_data = {"plan_user_type": MAIN_USER_TYPE}
    PlanUser.create_or_update_user(user_data, plan["plan_id"], profile_id, user_id)
    return plan


def reformat_plan_user_list(data):
    user_types = PlanUserType.all()
    result = {}
    dependent = []
    for type in user_types:
        users_list = []
        for user in data:
            if type["code"] == user["plan_user_type"]:
                users_list.append(user)

        if type["code"] in UNIQUE_PLAN_USER_TYPES:
            result[type["code"]] = users_list[0] if users_list else None
        elif type["code"] == CHILD_USER_TYPE:
            result[type["code"]] = users_list
        else:
            dependent.extend(users_list)
        result["dependent"] = dependent
    return result


def create_password():
    letters = "".join(random.choice(string.ascii_lowercase) for _ in range(5))
    letters_b = "".join(random.choice(string.ascii_uppercase) for _ in range(3))
    numbers = "".join(random.choice(string.digits) for _ in range(3))
    symbols = "".join(random.choice(string.punctuation) for _ in range(3))
    return letters + numbers + symbols + letters_b


def upload_blob(bytes, filename, mimetype):
    storage_client = storage.Client()
    bucket = storage_client.get_bucket(options.gcloud_bucket)
    blob = bucket.blob(filename)
    blob.upload_from_string(bytes, mimetype)


def download_blob(filename):
    client = storage.Client()
    bucket = client.get_bucket(options.gcloud_bucket)
    blob = bucket.get_blob(filename)
    return blob.download_as_string()


def delete_blob(blob_name):
    """Deletes a blob from the bucket."""
    storage_client = storage.Client()
    bucket = storage_client.get_bucket(options.gcloud_bucket)
    blob = bucket.blob(blob_name)

    blob.delete()
