from functools import lru_cache

from libraries.profile import ProfileClient
from tornado.options import options


class ValidationHelper:
    @staticmethod
    @lru_cache(maxsize=None)
    def get_countries():
        url = options.profile_url + "/countries"
        try:
            return ProfileClient.authenticated_request(url, method="GET", json_response=True)[
                "countries"
            ]
        except KeyError:
            # TODO: add something like a fallback to local cache, a file cache or something like it
            return []
