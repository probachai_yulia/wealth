import math


class Page:
    @staticmethod
    def calculate_page(items, page, page_size, total):
        previous_page = None
        next_page = None
        has_previous = page > 1
        if has_previous:
            previous_page = page - 1
        previous_items = (page - 1) * page_size
        has_next = previous_items + len(items) < total
        if has_next:
            next_page = page + 1
        pages = int(math.ceil(total / float(page_size)))
        result = {
            "total": total,
            "pages": pages,
            "has_next": has_next,
            "has_previous": has_previous,
            "next_page": next_page,
            "previous_page": previous_page,
            "items": items,
        }
        return result

    @staticmethod
    def paginate(query, page, page_size):
        if page <= 0:
            raise AttributeError("page needs to be >= 1")
        if page_size <= 0:
            raise AttributeError("page_size needs to be >= 1")
        items = query.limit(page_size).offset((page - 1) * page_size).all()
        total = query.order_by(None).count()
        result = Page.calculate_page(items, page, page_size, total)
        return result
