from utils.validation_utils import ValidationHelper

MAIN_USER_TYPE = "main"
CHILD_USER_TYPE = "child"
PARTNER_USER_TYPE = "partner"
GRANDCHILD_USER_TYPE = "grandchild"
PARENT_USER_TYPE = "parent"
DEPENDENT_USER_TYPE = "dependent"
OTHER_USER_TYPE = "other"
DEPENDENT_TYPES = [GRANDCHILD_USER_TYPE, PARENT_USER_TYPE, DEPENDENT_USER_TYPE]
ADVISER_USER_TYPE = "adviser"
CLIENT_USER_TYPE = "client"

UNIQUE_PLAN_USER_TYPES = ["main", "partner"]


# validation constants

MAXIMUM_DEPENDENT_SPEND = 10_000_000
MAXIMUM_NANNY_SPEND = 500_000
MAXIMUM_YEARS = 50


COUNTRY_LIST = ValidationHelper.get_countries()
COUNTY_CD_LIST = [country["iso_cd"] for country in COUNTRY_LIST]
