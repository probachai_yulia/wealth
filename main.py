# !/usr/bin/env python

# Python imports
import logging
import os

# External imports
import tornado.wsgi

# External imports
from tornado.options import options

import config
from models.session import Session
import uimethods
import web
from web_handlers import (
    admin,
    checks,
    permissions,
    presentation,
    survey,
    users,
    task,
    adviser_portal,
)

SERVER_SOFTWARE = os.getenv("SERVER_SOFTWARE", "")
IS_DEPLOYMENT = SERVER_SOFTWARE.startswith("Google App Engine/")

# Project imports
config.initialize()

server_settings = {
    "template_path": os.path.join(os.path.dirname(__file__), "templates"),
    "cookie_secret": "61oETzKXQAGaYdkL5gEmGeJJFuYh7EQnp2XdTP1o/Vo=",
    "login_url": "/",
    "debug": options.debug_mode,
    "autoreload": options.autoreload,
    "static_path": options.static_path,
    "ui_methods": uimethods,
}

url_list = [
    (r"/(privacy|terms)", web.StaticHandler),
    (r"/login", web.LoginHandler),
    (r"/logout", web.LogoutHandler),
    (r"/auth_callback", web.OAuthCallbackHandler),
    # task endpoints
    (r"/api/v1/tasks/migrate", task.MigrateHandler),
    (r"/api/v1/tasks/migrate/(?P<revision>[-\w]+)?", task.MigrateHandler),
    (r"/api/v1/tasks/rollback/(?P<revision>[-\w]+)?", task.RollbackHandler),
    (r"/api/v1/tasks/migration_history", task.MigrationHistoryHandler),
    (r"/api/v1/tasks/dev_user", task.DevUserHandler),
    (r"/api/v1/tasks/clear_database", task.ClearDatabaseHandler),
    # password endpoints
    (r"/api/v1/users/(?P<profile_uuid>[-\w]+)?/set_password", users.SetUserPasswordHandler),
    (r"/api/v1/users/change_password", users.ChangeUserPasswordHandler),
    (r"/api/v1/users", users.UserRoleHandler),
    # system endpoints
    (r"/api/v1/cron/token_expires", admin.CronExpireUserTokenHandler),
    (r"/api/v1/users/(?P<token>[-\w]+)?/permissions", permissions.UserTokenPermissionHandler),
    (r"/api/v1/users/(?P<profile_uuid>[-\w]+)?/token", admin.GetCreateUserTokenHandler),
    (r"/api/v1/users/(?P<profile_uuid>[-\w]+)?/change_email", users.ChangeUserEmailHandler),
    # get/update current user
    (r"/api/v1/users/me", users.CurrentUserHandler),
    # get current user active plan
    (r"/api/v1/users/me/active_plan", survey.CurrentPlanHandler),
    # creates user with role
    (r"/api/v1/users/roles/(?P<user_type>[-\w]+)?", users.UserRoleHandler),
    # update profile
    (r"/api/v1/users/(?P<profile_uuid>[-\w]+)?", users.UserHandler),
    # terms and conditions
    (r"/api/v1/users/(?P<profile_uuid>[-\w]+)?/accept_terms", survey.AcceptTermsHandler),
    (
        r"/api/v1/advisers/(?P<adviser_profile_uuid>[-\w]+)?/set_client/(?P<client_profile_uuid>[-\w]+)?",
        users.SetClientToAdviserHandler,
    ),
    (
        r"/api/v1/paraplanners/(?P<paraplanner_profile_uuid>[-\w]+)?/set_adviser/(?P<adviser_profile_uuid>[-\w]+)?",
        users.SetAdviserToParaplannerHandler,
    ),
    (
        r"/api/v1/paraplanners/(?P<profile_uuid>[-\w]+)?/advisers",
        users.GetParaplannerAdvisersHandler,
    ),
    # managing roles
    (r"/api/v1/roles", permissions.RoleHandler),
    (r"/api/v1/roles/(?P<role_name>[-\w]+)?", permissions.RoleHandler),
    (
        r"/api/v1/roles/(?P<role_name>[-\w]+)?/users/(?P<profile_uuid>[-\w]+)?",
        permissions.UserToRoleHandler,
    ),
    # managing permissions
    (r"/api/v1/permissions", permissions.PermissionHandler),
    (r"/api/v1/permissions/(?P<code>[-\w]+)?", permissions.PermissionHandler),
    (
        r"/api/v1/permissions/(?P<permission_code>[-\w]+)?/users/(?P<profile_uuid>[-\w]+)?",
        permissions.UserPermissionHandler,
    ),
    (
        r"/api/v1/permissions/(?P<permission_code>[-\w]+)?/roles/(?P<role_name>[-\w]+)?",
        permissions.RolePermissionHandler,
    ),
    # get address
    (r"/api/v1/get_address", survey.GetAddressHandler),
    # WebApp
    (r"/api/v1/presentation", presentation.PresentationHandler),
    (r"/api/v1/contact", presentation.ContactHandler),
    # plan option lists
    (r"/api/v1/options", survey.OptionsListHandler),
    # plan endpoints
    (r"/api/v1/plan", survey.PlanHandler),
    (r"/api/v1/plan/(?P<plan_uuid>[-\w]+)?", survey.PlanHandler),
    (r"/api/v1/plan/(?P<plan_uuid>[-\w]+)?/submit", survey.SubmitPlanHandler),
    (r"/api/v1/plan/(?P<plan_uuid>[-\w]+)?/asset", survey.PlanAssetHandler),
    (
        r"/api/v1/plan/(?P<plan_uuid>[-\w]+)?/asset/(?P<plan_asset_uuid>[-\w]+)?",
        survey.PlanAssetHandler,
    ),
    (
        r"/api/v1/plan/(?P<plan_uuid>[-\w]+)?/asset/(?P<plan_asset_uuid>[-\w]+)?/cashflow",
        survey.PlanAssetCashFlowHandler,
    ),
    (r"/api/v1/plan/(?P<plan_uuid>[-\w]+)?/debt", survey.PlanDebtHandler),
    (r"/api/v1/plan/(?P<plan_uuid>[-\w]+)?/debt/(?P<debt_uuid>[-\w]+)?", survey.PlanDebtHandler),
    (
        r"/api/v1/plan/(?P<plan_uuid>[-\w]+)?/debt/(?P<debt_uuid>[-\w]+)?/cashflow",
        survey.PlanDebtCashFlowHandler,
    ),
    (r"/api/v1/plan/(?P<plan_uuid>[-\w]+)?/employment", survey.PlanEmploymentHandler),
    (
        r"/api/v1/plan/(?P<plan_uuid>[-\w]+)?/employment/(?P<employment_uuid>[-\w]+)?",
        survey.PlanEmploymentHandler,
    ),
    (
        r"/api/v1/plan/(?P<plan_uuid>[-\w]+)?/employment/(?P<employment_uuid>[-\w]+)?/cashflow",
        survey.PlanEmploymentCashFlowHandler,
    ),
    (r"/api/v1/plan/(?P<plan_uuid>[-\w]+)?/cashflow", survey.PlanCashflowHandler),
    (
        r"/api/v1/plan/(?P<plan_uuid>[-\w]+)?/cashflow/(?P<cashflow_uuid>[-\w]+)?",
        survey.PlanCashflowHandler,
    ),
    (r"/api/v1/plan/(?P<plan_uuid>[-\w]+)?/address", survey.AddressHandler),
    (r"/api/v1/address/(?P<address_uuid>[-\w]+)?", survey.UpdateDeleteAddressHandler),
    (r"/api/v1/plan/(?P<plan_uuid>[-\w]+)?/giving", survey.PlanGivingHandler),
    (
        r"/api/v1/plan/(?P<plan_uuid>[-\w]+)?/giving/(?P<plan_giving_uuid>[-\w]+)?",
        survey.PlanGivingHandler,
    ),
    (
        r"/api/v1/plan/(?P<plan_uuid>[-\w]+)?/giving/(?P<plan_giving_uuid>[-\w]+)?/cashflow",
        survey.PlanGivingCashFlowHandler,
    ),
    (r"/api/v1/plan/(?P<plan_uuid>[-\w]+)?/users", survey.PlanUserHandler),
    (
        r"/api/v1/plan/(?P<plan_uuid>[-\w]+)?/users/(?P<profile_uuid>[-\w]+)?",
        survey.PlanUserHandler,
    ),
    (
        r"/api/v1/plan/(?P<plan_uuid>[-\w]+)?/users/(?P<profile_uuid>[-\w]+)?/employment",
        survey.PlanEmploymentHandler,
    ),
    # AML/KYC checks
    (r"/api/v1/aml/(?P<profile_uuid>[-\w]+)?/check", checks.AmlCheckHandler),
    (r"/api/v1/aml/(?P<profile_uuid>[-\w]+)?/webhook", checks.AmlWebhookHandler),
    (r"/api/v1/aml/(?P<profile_uuid>[-\w]+)?/cases", checks.AmlCasesHandler),
    (r"/api/v1/aml/(?P<profile_uuid>[-\w]+)?/newest_case", checks.AmlNewestCaseHandler),
    (r"/api/v1/aml/(?P<case_id>[-\w]+)?/status", checks.AmlStatusHandler),
    (r"/api/v1/aml/(?P<case_id>[-\w]+)?/results", checks.AmlResultsHandler),
    (r"/api/v1/aml/(?P<case_id>[-\w]+)?/documents", checks.AmlDocumentsHandler),
    # soft facts
    (r"/api/v1/plan/(?P<plan_uuid>[-\w]+)?/soft_fact", adviser_portal.SoftFactHandler),
    (r"/api/v1/soft_fact/(?P<soft_fact_uuid>[-\w]+)?", adviser_portal.UpdateDeleteSoftFactHandler),
    # upload documents
    (r"/api/v1/plan/(?P<plan_uuid>[-\w]+)?/documents", survey.UploadFileHandler),
    (
        r"/api/v1/plan/(?P<plan_uuid>[-\w]+)?/asset/(?P<asset_uuid>[-\w]+)?/documents",
        survey.UploadFileHandler,
    ),
    (r"/api/v1/documents/(?P<plan_document_uuid>[-\w]+)?", survey.DownloadFileHandler),
    (
        r"/api/v1/documents/(?P<plan_document_uuid>[-\w]+)?/(?P<type>[-\w]+)?",
        survey.DownloadFileHandler,
    ),
    (r"/", web.HomeHandler),
    (r"/.*", web.BaseHandler),
]


class Application(tornado.web.Application):
    def __init__(self, *ag, **kw):
        tornado.web.Application.__init__(self, *ag, **kw)
        self.session = Session()


if options.no_access_noise is True:
    logging.getLogger("tornado.access").disabled = True

app = Application(url_list, **server_settings)
if __name__ == "__main__":
    import tornado.httpserver

    app.listen(options.port)
    tornado.ioloop.IOLoop.current().start()
else:
    app = tornado.wsgi.WSGIAdapter(app)
