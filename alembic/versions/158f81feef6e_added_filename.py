"""added filename

Revision ID: 158f81feef6e
Revises: 8fcd0e301dc6
Create Date: 2018-10-17 18:50:32.704143

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = "158f81feef6e"
down_revision = "8fcd0e301dc6"
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.add_column("plan_document", sa.Column("filename", sa.String(length=255), nullable=True))
    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_column("plan_document", "filename")
    # ### end Alembic commands ###
