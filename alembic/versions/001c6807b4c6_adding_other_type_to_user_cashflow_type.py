"""Adding other type to user_cashflow_type

Revision ID: 001c6807b4c6
Revises: ba933b2003a6
Create Date: 2018-09-24 16:38:54.222291

"""
from alembic import op
import sqlalchemy as sa
from sqlalchemy.sql import table

# revision identifiers, used by Alembic.
revision = "001c6807b4c6"
down_revision = "ba933b2003a6"
branch_labels = None
depends_on = None


def upgrade():
    user_cashflow_type = table(
        "user_cashflow_type",
        sa.column("user_cashflow_type_id", sa.Integer),
        sa.column("code", sa.String),
        sa.column("name", sa.String),
    )
    op.execute(
        user_cashflow_type.update()
        .where(user_cashflow_type.c.user_cashflow_type_id == 14)
        .values({"name": "Other (Work)"})
    )
    op.bulk_insert(
        user_cashflow_type, [{"user_cashflow_type_id": 15, "code": "other", "name": "Other"}]
    )


def downgrade():
    pass
