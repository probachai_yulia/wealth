"""Changing profile_uuid to user_id

Revision ID: 55b077362cb0
Revises: 64999ccc828e
Create Date: 2018-10-16 15:13:55.706015

"""
from alembic import op
import sqlalchemy as sa
from sqlalchemy.dialects import mysql

# revision identifiers, used by Alembic.
revision = "55b077362cb0"
down_revision = "64999ccc828e"
branch_labels = None
depends_on = None


def op_add_column(*ag, **kw):
    try:
        if not op.get_context().as_sql:
            connection = op.get_bind()
            connection.execution_options(isolation_level="AUTOCOMMIT")
            op.add_column(*ag, **kw)
    except sa.exc.OperationalError as e:
        if "Duplicate column name" not in str(e):
            raise


def op_create_foreign_key(*ag, **kw):
    try:
        if not op.get_context().as_sql:
            connection = op.get_bind()
            connection.execution_options(isolation_level="AUTOCOMMIT")
            op.create_foreign_key(*ag, **kw)
    except sa.exc.IntegrityError as e:
        if "a foreign key constraint fails" not in str(e):
            raise


def op_drop_constraint(*ag, **kw):
    try:
        if not op.get_context().as_sql:
            connection = op.get_bind()
            connection.execution_options(isolation_level="AUTOCOMMIT")
            op.drop_constraint(*ag, **kw)
    except sa.exc.OperationalError as e:
        if "check that column/key exists" not in str(e):
            raise


def op_create_index(*ag, **kw):
    try:
        if not op.get_context().as_sql:
            connection = op.get_bind()
            connection.execution_options(isolation_level="AUTOCOMMIT")
            op.create_index(*ag, **kw)
    except sa.exc.OperationalError as e:
        if "Duplicate key name" not in str(e):
            raise


def op_drop_index(*ag, **kw):
    try:
        if not op.get_context().as_sql:
            connection = op.get_bind()
            connection.execution_options(isolation_level="AUTOCOMMIT")
            op.drop_index(*ag, **kw)
    except sa.exc.OperationalError as e:
        if "Cannot drop index" not in str(e) and "Can't DROP" not in str(e):
            raise


def upgrade():
    op_add_column("aml_kyc_case", sa.Column("user_id", sa.Integer(), nullable=True))
    con = op.get_bind()
    con.execute(
        """
        UPDATE aml_kyc_case c
        INNER JOIN user u on u.profile_uuid = c.profile_uuid
        SET c.user_id = u.user_id
    """
    )
    op.alter_column("aml_kyc_case", "user_id", nullable=False, existing_type=sa.Integer())
    op_create_index(op.f("ix_aml_kyc_case_user_id"), "aml_kyc_case", ["user_id"], unique=False)
    op_drop_index("ix_aml_kyc_case_is_newest_concluded", table_name="aml_kyc_case")
    op_create_index(
        "ix_aml_kyc_case_is_newest_concluded", "aml_kyc_case", ["user_id", "updated"], unique=False
    )
    op.drop_constraint("aml_kyc_case_ibfk_1", "aml_kyc_case", type_="foreignkey")
    op_create_foreign_key(
        "fk_aml_kyc_case_user_user_id", "aml_kyc_case", "user", ["user_id"], ["user_id"]
    )
    op.drop_column("aml_kyc_case", "profile_uuid")


def downgrade():
    op_add_column(
        "aml_kyc_case", sa.Column("profile_uuid", mysql.VARCHAR(length=36), nullable=False)
    )
    con = op.get_bind()
    con.execute(
        """
        UPDATE aml_kyc_case c
        INNER JOIN user u on c.user_id = u.user_id
        SET c.profile_uuid = u.profile_uuid
    """
    )
    op_drop_constraint("fk_aml_kyc_case_user_user_id", "aml_kyc_case", type_="foreignkey")
    op_create_foreign_key(
        "aml_kyc_case_ibfk_1", "aml_kyc_case", "user", ["profile_uuid"], ["profile_uuid"]
    )
    op_drop_index("ix_aml_kyc_case_is_newest_concluded", table_name="aml_kyc_case")
    op_create_index(
        "ix_aml_kyc_case_is_newest_concluded",
        "aml_kyc_case",
        ["profile_uuid", "updated"],
        unique=False,
    )
    op_drop_index(op.f("ix_aml_kyc_case_user_id"), table_name="aml_kyc_case")
    op.drop_column("aml_kyc_case", "user_id")
