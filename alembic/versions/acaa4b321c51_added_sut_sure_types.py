"""added sut sure types

Revision ID: acaa4b321c51
Revises: 62df3342b0b0
Create Date: 2018-08-31 13:10:13.228804

"""
from alembic import op

# revision identifiers, used by Alembic.
from sqlalchemy import Integer, String, column, table

revision = "acaa4b321c51"
down_revision = "62df3342b0b0"
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    # ### commands auto generated by Alembic - please adjust! ###
    rate_type = table(
        "rate_type", column("rate_type_id", Integer), column("code", String), column("name", String)
    )

    op.execute(
        rate_type.update()
        .where(rate_type.c.code == op.inline_literal("variable"))
        .values({"name": op.inline_literal("Variable rate")})
    )

    tax_benefit = table(
        "tax_benefit",
        column("tax_benefit_id", Integer),
        column("code", String),
        column("name", String),
    )
    op.bulk_insert(tax_benefit, [{"tax_benefit_id": 4, "code": "unknown", "name": "Unknown"}])


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    pass
