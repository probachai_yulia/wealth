"""Adjusting the aml tables

Revision ID: 8e59cd626f6d
Revises: ff2e0f5f1a3c
Create Date: 2018-09-27 13:40:39.172181

"""
from alembic import op
import sqlalchemy as sa

# revision identifiers, used by Alembic.
revision = "8e59cd626f6d"
down_revision = "ff2e0f5f1a3c"
branch_labels = None
depends_on = None


def upgrade():
    op.drop_table("aml_kyc_result_bank_account")
    op.drop_table("aml_kyc_result")
    op.create_table(
        "aml_kyc_result",
        sa.Column("result_id", sa.Integer, nullable=False),
        sa.Column("case_id", sa.String(length=36), nullable=False),
        sa.Column("provider", sa.Enum("TS", "WC", "HTAR"), nullable=False),
        sa.Column("status", sa.Enum("pass", "fail", "refer", "pending"), nullable=False),
        sa.Column("valid_until", sa.DateTime(), nullable=False),
        sa.Column("url", sa.String(255), nullable=True),
        sa.Column("pep", sa.Boolean(), nullable=True),
        sa.Column("bank_account_valid", sa.Text(), nullable=True),
        sa.Column("trigger", sa.Text(), nullable=False),
        sa.Column("archived_at", sa.Text(), nullable=True),
        sa.Column("created", sa.DateTime(), nullable=False),
        sa.Column("updated", sa.DateTime(), nullable=False),
        sa.ForeignKeyConstraint(["case_id"], ["aml_kyc_case.case_id"]),
        sa.PrimaryKeyConstraint("result_id"),
    )
    op.create_table(
        "aml_kyc_result_bank_account",
        sa.Column("result_id", sa.Integer, nullable=False),
        sa.Column("sort_code_valid", sa.Boolean(), nullable=True),
        sa.Column("account_number_valid", sa.Boolean(), nullable=True),
        sa.Column("account_name_valid", sa.Boolean(), nullable=True),
        sa.Column("account_address_valid", sa.Boolean(), nullable=True),
        sa.Column("account_status", sa.Boolean(), nullable=True),
        sa.Column("error", sa.Text(), nullable=True),
        sa.ForeignKeyConstraint(["result_id"], ["aml_kyc_result.result_id"]),
        sa.PrimaryKeyConstraint("result_id"),
    )
    op.create_unique_constraint("uq_aml_kyc_document_url", "aml_kyc_document", ["url"])


def downgrade():
    op.drop_table("aml_kyc_result_bank_account")
    op.drop_table("aml_kyc_result")
    op.create_table(
        "aml_kyc_result",
        sa.Column("case_id", sa.String(length=36), nullable=False),
        sa.Column("provider", sa.Enum("TS", "WC", "HTAR"), nullable=False),
        sa.Column("result_id", sa.String(length=36), nullable=True),
        sa.Column("status", sa.Enum("pass", "fail", "refer", "pending"), nullable=False),
        sa.Column("valid_until", sa.DateTime(), nullable=False),
        sa.Column("url", sa.String(255), nullable=True),
        sa.Column("pep", sa.Boolean(), nullable=True),
        sa.Column("bank_account_valid", sa.Text(), nullable=True),
        sa.Column("archived_at", sa.Text(), nullable=True),
        sa.Column("created", sa.DateTime(), nullable=False),
        sa.Column("updated", sa.DateTime(), nullable=False),
        sa.ForeignKeyConstraint(["case_id"], ["aml_kyc_case.case_id"]),
        sa.PrimaryKeyConstraint("case_id", "provider"),
        sa.UniqueConstraint("result_id"),
    )
    op.create_table(
        "aml_kyc_result_bank_account",
        sa.Column("result_id", sa.String(length=36), nullable=False),
        sa.Column("sort_code_valid", sa.Boolean(), nullable=True),
        sa.Column("account_number_valid", sa.Boolean(), nullable=True),
        sa.Column("account_name_valid", sa.Boolean(), nullable=True),
        sa.Column("account_address_valid", sa.Boolean(), nullable=True),
        sa.Column("account_status", sa.Boolean(), nullable=True),
        sa.Column("error", sa.Text(), nullable=True),
        sa.ForeignKeyConstraint(["result_id"], ["aml_kyc_result.result_id"]),
        sa.PrimaryKeyConstraint("result_id"),
    )
    op.drop_constraint("uq_aml_kyc_document_url", "aml_kyc_document", "unique")
