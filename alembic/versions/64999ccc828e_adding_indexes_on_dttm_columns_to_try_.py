"""Adding indexes on dttm columns to try better performance on queries

Revision ID: 64999ccc828e
Revises: 466684b83fd3
Create Date: 2018-10-15 19:03:47.303806

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = "64999ccc828e"
down_revision = "466684b83fd3"
branch_labels = None
depends_on = None


def op_create_index(*ag, **kw):
    try:
        if not op.get_context().as_sql:
            connection = op.get_bind()
            connection.execution_options(isolation_level="AUTOCOMMIT")
            op.create_index(*ag, **kw)
    except sa.exc.OperationalError as e:
        if "Duplicate key name" not in str(e):
            raise


def op_drop_index(*ag, **kw):
    try:
        if not op.get_context().as_sql:
            connection = op.get_bind()
            connection.execution_options(isolation_level="AUTOCOMMIT")
            op.drop_index(*ag, **kw)
    except sa.exc.OperationalError as e:
        if "Cannot drop index" not in str(e) and "Can't DROP" not in str(e):
            raise


def upgrade():
    op.alter_column(
        "aml_kyc_result", "archived_at", nullable=True, existing_type=sa.String(length=52)
    )
    op_create_index(
        "ix_aml_kyc_case_is_newest_concluded",
        "aml_kyc_case",
        ["profile_uuid", "updated"],
        unique=False,
    )
    op_create_index(op.f("ix_aml_kyc_case_created"), "aml_kyc_case", ["created"], unique=False)
    op_create_index(op.f("ix_aml_kyc_case_updated"), "aml_kyc_case", ["updated"], unique=False)
    op_create_index(
        op.f("ix_aml_kyc_document_case_id"), "aml_kyc_document", ["case_id"], unique=False
    )
    op_create_index(
        op.f("ix_aml_kyc_document_created"), "aml_kyc_document", ["created"], unique=False
    )
    op_create_index(
        op.f("ix_aml_kyc_document_updated"), "aml_kyc_document", ["updated"], unique=False
    )
    op_create_index(
        "ix_aml_kyc_case_newest_results",
        "aml_kyc_result",
        ["provider", "case_id", "status", "updated"],
        unique=False,
    )
    op_create_index(
        op.f("ix_aml_kyc_result_archived_at"), "aml_kyc_result", ["archived_at"], unique=False
    )
    op_create_index(op.f("ix_aml_kyc_result_case_id"), "aml_kyc_result", ["case_id"], unique=False)
    op_create_index(op.f("ix_aml_kyc_result_created"), "aml_kyc_result", ["created"], unique=False)
    op_create_index(op.f("ix_aml_kyc_result_status"), "aml_kyc_result", ["status"], unique=False)
    op_create_index(op.f("ix_aml_kyc_result_updated"), "aml_kyc_result", ["updated"], unique=False)
    op_create_index(op.f("ix_log_create_dttm"), "log", ["create_dttm"], unique=False)
    op_create_index(op.f("ix_log_delete_dttm"), "log", ["delete_dttm"], unique=False)
    op_create_index(op.f("ix_log_update_dttm"), "log", ["update_dttm"], unique=False)
    op_create_index(
        op.f("ix_modification_history_create_dttm"),
        "modification_history",
        ["create_dttm"],
        unique=False,
    )
    op_create_index(
        op.f("ix_modification_history_delete_dttm"),
        "modification_history",
        ["delete_dttm"],
        unique=False,
    )
    op_create_index(
        op.f("ix_modification_history_update_dttm"),
        "modification_history",
        ["update_dttm"],
        unique=False,
    )
    op_create_index(op.f("ix_permission_create_dttm"), "permission", ["create_dttm"], unique=False)
    op_create_index(op.f("ix_permission_delete_dttm"), "permission", ["delete_dttm"], unique=False)
    op_create_index(op.f("ix_permission_update_dttm"), "permission", ["update_dttm"], unique=False)
    op_create_index(op.f("ix_plan_create_dttm"), "plan", ["create_dttm"], unique=False)
    op_create_index(op.f("ix_plan_delete_dttm"), "plan", ["delete_dttm"], unique=False)
    op_create_index(op.f("ix_plan_update_dttm"), "plan", ["update_dttm"], unique=False)
    op_create_index(
        op.f("ix_plan_soft_fact_create_dttm"), "plan_soft_fact", ["create_dttm"], unique=False
    )
    op_create_index(
        op.f("ix_plan_soft_fact_update_dttm"), "plan_soft_fact", ["update_dttm"], unique=False
    )
    op_create_index(op.f("ix_plan_user_create_dttm"), "plan_user", ["create_dttm"], unique=False)
    op_create_index(op.f("ix_plan_user_delete_dttm"), "plan_user", ["delete_dttm"], unique=False)
    op_create_index(op.f("ix_plan_user_update_dttm"), "plan_user", ["update_dttm"], unique=False)
    op_create_index(
        op.f("ix_profile_operation_history_create_dttm"),
        "profile_operation_history",
        ["create_dttm"],
        unique=False,
    )
    op_create_index(
        op.f("ix_profile_operation_history_delete_dttm"),
        "profile_operation_history",
        ["delete_dttm"],
        unique=False,
    )
    op_create_index(
        op.f("ix_profile_operation_history_update_dttm"),
        "profile_operation_history",
        ["update_dttm"],
        unique=False,
    )
    op_create_index(op.f("ix_role_create_dttm"), "role", ["create_dttm"], unique=False)
    op_create_index(op.f("ix_role_delete_dttm"), "role", ["delete_dttm"], unique=False)
    op_create_index(op.f("ix_role_update_dttm"), "role", ["update_dttm"], unique=False)
    op_create_index(
        op.f("ix_role_permission_create_dttm"), "role_permission", ["create_dttm"], unique=False
    )
    op_create_index(
        op.f("ix_role_permission_delete_dttm"), "role_permission", ["delete_dttm"], unique=False
    )
    op_create_index(
        op.f("ix_role_permission_update_dttm"), "role_permission", ["update_dttm"], unique=False
    )
    op_create_index(
        op.f("ix_terms_and_condition_accept_create_dttm"),
        "terms_and_condition_accept",
        ["create_dttm"],
        unique=False,
    )
    op_create_index(
        op.f("ix_terms_and_condition_accept_delete_dttm"),
        "terms_and_condition_accept",
        ["delete_dttm"],
        unique=False,
    )
    op_create_index(
        op.f("ix_terms_and_condition_accept_update_dttm"),
        "terms_and_condition_accept",
        ["update_dttm"],
        unique=False,
    )
    op_create_index(
        op.f("ix_token_to_user_table_create_dttm"),
        "token_to_user_table",
        ["create_dttm"],
        unique=False,
    )
    op_create_index(
        op.f("ix_token_to_user_table_delete_dttm"),
        "token_to_user_table",
        ["delete_dttm"],
        unique=False,
    )
    op_create_index(
        op.f("ix_token_to_user_table_update_dttm"),
        "token_to_user_table",
        ["update_dttm"],
        unique=False,
    )
    op_create_index(op.f("ix_user_create_dttm"), "user", ["create_dttm"], unique=False)
    op_create_index(op.f("ix_user_delete_dttm"), "user", ["delete_dttm"], unique=False)
    op_create_index(op.f("ix_user_update_dttm"), "user", ["update_dttm"], unique=False)
    op_drop_index("profile_uuid", table_name="user")
    op_create_index(
        op.f("ix_user_permission_create_dttm"), "user_permission", ["create_dttm"], unique=False
    )
    op_create_index(
        op.f("ix_user_permission_delete_dttm"), "user_permission", ["delete_dttm"], unique=False
    )
    op_create_index(
        op.f("ix_user_permission_update_dttm"), "user_permission", ["update_dttm"], unique=False
    )
    op_create_index(op.f("ix_user_role_create_dttm"), "user_role", ["create_dttm"], unique=False)
    op_create_index(op.f("ix_user_role_delete_dttm"), "user_role", ["delete_dttm"], unique=False)
    op_create_index(op.f("ix_user_role_update_dttm"), "user_role", ["update_dttm"], unique=False)


def downgrade():
    op_drop_index(op.f("ix_user_role_update_dttm"), table_name="user_role")
    op_drop_index(op.f("ix_user_role_delete_dttm"), table_name="user_role")
    op_drop_index(op.f("ix_user_role_create_dttm"), table_name="user_role")
    op_drop_index(op.f("ix_user_permission_update_dttm"), table_name="user_permission")
    op_drop_index(op.f("ix_user_permission_delete_dttm"), table_name="user_permission")
    op_drop_index(op.f("ix_user_permission_create_dttm"), table_name="user_permission")
    op_drop_index(op.f("ix_user_update_dttm"), table_name="user")
    op_drop_index(op.f("ix_user_profile_uuid"), table_name="user")
    op_drop_index(op.f("ix_user_delete_dttm"), table_name="user")
    op_drop_index(op.f("ix_user_create_dttm"), table_name="user")
    op_drop_index(op.f("ix_token_to_user_table_update_dttm"), table_name="token_to_user_table")
    op_drop_index(op.f("ix_token_to_user_table_delete_dttm"), table_name="token_to_user_table")
    op_drop_index(op.f("ix_token_to_user_table_create_dttm"), table_name="token_to_user_table")
    op_drop_index(
        op.f("ix_terms_and_condition_accept_update_dttm"), table_name="terms_and_condition_accept"
    )
    op_drop_index(
        op.f("ix_terms_and_condition_accept_delete_dttm"), table_name="terms_and_condition_accept"
    )
    op_drop_index(
        op.f("ix_terms_and_condition_accept_create_dttm"), table_name="terms_and_condition_accept"
    )
    op_drop_index(op.f("ix_role_permission_update_dttm"), table_name="role_permission")
    op_drop_index(op.f("ix_role_permission_delete_dttm"), table_name="role_permission")
    op_drop_index(op.f("ix_role_permission_create_dttm"), table_name="role_permission")
    op_drop_index(op.f("ix_role_update_dttm"), table_name="role")
    op_drop_index(op.f("ix_role_delete_dttm"), table_name="role")
    op_drop_index(op.f("ix_role_create_dttm"), table_name="role")
    op_drop_index(
        op.f("ix_profile_operation_history_update_dttm"), table_name="profile_operation_history"
    )
    op_drop_index(
        op.f("ix_profile_operation_history_delete_dttm"), table_name="profile_operation_history"
    )
    op_drop_index(
        op.f("ix_profile_operation_history_create_dttm"), table_name="profile_operation_history"
    )
    op_drop_index(op.f("ix_plan_user_update_dttm"), table_name="plan_user")
    op_drop_index(op.f("ix_plan_user_delete_dttm"), table_name="plan_user")
    op_drop_index(op.f("ix_plan_user_create_dttm"), table_name="plan_user")
    op_drop_index(op.f("ix_plan_soft_fact_update_dttm"), table_name="plan_soft_fact")
    op_drop_index(op.f("ix_plan_soft_fact_create_dttm"), table_name="plan_soft_fact")
    op_drop_index(op.f("ix_plan_update_dttm"), table_name="plan")
    op_drop_index(op.f("ix_plan_delete_dttm"), table_name="plan")
    op_drop_index(op.f("ix_plan_create_dttm"), table_name="plan")
    op_drop_index(op.f("ix_permission_update_dttm"), table_name="permission")
    op_drop_index(op.f("ix_permission_delete_dttm"), table_name="permission")
    op_drop_index(op.f("ix_permission_create_dttm"), table_name="permission")
    op_drop_index(op.f("ix_modification_history_update_dttm"), table_name="modification_history")
    op_drop_index(op.f("ix_modification_history_delete_dttm"), table_name="modification_history")
    op_drop_index(op.f("ix_modification_history_create_dttm"), table_name="modification_history")
    op_drop_index(op.f("ix_log_update_dttm"), table_name="log")
    op_drop_index(op.f("ix_log_delete_dttm"), table_name="log")
    op_drop_index(op.f("ix_log_create_dttm"), table_name="log")
    op_drop_index(op.f("ix_aml_kyc_result_updated"), table_name="aml_kyc_result")
    op_drop_index(op.f("ix_aml_kyc_result_status"), table_name="aml_kyc_result")
    op_drop_index(op.f("ix_aml_kyc_result_created"), table_name="aml_kyc_result")
    op_drop_index(op.f("ix_aml_kyc_result_case_id"), table_name="aml_kyc_result")
    op_drop_index(op.f("ix_aml_kyc_result_archived_at"), table_name="aml_kyc_result")
    op_drop_index("ix_aml_kyc_case_newest_results", table_name="aml_kyc_result")
    op_drop_index(op.f("ix_aml_kyc_document_updated"), table_name="aml_kyc_document")
    op_drop_index(op.f("ix_aml_kyc_document_created"), table_name="aml_kyc_document")
    op_drop_index(op.f("ix_aml_kyc_document_case_id"), table_name="aml_kyc_document")
    op_drop_index(op.f("ix_aml_kyc_case_updated"), table_name="aml_kyc_case")
    op_drop_index("ix_aml_kyc_case_is_newest_concluded", table_name="aml_kyc_case")
    op_drop_index(op.f("ix_aml_kyc_case_created"), table_name="aml_kyc_case")

    op.alter_column("aml_kyc_result", "archived_at", nullable=True, existing_type=sa.Text)
