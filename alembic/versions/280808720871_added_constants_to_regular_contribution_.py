"""added constants to regular_contribution_type table

Revision ID: 280808720871
Revises: 5152ec34570f
Create Date: 2018-09-05 17:36:02.390760

"""
from alembic import op

# revision identifiers, used by Alembic.
from sqlalchemy import Integer, String, column, table

revision = "280808720871"
down_revision = "5152ec34570f"
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    regular_contribution_type = table(
        "regular_contribution_type",
        column("regular_contribution_type_id", Integer),
        column("code", String),
        column("name", String),
    )
    op.bulk_insert(
        regular_contribution_type,
        [
            {
                "regular_contribution_type_id": 1,
                "code": "employer_contribution",
                "name": "Employer contribution",
            },
            {
                "regular_contribution_type_id": 2,
                "code": "personal_contribution",
                "name": "Personal Contribution",
            },
            {"regular_contribution_type_id": 3, "code": "receive_income", "name": "Receive income"},
        ],
    )


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    pass
    # ### end Alembic commands ###
