"""Merging heads

Revision ID: ff2e0f5f1a3c
Revises: f5b8ab155d3a, 001c6807b4c6
Create Date: 2018-09-27 13:39:58.166446

"""


# revision identifiers, used by Alembic.
revision = "ff2e0f5f1a3c"
down_revision = ("f5b8ab155d3a", "001c6807b4c6")
branch_labels = None
depends_on = None


def upgrade():
    pass


def downgrade():
    pass
