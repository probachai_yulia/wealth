"""Change roles

Revision ID: a08e9ce3df80
Revises: fcfff8964f34
Create Date: 2018-08-14 11:43:55.219048

"""
from alembic import op

# revision identifiers, used by Alembic.
from sqlalchemy import table, column, Integer, String

revision = "a08e9ce3df80"
down_revision = "fcfff8964f34"
branch_labels = None
depends_on = None


def upgrade():
    role = table("role", column("role_id", Integer), column("name", String))
    op.execute(
        role.update()
        .where(role.c.role_id == op.inline_literal(1))
        .values({"name": op.inline_literal("client")})
    )


def downgrade():
    pass
