"""Adding some indexes to try improving query performance.

Revision ID: 466684b83fd3
Revises: f7906ce9175f
Create Date: 2018-10-15 18:50:57.950504

"""
from alembic import op
import sqlalchemy as sa
from sqlalchemy.dialects import mysql

# revision identifiers, used by Alembic.
revision = "466684b83fd3"
down_revision = "f7906ce9175f"
branch_labels = None
depends_on = None


def upgrade():
    try:
        op.drop_index("uq_aml_kyc_document_url", table_name="aml_kyc_document")
        op.drop_index("name", table_name="asset_type")
    except Exception:
        pass
    op.create_index(
        op.f("ix_plan_user_approximate_age_id"), "plan_user", ["approximate_age_id"], unique=False
    )
    op.create_index(op.f("ix_plan_user_plan_id"), "plan_user", ["plan_id"], unique=False)
    op.create_index(
        op.f("ix_plan_user_plan_user_type_id"), "plan_user", ["plan_user_type_id"], unique=False
    )
    op.create_index(op.f("ix_plan_user_user_id"), "plan_user", ["user_id"], unique=False)
    try:
        op.drop_column("plan_user", "expected_duration")
    except Exception:
        pass
    op.create_index(
        op.f("ix_role_permission_permission_id"), "role_permission", ["permission_id"], unique=False
    )
    op.create_index(
        op.f("ix_role_permission_role_id"), "role_permission", ["role_id"], unique=False
    )
    op.create_index(
        op.f("ix_terms_and_condition_accept_who_accept_user_id"),
        "terms_and_condition_accept",
        ["who_accept_user_id"],
        unique=False,
    )
    op.create_index(
        op.f("ix_token_to_user_table_user_id"), "token_to_user_table", ["user_id"], unique=False
    )
    op.create_index(
        op.f("ix_user_permission_permission_id"), "user_permission", ["permission_id"], unique=False
    )
    op.create_index(
        op.f("ix_user_permission_user_id"), "user_permission", ["user_id"], unique=False
    )
    op.create_index(op.f("ix_user_role_role_id"), "user_role", ["role_id"], unique=False)
    op.create_index(op.f("ix_user_role_user_id"), "user_role", ["user_id"], unique=False)


def downgrade():
    op.drop_index(op.f("ix_user_role_user_id"), table_name="user_role")
    op.drop_index(op.f("ix_user_role_role_id"), table_name="user_role")
    op.drop_index(op.f("ix_user_permission_user_id"), table_name="user_permission")
    op.drop_index(op.f("ix_user_permission_permission_id"), table_name="user_permission")
    op.drop_index(op.f("ix_token_to_user_table_user_id"), table_name="token_to_user_table")
    op.drop_index(
        op.f("ix_terms_and_condition_accept_who_accept_user_id"),
        table_name="terms_and_condition_accept",
    )
    op.drop_index(op.f("ix_role_permission_role_id"), table_name="role_permission")
    op.drop_index(op.f("ix_role_permission_permission_id"), table_name="role_permission")
    try:
        op.add_column(
            "plan_user",
            sa.Column(
                "expected_duration",
                mysql.INTEGER(display_width=11),
                autoincrement=False,
                nullable=True,
            ),
        )
    except Exception:
        pass
    op.drop_index(op.f("ix_plan_user_user_id"), table_name="plan_user")
    op.drop_index(op.f("ix_plan_user_plan_user_type_id"), table_name="plan_user")
    op.drop_index(op.f("ix_plan_user_plan_id"), table_name="plan_user")
    op.drop_index(op.f("ix_plan_user_approximate_age_id"), table_name="plan_user")
    try:
        op.create_index("name", "asset_type", ["name"], unique=True)
        op.create_index("uq_aml_kyc_document_url", "aml_kyc_document", ["url"], unique=True)
    except Exception:
        pass
