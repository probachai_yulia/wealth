"""added other_user_type

Revision ID: 65b095f5dbae
Revises: 71dc6aa0d47f
Create Date: 2018-09-06 15:17:32.361743

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = "65b095f5dbae"
down_revision = "71dc6aa0d47f"
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.add_column(
        "plan_user", sa.Column("plan_user_type_other", sa.String(length=45), nullable=True)
    )
    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_column("plan_user", "plan_user_type_other")
    # ### end Alembic commands ###
