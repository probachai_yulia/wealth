"""added bollean fields to plan

Revision ID: 776c52d1c61e
Revises: 001c6807b4c6
Create Date: 2018-10-01 15:42:23.110289

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = "776c52d1c61e"
down_revision = "001c6807b4c6"
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.add_column("plan", sa.Column("family_has_childcare", sa.Boolean(), nullable=True))
    op.add_column("plan", sa.Column("plan_has_other_income", sa.Boolean(), nullable=True))
    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_column("plan", "plan_has_other_income")
    op.drop_column("plan", "family_has_childcare")
    # ### end Alembic commands ###
