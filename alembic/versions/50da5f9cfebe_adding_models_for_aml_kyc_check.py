"""Adding models for aml/kyc check.

Revision ID: 50da5f9cfebe
Revises: 4f6bb08bd2a6
Create Date: 2018-09-13 11:33:13.388198

"""
from alembic import op
import sqlalchemy as sa

# revision identifiers, used by Alembic.
revision = "50da5f9cfebe"
down_revision = "4f6bb08bd2a6"
branch_labels = None
depends_on = None


def upgrade():
    op.create_table(
        "aml_kyc_case",
        sa.Column("case_id", sa.String(length=36), nullable=False),
        sa.Column("profile_uuid", sa.String(length=36), nullable=False),
        sa.Column("is_enabled", sa.Boolean(), nullable=False),
        sa.Column("created", sa.DateTime(), nullable=False),
        sa.Column("updated", sa.DateTime(), nullable=False),
        sa.ForeignKeyConstraint(["profile_uuid"], ["user.profile_uuid"]),
        sa.PrimaryKeyConstraint("case_id"),
    )
    op.create_table(
        "aml_kyc_document",
        sa.Column("document_id", sa.Integer(), nullable=False),
        sa.Column("case_id", sa.String(length=36), nullable=False),
        sa.Column("type", sa.Enum("passport", "driver_licence"), nullable=False),
        sa.Column("url", sa.String(255), nullable=False),
        sa.Column("created", sa.DateTime(), nullable=False),
        sa.Column("updated", sa.DateTime(), nullable=False),
        sa.ForeignKeyConstraint(["case_id"], ["aml_kyc_case.case_id"]),
        sa.PrimaryKeyConstraint("document_id"),
    )
    op.create_table(
        "aml_kyc_result",
        sa.Column("case_id", sa.String(length=36), nullable=False),
        sa.Column("provider", sa.Enum("TS", "WC", "HTAR"), nullable=False),
        sa.Column("result_id", sa.String(length=36), nullable=True),
        sa.Column("status", sa.Enum("pass", "fail", "refer", "pending"), nullable=False),
        sa.Column("valid_until", sa.DateTime(), nullable=False),
        sa.Column("url", sa.String(255), nullable=True),
        sa.Column("pep", sa.Boolean(), nullable=True),
        sa.Column("bank_account_valid", sa.Text(), nullable=True),
        sa.Column("archived_at", sa.Text(), nullable=True),
        sa.Column("created", sa.DateTime(), nullable=False),
        sa.Column("updated", sa.DateTime(), nullable=False),
        sa.ForeignKeyConstraint(["case_id"], ["aml_kyc_case.case_id"]),
        sa.PrimaryKeyConstraint("case_id", "provider"),
        sa.UniqueConstraint("result_id"),
    )
    op.create_table(
        "aml_kyc_result_bank_account",
        sa.Column("result_id", sa.String(length=36), nullable=False),
        sa.Column("sort_code_valid", sa.Boolean(), nullable=True),
        sa.Column("account_number_valid", sa.Boolean(), nullable=True),
        sa.Column("account_name_valid", sa.Boolean(), nullable=True),
        sa.Column("account_address_valid", sa.Boolean(), nullable=True),
        sa.Column("account_status", sa.Boolean(), nullable=True),
        sa.Column("error", sa.Text(), nullable=True),
        sa.ForeignKeyConstraint(["result_id"], ["aml_kyc_result.result_id"]),
        sa.PrimaryKeyConstraint("result_id"),
    )


def downgrade():
    op.drop_table("aml_kyc_result_bank_account")
    op.drop_table("aml_kyc_result")
    op.drop_table("aml_kyc_document")
    op.drop_table("aml_kyc_case")
