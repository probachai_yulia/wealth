"""added additional info to plan user

Revision ID: 0d4f66ff563f
Revises: 4f6bb08bd2a6
Create Date: 2018-09-11 15:39:15.323991

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = "0d4f66ff563f"
down_revision = "4f6bb08bd2a6"
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.add_column("plan_user", sa.Column("additional_info", sa.Text(), nullable=True))
    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_column("plan_user", "additional_info")
    # ### end Alembic commands ###
