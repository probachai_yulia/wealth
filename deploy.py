# -*- coding: utf-8 -*-

# Python Imports
import configparser
import os
import shutil
import subprocess
import sys

# Project Imports
import config
import popen2
from slackclient import SlackClient

# External imports
from tornado.options import options

# Command line options - abort early if options not set properly
config.initialize()
if not config.get_env():
    sys.exit(1)


def current_branch():
    branch, _, _ = popen2.popen3("git rev-parse --abbrev-ref HEAD")
    jon = branch.read().replace("\n", "")
    return jon


app_path = os.path.abspath(os.path.dirname(__file__))
sys.path.insert(0, app_path)


def incr_version(a1, a2, a3):
    if a3 < 99:
        return a1, a2, a3 + 1
    else:
        a3 = 0
        a2 += 1
    if a2 < 99:
        return a1, a2, a3
    else:
        a2 = 0
        a1 += 1
    return a1, a2, a3


class Release(object):
    def __init__(self, current_branch):
        self.branch = current_branch
        self.section = "version"
        self.last_tag = None
        self.config = configparser.ConfigParser()
        self.config.read("%s/version.cfg" % app_path)
        self.new_commits = ""

    def _set_prefix(self):
        self.tag_prefix = ""
        if self.branch != "master":
            self.tag_prefix = self.branch
            self.section = self.section + "_" + self.tag_prefix

    def _process_tag(self, t):
        tag = t.strip().replace("\n", "")
        if self.tag_prefix:
            if tag.startswith(self.tag_prefix):
                # value = tag.replace(self.tag_prefix, '').replace('-', '').split('.')
                value = tag.replace(self.tag_prefix, "")[1:].split("-")
                value = list(map(int, value))
                if value > self.max_tag:
                    self.max_tag = value
                    self.tag_changed = True
        elif tag[0].isdigit():
            try:
                value = list(map(int, tag.split("-")))
            except ValueError as ex:
                if "invalid literal for int()" in ex.message:
                    # print('skipping: %s' % tag)
                    return
                raise ex

            if value > self.max_tag:
                self.max_tag = value
                self.tag_changed = True

    def _process_tags(self):
        tags, _, _ = popen2.popen3("git tag")
        try:
            # gets current version number
            self.initial_tag = self.max_tag = self.config.get(self.section, "version").split("-")
            if self.branch == "master":
                self.last_tag = self.config.get(self.section, "last").split("-")
        except configparser.NoSectionError:
            self.config.add_section(self.section)
            self.initial_tag = self.max_tag = [0, 0, 0]
        self.max_tag = list(map(int, self.max_tag))
        self.tag_changed = False
        for t in tags:
            self._process_tag(t)

    def _update_tag(self):
        if self.tag_changed:
            print("%s tag conflict, created %s" % (self.initial_tag, incr_version(*self.max_tag)))
            self.max_tag = incr_version(*self.max_tag)
            self.max_tag_new = incr_version(*self.max_tag)
            if self.tag_prefix:
                self.max_tag = self.tag_prefix + "-" + "-".join(map(str, self.max_tag))
                self.max_tag_new = self.tag_prefix + "-".join(map(str, self.max_tag_new))
                self.config.set(
                    self.section, "version", self.max_tag.replace(self.tag_prefix, "")[1:]
                )
            else:
                self.max_tag = "-".join(map(str, self.max_tag))
                self.max_tag_new = "-".join(map(str, self.max_tag_new))
                self.config.set(self.section, "version", self.max_tag)
            f = open("%s/version.cfg" % app_path, "w")
            self.config.write(f)
            f.close()
            popen2.popen3(
                """git commit -m 'Bump version to %s [ci skip]' %s/version.cfg"""
                % (self.max_tag, app_path)
            )
            popen2.popen3("""git tag %s""" % self.max_tag)
        else:
            self.max_tag_new = incr_version(*self.max_tag)
            if self.tag_prefix:
                self.max_tag = self.tag_prefix + "-" + "-".join(map(str, self.max_tag))
                self.max_tag_new = self.tag_prefix + "-" + "-".join(map(str, self.max_tag_new))
            else:
                self.max_tag = "-".join(map(str, self.max_tag))
                self.max_tag_new = "-".join(map(str, self.max_tag_new))
            popen2.popen3("""git tag %s""" % self.max_tag)
            os.system("git push origin HEAD:%s" % self.branch)
        os.system("git push origin --tags")
        if self.tag_prefix:
            self.config.set(
                self.section, "version", self.max_tag_new.replace(self.tag_prefix, "")[1:]
            )
        else:
            self.config.set(self.section, "version", self.max_tag_new)
            self.config.set(self.section, "last", "-".join(map(str, self.initial_tag)))
        f = open("%s/version.cfg" % app_path, "w")
        self.config.write(f)
        f.close()
        print("pushed tag %s" % self.max_tag)
        popen2.popen3(
            """git commit -m 'Bump version to %s [ci skip]' %s/version.cfg"""
            % (self.max_tag_new, app_path)
        )
        if self.tag_prefix:
            print("bumped to %s" % self.max_tag_new.replace(self.tag_prefix, "")[1:])
        else:
            print("bumped to %s" % self.max_tag_new)
        os.system("git push origin HEAD:%s" % self.branch)
        os.system("git push origin --tags")
        os.system("git push origin HEAD:%s" % self.branch)
        return self.max_tag

    def _compile_js(self):
        shutil.copyfile("%s/static/gulpfile.js" % app_path, "%s/static/gulpfile.js.bak" % app_path)
        try:
            gulpfile = open("%s/static/gulpfile.js.bak" % app_path, "r")
            gulpfile_new = open("%s/static/gulpfile.js" % app_path, "w")
            for line in gulpfile:
                if "gulp.task('default'" in line:
                    gulpfile_new.write(line.replace(", 'watch'", ""))
                else:
                    gulpfile_new.write(line)
            gulpfile.close()
            gulpfile_new.close()
            subprocess.call(["gulp"], cwd=app_path + "/static")
        finally:
            shutil.move("%s/static/gulpfile.js.bak" % app_path, "%s/static/gulpfile.js" % app_path)

    def _ignore_uncomitted_changes(self):
        if subprocess.call(["git", "diff", "--quiet"]):
            subprocess.call(["git", "status"])
            print("")
            print("")
            answer = input("\033[93m There are uncomitted changes - are you sure? [y/N] \033[0m")
            print("")
            print("")
            if answer.upper() not in ["Y", "YES"]:
                return False
        return True

    def _run_tests(self):
        subprocess.check_call(["%s/runtests.sh" % app_path])

    def _publish_to_slack(self, slack_channel):
        sc = SlackClient(options.slack_token)
        sc.api_call(
            "chat.postMessage",
            channel=slack_channel,
            text=options.slack_message,
            username="deploy.py",
            icon_emoji=":robot_face:",
        )
        if self.new_commits:
            sc.api_call(
                "chat.postMessage",
                channel=slack_channel,
                text=self.new_commits,
                username="deploy.py",
                icon_emoji=":robot_face:",
            )

    def _rebuild_lib(self):
        shutil.rmtree("lib")
        subprocess.call("pip install -U -t lib/ -r requirements.txt".split(" "))

    def _commits(self):
        if self.last_tag:
            print("")
            print("")
            print(("Commits between %s and HEAD" % "-".join(map(str, self.last_tag))))
            print("")
            self.new_commits = subprocess.check_output(
                ["git", "log", "--pretty=oneline", "HEAD...%s" % "-".join(map(str, self.last_tag))]
            )
            print(("\033[93m{}\033[0m".format(self.new_commits)))

            if self.new_commits:
                commits = ""
                for line in self.new_commits.split("\n"):
                    if "Bump version" in line:
                        continue
                    if "Merge pull request" in line:
                        continue
                    commits = commits + line[34:] + "\n"
                self.new_commits = commits

    def _deploy(self, release):
        print("")
        print("")
        print("")
        print(("Deploying tag\033[93m %s \033[0mto \033[93m%s \033[0m" % (release, options.env)))
        print("")
        print("")
        print("")
        shutil.copyfile("%s/app.yaml" % app_path, "%s/app.yaml.bak" % app_path)
        version = "version: %s\n" % release
        project = "octopus-portal-apps-local"
        if options.env == config.PROD:
            application = "application: octopus-portal-apps\n"
            app_env = "  PORTAL_APPS_ENV: prod\n"
            project = "octopus-portal-apps"
        elif options.env != "local":
            application = "application: octopus-portal-apps-%s\n" % options.env
            app_env = "  PORTAL_APPS_ENV: %s\n" % options.env
            project = "octopus-portal-apps-%s" % options.env
        try:
            app_yaml = open("%s/app.yaml.bak" % app_path, "r")
            app_yaml_new = open("%s/app.yaml" % app_path, "w")
            for line in app_yaml:
                if "application:" in line:
                    app_yaml_new.write(application)
                elif "app-version" in line:
                    app_yaml_new.write(version)
                elif "PORTAL_APPS_ENV" in line:
                    app_yaml_new.write(app_env)
                else:
                    app_yaml_new.write(line)
            app_yaml.close()
            app_yaml_new.close()

            subprocess.call(["appcfg.py", "update", "."])
            subprocess.call(
                [
                    "gcloud",
                    "app",
                    "services",
                    "set-traffic",
                    "default",
                    "--splits",
                    "%s=1" % release,
                    "--quiet",
                    "--project",
                    project,
                ]
            )
            for slack_channel in options.slack_channels:
                self._publish_to_slack(slack_channel)

        finally:
            shutil.move("%s/app.yaml.bak" % app_path, "%s/app.yaml" % app_path)
            os.utime("%s/app.yaml" % app_path, None)  # equivalent of touch

    def process(self):
        release = None
        if True or self._ignore_uncomitted_changes():
            if options.env == config.PROD:
                self._rebuild_lib()
                # self._run_tests()
                # self._compile_js()
                self._set_prefix()
                self._process_tags()
                self._commits()

                # carry on?
                # answer = raw_input("\033[93m Carry on wih deployment? [y/N] \033[0m")
                # if answer.upper() in ['Y', 'YES']:
                release = self._update_tag()
                # deploying these can be done in one appcfg command but it essentially
                # does the same thing... runs it twice
                self._deploy(release)

            elif options.env == config.DEV:
                release = self.branch
                self._deploy(release)

        return release


def main():
    print("")
    print("")
    print(("Running deploy for\033[93m %s \033[0m" % options.env))
    print("")
    print("")

    branch = os.getenv("PORTAL_APPS_GIT_BRANCH", current_branch())
    release = Release(branch)
    release.process()


if __name__ == "__main__":
    main()
